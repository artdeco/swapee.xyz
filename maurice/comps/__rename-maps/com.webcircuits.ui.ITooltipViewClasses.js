/** @type {!com.webcircuits.ui.ITooltipClasses} */
const TooltipClasses={
  PopupWr: '_563-a',
  SvgDimensions: '_563-b',
  Absolute: '_563-c',
  Top: '_563-d',
  Left: '_563-e',
  Fixed: '_563-f',
  Backdrop: '_563-g',
  PopupContent: '_563-h',
  PopupClientRect: '_563-i',
  BorderPolygon: '_563-j',
  BorderWr: '_563-k',
  InnerBorderWr: '_563-l',
  Clip: '_563-m',
  Border: '_563-n',
  BorderPathSvg: '_563-o',
  AbsoluteTop: '_563-p',
  ShadowWr: '_563-q',
  BorderPath: '_563-r',
  ShownTooltip: '_563-s',
  PopupClip: '_563-t',
  PositionTracker: '_563-u',
  BackdropWr: '_563-v'
}
export default TooltipClasses