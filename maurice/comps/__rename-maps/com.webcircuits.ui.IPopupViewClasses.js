/** @type {!com.webcircuits.ui.IPopupClasses} */
const PopupClasses={
  Shown: '_e2b-a',
  Testing: '_e2b-b',
  Hidden: '_e2b-c',
  Closing: '_e2b-d'
}
export default PopupClasses