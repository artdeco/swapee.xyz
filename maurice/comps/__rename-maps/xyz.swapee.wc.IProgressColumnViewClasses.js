/** @type {!xyz.swapee.wc.IProgressColumnClasses} */
const ProgressColumnClasses={
  ProgressVertLine: '_dbd-a',
  ProgressCircleWr: '_dbd-b',
  ProgressCircle: '_dbd-c',
  CircleLoading: '_dbd-d',
  CircleWaiting: '_dbd-e',
  StepLabelWaiting: '_dbd-f',
  Link: '_dbd-g',
  CheckIm: '_dbd-h',
  CircleFailed: '_dbd-i',
  StepLabelFailed: '_dbd-j',
  CircleComplete: '_dbd-k',
  TickSvg: '_dbd-l',
  StepInfo: '_dbd-m',
  TgImg: '_dbd-n',
  StepLa: '_dbd-o',
  KycInfo: '_dbd-p',
  KycCrossing: '_dbd-q'
}
export default ProgressColumnClasses