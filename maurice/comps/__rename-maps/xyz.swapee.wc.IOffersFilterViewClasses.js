/** @type {!xyz.swapee.wc.IOffersFilterClasses} */
const OffersFilterClasses={
  FilterRowInputs: '_865-a',
  CryptoInput: '_865-b',
  CryptoOutput: '_865-c',
  AmountOutWr: '_865-d',
  CryptoSearch: '_865-e',
  MyCryptoSelect: '_865-f',
  MyCryptoSelectedBl: '_865-g',
  SwapCryptoBu: '_865-h',
  FilterFilterRow: '_865-i',
  FilterRowRate: '_865-j',
  RateType: '_865-k',
  SelectedRateType: '_865-l',
  Showable: '_865-m',
  RowOffers: '_865-n',
  LoadingWr: '_865-o'
}
export default OffersFilterClasses