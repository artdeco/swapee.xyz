/** @type {!xyz.swapee.wc.IOfferRowClasses} */
const OfferRowClasses={
  InnerOfferRow: '_8d8-a',
  OfferValue: '_8d8-b',
  EtaWr: '_8d8-c',
  Pointer: '_8d8-d',
  BestOfferPlaque: '_8d8-e',
  GetText: '_8d8-f',
  GetImg: '_8d8-g',
  OfferAmount: '_8d8-h',
  OfferCrypto: '_8d8-i',
  LogoPad: '_8d8-j',
  CoinImPoly: '_8d8-k',
  CancelLa: '_8d8-l',
  GetDealLa: '_8d8-m',
  ProgressVertLineLogoStub: '_8d8-n',
  OfferH: '_8d8-o',
  CoinIm: '_8d8-p',
  BestOffer: '_8d8-q',
  Eta: '_8d8-r',
  PolyBest: '_8d8-s',
  PolyRec: '_8d8-t',
  Poly: '_8d8-u',
  Recommended: '_8d8-v',
  RecommendedPlaque: '_8d8-w',
  RecommendedPopup: '_8d8-x'
}
export default OfferRowClasses