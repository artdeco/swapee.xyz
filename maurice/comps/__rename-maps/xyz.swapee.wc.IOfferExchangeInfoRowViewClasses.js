/** @type {!xyz.swapee.wc.IOfferExchangeInfoRowClasses} */
const OfferExchangeInfoRowClasses={
  NoticeLa: '_a81-a',
  FeeLaWr: '_a81-b',
  CryptoIm: '_a81-c',
  CryptoAmount: '_a81-d',
  GetOfferErrorWr: '_a81-e',
  ReloadGetOfferBu: '_a81-f',
  InfoColLeft: '_a81-g',
  InfoColRight: '_a81-h',
  BreakdownBu: '_a81-i',
  AmountLa: '_a81-j',
  Showable: '_a81-k'
}
export default OfferExchangeInfoRowClasses