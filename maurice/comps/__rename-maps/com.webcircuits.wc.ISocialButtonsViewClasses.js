/** @type {!com.webcircuits.wc.ISocialButtonsClasses} */
const SocialButtonsClasses={
  Button: '_e17-a',
  XButton: '_e17-b',
  FacebookButton: '_e17-c',
  LinkedInButton: '_e17-d',
  EmailButton: '_e17-e'
}
export default SocialButtonsClasses