/** @type {!xyz.swapee.wc.ISwapeeMeAccountClasses} */
const SwapeeMeAccountClasses={
  UserpicIm: '_f63-a',
  NoUserBlock: '_f63-b',
  Showable: '_f63-c',
  Shown: '_f63-d'
}
export default SwapeeMeAccountClasses