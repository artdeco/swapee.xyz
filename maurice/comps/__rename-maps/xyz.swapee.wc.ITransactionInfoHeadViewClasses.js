/** @type {!xyz.swapee.wc.ITransactionInfoHeadClasses} */
const TransactionInfoHeadClasses={
  NoticeLa: '_ec4-a',
  FeeLaWr: '_ec4-b',
  CryptoIm: '_ec4-c',
  CryptoAmount: '_ec4-d',
  GetOfferErrorWr: '_ec4-e',
  ReloadGetOfferBu: '_ec4-f',
  InfoColLeft: '_ec4-g',
  InfoColRight: '_ec4-h',
  BreakdownBu: '_ec4-i',
  AmountLa: '_ec4-j',
  Showable: '_ec4-k'
}
export default TransactionInfoHeadClasses