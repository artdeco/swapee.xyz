/** @type {!xyz.swapee.wc.IFileTreeClasses} */
const FileTreeClasses={
  ContentWr: '_396-a',
  Heading: '_396-b',
  FolderWr: '_396-c',
  File: '_396-d',
  FileOpen: '_396-e'
}
export default FileTreeClasses