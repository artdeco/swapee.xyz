/** @type {!xyz.swapee.wc.ICryptoSelectClasses} */
const CryptoSelectClasses={
  CryptoDown: '_3eb-a',
  CryptoSelectedBl: '_3eb-b',
  LabelWr: '_3eb-c',
  CryptoSelectedNameIn: '_3eb-d',
  Menu: '_3eb-e',
  NetworkLabel: '_3eb-f',
  ImgWr: '_3eb-g',
  KeyboardSelect: '_3eb-h',
  CryptoDropItem: '_3eb-i',
  DropDownKeyboardFocus: '_3eb-j',
  OneBeforeKeyboardSelect: '_3eb-k',
  ItemHovered: '_3eb-l',
  Pill: '_3eb-m',
  Highlight: '_3eb-n',
  MouseOver: '_3eb-o',
  Chevron: '_3eb-p',
  CryptoSelectedImWr: '_3eb-q',
  ItemWr: '_3eb-r',
  CryptoDropItemBeforeHover: '_3eb-s',
  FiatIcon: '_3eb-t',
  BackgroundStalked: '_3eb-u',
  CryptoMenu: '_3eb-v',
  HoveringOverItem: '_3eb-w',
  CryptoMenuPad: '_3eb-x'
}
export default CryptoSelectClasses