/** @type {!xyz.swapee.wc.IExchangeIdRowClasses} */
const ExchangeIdRowClasses={
  RateType: '_e19-a',
  SelectedRateType: '_e19-b',
  ExchangeIdLa: '_e19-c'
}
export default ExchangeIdRowClasses