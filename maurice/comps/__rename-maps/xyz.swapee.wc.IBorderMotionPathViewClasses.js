/** @type {!xyz.swapee.wc.IBorderMotionPathClasses} */
const BorderMotionPathClasses={
  ParticlesWr: '_093-a',
  Ready: '_093-b',
  ParticleWr: '_093-c',
  Particle: '_093-d'
}
export default BorderMotionPathClasses