import makeClassGetter from '../__mcg'
const renameMaps = {  }
import { makeIo, init, startPlain } from '../__competent-lib'
import Highlightjs from 'splendid/build/components/highlightjs/index'

const __components = {
  'highlightjs': () => Highlightjs,
}

const io = makeIo()

const meta = [{
  key: 'highlightjs',
  id: ',',
  props: {
    lang: 'javascript',
  },
}]
meta.forEach(({ key, id, props = {}, children = [] }) => {
  const Comp = __components[key]()
  const plain = true
  props.splendid = { mount: '/', addCSS(stylesheet) {
    return makeClassGetter(renameMaps[stylesheet])
  }, css(paths, rootSelector, { mapName } = {}) {
    if (mapName && !mapName.endsWith('.js')) {
      mapName = mapName += '.js'
    }
    return this.addCSS(mapName || paths)
  },
  elementRelative(path) { return `${path}` },
  export() {},
}

  const ids = id.split(',')
  ids.forEach((Id) => {
    const { parent, el } = init(Id, key)
    if (!el) return
    const renderMeta = /** @type {_competent.RenderMeta} */ ({ key, id: Id, plain })
    let comp
    el.rerender = () => {
      if (!comp) return
      const getComp = () => __components[key]()
      comp.rerender(getComp, { children, ...props })
    }
    el.render = () => {
      comp = startPlain(renderMeta, Comp, comp, el, parent, props, children)
      return comp
    }
    el.render.meta = renderMeta
    io.observe(el)
  })
})
