import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.changelly-exchange','ChangellyExchange',true)
// const Placeholder=makePlaceholder('xyz.swapee.changelly-exchange','ChangellyExchange',__dirname)

export default Placeholder
