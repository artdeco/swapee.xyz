import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.swapee-loading','SwapeeLoading',true)
const Placeholder=makePlaceholder('xyz.swapee.swapee-loading','SwapeeLoading',__dirname)

export default Placeholder
