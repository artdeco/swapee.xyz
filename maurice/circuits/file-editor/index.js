import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.file-editor','FileEditor',true)
const Placeholder=makePlaceholder('xyz.swapee.file-editor','FileEditor',__dirname)

export default Placeholder
