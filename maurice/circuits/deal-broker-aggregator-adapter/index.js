import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.deal-broker-aggregator-adapter','DealBrokerAggregatorAdapter',true)
// const Placeholder=makePlaceholder('xyz.swapee.deal-broker-aggregator-adapter','DealBrokerAggregatorAdapter',__dirname)

export default Placeholder
