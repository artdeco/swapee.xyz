import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.region-ping','RegionPing',true)
const Placeholder=makePlaceholder('xyz.swapee.region-ping','RegionPing',__dirname)

export default Placeholder
