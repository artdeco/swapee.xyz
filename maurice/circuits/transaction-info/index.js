import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.transaction-info','TransactionInfo',true)
// const Placeholder=makePlaceholder('xyz.swapee.transaction-info','TransactionInfo',__dirname)

export default Placeholder
