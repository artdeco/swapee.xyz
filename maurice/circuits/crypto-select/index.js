import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.crypto-select','CryptoSelect',true)
// const Placeholder=makePlaceholder('xyz.swapee.crypto-select','CryptoSelect',__dirname)

export default Placeholder
