/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine._inputKeyboardScroll} */
export default function _inputKeyboardScroll(kev) {
 const{
  asICryptoSelectTouchscreen,
  asICryptoSelectTouchscreen:{
   visibleItems:visibleItems,keyboardItem:keyboardItem,
   asICryptoSelectDisplay:{
    CryptoSelectedNameIn:CryptoSelectedNameIn,
   },
  },
 }=this
 if(kev.key=='ArrowUp'||kev.key=='ArrowDown') {
  kev.preventDefault()
 }

 let isSelectingBackwardSearch=false

 if(kev.key=='ArrowUp') {
  let newKeyboardItem
  if(keyboardItem) {
   let ind=visibleItems.indexOf(keyboardItem)
   newKeyboardItem=visibleItems[ind-1]
   // debugger
   if(ind-1==-1) { // isSelectingBackSearch
    isSelectingBackwardSearch=true
   }
  }
  // check if can go to bottom - "enableWrapping" option
  // else if(visibleItems.length) {
  //  newKeyboardItem=visibleItems[visibleItems.length-1]
  // }
  // need to change keyboard item manually -> is anything reacting to keyboard
  // item -> yes, adapt paint keyboard

  return{
   keyboardItem:newKeyboardItem,
  }
 }

 if(kev.key=='ArrowDown') {
  let newKeyboardItem
  if(keyboardItem) {
   let ind=visibleItems.indexOf(keyboardItem)
   newKeyboardItem=visibleItems[ind+1]
   // check whether will go back to the top via options
   // if(!newKeyboardItem) {
   //  newKeyboardItem=visibleItems[ind+1]
   // }
  } else if(visibleItems.length) {
   newKeyboardItem=visibleItems[0]
  }

  return {
   keyboardItem:newKeyboardItem,
  }
 }


 if(kev.key=='Enter') {
  let selected

  //  const item=resolveItem(dropDownKeyboardFocusIndex)
  if(keyboardItem) {
   selected=keyboardItem.dataset['value']
  }
  if(selected) {
   CryptoSelectedNameIn.blur()
  }
  return {
   selected:selected,
   menuExpanded:(selected?false:undefined)||isSelectingBackwardSearch,
  }
 }
}