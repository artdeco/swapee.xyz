import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.swapee-me','SwapeeMe',true)
// const Placeholder=makePlaceholder('xyz.swapee.swapee-me','SwapeeMe',__dirname)

export default Placeholder
