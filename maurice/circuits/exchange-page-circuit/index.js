import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.exchange-page-circuit','ExchangePageCircuit',true)
// const Placeholder=makePlaceholder('xyz.swapee.exchange-page-circuit','ExchangePageCircuit',__dirname)

export default Placeholder
