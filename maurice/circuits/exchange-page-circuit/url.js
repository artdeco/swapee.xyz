/** @type {xyz.swapee.wc.IExchangePageCircuitPort.Inputs} */
export const urlInputs={
 amountFrom:'amount',
 cryptoIn:'from',
 cryptoOut:'to',
 type:'type',
 id:'id',
}