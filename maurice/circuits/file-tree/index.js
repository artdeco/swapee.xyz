import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.file-tree','FileTree',true)
const Placeholder=makePlaceholder('xyz.swapee.file-tree','FileTree',__dirname)

export default Placeholder
