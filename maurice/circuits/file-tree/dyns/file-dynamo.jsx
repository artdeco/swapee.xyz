/** @type {xyz.swapee.wc.IFileTreeGenerator.FileDynamo} */
export const FileDynamo=({
 name:name,path,lang,isOpen:isOpen,
},{FileTree:{openFile}})=>{
 return(<span $id="File" FileOpen={isOpen} onClick={openFile(path,name,lang)}>
  <span $id="FileName">{name}</span>
 </span>)
}