/** @type {xyz.swapee.wc.IFileTreeGenerator.FolderDynamo} */
export const FolderDynamo=({name:name})=>{
 return(<span $id="Folder">
  <span $id="FolderName">{name}</span>
 </span>)
}