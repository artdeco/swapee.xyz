import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.offer-row','OfferRow',true)
const Placeholder=makePlaceholder('xyz.swapee.offer-row','OfferRow',__dirname)

export default Placeholder
