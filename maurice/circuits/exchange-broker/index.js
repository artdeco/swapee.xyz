import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.exchange-broker','ExchangeBroker',true)
// const Placeholder=makePlaceholder('xyz.swapee.exchange-broker','ExchangeBroker',__dirname)

export default Placeholder
