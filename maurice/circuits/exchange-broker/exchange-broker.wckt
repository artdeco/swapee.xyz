import {xyz} from 'types'

/**
 * Relays actions and messages between offer-exchange and specific exchange
 * components.
 */
export default class ExchangeBroker extends (
 /** @type {typeof xyz.swapee.wc.AbstractExchangeBroker} */ (<xyz.swapee.wc.IExchangeBroker>
  <core>
   {/* <string name="core">
    The core property.
   </string> */}
   <bool name="creatingTransaction">
    Creating the transaction now. Set by the exchange APIs when they start
    loading the first step of creating a transaction in their systems.
   </bool>
   <bool name="checkingStatus">
    Whether checking for payment now. Set by the exchange APIs when the user
    checks for the status of the transaction created as the first step.
   </bool>
   <bool name="checkStatusError">
    Whether the last status check resulted in error, the timer won't be reset.
   </bool>

   <bool name="processingPayment" />

   <pulse name="createTransaction">.</pulse>

   <string name="createTransactionError" setter />
   {/* <string name="fixedId">
    The ID of a fixed-rate transaction for which this rate applies.
   </string> */}
   <date name="statusLastChecked" setter nullable />

   {/* <bool name="processingPayment">
    The payment has been received.
   </bool> */}

   <pulse name="checkPayment">.</pulse>
   <string name="checkPaymentError" setter>Either a network, or service error.</string>

   {/* every item in core needs setters */}
   <string name="address" setter>
    Recipient address.
   </string>
   <string name="extraId" setter>
    Additional ID for address for currencies that use additional ID for
    transaction processing (XRP, XLM, EOS, IGNIS, BNB, XMR, ARDOR, DCT, XEM).
   </string>
   <string name="refundAddress" setter>
    Address of the wallet to refund in case of any technical issues during
    the exchange. The currency of the wallet must match with the from currency.
   </string>
   <string name="refundExtraId" setter>
    extraId for `refundAddress`.
   </string>

   <string name="id">The ID of the transaction.</string>
   <string name="trackUrl" />
   <string name="type" />
   <string name="payinAddress" />
   <bool name="kycRequired" nullable>Whether the user will have to pass KYC.</bool>

   <string name="confirmedAmountFrom">
    The amount that the user is expected to send as confirmed by the system.
   </string>

   <string name="payinExtraId" />
   <string name="payoutAddress" />
   <string name="payoutExtraId" />
   <enum name="status">
    <choices new waiting confirming exchanging sending finished failed refunded hold overdue expired />
   </enum>
   <string name="createdAt" />
  </core>

  <cache>
   <bool name="checkingPaymentStatus">
    Checking the status before the payment is received.
   </bool>
   <bool name="checkingExchangeStatus">
    Checking the status of the exchange after the payment is received.
   </bool>

   <date name="createdDate" nullable />
   <date name="finishedDate" nullable>
    Remembers when the exchange was finished to present the duraction at the end
    of the exchange for user experience.
   </date>
   <string name="Id" />
   <string name="notId" />

   <enum name="paymentStatus">
    <choices waiting confirming confirmed overdue expired />
    The status of the payment only (no processing).
   </enum>

   <bool name="waitingForPayment">
    The exchange hasn't started yet, waiting for payment to kick off the
    exchange process.
   </bool>
   <bool name="paymentExpiredOrOverdue">
    The exchange finished at the payment stage, without processing.
   </bool>
   <bool name="exchangeComplete">
    Whether the exchange has been completed (can be failed or finished).
   </bool>
   <bool name="exchangeFinished">The exchange has finished successfuly.</bool>
   <bool name="paymentReceived">
     Whether the payment was received but not confirmed yet.
   </bool>
   <bool name="paymentCompleted">
     Whether the payment was confirmed.
   </bool>
  </cache>

  <vdus>
   <div name="Debug" />
   {/* <button name="SubmitBu" /> */}
  </vdus>

  <classes>
   {/* <class name="Class">The class.</class> */}
  </classes>

  <computer>
   <adapter name="adaptPaymentReceived">
    <xyz.swapee.wc.IExchangeBrokerCore paymentReceived="empty" />
    <xyz.swapee.wc.IExchangeBrokerCore paymentStatus="required" />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentReceived />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentCompleted">
    <xyz.swapee.wc.IExchangeBrokerCore paymentCompleted="empty" />
    <xyz.swapee.wc.IExchangeBrokerCore paymentStatus="required" />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentCompleted />
    </outputs>
   </adapter>
   <adapter name="adaptCheckingPaymentStatus">
    <xyz.swapee.wc.IExchangeBrokerCore checkingStatus status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore checkingPaymentStatus />
    </outputs>
   </adapter>
   <adapter name="adaptCheckingExchangeStatus">
    <xyz.swapee.wc.IExchangeBrokerCore checkingStatus status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore checkingExchangeStatus />
    </outputs>
   </adapter>

   <adapter name="adaptFinishedDate">
    <xyz.swapee.wc.IExchangeBrokerCore exchangeFinished />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore finishedDate />
    </outputs>
    When the status changes to "finished", records the date.
   </adapter>
   <adapter name="adaptWaitingForPayment">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore waitingForPayment />
    </outputs>
   </adapter>
   <adapter name="adaptExchangeFinished">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore exchangeFinished />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentStatus">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentStatus />
    </outputs>
   </adapter>
   <adapter name="adaptProcessingPayment">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore processingPayment />
    </outputs>
   </adapter>
   <adapter name="adaptCreatedDate">
    <xyz.swapee.wc.IExchangeBrokerCore createdAt />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore createdDate />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentExpiredOrOverdue">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentExpiredOrOverdue />
    </outputs>
   </adapter>
   <adapter name="adaptExchangeComplete">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore exchangeComplete />
    </outputs>
   </adapter>

   <adapter name="adaptNotId">
    <xyz.swapee.wc.IExchangeBrokerCore id />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore notId Id />
    </outputs>
   </adapter>

   <adapter name="adaptStatusLastChecked">
    <xyz.swapee.wc.IExchangeBrokerCore checkStatusError="empty" checkingStatus="empty" />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore statusLastChecked />
    </outputs>
   </adapter>
   {/* <adapter name="adaptSomething">
    <xyz.swapee.wc.IExchangeBrokerCore />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore />
    </outputs>
   </adapter> */}
  </computer>

  <controller>
   <method name="reset" void on>
    Called to start over.
   </method>
  </controller>

  {/* <irq>
   <method name="submitBuCl" />
  </irq> */}

  <element v3 html>
   <attrs>
    {/* HTML attributes */}
   </attrs>
  </element>

  <records>
  </records>
 </xyz.swapee.wc.IExchangeBroker>).implements(
  /** @type {!xyz.swapee.wc.IExchangeBrokerScreen} */({
   // deduceInputs(el) {},
  }),
  /** @type {!xyz.swapee.wc.IExchangeBrokerHtmlComponent} */({
  }),
  /** @type {!xyz.swapee.wc.IExchangeBrokerComputer} */({
   adaptCheckingPaymentStatus({checkingStatus:checkingStatus,status:status}) {
    if(!checkingStatus) return{checkingPaymentStatus:false}
    return{checkingPaymentStatus:['new','waiting','confirming'].includes(status)}
   },
   adaptCheckingExchangeStatus({checkingStatus:checkingStatus,status:status}) {
    if(!checkingStatus) return{checkingExchangeStatus:false}
    return{checkingExchangeStatus:!['new','waiting','confirming'].includes(status)}
   },
   adaptFinishedDate({exchangeFinished:exchangeFinished}) {
    if(!exchangeFinished)return{finishedDate:null}
    return{
     finishedDate:new Date,
    }
   },
   adaptExchangeFinished({status:status}) {
    return{exchangeFinished:status=='finished'}
   },
   adaptPaymentStatus({status:status}) {
    if(['new','waiting'].includes(status)) return {paymentStatus:'waiting'}
    if(status=='confirming') return {paymentStatus:'confirming'}

    if(status=='exchanging') return {paymentStatus:'confirmed'}
    if(status=='sending') return {paymentStatus:'confirmed'}
    if(status=='finished') return {paymentStatus:'confirmed'}

    if(status=='overdue') return {paymentStatus:'overdue'} // final state
    if(status=='expired') return {paymentStatus:'expired'} // final state

    return{paymentStatus:''}
   },
   adaptPaymentReceived({paymentStatus:paymentStatus}) {
    return{paymentReceived:['confirming','confirmed'].includes(paymentStatus)}
   },
   adaptPaymentCompleted({paymentStatus:paymentStatus}) {
    return{paymentCompleted:paymentStatus=='confirmed'}
   },
   adaptWaitingForPayment({status}) {
    return{
     waitingForPayment:[
      'new','waiting',
     ].includes(status),
    }
   },
   adaptProcessingPayment({status}) {
    return{
     processingPayment:[
      'confirming','exchanging','sending',
     ].includes(status),
    }
   },
   adaptCreatedDate({createdAt:createdAt}) { // todo: automatic conversion between lambda functions
    return{
     createdDate:new Date(createdAt),
    }
   },
   adaptExchangeComplete({status:status}) {
    return{
     exchangeComplete:[
      'finished','failed','refunded','overdue','expired',
     ].includes(status),
    }
   },
   adaptPaymentExpiredOrOverdue({status:status}) {
    return{
     paymentExpiredOrOverdue:[
      'overdue','expired',
     ].includes(status),
    }
   },
   adaptStatusLastChecked(_,{checkingStatus:prevCheckingPayment}) {
    if(!prevCheckingPayment) return
    // if(prevCheckStatusError) return
    return{
     statusLastChecked:new Date,
    }
   },
  }),
  /** @type {!xyz.swapee.wc.IExchangeBroker} */({
  }),
  /** @type {!xyz.swapee.wc.IExchangeBrokerComputer} */({
   adaptNotId({id:id}) {
    return{
     Id:!!id,
     notId:!id,
    }
   },
  }),
  /** @type {!xyz.swapee.wc.IExchangeBrokerProcessor} */({
   reset() { // todo: reset to specify which field to reset
    const{
     asIExchangeBrokerController:{setInputs:setInputs,resetPort},
     asIExchangeBrokerProcessor:{setInfo:setInfo},
    }=this
    setInfo({createdDate:null})
    setInputs({
     status:'',id:'',createdAt:'',payinAddress:'',
    })
    resetPort()
   },
  }),
  /** @type {!xyz.swapee.wc.IExchangeBrokerController} */({
  }),
  /** @type {xyz.swapee.wc.IExchangeBrokerElement} */({
   classesMap: true,
   rootSelector:     `.ExchangeBroker`,
   stylesheet:       'html/styles/ExchangeBroker.css',
   blockName:        'html/ExchangeBrokerBlock.html',
   solder({
    address:address,
   }) {
    return{
     address:address,
     // 'host':this.REMOTE_HOST,
    }
   },
   inducer({},{}) {
    return(<exchange-broker>
    </exchange-broker>)
   },
   server() {
    return (<div $id="ExchangeBroker">
    </div>)
   },
   render({id,createTransactionError,createdAt,payinAddress,payinExtraId},{}) {
    return (<div $id="ExchangeBroker">
     <pre $id="Debug">{id}</pre>
    </div>)
   },
  }),
  /** @type {xyz.swapee.wc.IExchangeBrokerDesigner} */({
   relay({This,ExchangeBroker:{unsetCreateTransactionError}}) {
    return (<>
     <This onCreateTransactionHigh={
      unsetCreateTransactionError()
     } />
    </>)
   },
  }),
 )
) { }
