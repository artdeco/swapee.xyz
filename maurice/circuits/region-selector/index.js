import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.region-selector','RegionSelector',true)
// const Placeholder=makePlaceholder('xyz.swapee.region-selector','RegionSelector',__dirname)

export default Placeholder
