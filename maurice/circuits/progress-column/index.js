import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.progress-column','ProgressColumn',true)
// const Placeholder=makePlaceholder('xyz.swapee.progress-column','ProgressColumn',__dirname)

export default Placeholder
