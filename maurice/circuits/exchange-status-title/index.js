import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.exchange-status-title','ExchangeStatusTitle',true)
// const Placeholder=makePlaceholder('xyz.swapee.exchange-status-title','ExchangeStatusTitle',__dirname)

export default Placeholder
