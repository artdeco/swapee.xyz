import {xyz,com} from 'types'
import '@webcircuits/ui'

/**
 * The generic exchange inside an offer-row, which connects to specific exchanges.
 */
export default class OfferExchange extends (
 /**@type {typeof xyz.swapee.wc.AbstractOfferExchange}*/(<xyz.swapee.wc.IOfferExchange>
  <core>
   <bool name="tocAccepted" default={true} setter>
    Whether the terms and conditions have been accepted.
   </bool>
   <enum name="step">
    <choices create send complete />
   </enum>
   <string name="kycEmail">
    The email to display in the progress column if KYC will be required.
   </string>
   <bool name="paymentFailed">
     Whether the exchange could not be completed.
   </bool>
   <pulse name="copyPayinAddress">.</pulse>
   <bool name="payinAddressJustCopied">.</bool>
   <bool name="payinAddressCopied">.</bool>
   <pulse name="copyPayinExtra">.</pulse>
  </core>

  <cache>
   <bool name="canCreateTransaction" />
   <bool name="btcInput" />
   <bool name="btcOutput" />
  </cache>

  <display>
   <painter name="paintCopyPayinAddress">
    <xyz.swapee.wc.IOfferExchangeCore copyPayinAddress="required" />
    <xyz.swapee.wc.IExchangeBrokerCore payinAddress />
   </painter>
   <painter name="paintCopyPayinExtra">
    <xyz.swapee.wc.IOfferExchangeCore copyPayinExtra="required" />
    <xyz.swapee.wc.IExchangeBrokerCore payinExtraId />
   </painter>
  </display>

  {/* foreign-land */}
  <land>
   <com.webcircuits.ui.IPopup via="AddressPopup" no-start />
   <com.webcircuits.ui.IPopup via="RefundPopup" no-start />
   <com.webcircuits.ui.ICollapsar via="SendPaymentCollapsar" no-start />
   <com.webcircuits.ui.ICollapsar via="CreateTransactionCollapsar" no-start />
   <com.webcircuits.ui.ICollapsar via="FinishedCollapsar" no-start />
   <xyz.swapee.wc.IProgressColumn />
   <xyz.swapee.wc.IProgressColumn via="ProgressColumnMobTop">
    Progress column for mobile (top part).
   </xyz.swapee.wc.IProgressColumn>
   <xyz.swapee.wc.IProgressColumn via="ProgressColumnMobBot">
    Progress column for mobile (bottom).
   </xyz.swapee.wc.IProgressColumn>
   <xyz.swapee.wc.ISwapeeWalletPicker />
   <xyz.swapee.wc.IExchangeBroker selector />
   <xyz.swapee.wc.IDealBroker outside />
   <xyz.swapee.wc.IExchangeIntent outside server-await />
   <xyz.swapee.wc.IExchangeStatusHint />

   <xyz.swapee.wc.ITransactionInfo>
    <after>
     <xyz.swapee.wc.IExchangeBrokerCore id="real" />
    </after>
   </xyz.swapee.wc.ITransactionInfo>

   {/* started by themselves */}
   <xyz.swapee.wc.IOfferExchangeInfoRow no-start />
   <xyz.swapee.wc.IOfferExchangeInfoRow via="OfferExchangeInfoRowMob" no-start />

   <xyz.swapee.wc.IExchangeStatusTitle />
   <com.webcircuits.ui.ITimeAgo via="PaymentLastChecked" />
   <com.webcircuits.ui.ITimeInterval via="Duration" no-start />
  </land>

  <screen classes>
   The screen.
  </screen>

  <vdus>
   {/* <span name ="LoadingStripe" /> */}
   <div name    ="AddReviewErrorWr" />
   <span name   ="AddReviewErrorLa" />
   <button name ="AddReviewBu" />

   <div name ="WalletIn" />
   <div name ="WalletOut" />
   <div name ="BtcWalletPicker" />
   <div name ="BtcWalletPickerCont" />

   <div name = "KycRequired" multiple />
   <div name = "KycNotRequired" multiple />
   <div name = "ExchangeEmail" multiple />

   <div name="OfferExchangeInfoRowMobWr" />

   <span name   ="TransactionCurrencyFromLa">
    Used on the "send payment to" step.
   </span>
   <span name   ="OptionalLa" />
   <span name   ="RequiredLa" />
   <span name   ="CreateTransactionWr" />

   <div name    ="TransactionInfoWr" />
   <div name    ="TransactionInfo" />

   <div name    ="TransactionInfoHeadWr" />
   <div name    ="TransactionInfoHead" />
   <div name    ="TransactionInfoHeadMobWr" />
   <div name    ="TransactionInfoHeadMob" />

   <span name ="PayinAddressCopiedLa" />
   <button name ="CopyPayinAddressBu" />
   <div name ="Step1ProgressCircleWrMobBot" />
   <div name ="Step1LaMobBot" />
   <div name ="Step2ProgressCircleWrMobBot" />
   <div name ="Step2LaMobBot" />

   <div name ="Step2CoMobTop" />
   <div name ="Step3CoMobTop" />

   <div name ="Step1CoMobBot" />
   <div name ="Step2CoMobBot" />
   <div name ="Step3CoMobBot" />

   <form name ="ReviewForm" />

   <span name ="ConfirmedAmountFrom">How much the user is expected to send.</span>
   <span name ="StatusLa" />
   <span name ="CheckedWr" />
   <div name ="ExchangeStatusHintWr" />
   <form name ="SendFo" />

   <span name ="UpdateStatusLa" />
   <span name ="PaymentSentLa" />

   <button name ="RestartBu">The button to start over.</button>

   <span name ="CheckingLoIn" />

   <div name ="SendPaymentErrorWr" />
   <span name ="SendPaymentError" />
   <button name ="CheckStatusBu" />

   <div name ="ExchangeApiWr">The wrapper for the exchange API.</div>

   <span name ="Step2Circle" />
   <div name  ="CreateTransactionCol" />
   <div name  ="SendPaymentCol" />
   <div name="PaymentAddressInWr" />
   <input name="PaymentAddressIn" selector id />
   <input name="PaymentAddressHiddenIn">
    If the payment is not expected anymore (overdue or expired, this field
    will show the hidden payment address).
   </input>

   {/* maybe we don't have to define every single vdu manually unless we want to add a description, to save time. */}
   <div name="CreateTransactionErrorWr" />
   <span name="CreateTransactionError" />

   <span name="CurrencyFromLa" multiple>
    Used on the "create transaction" step. Hidden before the intent is
    extracted from the URL params, as rendered currency might be different.
   </span>
   <span name="CurrencyToLa" multiple />

   <label name="AddressLa" />
   <input name="AddressIn" selector id>The field for the user's wallet address.</input>

   <input name="ExtraId" />
   <span name ="TocLa" />
   <input name="TocIn" selector id>The input for accepting terms and conditions.</input>
   <button name="CreateTransactionBu">The button to create a transaction.</button>

   <input name="RefundAddressIn" selector id />
   <label name="RefundAddressLa"/>
  </vdus>

  <classes>
   <class name="Loading">Added to buttons when loading.</class>
   <class name="InputActive">Added to inputs' wrappers when active.</class>
   <class name="ProgressVertLine">.</class>
   <class name="PaymentReceived" />
   <class name="CircleLoading" />
   <class name="CircleComplete"/>
   <class name="CircleWaiting"/>
   <class name="HintRed"/>

   <class name="Copied"/>
   <class name="CopiedRemoving"/>
  </classes>

  <computer>
   <adapter name="adaptPickBtc">
    <xyz.swapee.wc.ISwapeeWalletPickerCore invoice="required" />
    <xyz.swapee.wc.IOfferExchangeCore btcInput btcOutput />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerPort address refundAddress />
    </outputs>
   </adapter>

   <adapter name="adaptBtc">
    <xyz.swapee.wc.IExchangeIntentCore currencyFrom="real" currencyTo  />
    <outputs>
     <xyz.swapee.wc.IOfferExchangeCore btcInput btcOutput />
    </outputs>
   </adapter>

   <adapter name="adaptCanCreateTransaction">
    <xyz.swapee.wc.IOfferExchangeCore tocAccepted />
    <xyz.swapee.wc.IExchangeIntentCore fixed />
    <xyz.swapee.wc.IExchangeBrokerCore creatingTransaction address="real" refundAddress />
    <outputs>
     <xyz.swapee.wc.IOfferExchangeCore canCreateTransaction />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentFailed">
    <xyz.swapee.wc.IExchangeBrokerCore status="real" />
    <outputs>
     <xyz.swapee.wc.IOfferExchangeCore paymentFailed />
    </outputs>
   </adapter>
  </computer>

  <controller>
   <method name="selectAddress" on>
    <arg name="address" string />
    When the address was selected from a popup.
   </method>
   <method name="swapAddresses" on>
    If the pair's direction changed, updates the addressed if they were entered.
   </method>
   <method name="selectRefund" on>
    <arg name="refund" string />
    When the refund address was selected from a popup.
   </method>
   <method name="reset" on void />
  </controller>

  <irq>
   <method name="atInputFocus" />
   <method name="atInputBlur" />
   <method name="atAddressPopupClick" />
  </irq>

  {/* <irq>
   <method name="submitBuCl" />
  </irq> */}

  <inputs>
   <htm name="exchangeApiContent" />
  </inputs>

  <element v3 html>
   <selectors>
    {/* todo: ids for selectors */}
    {/* <selector name="tocInput" id /> */}
   </selectors>
   <attrs>
    {/* HTML attributes */}
   </attrs>
  </element>
  <records>
  </records>
 </xyz.swapee.wc.IOfferExchange>).implements(
  /** @type {!xyz.swapee.wc.IOfferExchangeTouchscreen} */({
   deduceInputs(el) {},
  }),
  /** @type {!xyz.swapee.wc.IOfferExchangeDisplay} */({
   paintCopyPayinAddress(_,{ExchangeBroker:{payinAddress:payinAddress}}) { // todo: somehow make this reusable via "copy" attribute?
    const{asIOfferExchangeTouchscreen:{
     classes:{Copied:Copied,CopiedRemoving:CopiedRemoving},
     asIScreen:{copyToClipboard:copyToClipboard},
     // pulsePayinAddressCopied:pulsePayinAddressCopied,
     setInputs:setInputs,
    },asIOfferExchangeDisplay:{PayinAddressCopiedLa},
    }=this
    copyToClipboard(payinAddress)
    // pulsePayinAddressCopied()
    if(PayinAddressCopiedLa.classList.contains(Copied)||PayinAddressCopiedLa.classList.contains(CopiedRemoving)) {
     return
    }
    // setTimeout(()=>{
    //  setInputs({payinAddressCopied:true,payinAddressJustCopied:false})
    // },1)
    const play=()=>{
     setInputs({payinAddressCopied:true,payinAddressJustCopied:false})
     setTimeout(()=>{
      setInputs({payinAddressJustCopied:true})
      setTimeout(()=>{
       setInputs({payinAddressCopied:false,payinAddressJustCopied:false})
      },310)
     },1100)
    }

    play()
   },
  }),
  /** @type {!xyz.swapee.wc.IOfferExchangeInterruptLine} */({
   atAddressPopupClick(ev) {
    const{target:{dataset:{'address':address}}}=ev
    if(!address) return
    const{asIOfferExchangeTouchscreen:{selectAddress:selectAddress}}=this
    selectAddress(address)
   },
   atInputBlur(ev){
    const{
     asIOfferExchangeTouchscreen:{
      classes:{InputActive:InputActive},
     },
    }=this
    const{target}=ev
    target.parentElement.classList.remove(InputActive)
    // element.
   },
   atInputFocus(ev){
    const{
     asIOfferExchangeTouchscreen:{
      classes:{InputActive:InputActive},
     },
    }=this
    // can there be component to do this declaratively.
    const{target}=ev
    target.parentElement.classList.add(InputActive)
   },
  }),
  /** @type {!xyz.swapee.wc.IOfferExchangeHtmlComponent} */({
  }),
  /** @type {!xyz.swapee.wc.IOfferExchangeComputer} */({
   adaptPickBtc({invoice:invoice,btcInput:btcInput,btcOutput:btcOutput},{invoice:invoiceBeforeChange}) {
    if(invoiceBeforeChange===undefined) return {}
    if(btcInput) {
     return{
      refundAddress:invoice,
     }
    }
    if(btcOutput) {
     return{
      address:invoice,
     }
    }
   },
   adaptBtc({currencyFrom:currencyFrom,currencyTo:currencyTo}) {
    return {
     btcInput:`${currencyFrom}`.toLowerCase()=='btc',
     btcOutput:`${currencyTo}`.toLowerCase()=='btc',
    }
   },
   adaptPaymentFailed({status:status}) {
    return{
     paymentFailed:[
      'failed','refunded','overdue','expired',
     ].includes(status),
    }
   },
   adaptCanCreateTransaction({
    address:address,creatingTransaction:creatingTransaction,
    tocAccepted:tocAccepted,fixed:fixed,refundAddress:refundAddress,
   }) {
    return{
     canCreateTransaction:!!((!creatingTransaction&&address&&tocAccepted)&&(fixed?refundAddress:true)),
    }
   },
   // adaptSyncBroker({address,creatingTransaction}) {
   //  return{address,creatingTransaction}
   // },
  }),
  /** @type {!xyz.swapee.wc.IOfferExchange} */({
  }),
  /** @type {!xyz.swapee.wc.IOfferExchangeComputer} */({
  }),
  /** @type {!xyz.swapee.wc.IOfferExchangeController} */({
  }),
  /** @type {xyz.swapee.wc.IOfferExchangeElement} */({
   classesMap: true,
   rootSelector:     `.OfferExchange`,
   blockers: [
    'xyz.swapee.wc.IProgressColumn',
   ],
   preserveClasses:  ['ProgressColumn'],
   stylesheet:       [
    'html/styles/OfferExchange.css',
    // 'html/styles/ExchangeIdRow.css',
   ],
   blockName:        'html/OfferExchangeBlock.html',
   solder({
    currencyFrom:currencyFrom,currencyTo:currencyTo,amountFrom:amountFrom,
   }) {
    return{
     amountFrom:amountFrom,
     currencyTo:currencyTo,
     currencyFrom:currencyFrom,
    }
   },
   inducer({},{exchangeApiContent:exchangeApiContent}) {
    return(<offer-exchange>
     <exchange-api>{exchangeApiContent}</exchange-api>
    </offer-exchange>)
   },
   short({
    paymentFailed:paymentFailed,
   },{
    ProgressColumn:ProgressColumn,
    ProgressColumnMobBot:ProgressColumnMobBot,
    ProgressColumnMobTop:ProgressColumnMobTop,

    CreateTransactionCollapsar:CreateTransactionCollapsar,
    SendPaymentCollapsar:SendPaymentCollapsar,
    PaymentLastChecked:PaymentLastChecked,
    ExchangeStatusHint:ExchangeStatusHint,
    ExchangeStatusTitle:ExchangeStatusTitle,
    FinishedCollapsar:FinishedCollapsar,
    Duration:Duration,
   },{ExchangeBroker:{
    paymentCompleted:paymentCompleted,
    paymentExpiredOrOverdue:paymentExpiredOrOverdue,
    id:id,Id:Id,notId:notId,status:status,
    paymentStatus:paymentStatus,
    createdDate:createdDate,
    finishedDate:finishedDate,
    exchangeFinished:exchangeFinished,
    statusLastChecked:statusLastChecked,
    creatingTransaction:creatingTransaction,
    checkingPaymentStatus:checkingPaymentStatus,
    checkingExchangeStatus:checkingExchangeStatus,
   }}){
    // todo: $revealIn -> $collapseIn
    return(<>
     <Duration startDate={createdDate} endDate={finishedDate} />
     <ProgressColumn
      loadingStep1={creatingTransaction}
      loadingStep2={checkingPaymentStatus}
      loadingStep3={checkingExchangeStatus}
      transactionId={id}
      paymentFailed={paymentFailed}
      paymentCompleted={paymentCompleted}
      paymentStatus={paymentStatus}
      status={status}
      finishedOnPayment={paymentExpiredOrOverdue}
     />
     <ProgressColumnMobTop
      loadingStep1={creatingTransaction}
      loadingStep2={checkingPaymentStatus}
      loadingStep3={checkingExchangeStatus}
      transactionId={id}
      paymentFailed={paymentFailed}
      paymentCompleted={paymentCompleted}
      paymentStatus={paymentStatus}
      status={status}
      finishedOnPayment={paymentExpiredOrOverdue}
     />
     <ProgressColumnMobBot
      loadingStep1={creatingTransaction}
      loadingStep2={checkingPaymentStatus}
      loadingStep3={checkingExchangeStatus}
      transactionId={id}
      paymentFailed={paymentFailed}
      paymentCompleted={paymentCompleted}
      paymentStatus={paymentStatus}
      status={status}
      finishedOnPayment={paymentExpiredOrOverdue}
     />
     <CreateTransactionCollapsar collapsed={Id} />
     <SendPaymentCollapsar       collapsed={notId} />
     <FinishedCollapsar          expanded={exchangeFinished} />

     <ExchangeStatusTitle        status={status} />
     <ExchangeStatusHint         status={status} />

     <PaymentLastChecked refDate={statusLastChecked} />
    </>)
   },
   buildTransactionInfo({currencyFrom:currencyFrom}) {
    return (<div $id="OfferExchange">
     <label $id="TransactionCurrencyFromLa" $reveal={currencyFrom}>{currencyFrom}</label>
    </div>)
   },
   buildExchangeIntent({
    fixed:fixed,ready:ready,currencyTo:currencyTo,currencyFrom:currencyFrom,
   }){
    return (<div $id="OfferExchange">
     <span $id="RequiredLa" $reveal={fixed} $show={ready} />
     <span $id="OptionalLa" $reveal={!fixed} $show={ready} />
     <label $id="CurrencyFromLa" $reveal={ready}>{currencyFrom}</label>
     <label $id="CurrencyToLa" $reveal={ready}>{currencyTo}</label>
    </div>)
   },
   buildExchangeBroker({ // exchange broker is inside the offer-exchange
    payinAddress,address,
    creatingTransaction:creatingTransaction,
    createTransactionError:createTransactionError,
    checkPaymentError:checkPaymentError,
    checkingStatus:checkingStatus,
    status:status,
    Id:Id,notId,
    kycRequired:kycRequired,
    // paymentCompleted:paymentCompleted,
    paymentReceived:paymentReceived,
    paymentExpiredOrOverdue:paymentExpiredOrOverdue,
    exchangeComplete:exchangeComplete,
    refundAddress:refundAddress, // todo: deduce inputs to the peer component?
    confirmedAmountFrom:confirmedAmountFrom,
    waitingForPayment:waitingForPayment,
    processingPayment:processingPayment,
    // exchangeFinished:exchangeFinished,
   },{
    pulseCreateTransaction:pulseCreateTransaction,
    setRefundAddress:setRefundAddress,setAddress:setAddress,
    pulseCheckPayment:pulseCheckPayment,
   }){
    return <div $id="OfferExchange">
     <button $id="CreateTransactionBu" onClick={pulseCreateTransaction} Loading={creatingTransaction} />
     <span $id="CreateTransactionErrorWr" $reveal={createTransactionError}>
      <span $id="CreateTransactionError">{createTransactionError}</span>
     </span>

     <div $id="KycRequired" $reveal={kycRequired===true||status=='hold'} />
     <div $id="KycNotRequired" $reveal={kycRequired===false} />

     <input $id="AddressIn" value={address} onInput={setAddress} />
     <input $id="RefundAddressIn" value={refundAddress} onInput={setRefundAddress} />

     <div $id="PaymentAddressInWr" $conceal={paymentExpiredOrOverdue}>
      <input $id="PaymentAddressIn" value={payinAddress} disabled={!waitingForPayment} />
     </div>

     <button $id="CheckStatusBu" $reveal={waitingForPayment||processingPayment} $conceal={paymentExpiredOrOverdue}
      onClick={pulseCheckPayment}
      disabled={checkingStatus}
      Loading={checkingStatus&&status!='new'}
     />
     <span $id="SendPaymentErrorWr" $reveal={checkPaymentError}>
      <span $id="SendPaymentError">{checkPaymentError}</span>
     </span>

     <input $id="PaymentAddressHiddenIn" $reveal={paymentExpiredOrOverdue} value={'*'.repeat(payinAddress.length)} />

     <span $id="CheckedWr" $conceal={checkingStatus||exchangeComplete} />
     <span $id="ExchangeStatusHintWr" $conceal={status=='new'} HintRed={['failed','refunded','overdue','hold','expired'].includes(status)} />

     <span $id="CheckingLoIn" $reveal={checkingStatus} />
     <span $id="ConfirmedAmountFrom">{confirmedAmountFrom}</span>

     <span $id="StatusLa">{status}</span>

     <button $id="RestartBu" $reveal={paymentExpiredOrOverdue} />

     <button $id="SendFo" PaymentReceived={!waitingForPayment} />

     <button $id="PaymentSentLa" $reveal={waitingForPayment} />
     <button $id="UpdateStatusLa" $reveal={processingPayment} />

     <div $id="SendFo" $conceal={paymentReceived} />
     {/* <div $id="ReviewForm" $reveal={exchangeFinished} /> */}

     <div $id="Step2CoMobTop" $reveal={Id} />
     <div $id="Step3CoMobTop" $reveal={exchangeComplete} />

     <div $id="Step1CoMobBot" $conceal={Id} />

     <div $id="Step1ProgressCircleWrMobBot" $conceal={notId} />
     <div $id="Step1LaMobBot" $conceal={notId} />

     <div $id="Step2ProgressCircleWrMobBot" $conceal={Id} />
     <div $id="Step2LaMobBot" $conceal={Id} />
     <div $id="Step2CoMobBot" $conceal={exchangeComplete} />
     <div $id="Step3CoMobBot" $conceal={exchangeComplete} />
    </div>
   },
   // select({}) { // todo: selectors wiring.
   //  return <>
   //  </>
   // },
   // todo: select method that accepts selectors
   // query:exchange-intent={`[${xExchangeIntent}]`}
   // query:exchange-intent={`[${xExchangeIntent}]`}
   server({kycEmail:kycEmail},{exchangeApiContent:exchangeApiContent,exchangeIntentSel,dealBrokerSel}) {
    // const{xExchangeBroker}=this
    // exchangeApiContent=exchangeApiContent.replace(/(<\S+\s)/,`$1query:exchange-broker="[${xExchangeBroker}]"
    //  query:deal-broker="[${xDealBroker}]"
    //  query:exchange-intent="[${exchangeIntentSel}]" $fil `.replace(/\n +/g,' '))
    return (<div $id="OfferExchange">
     {/* <div $id="ExchangeApiWr" dangerouslySetInnerHTML={{__html:exchangeApiContent}} /> */}
     <div $id="OfferExchangeInfoRow" query:deal-broker={dealBrokerSel} query:exchange-intent={exchangeIntentSel}  />
     <div $id="OfferExchangeInfoRowMob" query:deal-broker={dealBrokerSel} query:exchange-intent={exchangeIntentSel} />
     <a $id="ExchangeEmail" href={`mailto:${kycEmail}`}>{kycEmail}</a>
    </div>)
   },
   render({
    // currencyTo:currencyTo,currencyFrom:currencyFrom,
    // address:address,refundAddress:refundAddress,
    // payinAddress:payinAddress,
    // id:id,
    btcInput:btcInput,btcOutput:btcOutput,
    payinAddressCopied:payinAddressCopied,
    payinAddressJustCopied:payinAddressJustCopied,
    canCreateTransaction:canCreateTransaction,
    // creatingTransaction:creatingTransaction, // todo: mesh for render
    // address:address, // todo: mesh for render
    tocAccepted:tocAccepted,
   },{
    atInputFocus:atInputFocus,atInputBlur:atInputBlur,
    setTocAccepted,unsetTocAccepted,atAddressPopupClick,
    pulseCopyPayinAddress,reset:reset,BtcWalletPicker,
   }) {
    let ev={target:{}}

    return (<div $id="OfferExchange">
     {/* todo: easy two way binding? */}

     <div $id="BtcWalletPickerCont">
      <BtcWalletPicker $append={!btcInput&&!btcOutput} />
     </div>
     <div $id="WalletOut">
      <BtcWalletPicker $append={btcInput} />
     </div>
     <div $id="WalletIn">
      <BtcWalletPicker $append={btcOutput} />
     </div>

     <input $id="AddressIn" onFocus={atInputFocus(ev)} onBlur={atInputBlur(ev)} />
     <input $id="RefundAddressIn" onFocus={atInputFocus(ev)} onBlur={atInputBlur(ev)} />

     {/* todo: allow to pass value to boolean setters */}
     {/* todo: easy checkers  */}
     <input $id="TocIn" checked={tocAccepted} onChange={ev.target.checked?setTocAccepted():unsetTocAccepted()} />
     {/* todo: enabled attribute */}
     <button $id="CreateTransactionBu" disabled={!canCreateTransaction} />
     <button $id="CopyPayinAddressBu" onClick={pulseCopyPayinAddress} />

     <input $id="PaymentAddressIn" onClick={ev.target.select} />
     <span $id="PayinAddressCopiedLa" Copied={payinAddressCopied} CopiedRemoving={payinAddressJustCopied} />

     <button $id="RestartBu" onClick={reset} />

     {/* CircleWaiting={canCreateTransaction} */}
     <div $id="AddressPopup" onClick={atAddressPopupClick(ev)} />
    </div>)
   },
  }),
  /** @type {xyz.swapee.wc.IOfferExchangeDesigner} */({
   borrowClasses({}){
    return <>
     <xyz.swapee.wc.ProgressColumnClasses />
    </>
   },
   relay({
    This:This,
    AddressPopup:AddressPopup,
    RefundPopup:RefundPopup,
    SwapeeWalletPicker:SwapeeWalletPicker,
    ExchangeBroker:ExchangeBroker,
    TransactionInfo:TransactionInfo,
   },{ExchangeBroker:{refundAddress,address}}){
    return <>
     <This
      onSwapAddresses={[
       ExchangeBroker.setAddress(refundAddress),
       ExchangeBroker.setRefundAddress(address),
      ]}
      onSelectAddress={[AddressPopup.hide(),ExchangeBroker.setAddress]}
      onSelectRefund={RefundPopup.hide()}
      onReset={[
       ExchangeBroker.reset(),
       TransactionInfo.resetPort(),
      ]}
     />
     <ExchangeBroker onIdHigh={ExchangeBroker.pulseCheckPayment} />
     <SwapeeWalletPicker onInvoiceHigh={[
      RefundPopup.hide(),
     ]} />
    </>
   },
  }),
 )
) { }


{/* <span $id="LoadingStripe" $reveal={['new','waiting','confirming','exchanging','sending'].includes(status)} /> */}
