import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.offer-exchange','OfferExchange',true)
// const Placeholder=makePlaceholder('xyz.swapee.offer-exchange','OfferExchange',__dirname)

export default Placeholder
