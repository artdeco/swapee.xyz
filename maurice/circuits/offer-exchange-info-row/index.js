import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.offer-exchange-info-row','OfferExchangeInfoRow',true)
// const Placeholder=makePlaceholder('xyz.swapee.offer-exchange-info-row','OfferExchangeInfoRow',__dirname)

export default Placeholder
