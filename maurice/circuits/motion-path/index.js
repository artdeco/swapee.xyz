import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.motion-path','MotionPath',true)
// const Placeholder=makePlaceholder('xyz.swapee.motion-path','MotionPath',__dirname)

export default Placeholder
