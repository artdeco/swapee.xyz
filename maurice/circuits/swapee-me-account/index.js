import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.swapee-me-account','SwapeeMeAccount',true)
// const Placeholder=makePlaceholder('xyz.swapee.swapee-me-account','SwapeeMeAccount',__dirname)

export default Placeholder
