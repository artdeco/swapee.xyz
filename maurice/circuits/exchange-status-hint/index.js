import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.exchange-status-hint','ExchangeStatusHint',true)
// const Placeholder=makePlaceholder('xyz.swapee.exchange-status-hint','ExchangeStatusHint',__dirname)

export default Placeholder
