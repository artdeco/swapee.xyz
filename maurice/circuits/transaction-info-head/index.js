import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.transaction-info-head','TransactionInfoHead',true)
// const Placeholder=makePlaceholder('xyz.swapee.transaction-info-head','TransactionInfoHead',__dirname)

export default Placeholder
