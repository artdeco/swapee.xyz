import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.offers-table','OffersTable',true)
const Placeholder=makePlaceholder('xyz.swapee.offers-table','OffersTable',__dirname)

export default Placeholder
