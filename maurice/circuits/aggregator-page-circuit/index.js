import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.aggregator-page-circuit','AggregatorPageCircuit',true)
// const Placeholder=makePlaceholder('xyz.swapee.aggregator-page-circuit','AggregatorPageCircuit',__dirname)

export default Placeholder
