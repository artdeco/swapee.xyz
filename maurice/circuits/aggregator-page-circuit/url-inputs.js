/** @type {xyz.swapee.wc.IAggregatorPageCircuitPort.Inputs} */
export const urlInputs={
 amountFrom:'amount',
 cryptoIn:'from',
 cryptoOut:'to',
 type:'type',
}