import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.wc.type-writer','TypeWriter',true)
const Placeholder=makePlaceholder('xyz.swapee.wc.type-writer','TypeWriter',__dirname)

export default Placeholder
