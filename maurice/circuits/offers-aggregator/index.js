import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.offers-aggregator','OffersAggregator',true)
const Placeholder=makePlaceholder('xyz.swapee.offers-aggregator','OffersAggregator',__dirname)

export default Placeholder
