import {ChangellyUniversal} from '@type.community/changelly.com'
import {xyz} from 'types'
import {ChangeNowUniversal,LetsExchangeUniversal} from '@swapee.xyz/swapee.xyz'

/**
 * Loads offers from various known exchanges.
 */
export default class OffersAggregator extends (
 /** @type {typeof xyz.swapee.wc.AbstractOffersAggregator} */ (<xyz.swapee.wc.IOffersAggregator>
  <core>
   <number name="changellyFixedOffer" nullable readonly setter>
     Loaded offer for _Changelly_ fixed rate. Reset when starts loading.
   </number>
   <string name="changellyFixedError" readonly setter>
     An error by _Changelly_.
   </string>
   <number name="changellyFixedMin" readonly setter>
     The minimum amount for the fixed transaction.
   </number>
   <number name="changellyFixedMax" readonly setter>
     The maximum amount for the fixed transaction.
   </number>
   <number name="changellyFloatingOffer" nullable readonly setter>
     Loaded offer for _Changelly_ floating rate. Reset when starts loading.
   </number>
   <string name="changellyFloatingError" readonly setter>
     An error by _Changelly_.
   </string>
   <number name="changellyFloatMin" readonly setter nullable>
     The minimum amount for the float transaction.
   </number>
   <number name="changellyFloatMax" readonly setter nullable>
     The maximum amount for the float transaction.
   </number>

   <number name="letsExchangeFixedOffer" nullable readonly setter>
     Loaded offer for _LetsExchange_ fixed rate. Reset when starts loading.
   </number>
   <string name="letsExchangeFixedError" readonly setter>
     An error by _LetsExchange_.
   </string>
   <number name="letsExchangeFloatingOffer" nullable readonly setter>
     Loaded offer for _LetsExchange_ floating rate. Reset when starts loading.
   </number>
   <string name="letsExchangeFloatingError" readonly setter>
     An error by _LetsExchange_.
   </string>

   <number name="changeNowFixedOffer" nullable readonly setter>
     Loaded offer for _LetsExchange_ fixed rate. Reset when starts loading.
   </number>
   <string name="changeNowFixedError" readonly setter>
     An error by _LetsExchange_.
   </string>
   <number name="changeNowFloatingOffer" nullable readonly setter>
     Loaded offer for _LetsExchange_ floating rate. Reset when starts loading.
   </number>
   <string name="changeNowFloatingError" readonly setter>
     An error by _LetsExchange_.
   </string>

   <number name="exchangesTotal" default={6} readonly />
  </core>

  <cache>
   <bool name="allExchangesLoaded" readonly />
   <number name="exchangesLoaded" readonly />
   <string name="host" />
   <number name="bestOffer" nullable>
    The best offer.
   </number>
   <number name="worstOffer" nullable>
    The worst offer.
   </number>
   <number name="minAmount" nullable>
    The min of all min errors.
   </number>
   <number name="minError" nullable>
    If none of the exchanges could provide an estimate, and some reported the
    min amount.
   </number>
   <number name="maxAmount" nullable>
    The max of all max errors.
   </number>
   <number name="maxError" nullable>
    If none of the exchanges could provide an estimate, and some reported the
    max amount.
   </number>
   <number name="estimatedOut" nullable>
    The estimated amount out.
   </number>
   <bool name="isAggregating" />
   <bool name="loadingEstimate">
    If no rates are available yet, and the loading of any is in progress.
   </bool>
  </cache>

  <vdus>
   <input name="AmountInIn">
     The input for the amount in.
   </input>
   <input name="AmountOutIn">
    The input for the amount out, either estimated or entered by hand.
   </input>
  </vdus>

  <service>
   <ChangellyUniversal />
   <LetsExchangeUniversal />
   <ChangeNowUniversal />
  </service>

  <land>
   <xyz.swapee.wc.IExchangeIntent outside />
  </land>

  <classes>
   {/* <class name="Class">The class.</class> */}
  </classes>

  <computer>
   {/* todo: automatically generate code for min-max adapters */}
   <adapter name="adaptMinAmount" min>
    <xyz.swapee.wc.IOffersAggregatorCore changellyFloatMin  />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore minAmount />
    </outputs>
   </adapter>
   <adapter name="adaptMinError">
    <xyz.swapee.wc.IOffersAggregatorCore minAmount estimatedOut />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore minError />
    </outputs>
   </adapter>
   <adapter name="adaptMaxAmount" max>
    <xyz.swapee.wc.IOffersAggregatorCore changellyFloatMax />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore maxAmount />
    </outputs>
   </adapter>
   <adapter name="adaptMaxError">
    <xyz.swapee.wc.IOffersAggregatorCore maxAmount estimatedOut />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore maxError />
    </outputs>
   </adapter>

   <adapter name="adaptEstimatedOut">
    <xyz.swapee.wc.IOffersAggregatorCore bestOffer />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore estimatedOut />
    </outputs>
   </adapter>
   <adapter name="adaptLoadingEstimate">
    <xyz.swapee.wc.IOffersAggregatorCore isAggregating estimatedOut  />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore loadingEstimate />
    </outputs>
   </adapter>

   <adapter name="adaptBestAndWorstOffers">
    <xyz.swapee.wc.IOffersAggregatorCore
     changellyFixedOffer changellyFloatingOffer
     letsExchangeFixedOffer letsExchangeFloatingOffer
     changeNowFixedOffer changeNowFloatingOffer
    />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore bestOffer worstOffer />
    </outputs>
   </adapter>

   <adapter name="adaptExchangesLoaded">
    <xyz.swapee.wc.IOffersAggregatorCore exchangesTotal="required"
     loadingChangellyFixedOffer loadingChangellyFloatingOffer
     loadingLetsExchangeFixedOffer loadingLetsExchangeFloatingOffer
     loadingChangeNowFixedOffer loadingChangeNowFloatingOffer
    />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore exchangesLoaded allExchangesLoaded />
    </outputs>
   </adapter>

   <adapter name="adaptIsAggregating">
    <xyz.swapee.wc.IOffersAggregatorCore
     loadingChangellyFloatingOffer loadingChangellyFixedOffer
     loadingLetsExchangeFloatingOffer loadingLetsExchangeFixedOffer
     loadingChangeNowFixedOffer loadingChangeNowFloatingOffer
    />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore isAggregating />
    </outputs>
   </adapter>
  </computer>

  <database>
   <filter name="changellyFloatingOffer">
    <xyz.swapee.wc.IExchangeIntentCore amountFrom="required" currencyFrom="required" currencyTo="required" />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore changellyFloatingOffer changellyFloatingError changellyFloatMin changellyFloatMax />
    </outputs>
    Loads the _Changelly_ floating offer.
   </filter>

   <filter name="changellyFixedOffer">
    <xyz.swapee.wc.IExchangeIntentCore amountFrom="required" currencyFrom="required" currencyTo="required" />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore changellyFixedOffer changellyFixedError changellyFixedMin changellyFixedMax />
    </outputs>
    Loads the _Changelly_ fixed offer.
   </filter>

   {/* LetsExchange */}
   <filter name="letsExchangeFloatingOffer">
    <xyz.swapee.wc.IExchangeIntentCore amountFrom="required" currencyFrom="required" currencyTo="required" />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore letsExchangeFloatingOffer letsExchangeFloatingError />
    </outputs>
    Loads the _Changelly_ floating offer.
   </filter>
   <filter name="letsExchangeFixedOffer">
    <xyz.swapee.wc.IExchangeIntentCore amountFrom="required" currencyFrom="required" currencyTo="required" />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore letsExchangeFixedOffer letsExchangeFixedError />
    </outputs>
    Loads the _Changelly_ fixed offer.
   </filter>

   {/* ChangeNow */}
   <filter name="changeNowFloatingOffer">
    <xyz.swapee.wc.IExchangeIntentCore amountFrom="required" currencyFrom="required" currencyTo="required" />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore changeNowFloatingOffer changeNowFloatingError />
    </outputs>
    Loads the _Changelly_ floating offer.
   </filter>
   <filter name="changeNowFixedOffer">
    <xyz.swapee.wc.IExchangeIntentCore amountFrom="required" currencyFrom="required" currencyTo="required" />
    <outputs>
     <xyz.swapee.wc.IOffersAggregatorCore changeNowFixedOffer changeNowFixedError />
    </outputs>
    Loads the _Changelly_ fixed offer.
   </filter>
  </database>

  <controller>
  </controller>

  <element v3 html>
   <attrs>
    {/* HTML attributes */}
   </attrs>
  </element>

  <records>
  </records>
 </xyz.swapee.wc.IOffersAggregator>).implements(
  /** @type {!xyz.swapee.wc.IOffersAggregatorScreen} */({
  }),
  /** @type {!xyz.swapee.wc.IOffersAggregatorHtmlComponent} */({
  }),
  /** @type {!xyz.swapee.wc.IOffersAggregatorProcessor} */({
  }),
  /** @type {!xyz.swapee.wc.IOffersAggregatorComputer} */({
   adaptLoadingEstimate({isAggregating,estimatedOut:estimatedOut}) {
    if(!isAggregating||estimatedOut) return{loadingEstimate:false}
    return{loadingEstimate:true}
   },
   adaptMinError({estimatedOut:estimatedOut,minAmount:minAmount}) {
    if(!minAmount||estimatedOut) return{minError:null}
    return{minError:minAmount}
   },
   adaptMaxError({estimatedOut:estimatedOut,maxAmount}) {
    if(!maxAmount||estimatedOut) return{maxError:null}
    return{maxError:maxAmount}
   },
   adaptEstimatedOut({bestOffer:bestOffer}){
    return{estimatedOut:bestOffer}
   },
   adaptBestAndWorstOffers({
    changellyFixedOffer:changellyFixedOffer,
    changellyFloatingOffer:changellyFloatingOffer,
    changeNowFixedOffer,changeNowFloatingOffer,
    letsExchangeFixedOffer,letsExchangeFloatingOffer,
   }) {
    const offers=[
     changellyFloatingOffer,changellyFixedOffer,
     changeNowFixedOffer,changeNowFloatingOffer,
     letsExchangeFixedOffer,letsExchangeFloatingOffer,
    ].filter(Boolean)
    offers.sort().reverse()
    const bestOffer=offers.length?offers[0]:null
    const worstOffer=offers.length?offers[offers.length-1]:null
    return{
     bestOffer:bestOffer,
     worstOffer:worstOffer,
    }
   },
   adaptIsAggregating:({
    loadingChangellyFloatingOffer:loadingChangellyFloatingOffer,
    loadingChangellyFixedOffer:loadingChangellyFixedOffer,
    loadingChangeNowFixedOffer:loadingChangeNowFixedOffer,
    loadingChangeNowFloatingOffer:loadingChangeNowFloatingOffer,
    loadingLetsExchangeFixedOffer:loadingLetsExchangeFixedOffer,
    loadingLetsExchangeFloatingOffer:loadingLetsExchangeFloatingOffer,
   })=>{
    return{ // todo: implement this in the radio?
     isAggregating:
      loadingChangellyFloatingOffer||loadingChangellyFixedOffer||
      loadingChangeNowFixedOffer||loadingChangeNowFloatingOffer||
      loadingLetsExchangeFixedOffer||loadingLetsExchangeFloatingOffer,
    }
   },
   adaptExchangesLoaded({
    loadingChangellyFloatingOffer,
    loadingLetsExchangeFixedOffer,
    loadingLetsExchangeFloatingOffer,
    loadingChangeNowFixedOffer,
    loadingChangeNowFloatingOffer,
    loadingChangellyFixedOffer,
    exchangesTotal,
   }){
    let loading=0

    if(loadingChangellyFixedOffer) loading++
    if(loadingChangellyFloatingOffer) loading++
    if(loadingLetsExchangeFixedOffer) loading++
    if(loadingLetsExchangeFloatingOffer) loading++
    if(loadingChangeNowFixedOffer) loading++
    if(loadingChangeNowFloatingOffer) loading++

    const allExchangesLoaded=loading==0
    return{
     allExchangesLoaded:allExchangesLoaded,
     exchangesLoaded:exchangesTotal-loading,
    }
   },
  }),
  /** @type {!xyz.swapee.wc.IOffersAggregator} */({
  }),
  /** @type {!xyz.swapee.wc.IOffersAggregatorController} */({
   //
  }),
  /** @type {xyz.swapee.wc.IOffersAggregatorElement} */({
   classesMap:       true,
   rootSelector:     `.OffersAggregator`,
   stylesheet:       'html/styles/OffersAggregator.css',
   blockName:        'html/OffersAggregatorBlock.html',
   solder({amountIn:amountIn}) {
    return{
     amountIn:amountIn,
     'host':this['REMOTE_HOST'],//'http://localhost:5000',
    }
   },
   inducer({},{}) {
    return(<offers-aggregator>
    </offers-aggregator>)
   },
   server() {
    return (<div $id="OffersAggregator">
    </div>)
   },
   render({amountIn:amountIn,estimatedOut:estimatedOut},{}) {
    return (<div $id="OffersAggregator">
     <input $id="AmountInIn" value={amountIn}/>
     <input $id="AmountOutIn" value={estimatedOut}/>
    </div>)
   },
  }),
  /** @type {xyz.swapee.wc.IOffersAggregatorService} */({
   async filterChangeNowFixedOffer({
    amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
   }){
    const{asUChangeNow:{changeNow:changeNow}}=this
    if(!changeNow) return

    const{
     asIChangeNow:{
      GetFixedExchangeAmount:GetFixedExchangeAmount,
     },
    }=changeNow

    try{
     const{toAmount:toAmount}=await GetFixedExchangeAmount({
      fromAmount:amountIn,
      fromCurrency:currencyIn,
      toCurrency: currencyOut,
     })
     const amountOut=parseFloat(toAmount)
     return{
      changeNowFixedOffer:amountOut,
     }
    }catch(err){
     return{
      changeNowFixedError:err.message,
     }
    }
   },
   async filterChangeNowFloatingOffer({
    amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
   }){
    const{asUChangeNow:{changeNow:changeNow}}=this
    if(!changeNow) return

    const{
     asIChangeNow:{
      GetExchangeAmount:GetExchangeAmount,
     },
    }=changeNow

    try{
     const{toAmount:toAmount}=await GetExchangeAmount({
      fromAmount:amountIn,
      fromCurrency:currencyIn,
      toCurrency: currencyOut,
     })
     const amountOut=parseFloat(toAmount)
     return{
      changeNowFloatingOffer:amountOut,
     }
    }catch(err){
     return{
      changeNowFloatingError:err.message,
     }
    }
   },
   async filterLetsExchangeFixedOffer({
    amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
   }){
    const{asULetsExchange:{letsExchange:letsExchange}}=this
    if(!letsExchange) return

    const{
     asILetsExchange:{
      GetInfo:GetInfo,
     },
    }=letsExchange

    try{
     const{amount:amount}=await GetInfo({
      amount:amountIn,
      from:currencyIn,
      to:currencyOut,
      affiliateId:'Zp1gafk2sd4goqJq',
      promocode:'swapee_test',
      float:false,
     })
     const amountOut=parseFloat(amount)
     return{
      letsExchangeFixedOffer:amountOut,
     }
    }catch(err){
     return{
      letsExchangeFixedError:err.message,
     }
    }
   },
   async filterLetsExchangeFloatingOffer({
    amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
   }){
    const{asULetsExchange:{letsExchange:letsExchange}}=this
    if(!letsExchange) return

    const{
     asILetsExchange:{
      GetInfo:GetInfo,
     },
    }=letsExchange

    try{
     const{amount:amount}=await GetInfo({
      amount:amountIn,
      from:currencyIn,
      to: currencyOut,
      affiliateId:'Zp1gafk2sd4goqJq',
      promocode:'swapee_test',
      float:true,
     })
     const amountOut=parseFloat(amount)
     return{
      letsExchangeFloatingOffer:amountOut,
     }
    }catch(err){
     return{
      letsExchangeFloatingError:err.message,
     }
    }
   },
   async filterChangellyFloatingOffer({
    amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
   }) {
    // could put changelly on the land somehow?
    const{asUChangelly:{changelly:changelly}}=this
    if(!changelly) return

    const{
     asIChangellyExchange:{
      GetExchangeAmount:GetExchangeAmount,
      GetExtendedExchangeAmount:GetExtendedExchangeAmount,
     },
    }=changelly
    try{
     const res=await GetExtendedExchangeAmount({
      amount: amountIn,
      from:   currencyIn,
      to:     currencyOut,
     })
     if(!res) {
      try {
       const rr=await GetExchangeAmount({
        amount:amountIn,
        from:  currencyIn,
        to:    currencyOut,
       })
       return{
        changellyFloatingOffer:rr,
       }
       // debugger
      }catch(err) {
       if(err['code']==-32600) {
        // Invalid amount: minimal amount for matic->bat is 33.813
        if(/maximal/.test(err.message)) {
         const[max]=/[\d.]+$/.exec(err.message)||[]
         return {
          changellyFloatMax:parseFloat(max),
         }
        }
        const[min]=/[\d.]+$/.exec(err.message)||[]
        return {
         changellyFloatMin:parseFloat(min),
        }
       }else if(err['code']) {
        throw new Error(`!${err.message}`)
       }
       throw err
      }
     }
     let{
      result:result,networkFee:networkFee,
     }=res
     networkFee=Number(networkFee)
     const r=Number(result)-networkFee

     return{
      changellyFloatingOffer:r,
     }
    }catch(err){
     return{
      changellyFloatingError:err.message,
     }
    }
   },
   async filterChangellyFixedOffer({
    amountFrom:amountIn,currencyFrom:currencyIn,currencyTo:currencyOut,
   }) {
    // could put changelly on the land somehow?
    const{asUChangelly:{changelly:changelly}}=this
    if(!changelly) return

    const{
     asIChangellyFixedRate:{
      GetFixRateForAmount:GetFixRateForAmount,
     },
    }=changelly
    try{
     const res=await GetFixRateForAmount({
      from:     currencyIn,
      to:      currencyOut,
      amountFrom: amountIn,
     })
     const amountOut=parseFloat(res)
     return{
      changellyFixedOffer:amountOut,
     }
    }catch(err){
     if(err['code']==-32600) {
      // Invalid amount: minimal amount for matic->bat is 33.813
      if(/maximal/.test(err.message)) {
       const[max]=/[\d.]+$/.exec(err.message)||[]
       return{
        changellyFixedMax:parseFloat(max),
       }
      }
      const[min]=/[\d.]+$/.exec(err.message)||[]
      return {
       changellyFixedMin:parseFloat(min),
      }
     }
     // throw err
     return {
      changellyFixedError:err.message,
     }
    }
   },
  }),
  /**@type {xyz.swapee.wc.IOffersAggregatorDesigner}*/({
   relay({This:This,OffersAggregator:_OffersAggregator,ExchangeIntent:ExchangeIntent}){
    return (<>
     <ExchangeIntent onIntentChange={[
      _OffersAggregator.resetPort(),
     ]} />
     <This
      onLoadingChangellyFixedOfferHigh={[
       _OffersAggregator.unsetChangellyFixedError(),
       // _OffersAggregator.unsetChangellyFixedOffer(),
      ]}
      onLoadingChangellyFloatingOfferHigh={[
       _OffersAggregator.unsetChangellyFloatingError(),
       // _OffersAggregator.unsetChangellyFloatingOffer(),
      ]}

      onLoadingLetsExchangeFixedOfferHigh={[
       _OffersAggregator.unsetLetsExchangeFixedError(),
       // _OffersAggregator.unsetLetsExchangeFixedOffer(),
      ]}
      onLoadingLetsExchangeFloatingOfferHigh={[
       _OffersAggregator.unsetLetsExchangeFloatingError(),
       // _OffersAggregator.unsetLetsExchangeFloatingOffer(),
      ]}

      onLoadingChangeNowFixedOfferHigh={[
       _OffersAggregator.unsetChangeNowFixedError(),
       // _OffersAggregator.unsetChangeNowFixedOffer(),
      ]}
      onLoadingChangeNowFloatingOfferHigh={[
       _OffersAggregator.unsetChangeNowFloatingError(),
       // _OffersAggregator.unsetChangeNowFloatingOffer(),
      ]}
     />
    </>)
   },
  }),
 )
) { }


{/* <This onLoadingChangellyFixedOffer={
      // OffersAggregator.
     } /> */}