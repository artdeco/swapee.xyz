import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.swapee-button','SwapeeButton',true)
// const Placeholder=makePlaceholder('xyz.swapee.swapee-button','SwapeeButton',__dirname)

export default Placeholder
