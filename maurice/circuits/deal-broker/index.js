import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.deal-broker','DealBroker',true)
// const Placeholder=makePlaceholder('xyz.swapee.deal-broker','DealBroker',__dirname)

export default Placeholder
