import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.border-motion-path','BorderMotionPath',true)
// const Placeholder=makePlaceholder('xyz.swapee.border-motion-path','BorderMotionPath',__dirname)

export default Placeholder
