import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.wc.irq-test','IrqTest',true)
const Placeholder=makePlaceholder('xyz.swapee.wc.irq-test','IrqTest',__dirname)

export default Placeholder
