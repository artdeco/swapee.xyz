/**@type {xyz.swapee.wc.ISwapeeWalletPickerGenerator.WalletDynamo}*/
export const WalletDynamo=({address:address})=>{
 return(<div $id="Wallet" data-address={address}>
  <span $id="WalletAddress">{address}</span>
 </div>)
}