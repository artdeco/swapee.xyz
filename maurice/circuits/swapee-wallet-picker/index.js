import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.swapee-wallet-picker','SwapeeWalletPicker',true)
// const Placeholder=makePlaceholder('xyz.swapee.swapee-wallet-picker','SwapeeWalletPicker',__dirname)

export default Placeholder
