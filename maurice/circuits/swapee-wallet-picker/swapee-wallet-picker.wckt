import {StatefulLoadable, StatefulLoader} from '@mauriceguest/guest2'
import {xyz} from 'types'
import {WalletDynamo} from './dyns/wallet-dynamo'

/**
 * Allows to pick a wallet address from the account on Swapee.me.
 */
export default class SwapeeWalletPicker extends (
 /** @type {typeof xyz.swapee.wc.AbstractSwapeeWalletPicker} */ (<xyz.swapee.wc.ISwapeeWalletPicker>
  <core>
   <bool name="isSignedIn">Whether the person is signed in.</bool>
   <list of="string" name="wallets">
    The available wallets.
   </list>
   <string name="invoice">The _BitCoin_ address.</string>
   <pulse name="loadCreateInvoice">Starts creating an invoice.</pulse>
  </core>

  <cores>
   <guest.maurice.IStatefulLoadable.State $={StatefulLoadable} />
  </cores>

  <cache>
   {/* <bool name="prop">
    A private model property.
   </bool> */}
  </cache>

  <land>
   <xyz.swapee.wc.ISwapeeMe outside />
  </land>

  <screen>
   <templates>
    <div $id="Wallet" />
   </templates>
  </screen>

  <generator hyper>
   <dynamo name="Wallet" rotor="wallets" reuse>
    <string name="address" />
    A single wallet available to the user.
   </dynamo>
  </generator>

  <vdus>
   <div name="InvoiceLoIn" />

   <div name="Wallets">
    The container for wallets.
   </div>
   <div name="SwapeeAccountWr">
    The wrapper for login/sign up buttons.
   </div>
   <button name="GetWalletBu">
    The button to sign up with swapee.me and get a wallet.
   </button>
   <button name="CreateInvoiceBu">
    Generates an invoice to get the _BitCoin_ address.
   </button>
   <button name="SwapeeLoginBu">
    The button to login with swapee.me to list wallets.
   </button>
  </vdus>

  <classes>
   <class name="Class">The class.</class>
  </classes>

  <computer>
   <adapter name="adaptWalletsRotor">
    <xyz.swapee.wc.ISwapeeWalletPickerCore wallets />
    <outputs>
     <xyz.swapee.wc.ISwapeeWalletPickerCore walletsRotor />
    </outputs>
   </adapter>

   <adapter name="adaptLoadBitcoinAddress" async>
    <xyz.swapee.wc.ISwapeeWalletPickerCore loadCreateInvoice="required" />
    <xyz.swapee.wc.ISwapeeMeCore swapeeHost="required" token="required" />
    <outputs>
     <xyz.swapee.wc.ISwapeeWalletPickerCore invoice  />
    </outputs>
   </adapter>
  </computer>

  <controller>
  </controller>

  {/* <irq>
   <method name="submitBuCl" />
  </irq> */}

  <element v3 html>
   <attrs>
    {/* HTML attributes */}
   </attrs>
  </element>

  <records>
  </records>
 </xyz.swapee.wc.ISwapeeWalletPicker>).implements(
  /** @type {!xyz.swapee.wc.ISwapeeWalletPickerScreen} */({
   deduceInputs(el) {},
  }),
  /**@type {!xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent}*/({
  }),
  StatefulLoader,
  /**@type {!xyz.swapee.wc.ISwapeeWalletPickerComputer}*/({
   __$constructor() {
    Object.defineProperty(this,'processJSON',{
     value(a) {
      return a
     },
    })
   },
   async adaptLoadBitcoinAddress({token:token,swapeeHost:swapeeHost}) {
    const {
     asIStatefulLoader: {
      fetchJSON:fetchJSON, // this can be applied to the best rates too via onramper
     },
    }=this
    const data=await fetchJSON(`${swapeeHost}/invoice`,{
     method:'POST',
     headers:{
      'Authorization':`Bearer ${token}`,
      "Content-Type": "application/json",
     },
     body:JSON.stringify({
      'invoice':{
       amount:0,
       type:'bitcoin',
      },
     }),
    })
    // debugger
    if(!data) return {}
    const{'hash':hash}=data

    return{invoice:hash}
   },
   adaptWalletsRotor({wallets:wallets}) {
    return{
     walletsRotor:wallets.map((address)=>({address:address})),
    }
   },
  }),
  /**@type {!xyz.swapee.wc.ISwapeeWalletPicker}*/({
  }),
  /**@type {!xyz.swapee.wc.ISwapeeWalletPickerController}*/({
  }),
  /**@type {xyz.swapee.wc.ISwapeeWalletPickerElement}*/({
   classesMap:       true,
   rootSelector:     `.SwapeeWalletPicker`,
   stylesheet:       'html/styles/SwapeeWalletPicker.css',
   blockName:        'html/SwapeeWalletPickerBlock.html',
   solder({}) {
    return{
     // wallets:['bc1qtmtr2z4rulvzjenhg4snvxwcydqsn00tdujtn7'].join(','),
     // 'host':this.REMOTE_HOST,
    }
   },
   inducer({},{}) {
    return(<swapee-wallet-picker>
    </swapee-wallet-picker>)
   },
   server() {
    return (<div $id="SwapeeWalletPicker">
    </div>)
   },
   buildSwapeeMe({token},{signIn}) {
    return (<div $id="SwapeeWalletPicker">
     <button $id="SwapeeLoginBu" onClick={signIn} />
     <button $id="SwapeeAccountWr" $conceal={token} />
     <button $id="CreateInvoiceBu" $reveal={token} />
    </div>)
   },
   render({walletsRotor:walletsRotor,loading:loading},{
    makeWalletElement:makeWalletElement,pulseLoadCreateInvoice,
   }) {
    return (<div $id="SwapeeWalletPicker">
     <WalletDynamo $id="Wallets" $rotor={walletsRotor} $make={makeWalletElement} />
     <button $id="CreateInvoiceBu" disabled={loading} onClick={pulseLoadCreateInvoice} />
     <span $id="InvoiceLoIn" $reveal={loading} />
    </div>)
   },
  }),
 )
) { }
