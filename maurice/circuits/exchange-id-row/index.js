import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.exchange-id-row','ExchangeIdRow',true)
// const Placeholder=makePlaceholder('xyz.swapee.exchange-id-row','ExchangeIdRow',__dirname)

export default Placeholder
