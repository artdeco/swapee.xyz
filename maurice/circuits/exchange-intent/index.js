import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.exchange-intent','ExchangeIntent',true)
// const Placeholder=makePlaceholder('xyz.swapee.exchange-intent','ExchangeIntent',__dirname)

export default Placeholder
