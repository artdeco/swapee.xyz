import {makePlaceholder} from "@mauriceguest/guest2"

const Placeholder=makePlaceholder('xyz.swapee.offers-filter','OffersFilter',true)
// const Placeholder=makePlaceholder('xyz.swapee.offers-filter','OffersFilter',__dirname)

export default Placeholder
