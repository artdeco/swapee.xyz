import '@mauriceguest/app'
import {links} from './links'

/** @type {guest.maurice.MauriceConfig} */
const config={
 statics:[
  {root:'generated/web_circuits'},
 ],
 frontendDirs:['types','maurice'],
 layout:'maurice/layout/MainLayout.html',
 output:'maurice/docs',
 cleanUrls:true,
 packages:[
  '@mauriceguest/spectrum-icons',
  '@mauriceguest/feather-icons', // iconFramework
  '@webcircuits/ui',
 ],
 replacements: [
  {
   re: /{{ company }}/g,
   replacement: '[Art Deco™](https://artd.eco)',
  },
 ],
 pretty: false,
 HOST: process.env.HOST || 'https://swapee.xyz',
 pages: '../pages',
 blocks: ['blocks'],
 elements: ['elements','circuits'],
 components: ['components'],
 // which prefixes to keep in the main CSS
 prefixes:['-webkit-hyphens','-ms-hyphens','-webkit-backdrop-filter'],
 // for sitemap and social-buttons
 url:process.env.HOST||'https://swapee.xyz',
 // required when pages are at org.gitlab.io/pages-name
 // local: true,
 mount: '/',
 ajax: false,
 potracePath: '~/.maurice/potrace',
 links:links,
}

export default config