/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
function h(a){var b={};const {top:{outerHeight:c,screenY:d,outerWidth:p,screenX:e}}=window,l=[];l.push("width=900",`left=${p/2+e-450}`);l.push(`top=${c/2+d-325-50}`,"height=650");const F=Object.keys(b).map(t=>`${t}=${b[t]}`);l.push(...F);window.open(a,"",l.join(","))};function k({u:a}){h(a)};function m(a){const {l:{s:b}}=this;b(a,null)}m._id="589f981";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const n=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const q=n["37270038985"],r=n["37270038986"],u=n["372700389810"],v=n["372700389811"];function w(a,b,c,d){return n["372700389812"](a,b,c,d,!1,void 0)};
const x=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const y=x["61893096584"],z=x["61893096586"],A=x["618930965811"],B=x["618930965812"],C=x["618930965815"];function D(){}D.prototype={};function E(){this.j=this.h=this.g=this.i=null}class G{}class H extends w(G,147217332116,E,{l:1,L:2}){}H[v]=[D,y,{constructor(){q(this,()=>{this.scan({})})},scan:function(){const {element:a,m:{vdusPQs:{i:b,g:c,h:d,j:p}}}=this,e=C(a);Object.assign(this,{i:e[b],g:e[c],h:e[d],j:e[p]})}},{[u]:{i:1,g:1,h:1,j:1},initializer({i:a,g:b,h:c,j:d}){void 0!==a&&(this.i=a);void 0!==b&&(this.g=b);void 0!==c&&(this.h=c);void 0!==d&&(this.j=d)}}];var I=class extends H.implements({paint:[m]},{s:k}){};function J(){var a=document.querySelector('meta[name="description"]');a=a?a.content.trim():null;return{...(a?{o:a}:{}),title:document.title,location:window.location.href}};function K(){}K.prototype={};class L{}class M extends w(L,147217332124,null,{v:1,K:2}){}M[v]=[K,A,{}];function N(){}N.prototype={};class O{}class P extends w(O,147217332127,null,{A:1,O:2}){}P[v]=[N,B,{allocator(){this.methods={}}}];const Q={D:"29d24",title:"d5d3d",url:"572d4",J:"b5e33",I:"c27f2",G:"48ae2",F:"5b658",H:"f14d9",u:"2be4b",C:"a53e6",text:"1cb25",o:"e9a23",location:"d5189"};const R={...Q};const S=Object.keys(Q).reduce((a,b)=>{a[Q[b]]=b;return a},{});function T(){}T.prototype={};class U{}class V extends w(U,147217332125,null,{m:1,M:2}){}function W(){}
V[v]=[T,W.prototype={inputsPQs:R,memoryQPs:S},z,P,W.prototype={vdusPQs:{i:"d4101",g:"d4102",j:"d4103",h:"d4104"}},W.prototype={constructor(){r(this,()=>{var a=this.i;a&&a.addEventListener("click",b=>{b.preventDefault();this.uart.t("inv",{mid:"2aa22"});return!1});(a=this.g)&&a.addEventListener("click",b=>{b.preventDefault();this.uart.t("inv",{mid:"fd3b9"});return!1});(a=this.h)&&a.addEventListener("click",b=>{b.preventDefault();this.uart.t("inv",{mid:"cb0ea"});return!1})})}}];var X=class extends V.implements(M,I,{get queries(){return this.settings}},{deduceInputs:J,__$id:1472173321}){};module.exports["147217332141"]=I;module.exports["147217332171"]=X;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1472173321']=module.exports