/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=h["37270038985"],ba=h["372700389810"],k=h["372700389811"];function l(b,c,d,e){return h["372700389812"](b,c,d,e,!1,void 0)};
const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const ca=m["61893096584"],da=m["61893096586"],ea=m["618930965811"],fa=m["618930965812"],ha=m["618930965815"],ia=m["618930965819"];function S(){}S.prototype={};function ja(){this.V=this.C=this.P=this.L=this.W=this.G=this.H=this.R=this.A=this.J=this.Z=this.Y=this.F=this.M=this.K=this.v=this.U=this.T=this.O=this.I=this.X=this.D=this.aa=this.h=this.i=this.$=this.g=this.o=this.m=this.s=this.l=this.j=this.u=null}class ka{}class T extends l(ka,950992890618,ja,{ba:1,ia:2}){}
T[k]=[S,ca,{constructor(){aa(this,()=>{this.scan({})})},scan:function(){const {element:b,ca:{vdusPQs:{$:c,g:d,u:e,j:n,l:p,s:q,m:r,o:t,i:u,h:v,aa:w,D:x,X:y,I:z,O:A,T:B,U:C,v:D,K:E,M:F,F:G,Y:H,Z:I,J,A:K,R:L,H:M,G:N,W:O,L:P,P:Q,C:R,V:pa}}}=this,a=ha(b);Object.assign(this,{$:a[c],g:a[d],u:a[e],j:a[n],l:a[p],s:a[q],m:a[r],o:a[t],i:a[u],h:a[v],aa:a[w],D:a[x],X:a[y],I:a[z],O:a[A],T:a[B],U:a[C],v:a[D],K:a[E],M:a[F],F:a[G],Y:a[H],Z:a[I],J:a[J],A:a[K],R:a[L],H:a[M],G:a[N],W:a[O],L:a[P],
P:a[Q],C:a[R],V:a[pa]})}},{[ba]:{u:1,j:1,l:1,s:1,m:1,o:1,g:1,$:1,i:1,h:1,aa:1,D:1,X:1,I:1,O:1,T:1,U:1,v:1,K:1,M:1,F:1,Y:1,Z:1,J:1,A:1,R:1,H:1,G:1,W:1,L:1,P:1,C:1,V:1},initializer({u:b,j:c,l:d,s:e,m:n,o:p,g:q,$:r,i:t,h:u,aa:v,D:w,X:x,I:y,O:z,T:A,U:B,v:C,K:D,M:E,F,Y:G,Z:H,J:I,A:J,R:K,H:L,G:M,W:N,L:O,P,C:Q,V:R}){void 0!==b&&(this.u=b);void 0!==c&&(this.j=c);void 0!==d&&(this.l=d);void 0!==e&&(this.s=e);void 0!==n&&(this.m=n);void 0!==p&&(this.o=p);void 0!==q&&(this.g=q);void 0!==r&&(this.$=r);void 0!==
t&&(this.i=t);void 0!==u&&(this.h=u);void 0!==v&&(this.aa=v);void 0!==w&&(this.D=w);void 0!==x&&(this.X=x);void 0!==y&&(this.I=y);void 0!==z&&(this.O=z);void 0!==A&&(this.T=A);void 0!==B&&(this.U=B);void 0!==C&&(this.v=C);void 0!==D&&(this.K=D);void 0!==E&&(this.M=E);void 0!==F&&(this.F=F);void 0!==G&&(this.Y=G);void 0!==H&&(this.Z=H);void 0!==I&&(this.J=I);void 0!==J&&(this.A=J);void 0!==K&&(this.R=K);void 0!==L&&(this.H=L);void 0!==M&&(this.G=M);void 0!==N&&(this.W=N);void 0!==O&&(this.L=O);void 0!==
P&&(this.P=P);void 0!==Q&&(this.C=Q);void 0!==R&&(this.V=R)}}];var U=class extends T.implements(){};function la(){};function V(){}V.prototype={};class ma{}class W extends l(ma,950992890625,null,{da:1,ha:2}){}W[k]=[V,ea,{}];function X(){}X.prototype={};class na{}class oa extends l(na,950992890628,null,{ea:1,ka:2}){}oa[k]=[X,fa,{allocator(){this.methods={}}}];const Y={ping:"df911",region:"960db",ga:"030c5"};const qa={...Y};const ra=Object.keys(Y).reduce((b,c)=>{b[Y[c]]=c;return b},{});const sa={la:"8e8e4",fa:"21528"};const ta=Object.keys(sa).reduce((b,c)=>{b[sa[c]]=c;return b},{});function ua(){}ua.prototype={};class va{}class wa extends l(va,950992890626,null,{ca:1,ja:2}){}function Z(){}
wa[k]=[ua,Z.prototype={inputsPQs:qa,memoryQPs:ra,cacheQPs:ta},da,ia,oa,Z.prototype={vdusPQs:{j:"d1ea1",u:"d1ea2",D:"d1ea3",X:"d1ea4",h:"d1ea5",aa:"d1ea6",I:"d1ea7",l:"d1ea9",i:"d1ea10",g:"d1ea11",$:"d1ea12",s:"d1ea13",m:"d1ea14",o:"d1ea15",O:"d1ea16",T:"d1ea17",U:"d1ea18",v:"d1ea19",K:"d1ea20",M:"d1ea21",F:"d1ea22",Y:"d1ea23",Z:"d1ea24",J:"d1ea25",A:"d1ea26",R:"d1ea27",H:"d1ea28",G:"d1ea29",W:"d1ea30",L:"d1ea31",P:"d1ea32",C:"d1ea33",V:"d1ea35"}},Z.prototype={deduceInputs(){const {ba:{g:b}}=this;
return{region:null==b?void 0:b.innerText}}}];var xa=class extends wa.implements(W,U,{get queries(){return this.settings}},{deduceInputs:la,__$id:9509928906}){};module.exports["950992890641"]=U;module.exports["950992890671"]=xa;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['9509928906']=module.exports