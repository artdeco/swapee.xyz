/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
function w({m:a,s:b}){const {l:{setInputs:c},j:{g:k,h:m,element:g}}=this,{left:h,right:e,bottom:d,height:x}=k.getBoundingClientRect(),{left:n,right:r,bottom:y}=m.getBoundingClientRect();a=(null!==b?b:a)||0;a=[[n,y-a],[r,y-a],[e,d],[h,d]];const [[t,C]]=a;a=a.map(([f,l])=>[f-t,l-C]);let p=b=0;for(const [f,l]of a)b=Math.max(b,f),p=Math.max(p,l);g.style.left=`${-(h-n)}px`;g.style.top=`${-(p-x)}px`;c({points:a.map(f=>f.join(",")).join(" "),width:b,height:p})};function z({m:a,o:b}){const {l:{setInputs:c},j:{g:k,h:m,element:g}}=this,{top:h,left:e,height:d,width:x}=k.getBoundingClientRect(),{top:n,left:r,height:y,width:t}=m.getBoundingClientRect();a=(null!==b?b:a)||0;b=[[r+t-a,n],[e+x,h],[e+x,h+d],[r+t-a,n+y]];const [[C,p]]=b;b=b.map(([u,D])=>[u-C,D-p]);let f=0,l=0;for(const [u,D]of b)f=Math.max(f,u),l=Math.max(l,D);g.style.top=`${-(h-n)}px`;g.style.left=`${-(e-(r+t))-a}px`;c({points:b.map(u=>u.join(",")).join(" "),width:f,height:l})};function A(a){const {j:{u:b}}=this;b(a,null)}A._id="92e3ff2";function B(a){const {j:{v:b}}=this;b(a,null)}B._id="9e6afe5";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const E=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const F=E["37270038985"],G=E["372700389810"],H=E["372700389811"];function I(a,b,c,k){return E["372700389812"](a,b,c,k,!1,void 0)};
const J=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const K=J["61893096584"],L=J["61893096586"],M=J["618930965811"],N=J["618930965812"],O=J["618930965815"];function P(){}P.prototype={};function aa(){this.h=this.g=this.i=null}class ba{}class Q extends I(ba,778547221817,aa,{j:1,J:2}){}
Q[H]=[P,K,{constructor(){F(this,()=>{const {queries:{G:a,M:b}}=this;this.scan({source:a,target:b})})},scan:function({source:a,target:b}){const {element:c,l:{vdusPQs:{i:k}},queries:{source:m,target:g}}=this,h=O(c);Object.assign(this,{i:h[k],g:(e=>{let d;e?d=c.closest(e):d=document;return m?d.querySelector(m):void 0})(a),h:(e=>{let d;e?d=c.closest(e):d=document;return g?d.querySelector(g):void 0})(b)})}},{[G]:{i:1,g:1,h:1},initializer({i:a,g:b,h:c}){void 0!==a&&(this.i=a);void 0!==
b&&(this.g=b);void 0!==c&&(this.h=c)}}];var R=class extends Q.implements({paint:[A,B]},{v:w,u:z}){};function S(){}S.prototype={};class ca{}class T extends I(ca,778547221825,null,{A:1,I:2}){}T[H]=[S,M,{}];function U(){}U.prototype={};class da{}class V extends I(da,778547221828,null,{C:1,L:2}){}V[H]=[U,N,{allocator(){this.methods={}}}];const W={direction:"ef72c",m:"0dcba",o:"8e2bd",F:"e2a80",s:"082d0",D:"2da4c",points:"0aab8",width:"eaae2",height:"b435e",visible:"46cf0"};const ea={...W};const fa=Object.keys(W).reduce((a,b)=>{a[W[b]]=b;return a},{});function X(){}X.prototype={};class ha{}class Y extends I(ha,778547221826,null,{l:1,K:2}){}function Z(){}Y[H]=[X,Z.prototype={inputsPQs:ea,queriesPQs:{H:"d5568",O:"a61c6",source:"36cd3",target:"42aef"},memoryQPs:fa},L,V,Z.prototype={vdusPQs:{i:"b5381",g:"b5384",h:"b5385"}}];var ia=class extends Y.implements(T,R,{get queries(){return this.settings}},{__$id:7785472218}){};module.exports["778547221841"]=R;module.exports["778547221871"]=ia;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['7785472218']=module.exports