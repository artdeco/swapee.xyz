/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const f=e["37270038985"],g=e["372700389811"];function h(a,b,k){return e["372700389812"](a,b,null,k,!1,void 0)};const l=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const m=l["61893096584"],n=l["61893096586"],p=l["618930965811"],q=l["618930965812"],r=l["618930965815"];function t(){}t.prototype={};class u{}class v extends h(u,709775549816,{i:1,m:2}){}v[g]=[t,m,{constructor(){f(this,()=>this.scan())},scan:function(){const {element:a,g:{vdusPQs:{Rect:b}}}=this,k=r(a);Object.assign(this,{Rect:k[b]})}}];var w=class extends v.implements(){};function x(){}x.prototype={};class y{}class z extends h(y,709775549823,{h:1,l:2}){}z[g]=[x,p,{}];function A(){}A.prototype={};class B{}class C extends h(B,709775549826,{j:1,s:2}){}C[g]=[A,q,{allocator(){this.methods={}}}];const D={delay:"7243f",duration:"b85ec"};const E={...D};const F=Object.keys(D).reduce((a,b)=>{a[D[b]]=b;return a},{});function G(){}G.prototype={};class H{}class I extends h(H,709775549824,{g:1,o:2}){}function J(){}I[g]=[G,J.prototype={inputsPQs:E,memoryQPs:F},n,C,J.prototype={vdusPQs:{Rect:"e7d62"}}];var K=class extends I.implements({get queries(){return this.settings}},z,w,{__$id:7097755498}){};module.exports["709775549841"]=w;module.exports["709775549871"]=K;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['7097755498']=module.exports