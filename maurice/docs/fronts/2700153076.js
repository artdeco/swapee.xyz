/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const k=h["37270038985"],l=h["372700389810"],m=h["372700389811"];function v(a,c,d,e){return h["372700389812"](a,c,d,e,!1,void 0)};
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const x=w["61893096584"],y=w["61893096586"],z=w["618930965811"],A=w["618930965812"],B=w["618930965815"];function C(){}C.prototype={};function D(){this.j=this.o=this.s=this.u=this.l=this.m=this.v=this.i=this.h=this.g=null}class E{}class F extends v(E,270015307616,D,{D:1,H:2}){}
F[m]=[C,x,{constructor(){k(this,()=>{this.scan({})})},scan:function(){const {element:a,A:{vdusPQs:{g:c,h:d,i:e,v:n,m:p,l:q,u:r,s:t,o:u,j:P}}}=this,b=B(a);Object.assign(this,{g:b[c],h:b[d],i:b[e],v:b[n],m:b[p],l:b[q],u:b[r],s:b[t],o:b[u],j:b[P]})}},{[l]:{g:1,h:1,i:1,v:1,m:1,l:1,u:1,s:1,o:1,j:1},initializer({g:a,h:c,i:d,v:e,m:n,l:p,u:q,s:r,o:t,j:u}){void 0!==a&&(this.g=a);void 0!==c&&(this.h=c);void 0!==d&&(this.i=d);void 0!==e&&(this.v=e);void 0!==n&&(this.m=n);void 0!==p&&(this.l=
p);void 0!==q&&(this.u=q);void 0!==r&&(this.s=r);void 0!==t&&(this.o=t);void 0!==u&&(this.j=u)}}];var G=class extends F.implements(){};function H(){};function I(){}I.prototype={};class J{}class K extends v(J,270015307623,null,{C:1,G:2}){}K[m]=[I,z,{}];function L(){}L.prototype={};class M{}class N extends v(M,270015307626,null,{F:1,J:2}){}N[m]=[L,A,{allocator(){this.methods={}}}];const O={status:"9acb4"};const Q={...O};const R=Object.keys(O).reduce((a,c)=>{a[O[c]]=c;return a},{});function S(){}S.prototype={};class T{}class U extends v(T,270015307624,null,{A:1,I:2}){}function V(){}U[m]=[S,V.prototype={inputsPQs:Q,memoryQPs:R},y,N,V.prototype={vdusPQs:{g:"ee221",h:"ee222",i:"ee223",v:"ee224",m:"ee225",l:"ee226",u:"ee227",s:"ee228",o:"ee229",j:"ee2210"}},V.prototype={deduceInputs(){return{}}}];var W=class extends U.implements(K,G,{get queries(){return this.settings}},{deduceInputs:H,__$id:2700153076}){};module.exports["270015307641"]=G;module.exports["270015307671"]=W;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['2700153076']=module.exports