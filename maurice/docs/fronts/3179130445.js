/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const f=e["37270038985"],g=e["372700389810"],h=e["372700389811"];function l(a,b,k,A){return e["372700389812"](a,b,k,A,!1,void 0)};
const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const n=m["61893096584"],p=m["61893096586"],q=m["618930965811"],r=m["618930965812"],t=m["618930965815"];function u(){}u.prototype={};function v(){this.g=null}class w{}class x extends l(w,317913044516,v,{j:1,s:2}){}x[h]=[u,n,{constructor(){f(this,()=>{this.scan({})})},scan:function(){const {element:a,h:{vdusPQs:{g:b}}}=this,k=t(a);Object.assign(this,{g:k[b]})}},{[g]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var y=class extends x.implements(){};function z(){};function B(){}B.prototype={};class C{}class D extends l(C,317913044523,null,{i:1,o:2}){}D[h]=[B,q,{}];function E(){}E.prototype={};class F{}class G extends l(F,317913044526,null,{l:1,v:2}){}G[h]=[E,r,{allocator(){this.methods={}}}];const H={active:"c76a5",m:"e0542"};const I={...H};const J=Object.keys(H).reduce((a,b)=>{a[H[b]]=b;return a},{});function K(){}K.prototype={};class L{}class M extends l(L,317913044524,null,{h:1,u:2}){}function N(){}M[h]=[K,N.prototype={inputsPQs:I,memoryQPs:J},p,G,N.prototype={vdusPQs:{g:"ie9d1"}}];var O=class extends M.implements(D,y,{get queries(){return this.settings}},{deduceInputs:z,__$id:3179130445}){};module.exports["317913044541"]=y;module.exports["317913044571"]=O;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['3179130445']=module.exports