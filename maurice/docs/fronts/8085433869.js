/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const k=h["37270038985"],l=h["372700389810"],m=h["372700389811"];function E(b,c,d,e){return h["372700389812"](b,c,d,e,!1,void 0)};
const F=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const G=F["61893096584"],H=F["61893096586"],I=F["618930965811"],J=F["618930965812"],K=F["618930965815"];function L(){}L.prototype={};function M(){this.s=this.v=this.i=this.o=this.u=this.K=this.J=this.A=this.l=this.D=this.F=this.G=this.h=this.C=this.j=this.I=this.g=this.H=this.m=null}class N{}class O extends E(N,808543386915,M,{O:1,U:2}){}
O[m]=[L,G,{constructor(){k(this,()=>{this.scan({})})},scan:function(){const {element:b,L:{vdusPQs:{m:c,H:d,g:e,I:n,j:p,C:q,h:r,G:t,F:u,D:v,l:w,A:x,J:y,K:z,u:A,o:B,i:C,v:D,s:Y}}}=this,a=K(b);Object.assign(this,{m:a[c],H:a[d],g:a[e],I:a[n],j:a[p],C:a[q],h:a[r],G:a[t],F:a[u],D:a[v],l:a[w],A:a[x],J:a[y],K:a[z],u:a[A],o:a[B],i:a[C],v:a[D],s:a[Y]})}},{[l]:{m:1,H:1,g:1,I:1,j:1,C:1,h:1,G:1,F:1,D:1,l:1,A:1,J:1,K:1,u:1,o:1,i:1,v:1,s:1},initializer({m:b,H:c,g:d,I:e,j:n,C:p,h:q,G:r,F:t,
D:u,l:v,A:w,J:x,K:y,u:z,o:A,i:B,v:C,s:D}){void 0!==b&&(this.m=b);void 0!==c&&(this.H=c);void 0!==d&&(this.g=d);void 0!==e&&(this.I=e);void 0!==n&&(this.j=n);void 0!==p&&(this.C=p);void 0!==q&&(this.h=q);void 0!==r&&(this.G=r);void 0!==t&&(this.F=t);void 0!==u&&(this.D=u);void 0!==v&&(this.l=v);void 0!==w&&(this.A=w);void 0!==x&&(this.J=x);void 0!==y&&(this.K=y);void 0!==z&&(this.u=z);void 0!==A&&(this.o=A);void 0!==B&&(this.i=B);void 0!==C&&(this.v=C);void 0!==D&&(this.s=D)}}];var P=class extends O.implements(){};function Q(){}Q.prototype={};class R{}class S extends E(R,808543386922,null,{M:1,T:2}){}S[m]=[Q,I,{}];function T(){}T.prototype={};class aa{}class U extends E(aa,808543386925,null,{P:1,W:2}){}U[m]=[T,J,{allocator(){this.methods={}}}];const V={R:"a74ad"};const ba={...V};const ca=Object.keys(V).reduce((b,c)=>{b[V[c]]=c;return b},{});function W(){}W.prototype={};class da{}class X extends E(da,808543386923,null,{L:1,V:2}){}function Z(){}X[m]=[W,Z.prototype={inputsPQs:ba,memoryQPs:ca},H,U,Z.prototype={vdusPQs:{G:"h8a71",F:"h8a72",l:"h8a75",u:"h8a76",o:"h8a77",v:"h8a78",s:"h8a79",J:"h8a710",K:"h8a711",D:"h8a712",i:"h8a713",m:"h8a715",A:"h8a716",C:"h8a717",h:"h8a718",j:"h8a719",g:"h8a720",I:"h8a721",H:"h8a722"}},Z.prototype={deduceInputs(){return{}}}];var ea=class extends X.implements(S,P,{get queries(){return this.settings}},{__$id:8085433869}){};module.exports["808543386941"]=P;module.exports["808543386971"]=ea;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['8085433869']=module.exports