/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
function d({g:a}){this.i=a}d._id="7dab2be";

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const g=e["37270038986"],h=e["372700389811"];function k(a,f,z){return e["372700389812"](a,f,null,z,!1,void 0)};const l=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const m=l["61893096584"],n=l["61893096586"],p=l["618930965811"],q=l["618930965812"];function r(){}r.prototype={};class t{}class u extends k(t,141468516716,{m:1,u:2}){}u[h]=[r,m];var v=class extends u.implements({paint:d}){};function w(){if(this.i){var {j:{element:a}}=this;a.blur();this.setInputs({g:!1})}};function x(){}x.prototype={};class y{}class A extends k(y,141468516723,{l:1,s:2}){}A[h]=[x,p,{}];function B(){}B.prototype={};class C{}class D extends k(C,141468516726,{o:1,A:2}){}D[h]=[B,q,{allocator(){this.methods={h:"18338"}}}];const E={g:"b737e"};const F={...E};const G=Object.keys(E).reduce((a,f)=>{a[E[f]]=f;return a},{});function H(){}H.prototype={};class I{}class J extends k(I,141468516724,{j:1,v:2}){}function K(){}J[h]=[H,K.prototype={inputsPQs:F,memoryQPs:G},n,D,K.prototype={vdusPQs:{}},K.prototype={constructor(){g(this,()=>{const a=this.element;a&&(a.addEventListener("mousedown",()=>{this.uart.t("inv",{mid:"155e5"})}),a.addEventListener("mouseup",()=>{this.uart.t("inv",{mid:"fc520"})}),a.addEventListener("mouseout",()=>{this.h()}))})}},K.prototype={deduceInputs(){return{}}}];var L=class extends J.implements({get queries(){return this.settings}},A,v,{h:w,__$id:1414685167}){};module.exports["141468516741"]=v;module.exports["141468516771"]=L;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1414685167']=module.exports