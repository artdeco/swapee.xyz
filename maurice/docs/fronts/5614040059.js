/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const g=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const k=g["37270038985"],l=g["372700389810"],m=g["372700389811"];function n(a,b,d,h){return g["372700389812"](a,b,d,h,!1,void 0)}const A=g.$advice;const B=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const C=B["61893096584"],D=B["61893096586"],E=B["618930965810"],F=B["618930965811"],G=B["618930965812"],H=B["618930965815"],I=B["618930965819"];function J(){}J.prototype={};function aa(){this.i=this.F=this.s=this.j=this.l=this.D=this.u=this.m=this.A=this.g=this.C=this.o=this.h=this.v=null}class ba{}class K extends n(ba,561404005913,aa,{H:1,$:2}){}
K[m]=[J,C,{constructor(){k(this,()=>this.scan())},scan:function(){const {element:a,queries:b,I:{vdusPQs:{i:d,g:h,h:p,l:q,m:r,v:t,o:u,C:v,A:w,u:x,D:y,j:z,s:ca}}}=this,c=H(a);Object.assign(this,{i:c[d],g:c[h],h:c[p],l:c[q],m:c[r],v:c[t],o:c[u],C:c[v],A:c[w],u:c[x],D:c[y],j:c[z],s:c[ca],F:b.L?document.querySelector(b.L):void 0})}},{[l]:{v:1,h:1,o:1,C:1,g:1,A:1,m:1,u:1,D:1,l:1,j:1,s:1,F:1,i:1},initializer({v:a,h:b,o:d,C:h,g:p,A:q,m:r,u:t,D:u,l:v,j:w,s:x,F:y,i:z}){void 0!==a&&(this.v=
a);void 0!==b&&(this.h=b);void 0!==d&&(this.o=d);void 0!==h&&(this.C=h);void 0!==p&&(this.g=p);void 0!==q&&(this.A=q);void 0!==r&&(this.m=r);void 0!==t&&(this.u=t);void 0!==u&&(this.D=u);void 0!==v&&(this.l=v);void 0!==w&&(this.j=w);void 0!==x&&(this.s=x);void 0!==y&&(this.F=y);void 0!==z&&(this.i=z)}}];var L=class extends K.implements(){};function da(){const {H:{F:a}}=this;if(a)return{K:a.innerHTML}};function M(){}M.prototype={};class ea{}class N extends n(ea,561404005923,null,{U:1,ba:2}){}N[m]=[M,G,{allocator(){this.methods={J:"e9df4",G:"8e05d"}}}];const O={V:"a74ad",memory:"cd69b",cache:"0fea6",X:"ab97f",K:"8ee75",O:"50581"};const fa={...O,cid:"4b7cc",pid:"0db32"};const ha=Object.keys(O).reduce((a,b)=>{a[O[b]]=b;return a},{});const P={connected:"06aa6",P:"c6f8a"};const ia=Object.keys(P).reduce((a,b)=>{a[P[b]]=b;return a},{});function Q(){}Q.prototype={};class ja{}class R extends n(ja,561404005921,null,{I:1,aa:2}){}function S(){}
R[m]=[S.prototype={J(a){const {i:b,asIScreen:{makeElement:d}}=this;return d(b,a)}},Q,S.prototype={inputsPQs:fa,queriesPQs:{K:"8ee75",W:"fe60c",L:"6f19e"},memoryQPs:ha,cacheQPs:ia},D,I,N,S.prototype={vdusPQs:{m:"cc2a1",i:"cc2a2",u:"cc2a5",D:"cc2a7",j:"cc2a8",l:"cc2a9",A:"cc2a10",s:"cc2a11",F:"cc2a12",g:"cc2a13",o:"cc2a14",h:"cc2a15",v:"cc2a16",C:"cc2a17"}},S.prototype={deduceInputs(){const {I:{G:a}}=this;return{...a()}}},S.prototype={deduceInputs(){const {H:{g:a,h:b}}=this;return{O:a?a.innerText:void 0,
P:b?b.innerText:void 0}}}];function T(){}T.prototype={};class ka{}class U extends n(ka,561404005920,null,{T:1,Z:2}){}U[m]=[T,F,{}];function V(){}V.prototype={};class W{}W.prototype[A]=V;class X extends n(W,561404005935,null,{}){}X[m]=[{J(){return{ca:1}},G(){this.M=5}},V];function Y(){}Y.prototype={};class la{static consults(...a){return Z.clone({aspectsInstaller:X}).consults(...a)}}class Z extends n(la,561404005936,null,{R:1,Y:2}){}Z[m]=[Y];Z[m]=[X,{M:E}];var ma=class extends Z.implements({get queries(){return this.settings}},R,U,L,{G:da,__$id:5614040059}){};module.exports["561404005941"]=L;module.exports["561404005971"]=ma;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['5614040059']=module.exports