/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
function n({duration:b,s:a,A:c,l:d},{ClientRect:{height:e}}){e=`${e}`.includes(".")?Math.ceil(e):e+1;const {F:{C:h,D:f},classes:{m:g},element:v,g:L,asIDisplay:{setOwnVar:k}}=this;c&&d?a?(k(h,0),k(f,-e-1E3+"px")):(k(h,0),k(f,-e+"px"),getComputedStyle(L).marginTop,this.setInputs({l:!1})):(k(h,b),k(f,-e+"px"),a?v.classList.add(g):v.classList.remove(g))};function p(b,a){const {o:{v:c}}=this,d={};(a=a["75674"])?({b435e:a}=a,d.ClientRect={height:a}):d.ClientRect={};c(b,d)}p._id="eba6c0c";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const q=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const r=q["37270038985"],t=q["37270038986"],u=q["372700389810"],w=q["372700389811"];function x(b,a,c,d){return q["372700389812"](b,a,c,d,!1,void 0)};
const y=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const z=y["61893096584"],A=y["61893096586"],B=y["618930965811"],C=y["618930965812"],D=y["618930965819"];function E(){}E.prototype={};function F(){this.g=null;this.h=[];this.ClientRect=this.i=null}class G{}class H extends x(G,285497051318,F,{o:1,T:2}){}function I(){}
H[w]=[E,z,I.prototype={constructor(){r(this,()=>{const {queries:{u:b,O:a}}=this;this.scan({handle:b,j:a})})},scan:function({handle:b,j:a}){const {element:c,queries:{handle:d,j:e}}=this;let h=[];if(d){let g;b?g=c.closest(b):g=document;h.push(...(g?g.querySelectorAll(d):[]))}let f;a?f=c.closest(a):f=document;Object.assign(this,{g:c?c.querySelector("*"):null,h,ClientRect:e?f.querySelector(e):void 0})}},I.prototype={F:{C:"b85ec",D:"f0cba"}},{[u]:{g:1,h:1,i:1,ClientRect:1},initializer({g:b,h:a,i:c,ClientRect:d}){void 0!==
b&&(this.g=b);void 0!==a&&(this.h=a);void 0!==c&&(this.i=c);void 0!==d&&(this.ClientRect=d)}}];var J=class extends H.implements({paint:[p]},{v:n},{}){};function K(){r(this,()=>{const b=this.ClientRect,a=this.g;a.style.setProperty("position","relative");a.appendChild(b)})};function M(){}M.prototype={};class N{}class O extends x(N,285497051327,null,{K:1,R:2}){}O[w]=[M,B,{collapse(){this.uart.t("inv",{mid:"b44bd"})}}];const P={m:"2a735",I:"ab27f",G:"e2209",H:"34fda"};function Q(){}Q.prototype={};class R{}class S extends x(R,285497051330,null,{M:1,V:2}){}S[w]=[Q,C,{allocator(){this.methods={}}}];const T={duration:"b85ec",direction:"ef72c",J:"35898",l:"4eb15",A:"1730b",collapsed:"3338c",P:"1a613",s:"5eb0d"};const U={...T,id:"b80bb"};const V=Object.keys(T).reduce((b,a)=>{b[T[a]]=a;return b},{});const W={marginTop:"50cba"};const aa=Object.keys(W).reduce((b,a)=>{b[W[a]]=a;return b},{});const ba=Object.keys(P).reduce((b,a)=>{b[P[a]]=a;return b},{});function X(){}X.prototype={};class ca{}class Y extends x(ca,285497051328,null,{L:1,U:2}){}function Z(){}
Y[w]=[X,Z.prototype={inputsPQs:U,queriesPQs:{handle:"e1260",u:"0c358",j:"87f28"},memoryQPs:V,cacheQPs:aa},A,D,S,Z.prototype={vdusPQs:{g:"hf761",h:"hf762",ClientRect:"hf763",i:"hf764"}},Z.prototype={classesQPs:ba},Z.prototype={constructor(){t(this,()=>{var b=this.g;b&&(b.addEventListener("transitionstart",()=>{this.uart.t("inv",{mid:"abaa1"})}),b.addEventListener("transitionend",()=>{this.uart.t("inv",{mid:"bde71"})}));b=this.h;for(const a of b)a.addEventListener("click",()=>{this.uart.t("inv",{mid:"0e1c3"})})})}}];var da=class extends Y.implements(O,J,{get queries(){return this.settings}},{__$constructor:K,__$id:2854970513}){};module.exports["285497051341"]=J;module.exports["285497051371"]=da;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['2854970513']=module.exports