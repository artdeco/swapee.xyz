/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const k=h["37270038985"],l=h["372700389810"],m=h["372700389811"];function t(a,b,c,d){return h["372700389812"](a,b,c,d,!1,void 0)};
const u=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const v=u["61893096584"],w=u["61893096586"],x=u["618930965811"],y=u["618930965812"],z=u["618930965815"],A=u["618930965819"];function B(){}B.prototype={};function C(){this.j=this.s=this.i=this.o=this.h=this.m=this.g=this.l=null}class D{}class E extends t(D,232415863517,C,{u:1,Z:2}){}
E[m]=[B,v,{constructor(){k(this,()=>{this.scan({})})},scan:function(){const {element:a,v:{vdusPQs:{l:b,g:c,m:d,h:n,o:p,i:q,s:r,j:P}}}=this,e=z(a);Object.assign(this,{l:e[b],g:e[c],m:e[d],h:e[n],o:e[p],i:e[q],s:e[r],j:e[P]})}},{[l]:{l:1,g:1,m:1,h:1,o:1,i:1,s:1,j:1},initializer({l:a,g:b,m:c,h:d,o:n,i:p,s:q,j:r}){void 0!==a&&(this.l=a);void 0!==b&&(this.g=b);void 0!==c&&(this.m=c);void 0!==d&&(this.h=d);void 0!==n&&(this.o=n);void 0!==p&&(this.i=p);void 0!==q&&(this.s=q);void 0!==
r&&(this.j=r)}}];var F=class extends E.implements(){};function G(){};function H(){}H.prototype={};class I{}class J extends t(I,232415863524,null,{G:1,Y:2}){}J[m]=[H,x,{}];function K(){}K.prototype={};class L{}class M extends t(L,232415863527,null,{H:1,aa:2}){}M[m]=[K,y,{allocator(){this.methods={}}}];const N={P:"8a967",X:"e4dfb",K:"813e9"};const O={...N};const Q=Object.keys(N).reduce((a,b)=>{a[N[b]]=b;return a},{});const R={A:"44fde",C:"73cdd",D:"1442a",F:"975a8",W:"f5c90",I:"66c5d",U:"e06aa",M:"f9653",J:"2d2de",V:"9cd41",L:"f63b3",O:"b4573",R:"803a0",T:"f383c"};const S=Object.keys(R).reduce((a,b)=>{a[R[b]]=b;return a},{});function T(){}T.prototype={};class U{}class V extends t(U,232415863525,null,{v:1,$:2}){}function W(){}V[m]=[T,W.prototype={inputsPQs:O,memoryQPs:Q,cacheQPs:S},w,A,M,W.prototype={vdusPQs:{l:"e5c91",g:"e5c92",m:"e5c93",h:"e5c94",o:"e5c95",i:"e5c96",s:"e5c97",j:"e5c98"}},W.prototype={deduceInputs(){const {u:{g:a,h:b,i:c,j:d}}=this;return{A:null==a?void 0:a.innerText,C:null==b?void 0:b.innerText,D:null==c?void 0:c.innerText,F:null==d?void 0:d.innerText}}}];var X=class extends V.implements(J,F,{get queries(){return this.settings}},{deduceInputs:G,__$id:2324158635}){};module.exports["232415863541"]=F;module.exports["232415863571"]=X;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['2324158635']=module.exports