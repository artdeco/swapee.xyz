/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const l=h["37270038985"],m=h["372700389810"],n=h["372700389811"];function p(a,b,d,f){return h["372700389812"](a,b,d,f,!1,void 0)}const q=h.$advice;const D=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const E=D["61893096584"],F=D["61893096586"],G=D["618930965810"],H=D["618930965811"],I=D["618930965812"],aa=D["618930965815"];function J(){}J.prototype={};function ba(){this.g=this.C=this.D=this.F=this.i=this.l=this.o=this.Event=this.j=this.m=this.s=this.u=this.A=this.v=this.h=this.G=null}class ca{}class K extends p(ca,645176555820,ba,{H:1,ea:2}){}
K[n]=[J,E,{constructor(){l(this,()=>this.scan())},scan:function(){const {element:a,queries:b,I:{vdusPQs:{g:d,u:f,h:k,v:r,A:t,s:u,m:v,j:w,Event:x,o:y,l:z,i:A,D:B,C}}}=this,c=aa(a);Object.assign(this,{g:c[d],u:c[f],h:c[k],G:b.M?document.querySelector(b.M):void 0,v:c[r],A:c[t],s:c[u],m:c[v],j:c[w],Event:c[x],o:c[y],l:c[z],i:c[A],F:b.L?document.querySelector(b.L):void 0,D:c[B],C:c[C]})}},{[m]:{G:1,h:1,v:1,A:1,u:1,s:1,m:1,j:1,Event:1,o:1,l:1,i:1,F:1,D:1,C:1,g:1},initializer({G:a,
h:b,v:d,A:f,u:k,s:r,m:t,j:u,Event:v,o:w,l:x,i:y,F:z,D:A,C:B,g:C}){void 0!==a&&(this.G=a);void 0!==b&&(this.h=b);void 0!==d&&(this.v=d);void 0!==f&&(this.A=f);void 0!==k&&(this.u=k);void 0!==r&&(this.s=r);void 0!==t&&(this.m=t);void 0!==u&&(this.j=u);void 0!==v&&(this.Event=v);void 0!==w&&(this.o=w);void 0!==x&&(this.l=x);void 0!==y&&(this.i=y);void 0!==z&&(this.F=z);void 0!==A&&(this.D=A);void 0!==B&&(this.C=B);void 0!==C&&(this.g=C)}}];var L=class extends K.implements(){};function da(){const {H:{F:a}}=this;if(a)return{U:a.innerHTML}};function M({R:a}){if(a){var {H:{I:{O:b}}}=this;for(const [d,f,k]of a)a=new CustomEvent(d),b(`${d}: ${f} -> ${k}`,a.timeStamp,0)}}M._id="2c7be0c";function N(){}N.prototype={};class ea{}class O extends p(ea,645176555831,null,{W:1,ga:2}){}O[n]=[N,I,{allocator(){this.methods={K:"e5dd2",J:"2caeb"}}}];const P={Y:"a74ad",ja:"67a9c",type:"599dc",$:"16908",ba:"8106a",T:"a0fbf",Z:"de81b",aa:"01ae5",ia:"83112",R:"5a9d1",U:"8ee75"};const fa={...P};const ha=Object.keys(P).reduce((a,b)=>{a[P[b]]=b;return a},{});function Q(){}Q.prototype={};class ia{}class R extends p(ia,645176555829,null,{I:1,fa:2}){}function S(){}
R[n]=[S.prototype={K(a){const {g:b,asIScreen:{makeElement:d}}=this;return d(b,a)}},Q,S.prototype={inputsPQs:fa,queriesPQs:{M:"a61c6",L:"6f19e",ca:"7f5b6"},memoryQPs:ha},F,O,S.prototype={vdusPQs:{g:"d2a21",u:"d2a22",G:"d2a23",A:"d2a24",s:"d2a25",m:"d2a26",j:"d2a27",Event:"d2a28",o:"d2a29",l:"d2a210",i:"d2a211",F:"d2a212",h:"d2a213",v:"d2a214",C:"d2a215",D:"d2a216"}},S.prototype={deduceInputs(){const {I:{J:a}}=this;return{...a()}}},S.prototype={deduceInputs(){return{}}},S.prototype={deduceInputs(){const {H:{h:a}}=
this;return{T:a?a.innerText:void 0}}}];function T(){}T.prototype={};class ja{}class U extends p(ja,645176555828,null,{V:1,da:2}){}U[n]=[T,H,{O(){this.uart.t("inv",{mid:"9058d",args:[...arguments]})}}];function V(){}V.prototype={};class W{}W.prototype[q]=V;class X extends p(W,645176555836,null,{}){}X[n]=[{K(){return{ka:1}},J(){this.P=5}},V];function Y(){}Y.prototype={};class ka{static consults(...a){Z[n]=[X,...a];return Z}}class Z extends p(ka,645176555837,null,{X:1,ha:2}){}Z[n]=[Y];Z[n]=[X,{P:G}];var la=class extends Z.implements({get queries(){return this.settings}},R,U,L,{J:da,paint:M,__$id:6451765558}){};module.exports["645176555841"]=L;module.exports["645176555871"]=la;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['6451765558']=module.exports