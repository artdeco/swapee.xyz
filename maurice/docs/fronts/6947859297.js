/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
function f(){let {l:{s:a}}=this;a&&clearInterval(a)};function h({F:a}){let {l:{s:b,v:{o:c}}}=this;c();b&&clearInterval(b);this.s=b=setInterval(c,1E3*a)};function k(a){const {m:{D:b}}=this;b(a,null)}k._id="c748272";function l(a){const {m:{C:b}}=this;b(a,null)}l._id="45ac68b";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const n=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const p=n["37270038985"],q=n["372700389810"],r=n["372700389811"];function t(a,b,c,g){return n["372700389812"](a,b,c,g,!1,void 0)}const u=n.$advice;
const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const w=v["61893096584"],x=v["61893096586"],y=v["618930965810"],z=v["618930965811"],A=v["618930965812"],B=v["618930965815"];function C(){}C.prototype={};function D(){this.j=this.h=this.g=this.i=null}class E{}class F extends t(E,694785929716,D,{m:1,da:2}){}F[r]=[C,w,{constructor(){p(this,()=>{this.scan({})})},scan:function(){const {element:a,l:{vdusPQs:{j:b,i:c,g,h:Y}}}=this,m=B(a);Object.assign(this,{j:m[b],i:m[c],g:m[g],h:m[Y]})}},{[q]:{i:1,g:1,h:1,j:1},initializer({i:a,g:b,h:c,j:g}){void 0!==a&&(this.i=a);void 0!==b&&(this.g=b);void 0!==c&&(this.h=c);void 0!==g&&(this.j=g)}}];var G=class extends F.implements({paint:[k,l]},{D:f,C:h},{}){};function H(){}H.prototype={};class I{}class J extends t(I,694785929723,null,{H:1,ca:2}){}J[r]=[H,z,{}];function K(){}K.prototype={};class L{}L.prototype[u]=K;class M extends t(L,4,null,{}){}M[r]=[{o(){this.u=5}},K];function N(){}N.prototype={};class aa{static consults(...a){return O.clone({aspectsInstaller:M}).consults(...a)}}class O extends t(aa,7,null,{G:1,ba:2}){}O[r]=[N];function P(){}P.prototype={};class ba{}class Q extends t(ba,8,null,{v:1,ea:2}){}function ca(){}Q[r]=[P];function da(){return{A:new Date}};var ea=class extends Q.implements(ca.prototype={o:da}){};function R(){}R.prototype={};class fa{}class S extends n["372700389817"](fa,6,null,{},!1,void 0,void 0){}S[r]=[R];var ha=class extends S.continues({u:y}){};var ia=class extends O.consults(ha).implements(ea){};function T(){}T.prototype={};class ja{}class U extends t(ja,694785929726,null,{I:1,ga:2}){}U[r]=[T,A,{allocator(){this.methods={}}}];const V={Y:"d5e14",X:"9e0f1",aa:"01f13",ha:"b6d8e",V:"0890a",R:"76bde",A:"5486a",T:"803a0",U:"f383c",P:"8a967",$:"f5c90",L:"2d2de",Z:"9cd41",M:"f63b3",W:"e06aa",O:"f9653",J:"66c5d",K:"5f749",F:"c39c1"};const ka={...V,locale:"fb216"};const la=Object.keys(V).reduce((a,b)=>{a[V[b]]=b;return a},{});function W(){}W.prototype={};class ma{}class X extends t(ma,694785929724,null,{l:1,fa:2}){}function Z(){}X[r]=[W,Z.prototype={inputsPQs:ka,memoryQPs:la},x,U,Z.prototype={vdusPQs:{h:"f0276",g:"f0277",i:"f0279",j:"f02713"}}];var na=class extends X.implements(J,ia,G,{get queries(){return this.settings}},{__$id:6947859297}){};module.exports["694785929741"]=G;module.exports["694785929771"]=na;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['6947859297']=module.exports