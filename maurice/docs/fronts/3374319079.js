/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
class n{constructor(a,b){this.loaded=this.h=null;this.l=[a];this.j=[b]}error(a){this.h=a;this.j.forEach(b=>{b(a)})}load(a){this.loaded=a;this.l.forEach(b=>{b(a)})}};const p=(a,b,c)=>{const d=document.createElement("script");d.src=a;d.onload=b;d.onerror=c;(document.head||document.body).appendChild(d)},q=(a,b,c)=>{const d=document.createElement("link");d.rel="stylesheet";d.href=a;d.onload=b;d.onerror=c;(document.head||document.body).appendChild(d)};const r=(a,b)=>{const c=new XMLHttpRequest;c.onreadystatechange=function(){4==c.readyState&&200==c.status&&b(null,c.responseText)};c.onerror=d=>b(d);c.open("GET",a,!0);c.send()},v=(a,b)=>{const c=t(a,q,u[a],d=>b(null,d),d=>b(d),a.nocache);c&&(u[a]=c)};function t(a,b,c,d,e,k){if(k)b(a,d,e);else if(c&&c.loaded)d(c.loaded);else if(c&&c.h)e(c.h);else if(c)c.l.push(d),c.j.push(e);else{const l=new n(d,e);b(a,g=>{l.load(g)},g=>{l.error(g)});return l}}
function x(a,b){let c=!1,d=[],e=0;a.forEach((k,l)=>{const g=f=>{c||(e++,d[l]=f,e==a.length&&b(null,d))},w=f=>{c||(c=f,b(f))};k.endsWith(".json")?r(k,(f,P)=>{f?w(f):g(P)}):y(k,g,w)})}const z={},u={},y=(a,b,c)=>{(b=t(a,p,z[a],b,c,a.nocache))&&(z[a]=b)};function A({o:a,u:b}){const {i:{g:c}}=this;a=a.map(d=>`${"https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.0.0"}/languages/${d}.min.js`);v(`${"https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.0.0"}/styles/${b}.min.css`,()=>{});x(["https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.0.0/highlight.min.js",...a],()=>{window.hljs.highlightElement(c)})};function B(a){const {i:{s:b}}=this;b(a,null)}B._id="73a63a1";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const C=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const D=C["37270038985"],E=C["372700389810"],F=C["372700389811"];function G(a,b,c,d){return C["372700389812"](a,b,c,d,!1,void 0)};
const H=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const I=H["61893096584"],J=H["61893096586"],K=H["618930965811"],L=H["618930965812"],M=H["618930965815"];function N(){}N.prototype={};function O(){this.g=null}class aa{}class Q extends G(aa,337431907916,O,{i:1,D:2}){}Q[F]=[N,I,{constructor(){D(this,()=>{this.scan({})})},scan:function(){const {element:a,m:{vdusPQs:{g:b}}}=this,c=M(a);Object.assign(this,{g:c[b]})}},{[E]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var R=class extends Q.implements({paint:[B]},{s:A}){};function ba(){};function S(){}S.prototype={};class ca{}class T extends G(ca,337431907923,null,{v:1,C:2}){}T[F]=[S,K,{}];function U(){}U.prototype={};class da{}class V extends G(da,337431907926,null,{A:1,G:2}){}V[F]=[U,L,{allocator(){this.methods={}}}];const W={lang:"75725",src:"25d90",u:"f4845",o:"5a058",content:"9a036"};const ea={...W};const fa=Object.keys(W).reduce((a,b)=>{a[W[b]]=b;return a},{});function X(){}X.prototype={};class ha{}class Y extends G(ha,337431907924,null,{m:1,F:2}){}function Z(){}Y[F]=[X,Z.prototype={inputsPQs:ea,memoryQPs:fa},J,V,Z.prototype={vdusPQs:{g:"a28e1"}}];var ia=class extends Y.implements(T,R,{get queries(){return this.settings}},{constructor:ba,__$id:3374319079},{}){};module.exports["337431907941"]=R;module.exports["337431907971"]=ia;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['3374319079']=module.exports