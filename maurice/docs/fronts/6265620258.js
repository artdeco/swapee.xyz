/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const e=d["37270038985"],f=d["37270038986"],h=d["372700389810"],k=d["372700389811"];function l(a,g,A,B){return d["372700389812"](a,g,A,B,!1,void 0)};const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const n=m["61893096584"],p=m["61893096586"],q=m["618930965811"],r=m["618930965812"];function t(){}t.prototype={};function u(){this.g=null}class v{}class w extends l(v,626562025817,u,{j:1,s:2}){}w[k]=[t,n,{constructor(){e(this,()=>this.scan())},scan:function(){const a=this.queries;Object.assign(this,{g:a.h?document.querySelector(a.h):void 0})}},{[h]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var x=class extends w.implements(){};function y(){};function z(){}z.prototype={};class C{}class D extends l(C,626562025825,null,{i:1,o:2}){}D[k]=[z,q,{}];function E(){}E.prototype={};class F{}class G extends l(F,626562025828,null,{m:1,v:2}){}G[k]=[E,r,{allocator(){this.methods={}}}];const H={A:"67a9c",methodName:"ddaa6"};const I={...H};const J=Object.keys(H).reduce((a,g)=>{a[H[g]]=g;return a},{});function K(){}K.prototype={};class L{}class M extends l(L,626562025826,null,{l:1,u:2}){}function N(){}M[k]=[K,N.prototype={inputsPQs:I,queriesPQs:{h:"a61c6"},memoryQPs:J},p,G,N.prototype={vdusPQs:{g:"ee9b1"}},N.prototype={constructor(){f(this,()=>{const a=this.element;a&&a.addEventListener("click",()=>{this.uart.t("inv",{mid:"60a4f"})})})}}];var O=class extends M.implements({get queries(){return this.settings}},D,x,{deduceInputs:y,__$id:6265620258}){};module.exports["626562025841"]=x;module.exports["626562025871"]=O;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['6265620258']=module.exports