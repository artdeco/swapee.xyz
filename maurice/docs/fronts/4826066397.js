/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const f=e["37270038985"],g=e["372700389810"],h=e["372700389811"];function l(a,b,k,B){return e["372700389812"](a,b,k,B,!1,void 0)};const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const n=m["61893096584"],p=m["61893096586"],q=m["618930965811"],r=m["618930965812"],t=m["618930965815"],u=m["618930965819"];function v(){}v.prototype={};function w(){this.g=null}class x{}class y extends l(x,482606639713,w,{m:1,C:2}){}y[h]=[v,n,{constructor(){f(this,()=>this.scan())},scan:function(){const {element:a,h:{vdusPQs:{g:b}}}=this,k=t(a);Object.assign(this,{g:k[b]})}},{[g]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var z=class extends y.implements(){};function A(){};function C(){}C.prototype={};class D{}class E extends l(D,482606639720,null,{l:1,A:2}){}E[h]=[C,q,{nextPage(){this.uart.t("inv",{mid:"d888d"})}}];function F(){}F.prototype={};class G{}class H extends l(G,482606639723,null,{o:1,F:2}){}H[h]=[F,r,{allocator(){this.methods={}}}];const I={v:"4da44",G:"3568a",j:"8c5a1",i:"de26a",page:"71860"};const J={...I,j:"8c5a1",i:"de26a"};const K=Object.keys(I).reduce((a,b)=>{a[I[b]]=b;return a},{});const L={H:"cfa17",s:"bd1dc",u:"b56e2"};const M=Object.keys(L).reduce((a,b)=>{a[L[b]]=b;return a},{});function N(){}N.prototype={};class O{}class P extends l(O,482606639721,null,{h:1,D:2}){}function Q(){}P[h]=[N,Q.prototype={inputsPQs:J,memoryQPs:K,cacheQPs:M},p,u,H,Q.prototype={vdusPQs:{g:"g45d1"}}];var R=class extends P.implements({get queries(){return this.settings}},E,z,{deduceInputs:A,__$id:4826066397}){};module.exports["482606639741"]=z;module.exports["482606639771"]=R;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['4826066397']=module.exports