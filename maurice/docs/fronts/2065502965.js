/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}()
const f=e["37270038985"],g=e["372700389810"],h=e["372700389811"];function k(a,b,l,m){return e["372700389812"](a,b,l,m,!1,void 0)}const n=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}()
const p=n["61893096584"],q=n["61893096586"],r=n["618930965811"],u=n["618930965812"],v=n["618930965819"];function w(){}w.prototype={};function x(){this.g=null}class y{}class z extends k(y,206550296515,x,{o:1,D:2}){}z[h]=[w,p,{constructor(){f(this,()=>this.scan())},scan:function(){const a=this.queries;Object.assign(this,{g:a.h?document.querySelector(a.h):void 0})}},{[g]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var A=class extends z.implements(){};function B(a){const b=document.createElement("img"),{asIScreen:{setInputs:l}}=this;return new Promise((m,L)=>{b.onload=()=>{m(a);l({j:a})};b.onerror=()=>{const t=`Image ${a} could not be loaded.`;L(Error(t));l({i:t})};b.src=a})}function C(){}C.prototype={};class D{}class E extends k(D,206550296523,null,{m:1,C:2}){}E[h]=[C,r,{}];function F(){}F.prototype={};class G{}class H extends k(G,206550296526,null,{u:1,G:2}){}H[h]=[F,u,{allocator(){this.methods={l:"bbbe3"}}}];const I={i:"8f6ab",j:"3deca"};const J={...I,A:"92224"};const K=Object.keys(I).reduce((a,b)=>{a[I[b]]=b;return a},{});const M={v:"8000f"};const N=Object.keys(M).reduce((a,b)=>{a[M[b]]=b;return a},{});function O(){}O.prototype={};class P{}class Q extends k(P,206550296524,null,{s:1,F:2}){}function R(){}Q[h]=[O,q,R.prototype={inputsPQs:J},R.prototype={},R.prototype={queriesPQs:{h:"b798a"}},R.prototype={memoryQPs:K},R.prototype={cacheQPs:N},v,H,R.prototype={vdusPQs:{g:"21133"}}];var S=class extends Q.implements({get queries(){return this.settings}},E,A,{l:B,__$id:2065502965}){};module.exports["206550296541"]=A;module.exports["206550296571"]=S

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['2065502965']=module.exports