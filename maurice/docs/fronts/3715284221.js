/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const f=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const h=f["37270038984"],k=f["372700389811"];function l(a,d,e){return f["372700389812"](a,d,null,e,!1,void 0)}const m=f.$advice;
const p=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const q=p["61893096584"],r=p["61893096586"],t=p["618930965810"],u=p["618930965811"],v=p["618930965812"],w=p["618930965819"];function x(){}x.prototype={};class y{}class z extends l(y,371528422117,{l:1,F:2}){}z[k]=[x,q];var A=class extends z.implements(){};function B(){const {m:{resize:a}}=this;window.addEventListener("resize",a);if("ResizeObserver"in window){if(!("37152842216::observe"in window)){const e=new Map,Z=new ResizeObserver(g=>{for(const {target:n}of g)(g=e.get(n))&&g()});window["37152842216::observe"]=(g,n)=>{e.set(g,n);Z.observe(g)}}var d=window["37152842216::observe"];h(this,()=>{const {l:{element:e}}=this;d(e,a)})}};function aa(){const a=this.element.getBoundingClientRect();return{bottom:a.bottom,height:a.height,left:a.left,right:a.right,top:a.top,width:a.width,x:a.x,y:a.y}};function C(){}C.prototype={};class ba{}class D extends l(ba,371528422133,{s:1,I:2}){}D[k]=[C,v,{allocator(){this.methods={g:"cbab9"}}}];const E={width:"eaae2",height:"b435e",left:"81188",top:"b2835",right:"7c4f2",bottom:"71f26",x:"9dd4e",y:"41529",O:"72d91",C:"c7da9"};const ca={...E};const da=Object.keys(E).reduce((a,d)=>{a[E[d]]=d;return a},{});const F={M:"32afd",A:"c045a"};const ea=Object.keys(F).reduce((a,d)=>{a[F[d]]=d;return a},{});function G(){}G.prototype={};class fa{}class H extends l(fa,371528422129,{h:1,H:2}){}function I(){}H[k]=[G,I.prototype={inputsPQs:ca,memoryQPs:da,cacheQPs:ea},r,w,D,I.prototype={vdusPQs:{}},I.prototype={deduceInputs(){const {h:{g:a}}=this;return{...a()}}}];function J(){}J.prototype={};class ha{}class K extends l(ha,371528422124,{o:1,D:2}){}K[k]=[J,u,{}];function L(){}L.prototype={};class M{}M.prototype[m]=L;class N extends l(M,4,{}){}N[k]=[{resize(){this.i=5}},L];function O(){}O.prototype={};class ia{static consults(...a){return P.clone({aspectsInstaller:N}).consults(...a)}}class P extends l(ia,7,{u:1,J:2}){}P[k]=[O];function Q(){}Q.prototype={};class ja{}class R extends l(ja,8,{m:1,G:2}){}function ka(){}R[k]=[Q];function la(){const {h:{g:a}}=this;a();return{}};var ma=class extends R.implements(ka.prototype={resize:la}){};function S(){}S.prototype={};class na{}class T extends f["372700389817"](na,6,null,{},!1,void 0,void 0){}T[k]=[S];var oa=class extends T.continues({i:t}){};var pa=class extends P.consults(oa).implements(ma){};function U(){}U.prototype={};class V{}V.prototype[m]=U;class W extends l(V,371528422128,{}){}W[k]=[{g(){this.j=5}},U];function X(){}X.prototype={};class qa{static consults(...a){return Y.clone({aspectsInstaller:W}).consults(...a)}}class Y extends l(qa,371528422130,{v:1,K:2}){}Y[k]=[X];Y[k]=[W,{j:t}];var ra=class extends Y.implements(H,K,pa,A,{get queries(){return this.settings}},{__$constructor:B,g:aa,__$id:3715284221}){};module.exports["371528422141"]=A;module.exports["371528422171"]=ra;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['3715284221']=module.exports