/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=e["372700389810"],f=e["372700389811"];function h(a,b,c,d){return e["372700389812"](a,b,c,d,!1,void 0)};function k(){}k.prototype={};class ba{}class m extends h(ba,23241586359,null,{oa:1,Ba:2}){}m[f]=[k];
const n=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=n["61505580523"],p=n["615055805212"],da=n["615055805218"],ea=n["615055805221"],fa=n["615055805223"],q=n["615055805233"],ha=n["615055805238"];const r={v:"8a967",C:"e4dfb",s:"813e9"};const t={M:"44fde",O:"73cdd",P:"1442a",o:"975a8",i:"f5c90",m:"66c5d",g:"e06aa",h:"f9653",j:"2d2de",A:"9cd41",u:"f63b3",l:"b4573",R:"803a0",T:"f383c"};function u(){}u.prototype={};function ia(){this.model={M:0,O:0,P:0,o:0,i:null,m:null,g:null,h:null,j:null,A:null,u:null,l:!1,R:!1,T:!1}}class ja{}class v extends h(ja,23241586358,ia,{ia:1,ua:2}){}function w(){}w.prototype={};function x(){this.model={v:null,C:null,s:null}}class ka{}class y extends h(ka,23241586353,x,{la:1,za:2}){}y[f]=[w,{constructor(){q(this.model,"",r);q(this.model,"",t)}}];v[f]=[{},u,y];

const z=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=z.IntegratedController,ma=z.Parametric;
const A=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const na=A.IntegratedComponentInitialiser,oa=A.IntegratedComponent,pa=A["38"];function B(){}B.prototype={};class qa{}class C extends h(qa,23241586351,null,{ga:1,sa:2}){}C[f]=[B,da];const D={regulate:p({v:Number,C:Date,s:Date})};const E={...r};function F(){}F.prototype={};function ra(){const a={model:null};x.call(a);this.inputs=a.model}class sa{}class G extends h(sa,23241586355,ra,{ma:1,Aa:2}){}function H(){}G[f]=[H.prototype={},F,ma,H.prototype={constructor(){q(this.inputs,"",E)}}];function I(){}I.prototype={};class ta{}class J extends h(ta,232415863520,null,{ba:1,da:2}){}J[f]=[{},I,D,la,{get Port(){return G}}];const ua=p({M:Number,O:Number,P:Number,o:Number,i:Number,m:Number,g:Number,h:Number,j:Number,A:Number,u:Number,l:Boolean,R:Boolean,T:Boolean},{silent:!0});const va=Object.keys(t).reduce((a,b)=>{a[t[b]]=b;return a},{});function K(){}K.prototype={};class wa{}class L extends h(wa,232415863510,null,{fa:1,ra:2}){}L[f]=[K,v,m,oa,C,J,{regulateState:ua,stateQPs:va}];
const M=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const xa=M["12817393923"],ya=M["12817393924"],za=M["12817393925"],Aa=M["12817393926"];function N(){}N.prototype={};class Ba{}class O extends h(Ba,232415863523,null,{ha:1,ta:2}){}O[f]=[N,Aa,{allocator(){this.methods={}}}];function P(){}P.prototype={};class Ca{}class Q extends h(Ca,232415863522,null,{ba:1,da:2}){}Q[f]=[P,J,O,xa];var R=class extends Q.implements(){};const Da=C.__trait({V:function({C:a,s:b}){let c=b.getTime()-a.getTime(),d=null,g=null;0>c?d=a.getTime()-b.getTime():g=c;return{u:d,A:g,j:d||g}},Y:function({i:a}){return{g:a/60}},W:function({g:a}){return{h:a/60}},U:function({h:a}){return{m:a/24}},Z:function({j:a,A:b,u:c}){return{i:Math.floor(a/1E3),R:!!c,T:!!b}},$:function({j:a}){return 0==a?{i:0,g:0,h:0,m:0}:{i:null,g:null,h:null,m:null}},aa:function({i:a,m:b,g:c,h:d,l:g}){b=Math.floor(b);d-=24*b;c-=1440*b;a-=86400*b;d=Math.floor(d);c-=60*d;a-=3600*
d;c=Math.floor(c);let l=0;g||(a-=60*c,l=Math.floor(a));return{M:b,O:d,P:c,o:l}},X:function({v:a,g:b}){return{l:b>a}},adapt:[function(a,b,c){a={C:a.C,s:a.s};a=c?c(a):a;if(a.C&&a.s)return b=c?c(b):b,this.V(a,b)},function(a,b,c){a={i:a.i};a=c?c(a):a;if(a.i)return b=c?c(b):b,this.Y(a,b)},function(a,b,c){a={g:a.g};a=c?c(a):a;if(a.g)return b=c?c(b):b,this.W(a,b)},function(a,b,c){a={h:a.h};a=c?c(a):a;if(a.h)return b=c?c(b):b,this.U(a,b)},function(a,b,c){a={j:a.j,A:a.A,u:a.u};a=c?c(a):a;if(a.j)return b=c?
c(b):b,this.Z(a,b)},function(a,b,c){a={j:a.j};a=c?c(a):a;if(!a.j)return b=c?c(b):b,this.$(a,b)},function(a,b,c){a={i:a.i,g:a.g,h:a.h,m:a.m,l:a.l};a=c?c(a):a;b=c?c(b):b;return this.aa(a,b)},function(a,b,c){a={v:a.v,g:a.g};a=c?c(a):a;if(a.v)return b=c?c(b):b,this.X(a,b)}]});class S extends C.implements(Da){};function T(){}T.prototype={};class Ea{}class U extends h(Ea,232415863519,null,{ja:1,wa:2}){}
U[f]=[T,ya,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{F:a,D:a,H:a,G:a,J:a,I:a,L:a,K:a})}},{[aa]:{F:1,D:1,H:1,G:1,J:1,I:1,L:1,K:1},initializer({F:a,D:b,H:c,G:d,J:g,I:l,L:X,K:Y}){void 0!==a&&(this.F=a);void 0!==b&&(this.D=b);void 0!==c&&(this.H=c);void 0!==d&&(this.G=d);void 0!==g&&(this.J=g);void 0!==l&&(this.I=l);void 0!==X&&(this.L=X);void 0!==Y&&(this.K=Y)}}];const V={ea:"c57de"};const Fa=Object.keys(V).reduce((a,b)=>{a[V[b]]=b;return a},{});const W={F:"e5c91",D:"e5c92",H:"e5c93",G:"e5c94",J:"e5c95",I:"e5c96",L:"e5c97",K:"e5c98"};const Ga=Object.keys(W).reduce((a,b)=>{a[W[b]]=b;return a},{});function Ha(){}Ha.prototype={};class Ia{}class Ja extends h(Ia,232415863516,null,{ca:1,xa:2}){}function Ka(){}Ja[f]=[Ha,Ka.prototype={classesQPs:Fa,vdusQPs:Ga,memoryPQs:r},U,ca,Ka.prototype={allocator(){pa(this.classes,"",V)}}];function La(){}La.prototype={};class Ma{}class Na extends h(Ma,232415863528,null,{qa:1,Da:2}){}Na[f]=[La,za];function Oa(){}Oa.prototype={};class Pa{}class Qa extends h(Pa,232415863526,null,{pa:1,Ca:2}){}Qa[f]=[Oa,Na];const Ra=Object.keys(E).reduce((a,b)=>{a[E[b]]=b;return a},{});function Sa(){}Sa.prototype={};class Ta{static mvc(a,b,c){return fa(this,a,b,null,c)}}class Ua extends h(Ta,232415863512,null,{ka:1,ya:2}){}function Z(){}
Ua[f]=[Sa,ea,L,Ja,Qa,Z.prototype={inputsQPs:Ra},{paint:[function(){this.D.setText(this.model.M)},function(){this.G.setText(this.model.O)},function(){this.I.setText(this.model.P)},function(){this.K.setText(this.model.o)}]},Z.prototype={paint:function({l:a,o:b}){const {ca:{L:c},asIBrowserView:{conceal:d,reveal:g}}=this;({l:a,o:b}={l:a,o:b});a?d(c,!0):g(c,b)}},Z.prototype={paint:ha({F:{M:1},H:{O:1},J:{P:1}})}];var Va=class extends Ua.implements(R,S,na){};module.exports["23241586350"]=L;module.exports["23241586351"]=L;module.exports["23241586353"]=G;module.exports["23241586354"]=J;module.exports["232415863510"]=Va;module.exports["232415863511"]=D;module.exports["232415863530"]=C;module.exports["232415863531"]=S;module.exports["232415863561"]=R;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['2324158635']=module.exports