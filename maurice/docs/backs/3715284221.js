/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const e=d["372700389811"];function f(a,b,c,ba){return d["372700389812"](a,b,c,ba,!1,void 0)};function g(){}g.prototype={};class aa{}class h extends f(aa,37152842219,null,{L:1,$:2}){}h[e]=[g];
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=k["61505580523"],l=k["615055805212"],da=k["615055805218"],ea=k["615055805221"],fa=k["615055805223"],m=k["615055805233"];const n={width:"eaae2",height:"b435e",left:"81188",top:"b2835",right:"7c4f2",bottom:"71f26",x:"9dd4e",y:"41529",h:"72d91",g:"c7da9"};const p={j:"32afd",i:"c045a"};function q(){}q.prototype={};function ha(){this.model={j:null,i:null}}class ia{}class r extends f(ia,37152842218,ha,{F:1,U:2}){}function t(){}t.prototype={};function u(){this.model={width:null,height:null,left:null,top:null,right:null,bottom:null,x:null,y:null,h:"",g:""}}class ja{}class v extends f(ja,37152842213,u,{J:1,Y:2}){}v[e]=[t,{constructor(){m(this.model,"",n);m(this.model,"",p)}}];r[e]=[{},q,v];

const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ka=w.IntegratedController,la=w.Parametric;
const x=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ma=x.IntegratedComponentInitialiser,na=x.IntegratedComponent;function y(){}y.prototype={};class oa{}class z extends f(oa,37152842211,null,{l:1,R:2}){}z[e]=[y,da];const A={regulate:l({width:Number,height:Number,left:Number,top:Number,right:Number,bottom:Number,x:Number,y:Number,h:String,g:String})};const B={...n};function C(){}C.prototype={};function pa(){const a={model:null};u.call(a);this.inputs=a.model}class qa{}class D extends f(qa,37152842215,pa,{K:1,Z:2}){}function E(){}D[e]=[E.prototype={},C,la,E.prototype={constructor(){m(this.inputs,"",B)}}];function F(){}F.prototype={};class ra{}class G extends f(ra,371528422120,null,{v:1,A:2}){}G[e]=[{},F,A,ka,{get Port(){return D}}];const sa=l({j:String,i:String},{silent:!0});const ta=Object.keys(p).reduce((a,b)=>{a[p[b]]=b;return a},{});function H(){}H.prototype={};class ua{}class I extends f(ua,371528422110,null,{C:1,P:2}){}I[e]=[H,r,h,na,z,G,{regulateState:sa,stateQPs:ta}];
const J=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const va=J["12817393923"],wa=J["12817393924"],xa=J["12817393925"],ya=J["12817393926"];function K(){}K.prototype={};class za{}class L extends f(za,371528422123,null,{D:1,T:2}){}L[e]=[K,ya,{allocator(){this.methods={}}}];function M(){}M.prototype={};class Aa{}class N extends f(Aa,371528422122,null,{v:1,A:2}){}N[e]=[M,G,L,va];var O=class extends N.implements(){};function Ba(a,b){"round"==b?a=Math.round(a):"ceil"==b?a=Math.ceil(a):"floor"==b&&(a=Math.floor(a));return a};function Ca(){return{j:"px"}};function Da(){return{i:"px"}};function Ea({width:a,h:b}){const {l:{round:c}}=this;a=c(a,b);return{j:`${a}px`}};function Fa({height:a,g:b}){const {l:{round:c}}=this;a=c(a,b);return{i:`${a}px`}};function Ga(a,b,c){a={width:a.width,h:a.h};a=c?c(a):a;if(a.width)return b=c?c(b):b,this.s(a,b)}function Ha(a,b,c){a={width:a.width};a=c?c(a):a;if(!a.width)return b=c?c(b):b,this.u(a,b)}function Ia(a,b,c){a={height:a.height,g:a.g};a=c?c(a):a;if(a.height)return b=c?c(b):b,this.m(a,b)}function Ja(a,b,c){a={height:a.height};a=c?c(a):a;if(!a.height)return b=c?c(b):b,this.o(a,b)};class P extends z.implements({round:Ba,u:Ca,o:Da,s:Ea,m:Fa,adapt:[Ha,Ja,Ga,Ia]},{}){};function Q(){}Q.prototype={};class Ka{}class R extends f(Ka,371528422119,null,{G:1,V:2}){}R[e]=[Q,wa];const S={};const La=Object.keys(S).reduce((a,b)=>{a[S[b]]=b;return a},{});function T(){}T.prototype={};class Ma{}class U extends f(Ma,371528422116,null,{H:1,W:2}){}U[e]=[T,{vdusQPs:La,memoryPQs:n},R,ca];function V(){}V.prototype={};class Na{}class W extends f(Na,371528422134,null,{O:1,ba:2}){}W[e]=[V,xa,{}];function X(){}X.prototype={};class Oa{}class Y extends f(Oa,371528422132,null,{M:1,aa:2}){}Y[e]=[X,W];const Pa=Object.keys(B).reduce((a,b)=>{a[B[b]]=b;return a},{});function Z(){}Z.prototype={};class Qa{static mvc(a,b,c){return fa(this,a,b,null,c)}}class Ra extends f(Qa,371528422112,null,{I:1,X:2}){}Ra[e]=[Z,ea,I,U,Y,{inputsQPs:Pa}];var Sa=class extends Ra.implements(O,P,ma){};module.exports["37152842210"]=I;module.exports["37152842211"]=I;module.exports["37152842212"]=r;module.exports["37152842213"]=D;module.exports["37152842214"]=G;module.exports["371528422110"]=Sa;module.exports["371528422111"]=A;module.exports["371528422130"]=z;module.exports["371528422131"]=P;module.exports["371528422161"]=O;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['3715284221']=module.exports