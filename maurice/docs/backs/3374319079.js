/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=c["372700389810"],d=c["372700389811"];function f(a,b,e,n){return c["372700389812"](a,b,e,n,!1,void 0)};function g(){}g.prototype={};class ba{}class h extends f(ba,33743190798,null,{H:1,W:2}){}h[d]=[g];
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=k["61505580523"],da=k["61505580526"],ea=k["615055805212"],fa=k["615055805218"],ha=k["615055805221"],ia=k["615055805223"],l=k["615055805233"];const m={lang:"75725",src:"25d90",i:"f4845",h:"5a058",content:"9a036"};function p(){}p.prototype={};class ja{}class q extends f(ja,33743190797,null,{v:1,O:2}){}function r(){}r.prototype={};function t(){this.model={lang:"",i:"default",src:"",content:"",h:[]}}class ka{}class u extends f(ka,33743190793,t,{F:1,U:2}){}u[d]=[r,{constructor(){l(this.model,"",m)}}];q[d]=[{},p,u];

const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=v.IntegratedController,ma=v.Parametric;
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const na=w.IntegratedComponentInitialiser,oa=w.IntegratedComponent,pa=w["38"];function x(){}x.prototype={};class qa{}class y extends f(qa,33743190791,null,{s:1,L:2}){}y[d]=[x,fa];const z={regulate:ea({lang:String,i:String,src:String,content:String,h:[String]})};const A={...m};function B(){}B.prototype={};function ra(){const a={model:null};t.call(a);this.inputs=a.model}class sa{}class C extends f(sa,33743190795,ra,{G:1,V:2}){}function D(){}C[d]=[D.prototype={},B,ma,D.prototype={constructor(){l(this.inputs,"",A)}}];function E(){}E.prototype={};class ta{}class F extends f(ta,337431907919,null,{l:1,m:2}){}F[d]=[{},E,z,la,{get Port(){return C}}];function G(){}G.prototype={};class ua{}class H extends f(ua,33743190799,null,{o:1,K:2}){}H[d]=[G,q,h,oa,y,F];
const I=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const va=I["12817393923"],wa=I["12817393924"],xa=I["12817393925"],ya=I["12817393926"];function J(){}J.prototype={};class za{}class K extends f(za,337431907922,null,{u:1,M:2}){}K[d]=[J,ya,{allocator(){this.methods={}}}];function L(){}L.prototype={};class Aa{}class M extends f(Aa,337431907921,null,{l:1,m:2}){}M[d]=[L,F,K,va];var N=class extends M.implements(){};const Ba=da.__trait({paint:[function({i:a,h:b}){const {asIGraphicsDriverBack:{serMemory:e,t_pa:n}}=this;a=e({i:a,h:b});n({pid:"73a63a1",mem:a})}]});function O(){}O.prototype={};class Ca{}class P extends f(Ca,337431907918,null,{A:1,P:2}){}P[d]=[O,wa,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{g:a})}},{[aa]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];const Q={g:"ca0db"};const Da=Object.keys(Q).reduce((a,b)=>{a[Q[b]]=b;return a},{});const R={g:"a28e1"};const Ea=Object.keys(R).reduce((a,b)=>{a[R[b]]=b;return a},{});function S(){}S.prototype={};class Fa{}class T extends f(Fa,337431907915,null,{C:1,R:2}){}function U(){}T[d]=[S,U.prototype={classesQPs:Da,vdusQPs:Ea,memoryPQs:m},P,ca,U.prototype={allocator(){pa(this.classes,"",Q)}}];function V(){}V.prototype={};class Ga{}class W extends f(Ga,337431907927,null,{J:1,Y:2}){}W[d]=[V,xa];function X(){}X.prototype={};class Ha{}class Y extends f(Ha,337431907925,null,{I:1,X:2}){}Y[d]=[X,W];const Ia=Object.keys(A).reduce((a,b)=>{a[A[b]]=b;return a},{});function Z(){}Z.prototype={};class Ja{static mvc(a,b,e){return ia(this,a,b,null,e)}}class Ka extends f(Ja,337431907911,null,{D:1,T:2}){}Ka[d]=[Z,ha,H,T,Y,{inputsQPs:Ia}];var La=class extends Ka.implements(N,Ba,na){};function Ma({lang:a}){return{h:[a]}};function Na(a,b,e){a={lang:a.lang};a=e?e(a):a;if(a.lang)return b=e?e(b):b,this.j(a,b)};class Oa extends y.implements({j:Ma,adapt:[Na]}){};module.exports["33743190790"]=H;module.exports["33743190791"]=H;module.exports["33743190793"]=C;module.exports["33743190794"]=F;module.exports["337431907910"]=La;module.exports["337431907911"]=z;module.exports["337431907930"]=y;module.exports["337431907931"]=Oa;module.exports["337431907961"]=N;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['3374319079']=module.exports