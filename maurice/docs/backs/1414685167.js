/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const b=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=b["372700389811"];function e(a,d,f,V){return b["372700389812"](a,d,f,V,!1,void 0)};function g(){}g.prototype={};class h{}class k extends e(h,14146851677,null,{G:1,T:2}){}k[c]=[g];
const l=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const m=l["61505580523"],n=l["61505580526"],p=l["615055805212"],aa=l["615055805218"],ba=l["615055805221"],ca=l["615055805223"],q=l["615055805233"];const r={g:"b737e"};function t(){}t.prototype={};class da{}class u extends e(da,14146851676,null,{u:1,K:2}){}function v(){}v.prototype={};function w(){this.model={g:!1}}class ea{}class x extends e(ea,14146851673,w,{D:1,P:2}){}x[c]=[v,{constructor(){q(this.model,"",r)}}];u[c]=[{},t,x];

const y=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const fa=y.IntegratedController,ha=y.Parametric;
const z=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ia=z.IntegratedComponentInitialiser,ja=z.IntegratedComponent;function A(){}A.prototype={};class ka{}class B extends e(ka,14146851671,null,{o:1,I:2}){}B[c]=[A,aa];const C={regulate:p({g:Boolean})};const D={...r};function E(){}E.prototype={};function la(){const a={model:null};w.call(a);this.inputs=a.model}class ma{}class F extends e(ma,14146851675,la,{F:1,R:2}){}function G(){}F[c]=[G.prototype={},E,ha,G.prototype={constructor(){q(this.inputs,"",D)}}];function H(){}H.prototype={};class na{}class I extends e(na,141468516719,null,{h:1,j:2}){}function J(){}I[c]=[J.prototype={},H,C,fa,{get Port(){return F}},J.prototype={i(){const {h:{setInputs:a}}=this;a({g:!0})},l(){const {h:{setInputs:a}}=this;a({g:!1})}}];function K(){}K.prototype={};class oa{}class L extends e(oa,14146851678,null,{m:1,H:2}){}L[c]=[K,u,k,ja,B,I];const M=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const pa=M["12817393923"],qa=M["12817393924"],ra=M["12817393926"];function N(){}N.prototype={};class sa{}class O extends e(sa,141468516722,null,{s:1,J:2}){}O[c]=[N,ra,{allocator(){this.methods={i:"155e5",l:"fc520"}}}];function P(){}P.prototype={};class ta{}class Q extends e(ta,141468516721,null,{h:1,j:2}){}Q[c]=[P,I,O,pa];var R=class extends Q.implements(){};const ua=n.__trait({paint:function({g:a}){const {asIGraphicsDriverBack:{serMemory:d,t_pa:f}}=this;a=d({g:a});f({pid:"7dab2be",mem:a})}});function S(){}S.prototype={};class va{}class T extends e(va,141468516718,null,{v:1,L:2}){}T[c]=[S,qa];const U={};const wa=Object.keys(U).reduce((a,d)=>{a[U[d]]=d;return a},{});function W(){}W.prototype={};class xa{}class X extends e(xa,141468516715,null,{A:1,M:2}){}X[c]=[W,{vdusQPs:wa,memoryPQs:r},T,m];const ya=Object.keys(D).reduce((a,d)=>{a[D[d]]=d;return a},{});function Y(){}Y.prototype={};class za{static mvc(a,d,f){return ca(this,a,d,null,f)}}class Z extends e(za,141468516710,null,{C:1,O:2}){}Z[c]=[Y,ba,L,X,{inputsQPs:ya}];var Aa=class extends Z.implements(R,ua,ia){};module.exports["14146851670"]=L;module.exports["14146851671"]=L;module.exports["14146851672"]=u;module.exports["14146851673"]=F;module.exports["14146851674"]=I;module.exports["141468516710"]=Aa;module.exports["141468516711"]=C;module.exports["141468516730"]=B;module.exports["141468516761"]=R;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1414685167']=module.exports