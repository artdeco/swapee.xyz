/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=e["372700389810"],f=e["372700389811"];function h(a,b,c,g){return e["372700389812"](a,b,c,g,!1,void 0)};function k(){}k.prototype={};class ba{}class r extends h(ba,77854722188,null,{M:1,aa:2}){}r[f]=[k];
const t=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=t["61505580523"],da=t["61505580526"],ea=t["615055805212"],fa=t["615055805218"],ha=t["615055805221"],ia=t["615055805223"],u=t["615055805233"],ja=t["615055805235"];const v={direction:"ef72c",i:"0dcba",m:"8e2bd",v:"e2a80",o:"082d0",u:"2da4c",points:"0aab8",width:"eaae2",height:"b435e",visible:"46cf0"};function w(){}w.prototype={};class ka{}class x extends h(ka,77854722187,null,{H:1,V:2}){}function y(){}y.prototype={};function z(){this.model={direction:"left",visible:!1,i:null,m:null,v:null,o:null,u:null,points:"",width:0,height:0}}class la{}class A extends h(la,77854722183,z,{K:1,Z:2}){}A[f]=[y,{constructor(){u(this.model,"",v)}}];x[f]=[{},w,A];

const D=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ma=D.IntegratedController,na=D.Parametric;
const E=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const oa=E.IntegratedComponentInitialiser,pa=E.IntegratedComponent,qa=E["38"];function F(){}F.prototype={};class ra{}class G extends h(ra,77854722181,null,{F:1,T:2}){}G[f]=[F,fa];const H={regulate:ea({direction:String,visible:Boolean,i:Number,m:Number,v:Number,o:Number,u:Number,points:String,width:Number,height:Number})};const I={...v};function J(){}J.prototype={};function sa(){const a={model:null};z.call(a);this.inputs=a.model}class ta{}class K extends h(ta,77854722185,sa,{L:1,$:2}){}function L(){}K[f]=[L.prototype={},J,na,L.prototype={constructor(){u(this.inputs,"",I)}}];function M(){}M.prototype={};class ua{}class N extends h(ua,778547221821,null,{A:1,C:2}){}N[f]=[{},M,H,ma,{get Port(){return K}}];function O(){}O.prototype={};class va{}class P extends h(va,77854722189,null,{D:1,R:2}){}P[f]=[O,x,r,pa,G,N];
const Q=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const wa=Q["12817393923"],xa=Q["12817393924"],ya=Q["12817393925"],za=Q["12817393926"];function R(){}R.prototype={};class Aa{}class S extends h(Aa,778547221824,null,{G:1,U:2}){}S[f]=[R,za,{allocator(){this.methods={}}}];function T(){}T.prototype={};class Ba{}class U extends h(Ba,778547221823,null,{A:1,C:2}){}U[f]=[T,N,S,wa];var V=class extends U.implements(){};const Ca=da.__trait({paint:[function({direction:a,i:b,m:c},{g,h:l}){const {asIGraphicsDriverBack:{serMemory:B,t_pa:C}}=this,d={};if(g){const {81188:m,b2835:n,b435e:p,eaae2:q}=g.model;d.fa206={81188:m,b2835:n,b435e:p,eaae2:q}}else d.fa206={};if(l){const {81188:m,b2835:n,b435e:p,eaae2:q}=l.model;d["8b71f"]={81188:m,b2835:n,b435e:p,eaae2:q}}else d["8b71f"]={};d.fa206.b435e&&d["8b71f"].b435e&&"left"==a&&(a=B({direction:a,i:b,m:c}),C({pid:"92e3ff2",mem:a,lan:d}))},function({direction:a,i:b,o:c},{g,h:l}){const {asIGraphicsDriverBack:{serMemory:B,
t_pa:C}}=this,d={};if(g){const {81188:m,b2835:n,b435e:p,eaae2:q}=g.model;d.fa206={81188:m,b2835:n,b435e:p,eaae2:q}}else d.fa206={};if(l){const {81188:m,b2835:n,b435e:p,eaae2:q}=l.model;d["8b71f"]={81188:m,b2835:n,b435e:p,eaae2:q}}else d["8b71f"]={};d.fa206.b435e&&d["8b71f"].b435e&&"top"==a&&(a=B({direction:a,i:b,o:c}),C({pid:"9e6afe5",mem:a,lan:d}))}]});function W(){}W.prototype={};class Da{}class Ea extends h(Da,778547221820,null,{I:1,W:2}){}function X(){}
Ea[f]=[W,xa,X.prototype={constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{j:a,g:a,h:a})}},X.prototype={paint({width:a}){const {asIGraphicsDriverBack:{setStyle:b,element:c}}=this;b(c,"width",a)}},X.prototype={paint({height:a}){const {asIGraphicsDriverBack:{setStyle:b,element:c}}=this;b(c,"height",a)}},{[aa]:{j:1,g:1,h:1},initializer({j:a,g:b,h:c}){void 0!==a&&(this.j=a);void 0!==b&&(this.g=b);void 0!==c&&(this.h=c)}}];const Y={s:"ec24d"};const Fa=Object.keys(Y).reduce((a,b)=>{a[Y[b]]=b;return a},{});const Ga={j:"b5381",g:"b5384",h:"b5385"};const Ha=Object.keys(Ga).reduce((a,b)=>{a[Ga[b]]=b;return a},{});function Ia(){}Ia.prototype={};class Ja{}class Ka extends h(Ja,778547221816,null,{l:1,X:2}){}function La(){}Ka[f]=[Ia,La.prototype={classesQPs:Fa,vdusQPs:Ha,memoryPQs:v},Ea,ca,La.prototype={allocator(){qa(this.classes,"",Y)}}];function Ma(){}Ma.prototype={};class Na{}class Oa extends h(Na,778547221829,null,{P:1,ca:2}){}Oa[f]=[Ma,ya];function Pa(){}Pa.prototype={};class Qa{}class Ra extends h(Qa,778547221827,null,{O:1,ba:2}){}Ra[f]=[Pa,Oa];const Sa=Object.keys(I).reduce((a,b)=>{a[I[b]]=b;return a},{});function Ta(){}Ta.prototype={};class Ua{static mvc(a,b,c){return ia(this,a,b,null,c)}}class Va extends h(Ua,778547221812,null,{J:1,Y:2}){}function Z(){}
Va[f]=[Ta,ha,P,Ka,Ra,ja,Z.prototype={constructor(){this.land={g:null,h:null}}},Z.prototype={deduceProps(){const {asIBrowserView:{weHaveClass:a},classes:{s:b}}=this;return{visible:a(b)}}},Z.prototype={inputsQPs:Sa},Z.prototype={paint:function({points:a}){const {l:{j:b},asIBrowserView:{attr:c}}=this;c(b,"points",a)}},Z.prototype={paint:function({width:a}){const {l:{element:b},asIBrowserView:{attr:c}}=this;c(b,"width",a)}},Z.prototype={paint:function({height:a}){const {l:{element:b},asIBrowserView:{attr:c}}=
this;c(b,"height",a)}},Z.prototype={paint:function({visible:a}){const {l:{element:b},classes:{s:c},asIBrowserView:{addClass:g,removeClass:l}}=this;a?g(b,c):l(b,c)}},Z.prototype={scheduleTwo:function(){const {asIHtmlComponent:{complete:a},l:{g:b,h:c}}=this;a(3715284221,{g:b});a(3715284221,{h:c})}}];var Wa=class extends Va.implements(V,Ca,oa,{}){};module.exports["77854722180"]=P;module.exports["77854722181"]=P;module.exports["77854722183"]=K;module.exports["77854722184"]=N;module.exports["778547221810"]=Wa;module.exports["778547221811"]=H;module.exports["778547221830"]=G;module.exports["778547221861"]=V;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['7785472218']=module.exports