/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const b=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=b["372700389810"],c=b["372700389811"];function e(a,d,l,ba){return b["372700389812"](a,d,l,ba,!1,void 0)};function f(){}f.prototype={};class ca{}class g extends e(ca,62656202587,null,{G:1,V:2}){}g[c]=[f];
const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const da=h["61505580523"],ea=h["615055805212"],fa=h["615055805218"],ha=h["615055805221"],ia=h["615055805223"],k=h["615055805233"],ja=h["615055805235"];const m={h:"67a9c",methodName:"ddaa6"};function n(){}n.prototype={};class ka{}class p extends e(ka,62656202586,null,{v:1,M:2}){}function q(){}q.prototype={};function r(){this.model={h:0,methodName:""}}class la{}class t extends e(la,62656202583,r,{D:1,T:2}){}t[c]=[q,{constructor(){k(this.model,"",m)}}];p[c]=[{},n,t];

const u=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ma=u.IntegratedController,na=u.Parametric;
const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const oa=v.IntegratedComponentInitialiser,pa=v.IntegratedComponent;function w(){}w.prototype={};class qa{}class x extends e(qa,62656202581,null,{s:1,K:2}){}x[c]=[w,fa];const y={regulate:ea({h:Number,methodName:String})};const z={...m};function A(){}A.prototype={};function ra(){const a={model:null};r.call(a);this.inputs=a.model}class sa{}class B extends e(sa,62656202585,ra,{F:1,U:2}){}function C(){}B[c]=[C.prototype={},A,na,C.prototype={constructor(){k(this.inputs,"",z)}}];function D(){}D.prototype={};class ta{}class E extends e(ta,626562025821,null,{i:1,m:2}){}E[c]=[{},D,y,ma,{get Port(){return B}}];function F(){}F.prototype={};class ua{}class G extends e(ua,62656202588,null,{o:1,J:2}){}G[c]=[F,p,g,pa,x,E];function va(){};const H=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const wa=H["12817393923"],xa=H["12817393924"],ya=H["12817393925"],za=H["12817393926"];function I(){}I.prototype={};class Aa{}class J extends e(Aa,626562025824,null,{u:1,L:2}){}J[c]=[I,za,{allocator(){this.methods={l:"60a4f"}}}];function K(){}K.prototype={};class Ba{}class L extends e(Ba,626562025823,null,{i:1,m:2}){}L[c]=[K,E,J,wa];var M=class extends L.implements(){};function Ca(){const {model:{methodName:a},land:{g:d}}=this;if(d)d[a]()};var N=class extends g.implements({l:Ca}){};function O(){}O.prototype={};class Da{}class P extends e(Da,626562025820,null,{A:1,O:2}){}P[c]=[O,xa,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{g:a})}},{[aa]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];const Q={g:"ee9b1"};const Ea=Object.keys(Q).reduce((a,d)=>{a[Q[d]]=d;return a},{});function R(){}R.prototype={};class Fa{}class S extends e(Fa,626562025816,null,{j:1,P:2}){}S[c]=[R,{vdusQPs:Ea,memoryPQs:m},P,da];function T(){}T.prototype={};class Ga{}class U extends e(Ga,626562025829,null,{I:1,X:2}){}U[c]=[T,ya];function V(){}V.prototype={};class Ha{}class W extends e(Ha,626562025827,null,{H:1,W:2}){}W[c]=[V,U];const Ia=Object.keys(z).reduce((a,d)=>{a[z[d]]=d;return a},{});function X(){}X.prototype={};class Ja{static mvc(a,d,l){return ia(this,a,d,null,l)}}class Y extends e(Ja,626562025811,null,{C:1,R:2}){}function Z(){}Y[c]=[X,ha,G,S,W,ja,Z.prototype={constructor(){this.land={g:null}}},Z.prototype={inputsQPs:Ia},Z.prototype={scheduleTwo:function(){const {asIHtmlComponent:{complete:a},j:{g:d}}=this;a(this.model.h,{g:d})}}];var Ka=class extends Y.implements(M,N,oa,{__$constructor:va}){};module.exports["62656202580"]=G;module.exports["62656202581"]=G;module.exports["62656202582"]=p;module.exports["62656202583"]=B;module.exports["62656202584"]=E;module.exports["626562025810"]=Ka;module.exports["626562025811"]=y;module.exports["626562025830"]=x;module.exports["626562025851"]=N;module.exports["626562025861"]=M;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['6265620258']=module.exports