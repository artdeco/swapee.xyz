/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}


const c=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function f(a,b,e,ba){return c["372700389812"](a,b,e,ba,!1,void 0)};function g(){}g.prototype={};class aa{}class h extends f(aa,70977554987,null,{C:1,R:2}){}h[d]=[g];
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=k["61505580523"],da=k["615055805212"],ea=k["615055805218"],fa=k["615055805221"],ha=k["615055805223"],l=k["615055805233"];const m={delay:"7243f",duration:"b85ec"};function n(){}n.prototype={};class ia{}class p extends f(ia,70977554986,null,{o:1,J:2}){}function q(){}q.prototype={};function r(){this.model={delay:null,duration:"2s"}}class ja{}class t extends f(ja,70977554983,r,{v:1,O:2}){}t[d]=[q,{constructor(){l(this.model,"",m)}}];p[d]=[{},n,t];

const u=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ka=u.IntegratedController,la=u.Parametric;
const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ma=v.IntegratedComponentInitialiser,na=v.IntegratedComponent,oa=v["38"];function w(){}w.prototype={};class pa{}class x extends f(pa,70977554981,null,{l:1,H:2}){}x[d]=[w,ea];const y={regulate:da({delay:String,duration:String})};const z={...m};function A(){}A.prototype={};function qa(){const a={model:null};r.call(a);this.inputs=a.model}class ra{}class B extends f(ra,70977554985,qa,{A:1,P:2}){}function C(){}B[d]=[C.prototype={},A,la,C.prototype={constructor(){l(this.inputs,"",z)}}];function D(){}D.prototype={};class sa{}class E extends f(sa,709775549819,null,{h:1,i:2}){}E[d]=[{},D,y,ka,{get Port(){return B}}];function F(){}F.prototype={};class ta{}class G extends f(ta,70977554988,null,{j:1,G:2}){}G[d]=[F,p,h,na,x,E];const H=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const ua=H["12817393923"],va=H["12817393924"],wa=H["12817393925"],xa=H["12817393926"];function I(){}I.prototype={};class ya{}class J extends f(ya,709775549822,null,{m:1,I:2}){}J[d]=[I,xa,{allocator(){this.methods={}}}];function K(){}K.prototype={};class za{}class L extends f(za,709775549821,null,{h:1,i:2}){}L[d]=[K,E,J,ua];var M=class extends L.implements(){};function N(){}N.prototype={};class Aa{}class O extends f(Aa,709775549818,null,{s:1,K:2}){}O[d]=[N,va,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{Rect:a})}}];const P={props:"50fe0",Rect:"69ad5"};const Ba=Object.keys(P).reduce((a,b)=>{a[P[b]]=b;return a},{});const Q={Rect:"e7d62"};const Ca=Object.keys(Q).reduce((a,b)=>{a[Q[b]]=b;return a},{});function R(){}R.prototype={};class Da{}class S extends f(Da,709775549815,null,{g:1,L:2}){}function T(){}S[d]=[R,T.prototype={classesQPs:Ba,vdusQPs:Ca,memoryPQs:m},O,ca,T.prototype={allocator(){oa(this.classes,"",P)}}];function U(){}U.prototype={};class Ea{}class V extends f(Ea,709775549827,null,{F:1,U:2}){}V[d]=[U,wa];function W(){}W.prototype={};class Fa{}class X extends f(Fa,709775549825,null,{D:1,T:2}){}X[d]=[W,V];const Ga=Object.keys(z).reduce((a,b)=>{a[z[b]]=b;return a},{});function Y(){}Y.prototype={};class Ha{static mvc(a,b,e){return ha(this,a,b,null,e)}}class Ia extends f(Ha,709775549810,null,{u:1,M:2}){}function Z(){}Ia[d]=[Y,fa,G,S,X,Z.prototype={inputsQPs:Ga},Z.prototype={paint:function({duration:a}){const {g:{Rect:b},asIBrowserView:{style:e}}=this;e(b,"animation-duration",a)}},Z.prototype={paint:function({delay:a}){const {g:{Rect:b},asIBrowserView:{style:e}}=this;e(b,"animation-delay",a)}}];var Ja=class extends Ia.implements(M,ma){};module.exports["70977554980"]=G;module.exports["70977554981"]=G;module.exports["70977554982"]=p;module.exports["70977554983"]=B;module.exports["70977554984"]=E;module.exports["709775549810"]=Ja;module.exports["709775549811"]=y;module.exports["709775549830"]=x;module.exports["709775549861"]=M;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['7097755498']=module.exports