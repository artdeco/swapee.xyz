FROM node:alpine

# RUN apk add --no-cache wkhtmltopdf

# Use the following command to set up dokku
# dokku docker-options:add swapee-bot build '--build-arg ARTDECO_TOKEN=`dokku config:get luddites LUDDITES_TOKEN`'

ARG ARTDECO_TOKEN

# app itself
# WORKDIR /
# COPY generated generated

WORKDIR /app
COPY package.json yarn.lock .npmrc scripts.json ./
COPY types/package types/package
COPY generated/link-ws.js generated/link-ws.js
COPY .vscode .vscode
# COPY generated generated
RUN yarn

COPY . .

# server
WORKDIR /app/server
COPY server/package.json server/yarn.lock ./
RUN yarn

# model
# WORKDIR /app/model
# COPY model/package.json model/yarn.lock ./
# RUN yarn

# telega
# WORKDIR /app/telega
# COPY telega/package.json telega/yarn.lock ./
# RUN yarn

WORKDIR /app

ENV NODE_ENV production
# ENV _IDIO_DEBUG true

CMD [ "node", "server/bin", "server"]
