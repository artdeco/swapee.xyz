import {installLudditesTypeEngineer, LUDDITES as luddites, removeH, removeTypalMeta} from '@artdeco/package'
import {description} from '../package.json'
import {TYPEDEFS,EXTERNS, RUNTYPES, TYPOLOGY} from './common'

export const LUDDITES=luddites(description)

LUDDITES.copy={
 ...LUDDITES.copy,...TYPEDEFS,...EXTERNS,...RUNTYPES,...TYPOLOGY,
}
LUDDITES.ignore=[
 '**/typal.js','**/*.typal.js',
 '**/precompile/index.js',
 '**/precompile/browser.js',
]
LUDDITES.postProcessing=()=>{
 removeTypalMeta('package/luddites','types/typedefs.js','types/externs.js')
 removeH('package/luddites','browser/','node/')
}
// installShared(LUDDITES, '@can/install-shared')
installLudditesTypeEngineer(LUDDITES)

LUDDITES.addFiles.push(
 'compile/dist/paid/node',
 'compile/dist/paid/browser',
 'compile/dist/paid/version',
 'compile/dist/paid/3600135954',
 'compile/dist/paid/precompile',
)
LUDDITES.externs=[
 'types/ns/xyz.swapee.js',
 'types/ns/io.letsexchange.js',
 'types/ns/io.changenow.js',
 'types/externs.js',
]
LUDDITES.runtypes=[
 'types/ns/$xyz.ns',
 'types/ns/$xyz.swapee.ns',
 'types/ns/$io.ns',
 'types/ns/$io.changenow.ns',
 'types/ns/$io.letsexchange.ns',
 'types/define.js',
]
LUDDITES.module='node/index.js'
LUDDITES.browser='browser/index.js'

LUDDITES.bin={
 "swapee_xyz":"3600135954/bin/swapee_xyz.js",
}