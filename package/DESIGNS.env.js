import '@type.engineering/web-computing'
import {description, name} from '../package.json'

/** @type {_lud.Env} */
export const DESIGNS={
 name:`${name}.d`,
 description:`${description} [DESIGNS]`,
 addFiles:[
  'types/design',
  'types/api.xml',

  'CHANGELOG.md',
  'node_modules/@artdeco/license/EULA.md',
 ],
}