require('@type.engineering/type-engineer/types/typedefs')
require('./types/typology')
require('./types/typedefs')

/**
@license
@LICENSE @swapee.xyz/swapee_xyz (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/**
 * The _LetsExchange_ client.
 * @extends {io.letsexchange.LetsExchange}
 */
class LetsExchange extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `io.letsexchange.ILetsExchangeAspects` interface.
 * @extends {io.letsexchange.AbstractLetsExchangeAspects}
 */
class AbstractLetsExchangeAspects extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `io.letsexchange.IHyperLetsExchange` interface.
 * @extends {io.letsexchange.AbstractHyperLetsExchange}
 */
class AbstractHyperLetsExchange extends (class {/* lazy-loaded */}) {}
/** @extends {io.letsexchange.HyperLetsExchange} */
class HyperLetsExchange extends (class {/* lazy-loaded */}) {}
/**
 * The symbol used to inform the meta-universal of mesa-universals.
 * @type {symbol}
 */
let LetsExchangeMetaUniversal
/**
 * A trait that allows to access an _ILetsExchange_ object via a `.letsExchange` field.
 * @extends {io.letsexchange.LetsExchangeUniversal}
 */
class LetsExchangeUniversal extends (class {/* lazy-loaded */}) {}
/** @extends {io.changenow.ChangeNowAspectsInstaller} */
class ChangeNowAspectsInstaller extends (class {/* lazy-loaded */}) {}
/**
 * The _ChangeNow_ client.
 * @extends {io.changenow.ChangeNow}
 */
class ChangeNow extends (class {/* lazy-loaded */}) {}
/**
 * The symbol used to inform the meta-universal of mesa-universals.
 * @type {symbol}
 */
let ChangeNowMetaUniversal
/**
 * A trait that allows to access an _IChangeNow_ object via a `.changeNow` field.
 * @extends {io.changenow.ChangeNowUniversal}
 */
class ChangeNowUniversal extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `io.changenow.IChangeNowAspects` interface.
 * @extends {io.changenow.AbstractChangeNowAspects}
 */
class AbstractChangeNowAspects extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `io.changenow.IHyperChangeNow` interface.
 * @extends {io.changenow.AbstractHyperChangeNow}
 */
class AbstractHyperChangeNow extends (class {/* lazy-loaded */}) {}
/** @extends {io.changenow.HyperChangeNow} */
class HyperChangeNow extends (class {/* lazy-loaded */}) {}
/** @extends {io.letsexchange.LetsExchangeAspectsInstaller} */
class LetsExchangeAspectsInstaller extends (class {/* lazy-loaded */}) {}
/**
 * The package.
 * @extends {xyz.swapee.Swapee}
 */
class Swapee extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.SwapeeAspectsInstaller} */
class SwapeeAspectsInstaller extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.ISwapeeAspects` interface.
 * @extends {xyz.swapee.AbstractSwapeeAspects}
 */
class AbstractSwapeeAspects extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.IHyperSwapee` interface.
 * @extends {xyz.swapee.AbstractHyperSwapee}
 */
class AbstractHyperSwapee extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.HyperSwapee} */
class HyperSwapee extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.SwapeeAide} */
class SwapeeAide extends (class {/* lazy-loaded */}) {}

module.exports.LetsExchange = LetsExchange
module.exports.AbstractLetsExchangeAspects = AbstractLetsExchangeAspects
module.exports.AbstractHyperLetsExchange = AbstractHyperLetsExchange
module.exports.HyperLetsExchange = HyperLetsExchange
module.exports.LetsExchangeMetaUniversal = LetsExchangeMetaUniversal
module.exports.LetsExchangeUniversal = LetsExchangeUniversal
module.exports.ChangeNowAspectsInstaller = ChangeNowAspectsInstaller
module.exports.ChangeNow = ChangeNow
module.exports.ChangeNowMetaUniversal = ChangeNowMetaUniversal
module.exports.ChangeNowUniversal = ChangeNowUniversal
module.exports.AbstractChangeNowAspects = AbstractChangeNowAspects
module.exports.AbstractHyperChangeNow = AbstractHyperChangeNow
module.exports.HyperChangeNow = HyperChangeNow
module.exports.LetsExchangeAspectsInstaller = LetsExchangeAspectsInstaller
module.exports.Swapee = Swapee
module.exports.SwapeeAspectsInstaller = SwapeeAspectsInstaller
module.exports.AbstractSwapeeAspects = AbstractSwapeeAspects
module.exports.AbstractHyperSwapee = AbstractHyperSwapee
module.exports.HyperSwapee = HyperSwapee
module.exports.SwapeeAide = SwapeeAide

Object.defineProperties(module.exports, {
 'LetsExchange': {get: () => require('./compile')[36001359542]},
 [36001359542]: {get: () => module.exports['LetsExchange']},
 'AbstractLetsExchangeAspects': {get: () => require('./compile')[36001359543]},
 [36001359543]: {get: () => module.exports['AbstractLetsExchangeAspects']},
 'AbstractHyperLetsExchange': {get: () => require('./compile')[36001359545]},
 [36001359545]: {get: () => module.exports['AbstractHyperLetsExchange']},
 'HyperLetsExchange': {get: () => require('./compile')[36001359546]},
 [36001359546]: {get: () => module.exports['HyperLetsExchange']},
 'LetsExchangeMetaUniversal': {get: () => require('./compile')[36001359547]},
 [36001359547]: {get: () => module.exports['LetsExchangeMetaUniversal']},
 'LetsExchangeUniversal': {get: () => require('./compile')[36001359548]},
 [36001359548]: {get: () => module.exports['LetsExchangeUniversal']},
 'ChangeNowAspectsInstaller': {get: () => require('./compile')[36001359549]},
 [36001359549]: {get: () => module.exports['ChangeNowAspectsInstaller']},
 'ChangeNow': {get: () => require('./compile')[360013595410]},
 [360013595410]: {get: () => module.exports['ChangeNow']},
 'ChangeNowMetaUniversal': {get: () => require('./compile')[360013595411]},
 [360013595411]: {get: () => module.exports['ChangeNowMetaUniversal']},
 'ChangeNowUniversal': {get: () => require('./compile')[360013595412]},
 [360013595412]: {get: () => module.exports['ChangeNowUniversal']},
 'AbstractChangeNowAspects': {get: () => require('./compile')[360013595413]},
 [360013595413]: {get: () => module.exports['AbstractChangeNowAspects']},
 'AbstractHyperChangeNow': {get: () => require('./compile')[360013595415]},
 [360013595415]: {get: () => module.exports['AbstractHyperChangeNow']},
 'HyperChangeNow': {get: () => require('./compile')[360013595416]},
 [360013595416]: {get: () => module.exports['HyperChangeNow']},
 'LetsExchangeAspectsInstaller': {get: () => require('./compile')[360013595417]},
 [360013595417]: {get: () => module.exports['LetsExchangeAspectsInstaller']},
 'Swapee': {get: () => require('./compile')[360013595418]},
 [360013595418]: {get: () => module.exports['Swapee']},
 'SwapeeAspectsInstaller': {get: () => require('./compile')[360013595419]},
 [360013595419]: {get: () => module.exports['SwapeeAspectsInstaller']},
 'AbstractSwapeeAspects': {get: () => require('./compile')[360013595420]},
 [360013595420]: {get: () => module.exports['AbstractSwapeeAspects']},
 'AbstractHyperSwapee': {get: () => require('./compile')[360013595422]},
 [360013595422]: {get: () => module.exports['AbstractHyperSwapee']},
 'HyperSwapee': {get: () => require('./compile')[360013595423]},
 [360013595423]: {get: () => module.exports['HyperSwapee']},
 'SwapeeAide': {get: () => require('./compile')[360013595424]},
 [360013595424]: {get: () => module.exports['SwapeeAide']},
})