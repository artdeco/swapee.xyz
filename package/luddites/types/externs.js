/**
 * @fileoverview
 * @externs
 */

/** @const */
var $$io={}
$$io.changenow={}
$$io.changenow.IChangeNowJoinpointModel={}
$$io.changenow.IChangeNow={}
$$io.letsexchange={}
$$io.letsexchange.ILetsExchangeJoinpointModel={}
$$io.letsexchange.ILetsExchange={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.ISwapeeAide={}
$$xyz.swapee.ISwapeeJoinpointModel={}
$$xyz.swapee.ISwapee={}
$$xyz.swapee.IChangellyConnectorJoinpointModel={}
$$xyz.swapee.IChangellyConnector={}
xyz.swapee.swapee_xyz={}
/**
 * @record
 * @extends {engineer.type.INetwork.Initialese}
 */
io.changenow.IChangeNow.Initialese = function() {}
/** @type {string|undefined} */
io.changenow.IChangeNow.Initialese.prototype.host
/** @type {string|undefined} */
io.changenow.IChangeNow.Initialese.prototype.apiPath
/** @type {string|undefined} */
io.changenow.IChangeNow.Initialese.prototype.changeNowApiKey

/** @interface */
io.changenow.IChangeNowFields
/** @type {string} */
io.changenow.IChangeNowFields.prototype.host
/** @type {string} */
io.changenow.IChangeNowFields.prototype.apiPath
/** @type {string} */
io.changenow.IChangeNowFields.prototype.changeNowApiKey

/** @interface */
io.changenow.IChangeNowCaster
/** @type {!io.changenow.BoundIChangeNow} */
io.changenow.IChangeNowCaster.prototype.asIChangeNow
/** @type {!io.changenow.BoundChangeNow} */
io.changenow.IChangeNowCaster.prototype.superChangeNow

/** @interface */
io.changenow.UChangeNowFields
/** @type {io.changenow.IChangeNow} */
io.changenow.UChangeNowFields.prototype.changeNow

/** @interface */
io.changenow.UChangeNowCaster
/** @type {!io.changenow.BoundChangeNow} */
io.changenow.UChangeNowCaster.prototype.asChangeNow
/** @type {!io.changenow.BoundUChangeNow} */
io.changenow.UChangeNowCaster.prototype.asUChangeNow
/** @type {!io.changenow.BoundChangeNowUniversal} */
io.changenow.UChangeNowCaster.prototype.superChangeNowUniversal

/**
 * @interface
 * @extends {io.changenow.UChangeNowFields}
 * @extends {engineering.type.IEngineer}
 * @extends {io.changenow.UChangeNowCaster}
 */
io.changenow.UChangeNow = function() {}
/** @param {...!io.changenow.UChangeNow.Initialese} init */
io.changenow.UChangeNow.prototype.constructor = function(...init) {}

/**
 * @interface
 * @extends {io.changenow.IChangeNowFields}
 * @extends {engineering.type.IEngineer}
 * @extends {io.changenow.IChangeNowCaster}
 * @extends {engineer.type.INetwork}
 * @extends {io.changenow.UChangeNow}
 */
io.changenow.IChangeNow = function() {}
/** @param {...!io.changenow.IChangeNow.Initialese} init */
io.changenow.IChangeNow.prototype.constructor = function(...init) {}
/**
 * @param {!io.changenow.IChangeNow.GetExchangeAmount.Data} data
 * @return {!Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>}
 */
io.changenow.IChangeNow.prototype.GetExchangeAmount = function(data) {}
/**
 * @param {!io.changenow.IChangeNow.GetFixedExchangeAmount.Data} data
 * @return {!Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>}
 */
io.changenow.IChangeNow.prototype.GetFixedExchangeAmount = function(data) {}

/**
 * @constructor
 * @param {...!io.changenow.IChangeNow.Initialese} init
 * @implements {io.changenow.IChangeNow}
 * @implements {engineering.type.IInitialiser<!io.changenow.IChangeNow.Initialese>}
 */
io.changenow.ChangeNow = function(...init) {}
/** @param {...!io.changenow.IChangeNow.Initialese} init */
io.changenow.ChangeNow.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.ChangeNow.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.changenow.ChangeNow}
 */
io.changenow.AbstractChangeNow = function() {}
/**
 * @param {...((!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof io.changenow.ChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractChangeNow.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.changenow.AbstractChangeNow}
 */
io.changenow.AbstractChangeNow.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.AbstractChangeNow.__extend = function(...Extensions) {}
/**
 * @param {...((!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.AbstractChangeNow.continues = function(...Implementations) {}
/**
 * @param {...((!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.AbstractChangeNow.__trait = function(...Implementations) {}

/** @interface */
io.changenow.IChangeNowJoinpointModelHyperslice = function() {}
/** @type {(!io.changenow.IChangeNowJoinpointModel._before_GetExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._before_GetExchangeAmount>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.before_GetExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmount>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountThrows|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountThrows>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetExchangeAmountThrows
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountReturns|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountReturns>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetExchangeAmountReturns
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountCancels|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountCancels>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetExchangeAmountCancels
/** @type {(!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetExchangeAmount>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.immediatelyAfter_GetExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel._before_GetFixedExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._before_GetFixedExchangeAmount>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.before_GetFixedExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmount>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetFixedExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountThrows|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountThrows>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetFixedExchangeAmountThrows
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountReturns|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountReturns>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetFixedExchangeAmountReturns
/** @type {(!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountCancels|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountCancels>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.after_GetFixedExchangeAmountCancels
/** @type {(!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetFixedExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetFixedExchangeAmount>)} */
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.immediatelyAfter_GetFixedExchangeAmount

/**
 * @constructor
 * @implements {io.changenow.IChangeNowJoinpointModelHyperslice}
 */
io.changenow.ChangeNowJoinpointModelHyperslice = function() {}

/**
 * @interface
 * @template THIS
 */
io.changenow.IChangeNowJoinpointModelBindingHyperslice = function() {}
/** @type {(!io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.before_GetExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetExchangeAmountThrows
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetExchangeAmountReturns
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetExchangeAmountCancels
/** @type {(!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.immediatelyAfter_GetExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.before_GetFixedExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetFixedExchangeAmount
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetFixedExchangeAmountThrows
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetFixedExchangeAmountReturns
/** @type {(!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.after_GetFixedExchangeAmountCancels
/** @type {(!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount<THIS>>)} */
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.immediatelyAfter_GetFixedExchangeAmount

/**
 * @constructor
 * @implements {io.changenow.IChangeNowJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
io.changenow.ChangeNowJoinpointModelBindingHyperslice = function() {}

/** @interface */
io.changenow.IChangeNowJoinpointModel = function() {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.before_GetExchangeAmount = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmount = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmountThrows = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmountReturns = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmountCancels = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.immediatelyAfter_GetExchangeAmount = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.before_GetFixedExchangeAmount = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmount = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmountThrows = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmountReturns = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmountCancels = function(data) {}
/**
 * @param {!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.prototype.immediatelyAfter_GetFixedExchangeAmount = function(data) {}

/**
 * @constructor
 * @implements {io.changenow.IChangeNowJoinpointModel}
 */
io.changenow.ChangeNowJoinpointModel = function() {}

/** @typedef {{ before_GetExchangeAmount: io.changenow.IChangeNowJoinpointModel.before_GetExchangeAmount, after_GetExchangeAmount: io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmount, after_GetExchangeAmountThrows: io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountThrows, after_GetExchangeAmountReturns: io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountReturns, after_GetExchangeAmountCancels: io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountCancels, immediatelyAfter_GetExchangeAmount: io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetExchangeAmount, before_GetFixedExchangeAmount: io.changenow.IChangeNowJoinpointModel.before_GetFixedExchangeAmount, after_GetFixedExchangeAmount: io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmount, after_GetFixedExchangeAmountThrows: io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountThrows, after_GetFixedExchangeAmountReturns: io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountReturns, after_GetFixedExchangeAmountCancels: io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountCancels, immediatelyAfter_GetFixedExchangeAmount: io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetFixedExchangeAmount }} */
io.changenow.RecordIChangeNowJoinpointModel

/**
 * @record
 * @extends {io.changenow.RecordIChangeNowJoinpointModel}
 */
io.changenow.BoundIChangeNowJoinpointModel = function() {}

/**
 * @record
 * @extends {io.changenow.BoundIChangeNowJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.changenow.BoundChangeNowJoinpointModel = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.before_GetExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._before_GetExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountThrows
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountThrows
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows} */
io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountReturns
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountReturns
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns} */
io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountCancels
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountCancels
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels} */
io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.before_GetFixedExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._before_GetFixedExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountThrows
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountThrows
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows} */
io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountReturns
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountReturns
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns} */
io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountCancels
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountCancels
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels} */
io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData} [data]
 * @return {void}
 */
$$io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetFixedExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNowJoinpointModel, !io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData=): void} */
io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetFixedExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount

/** @record */
io.changenow.IChangeNowAspectsInstaller.Initialese = function() {}

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
io.changenow.IChangeNowAspectsInstaller = function() {}
/** @param {...!io.changenow.IChangeNowAspectsInstaller.Initialese} init */
io.changenow.IChangeNowAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.before_GetExchangeAmount
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetExchangeAmount
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetExchangeAmountThrows
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetExchangeAmountReturns
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetExchangeAmountCancels
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.immediateAfter_GetExchangeAmount
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.before_GetFixedExchangeAmount
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetFixedExchangeAmount
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetFixedExchangeAmountThrows
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetFixedExchangeAmountReturns
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.after_GetFixedExchangeAmountCancels
/** @type {number} */
io.changenow.IChangeNowAspectsInstaller.prototype.immediateAfter_GetFixedExchangeAmount
/** @return {?} */
io.changenow.IChangeNowAspectsInstaller.prototype.GetExchangeAmount = function() {}
/** @return {?} */
io.changenow.IChangeNowAspectsInstaller.prototype.GetFixedExchangeAmount = function() {}

/**
 * @constructor
 * @param {...!io.changenow.IChangeNowAspectsInstaller.Initialese} init
 * @implements {io.changenow.IChangeNowAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!io.changenow.IChangeNowAspectsInstaller.Initialese>}
 */
io.changenow.ChangeNowAspectsInstaller = function(...init) {}
/** @param {...!io.changenow.IChangeNowAspectsInstaller.Initialese} init */
io.changenow.ChangeNowAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.ChangeNowAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller = function() {}
/**
 * @param {...(!io.changenow.IChangeNowAspectsInstaller|typeof io.changenow.ChangeNowAspectsInstaller)} Implementations
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 * @nosideeffects
 */
io.changenow.AbstractChangeNowAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.changenow.AbstractChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!io.changenow.IChangeNowAspectsInstaller|typeof io.changenow.ChangeNowAspectsInstaller)} Implementations
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!io.changenow.IChangeNowAspectsInstaller|typeof io.changenow.ChangeNowAspectsInstaller)} Implementations
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {function(new: io.changenow.IChangeNowAspectsInstaller, ...!io.changenow.IChangeNowAspectsInstaller.Initialese)} */
io.changenow.ChangeNowAspectsInstallerConstructor

/** @typedef {{ data: !io.changenow.IChangeNow.GetExchangeAmount.Data }} */
io.changenow.IChangeNow.GetExchangeAmountNArgs

/** @typedef {{ ticket: symbol, args: io.changenow.IChangeNow.GetExchangeAmountNArgs, proc: !Function }} */
io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData.prototype.cond
/**
 * @param {io.changenow.IChangeNow.GetExchangeAmountNArgs} args
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>} value
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData = function() {}
/** @type {io.changenow.IChangeNow.GetExchangeAmount.Return} */
io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData.prototype.res

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData = function() {}
/** @type {!Error} */
io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData.prototype.err
/** @return {void} */
io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData = function() {}
/** @type {io.changenow.IChangeNow.GetExchangeAmount.Return} */
io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData.prototype.res
/**
 * @param {(!Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>|io.changenow.IChangeNow.GetExchangeAmount.Return)} value
 * @return {?}
 */
io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData = function() {}
/** @type {!Set<string>} */
io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData.prototype.reasons

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData = function() {}
/** @type {!Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>} */
io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData.prototype.promise

/** @typedef {{ data: !io.changenow.IChangeNow.GetFixedExchangeAmount.Data }} */
io.changenow.IChangeNow.GetFixedExchangeAmountNArgs

/** @typedef {{ ticket: symbol, args: io.changenow.IChangeNow.GetFixedExchangeAmountNArgs, proc: !Function }} */
io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData.prototype.cond
/**
 * @param {io.changenow.IChangeNow.GetFixedExchangeAmountNArgs} args
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>} value
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData = function() {}
/** @type {io.changenow.IChangeNow.GetFixedExchangeAmount.Return} */
io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData.prototype.res

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData = function() {}
/** @type {!Error} */
io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData.prototype.err
/** @return {void} */
io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData = function() {}
/** @type {io.changenow.IChangeNow.GetFixedExchangeAmount.Return} */
io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData.prototype.res
/**
 * @param {(!Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>|io.changenow.IChangeNow.GetFixedExchangeAmount.Return)} value
 * @return {?}
 */
io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData = function() {}
/** @type {!Set<string>} */
io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData.prototype.reasons

/**
 * @record
 * @extends {io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData}
 */
io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData = function() {}
/** @type {!Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>} */
io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData.prototype.promise

/** @typedef {function(new: io.changenow.IChangeNow, ...!io.changenow.IChangeNow.Initialese)} */
io.changenow.ChangeNowConstructor

/** @typedef {symbol} */
io.changenow.ChangeNowMetaUniversal

/** @record */
io.changenow.UChangeNow.Initialese = function() {}
/** @type {io.changenow.IChangeNow|undefined} */
io.changenow.UChangeNow.Initialese.prototype.changeNow

/**
 * @constructor
 * @param {...!io.changenow.UChangeNow.Initialese} init
 * @implements {io.changenow.UChangeNow}
 * @implements {engineering.type.IInitialiser<!io.changenow.UChangeNow.Initialese>}
 */
io.changenow.ChangeNowUniversal = function(...init) {}
/** @param {...!io.changenow.UChangeNow.Initialese} init */
io.changenow.ChangeNowUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.ChangeNowUniversal.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.changenow.ChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal = function() {}
/**
 * @param {...(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations
 * @return {typeof io.changenow.ChangeNowUniversal}
 * @nosideeffects
 */
io.changenow.AbstractChangeNowUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.changenow.AbstractChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.__trait = function(...Implementations) {}

/** @typedef {io.changenow.ChangeNowMetaUniversal} */
io.changenow.AbstractChangeNowUniversal.MetaUniversal

/** @typedef {function(new: io.changenow.UChangeNow, ...!io.changenow.UChangeNow.Initialese)} */
io.changenow.UChangeNowConstructor

/** @typedef {typeof __$te_plain} */
io.changenow.RecordUChangeNow

/**
 * @record
 * @extends {io.changenow.UChangeNowFields}
 * @extends {io.changenow.RecordUChangeNow}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {io.changenow.UChangeNowCaster}
 */
io.changenow.BoundUChangeNow = function() {}

/**
 * @record
 * @extends {io.changenow.BoundUChangeNow}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.changenow.BoundChangeNowUniversal = function() {}

/**
 * @interface
 * @template THIS
 */
io.changenow.BChangeNowAspectsCaster
/** @type {!io.changenow.BoundIChangeNow} */
io.changenow.BChangeNowAspectsCaster.prototype.asIChangeNow

/**
 * @interface
 * @extends {io.changenow.BChangeNowAspectsCaster<THIS>}
 * @extends {io.changenow.IChangeNowJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
io.changenow.BChangeNowAspects = function() {}

/** @record */
io.changenow.IChangeNowAspects.Initialese = function() {}

/** @interface */
io.changenow.IChangeNowAspectsCaster
/** @type {!io.changenow.BoundIChangeNow} */
io.changenow.IChangeNowAspectsCaster.prototype.asIChangeNow

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {io.changenow.IChangeNowAspectsCaster}
 * @extends {io.changenow.BChangeNowAspects<!io.changenow.IChangeNowAspects>}
 */
io.changenow.IChangeNowAspects = function() {}
/** @param {...!io.changenow.IChangeNowAspects.Initialese} init */
io.changenow.IChangeNowAspects.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!io.changenow.IChangeNowAspects.Initialese} init
 * @implements {io.changenow.IChangeNowAspects}
 * @implements {engineering.type.IInitialiser<!io.changenow.IChangeNowAspects.Initialese>}
 */
io.changenow.ChangeNowAspects = function(...init) {}
/** @param {...!io.changenow.IChangeNowAspects.Initialese} init */
io.changenow.ChangeNowAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.ChangeNowAspects.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.changenow.ChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects = function() {}
/**
 * @param {...((!io.changenow.IChangeNowAspects|typeof io.changenow.ChangeNowAspects)|(!io.changenow.BChangeNowAspects|typeof io.changenow.BChangeNowAspects))} Implementations
 * @return {typeof io.changenow.ChangeNowAspects}
 * @nosideeffects
 */
io.changenow.AbstractChangeNowAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.changenow.AbstractChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!io.changenow.IChangeNowAspects|typeof io.changenow.ChangeNowAspects)|(!io.changenow.BChangeNowAspects|typeof io.changenow.BChangeNowAspects))} Implementations
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.continues = function(...Implementations) {}
/**
 * @param {...((!io.changenow.IChangeNowAspects|typeof io.changenow.ChangeNowAspects)|(!io.changenow.BChangeNowAspects|typeof io.changenow.BChangeNowAspects))} Implementations
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.__trait = function(...Implementations) {}

/** @typedef {function(new: io.changenow.IChangeNowAspects, ...!io.changenow.IChangeNowAspects.Initialese)} */
io.changenow.ChangeNowAspectsConstructor

/**
 * @record
 * @extends {io.changenow.IChangeNow.Initialese}
 */
io.changenow.IHyperChangeNow.Initialese = function() {}

/** @interface */
io.changenow.IHyperChangeNowCaster
/** @type {!io.changenow.BoundIHyperChangeNow} */
io.changenow.IHyperChangeNowCaster.prototype.asIHyperChangeNow
/** @type {!io.changenow.BoundHyperChangeNow} */
io.changenow.IHyperChangeNowCaster.prototype.superHyperChangeNow

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {io.changenow.IHyperChangeNowCaster}
 * @extends {io.changenow.IChangeNow}
 */
io.changenow.IHyperChangeNow = function() {}
/** @param {...!io.changenow.IHyperChangeNow.Initialese} init */
io.changenow.IHyperChangeNow.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!io.changenow.IHyperChangeNow.Initialese} init
 * @implements {io.changenow.IHyperChangeNow}
 * @implements {engineering.type.IInitialiser<!io.changenow.IHyperChangeNow.Initialese>}
 */
io.changenow.HyperChangeNow = function(...init) {}
/** @param {...!io.changenow.IHyperChangeNow.Initialese} init */
io.changenow.HyperChangeNow.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.HyperChangeNow.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.changenow.HyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow = function() {}
/**
 * @param {...((!io.changenow.IHyperChangeNow|typeof io.changenow.HyperChangeNow)|(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow))} Implementations
 * @return {typeof io.changenow.HyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.__extend = function(...Extensions) {}
/**
 * @param {...((!io.changenow.IHyperChangeNow|typeof io.changenow.HyperChangeNow)|(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow))} Implementations
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.continues = function(...Implementations) {}
/**
 * @param {...((!io.changenow.IHyperChangeNow|typeof io.changenow.HyperChangeNow)|(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow))} Implementations
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.__trait = function(...Implementations) {}
/**
 * @param {...(!io.changenow.IChangeNowAspects|!Array<!io.changenow.IChangeNowAspects>|function(new: io.changenow.IChangeNowAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.installs = function(...aspectsInstallers) {}

/** @typedef {function(new: io.changenow.IHyperChangeNow, ...!io.changenow.IHyperChangeNow.Initialese)} */
io.changenow.HyperChangeNowConstructor

/** @typedef {typeof __$te_plain} */
io.changenow.RecordIHyperChangeNow

/**
 * @record
 * @extends {io.changenow.RecordIHyperChangeNow}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {io.changenow.IHyperChangeNowCaster}
 */
io.changenow.BoundIHyperChangeNow = function() {}

/**
 * @record
 * @extends {io.changenow.BoundIHyperChangeNow}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.changenow.BoundHyperChangeNow = function() {}

/** @typedef {{ GetExchangeAmount: io.changenow.IChangeNow.GetExchangeAmount, GetFixedExchangeAmount: io.changenow.IChangeNow.GetFixedExchangeAmount }} */
io.changenow.RecordIChangeNow

/**
 * @record
 * @extends {io.changenow.IChangeNowFields}
 * @extends {io.changenow.RecordIChangeNow}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {io.changenow.IChangeNowCaster}
 * @extends {engineer.type.BoundINetwork}
 */
io.changenow.BoundIChangeNow = function() {}

/**
 * @record
 * @extends {io.changenow.BoundIChangeNow}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.changenow.BoundChangeNow = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNow.GetExchangeAmount.Data} data
 * @return {!Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>}
 */
$$io.changenow.IChangeNow.__GetExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNow.GetExchangeAmount.Data): !Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>} */
io.changenow.IChangeNow.GetExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNow, !io.changenow.IChangeNow.GetExchangeAmount.Data): !Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>} */
io.changenow.IChangeNow._GetExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNow.__GetExchangeAmount} */
io.changenow.IChangeNow.__GetExchangeAmount

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.changenow.IChangeNow.GetFixedExchangeAmount.Data} data
 * @return {!Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>}
 */
$$io.changenow.IChangeNow.__GetFixedExchangeAmount = function(data) {}
/** @typedef {function(!io.changenow.IChangeNow.GetFixedExchangeAmount.Data): !Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>} */
io.changenow.IChangeNow.GetFixedExchangeAmount
/** @typedef {function(this: io.changenow.IChangeNow, !io.changenow.IChangeNow.GetFixedExchangeAmount.Data): !Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>} */
io.changenow.IChangeNow._GetFixedExchangeAmount
/** @typedef {typeof $$io.changenow.IChangeNow.__GetFixedExchangeAmount} */
io.changenow.IChangeNow.__GetFixedExchangeAmount

/** @record */
io.changenow.IChangeNow.GetExchangeAmount.Data = function() {}
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Data.prototype.fromCurrency
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Data.prototype.toCurrency
/** @type {string|undefined} */
io.changenow.IChangeNow.GetExchangeAmount.Data.prototype.fromNetwork
/** @type {string|undefined} */
io.changenow.IChangeNow.GetExchangeAmount.Data.prototype.toNetwork
/** @type {number} */
io.changenow.IChangeNow.GetExchangeAmount.Data.prototype.fromAmount
/** @type {number|undefined} */
io.changenow.IChangeNow.GetExchangeAmount.Data.prototype.toAmount

/** @record */
io.changenow.IChangeNow.GetExchangeAmount.Return = function() {}
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.fromCurrency
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.fromNetwork
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.toCurrency
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.toNetwork
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.type
/** @type {string} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.transactionSpeedForecast
/** @type {?*} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.warningMessage
/** @type {number} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.depositFee
/** @type {number} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.withdrawalFee
/** @type {?*} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.userId
/** @type {number} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.fromAmount
/** @type {number} */
io.changenow.IChangeNow.GetExchangeAmount.Return.prototype.toAmount

/** @record */
io.changenow.IChangeNow.GetFixedExchangeAmount.Data = function() {}
/** @type {string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Data.prototype.fromCurrency
/** @type {string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Data.prototype.toCurrency
/** @type {string|undefined} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Data.prototype.fromNetwork
/** @type {string|undefined} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Data.prototype.toNetwork
/** @type {number} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Data.prototype.fromAmount
/** @type {number} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Data.prototype.toAmount

/** @record */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return = function() {}
/** @type {string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.fromCurrency
/** @type {string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.fromNetwork
/** @type {string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.toCurrency
/** @type {string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.toNetwork
/** @type {string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.type
/** @type {*} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.rateId
/** @type {!Date} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.validUntil
/** @type {?string} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.transactionSpeedForecast
/** @type {?*} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.warningMessage
/** @type {number} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.depositFee
/** @type {number} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.withdrawalFee
/** @type {?*} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.userId
/** @type {number} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.fromAmount
/** @type {number} */
io.changenow.IChangeNow.GetFixedExchangeAmount.Return.prototype.toAmount

/**
 * @record
 * @extends {engineer.type.INetwork.Initialese}
 */
io.letsexchange.ILetsExchange.Initialese = function() {}
/** @type {string|undefined} */
io.letsexchange.ILetsExchange.Initialese.prototype.host
/** @type {string|undefined} */
io.letsexchange.ILetsExchange.Initialese.prototype.apiPath
/** @type {string|undefined} */
io.letsexchange.ILetsExchange.Initialese.prototype.letsExchangeApiKey

/** @interface */
io.letsexchange.ILetsExchangeFields
/** @type {string} */
io.letsexchange.ILetsExchangeFields.prototype.host
/** @type {string} */
io.letsexchange.ILetsExchangeFields.prototype.apiPath
/** @type {string} */
io.letsexchange.ILetsExchangeFields.prototype.letsExchangeApiKey

/** @interface */
io.letsexchange.ILetsExchangeCaster
/** @type {!io.letsexchange.BoundILetsExchange} */
io.letsexchange.ILetsExchangeCaster.prototype.asILetsExchange
/** @type {!io.letsexchange.BoundLetsExchange} */
io.letsexchange.ILetsExchangeCaster.prototype.superLetsExchange

/** @interface */
io.letsexchange.ULetsExchangeFields
/** @type {io.letsexchange.ILetsExchange} */
io.letsexchange.ULetsExchangeFields.prototype.letsExchange

/** @interface */
io.letsexchange.ULetsExchangeCaster
/** @type {!io.letsexchange.BoundLetsExchange} */
io.letsexchange.ULetsExchangeCaster.prototype.asLetsExchange
/** @type {!io.letsexchange.BoundULetsExchange} */
io.letsexchange.ULetsExchangeCaster.prototype.asULetsExchange
/** @type {!io.letsexchange.BoundLetsExchangeUniversal} */
io.letsexchange.ULetsExchangeCaster.prototype.superLetsExchangeUniversal

/**
 * @interface
 * @extends {io.letsexchange.ULetsExchangeFields}
 * @extends {engineering.type.IEngineer}
 * @extends {io.letsexchange.ULetsExchangeCaster}
 */
io.letsexchange.ULetsExchange = function() {}
/** @param {...!io.letsexchange.ULetsExchange.Initialese} init */
io.letsexchange.ULetsExchange.prototype.constructor = function(...init) {}

/**
 * @interface
 * @extends {io.letsexchange.ILetsExchangeFields}
 * @extends {engineering.type.IEngineer}
 * @extends {io.letsexchange.ILetsExchangeCaster}
 * @extends {engineer.type.INetwork}
 * @extends {io.letsexchange.ULetsExchange}
 */
io.letsexchange.ILetsExchange = function() {}
/** @param {...!io.letsexchange.ILetsExchange.Initialese} init */
io.letsexchange.ILetsExchange.prototype.constructor = function(...init) {}
/**
 * @param {!io.letsexchange.ILetsExchange.GetInfo.Data} data
 * @return {!Promise<io.letsexchange.ILetsExchange.GetInfo.Return>}
 */
io.letsexchange.ILetsExchange.prototype.GetInfo = function(data) {}

/**
 * @constructor
 * @param {...!io.letsexchange.ILetsExchange.Initialese} init
 * @implements {io.letsexchange.ILetsExchange}
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ILetsExchange.Initialese>}
 */
io.letsexchange.LetsExchange = function(...init) {}
/** @param {...!io.letsexchange.ILetsExchange.Initialese} init */
io.letsexchange.LetsExchange.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.LetsExchange.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.letsexchange.LetsExchange}
 */
io.letsexchange.AbstractLetsExchange = function() {}
/**
 * @param {...((!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof io.letsexchange.LetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchange.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.letsexchange.AbstractLetsExchange}
 */
io.letsexchange.AbstractLetsExchange.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.AbstractLetsExchange.__extend = function(...Extensions) {}
/**
 * @param {...((!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.AbstractLetsExchange.continues = function(...Implementations) {}
/**
 * @param {...((!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.AbstractLetsExchange.__trait = function(...Implementations) {}

/** @interface */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice = function() {}
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel._before_GetInfo|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._before_GetInfo>)} */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice.prototype.before_GetInfo
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfo|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfo>)} */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice.prototype.after_GetInfo
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoThrows|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoThrows>)} */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice.prototype.after_GetInfoThrows
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoReturns|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoReturns>)} */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice.prototype.after_GetInfoReturns
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoCancels|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoCancels>)} */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice.prototype.after_GetInfoCancels
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel._immediatelyAfter_GetInfo|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._immediatelyAfter_GetInfo>)} */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice.prototype.immediatelyAfter_GetInfo

/**
 * @constructor
 * @implements {io.letsexchange.ILetsExchangeJoinpointModelHyperslice}
 */
io.letsexchange.LetsExchangeJoinpointModelHyperslice = function() {}

/**
 * @interface
 * @template THIS
 */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice = function() {}
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo<THIS>>)} */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.prototype.before_GetInfo
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo<THIS>>)} */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.prototype.after_GetInfo
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows<THIS>>)} */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.prototype.after_GetInfoThrows
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns<THIS>>)} */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.prototype.after_GetInfoReturns
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels<THIS>>)} */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.prototype.after_GetInfoCancels
/** @type {(!io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo<THIS>>)} */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.prototype.immediatelyAfter_GetInfo

/**
 * @constructor
 * @implements {io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
io.letsexchange.LetsExchangeJoinpointModelBindingHyperslice = function() {}

/** @interface */
io.letsexchange.ILetsExchangeJoinpointModel = function() {}
/**
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData} [data]
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.before_GetInfo = function(data) {}
/**
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData} [data]
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfo = function(data) {}
/**
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData} [data]
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfoThrows = function(data) {}
/**
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData} [data]
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfoReturns = function(data) {}
/**
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData} [data]
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfoCancels = function(data) {}
/**
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData} [data]
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.immediatelyAfter_GetInfo = function(data) {}

/**
 * @constructor
 * @implements {io.letsexchange.ILetsExchangeJoinpointModel}
 */
io.letsexchange.LetsExchangeJoinpointModel = function() {}

/** @typedef {{ before_GetInfo: io.letsexchange.ILetsExchangeJoinpointModel.before_GetInfo, after_GetInfo: io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfo, after_GetInfoThrows: io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoThrows, after_GetInfoReturns: io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoReturns, after_GetInfoCancels: io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoCancels, immediatelyAfter_GetInfo: io.letsexchange.ILetsExchangeJoinpointModel.immediatelyAfter_GetInfo }} */
io.letsexchange.RecordILetsExchangeJoinpointModel

/**
 * @record
 * @extends {io.letsexchange.RecordILetsExchangeJoinpointModel}
 */
io.letsexchange.BoundILetsExchangeJoinpointModel = function() {}

/**
 * @record
 * @extends {io.letsexchange.BoundILetsExchangeJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.letsexchange.BoundLetsExchangeJoinpointModel = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData} [data]
 * @return {void}
 */
$$io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo = function(data) {}
/** @typedef {function(!io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel.before_GetInfo
/** @typedef {function(this: io.letsexchange.ILetsExchangeJoinpointModel, !io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel._before_GetInfo
/** @typedef {typeof $$io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo} */
io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData} [data]
 * @return {void}
 */
$$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo = function(data) {}
/** @typedef {function(!io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfo
/** @typedef {function(this: io.letsexchange.ILetsExchangeJoinpointModel, !io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfo
/** @typedef {typeof $$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo} */
io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData} [data]
 * @return {void}
 */
$$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows = function(data) {}
/** @typedef {function(!io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoThrows
/** @typedef {function(this: io.letsexchange.ILetsExchangeJoinpointModel, !io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoThrows
/** @typedef {typeof $$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows} */
io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData} [data]
 * @return {void}
 */
$$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns = function(data) {}
/** @typedef {function(!io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoReturns
/** @typedef {function(this: io.letsexchange.ILetsExchangeJoinpointModel, !io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoReturns
/** @typedef {typeof $$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns} */
io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData} [data]
 * @return {void}
 */
$$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels = function(data) {}
/** @typedef {function(!io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoCancels
/** @typedef {function(this: io.letsexchange.ILetsExchangeJoinpointModel, !io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoCancels
/** @typedef {typeof $$io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels} */
io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData} [data]
 * @return {void}
 */
$$io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo = function(data) {}
/** @typedef {function(!io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel.immediatelyAfter_GetInfo
/** @typedef {function(this: io.letsexchange.ILetsExchangeJoinpointModel, !io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData=): void} */
io.letsexchange.ILetsExchangeJoinpointModel._immediatelyAfter_GetInfo
/** @typedef {typeof $$io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo} */
io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo

/** @record */
io.letsexchange.ILetsExchangeAspectsInstaller.Initialese = function() {}

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
io.letsexchange.ILetsExchangeAspectsInstaller = function() {}
/** @param {...!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese} init */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.before_GetInfo
/** @type {number} */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.after_GetInfo
/** @type {number} */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.after_GetInfoThrows
/** @type {number} */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.after_GetInfoReturns
/** @type {number} */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.after_GetInfoCancels
/** @type {number} */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.immediateAfter_GetInfo
/** @return {?} */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.GetInfo = function() {}

/**
 * @constructor
 * @param {...!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese} init
 * @implements {io.letsexchange.ILetsExchangeAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese>}
 */
io.letsexchange.LetsExchangeAspectsInstaller = function(...init) {}
/** @param {...!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese} init */
io.letsexchange.LetsExchangeAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.LetsExchangeAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller = function() {}
/**
 * @param {...(!io.letsexchange.ILetsExchangeAspectsInstaller|typeof io.letsexchange.LetsExchangeAspectsInstaller)} Implementations
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.letsexchange.AbstractLetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!io.letsexchange.ILetsExchangeAspectsInstaller|typeof io.letsexchange.LetsExchangeAspectsInstaller)} Implementations
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!io.letsexchange.ILetsExchangeAspectsInstaller|typeof io.letsexchange.LetsExchangeAspectsInstaller)} Implementations
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {function(new: io.letsexchange.ILetsExchangeAspectsInstaller, ...!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese)} */
io.letsexchange.LetsExchangeAspectsInstallerConstructor

/** @typedef {{ data: !io.letsexchange.ILetsExchange.GetInfo.Data }} */
io.letsexchange.ILetsExchange.GetInfoNArgs

/** @typedef {{ ticket: symbol, args: io.letsexchange.ILetsExchange.GetInfoNArgs, proc: !Function }} */
io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData

/**
 * @record
 * @extends {io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData}
 */
io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData.prototype.cond
/**
 * @param {io.letsexchange.ILetsExchange.GetInfoNArgs} args
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<io.letsexchange.ILetsExchange.GetInfo.Return>} value
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData}
 */
io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData = function() {}
/** @type {io.letsexchange.ILetsExchange.GetInfo.Return} */
io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData.prototype.res

/**
 * @record
 * @extends {io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData}
 */
io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData = function() {}
/** @type {!Error} */
io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData.prototype.err
/** @return {void} */
io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData}
 */
io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData = function() {}
/** @type {io.letsexchange.ILetsExchange.GetInfo.Return} */
io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData.prototype.res
/**
 * @param {(!Promise<io.letsexchange.ILetsExchange.GetInfo.Return>|io.letsexchange.ILetsExchange.GetInfo.Return)} value
 * @return {?}
 */
io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData}
 */
io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData = function() {}
/** @type {!Set<string>} */
io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData.prototype.reasons

/**
 * @record
 * @extends {io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData}
 */
io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData = function() {}
/** @type {!Promise<io.letsexchange.ILetsExchange.GetInfo.Return>} */
io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData.prototype.promise

/** @typedef {function(new: io.letsexchange.ILetsExchange, ...!io.letsexchange.ILetsExchange.Initialese)} */
io.letsexchange.LetsExchangeConstructor

/** @typedef {symbol} */
io.letsexchange.LetsExchangeMetaUniversal

/** @record */
io.letsexchange.ULetsExchange.Initialese = function() {}
/** @type {io.letsexchange.ILetsExchange|undefined} */
io.letsexchange.ULetsExchange.Initialese.prototype.letsExchange

/**
 * @constructor
 * @param {...!io.letsexchange.ULetsExchange.Initialese} init
 * @implements {io.letsexchange.ULetsExchange}
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ULetsExchange.Initialese>}
 */
io.letsexchange.LetsExchangeUniversal = function(...init) {}
/** @param {...!io.letsexchange.ULetsExchange.Initialese} init */
io.letsexchange.LetsExchangeUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.LetsExchangeUniversal.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal = function() {}
/**
 * @param {...(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchangeUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.letsexchange.AbstractLetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.__trait = function(...Implementations) {}

/** @typedef {io.letsexchange.LetsExchangeMetaUniversal} */
io.letsexchange.AbstractLetsExchangeUniversal.MetaUniversal

/** @typedef {function(new: io.letsexchange.ULetsExchange, ...!io.letsexchange.ULetsExchange.Initialese)} */
io.letsexchange.ULetsExchangeConstructor

/** @typedef {typeof __$te_plain} */
io.letsexchange.RecordULetsExchange

/**
 * @record
 * @extends {io.letsexchange.ULetsExchangeFields}
 * @extends {io.letsexchange.RecordULetsExchange}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {io.letsexchange.ULetsExchangeCaster}
 */
io.letsexchange.BoundULetsExchange = function() {}

/**
 * @record
 * @extends {io.letsexchange.BoundULetsExchange}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.letsexchange.BoundLetsExchangeUniversal = function() {}

/**
 * @interface
 * @template THIS
 */
io.letsexchange.BLetsExchangeAspectsCaster
/** @type {!io.letsexchange.BoundILetsExchange} */
io.letsexchange.BLetsExchangeAspectsCaster.prototype.asILetsExchange

/**
 * @interface
 * @extends {io.letsexchange.BLetsExchangeAspectsCaster<THIS>}
 * @extends {io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
io.letsexchange.BLetsExchangeAspects = function() {}

/** @record */
io.letsexchange.ILetsExchangeAspects.Initialese = function() {}

/** @interface */
io.letsexchange.ILetsExchangeAspectsCaster
/** @type {!io.letsexchange.BoundILetsExchange} */
io.letsexchange.ILetsExchangeAspectsCaster.prototype.asILetsExchange

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {io.letsexchange.ILetsExchangeAspectsCaster}
 * @extends {io.letsexchange.BLetsExchangeAspects<!io.letsexchange.ILetsExchangeAspects>}
 */
io.letsexchange.ILetsExchangeAspects = function() {}
/** @param {...!io.letsexchange.ILetsExchangeAspects.Initialese} init */
io.letsexchange.ILetsExchangeAspects.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!io.letsexchange.ILetsExchangeAspects.Initialese} init
 * @implements {io.letsexchange.ILetsExchangeAspects}
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ILetsExchangeAspects.Initialese>}
 */
io.letsexchange.LetsExchangeAspects = function(...init) {}
/** @param {...!io.letsexchange.ILetsExchangeAspects.Initialese} init */
io.letsexchange.LetsExchangeAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.LetsExchangeAspects.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects = function() {}
/**
 * @param {...((!io.letsexchange.ILetsExchangeAspects|typeof io.letsexchange.LetsExchangeAspects)|(!io.letsexchange.BLetsExchangeAspects|typeof io.letsexchange.BLetsExchangeAspects))} Implementations
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchangeAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.letsexchange.AbstractLetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!io.letsexchange.ILetsExchangeAspects|typeof io.letsexchange.LetsExchangeAspects)|(!io.letsexchange.BLetsExchangeAspects|typeof io.letsexchange.BLetsExchangeAspects))} Implementations
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.continues = function(...Implementations) {}
/**
 * @param {...((!io.letsexchange.ILetsExchangeAspects|typeof io.letsexchange.LetsExchangeAspects)|(!io.letsexchange.BLetsExchangeAspects|typeof io.letsexchange.BLetsExchangeAspects))} Implementations
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.__trait = function(...Implementations) {}

/** @typedef {function(new: io.letsexchange.ILetsExchangeAspects, ...!io.letsexchange.ILetsExchangeAspects.Initialese)} */
io.letsexchange.LetsExchangeAspectsConstructor

/**
 * @record
 * @extends {io.letsexchange.ILetsExchange.Initialese}
 */
io.letsexchange.IHyperLetsExchange.Initialese = function() {}

/** @interface */
io.letsexchange.IHyperLetsExchangeCaster
/** @type {!io.letsexchange.BoundIHyperLetsExchange} */
io.letsexchange.IHyperLetsExchangeCaster.prototype.asIHyperLetsExchange
/** @type {!io.letsexchange.BoundHyperLetsExchange} */
io.letsexchange.IHyperLetsExchangeCaster.prototype.superHyperLetsExchange

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {io.letsexchange.IHyperLetsExchangeCaster}
 * @extends {io.letsexchange.ILetsExchange}
 */
io.letsexchange.IHyperLetsExchange = function() {}
/** @param {...!io.letsexchange.IHyperLetsExchange.Initialese} init */
io.letsexchange.IHyperLetsExchange.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!io.letsexchange.IHyperLetsExchange.Initialese} init
 * @implements {io.letsexchange.IHyperLetsExchange}
 * @implements {engineering.type.IInitialiser<!io.letsexchange.IHyperLetsExchange.Initialese>}
 */
io.letsexchange.HyperLetsExchange = function(...init) {}
/** @param {...!io.letsexchange.IHyperLetsExchange.Initialese} init */
io.letsexchange.HyperLetsExchange.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.HyperLetsExchange.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange = function() {}
/**
 * @param {...((!io.letsexchange.IHyperLetsExchange|typeof io.letsexchange.HyperLetsExchange)|(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange))} Implementations
 * @return {typeof io.letsexchange.HyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.__extend = function(...Extensions) {}
/**
 * @param {...((!io.letsexchange.IHyperLetsExchange|typeof io.letsexchange.HyperLetsExchange)|(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange))} Implementations
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.continues = function(...Implementations) {}
/**
 * @param {...((!io.letsexchange.IHyperLetsExchange|typeof io.letsexchange.HyperLetsExchange)|(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange))} Implementations
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.__trait = function(...Implementations) {}
/**
 * @param {...(!io.letsexchange.ILetsExchangeAspects|!Array<!io.letsexchange.ILetsExchangeAspects>|function(new: io.letsexchange.ILetsExchangeAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.installs = function(...aspectsInstallers) {}

/** @typedef {function(new: io.letsexchange.IHyperLetsExchange, ...!io.letsexchange.IHyperLetsExchange.Initialese)} */
io.letsexchange.HyperLetsExchangeConstructor

/** @typedef {typeof __$te_plain} */
io.letsexchange.RecordIHyperLetsExchange

/**
 * @record
 * @extends {io.letsexchange.RecordIHyperLetsExchange}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {io.letsexchange.IHyperLetsExchangeCaster}
 */
io.letsexchange.BoundIHyperLetsExchange = function() {}

/**
 * @record
 * @extends {io.letsexchange.BoundIHyperLetsExchange}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.letsexchange.BoundHyperLetsExchange = function() {}

/** @typedef {{ GetInfo: io.letsexchange.ILetsExchange.GetInfo }} */
io.letsexchange.RecordILetsExchange

/**
 * @record
 * @extends {io.letsexchange.ILetsExchangeFields}
 * @extends {io.letsexchange.RecordILetsExchange}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {io.letsexchange.ILetsExchangeCaster}
 * @extends {engineer.type.BoundINetwork}
 */
io.letsexchange.BoundILetsExchange = function() {}

/**
 * @record
 * @extends {io.letsexchange.BoundILetsExchange}
 * @extends {engineering.type.BoundIInitialiser}
 */
io.letsexchange.BoundLetsExchange = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @param {!io.letsexchange.ILetsExchange.GetInfo.Data} data
 * @return {!Promise<io.letsexchange.ILetsExchange.GetInfo.Return>}
 */
$$io.letsexchange.ILetsExchange.__GetInfo = function(data) {}
/** @typedef {function(!io.letsexchange.ILetsExchange.GetInfo.Data): !Promise<io.letsexchange.ILetsExchange.GetInfo.Return>} */
io.letsexchange.ILetsExchange.GetInfo
/** @typedef {function(this: io.letsexchange.ILetsExchange, !io.letsexchange.ILetsExchange.GetInfo.Data): !Promise<io.letsexchange.ILetsExchange.GetInfo.Return>} */
io.letsexchange.ILetsExchange._GetInfo
/** @typedef {typeof $$io.letsexchange.ILetsExchange.__GetInfo} */
io.letsexchange.ILetsExchange.__GetInfo

/** @record */
io.letsexchange.ILetsExchange.GetInfo.Data = function() {}
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.from
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.to
/** @type {string|undefined} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.networkFrom
/** @type {string|undefined} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.networkTo
/** @type {(string|number)} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.amount
/** @type {string|undefined} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.promocode
/** @type {string|undefined} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.affiliateId
/** @type {boolean|undefined} */
io.letsexchange.ILetsExchange.GetInfo.Data.prototype.float

/** @record */
io.letsexchange.ILetsExchange.GetInfo.Return = function() {}
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.deposit_min_amount
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.deposit_max_amount
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.min_amount
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.max_amount
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.amount
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.fee
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.rate
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.profit
/** @type {number} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.withdrawal_fee
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.extra_fee_amount
/** @type {number} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.rate_id
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.rate_id_expired_at
/** @type {number} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.applied_promo_code_id
/** @type {!Array} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.networks_from
/** @type {!Array} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.networks_to
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.deposit_amount_usdt
/** @type {string} */
io.letsexchange.ILetsExchange.GetInfo.Return.prototype.expired_at

/**
 * @record
 * @extends {xyz.swapee.ISwapee.Initialese}
 */
xyz.swapee.ISwapeeAide.Initialese = function() {}
/** @type {string|undefined} */
xyz.swapee.ISwapeeAide.Initialese.prototype.output
/** @type {boolean|undefined} */
xyz.swapee.ISwapeeAide.Initialese.prototype.showHelp
/** @type {boolean|undefined} */
xyz.swapee.ISwapeeAide.Initialese.prototype.testExchanges
/** @type {boolean|undefined} */
xyz.swapee.ISwapeeAide.Initialese.prototype.showVersion
/** @type {boolean|undefined} */
xyz.swapee.ISwapeeAide.Initialese.prototype.debug
/** @type {string|undefined} */
xyz.swapee.ISwapeeAide.Initialese.prototype.version

/** @interface */
xyz.swapee.ISwapeeAideFields
/** @type {string} */
xyz.swapee.ISwapeeAideFields.prototype.output
/** @type {boolean} */
xyz.swapee.ISwapeeAideFields.prototype.showHelp
/** @type {boolean} */
xyz.swapee.ISwapeeAideFields.prototype.testExchanges
/** @type {boolean} */
xyz.swapee.ISwapeeAideFields.prototype.showVersion
/** @type {boolean} */
xyz.swapee.ISwapeeAideFields.prototype.debug
/** @type {string} */
xyz.swapee.ISwapeeAideFields.prototype.version

/** @interface */
xyz.swapee.ISwapeeAideCaster
/** @type {!xyz.swapee.BoundISwapeeAide} */
xyz.swapee.ISwapeeAideCaster.prototype.asISwapeeAide
/** @type {!xyz.swapee.BoundSwapeeAide} */
xyz.swapee.ISwapeeAideCaster.prototype.superSwapeeAide

/**
 * @interface
 * @extends {xyz.swapee.ISwapeeAideFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.ISwapeeAideCaster}
 * @extends {xyz.swapee.ISwapee}
 */
xyz.swapee.ISwapeeAide = function() {}
/** @param {...!xyz.swapee.ISwapeeAide.Initialese} init */
xyz.swapee.ISwapeeAide.prototype.constructor = function(...init) {}
/** @return {?} */
xyz.swapee.ISwapeeAide.prototype.ShowHelp = function() {}
/** @return {?} */
xyz.swapee.ISwapeeAide.prototype.ShowVersion = function() {}

/**
 * @constructor
 * @param {...!xyz.swapee.ISwapeeAide.Initialese} init
 * @implements {xyz.swapee.ISwapeeAide}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeAide.Initialese>}
 */
xyz.swapee.SwapeeAide = function(...init) {}
/** @param {...!xyz.swapee.ISwapeeAide.Initialese} init */
xyz.swapee.SwapeeAide.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.SwapeeAide.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.SwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide = function() {}
/**
 * @param {...((!xyz.swapee.ISwapeeAide|typeof xyz.swapee.SwapeeAide)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee))} Implementations
 * @return {typeof xyz.swapee.SwapeeAide}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeAide.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractSwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.ISwapeeAide|typeof xyz.swapee.SwapeeAide)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee))} Implementations
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.ISwapeeAide|typeof xyz.swapee.SwapeeAide)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee))} Implementations
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.ISwapeeAide, ...!xyz.swapee.ISwapeeAide.Initialese)} */
xyz.swapee.SwapeeAideConstructor

/** @typedef {{ ShowHelp: xyz.swapee.ISwapeeAide.ShowHelp, ShowVersion: xyz.swapee.ISwapeeAide.ShowVersion }} */
xyz.swapee.RecordISwapeeAide

/**
 * @record
 * @extends {xyz.swapee.ISwapeeAideFields}
 * @extends {xyz.swapee.RecordISwapeeAide}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.ISwapeeAideCaster}
 * @extends {xyz.swapee.BoundISwapee}
 */
xyz.swapee.BoundISwapeeAide = function() {}

/**
 * @record
 * @extends {xyz.swapee.BoundISwapeeAide}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundSwapeeAide = function() {}

/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.ISwapeeAide.__ShowHelp = function() {}
/** @typedef {function()} */
xyz.swapee.ISwapeeAide.ShowHelp
/** @typedef {function(this: xyz.swapee.ISwapeeAide)} */
xyz.swapee.ISwapeeAide._ShowHelp
/** @typedef {typeof $$xyz.swapee.ISwapeeAide.__ShowHelp} */
xyz.swapee.ISwapeeAide.__ShowHelp

/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.ISwapeeAide.__ShowVersion = function() {}
/** @typedef {function()} */
xyz.swapee.ISwapeeAide.ShowVersion
/** @typedef {function(this: xyz.swapee.ISwapeeAide)} */
xyz.swapee.ISwapeeAide._ShowVersion
/** @typedef {typeof $$xyz.swapee.ISwapeeAide.__ShowVersion} */
xyz.swapee.ISwapeeAide.__ShowVersion

/** @record */
xyz.swapee.ISwapee.Initialese = function() {}
/** @type {string|undefined} */
xyz.swapee.ISwapee.Initialese.prototype.text

/** @interface */
xyz.swapee.ISwapeeFields
/** @type {string} */
xyz.swapee.ISwapeeFields.prototype.example
/** @type {string} */
xyz.swapee.ISwapeeFields.prototype.text

/** @interface */
xyz.swapee.ISwapeeCaster
/** @type {!xyz.swapee.BoundISwapee} */
xyz.swapee.ISwapeeCaster.prototype.asISwapee
/** @type {!xyz.swapee.BoundSwapee} */
xyz.swapee.ISwapeeCaster.prototype.superSwapee

/**
 * @interface
 * @extends {xyz.swapee.ISwapeeFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.ISwapeeCaster}
 */
xyz.swapee.ISwapee = function() {}
/** @param {...!xyz.swapee.ISwapee.Initialese} init */
xyz.swapee.ISwapee.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.swapee_xyz.Config} [conf]
 * @return {!Promise<string>}
 */
xyz.swapee.ISwapee.prototype.run = function(conf) {}
/**
 * @param {!xyz.swapee.ISwapee.ShowText.Conf} conf
 * @return {!Promise<string>}
 */
xyz.swapee.ISwapee.prototype.ShowText = function(conf) {}
/**
 * @param {!xyz.swapee.ISwapee.connectExchanges.Opts} [opts]
 * @return {!Promise<xyz.swapee.ISwapee.connectExchanges.Return>}
 */
xyz.swapee.ISwapee.prototype.connectExchanges = function(opts) {}

/**
 * @constructor
 * @param {...!xyz.swapee.ISwapee.Initialese} init
 * @implements {xyz.swapee.ISwapee}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapee.Initialese>}
 */
xyz.swapee.Swapee = function(...init) {}
/** @param {...!xyz.swapee.ISwapee.Initialese} init */
xyz.swapee.Swapee.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.Swapee.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.Swapee}
 */
xyz.swapee.AbstractSwapee = function() {}
/**
 * @param {...((!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|!xyz.swapee.ISwapeeHyperslice)} Implementations
 * @return {typeof xyz.swapee.Swapee}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapee.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractSwapee}
 */
xyz.swapee.AbstractSwapee.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.AbstractSwapee.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|!xyz.swapee.ISwapeeHyperslice)} Implementations
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.AbstractSwapee.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|!xyz.swapee.ISwapeeHyperslice)} Implementations
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.AbstractSwapee.__trait = function(...Implementations) {}

/** @interface */
xyz.swapee.ISwapeeJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._beforeRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeRun>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.beforeRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRun>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterRunThrows|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRunThrows>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterRunThrows
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterRunReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRunReturns>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterRunReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterRunCancels|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRunCancels>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterRunCancels
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._beforeEachRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeEachRun>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.beforeEachRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterEachRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachRun>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterEachRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterEachRunReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachRunReturns>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterEachRunReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterRun>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.immediatelyAfterRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._before_ShowText|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._before_ShowText>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.before_ShowText
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._after_ShowText|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowText>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.after_ShowText
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextThrows|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextThrows>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.after_ShowTextThrows
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextReturns>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.after_ShowTextReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextCancels|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextCancels>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.after_ShowTextCancels
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfter_ShowText|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfter_ShowText>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.immediatelyAfter_ShowText
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._beforeConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeConnectExchanges>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.beforeConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchanges>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesThrows|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesThrows>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterConnectExchangesThrows
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesReturns>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterConnectExchangesReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesCancels|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesCancels>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterConnectExchangesCancels
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._beforeEachConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeEachConnectExchanges>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.beforeEachConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchanges>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterEachConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchangesReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchangesReturns>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.afterEachConnectExchangesReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterConnectExchanges>)} */
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.immediatelyAfterConnectExchanges

/**
 * @constructor
 * @implements {xyz.swapee.ISwapeeJoinpointModelHyperslice}
 */
xyz.swapee.SwapeeJoinpointModelHyperslice = function() {}

/**
 * @interface
 * @template THIS
 */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__beforeRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeRun<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.beforeRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRun<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterRunThrows
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterRunReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterRunCancels
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.beforeEachRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterEachRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachRun<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterEachRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterEachRunReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.immediatelyAfterRun
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__before_ShowText<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__before_ShowText<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.before_ShowText
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__after_ShowText<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowText<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.after_ShowText
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.after_ShowTextThrows
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.after_ShowTextReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.after_ShowTextCancels
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.immediatelyAfter_ShowText
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.beforeConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterConnectExchangesThrows
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterConnectExchangesReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterConnectExchangesCancels
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.beforeEachConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterEachConnectExchanges
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.afterEachConnectExchangesReturns
/** @type {(!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges<THIS>>)} */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.immediatelyAfterConnectExchanges

/**
 * @constructor
 * @implements {xyz.swapee.ISwapeeJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.SwapeeJoinpointModelBindingHyperslice = function() {}

/** @interface */
xyz.swapee.ISwapeeJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeRun = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRun = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRunThrows = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRunReturns = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRunCancels = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeEachRun = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachRun = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachRunReturns = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.immediatelyAfterRun = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.before_ShowText = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowText = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowTextThrows = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowTextReturns = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowTextCancels = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.immediatelyAfter_ShowText = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeConnectExchanges = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchanges = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchangesThrows = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchangesReturns = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchangesCancels = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeEachConnectExchanges = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachConnectExchanges = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachConnectExchangesReturns = function(data) {}
/**
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.prototype.immediatelyAfterConnectExchanges = function(data) {}

/**
 * @constructor
 * @implements {xyz.swapee.ISwapeeJoinpointModel}
 */
xyz.swapee.SwapeeJoinpointModel = function() {}

/** @typedef {{ beforeRun: xyz.swapee.ISwapeeJoinpointModel.beforeRun, afterRun: xyz.swapee.ISwapeeJoinpointModel.afterRun, afterRunThrows: xyz.swapee.ISwapeeJoinpointModel.afterRunThrows, afterRunReturns: xyz.swapee.ISwapeeJoinpointModel.afterRunReturns, afterRunCancels: xyz.swapee.ISwapeeJoinpointModel.afterRunCancels, beforeEachRun: xyz.swapee.ISwapeeJoinpointModel.beforeEachRun, afterEachRun: xyz.swapee.ISwapeeJoinpointModel.afterEachRun, afterEachRunReturns: xyz.swapee.ISwapeeJoinpointModel.afterEachRunReturns, immediatelyAfterRun: xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterRun, before_ShowText: xyz.swapee.ISwapeeJoinpointModel.before_ShowText, after_ShowText: xyz.swapee.ISwapeeJoinpointModel.after_ShowText, after_ShowTextThrows: xyz.swapee.ISwapeeJoinpointModel.after_ShowTextThrows, after_ShowTextReturns: xyz.swapee.ISwapeeJoinpointModel.after_ShowTextReturns, after_ShowTextCancels: xyz.swapee.ISwapeeJoinpointModel.after_ShowTextCancels, immediatelyAfter_ShowText: xyz.swapee.ISwapeeJoinpointModel.immediatelyAfter_ShowText, beforeConnectExchanges: xyz.swapee.ISwapeeJoinpointModel.beforeConnectExchanges, afterConnectExchanges: xyz.swapee.ISwapeeJoinpointModel.afterConnectExchanges, afterConnectExchangesThrows: xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesThrows, afterConnectExchangesReturns: xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesReturns, afterConnectExchangesCancels: xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesCancels, beforeEachConnectExchanges: xyz.swapee.ISwapeeJoinpointModel.beforeEachConnectExchanges, afterEachConnectExchanges: xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchanges, afterEachConnectExchangesReturns: xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchangesReturns, immediatelyAfterConnectExchanges: xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterConnectExchanges }} */
xyz.swapee.RecordISwapeeJoinpointModel

/**
 * @record
 * @extends {xyz.swapee.RecordISwapeeJoinpointModel}
 */
xyz.swapee.BoundISwapeeJoinpointModel = function() {}

/**
 * @record
 * @extends {xyz.swapee.BoundISwapeeJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundSwapeeJoinpointModel = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__beforeRun = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.beforeRun
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._beforeRun
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__beforeRun} */
xyz.swapee.ISwapeeJoinpointModel.__beforeRun

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterRun = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterRun
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterRun
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterRun} */
xyz.swapee.ISwapeeJoinpointModel.__afterRun

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterRunThrows
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterRunThrows
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows} */
xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterRunReturns
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterRunReturns
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns} */
xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterRunCancels
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterRunCancels
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels} */
xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.beforeEachRun
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._beforeEachRun
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun} */
xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterEachRun = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterEachRun
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterEachRun
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterEachRun} */
xyz.swapee.ISwapeeJoinpointModel.__afterEachRun

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterEachRunReturns
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterEachRunReturns
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns} */
xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterRun
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterRun
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun} */
xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__before_ShowText = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.before_ShowText
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._before_ShowText
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__before_ShowText} */
xyz.swapee.ISwapeeJoinpointModel.__before_ShowText

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__after_ShowText = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.after_ShowText
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._after_ShowText
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__after_ShowText} */
xyz.swapee.ISwapeeJoinpointModel.__after_ShowText

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.after_ShowTextThrows
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._after_ShowTextThrows
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows} */
xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.after_ShowTextReturns
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._after_ShowTextReturns
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns} */
xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.after_ShowTextCancels
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._after_ShowTextCancels
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels} */
xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.immediatelyAfter_ShowText
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._immediatelyAfter_ShowText
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText} */
xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.beforeConnectExchanges
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._beforeConnectExchanges
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchanges
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterConnectExchanges
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesThrows
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesThrows
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows} */
xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesReturns
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesReturns
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns} */
xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesCancels
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesCancels
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels} */
xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.beforeEachConnectExchanges
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._beforeEachConnectExchanges
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchanges
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchanges
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchangesReturns
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchangesReturns
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns} */
xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges = function(data) {}
/** @typedef {function(!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterConnectExchanges
/** @typedef {function(this: xyz.swapee.ISwapeeJoinpointModel, !xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData=): void} */
xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterConnectExchanges
/** @typedef {typeof $$xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges

/** @record */
xyz.swapee.ISwapeeAspectsInstaller.Initialese = function() {}

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
xyz.swapee.ISwapeeAspectsInstaller = function() {}
/** @param {...!xyz.swapee.ISwapeeAspectsInstaller.Initialese} init */
xyz.swapee.ISwapeeAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.beforeRun
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterRun
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterRunThrows
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterRunReturns
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterRunCancels
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.beforeEachRun
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterEachRun
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterEachRunReturns
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.immediateAfterRun
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.before_ShowText
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.after_ShowText
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.after_ShowTextThrows
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.after_ShowTextReturns
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.after_ShowTextCancels
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.immediateAfter_ShowText
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.beforeConnectExchanges
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterConnectExchanges
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterConnectExchangesThrows
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterConnectExchangesReturns
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterConnectExchangesCancels
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.beforeEachConnectExchanges
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterEachConnectExchanges
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.afterEachConnectExchangesReturns
/** @type {number} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.immediateAfterConnectExchanges
/** @return {?} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.run = function() {}
/** @return {?} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.ShowText = function() {}
/** @return {?} */
xyz.swapee.ISwapeeAspectsInstaller.prototype.connectExchanges = function() {}

/**
 * @constructor
 * @param {...!xyz.swapee.ISwapeeAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.ISwapeeAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeAspectsInstaller.Initialese>}
 */
xyz.swapee.SwapeeAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.ISwapeeAspectsInstaller.Initialese} init */
xyz.swapee.SwapeeAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.SwapeeAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller = function() {}
/**
 * @param {...(!xyz.swapee.ISwapeeAspectsInstaller|typeof xyz.swapee.SwapeeAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractSwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.ISwapeeAspectsInstaller|typeof xyz.swapee.SwapeeAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.ISwapeeAspectsInstaller|typeof xyz.swapee.SwapeeAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.ISwapeeAspectsInstaller, ...!xyz.swapee.ISwapeeAspectsInstaller.Initialese)} */
xyz.swapee.SwapeeAspectsInstallerConstructor

/** @typedef {{ conf: !xyz.swapee.swapee_xyz.Config }} */
xyz.swapee.ISwapee.RunNArgs

/** @typedef {{ ticket: symbol, args: xyz.swapee.ISwapee.RunNArgs, proc: !Function }} */
xyz.swapee.ISwapeeJoinpointModel.RunPointcutData

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.RunPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData.prototype.cond
/**
 * @param {xyz.swapee.ISwapee.RunNArgs} args
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<string>} value
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.RunPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData = function() {}
/** @type {string} */
xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData.prototype.res

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.RunPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData.prototype.err
/** @return {void} */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.RunPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData = function() {}
/** @type {string} */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData.prototype.res
/**
 * @param {(!Promise<string>|string)} value
 * @return {?}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.RunPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData.prototype.reasons

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.RunPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData = function() {}
/** @type {!Promise<string>} */
xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData.prototype.promise

/** @typedef {{ conf: !xyz.swapee.ISwapee.ShowText.Conf }} */
xyz.swapee.ISwapee.ShowTextNArgs

/** @typedef {{ ticket: symbol, args: xyz.swapee.ISwapee.ShowTextNArgs, proc: !Function }} */
xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData.prototype.cond
/**
 * @param {xyz.swapee.ISwapee.ShowTextNArgs} args
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<string>} value
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData = function() {}
/** @type {string} */
xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData.prototype.res

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData.prototype.err
/** @return {void} */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData = function() {}
/** @type {string} */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData.prototype.res
/**
 * @param {(!Promise<string>|string)} value
 * @return {?}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData.prototype.reasons

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData = function() {}
/** @type {!Promise<string>} */
xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData.prototype.promise

/** @typedef {{ opts: !xyz.swapee.ISwapee.connectExchanges.Opts }} */
xyz.swapee.ISwapee.ConnectExchangesNArgs

/** @typedef {{ ticket: symbol, args: xyz.swapee.ISwapee.ConnectExchangesNArgs, proc: !Function }} */
xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData.prototype.cond
/**
 * @param {xyz.swapee.ISwapee.ConnectExchangesNArgs} args
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<xyz.swapee.ISwapee.connectExchanges.Return>} value
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData = function() {}
/** @type {xyz.swapee.ISwapee.connectExchanges.Return} */
xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData.prototype.res

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData.prototype.err
/** @return {void} */
xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData = function() {}
/** @type {xyz.swapee.ISwapee.connectExchanges.Return} */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData.prototype.res
/**
 * @param {(!Promise<xyz.swapee.ISwapee.connectExchanges.Return>|xyz.swapee.ISwapee.connectExchanges.Return)} value
 * @return {?}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData.prototype.reasons

/**
 * @record
 * @extends {xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData}
 */
xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData = function() {}
/** @type {!Promise<xyz.swapee.ISwapee.connectExchanges.Return>} */
xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData.prototype.promise

/** @typedef {function(new: xyz.swapee.ISwapee, ...!xyz.swapee.ISwapee.Initialese)} */
xyz.swapee.SwapeeConstructor

/**
 * @interface
 * @template THIS
 */
xyz.swapee.BSwapeeAspectsCaster
/** @type {!xyz.swapee.BoundISwapee} */
xyz.swapee.BSwapeeAspectsCaster.prototype.asISwapee

/**
 * @interface
 * @extends {xyz.swapee.BSwapeeAspectsCaster<THIS>}
 * @extends {xyz.swapee.ISwapeeJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.BSwapeeAspects = function() {}

/** @record */
xyz.swapee.ISwapeeAspects.Initialese = function() {}

/** @interface */
xyz.swapee.ISwapeeAspectsCaster
/** @type {!xyz.swapee.BoundISwapee} */
xyz.swapee.ISwapeeAspectsCaster.prototype.asISwapee

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.ISwapeeAspectsCaster}
 * @extends {xyz.swapee.BSwapeeAspects<!xyz.swapee.ISwapeeAspects>}
 */
xyz.swapee.ISwapeeAspects = function() {}
/** @param {...!xyz.swapee.ISwapeeAspects.Initialese} init */
xyz.swapee.ISwapeeAspects.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!xyz.swapee.ISwapeeAspects.Initialese} init
 * @implements {xyz.swapee.ISwapeeAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeAspects.Initialese>}
 */
xyz.swapee.SwapeeAspects = function(...init) {}
/** @param {...!xyz.swapee.ISwapeeAspects.Initialese} init */
xyz.swapee.SwapeeAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.SwapeeAspects.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.SwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects = function() {}
/**
 * @param {...((!xyz.swapee.ISwapeeAspects|typeof xyz.swapee.SwapeeAspects)|(!xyz.swapee.BSwapeeAspects|typeof xyz.swapee.BSwapeeAspects))} Implementations
 * @return {typeof xyz.swapee.SwapeeAspects}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractSwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.ISwapeeAspects|typeof xyz.swapee.SwapeeAspects)|(!xyz.swapee.BSwapeeAspects|typeof xyz.swapee.BSwapeeAspects))} Implementations
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.ISwapeeAspects|typeof xyz.swapee.SwapeeAspects)|(!xyz.swapee.BSwapeeAspects|typeof xyz.swapee.BSwapeeAspects))} Implementations
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.ISwapeeAspects, ...!xyz.swapee.ISwapeeAspects.Initialese)} */
xyz.swapee.SwapeeAspectsConstructor

/**
 * @record
 * @extends {xyz.swapee.ISwapee.Initialese}
 */
xyz.swapee.IHyperSwapee.Initialese = function() {}

/** @interface */
xyz.swapee.IHyperSwapeeCaster
/** @type {!xyz.swapee.BoundIHyperSwapee} */
xyz.swapee.IHyperSwapeeCaster.prototype.asIHyperSwapee
/** @type {!xyz.swapee.BoundHyperSwapee} */
xyz.swapee.IHyperSwapeeCaster.prototype.superHyperSwapee

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IHyperSwapeeCaster}
 * @extends {xyz.swapee.ISwapee}
 */
xyz.swapee.IHyperSwapee = function() {}
/** @param {...!xyz.swapee.IHyperSwapee.Initialese} init */
xyz.swapee.IHyperSwapee.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!xyz.swapee.IHyperSwapee.Initialese} init
 * @implements {xyz.swapee.IHyperSwapee}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IHyperSwapee.Initialese>}
 */
xyz.swapee.HyperSwapee = function(...init) {}
/** @param {...!xyz.swapee.IHyperSwapee.Initialese} init */
xyz.swapee.HyperSwapee.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.HyperSwapee.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.HyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee = function() {}
/**
 * @param {...((!xyz.swapee.IHyperSwapee|typeof xyz.swapee.HyperSwapee)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|(!xyz.swapee.ISwapeeHyperslice|typeof xyz.swapee.SwapeeHyperslice))} Implementations
 * @return {typeof xyz.swapee.HyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IHyperSwapee|typeof xyz.swapee.HyperSwapee)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|(!xyz.swapee.ISwapeeHyperslice|typeof xyz.swapee.SwapeeHyperslice))} Implementations
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IHyperSwapee|typeof xyz.swapee.HyperSwapee)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|(!xyz.swapee.ISwapeeHyperslice|typeof xyz.swapee.SwapeeHyperslice))} Implementations
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.ISwapeeAspects|!Array<!xyz.swapee.ISwapeeAspects>|function(new: xyz.swapee.ISwapeeAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.installs = function(...aspectsInstallers) {}

/** @typedef {function(new: xyz.swapee.IHyperSwapee, ...!xyz.swapee.IHyperSwapee.Initialese)} */
xyz.swapee.HyperSwapeeConstructor

/** @typedef {typeof __$te_plain} */
xyz.swapee.RecordIHyperSwapee

/**
 * @record
 * @extends {xyz.swapee.RecordIHyperSwapee}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.IHyperSwapeeCaster}
 */
xyz.swapee.BoundIHyperSwapee = function() {}

/**
 * @record
 * @extends {xyz.swapee.BoundIHyperSwapee}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundHyperSwapee = function() {}

/** @interface */
xyz.swapee.ISwapeeHyperslice = function() {}
/** @type {(!xyz.swapee.ISwapee._run|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee._run>)} */
xyz.swapee.ISwapeeHyperslice.prototype.run
/** @type {(!xyz.swapee.ISwapee._connectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee._connectExchanges>)} */
xyz.swapee.ISwapeeHyperslice.prototype.connectExchanges

/**
 * @constructor
 * @implements {xyz.swapee.ISwapeeHyperslice}
 */
xyz.swapee.SwapeeHyperslice = function() {}

/**
 * @interface
 * @template THIS
 */
xyz.swapee.ISwapeeBindingHyperslice = function() {}
/** @type {(!xyz.swapee.ISwapee.__run<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee.__run<THIS>>)} */
xyz.swapee.ISwapeeBindingHyperslice.prototype.run
/** @type {(!xyz.swapee.ISwapee.__connectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee.__connectExchanges<THIS>>)} */
xyz.swapee.ISwapeeBindingHyperslice.prototype.connectExchanges

/**
 * @constructor
 * @implements {xyz.swapee.ISwapeeBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.SwapeeBindingHyperslice = function() {}

/** @typedef {{ run: xyz.swapee.ISwapee.run, ShowText: xyz.swapee.ISwapee.ShowText, connectExchanges: xyz.swapee.ISwapee.connectExchanges }} */
xyz.swapee.RecordISwapee

/**
 * @record
 * @extends {xyz.swapee.ISwapeeFields}
 * @extends {xyz.swapee.RecordISwapee}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.ISwapeeCaster}
 */
xyz.swapee.BoundISwapee = function() {}

/**
 * @record
 * @extends {xyz.swapee.BoundISwapee}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundSwapee = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.swapee_xyz.Config} [conf]
 * @return {!Promise<string>}
 */
$$xyz.swapee.ISwapee.__run = function(conf) {}
/** @typedef {function(!xyz.swapee.swapee_xyz.Config=): !Promise<string>} */
xyz.swapee.ISwapee.run
/** @typedef {function(this: xyz.swapee.ISwapee, !xyz.swapee.swapee_xyz.Config=): !Promise<string>} */
xyz.swapee.ISwapee._run
/** @typedef {typeof $$xyz.swapee.ISwapee.__run} */
xyz.swapee.ISwapee.__run

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapee.ShowText.Conf} conf
 * @return {!Promise<string>}
 */
$$xyz.swapee.ISwapee.__ShowText = function(conf) {}
/** @typedef {function(!xyz.swapee.ISwapee.ShowText.Conf): !Promise<string>} */
xyz.swapee.ISwapee.ShowText
/** @typedef {function(this: xyz.swapee.ISwapee, !xyz.swapee.ISwapee.ShowText.Conf): !Promise<string>} */
xyz.swapee.ISwapee._ShowText
/** @typedef {typeof $$xyz.swapee.ISwapee.__ShowText} */
xyz.swapee.ISwapee.__ShowText

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.ISwapee.connectExchanges.Opts} [opts]
 * @return {!Promise<xyz.swapee.ISwapee.connectExchanges.Return>}
 */
$$xyz.swapee.ISwapee.__connectExchanges = function(opts) {}
/** @typedef {function(!xyz.swapee.ISwapee.connectExchanges.Opts=): !Promise<xyz.swapee.ISwapee.connectExchanges.Return>} */
xyz.swapee.ISwapee.connectExchanges
/** @typedef {function(this: xyz.swapee.ISwapee, !xyz.swapee.ISwapee.connectExchanges.Opts=): !Promise<xyz.swapee.ISwapee.connectExchanges.Return>} */
xyz.swapee.ISwapee._connectExchanges
/** @typedef {typeof $$xyz.swapee.ISwapee.__connectExchanges} */
xyz.swapee.ISwapee.__connectExchanges

/** @record */
xyz.swapee.ISwapee.ShowText.Conf = function() {}
/** @type {string} */
xyz.swapee.ISwapee.ShowText.Conf.prototype.text
/** @type {string|undefined} */
xyz.swapee.ISwapee.ShowText.Conf.prototype.output

/** @record */
xyz.swapee.ISwapee.connectExchanges.Opts = function() {}
/** @type {string|undefined} */
xyz.swapee.ISwapee.connectExchanges.Opts.prototype.amount
/** @type {string|undefined} */
xyz.swapee.ISwapee.connectExchanges.Opts.prototype.cryptoTo
/** @type {string|undefined} */
xyz.swapee.ISwapee.connectExchanges.Opts.prototype.cryptoFrom

/** @record */
xyz.swapee.ISwapee.connectExchanges.Return = function() {}
/** @type {!Object<string, Object>} */
xyz.swapee.ISwapee.connectExchanges.Return.prototype.exchanges
/** @type {!Map} */
xyz.swapee.ISwapee.connectExchanges.Return.prototype.results

/**
 * @record
 * @extends {engineer.type.IConnector.Initialese}
 * @extends {com.changelly.UChangelly.Initialese}
 */
xyz.swapee.IChangellyConnector.Initialese = function() {}
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyClientId
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyClientSecret
/** @type {boolean|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.connectChangelly
/** @type {((boolean|string))|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.cacheChangellyConnectResponse
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyConnectorFrom
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyConnectorTo
/** @type {number|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyConnectorAmount

/** @interface */
xyz.swapee.IChangellyConnectorFields
/** @type {(boolean|string)} */
xyz.swapee.IChangellyConnectorFields.prototype.cacheChangellyConnectResponse
/** @type {string} */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorFrom
/** @type {string} */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorTo
/** @type {number} */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorAmount

/** @interface */
xyz.swapee.IChangellyConnectorCaster
/** @type {!xyz.swapee.BoundIChangellyConnector} */
xyz.swapee.IChangellyConnectorCaster.prototype.asIChangellyConnector
/** @type {!xyz.swapee.BoundChangellyConnector} */
xyz.swapee.IChangellyConnectorCaster.prototype.superChangellyConnector

/**
 * @interface
 * @extends {xyz.swapee.IChangellyConnectorFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IChangellyConnectorCaster}
 * @extends {engineer.type.IConnector}
 * @extends {com.changelly.UChangelly}
 */
xyz.swapee.IChangellyConnector = function() {}
/** @param {...!xyz.swapee.IChangellyConnector.Initialese} init */
xyz.swapee.IChangellyConnector.prototype.constructor = function(...init) {}
/** @return {!Promise} */
xyz.swapee.IChangellyConnector.prototype.ping = function() {}
/**
 * @param {!xyz.swapee.IChangellyConnector.pingChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
xyz.swapee.IChangellyConnector.prototype.pingChangelly = function(opts) {}
/**
 * @param {!xyz.swapee.IChangellyConnector.connectChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
xyz.swapee.IChangellyConnector.prototype.connectChangelly = function(opts) {}
/**
 * @param {!Array<!com.changelly.ExchangeInfo>} connectResult
 * @return {!Promise<void>}
 */
xyz.swapee.IChangellyConnector.prototype.postConnectChangelly = function(connectResult) {}

/**
 * @constructor
 * @param {...!xyz.swapee.IChangellyConnector.Initialese} init
 * @implements {xyz.swapee.IChangellyConnector}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnector.Initialese>}
 */
xyz.swapee.ChangellyConnector = function(...init) {}
/** @param {...!xyz.swapee.IChangellyConnector.Initialese} init */
xyz.swapee.ChangellyConnector.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.ChangellyConnector.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector = function() {}
/**
 * @param {...((!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnector.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.__trait = function(...Implementations) {}

/** @interface */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforePing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePing>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforePing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPing>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforePingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforeConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforePostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterPostConnectChangelly

/**
 * @constructor
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModelHyperslice}
 */
xyz.swapee.ChangellyConnectorJoinpointModelHyperslice = function() {}

/**
 * @interface
 * @template THIS
 */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforePing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforePingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforeConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforePostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterPostConnectChangelly

/**
 * @constructor
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.ChangellyConnectorJoinpointModelBindingHyperslice = function() {}

/** @interface */
xyz.swapee.IChangellyConnectorJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePing = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPing = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPing = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePingChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPingChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforeConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePostConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPostConnectChangelly = function(data) {}

/**
 * @constructor
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModel}
 */
xyz.swapee.ChangellyConnectorJoinpointModel = function() {}

/** @typedef {{ beforePing: xyz.swapee.IChangellyConnectorJoinpointModel.beforePing, afterPing: xyz.swapee.IChangellyConnectorJoinpointModel.afterPing, afterPingThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows, afterPingReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns, afterPingCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels, immediatelyAfterPing: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing, beforePingChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly, afterPingChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly, afterPingChangellyThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows, afterPingChangellyReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns, afterPingChangellyCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels, immediatelyAfterPingChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly, beforeConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly, afterConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly, afterConnectChangellyThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows, afterConnectChangellyReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns, afterConnectChangellyCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels, immediatelyAfterConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly, beforePostConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly, afterPostConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly, afterPostConnectChangellyThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows, afterPostConnectChangellyReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns, afterPostConnectChangellyCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels, immediatelyAfterPostConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly }} */
xyz.swapee.RecordIChangellyConnectorJoinpointModel

/**
 * @record
 * @extends {xyz.swapee.RecordIChangellyConnectorJoinpointModel}
 */
xyz.swapee.BoundIChangellyConnectorJoinpointModel = function() {}

/**
 * @record
 * @extends {xyz.swapee.BoundIChangellyConnectorJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundChangellyConnectorJoinpointModel = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePing
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforePing
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPing
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPing
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly

/** @record */
xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese = function() {}

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
xyz.swapee.IChangellyConnectorAspectsInstaller = function() {}
/** @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforePing
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPing
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterPing
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforePingChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangellyThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangellyReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangellyCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterPingChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforeConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangellyThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangellyReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangellyCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforePostConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangellyThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangellyReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangellyCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterPostConnectChangelly
/** @return {void} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.ping = function() {}
/** @return {?} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.pingChangelly = function() {}
/** @return {?} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.connectChangelly = function() {}
/** @return {?} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.postConnectChangelly = function() {}

/**
 * @constructor
 * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.IChangellyConnectorAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese>}
 */
xyz.swapee.ChangellyConnectorAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init */
xyz.swapee.ChangellyConnectorAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.ChangellyConnectorAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller = function() {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.IChangellyConnectorAspectsInstaller, ...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese)} */
xyz.swapee.ChangellyConnectorAspectsInstallerConstructor

/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData.prototype.cancel = function(reason) {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData.prototype.reasons

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData = function() {}
/** @type {!Promise<void>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData.prototype.promise

/** @typedef {{ opts: !xyz.swapee.IChangellyConnector.pingChangelly.Opts }} */
xyz.swapee.IChangellyConnector.PingChangellyNArgs

/** @typedef {{ ticket: symbol, args: xyz.swapee.IChangellyConnector.PingChangellyNArgs, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.cond
/**
 * @param {xyz.swapee.IChangellyConnector.PingChangellyNArgs} args
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<!Array<!com.changelly.ExchangeInfo>>} value
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData.prototype.res

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData.prototype.res
/**
 * @param {(!Promise<!Array<!com.changelly.ExchangeInfo>>|!Array<!com.changelly.ExchangeInfo>)} value
 * @return {?}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData.prototype.reasons

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData = function() {}
/** @type {!Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData.prototype.promise

/** @typedef {{ opts: !xyz.swapee.IChangellyConnector.connectChangelly.Opts }} */
xyz.swapee.IChangellyConnector.ConnectChangellyNArgs

/** @typedef {{ ticket: symbol, args: xyz.swapee.IChangellyConnector.ConnectChangellyNArgs, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.cond
/**
 * @param {xyz.swapee.IChangellyConnector.ConnectChangellyNArgs} args
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<!Array<!com.changelly.ExchangeInfo>>} value
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData.prototype.res

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData.prototype.res
/**
 * @param {(!Promise<!Array<!com.changelly.ExchangeInfo>>|!Array<!com.changelly.ExchangeInfo>)} value
 * @return {?}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData.prototype.sub = function(value) {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData.prototype.reasons

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData = function() {}
/** @type {!Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData.prototype.promise

/** @typedef {{ connectResult: !Array<!com.changelly.ExchangeInfo> }} */
xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs

/** @typedef {{ ticket: symbol, args: xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData.prototype.cond
/**
 * @param {xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs} args
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData.prototype.cancel = function(reason) {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData.prototype.hide = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData = function() {}

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData.prototype.reasons

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData = function() {}
/** @type {!Promise<void>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData.prototype.promise

/** @typedef {function(new: xyz.swapee.IChangellyConnector, ...!xyz.swapee.IChangellyConnector.Initialese)} */
xyz.swapee.ChangellyConnectorConstructor

/**
 * @interface
 * @template THIS
 */
xyz.swapee.BChangellyConnectorAspectsCaster
/** @type {!xyz.swapee.BoundIChangellyConnector} */
xyz.swapee.BChangellyConnectorAspectsCaster.prototype.asIChangellyConnector

/**
 * @interface
 * @extends {xyz.swapee.BChangellyConnectorAspectsCaster<THIS>}
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.BChangellyConnectorAspects = function() {}

/** @record */
xyz.swapee.IChangellyConnectorAspects.Initialese = function() {}

/** @interface */
xyz.swapee.IChangellyConnectorAspectsCaster
/** @type {!xyz.swapee.BoundIChangellyConnector} */
xyz.swapee.IChangellyConnectorAspectsCaster.prototype.asIChangellyConnector

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IChangellyConnectorAspectsCaster}
 * @extends {xyz.swapee.BChangellyConnectorAspects<!xyz.swapee.IChangellyConnectorAspects>}
 */
xyz.swapee.IChangellyConnectorAspects = function() {}
/** @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init */
xyz.swapee.IChangellyConnectorAspects.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init
 * @implements {xyz.swapee.IChangellyConnectorAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspects.Initialese>}
 */
xyz.swapee.ChangellyConnectorAspects = function(...init) {}
/** @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init */
xyz.swapee.ChangellyConnectorAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.ChangellyConnectorAspects.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects = function() {}
/**
 * @param {...((!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnectorAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.IChangellyConnectorAspects, ...!xyz.swapee.IChangellyConnectorAspects.Initialese)} */
xyz.swapee.ChangellyConnectorAspectsConstructor

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnector.Initialese}
 */
xyz.swapee.IHyperChangellyConnector.Initialese = function() {}

/** @interface */
xyz.swapee.IHyperChangellyConnectorCaster
/** @type {!xyz.swapee.BoundIHyperChangellyConnector} */
xyz.swapee.IHyperChangellyConnectorCaster.prototype.asIHyperChangellyConnector
/** @type {!xyz.swapee.BoundHyperChangellyConnector} */
xyz.swapee.IHyperChangellyConnectorCaster.prototype.superHyperChangellyConnector

/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IHyperChangellyConnectorCaster}
 * @extends {xyz.swapee.IChangellyConnector}
 */
xyz.swapee.IHyperChangellyConnector = function() {}
/** @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init */
xyz.swapee.IHyperChangellyConnector.prototype.constructor = function(...init) {}

/**
 * @constructor
 * @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init
 * @implements {xyz.swapee.IHyperChangellyConnector}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IHyperChangellyConnector.Initialese>}
 */
xyz.swapee.HyperChangellyConnector = function(...init) {}
/** @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init */
xyz.swapee.HyperChangellyConnector.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.HyperChangellyConnector.__extend = function(...Extensions) {}

/**
 * @constructor
 * @extends {xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector = function() {}
/**
 * @param {...((!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector))} Implementations
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector))} Implementations
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector))} Implementations
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspects|!Array<!xyz.swapee.IChangellyConnectorAspects>|function(new: xyz.swapee.IChangellyConnectorAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.installs = function(...aspectsInstallers) {}

/** @typedef {function(new: xyz.swapee.IHyperChangellyConnector, ...!xyz.swapee.IHyperChangellyConnector.Initialese)} */
xyz.swapee.HyperChangellyConnectorConstructor

/** @typedef {typeof __$te_plain} */
xyz.swapee.RecordIHyperChangellyConnector

/**
 * @record
 * @extends {xyz.swapee.RecordIHyperChangellyConnector}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.IHyperChangellyConnectorCaster}
 */
xyz.swapee.BoundIHyperChangellyConnector = function() {}

/**
 * @record
 * @extends {xyz.swapee.BoundIHyperChangellyConnector}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundHyperChangellyConnector = function() {}

/** @typedef {{ ping: xyz.swapee.IChangellyConnector.ping, pingChangelly: xyz.swapee.IChangellyConnector.pingChangelly, connectChangelly: xyz.swapee.IChangellyConnector.connectChangelly, postConnectChangelly: xyz.swapee.IChangellyConnector.postConnectChangelly }} */
xyz.swapee.RecordIChangellyConnector

/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorFields}
 * @extends {xyz.swapee.RecordIChangellyConnector}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.IChangellyConnectorCaster}
 * @extends {engineer.type.BoundIConnector}
 * @extends {com.changelly.BoundUChangelly}
 */
xyz.swapee.BoundIChangellyConnector = function() {}

/**
 * @record
 * @extends {xyz.swapee.BoundIChangellyConnector}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundChangellyConnector = function() {}

/**
 * @this {THIS}
 * @template THIS
 * @return {!Promise}
 */
$$xyz.swapee.IChangellyConnector.__ping = function() {}
/** @typedef {function(): !Promise} */
xyz.swapee.IChangellyConnector.ping
/** @typedef {function(this: xyz.swapee.IChangellyConnector): !Promise} */
xyz.swapee.IChangellyConnector._ping
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__ping} */
xyz.swapee.IChangellyConnector.__ping

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnector.pingChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
$$xyz.swapee.IChangellyConnector.__pingChangelly = function(opts) {}
/** @typedef {function(!xyz.swapee.IChangellyConnector.pingChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector.pingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnector, !xyz.swapee.IChangellyConnector.pingChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector._pingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__pingChangelly} */
xyz.swapee.IChangellyConnector.__pingChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnector.connectChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
$$xyz.swapee.IChangellyConnector.__connectChangelly = function(opts) {}
/** @typedef {function(!xyz.swapee.IChangellyConnector.connectChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector.connectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnector, !xyz.swapee.IChangellyConnector.connectChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector._connectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__connectChangelly} */
xyz.swapee.IChangellyConnector.__connectChangelly

/**
 * @this {THIS}
 * @template THIS
 * @param {!Array<!com.changelly.ExchangeInfo>} connectResult
 * @return {!Promise<void>}
 */
$$xyz.swapee.IChangellyConnector.__postConnectChangelly = function(connectResult) {}
/** @typedef {function(!Array<!com.changelly.ExchangeInfo>): !Promise<void>} */
xyz.swapee.IChangellyConnector.postConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnector, !Array<!com.changelly.ExchangeInfo>): !Promise<void>} */
xyz.swapee.IChangellyConnector._postConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__postConnectChangelly} */
xyz.swapee.IChangellyConnector.__postConnectChangelly

/** @record */
xyz.swapee.IChangellyConnector.pingChangelly.Opts = function() {}
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.pingChangelly.Opts.prototype.from
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.pingChangelly.Opts.prototype.to
/** @type {number|undefined} */
xyz.swapee.IChangellyConnector.pingChangelly.Opts.prototype.amount

/** @record */
xyz.swapee.IChangellyConnector.connectChangelly.Opts = function() {}
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.connectChangelly.Opts.prototype.from
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.connectChangelly.Opts.prototype.to
/** @type {number|undefined} */
xyz.swapee.IChangellyConnector.connectChangelly.Opts.prototype.amount

/** @record */
xyz.swapee.swapee_xyz.Config = function() {}
/** @type {boolean|undefined} */
xyz.swapee.swapee_xyz.Config.prototype.shouldRun
/** @type {string|undefined} */
xyz.swapee.swapee_xyz.Config.prototype.text