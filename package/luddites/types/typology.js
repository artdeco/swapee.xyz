/** @nocompile */
const{registerTypology}=require('@type.engineering/type-engineer')

registerTypology({
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModelHyperslice': {
  'id': 36001359541,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModelBindingHyperslice': {
  'id': 36001359542,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModel': {
  'id': 36001359543,
  'symbols': {},
  'methods': {
   'beforeGetRpc': 1,
   'afterGetRpc': 2,
   'afterGetRpcThrows': 3,
   'afterGetRpcReturns': 4,
   'afterGetRpcCancels': 5,
   'beforeEachGetRpc': 6,
   'afterEachGetRpc': 7,
   'afterEachGetRpcReturns': 8,
   'immediatelyAfterGetRpc': 9,
   'beforeQueryRpc': 10,
   'afterQueryRpc': 11,
   'afterQueryRpcThrows': 12,
   'afterQueryRpcReturns': 13,
   'afterQueryRpcCancels': 14,
   'beforePutRpc': 15,
   'afterPutRpc': 16,
   'afterPutRpcThrows': 17,
   'afterPutRpcReturns': 18,
   'afterPutRpcCancels': 19,
   'immediatelyAfterPutRpc': 20,
   'beforePostRpc': 21,
   'afterPostRpc': 22,
   'afterPostRpcThrows': 23,
   'afterPostRpcReturns': 24,
   'afterPostRpcCancels': 25,
   'immediatelyAfterPostRpc': 26
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayAspectsInstaller': {
  'id': 36001359544,
  'symbols': {},
  'methods': {
   'getRpc': 1,
   'queryRpc': 2,
   'putRpc': 3,
   'postRpc': 4
  }
 },
 'xyz.swapee.ws.rpc.BRpcGatewayAspects': {
  'id': 36001359545,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayForwardingAspects': {
  'id': 36001359546,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayAspects': {
  'id': 36001359547,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IHyperRpcGateway': {
  'id': 36001359548,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayHyperslice': {
  'id': 36001359549,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayBindingHyperslice': {
  'id': 360013595410,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGateway': {
  'id': 360013595411,
  'symbols': {},
  'methods': {
   'getRpc': 1,
   'queryRpc': 2,
   'putRpc': 3,
   'postRpc': 4
  }
 },
 'xyz.swapee.wc.ITestingComputer': {
  'id': 360013595412,
  'symbols': {},
  'methods': {
   'adaptChangellyFloatingOffer': 1,
   'adaptChangenowOffer': 2,
   'adaptAnyLoading': 3,
   'compute': 4,
   'adaptChangellyFixedOffer': 5
  }
 },
 'xyz.swapee.wc.TestingMemoryPQs': {
  'id': 360013595413,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingOuterCore': {
  'id': 360013595414,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TestingInputsPQs': {
  'id': 360013595415,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingPort': {
  'id': 360013595416,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTestingPort': 2
  }
 },
 'xyz.swapee.wc.TestingCachePQs': {
  'id': 360013595417,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingCore': {
  'id': 360013595418,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTestingCore': 2
  }
 },
 'xyz.swapee.wc.ITestingProcessor': {
  'id': 360013595419,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITesting': {
  'id': 360013595420,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingBuffer': {
  'id': 360013595421,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingHtmlComponent': {
  'id': 360013595422,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingElement': {
  'id': 360013595423,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ITestingElementPort': {
  'id': 360013595424,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingRadio': {
  'id': 360013595425,
  'symbols': {},
  'methods': {
   'adaptLoadChangellyFloatingOffer': 1,
   'adaptLoadChangenowOffer': 2,
   'adaptLoadChangellyFixedOffer': 3
  }
 },
 'xyz.swapee.wc.ITestingDesigner': {
  'id': 360013595426,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITestingService': {
  'id': 360013595427,
  'symbols': {},
  'methods': {
   'filterChangellyFloatingOffer': 1,
   'filterChangenowOffer': 2,
   'filterChangellyFixedOffer': 3
  }
 },
 'xyz.swapee.wc.ITestingGPU': {
  'id': 360013595428,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingDisplay': {
  'id': 360013595429,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TestingVdusPQs': {
  'id': 360013595430,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITestingDisplay': {
  'id': 360013595431,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ITestingController': {
  'id': 360013595432,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'increaseAmount': 2,
   'decreaseAmount': 3,
   'setCore': 4,
   'unsetCore': 5,
   'setAmountIn': 6,
   'unsetAmountIn': 7,
   'setCurrencyIn': 8,
   'unsetCurrencyIn': 9,
   'setCurrencyOut': 10,
   'unsetCurrencyOut': 11,
   'loadChangellyFloatingOffer': 12,
   'loadChangenowOffer': 13,
   'loadChangellyFixedOffer': 14
  }
 },
 'xyz.swapee.wc.front.ITestingController': {
  'id': 360013595433,
  'symbols': {},
  'methods': {
   'increaseAmount': 1,
   'decreaseAmount': 2,
   'setCore': 3,
   'unsetCore': 4,
   'setAmountIn': 5,
   'unsetAmountIn': 6,
   'setCurrencyIn': 7,
   'unsetCurrencyIn': 8,
   'setCurrencyOut': 9,
   'unsetCurrencyOut': 10,
   'loadChangellyFloatingOffer': 11,
   'loadChangenowOffer': 12,
   'loadChangellyFixedOffer': 13
  }
 },
 'xyz.swapee.wc.back.ITestingController': {
  'id': 360013595434,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingControllerAR': {
  'id': 360013595435,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITestingControllerAT': {
  'id': 360013595436,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingScreen': {
  'id': 360013595437,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingScreen': {
  'id': 360013595438,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITestingScreenAR': {
  'id': 360013595439,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingScreenAT': {
  'id': 360013595440,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterComputer': {
  'id': 360013595441,
  'symbols': {},
  'methods': {
   'adaptNextPhrase': 1,
   'compute': 2
  }
 },
 'xyz.swapee.wc.TypeWriterMemoryPQs': {
  'id': 360013595442,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITypeWriterOuterCore': {
  'id': 360013595443,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TypeWriterInputsPQs': {
  'id': 360013595444,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITypeWriterPort': {
  'id': 360013595445,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTypeWriterPort': 2
  }
 },
 'xyz.swapee.wc.ITypeWriterCore': {
  'id': 360013595446,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTypeWriterCore': 2
  }
 },
 'xyz.swapee.wc.ITypeWriterProcessor': {
  'id': 360013595447,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriter': {
  'id': 360013595448,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterBuffer': {
  'id': 360013595449,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterHtmlComponent': {
  'id': 360013595450,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterElement': {
  'id': 360013595451,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ITypeWriterElementPort': {
  'id': 360013595452,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterDesigner': {
  'id': 360013595453,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITypeWriterGPU': {
  'id': 360013595454,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterDisplay': {
  'id': 360013595455,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TypeWriterVdusPQs': {
  'id': 360013595456,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITypeWriterDisplay': {
  'id': 360013595457,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ITypeWriterController': {
  'id': 360013595458,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'next': 2
  }
 },
 'xyz.swapee.wc.front.ITypeWriterController': {
  'id': 360013595459,
  'symbols': {},
  'methods': {
   'next': 1
  }
 },
 'xyz.swapee.wc.back.ITypeWriterController': {
  'id': 360013595460,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITypeWriterControllerAR': {
  'id': 360013595461,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITypeWriterControllerAT': {
  'id': 360013595462,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterScreen': {
  'id': 360013595463,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITypeWriterScreen': {
  'id': 360013595464,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITypeWriterScreenAR': {
  'id': 360013595465,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITypeWriterScreenAT': {
  'id': 360013595466,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableComputer': {
  'id': 360013595467,
  'symbols': {},
  'methods': {
   'compute': 2
  }
 },
 'xyz.swapee.wc.OffersTableMemoryPQs': {
  'id': 360013595468,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTableOuterCore': {
  'id': 360013595469,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersTableInputsPQs': {
  'id': 360013595470,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTablePort': {
  'id': 360013595471,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersTablePort': 2
  }
 },
 'xyz.swapee.wc.IOffersTableCore': {
  'id': 360013595472,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersTableCore': 2
  }
 },
 'xyz.swapee.wc.IOffersTableProcessor': {
  'id': 360013595473,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTable': {
  'id': 360013595474,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableBuffer': {
  'id': 360013595475,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableHtmlComponentUtil': {
  'id': 360013595476,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IOffersTableHtmlComponent': {
  'id': 360013595477,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableElement': {
  'id': 360013595478,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildProgressCollapsar': 5,
   'short': 6,
   'server': 7,
   'inducer': 8,
   'buildOffersAggregator': 9,
   'buildCryptoSelectIn': 10,
   'buildCryptoSelectOut': 11
  }
 },
 'xyz.swapee.wc.IOffersTableElementPort': {
  'id': 360013595479,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableDesigner': {
  'id': 360013595480,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersTableGPU': {
  'id': 360013595481,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableDisplay': {
  'id': 360013595482,
  'symbols': {},
  'methods': {
   'paint': 1,
   'paintTableOrder': 2,
   'paintSelectedCoinIm': 3
  }
 },
 'xyz.swapee.wc.OffersTableQueriesPQs': {
  'id': 360013595483,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.OffersTableVdusPQs': {
  'id': 360013595484,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersTableDisplay': {
  'id': 360013595485,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersTableClassesPQs': {
  'id': 360013595486,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTableControllerHyperslice': {
  'id': 360013595487,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableControllerBindingHyperslice': {
  'id': 360013595488,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableController': {
  'id': 360013595489,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTick': 2,
   'reset': 3,
   'onReset': 4,
   'swapCryptos': 5,
   'onSwapCryptos': 6,
   'setAmountIn': 7,
   'unsetAmountIn': 8,
   'setOnlyFixedRate': 9,
   'unsetOnlyFixedRate': 10,
   'setOnlyFloatingRate': 11,
   'unsetOnlyFloatingRate': 12,
   'setCoinIm': 13,
   'unsetCoinIm': 14
  }
 },
 'xyz.swapee.wc.front.IOffersTableControllerHyperslice': {
  'id': 360013595490,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice': {
  'id': 360013595491,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableController': {
  'id': 360013595492,
  'symbols': {},
  'methods': {
   'resetTick': 1,
   'reset': 2,
   'onReset': 3,
   'swapCryptos': 4,
   'onSwapCryptos': 5,
   'setAmountIn': 6,
   'unsetAmountIn': 7,
   'setOnlyFixedRate': 8,
   'unsetOnlyFixedRate': 9,
   'setOnlyFloatingRate': 10,
   'unsetOnlyFloatingRate': 11,
   'setCoinIm': 12,
   'unsetCoinIm': 13
  }
 },
 'xyz.swapee.wc.back.IOffersTableController': {
  'id': 360013595493,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableControllerAR': {
  'id': 360013595494,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableControllerAT': {
  'id': 360013595495,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableScreen': {
  'id': 360013595496,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableScreen': {
  'id': 360013595497,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableScreenAR': {
  'id': 360013595498,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableScreenAT': {
  'id': 360013595499,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectComputer': {
  'id': 3600135954100,
  'symbols': {},
  'methods': {
   'adaptWideness': 1,
   'adaptSelectedCrypto': 2,
   'adaptSearchInput': 3,
   'adaptPopupShown': 4,
   'adaptMatchedKeys': 5,
   'compute': 8
  }
 },
 'xyz.swapee.wc.CryptoSelectMemoryPQs': {
  'id': 3600135954101,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectOuterCore': {
  'id': 3600135954102,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.CryptoSelectInputsPQs': {
  'id': 3600135954103,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectPort': {
  'id': 3600135954104,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetCryptoSelectPort': 2
  }
 },
 'xyz.swapee.wc.CryptoSelectCachePQs': {
  'id': 3600135954105,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectCore': {
  'id': 3600135954106,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetCryptoSelectCore': 2
  }
 },
 'xyz.swapee.wc.ICryptoSelectProcessor': {
  'id': 3600135954107,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelect': {
  'id': 3600135954108,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectBuffer': {
  'id': 3600135954109,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectHtmlComponentUtil': {
  'id': 3600135954110,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectHtmlComponent': {
  'id': 3600135954111,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectElement': {
  'id': 3600135954112,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildPopup': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ICryptoSelectElementPort': {
  'id': 3600135954113,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectDesigner': {
  'id': 3600135954114,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ICryptoSelectGPU': {
  'id': 3600135954115,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectDisplay': {
  'id': 3600135954116,
  'symbols': {},
  'methods': {
   'resolveMouseItem': 1,
   'resolveItem': 2,
   'resolveItemByKey': 3,
   'resolveItemIndex': 4,
   'scrollItemIntoView': 5,
   'paint': 6,
   'paintSmHeight': 7,
   'paintSearch': 8,
   'paintSelectedImg': 9,
   'paintHoveringIndex': 10,
   'paintBeforeHovering': 11,
   'paintSearchInput': 12
  }
 },
 'xyz.swapee.wc.CryptoSelectVdusPQs': {
  'id': 3600135954117,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectDisplay': {
  'id': 3600135954118,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.CryptoSelectClassesPQs': {
  'id': 3600135954119,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectController': {
  'id': 3600135954120,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'flipMenuExpanded': 2,
   'calibrateDataSource': 3,
   'calibrateResetMouseOver': 4,
   'setSelected': 5,
   'unsetSelected': 6
  }
 },
 'xyz.swapee.wc.front.ICryptoSelectController': {
  'id': 3600135954121,
  'symbols': {},
  'methods': {
   'flipMenuExpanded': 1,
   'setSelected': 2,
   'unsetSelected': 3
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectController': {
  'id': 3600135954122,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ICryptoSelectControllerAR': {
  'id': 3600135954123,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ICryptoSelectControllerAT': {
  'id': 3600135954124,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectTouchscreen': {
  'id': 3600135954125,
  'symbols': {},
  'methods': {
   'stashSelectedInLocalStorage': 1,
   'stashIsMenuExpanded': 2,
   'stashKeyboardItemAfterMenuExpanded': 3,
   'stashVisibleItems': 4
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectTouchscreen': {
  'id': 3600135954126,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ICryptoSelectTouchscreenAR': {
  'id': 3600135954127,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ICryptoSelectTouchscreenAT': {
  'id': 3600135954128,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableTouchscreen': {
  'id': 3600135954129,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableTouchscreen': {
  'id': 3600135954130,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableTouchscreenAR': {
  'id': 3600135954131,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableTouchscreenAT': {
  'id': 3600135954132,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorComputer': {
  'id': 3600135954133,
  'symbols': {},
  'methods': {
   'adaptChangellyFloatingOffer': 1,
   'adaptChangellyFixedOffer': 2,
   'adaptEstimatedOut': 4,
   'adaptBestAndWorstOffers': 5,
   'adaptExchangesLoaded': 6,
   'adaptIsAggregating': 7,
   'compute': 8,
   'adaptLetsExchangeFloatingOffer': 9,
   'adaptLetsExchangeFixedOffer': 10,
   'adaptChangeNowFloatingOffer': 11,
   'adaptChangeNowFixedOffer': 12
  }
 },
 'xyz.swapee.wc.OffersAggregatorMemoryPQs': {
  'id': 3600135954134,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorOuterCore': {
  'id': 3600135954135,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersAggregatorInputsPQs': {
  'id': 3600135954136,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorPort': {
  'id': 3600135954137,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersAggregatorPort': 2
  }
 },
 'xyz.swapee.wc.OffersAggregatorCachePQs': {
  'id': 3600135954138,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorCore': {
  'id': 3600135954139,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersAggregatorCore': 2
  }
 },
 'xyz.swapee.wc.IOffersAggregatorProcessor': {
  'id': 3600135954140,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregator': {
  'id': 3600135954141,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorBuffer': {
  'id': 3600135954142,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorHtmlComponent': {
  'id': 3600135954143,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorElement': {
  'id': 3600135954144,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IOffersAggregatorElementPort': {
  'id': 3600135954145,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorRadio': {
  'id': 3600135954146,
  'symbols': {},
  'methods': {
   'adaptLoadChangellyFloatingOffer': 1,
   'adaptLoadChangellyFixedOffer': 2,
   'adaptLoadLetsExchangeFloatingOffer': 4,
   'adaptLoadLetsExchangeFixedOffer': 5,
   'adaptLoadChangeNowFloatingOffer': 6,
   'adaptLoadChangeNowFixedOffer': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorDesigner': {
  'id': 3600135954147,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersAggregatorService': {
  'id': 3600135954148,
  'symbols': {},
  'methods': {
   'filterChangellyFloatingOffer': 1,
   'filterChangellyFixedOffer': 2,
   'filterLetsExchangeFloatingOffer': 4,
   'filterLetsExchangeFixedOffer': 5,
   'filterChangeNowFloatingOffer': 6,
   'filterChangeNowFixedOffer': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorGPU': {
  'id': 3600135954149,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorDisplay': {
  'id': 3600135954150,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersAggregatorVdusPQs': {
  'id': 3600135954151,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersAggregatorDisplay': {
  'id': 3600135954152,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorController': {
  'id': 3600135954153,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setChangellyFixedOffer': 2,
   'unsetChangellyFixedOffer': 3,
   'setChangellyFixedError': 4,
   'unsetChangellyFixedError': 5,
   'setChangellyFloatingError': 6,
   'unsetChangellyFloatingError': 7,
   'setChangellyFloatingOffer': 8,
   'unsetChangellyFloatingOffer': 9,
   'setAmountIn': 10,
   'unsetAmountIn': 11,
   'setCurrencyIn': 12,
   'unsetCurrencyIn': 13,
   'setCurrencyOut': 14,
   'unsetCurrencyOut': 15,
   'loadChangellyFloatingOffer': 16,
   'loadChangellyFixedOffer': 17,
   'setLetsExchangeFixedOffer': 19,
   'unsetLetsExchangeFixedOffer': 20,
   'setLetsExchangeFixedError': 21,
   'unsetLetsExchangeFixedError': 22,
   'setLetsExchangeFloatingOffer': 23,
   'unsetLetsExchangeFloatingOffer': 24,
   'setLetsExchangeFloatingError': 25,
   'unsetLetsExchangeFloatingError': 26,
   'loadLetsExchangeFloatingOffer': 27,
   'loadLetsExchangeFixedOffer': 28,
   'setChangeNowFixedOffer': 29,
   'unsetChangeNowFixedOffer': 30,
   'setChangeNowFixedError': 31,
   'unsetChangeNowFixedError': 32,
   'setChangeNowFloatingOffer': 33,
   'unsetChangeNowFloatingOffer': 34,
   'setChangeNowFloatingError': 35,
   'unsetChangeNowFloatingError': 36,
   'loadChangeNowFloatingOffer': 37,
   'loadChangeNowFixedOffer': 38
  }
 },
 'xyz.swapee.wc.front.IOffersAggregatorController': {
  'id': 3600135954154,
  'symbols': {},
  'methods': {
   'setChangellyFixedOffer': 1,
   'unsetChangellyFixedOffer': 2,
   'setChangellyFixedError': 3,
   'unsetChangellyFixedError': 4,
   'setChangellyFloatingError': 5,
   'unsetChangellyFloatingError': 6,
   'setChangellyFloatingOffer': 7,
   'unsetChangellyFloatingOffer': 8,
   'setAmountIn': 9,
   'unsetAmountIn': 10,
   'setCurrencyIn': 11,
   'unsetCurrencyIn': 12,
   'setCurrencyOut': 13,
   'unsetCurrencyOut': 14,
   'loadChangellyFloatingOffer': 15,
   'loadChangellyFixedOffer': 16,
   'setLetsExchangeFixedOffer': 18,
   'unsetLetsExchangeFixedOffer': 19,
   'setLetsExchangeFixedError': 20,
   'unsetLetsExchangeFixedError': 21,
   'setLetsExchangeFloatingOffer': 22,
   'unsetLetsExchangeFloatingOffer': 23,
   'setLetsExchangeFloatingError': 24,
   'unsetLetsExchangeFloatingError': 25,
   'loadLetsExchangeFloatingOffer': 26,
   'loadLetsExchangeFixedOffer': 27,
   'setChangeNowFixedOffer': 28,
   'unsetChangeNowFixedOffer': 29,
   'setChangeNowFixedError': 30,
   'unsetChangeNowFixedError': 31,
   'setChangeNowFloatingOffer': 32,
   'unsetChangeNowFloatingOffer': 33,
   'setChangeNowFloatingError': 34,
   'unsetChangeNowFloatingError': 35,
   'loadChangeNowFloatingOffer': 36,
   'loadChangeNowFixedOffer': 37
  }
 },
 'xyz.swapee.wc.back.IOffersAggregatorController': {
  'id': 3600135954155,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorControllerAR': {
  'id': 3600135954156,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersAggregatorControllerAT': {
  'id': 3600135954157,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorScreen': {
  'id': 3600135954158,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorScreen': {
  'id': 3600135954159,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersAggregatorScreenAR': {
  'id': 3600135954160,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorScreenAT': {
  'id': 3600135954161,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ILetsExchangeJoinpointModel': {
  'id': 3600135954162,
  'symbols': {},
  'methods': {
   'before_GetInfo': 1,
   'after_GetInfo': 2,
   'after_GetInfoThrows': 3,
   'after_GetInfoReturns': 4,
   'after_GetInfoCancels': 5,
   'immediatelyAfter_GetInfo': 6
  }
 },
 'io.letsexchange.ILetsExchangeAspectsInstaller': {
  'id': 3600135954163,
  'symbols': {},
  'methods': {
   'GetInfo': 1
  }
 },
 'io.letsexchange.BLetsExchangeAspects': {
  'id': 3600135954164,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'io.letsexchange.ILetsExchangeAspects': {
  'id': 3600135954165,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.IHyperLetsExchange': {
  'id': 3600135954166,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ILetsExchange': {
  'id': 3600135954167,
  'symbols': {
   'host': 1,
   'apiPath': 2,
   'letsExchangeApiKey': 3
  },
  'methods': {
   'GetInfo': 1
  }
 },
 'io.letsexchange.ILetsExchangeJoinpointModelHyperslice': {
  'id': 3600135954168,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice': {
  'id': 3600135954169,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ULetsExchange': {
  'id': 3600135954170,
  'symbols': {
   'letsExchange': 1
  },
  'methods': {}
 },
 'io.changenow.IChangeNowJoinpointModelHyperslice': {
  'id': 3600135954171,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IChangeNowJoinpointModelBindingHyperslice': {
  'id': 3600135954172,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IChangeNowJoinpointModel': {
  'id': 3600135954173,
  'symbols': {},
  'methods': {
   'before_GetExchangeAmount': 1,
   'after_GetExchangeAmount': 2,
   'after_GetExchangeAmountThrows': 3,
   'after_GetExchangeAmountReturns': 4,
   'after_GetExchangeAmountCancels': 5,
   'immediatelyAfter_GetExchangeAmount': 6,
   'before_GetFixedExchangeAmount': 7,
   'after_GetFixedExchangeAmount': 8,
   'after_GetFixedExchangeAmountThrows': 9,
   'after_GetFixedExchangeAmountReturns': 10,
   'after_GetFixedExchangeAmountCancels': 11,
   'immediatelyAfter_GetFixedExchangeAmount': 12
  }
 },
 'io.changenow.IChangeNowAspectsInstaller': {
  'id': 3600135954174,
  'symbols': {},
  'methods': {
   'GetExchangeAmount': 1,
   'GetFixedExchangeAmount': 2
  }
 },
 'io.changenow.UChangeNow': {
  'id': 3600135954175,
  'symbols': {
   'changeNow': 1
  },
  'methods': {}
 },
 'io.changenow.BChangeNowAspects': {
  'id': 3600135954176,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'io.changenow.IChangeNowAspects': {
  'id': 3600135954177,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IHyperChangeNow': {
  'id': 3600135954178,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IChangeNow': {
  'id': 3600135954179,
  'symbols': {
   'host': 1,
   'apiPath': 2,
   'changeNowApiKey': 3
  },
  'methods': {
   'GetExchangeAmount': 4,
   'GetFixedExchangeAmount': 5
  }
 },
 'xyz.swapee.wc.IChangellyExchangeComputer': {
  'id': 3600135954180,
  'symbols': {},
  'methods': {
   'adaptCreateTransaction': 1,
   'compute': 2
  }
 },
 'xyz.swapee.wc.ChangellyExchangeMemoryPQs': {
  'id': 3600135954181,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeOuterCore': {
  'id': 3600135954182,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ChangellyExchangeInputsPQs': {
  'id': 3600135954183,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangePort': {
  'id': 3600135954184,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetChangellyExchangePort': 2
  }
 },
 'xyz.swapee.wc.ChangellyExchangeCachePQs': {
  'id': 3600135954185,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeCore': {
  'id': 3600135954186,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetChangellyExchangeCore': 2
  }
 },
 'xyz.swapee.wc.IChangellyExchangeProcessor': {
  'id': 3600135954187,
  'symbols': {},
  'methods': {
   'pulseCreateTransaction': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchange': {
  'id': 3600135954188,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeBuffer': {
  'id': 3600135954189,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeHtmlComponent': {
  'id': 3600135954190,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeElement': {
  'id': 3600135954191,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4,
   'build': 5,
   'buildAddressPopup': 6,
   'buildRefundPopup': 7,
   'buildProgressColumn': 8,
   'short': 9
  }
 },
 'xyz.swapee.wc.IChangellyExchangeElementPort': {
  'id': 3600135954192,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeRadio': {
  'id': 3600135954193,
  'symbols': {},
  'methods': {
   'adaptLoadCreateTransaction': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeDesigner': {
  'id': 3600135954194,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IChangellyExchangeService': {
  'id': 3600135954195,
  'symbols': {},
  'methods': {
   'filterCreateTransaction': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeGPU': {
  'id': 3600135954196,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeDisplay': {
  'id': 3600135954197,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ChangellyExchangeVdusPQs': {
  'id': 3600135954198,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IChangellyExchangeDisplay': {
  'id': 3600135954199,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ChangellyExchangeClassesPQs': {
  'id': 3600135954200,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeController': {
  'id': 3600135954201,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'loadCreateTransaction': 2,
   'selectAddress': 3,
   'selectRefund': 4,
   'onSelectAddress': 5,
   'onSelectRefund': 6,
   'setAddress': 7,
   'unsetAddress': 8,
   'setRefundAddress': 9,
   'unsetRefundAddress': 10,
   'setError': 11,
   'unsetError': 12,
   'setTocAccepted': 13,
   'unsetTocAccepted': 14,
   'pulseCreateTransaction': 15
  }
 },
 'xyz.swapee.wc.front.IChangellyExchangeController': {
  'id': 3600135954202,
  'symbols': {},
  'methods': {
   'loadCreateTransaction': 1,
   'selectAddress': 2,
   'selectRefund': 3,
   'onSelectAddress': 4,
   'onSelectRefund': 5,
   'setAddress': 6,
   'unsetAddress': 7,
   'setRefundAddress': 8,
   'unsetRefundAddress': 9,
   'setError': 10,
   'unsetError': 11,
   'setTocAccepted': 12,
   'unsetTocAccepted': 13,
   'pulseCreateTransaction': 14
  }
 },
 'xyz.swapee.wc.back.IChangellyExchangeController': {
  'id': 3600135954203,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeControllerAR': {
  'id': 3600135954204,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IChangellyExchangeControllerAT': {
  'id': 3600135954205,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeTouchscreen': {
  'id': 3600135954206,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeTouchscreen': {
  'id': 3600135954207,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IChangellyExchangeTouchscreenAR': {
  'id': 3600135954208,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeTouchscreenAT': {
  'id': 3600135954209,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnComputer': {
  'id': 3600135954210,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.ProgressColumnMemoryPQs': {
  'id': 3600135954211,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IProgressColumnOuterCore': {
  'id': 3600135954212,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ProgressColumnInputsPQs': {
  'id': 3600135954213,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IProgressColumnPort': {
  'id': 3600135954214,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetProgressColumnPort': 2
  }
 },
 'xyz.swapee.wc.IProgressColumnCore': {
  'id': 3600135954215,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetProgressColumnCore': 2
  }
 },
 'xyz.swapee.wc.IProgressColumnProcessor': {
  'id': 3600135954216,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumn': {
  'id': 3600135954217,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnBuffer': {
  'id': 3600135954218,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnHtmlComponent': {
  'id': 3600135954219,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnElement': {
  'id': 3600135954220,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IProgressColumnElementPort': {
  'id': 3600135954221,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnDesigner': {
  'id': 3600135954222,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IProgressColumnGPU': {
  'id': 3600135954223,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnDisplay': {
  'id': 3600135954224,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ProgressColumnVdusPQs': {
  'id': 3600135954225,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IProgressColumnDisplay': {
  'id': 3600135954226,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ProgressColumnClassesPQs': {
  'id': 3600135954227,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IProgressColumnController': {
  'id': 3600135954228,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IProgressColumnController': {
  'id': 3600135954229,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnController': {
  'id': 3600135954230,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnControllerAR': {
  'id': 3600135954231,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IProgressColumnControllerAT': {
  'id': 3600135954232,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnScreen': {
  'id': 3600135954233,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnScreen': {
  'id': 3600135954234,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IProgressColumnScreenAR': {
  'id': 3600135954235,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnScreenAT': {
  'id': 3600135954236,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil': {
  'id': 3600135954237,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.ISwapee': {
  'id': 3600135954238,
  'symbols': {
   'text': 3
  },
  'methods': {
   'run': 2,
   'ShowText': 6,
   'connectExchanges': 7
  }
 },
 'xyz.swapee.ISwapeeJoinpointModelHyperslice': {
  'id': 3600135954239,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeJoinpointModelBindingHyperslice': {
  'id': 3600135954240,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeJoinpointModel': {
  'id': 3600135954241,
  'symbols': {},
  'methods': {
   'beforeRun': 1,
   'afterRun': 2,
   'afterRunThrows': 3,
   'afterRunReturns': 4,
   'afterRunCancels': 5,
   'immediatelyAfterRun': 6,
   'beforeEachRun': 13,
   'afterEachRun': 14,
   'afterEachRunReturns': 15,
   'before_ShowText': 28,
   'after_ShowText': 29,
   'after_ShowTextThrows': 30,
   'after_ShowTextReturns': 31,
   'after_ShowTextCancels': 32,
   'immediatelyAfter_ShowText': 33,
   'beforeConnectExchanges': 34,
   'afterConnectExchanges': 35,
   'afterConnectExchangesThrows': 36,
   'afterConnectExchangesReturns': 37,
   'afterConnectExchangesCancels': 38,
   'immediatelyAfterConnectExchanges': 39,
   'beforeEachConnectExchanges': 40,
   'afterEachConnectExchanges': 41,
   'afterEachConnectExchangesReturns': 42
  }
 },
 'xyz.swapee.ISwapeeAspectsInstaller': {
  'id': 3600135954242,
  'symbols': {},
  'methods': {
   'run': 1,
   'ShowText': 5,
   'connectExchanges': 6
  }
 },
 'xyz.swapee.BSwapeeAspects': {
  'id': 3600135954243,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.ISwapeeAspects': {
  'id': 3600135954244,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.IHyperSwapee': {
  'id': 3600135954245,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeAide': {
  'id': 3600135954246,
  'symbols': {
   'output': 1,
   'showHelp': 2,
   'showVersion': 3,
   'version': 4,
   'debug': 5,
   'testExchanges': 6
  },
  'methods': {
   'ShowHelp': 3,
   'ShowVersion': 4
  }
 },
 'xyz.swapee.ISwapeeHyperslice': {
  'id': 3600135954247,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeBindingHyperslice': {
  'id': 3600135954248,
  'symbols': {},
  'methods': {}
 }
})