import Module from './node'

/**
@license
@LICENSE @swapee.xyz/swapee_xyz (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @type {typeof io.letsexchange.LetsExchange} */
export const LetsExchange=Module['36001359542']
/**@extends {io.letsexchange.AbstractLetsExchangeAspects}*/
export class AbstractLetsExchangeAspects extends Module['36001359543'] {}
/** @type {typeof io.letsexchange.AbstractLetsExchangeAspects} */
AbstractLetsExchangeAspects.class=function(){}
/**@extends {io.letsexchange.AbstractHyperLetsExchange}*/
export class AbstractHyperLetsExchange extends Module['36001359545'] {}
/** @type {typeof io.letsexchange.AbstractHyperLetsExchange} */
AbstractHyperLetsExchange.class=function(){}
/** @type {typeof io.letsexchange.HyperLetsExchange} */
export const HyperLetsExchange=Module['36001359546']
/** @type {typeof io.letsexchange.LetsExchangeMetaUniversal} */
export const LetsExchangeMetaUniversal=Module['36001359547']
/** @type {typeof io.letsexchange.LetsExchangeUniversal} */
export const LetsExchangeUniversal=Module['36001359548']
/** @type {typeof io.changenow.ChangeNowAspectsInstaller} */
export const ChangeNowAspectsInstaller=Module['36001359549']
/** @type {typeof io.changenow.ChangeNow} */
export const ChangeNow=Module['360013595410']
/** @type {typeof io.changenow.ChangeNowMetaUniversal} */
export const ChangeNowMetaUniversal=Module['360013595411']
/** @type {typeof io.changenow.ChangeNowUniversal} */
export const ChangeNowUniversal=Module['360013595412']
/**@extends {io.changenow.AbstractChangeNowAspects}*/
export class AbstractChangeNowAspects extends Module['360013595413'] {}
/** @type {typeof io.changenow.AbstractChangeNowAspects} */
AbstractChangeNowAspects.class=function(){}
/**@extends {io.changenow.AbstractHyperChangeNow}*/
export class AbstractHyperChangeNow extends Module['360013595415'] {}
/** @type {typeof io.changenow.AbstractHyperChangeNow} */
AbstractHyperChangeNow.class=function(){}
/** @type {typeof io.changenow.HyperChangeNow} */
export const HyperChangeNow=Module['360013595416']
/** @type {typeof io.letsexchange.LetsExchangeAspectsInstaller} */
export const LetsExchangeAspectsInstaller=Module['360013595417']
/** @type {typeof xyz.swapee.Swapee} */
export const Swapee=Module['360013595418']
/** @type {typeof xyz.swapee.SwapeeAspectsInstaller} */
export const SwapeeAspectsInstaller=Module['360013595419']
/**@extends {xyz.swapee.AbstractSwapeeAspects}*/
export class AbstractSwapeeAspects extends Module['360013595420'] {}
/** @type {typeof xyz.swapee.AbstractSwapeeAspects} */
AbstractSwapeeAspects.class=function(){}
/**@extends {xyz.swapee.AbstractHyperSwapee}*/
export class AbstractHyperSwapee extends Module['360013595422'] {}
/** @type {typeof xyz.swapee.AbstractHyperSwapee} */
AbstractHyperSwapee.class=function(){}
/** @type {typeof xyz.swapee.HyperSwapee} */
export const HyperSwapee=Module['360013595423']
/** @type {typeof xyz.swapee.SwapeeAide} */
export const SwapeeAide=Module['360013595424']

/** Allows to embed the object code directly into other packages. */