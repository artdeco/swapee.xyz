/**
 * @suppress {uselessCode}
 */
export const isShared = () => { try {
 return XYZ_SWAPEE_SWAPEE_XYZ_COMPILE_SHARED
} catch (err) {
 return false
}}

/**
 * @suppress {uselessCode}
 */
export const getPackageName = () => { try {
 return XYZ_SWAPEE_SWAPEE_XYZ_PACKAGE_NAME
} catch (err) {
 return '@swapee.xyz/swapee_xyz'
}}
