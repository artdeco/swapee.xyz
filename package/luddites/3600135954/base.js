/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const k=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();const n=k["372700389810"],p=k["372700389811"];function r(a,b,c,d){return k["372700389812"](a,b,null,c,!1,d)}const aa=k["372700389815"];function t(a,b){return k["372700389817"](a,b,null,{},!1,void 0,void 0)}const u=k.iu,ba=k.prereturnFirst,y=k.$advice,A=k.returnFirst,ca=k.precombinedGet,da=k.precombined;module.exports={get y(){return y},get r(){return r},get p(){return p},get t(){return t},get u(){return u},
get aa(){return aa},get ca(){return ca},get n(){return n},get ba(){return ba},get da(){return da},get A(){return A}};