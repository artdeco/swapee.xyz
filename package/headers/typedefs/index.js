/** @nocompile */
eval(`var io={}
io.changenow={}
io.changenow.IChangeNow={}
io.changenow.IChangeNow.GetExchangeAmount={}
io.changenow.IChangeNow.GetFixedExchangeAmount={}
io.changenow.IChangeNowAspectsInstaller={}
io.changenow.IChangeNowJoinpointModel={}
io.changenow.UChangeNow={}
io.changenow.AbstractChangeNowUniversal={}
io.changenow.IChangeNowAspects={}
io.changenow.IHyperChangeNow={}
io.letsexchange={}
io.letsexchange.ILetsExchange={}
io.letsexchange.ILetsExchange.GetInfo={}
io.letsexchange.ILetsExchangeAspectsInstaller={}
io.letsexchange.ILetsExchangeJoinpointModel={}
io.letsexchange.ULetsExchange={}
io.letsexchange.AbstractLetsExchangeUniversal={}
io.letsexchange.ILetsExchangeAspects={}
io.letsexchange.IHyperLetsExchange={}
var xyz={}
xyz.swapee={}
xyz.swapee.ISwapeeAide={}
xyz.swapee.ISwapee={}
xyz.swapee.ISwapee.ShowText={}
xyz.swapee.ISwapee.connectExchanges={}
xyz.swapee.ISwapeeAspectsInstaller={}
xyz.swapee.ISwapeeJoinpointModel={}
xyz.swapee.ISwapeeAspects={}
xyz.swapee.IHyperSwapee={}
xyz.swapee.swapee_xyz={}`)

/** */
var __$te_plain={}

/**
 * @typedef {Object} $io.changenow.IChangeNow.Initialese
 * @prop {string} [host] The _ChangeNow_ instance host.
 * @prop {string} [apiPath] The path after the host to use for request.
 * @prop {string} [changeNowApiKey] The client id.
 */
/** @typedef {$io.changenow.IChangeNow.Initialese&engineer.type.INetwork.Initialese} io.changenow.IChangeNow.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: io.changenow.ChangeNow)} io.changenow.AbstractChangeNow.constructor */
/** @typedef {typeof io.changenow.ChangeNow} io.changenow.ChangeNow.typeof */
/**
 * An abstract class of `io.changenow.IChangeNow` interface.
 * @constructor io.changenow.AbstractChangeNow
 */
io.changenow.AbstractChangeNow = class extends /** @type {io.changenow.AbstractChangeNow.constructor&io.changenow.ChangeNow.typeof} */ (class {}) { }
io.changenow.AbstractChangeNow.prototype.constructor = io.changenow.AbstractChangeNow
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.changenow.AbstractChangeNow.class = /** @type {typeof io.changenow.AbstractChangeNow} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractChangeNow.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.changenow.AbstractChangeNow}
 */
io.changenow.AbstractChangeNow.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.AbstractChangeNow.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.AbstractChangeNow.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.AbstractChangeNow.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface io.changenow.IChangeNowJoinpointModelHyperslice
 */
io.changenow.IChangeNowJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_GetExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel._before_GetExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._before_GetExchangeAmount>} */ (void 0)
    /**
     * After the method.
     */
    this.after_GetExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmount>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_GetExchangeAmountThrows=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountThrows|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_GetExchangeAmountReturns=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountReturns|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_GetExchangeAmountCancels=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountCancels|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_GetExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetExchangeAmount>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_GetFixedExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel._before_GetFixedExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._before_GetFixedExchangeAmount>} */ (void 0)
    /**
     * After the method.
     */
    this.after_GetFixedExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmount>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_GetFixedExchangeAmountThrows=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountThrows|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_GetFixedExchangeAmountReturns=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountReturns|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_GetFixedExchangeAmountCancels=/** @type {!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountCancels|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_GetFixedExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetFixedExchangeAmount|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetFixedExchangeAmount>} */ (void 0)
  }
}
io.changenow.IChangeNowJoinpointModelHyperslice.prototype.constructor = io.changenow.IChangeNowJoinpointModelHyperslice

/**
 * A concrete class of _IChangeNowJoinpointModelHyperslice_ instances.
 * @constructor io.changenow.ChangeNowJoinpointModelHyperslice
 * @implements {io.changenow.IChangeNowJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
io.changenow.ChangeNowJoinpointModelHyperslice = class extends io.changenow.IChangeNowJoinpointModelHyperslice { }
io.changenow.ChangeNowJoinpointModelHyperslice.prototype.constructor = io.changenow.ChangeNowJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface io.changenow.IChangeNowJoinpointModelBindingHyperslice
 * @template THIS
 */
io.changenow.IChangeNowJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_GetExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_GetExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_GetExchangeAmountThrows=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_GetExchangeAmountReturns=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_GetExchangeAmountCancels=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_GetExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_GetFixedExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_GetFixedExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_GetFixedExchangeAmountThrows=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_GetFixedExchangeAmountReturns=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_GetFixedExchangeAmountCancels=/** @type {!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_GetFixedExchangeAmount=/** @type {!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount<THIS>|!engineering.type.RecursiveArray<!io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount<THIS>>} */ (void 0)
  }
}
io.changenow.IChangeNowJoinpointModelBindingHyperslice.prototype.constructor = io.changenow.IChangeNowJoinpointModelBindingHyperslice

/**
 * A concrete class of _IChangeNowJoinpointModelBindingHyperslice_ instances.
 * @constructor io.changenow.ChangeNowJoinpointModelBindingHyperslice
 * @implements {io.changenow.IChangeNowJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {io.changenow.IChangeNowJoinpointModelBindingHyperslice<THIS>}
 */
io.changenow.ChangeNowJoinpointModelBindingHyperslice = class extends io.changenow.IChangeNowJoinpointModelBindingHyperslice { }
io.changenow.ChangeNowJoinpointModelBindingHyperslice.prototype.constructor = io.changenow.ChangeNowJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IChangeNow`'s methods.
 * @interface io.changenow.IChangeNowJoinpointModel
 */
io.changenow.IChangeNowJoinpointModel = class { }
/** @type {io.changenow.IChangeNowJoinpointModel.before_GetExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.prototype.before_GetExchangeAmount = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmount = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountThrows} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmountThrows = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountReturns} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmountReturns = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountCancels} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetExchangeAmountCancels = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.prototype.immediatelyAfter_GetExchangeAmount = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.before_GetFixedExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.prototype.before_GetFixedExchangeAmount = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmount = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountThrows} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmountThrows = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountReturns} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmountReturns = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountCancels} */
io.changenow.IChangeNowJoinpointModel.prototype.after_GetFixedExchangeAmountCancels = function() {}
/** @type {io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetFixedExchangeAmount} */
io.changenow.IChangeNowJoinpointModel.prototype.immediatelyAfter_GetFixedExchangeAmount = function() {}

/**
 * A concrete class of _IChangeNowJoinpointModel_ instances.
 * @constructor io.changenow.ChangeNowJoinpointModel
 * @implements {io.changenow.IChangeNowJoinpointModel} An interface that enumerates the joinpoints of `IChangeNow`'s methods.
 */
io.changenow.ChangeNowJoinpointModel = class extends io.changenow.IChangeNowJoinpointModel { }
io.changenow.ChangeNowJoinpointModel.prototype.constructor = io.changenow.ChangeNowJoinpointModel

/** @typedef {io.changenow.IChangeNowJoinpointModel} */
io.changenow.RecordIChangeNowJoinpointModel

/** @typedef {io.changenow.IChangeNowJoinpointModel} io.changenow.BoundIChangeNowJoinpointModel */

/** @typedef {io.changenow.ChangeNowJoinpointModel} io.changenow.BoundChangeNowJoinpointModel */

/** @typedef {typeof __$te_plain} io.changenow.IChangeNowAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: io.changenow.ChangeNowAspectsInstaller)} io.changenow.AbstractChangeNowAspectsInstaller.constructor */
/** @typedef {typeof io.changenow.ChangeNowAspectsInstaller} io.changenow.ChangeNowAspectsInstaller.typeof */
/**
 * An abstract class of `io.changenow.IChangeNowAspectsInstaller` interface.
 * @constructor io.changenow.AbstractChangeNowAspectsInstaller
 */
io.changenow.AbstractChangeNowAspectsInstaller = class extends /** @type {io.changenow.AbstractChangeNowAspectsInstaller.constructor&io.changenow.ChangeNowAspectsInstaller.typeof} */ (class {}) { }
io.changenow.AbstractChangeNowAspectsInstaller.prototype.constructor = io.changenow.AbstractChangeNowAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.changenow.AbstractChangeNowAspectsInstaller.class = /** @type {typeof io.changenow.AbstractChangeNowAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!io.changenow.IChangeNowAspectsInstaller|typeof io.changenow.ChangeNowAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 * @nosideeffects
 */
io.changenow.AbstractChangeNowAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.changenow.AbstractChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!io.changenow.IChangeNowAspectsInstaller|typeof io.changenow.ChangeNowAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!io.changenow.IChangeNowAspectsInstaller|typeof io.changenow.ChangeNowAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.AbstractChangeNowAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !io.changenow.IChangeNowAspectsInstaller.Initialese[]) => io.changenow.IChangeNowAspectsInstaller} io.changenow.ChangeNowAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} io.changenow.IChangeNowAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface io.changenow.IChangeNowAspectsInstaller */
io.changenow.IChangeNowAspectsInstaller = class extends /** @type {io.changenow.IChangeNowAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.before_GetExchangeAmount=/** @type {number} */ (void 0)
    this.after_GetExchangeAmount=/** @type {number} */ (void 0)
    this.after_GetExchangeAmountThrows=/** @type {number} */ (void 0)
    this.after_GetExchangeAmountReturns=/** @type {number} */ (void 0)
    this.after_GetExchangeAmountCancels=/** @type {number} */ (void 0)
    this.immediateAfter_GetExchangeAmount=/** @type {number} */ (void 0)
    this.before_GetFixedExchangeAmount=/** @type {number} */ (void 0)
    this.after_GetFixedExchangeAmount=/** @type {number} */ (void 0)
    this.after_GetFixedExchangeAmountThrows=/** @type {number} */ (void 0)
    this.after_GetFixedExchangeAmountReturns=/** @type {number} */ (void 0)
    this.after_GetFixedExchangeAmountCancels=/** @type {number} */ (void 0)
    this.immediateAfter_GetFixedExchangeAmount=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  GetExchangeAmount() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  GetFixedExchangeAmount() { }
}
/**
 * Create a new *IChangeNowAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!io.changenow.IChangeNowAspectsInstaller.Initialese} init The initialisation options.
 */
io.changenow.IChangeNowAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.changenow.IChangeNowAspectsInstaller&engineering.type.IInitialiser<!io.changenow.IChangeNowAspectsInstaller.Initialese>)} io.changenow.ChangeNowAspectsInstaller.constructor */
/** @typedef {typeof io.changenow.IChangeNowAspectsInstaller} io.changenow.IChangeNowAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IChangeNowAspectsInstaller_ instances.
 * @constructor io.changenow.ChangeNowAspectsInstaller
 * @implements {io.changenow.IChangeNowAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!io.changenow.IChangeNowAspectsInstaller.Initialese>} ‎
 */
io.changenow.ChangeNowAspectsInstaller = class extends /** @type {io.changenow.ChangeNowAspectsInstaller.constructor&io.changenow.IChangeNowAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangeNowAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!io.changenow.IChangeNowAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangeNowAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!io.changenow.IChangeNowAspectsInstaller.Initialese} init The initialisation options.
 */
io.changenow.ChangeNowAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowAspectsInstaller}
 */
io.changenow.ChangeNowAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} io.changenow.IChangeNow.GetExchangeAmountNArgs
 * @prop {!io.changenow.IChangeNow.GetExchangeAmount.Data} data The data.
 */

/**
 * @typedef {Object} io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {io.changenow.IChangeNow.GetExchangeAmountNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: io.changenow.IChangeNow.GetExchangeAmountNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `GetExchangeAmount` method from being executed.
 * @prop {(value: !Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>) => void} sub Cancels a call to `GetExchangeAmount` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData
 * @prop {io.changenow.IChangeNow.GetExchangeAmount.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `GetExchangeAmount` (use only at the top-level of program flow).
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData
 * @prop {io.changenow.IChangeNow.GetExchangeAmount.Return} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>|io.changenow.IChangeNow.GetExchangeAmount.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData
 * @prop {!Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} io.changenow.IChangeNow.GetFixedExchangeAmountNArgs
 * @prop {!io.changenow.IChangeNow.GetFixedExchangeAmount.Data} data The data.
 */

/**
 * @typedef {Object} io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {io.changenow.IChangeNow.GetFixedExchangeAmountNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: io.changenow.IChangeNow.GetFixedExchangeAmountNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `GetFixedExchangeAmount` method from being executed.
 * @prop {(value: !Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>) => void} sub Cancels a call to `GetFixedExchangeAmount` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData
 * @prop {io.changenow.IChangeNow.GetFixedExchangeAmount.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `GetFixedExchangeAmount` (use only at the top-level of program flow).
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData
 * @prop {io.changenow.IChangeNow.GetFixedExchangeAmount.Return} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>|io.changenow.IChangeNow.GetFixedExchangeAmount.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData
 * @prop {!Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData&io.changenow.IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData} io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/** @typedef {new (...args: !io.changenow.IChangeNow.Initialese[]) => io.changenow.IChangeNow} io.changenow.ChangeNowConstructor */

/** @typedef {symbol} io.changenow.ChangeNowMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} io.changenow.UChangeNow.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {io.changenow.IChangeNow} [changeNow] The _ChangeNow_ client.
 */

/** @typedef {function(new: io.changenow.ChangeNowUniversal)} io.changenow.AbstractChangeNowUniversal.constructor */
/** @typedef {typeof io.changenow.ChangeNowUniversal} io.changenow.ChangeNowUniversal.typeof */
/**
 * An abstract class of `io.changenow.UChangeNow` interface.
 * @constructor io.changenow.AbstractChangeNowUniversal
 */
io.changenow.AbstractChangeNowUniversal = class extends /** @type {io.changenow.AbstractChangeNowUniversal.constructor&io.changenow.ChangeNowUniversal.typeof} */ (class {}) { }
io.changenow.AbstractChangeNowUniversal.prototype.constructor = io.changenow.AbstractChangeNowUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.changenow.AbstractChangeNowUniversal.class = /** @type {typeof io.changenow.AbstractChangeNowUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!io.changenow.UChangeNow|typeof io.changenow.UChangeNow} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowUniversal}
 * @nosideeffects
 */
io.changenow.AbstractChangeNowUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.changenow.AbstractChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!io.changenow.UChangeNow|typeof io.changenow.UChangeNow} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!io.changenow.UChangeNow|typeof io.changenow.UChangeNow} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.AbstractChangeNowUniversal.__trait = function(...Implementations) {}

/** @typedef {io.changenow.ChangeNowMetaUniversal} io.changenow.AbstractChangeNowUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !io.changenow.UChangeNow.Initialese[]) => io.changenow.UChangeNow} io.changenow.UChangeNowConstructor */

/** @typedef {function(new: io.changenow.UChangeNowFields&engineering.type.IEngineer&io.changenow.UChangeNowCaster)} io.changenow.UChangeNow.constructor */
/**
 * A trait that allows to access an _IChangeNow_ object via a `.changeNow` field.
 * @interface io.changenow.UChangeNow
 */
io.changenow.UChangeNow = class extends /** @type {io.changenow.UChangeNow.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UChangeNow* instance and automatically initialise it with options.
 * @param {...!io.changenow.UChangeNow.Initialese} init The initialisation options.
 */
io.changenow.UChangeNow.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.changenow.UChangeNow&engineering.type.IInitialiser<!io.changenow.UChangeNow.Initialese>)} io.changenow.ChangeNowUniversal.constructor */
/** @typedef {typeof io.changenow.UChangeNow} io.changenow.UChangeNow.typeof */
/**
 * A concrete class of _UChangeNow_ instances.
 * @constructor io.changenow.ChangeNowUniversal
 * @implements {io.changenow.UChangeNow} A trait that allows to access an _IChangeNow_ object via a `.changeNow` field.
 * @implements {engineering.type.IInitialiser<!io.changenow.UChangeNow.Initialese>} ‎
 */
io.changenow.ChangeNowUniversal = class extends /** @type {io.changenow.ChangeNowUniversal.constructor&io.changenow.UChangeNow.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UChangeNow* instance and automatically initialise it with options.
   * @param {...!io.changenow.UChangeNow.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UChangeNow* instance and automatically initialise it with options.
 * @param {...!io.changenow.UChangeNow.Initialese} init The initialisation options.
 */
io.changenow.ChangeNowUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowUniversal}
 */
io.changenow.ChangeNowUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UChangeNow.
 * @interface io.changenow.UChangeNowFields
 */
io.changenow.UChangeNowFields = class { }
/**
 * The _ChangeNow_ client. Default `null`.
 */
io.changenow.UChangeNowFields.prototype.changeNow = /** @type {io.changenow.IChangeNow} */ (void 0)

/** @typedef {io.changenow.UChangeNow} */
io.changenow.RecordUChangeNow

/** @typedef {io.changenow.UChangeNow} io.changenow.BoundUChangeNow */

/** @typedef {io.changenow.ChangeNowUniversal} io.changenow.BoundChangeNowUniversal */

/**
 * Contains getters to cast the _UChangeNow_ interface.
 * @interface io.changenow.UChangeNowCaster
 */
io.changenow.UChangeNowCaster = class { }
/**
 * Provides direct access to _UChangeNow_ via the _BoundUChangeNow_ universal.
 * @type {!io.changenow.BoundChangeNow}
 */
io.changenow.UChangeNowCaster.prototype.asChangeNow
/**
 * Cast the _UChangeNow_ instance into the _BoundUChangeNow_ type.
 * @type {!io.changenow.BoundUChangeNow}
 */
io.changenow.UChangeNowCaster.prototype.asUChangeNow
/**
 * Access the _ChangeNowUniversal_ prototype.
 * @type {!io.changenow.BoundChangeNowUniversal}
 */
io.changenow.UChangeNowCaster.prototype.superChangeNowUniversal

/** @typedef {function(new: io.changenow.BChangeNowAspectsCaster<THIS>&io.changenow.IChangeNowJoinpointModelBindingHyperslice<THIS>)} io.changenow.BChangeNowAspects.constructor */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModelBindingHyperslice} io.changenow.IChangeNowJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IChangeNow* that bind to an instance.
 * @interface io.changenow.BChangeNowAspects
 * @template THIS
 */
io.changenow.BChangeNowAspects = class extends /** @type {io.changenow.BChangeNowAspects.constructor&io.changenow.IChangeNowJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
io.changenow.BChangeNowAspects.prototype.constructor = io.changenow.BChangeNowAspects

/** @typedef {typeof __$te_plain} io.changenow.IChangeNowAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: io.changenow.ChangeNowAspects)} io.changenow.AbstractChangeNowAspects.constructor */
/** @typedef {typeof io.changenow.ChangeNowAspects} io.changenow.ChangeNowAspects.typeof */
/**
 * An abstract class of `io.changenow.IChangeNowAspects` interface.
 * @constructor io.changenow.AbstractChangeNowAspects
 */
io.changenow.AbstractChangeNowAspects = class extends /** @type {io.changenow.AbstractChangeNowAspects.constructor&io.changenow.ChangeNowAspects.typeof} */ (class {}) { }
io.changenow.AbstractChangeNowAspects.prototype.constructor = io.changenow.AbstractChangeNowAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.changenow.AbstractChangeNowAspects.class = /** @type {typeof io.changenow.AbstractChangeNowAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!io.changenow.IChangeNowAspects|typeof io.changenow.ChangeNowAspects)|(!io.changenow.BChangeNowAspects|typeof io.changenow.BChangeNowAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspects}
 * @nosideeffects
 */
io.changenow.AbstractChangeNowAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.changenow.AbstractChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!io.changenow.IChangeNowAspects|typeof io.changenow.ChangeNowAspects)|(!io.changenow.BChangeNowAspects|typeof io.changenow.BChangeNowAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!io.changenow.IChangeNowAspects|typeof io.changenow.ChangeNowAspects)|(!io.changenow.BChangeNowAspects|typeof io.changenow.BChangeNowAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.AbstractChangeNowAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !io.changenow.IChangeNowAspects.Initialese[]) => io.changenow.IChangeNowAspects} io.changenow.ChangeNowAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&io.changenow.IChangeNowAspectsCaster&io.changenow.BChangeNowAspects<!io.changenow.IChangeNowAspects>)} io.changenow.IChangeNowAspects.constructor */
/** @typedef {typeof io.changenow.BChangeNowAspects} io.changenow.BChangeNowAspects.typeof */
/**
 * The aspects of the *IChangeNow*.
 * @interface io.changenow.IChangeNowAspects
 */
io.changenow.IChangeNowAspects = class extends /** @type {io.changenow.IChangeNowAspects.constructor&engineering.type.IEngineer.typeof&io.changenow.BChangeNowAspects.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.props=/** @type {io.changenow.IChangeNowAspects} */ (void 0)
  }
}
/**
 * Create a new *IChangeNowAspects* instance and automatically initialise it with options.
 * @param {...!io.changenow.IChangeNowAspects.Initialese} init The initialisation options.
 */
io.changenow.IChangeNowAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.changenow.IChangeNowAspects&engineering.type.IInitialiser<!io.changenow.IChangeNowAspects.Initialese>)} io.changenow.ChangeNowAspects.constructor */
/** @typedef {typeof io.changenow.IChangeNowAspects} io.changenow.IChangeNowAspects.typeof */
/**
 * A concrete class of _IChangeNowAspects_ instances.
 * @constructor io.changenow.ChangeNowAspects
 * @implements {io.changenow.IChangeNowAspects} The aspects of the *IChangeNow*.
 * @implements {engineering.type.IInitialiser<!io.changenow.IChangeNowAspects.Initialese>} ‎
 */
io.changenow.ChangeNowAspects = class extends /** @type {io.changenow.ChangeNowAspects.constructor&io.changenow.IChangeNowAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangeNowAspects* instance and automatically initialise it with options.
   * @param {...!io.changenow.IChangeNowAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangeNowAspects* instance and automatically initialise it with options.
 * @param {...!io.changenow.IChangeNowAspects.Initialese} init The initialisation options.
 */
io.changenow.ChangeNowAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNowAspects}
 */
io.changenow.ChangeNowAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BChangeNowAspects_ interface.
 * @interface io.changenow.BChangeNowAspectsCaster
 * @template THIS
 */
io.changenow.BChangeNowAspectsCaster = class { }
/**
 * Cast the _BChangeNowAspects_ instance into the _BoundIChangeNow_ type.
 * @type {!io.changenow.BoundIChangeNow}
 */
io.changenow.BChangeNowAspectsCaster.prototype.asIChangeNow

/**
 * Contains getters to cast the _IChangeNowAspects_ interface.
 * @interface io.changenow.IChangeNowAspectsCaster
 */
io.changenow.IChangeNowAspectsCaster = class { }
/**
 * Cast the _IChangeNowAspects_ instance into the _BoundIChangeNow_ type.
 * @type {!io.changenow.BoundIChangeNow}
 */
io.changenow.IChangeNowAspectsCaster.prototype.asIChangeNow

/** @typedef {io.changenow.IChangeNow.Initialese} io.changenow.IHyperChangeNow.Initialese */

/** @typedef {function(new: io.changenow.HyperChangeNow)} io.changenow.AbstractHyperChangeNow.constructor */
/** @typedef {typeof io.changenow.HyperChangeNow} io.changenow.HyperChangeNow.typeof */
/**
 * An abstract class of `io.changenow.IHyperChangeNow` interface.
 * @constructor io.changenow.AbstractHyperChangeNow
 */
io.changenow.AbstractHyperChangeNow = class extends /** @type {io.changenow.AbstractHyperChangeNow.constructor&io.changenow.HyperChangeNow.typeof} */ (class {}) { }
io.changenow.AbstractHyperChangeNow.prototype.constructor = io.changenow.AbstractHyperChangeNow
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.changenow.AbstractHyperChangeNow.class = /** @type {typeof io.changenow.AbstractHyperChangeNow} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!io.changenow.IHyperChangeNow|typeof io.changenow.HyperChangeNow)|(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.HyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!io.changenow.IHyperChangeNow|typeof io.changenow.HyperChangeNow)|(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!io.changenow.IHyperChangeNow|typeof io.changenow.HyperChangeNow)|(!io.changenow.IChangeNow|typeof io.changenow.ChangeNow)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.AbstractHyperChangeNow.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!io.changenow.IChangeNowAspects|!Array<!io.changenow.IChangeNowAspects>|function(new: io.changenow.IChangeNowAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the IChangeNow to implement aspects.
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof io.changenow.AbstractHyperChangeNow}
 * @nosideeffects
 */
io.changenow.AbstractHyperChangeNow.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !io.changenow.IHyperChangeNow.Initialese[]) => io.changenow.IHyperChangeNow} io.changenow.HyperChangeNowConstructor */

/** @typedef {function(new: engineering.type.IEngineer&io.changenow.IHyperChangeNowCaster&io.changenow.IChangeNow)} io.changenow.IHyperChangeNow.constructor */
/** @typedef {typeof io.changenow.IChangeNow} io.changenow.IChangeNow.typeof */
/** @interface io.changenow.IHyperChangeNow */
io.changenow.IHyperChangeNow = class extends /** @type {io.changenow.IHyperChangeNow.constructor&engineering.type.IEngineer.typeof&io.changenow.IChangeNow.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperChangeNow* instance and automatically initialise it with options.
 * @param {...!io.changenow.IHyperChangeNow.Initialese} init The initialisation options.
 */
io.changenow.IHyperChangeNow.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.changenow.IHyperChangeNow&engineering.type.IInitialiser<!io.changenow.IHyperChangeNow.Initialese>)} io.changenow.HyperChangeNow.constructor */
/** @typedef {typeof io.changenow.IHyperChangeNow} io.changenow.IHyperChangeNow.typeof */
/**
 * A concrete class of _IHyperChangeNow_ instances.
 * @constructor io.changenow.HyperChangeNow
 * @implements {io.changenow.IHyperChangeNow} ‎
 * @implements {engineering.type.IInitialiser<!io.changenow.IHyperChangeNow.Initialese>} ‎
 */
io.changenow.HyperChangeNow = class extends /** @type {io.changenow.HyperChangeNow.constructor&io.changenow.IHyperChangeNow.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperChangeNow* instance and automatically initialise it with options.
   * @param {...!io.changenow.IHyperChangeNow.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperChangeNow* instance and automatically initialise it with options.
 * @param {...!io.changenow.IHyperChangeNow.Initialese} init The initialisation options.
 */
io.changenow.HyperChangeNow.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.changenow.HyperChangeNow}
 */
io.changenow.HyperChangeNow.__extend = function(...Extensions) {}

/** @typedef {io.changenow.IHyperChangeNow} */
io.changenow.RecordIHyperChangeNow

/** @typedef {io.changenow.IHyperChangeNow} io.changenow.BoundIHyperChangeNow */

/** @typedef {io.changenow.HyperChangeNow} io.changenow.BoundHyperChangeNow */

/**
 * Contains getters to cast the _IHyperChangeNow_ interface.
 * @interface io.changenow.IHyperChangeNowCaster
 */
io.changenow.IHyperChangeNowCaster = class { }
/**
 * Cast the _IHyperChangeNow_ instance into the _BoundIHyperChangeNow_ type.
 * @type {!io.changenow.BoundIHyperChangeNow}
 */
io.changenow.IHyperChangeNowCaster.prototype.asIHyperChangeNow
/**
 * Access the _HyperChangeNow_ prototype.
 * @type {!io.changenow.BoundHyperChangeNow}
 */
io.changenow.IHyperChangeNowCaster.prototype.superHyperChangeNow

/** @typedef {function(new: io.changenow.IChangeNowFields&engineering.type.IEngineer&io.changenow.IChangeNowCaster&engineer.type.INetwork&io.changenow.UChangeNow)} io.changenow.IChangeNow.constructor */
/** @typedef {typeof engineer.type.INetwork} engineer.type.INetwork.typeof */
/**
 * The _ChangeNow_ client.
 * @interface io.changenow.IChangeNow
 */
io.changenow.IChangeNow = class extends /** @type {io.changenow.IChangeNow.constructor&engineering.type.IEngineer.typeof&engineer.type.INetwork.typeof&io.changenow.UChangeNow.typeof} */ (class {}) {
}
/**
 * Create a new *IChangeNow* instance and automatically initialise it with options.
 * @param {...!io.changenow.IChangeNow.Initialese} init The initialisation options.
 */
io.changenow.IChangeNow.prototype.constructor = function(...init) {}
/** @type {io.changenow.IChangeNow.GetExchangeAmount} */
io.changenow.IChangeNow.prototype.GetExchangeAmount = function() {}
/** @type {io.changenow.IChangeNow.GetFixedExchangeAmount} */
io.changenow.IChangeNow.prototype.GetFixedExchangeAmount = function() {}

/** @typedef {function(new: io.changenow.IChangeNow&engineering.type.IInitialiser<!io.changenow.IChangeNow.Initialese>)} io.changenow.ChangeNow.constructor */
/**
 * A concrete class of _IChangeNow_ instances.
 * @constructor io.changenow.ChangeNow
 * @implements {io.changenow.IChangeNow} The _ChangeNow_ client.
 * @implements {engineering.type.IInitialiser<!io.changenow.IChangeNow.Initialese>} ‎
 */
io.changenow.ChangeNow = class extends /** @type {io.changenow.ChangeNow.constructor&io.changenow.IChangeNow.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangeNow* instance and automatically initialise it with options.
   * @param {...!io.changenow.IChangeNow.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangeNow* instance and automatically initialise it with options.
 * @param {...!io.changenow.IChangeNow.Initialese} init The initialisation options.
 */
io.changenow.ChangeNow.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.changenow.ChangeNow}
 */
io.changenow.ChangeNow.__extend = function(...Extensions) {}

/**
 * Fields of the IChangeNow.
 * @interface io.changenow.IChangeNowFields
 */
io.changenow.IChangeNowFields = class { }
/**
 * The _ChangeNow_ instance host. Default `https://api.changenow.io`.
 */
io.changenow.IChangeNowFields.prototype.host = /** @type {string} */ (void 0)
/**
 * The path after the host to use for request. Default `/v2`.
 */
io.changenow.IChangeNowFields.prototype.apiPath = /** @type {string} */ (void 0)
/**
 * The client id. Default empty string.
 */
io.changenow.IChangeNowFields.prototype.changeNowApiKey = /** @type {string} */ (void 0)

/** @typedef {io.changenow.IChangeNow} */
io.changenow.RecordIChangeNow

/** @typedef {io.changenow.IChangeNow} io.changenow.BoundIChangeNow */

/** @typedef {io.changenow.ChangeNow} io.changenow.BoundChangeNow */

/**
 * Contains getters to cast the _IChangeNow_ interface.
 * @interface io.changenow.IChangeNowCaster
 */
io.changenow.IChangeNowCaster = class { }
/**
 * Cast the _IChangeNow_ instance into the _BoundIChangeNow_ type.
 * @type {!io.changenow.BoundIChangeNow}
 */
io.changenow.IChangeNowCaster.prototype.asIChangeNow
/**
 * Access the _ChangeNow_ prototype.
 * @type {!io.changenow.BoundChangeNow}
 */
io.changenow.IChangeNowCaster.prototype.superChangeNow

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__before_GetExchangeAmount<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._before_GetExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.before_GetExchangeAmount} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.BeforeGetExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetExchangeAmount_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangeNow.GetExchangeAmountNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `GetExchangeAmount` method from being executed.
 * - `sub` _(value: !Promise&lt;IChangeNow.GetExchangeAmount.Return&gt;) =&gt; void_ Cancels a call to `GetExchangeAmount` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.before_GetExchangeAmount = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmount<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmount} */
/**
 * After the method. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterGetExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetExchangeAmount_ at the `after` joinpoint.
 * - `res` _IChangeNow.GetExchangeAmount.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmount = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountThrows<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountThrows */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetExchangeAmount_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `GetExchangeAmount` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountReturns<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountReturns */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetExchangeAmount_ at the `afterReturns` joinpoint.
 * - `res` _IChangeNow.GetExchangeAmount.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;IChangeNow.GetExchangeAmount.Return&gt;&vert;IChangeNow.GetExchangeAmount.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetExchangeAmountCancels<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetExchangeAmountCancels */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetExchangeAmount_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetExchangeAmountCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetExchangeAmount<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetExchangeAmount} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetExchangeAmount_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;IChangeNow.GetExchangeAmount.Return&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetExchangeAmount = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__before_GetFixedExchangeAmount<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._before_GetFixedExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.before_GetFixedExchangeAmount} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.BeforeGetFixedExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetFixedExchangeAmount_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangeNow.GetFixedExchangeAmountNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `GetFixedExchangeAmount` method from being executed.
 * - `sub` _(value: !Promise&lt;IChangeNow.GetFixedExchangeAmount.Return&gt;) =&gt; void_ Cancels a call to `GetFixedExchangeAmount` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetFixedExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.before_GetFixedExchangeAmount = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmount<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmount} */
/**
 * After the method. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterGetFixedExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetFixedExchangeAmount_ at the `after` joinpoint.
 * - `res` _IChangeNow.GetFixedExchangeAmount.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetFixedExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmount = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountThrows<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountThrows */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterThrowsGetFixedExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetFixedExchangeAmount_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `GetFixedExchangeAmount` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetFixedExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountReturns<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountReturns */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterReturnsGetFixedExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetFixedExchangeAmount_ at the `afterReturns` joinpoint.
 * - `res` _IChangeNow.GetFixedExchangeAmount.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;IChangeNow.GetFixedExchangeAmount.Return&gt;&vert;IChangeNow.GetFixedExchangeAmount.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetFixedExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__after_GetFixedExchangeAmountCancels<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._after_GetFixedExchangeAmountCancels */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.AfterCancelsGetFixedExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetFixedExchangeAmount_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetFixedExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.after_GetFixedExchangeAmountCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData) => void} io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNowJoinpointModel.__immediatelyAfter_GetFixedExchangeAmount<!io.changenow.IChangeNowJoinpointModel>} io.changenow.IChangeNowJoinpointModel._immediatelyAfter_GetFixedExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetFixedExchangeAmount} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!io.changenow.IChangeNowJoinpointModel.ImmediatelyAfterGetFixedExchangeAmountPointcutData} [data] Metadata passed to the pointcuts of _GetFixedExchangeAmount_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;IChangeNow.GetFixedExchangeAmount.Return&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `args` _IChangeNow.GetFixedExchangeAmountNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangeNowJoinpointModel.GetFixedExchangeAmountPointcutData*
 * @return {void}
 */
io.changenow.IChangeNowJoinpointModel.immediatelyAfter_GetFixedExchangeAmount = function(data) {}

/**
 * @typedef {(this: THIS, data: !io.changenow.IChangeNow.GetExchangeAmount.Data) => !Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>} io.changenow.IChangeNow.__GetExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNow.__GetExchangeAmount<!io.changenow.IChangeNow>} io.changenow.IChangeNow._GetExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNow.GetExchangeAmount} */
/**
 * Returns estimated exchange amount for the exchange and some additional
 * fields. Accepts to and from currencies, currencies' networks, exchange
 * flow, and RateID.
 * @param {!io.changenow.IChangeNow.GetExchangeAmount.Data} data The data.
 * - `fromCurrency` _string_
 * - `toCurrency` _string_
 * - `fromAmount` _number_
 * - `[fromNetwork]` _string?_
 * - `[toNetwork]` _string?_
 * - `[toAmount]` _number?_
 * @return {!Promise<io.changenow.IChangeNow.GetExchangeAmount.Return>}
 */
io.changenow.IChangeNow.GetExchangeAmount = function(data) {}

/**
 * @typedef {Object} io.changenow.IChangeNow.GetExchangeAmount.Data The data.
 * @prop {string} fromCurrency
 * @prop {string} toCurrency
 * @prop {number} fromAmount
 * @prop {string} [fromNetwork]
 * @prop {string} [toNetwork]
 * @prop {number} [toAmount]
 */

/**
 * @typedef {Object} io.changenow.IChangeNow.GetExchangeAmount.Return
 * @prop {string} fromCurrency Example: `btc`.
 * @prop {string} fromNetwork Example: `btc`.
 * @prop {string} toCurrency Example: `eth`.
 * @prop {string} toNetwork Example: `eth`.
 * @prop {string} type Example: `direct`.
 * @prop {string} transactionSpeedForecast Example: `10-60`.
 * @prop {?*} warningMessage Example: `null`.
 * @prop {number} depositFee Example: `0.0002328`.
 * @prop {number} withdrawalFee Example: `0.000704`.
 * @prop {?*} userId Example: `null`.
 * @prop {number} fromAmount Example: `0.007`.
 * @prop {number} toAmount Example: `0.112939`.
 */

/**
 * @typedef {(this: THIS, data: !io.changenow.IChangeNow.GetFixedExchangeAmount.Data) => !Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>} io.changenow.IChangeNow.__GetFixedExchangeAmount
 * @template THIS
 */
/** @typedef {io.changenow.IChangeNow.__GetFixedExchangeAmount<!io.changenow.IChangeNow>} io.changenow.IChangeNow._GetFixedExchangeAmount */
/** @typedef {typeof io.changenow.IChangeNow.GetFixedExchangeAmount} */
/**
 * Returns the fixed-rate exchange amount for the exchange and some additional
 * fields. Accepts to and from currencies, currencies' networks, exchange
 * flow, and RateID.
 * @param {!io.changenow.IChangeNow.GetFixedExchangeAmount.Data} data The data.
 * - `fromCurrency` _string_
 * - `toCurrency` _string_
 * - `fromAmount` _number_
 * - `toAmount` _number_
 * - `[fromNetwork]` _string?_
 * - `[toNetwork]` _string?_
 * @return {!Promise<io.changenow.IChangeNow.GetFixedExchangeAmount.Return>}
 */
io.changenow.IChangeNow.GetFixedExchangeAmount = function(data) {}

/**
 * @typedef {Object} io.changenow.IChangeNow.GetFixedExchangeAmount.Data The data.
 * @prop {string} fromCurrency
 * @prop {string} toCurrency
 * @prop {number} fromAmount
 * @prop {number} toAmount
 * @prop {string} [fromNetwork]
 * @prop {string} [toNetwork]
 */

/**
 * @typedef {Object} io.changenow.IChangeNow.GetFixedExchangeAmount.Return
 * @prop {string} fromCurrency Example: `btc`.
 * @prop {string} fromNetwork Example: `btc`.
 * @prop {string} toCurrency Example: `eth`.
 * @prop {string} toNetwork Example: `eth`.
 * @prop {string} type Example: `direct`.
 * @prop {*} rateId Example: `kHgL5dOtiFOA4sJoeJLgeQyUCPSaAWFy5drL5+LkBE`.
 * @prop {!Date} validUntil Example: `2024-01-18T13:22:55.223Z`.
 * @prop {?string} transactionSpeedForecast Example: `10-60`.
 * @prop {?*} warningMessage Example: `null`.
 * @prop {number} depositFee Example: `0.0002328`.
 * @prop {number} withdrawalFee Example: `0.000704`.
 * @prop {?*} userId Example: `null`.
 * @prop {number} fromAmount Example: `0.007`.
 * @prop {number} toAmount Example: `0.112939`.
 */

/**
 * @typedef {Object} $io.letsexchange.ILetsExchange.Initialese
 * @prop {string} [host] The _LetsExchange_ instance host.
 * @prop {string} [apiPath] The path after the host to use for request.
 * @prop {string} [letsExchangeApiKey] The client id.
 */
/** @typedef {$io.letsexchange.ILetsExchange.Initialese&engineer.type.INetwork.Initialese} io.letsexchange.ILetsExchange.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: io.letsexchange.LetsExchange)} io.letsexchange.AbstractLetsExchange.constructor */
/** @typedef {typeof io.letsexchange.LetsExchange} io.letsexchange.LetsExchange.typeof */
/**
 * An abstract class of `io.letsexchange.ILetsExchange` interface.
 * @constructor io.letsexchange.AbstractLetsExchange
 */
io.letsexchange.AbstractLetsExchange = class extends /** @type {io.letsexchange.AbstractLetsExchange.constructor&io.letsexchange.LetsExchange.typeof} */ (class {}) { }
io.letsexchange.AbstractLetsExchange.prototype.constructor = io.letsexchange.AbstractLetsExchange
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.letsexchange.AbstractLetsExchange.class = /** @type {typeof io.letsexchange.AbstractLetsExchange} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchange.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.letsexchange.AbstractLetsExchange}
 */
io.letsexchange.AbstractLetsExchange.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.AbstractLetsExchange.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.AbstractLetsExchange.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)|(!engineer.type.INetwork|typeof engineer.type.Network)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.AbstractLetsExchange.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface io.letsexchange.ILetsExchangeJoinpointModelHyperslice
 */
io.letsexchange.ILetsExchangeJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_GetInfo=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel._before_GetInfo|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._before_GetInfo>} */ (void 0)
    /**
     * After the method.
     */
    this.after_GetInfo=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfo|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfo>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_GetInfoThrows=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoThrows|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_GetInfoReturns=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoReturns|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_GetInfoCancels=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoCancels|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_GetInfo=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel._immediatelyAfter_GetInfo|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel._immediatelyAfter_GetInfo>} */ (void 0)
  }
}
io.letsexchange.ILetsExchangeJoinpointModelHyperslice.prototype.constructor = io.letsexchange.ILetsExchangeJoinpointModelHyperslice

/**
 * A concrete class of _ILetsExchangeJoinpointModelHyperslice_ instances.
 * @constructor io.letsexchange.LetsExchangeJoinpointModelHyperslice
 * @implements {io.letsexchange.ILetsExchangeJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
io.letsexchange.LetsExchangeJoinpointModelHyperslice = class extends io.letsexchange.ILetsExchangeJoinpointModelHyperslice { }
io.letsexchange.LetsExchangeJoinpointModelHyperslice.prototype.constructor = io.letsexchange.LetsExchangeJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice
 * @template THIS
 */
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_GetInfo=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_GetInfo=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_GetInfoThrows=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_GetInfoReturns=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_GetInfoCancels=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_GetInfo=/** @type {!io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo<THIS>|!engineering.type.RecursiveArray<!io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo<THIS>>} */ (void 0)
  }
}
io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.prototype.constructor = io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice

/**
 * A concrete class of _ILetsExchangeJoinpointModelBindingHyperslice_ instances.
 * @constructor io.letsexchange.LetsExchangeJoinpointModelBindingHyperslice
 * @implements {io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice<THIS>}
 */
io.letsexchange.LetsExchangeJoinpointModelBindingHyperslice = class extends io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice { }
io.letsexchange.LetsExchangeJoinpointModelBindingHyperslice.prototype.constructor = io.letsexchange.LetsExchangeJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `ILetsExchange`'s methods.
 * @interface io.letsexchange.ILetsExchangeJoinpointModel
 */
io.letsexchange.ILetsExchangeJoinpointModel = class { }
/** @type {io.letsexchange.ILetsExchangeJoinpointModel.before_GetInfo} */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.before_GetInfo = function() {}
/** @type {io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfo} */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfo = function() {}
/** @type {io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoThrows} */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfoThrows = function() {}
/** @type {io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoReturns} */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfoReturns = function() {}
/** @type {io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoCancels} */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.after_GetInfoCancels = function() {}
/** @type {io.letsexchange.ILetsExchangeJoinpointModel.immediatelyAfter_GetInfo} */
io.letsexchange.ILetsExchangeJoinpointModel.prototype.immediatelyAfter_GetInfo = function() {}

/**
 * A concrete class of _ILetsExchangeJoinpointModel_ instances.
 * @constructor io.letsexchange.LetsExchangeJoinpointModel
 * @implements {io.letsexchange.ILetsExchangeJoinpointModel} An interface that enumerates the joinpoints of `ILetsExchange`'s methods.
 */
io.letsexchange.LetsExchangeJoinpointModel = class extends io.letsexchange.ILetsExchangeJoinpointModel { }
io.letsexchange.LetsExchangeJoinpointModel.prototype.constructor = io.letsexchange.LetsExchangeJoinpointModel

/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel} */
io.letsexchange.RecordILetsExchangeJoinpointModel

/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel} io.letsexchange.BoundILetsExchangeJoinpointModel */

/** @typedef {io.letsexchange.LetsExchangeJoinpointModel} io.letsexchange.BoundLetsExchangeJoinpointModel */

/** @typedef {typeof __$te_plain} io.letsexchange.ILetsExchangeAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: io.letsexchange.LetsExchangeAspectsInstaller)} io.letsexchange.AbstractLetsExchangeAspectsInstaller.constructor */
/** @typedef {typeof io.letsexchange.LetsExchangeAspectsInstaller} io.letsexchange.LetsExchangeAspectsInstaller.typeof */
/**
 * An abstract class of `io.letsexchange.ILetsExchangeAspectsInstaller` interface.
 * @constructor io.letsexchange.AbstractLetsExchangeAspectsInstaller
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller = class extends /** @type {io.letsexchange.AbstractLetsExchangeAspectsInstaller.constructor&io.letsexchange.LetsExchangeAspectsInstaller.typeof} */ (class {}) { }
io.letsexchange.AbstractLetsExchangeAspectsInstaller.prototype.constructor = io.letsexchange.AbstractLetsExchangeAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.class = /** @type {typeof io.letsexchange.AbstractLetsExchangeAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!io.letsexchange.ILetsExchangeAspectsInstaller|typeof io.letsexchange.LetsExchangeAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.letsexchange.AbstractLetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!io.letsexchange.ILetsExchangeAspectsInstaller|typeof io.letsexchange.LetsExchangeAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!io.letsexchange.ILetsExchangeAspectsInstaller|typeof io.letsexchange.LetsExchangeAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.AbstractLetsExchangeAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !io.letsexchange.ILetsExchangeAspectsInstaller.Initialese[]) => io.letsexchange.ILetsExchangeAspectsInstaller} io.letsexchange.LetsExchangeAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} io.letsexchange.ILetsExchangeAspectsInstaller.constructor */
/** @interface io.letsexchange.ILetsExchangeAspectsInstaller */
io.letsexchange.ILetsExchangeAspectsInstaller = class extends /** @type {io.letsexchange.ILetsExchangeAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.before_GetInfo=/** @type {number} */ (void 0)
    this.after_GetInfo=/** @type {number} */ (void 0)
    this.after_GetInfoThrows=/** @type {number} */ (void 0)
    this.after_GetInfoReturns=/** @type {number} */ (void 0)
    this.after_GetInfoCancels=/** @type {number} */ (void 0)
    this.immediateAfter_GetInfo=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  GetInfo() { }
}
/**
 * Create a new *ILetsExchangeAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese} init The initialisation options.
 */
io.letsexchange.ILetsExchangeAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.letsexchange.ILetsExchangeAspectsInstaller&engineering.type.IInitialiser<!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese>)} io.letsexchange.LetsExchangeAspectsInstaller.constructor */
/** @typedef {typeof io.letsexchange.ILetsExchangeAspectsInstaller} io.letsexchange.ILetsExchangeAspectsInstaller.typeof */
/**
 * A concrete class of _ILetsExchangeAspectsInstaller_ instances.
 * @constructor io.letsexchange.LetsExchangeAspectsInstaller
 * @implements {io.letsexchange.ILetsExchangeAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese>} ‎
 */
io.letsexchange.LetsExchangeAspectsInstaller = class extends /** @type {io.letsexchange.LetsExchangeAspectsInstaller.constructor&io.letsexchange.ILetsExchangeAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ILetsExchangeAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ILetsExchangeAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ILetsExchangeAspectsInstaller.Initialese} init The initialisation options.
 */
io.letsexchange.LetsExchangeAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeAspectsInstaller}
 */
io.letsexchange.LetsExchangeAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} io.letsexchange.ILetsExchange.GetInfoNArgs
 * @prop {!io.letsexchange.ILetsExchange.GetInfo.Data} data The data.
 */

/**
 * @typedef {Object} io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {io.letsexchange.ILetsExchange.GetInfoNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: io.letsexchange.ILetsExchange.GetInfoNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `GetInfo` method from being executed.
 * @prop {(value: !Promise<io.letsexchange.ILetsExchange.GetInfo.Return>) => void} sub Cancels a call to `GetInfo` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData&io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData} io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData
 * @prop {io.letsexchange.ILetsExchange.GetInfo.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData&io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData} io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `GetInfo` (use only at the top-level of program flow).
 */
/** @typedef {$io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData&io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData} io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData
 * @prop {io.letsexchange.ILetsExchange.GetInfo.Return} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<io.letsexchange.ILetsExchange.GetInfo.Return>|io.letsexchange.ILetsExchange.GetInfo.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData&io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData} io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData&io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData} io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData
 * @prop {!Promise<io.letsexchange.ILetsExchange.GetInfo.Return>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData&io.letsexchange.ILetsExchangeJoinpointModel.GetInfoPointcutData} io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/** @typedef {new (...args: !io.letsexchange.ILetsExchange.Initialese[]) => io.letsexchange.ILetsExchange} io.letsexchange.LetsExchangeConstructor */

/** @typedef {symbol} io.letsexchange.LetsExchangeMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} io.letsexchange.ULetsExchange.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {io.letsexchange.ILetsExchange} [letsExchange] The _LetsExchange_ client.
 */

/** @typedef {function(new: io.letsexchange.LetsExchangeUniversal)} io.letsexchange.AbstractLetsExchangeUniversal.constructor */
/** @typedef {typeof io.letsexchange.LetsExchangeUniversal} io.letsexchange.LetsExchangeUniversal.typeof */
/**
 * An abstract class of `io.letsexchange.ULetsExchange` interface.
 * @constructor io.letsexchange.AbstractLetsExchangeUniversal
 */
io.letsexchange.AbstractLetsExchangeUniversal = class extends /** @type {io.letsexchange.AbstractLetsExchangeUniversal.constructor&io.letsexchange.LetsExchangeUniversal.typeof} */ (class {}) { }
io.letsexchange.AbstractLetsExchangeUniversal.prototype.constructor = io.letsexchange.AbstractLetsExchangeUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.letsexchange.AbstractLetsExchangeUniversal.class = /** @type {typeof io.letsexchange.AbstractLetsExchangeUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchangeUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.letsexchange.AbstractLetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.AbstractLetsExchangeUniversal.__trait = function(...Implementations) {}

/** @typedef {io.letsexchange.LetsExchangeMetaUniversal} io.letsexchange.AbstractLetsExchangeUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !io.letsexchange.ULetsExchange.Initialese[]) => io.letsexchange.ULetsExchange} io.letsexchange.ULetsExchangeConstructor */

/** @typedef {function(new: io.letsexchange.ULetsExchangeFields&engineering.type.IEngineer&io.letsexchange.ULetsExchangeCaster)} io.letsexchange.ULetsExchange.constructor */
/**
 * A trait that allows to access an _ILetsExchange_ object via a `.letsExchange` field.
 * @interface io.letsexchange.ULetsExchange
 */
io.letsexchange.ULetsExchange = class extends /** @type {io.letsexchange.ULetsExchange.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *ULetsExchange* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ULetsExchange.Initialese} init The initialisation options.
 */
io.letsexchange.ULetsExchange.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.letsexchange.ULetsExchange&engineering.type.IInitialiser<!io.letsexchange.ULetsExchange.Initialese>)} io.letsexchange.LetsExchangeUniversal.constructor */
/** @typedef {typeof io.letsexchange.ULetsExchange} io.letsexchange.ULetsExchange.typeof */
/**
 * A concrete class of _ULetsExchange_ instances.
 * @constructor io.letsexchange.LetsExchangeUniversal
 * @implements {io.letsexchange.ULetsExchange} A trait that allows to access an _ILetsExchange_ object via a `.letsExchange` field.
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ULetsExchange.Initialese>} ‎
 */
io.letsexchange.LetsExchangeUniversal = class extends /** @type {io.letsexchange.LetsExchangeUniversal.constructor&io.letsexchange.ULetsExchange.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ULetsExchange* instance and automatically initialise it with options.
   * @param {...!io.letsexchange.ULetsExchange.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ULetsExchange* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ULetsExchange.Initialese} init The initialisation options.
 */
io.letsexchange.LetsExchangeUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeUniversal}
 */
io.letsexchange.LetsExchangeUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the ULetsExchange.
 * @interface io.letsexchange.ULetsExchangeFields
 */
io.letsexchange.ULetsExchangeFields = class { }
/**
 * The _LetsExchange_ client. Default `null`.
 */
io.letsexchange.ULetsExchangeFields.prototype.letsExchange = /** @type {io.letsexchange.ILetsExchange} */ (void 0)

/** @typedef {io.letsexchange.ULetsExchange} */
io.letsexchange.RecordULetsExchange

/** @typedef {io.letsexchange.ULetsExchange} io.letsexchange.BoundULetsExchange */

/** @typedef {io.letsexchange.LetsExchangeUniversal} io.letsexchange.BoundLetsExchangeUniversal */

/**
 * Contains getters to cast the _ULetsExchange_ interface.
 * @interface io.letsexchange.ULetsExchangeCaster
 */
io.letsexchange.ULetsExchangeCaster = class { }
/**
 * Provides direct access to _ULetsExchange_ via the _BoundULetsExchange_ universal.
 * @type {!io.letsexchange.BoundLetsExchange}
 */
io.letsexchange.ULetsExchangeCaster.prototype.asLetsExchange
/**
 * Cast the _ULetsExchange_ instance into the _BoundULetsExchange_ type.
 * @type {!io.letsexchange.BoundULetsExchange}
 */
io.letsexchange.ULetsExchangeCaster.prototype.asULetsExchange
/**
 * Access the _LetsExchangeUniversal_ prototype.
 * @type {!io.letsexchange.BoundLetsExchangeUniversal}
 */
io.letsexchange.ULetsExchangeCaster.prototype.superLetsExchangeUniversal

/** @typedef {function(new: io.letsexchange.BLetsExchangeAspectsCaster<THIS>&io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice<THIS>)} io.letsexchange.BLetsExchangeAspects.constructor */
/** @typedef {typeof io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice} io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *ILetsExchange* that bind to an instance.
 * @interface io.letsexchange.BLetsExchangeAspects
 * @template THIS
 */
io.letsexchange.BLetsExchangeAspects = class extends /** @type {io.letsexchange.BLetsExchangeAspects.constructor&io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
io.letsexchange.BLetsExchangeAspects.prototype.constructor = io.letsexchange.BLetsExchangeAspects

/** @typedef {typeof __$te_plain} io.letsexchange.ILetsExchangeAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: io.letsexchange.LetsExchangeAspects)} io.letsexchange.AbstractLetsExchangeAspects.constructor */
/** @typedef {typeof io.letsexchange.LetsExchangeAspects} io.letsexchange.LetsExchangeAspects.typeof */
/**
 * An abstract class of `io.letsexchange.ILetsExchangeAspects` interface.
 * @constructor io.letsexchange.AbstractLetsExchangeAspects
 */
io.letsexchange.AbstractLetsExchangeAspects = class extends /** @type {io.letsexchange.AbstractLetsExchangeAspects.constructor&io.letsexchange.LetsExchangeAspects.typeof} */ (class {}) { }
io.letsexchange.AbstractLetsExchangeAspects.prototype.constructor = io.letsexchange.AbstractLetsExchangeAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.letsexchange.AbstractLetsExchangeAspects.class = /** @type {typeof io.letsexchange.AbstractLetsExchangeAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!io.letsexchange.ILetsExchangeAspects|typeof io.letsexchange.LetsExchangeAspects)|(!io.letsexchange.BLetsExchangeAspects|typeof io.letsexchange.BLetsExchangeAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 * @nosideeffects
 */
io.letsexchange.AbstractLetsExchangeAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.letsexchange.AbstractLetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!io.letsexchange.ILetsExchangeAspects|typeof io.letsexchange.LetsExchangeAspects)|(!io.letsexchange.BLetsExchangeAspects|typeof io.letsexchange.BLetsExchangeAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!io.letsexchange.ILetsExchangeAspects|typeof io.letsexchange.LetsExchangeAspects)|(!io.letsexchange.BLetsExchangeAspects|typeof io.letsexchange.BLetsExchangeAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.AbstractLetsExchangeAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !io.letsexchange.ILetsExchangeAspects.Initialese[]) => io.letsexchange.ILetsExchangeAspects} io.letsexchange.LetsExchangeAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&io.letsexchange.ILetsExchangeAspectsCaster&io.letsexchange.BLetsExchangeAspects<!io.letsexchange.ILetsExchangeAspects>)} io.letsexchange.ILetsExchangeAspects.constructor */
/** @typedef {typeof io.letsexchange.BLetsExchangeAspects} io.letsexchange.BLetsExchangeAspects.typeof */
/**
 * The aspects of the *ILetsExchange*.
 * @interface io.letsexchange.ILetsExchangeAspects
 */
io.letsexchange.ILetsExchangeAspects = class extends /** @type {io.letsexchange.ILetsExchangeAspects.constructor&engineering.type.IEngineer.typeof&io.letsexchange.BLetsExchangeAspects.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.props=/** @type {io.letsexchange.ILetsExchangeAspects} */ (void 0)
  }
}
/**
 * Create a new *ILetsExchangeAspects* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ILetsExchangeAspects.Initialese} init The initialisation options.
 */
io.letsexchange.ILetsExchangeAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.letsexchange.ILetsExchangeAspects&engineering.type.IInitialiser<!io.letsexchange.ILetsExchangeAspects.Initialese>)} io.letsexchange.LetsExchangeAspects.constructor */
/** @typedef {typeof io.letsexchange.ILetsExchangeAspects} io.letsexchange.ILetsExchangeAspects.typeof */
/**
 * A concrete class of _ILetsExchangeAspects_ instances.
 * @constructor io.letsexchange.LetsExchangeAspects
 * @implements {io.letsexchange.ILetsExchangeAspects} The aspects of the *ILetsExchange*.
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ILetsExchangeAspects.Initialese>} ‎
 */
io.letsexchange.LetsExchangeAspects = class extends /** @type {io.letsexchange.LetsExchangeAspects.constructor&io.letsexchange.ILetsExchangeAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ILetsExchangeAspects* instance and automatically initialise it with options.
   * @param {...!io.letsexchange.ILetsExchangeAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ILetsExchangeAspects* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ILetsExchangeAspects.Initialese} init The initialisation options.
 */
io.letsexchange.LetsExchangeAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchangeAspects}
 */
io.letsexchange.LetsExchangeAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BLetsExchangeAspects_ interface.
 * @interface io.letsexchange.BLetsExchangeAspectsCaster
 * @template THIS
 */
io.letsexchange.BLetsExchangeAspectsCaster = class { }
/**
 * Cast the _BLetsExchangeAspects_ instance into the _BoundILetsExchange_ type.
 * @type {!io.letsexchange.BoundILetsExchange}
 */
io.letsexchange.BLetsExchangeAspectsCaster.prototype.asILetsExchange

/**
 * Contains getters to cast the _ILetsExchangeAspects_ interface.
 * @interface io.letsexchange.ILetsExchangeAspectsCaster
 */
io.letsexchange.ILetsExchangeAspectsCaster = class { }
/**
 * Cast the _ILetsExchangeAspects_ instance into the _BoundILetsExchange_ type.
 * @type {!io.letsexchange.BoundILetsExchange}
 */
io.letsexchange.ILetsExchangeAspectsCaster.prototype.asILetsExchange

/** @typedef {io.letsexchange.ILetsExchange.Initialese} io.letsexchange.IHyperLetsExchange.Initialese */

/** @typedef {function(new: io.letsexchange.HyperLetsExchange)} io.letsexchange.AbstractHyperLetsExchange.constructor */
/** @typedef {typeof io.letsexchange.HyperLetsExchange} io.letsexchange.HyperLetsExchange.typeof */
/**
 * An abstract class of `io.letsexchange.IHyperLetsExchange` interface.
 * @constructor io.letsexchange.AbstractHyperLetsExchange
 */
io.letsexchange.AbstractHyperLetsExchange = class extends /** @type {io.letsexchange.AbstractHyperLetsExchange.constructor&io.letsexchange.HyperLetsExchange.typeof} */ (class {}) { }
io.letsexchange.AbstractHyperLetsExchange.prototype.constructor = io.letsexchange.AbstractHyperLetsExchange
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
io.letsexchange.AbstractHyperLetsExchange.class = /** @type {typeof io.letsexchange.AbstractHyperLetsExchange} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!io.letsexchange.IHyperLetsExchange|typeof io.letsexchange.HyperLetsExchange)|(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.HyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!io.letsexchange.IHyperLetsExchange|typeof io.letsexchange.HyperLetsExchange)|(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!io.letsexchange.IHyperLetsExchange|typeof io.letsexchange.HyperLetsExchange)|(!io.letsexchange.ILetsExchange|typeof io.letsexchange.LetsExchange)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.AbstractHyperLetsExchange.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!io.letsexchange.ILetsExchangeAspects|!Array<!io.letsexchange.ILetsExchangeAspects>|function(new: io.letsexchange.ILetsExchangeAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the ILetsExchange to implement aspects.
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof io.letsexchange.AbstractHyperLetsExchange}
 * @nosideeffects
 */
io.letsexchange.AbstractHyperLetsExchange.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !io.letsexchange.IHyperLetsExchange.Initialese[]) => io.letsexchange.IHyperLetsExchange} io.letsexchange.HyperLetsExchangeConstructor */

/** @typedef {function(new: engineering.type.IEngineer&io.letsexchange.IHyperLetsExchangeCaster&io.letsexchange.ILetsExchange)} io.letsexchange.IHyperLetsExchange.constructor */
/** @typedef {typeof io.letsexchange.ILetsExchange} io.letsexchange.ILetsExchange.typeof */
/** @interface io.letsexchange.IHyperLetsExchange */
io.letsexchange.IHyperLetsExchange = class extends /** @type {io.letsexchange.IHyperLetsExchange.constructor&engineering.type.IEngineer.typeof&io.letsexchange.ILetsExchange.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperLetsExchange* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.IHyperLetsExchange.Initialese} init The initialisation options.
 */
io.letsexchange.IHyperLetsExchange.prototype.constructor = function(...init) {}

/** @typedef {function(new: io.letsexchange.IHyperLetsExchange&engineering.type.IInitialiser<!io.letsexchange.IHyperLetsExchange.Initialese>)} io.letsexchange.HyperLetsExchange.constructor */
/** @typedef {typeof io.letsexchange.IHyperLetsExchange} io.letsexchange.IHyperLetsExchange.typeof */
/**
 * A concrete class of _IHyperLetsExchange_ instances.
 * @constructor io.letsexchange.HyperLetsExchange
 * @implements {io.letsexchange.IHyperLetsExchange} ‎
 * @implements {engineering.type.IInitialiser<!io.letsexchange.IHyperLetsExchange.Initialese>} ‎
 */
io.letsexchange.HyperLetsExchange = class extends /** @type {io.letsexchange.HyperLetsExchange.constructor&io.letsexchange.IHyperLetsExchange.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperLetsExchange* instance and automatically initialise it with options.
   * @param {...!io.letsexchange.IHyperLetsExchange.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperLetsExchange* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.IHyperLetsExchange.Initialese} init The initialisation options.
 */
io.letsexchange.HyperLetsExchange.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.HyperLetsExchange}
 */
io.letsexchange.HyperLetsExchange.__extend = function(...Extensions) {}

/** @typedef {io.letsexchange.IHyperLetsExchange} */
io.letsexchange.RecordIHyperLetsExchange

/** @typedef {io.letsexchange.IHyperLetsExchange} io.letsexchange.BoundIHyperLetsExchange */

/** @typedef {io.letsexchange.HyperLetsExchange} io.letsexchange.BoundHyperLetsExchange */

/**
 * Contains getters to cast the _IHyperLetsExchange_ interface.
 * @interface io.letsexchange.IHyperLetsExchangeCaster
 */
io.letsexchange.IHyperLetsExchangeCaster = class { }
/**
 * Cast the _IHyperLetsExchange_ instance into the _BoundIHyperLetsExchange_ type.
 * @type {!io.letsexchange.BoundIHyperLetsExchange}
 */
io.letsexchange.IHyperLetsExchangeCaster.prototype.asIHyperLetsExchange
/**
 * Access the _HyperLetsExchange_ prototype.
 * @type {!io.letsexchange.BoundHyperLetsExchange}
 */
io.letsexchange.IHyperLetsExchangeCaster.prototype.superHyperLetsExchange

/** @typedef {function(new: io.letsexchange.ILetsExchangeFields&engineering.type.IEngineer&io.letsexchange.ILetsExchangeCaster&engineer.type.INetwork&io.letsexchange.ULetsExchange)} io.letsexchange.ILetsExchange.constructor */
/**
 * The _LetsExchange_ client.
 * @interface io.letsexchange.ILetsExchange
 */
io.letsexchange.ILetsExchange = class extends /** @type {io.letsexchange.ILetsExchange.constructor&engineering.type.IEngineer.typeof&engineer.type.INetwork.typeof&io.letsexchange.ULetsExchange.typeof} */ (class {}) {
}
/**
 * Create a new *ILetsExchange* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ILetsExchange.Initialese} init The initialisation options.
 */
io.letsexchange.ILetsExchange.prototype.constructor = function(...init) {}
/** @type {io.letsexchange.ILetsExchange.GetInfo} */
io.letsexchange.ILetsExchange.prototype.GetInfo = function() {}

/** @typedef {function(new: io.letsexchange.ILetsExchange&engineering.type.IInitialiser<!io.letsexchange.ILetsExchange.Initialese>)} io.letsexchange.LetsExchange.constructor */
/**
 * A concrete class of _ILetsExchange_ instances.
 * @constructor io.letsexchange.LetsExchange
 * @implements {io.letsexchange.ILetsExchange} The _LetsExchange_ client.
 * @implements {engineering.type.IInitialiser<!io.letsexchange.ILetsExchange.Initialese>} ‎
 */
io.letsexchange.LetsExchange = class extends /** @type {io.letsexchange.LetsExchange.constructor&io.letsexchange.ILetsExchange.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ILetsExchange* instance and automatically initialise it with options.
   * @param {...!io.letsexchange.ILetsExchange.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ILetsExchange* instance and automatically initialise it with options.
 * @param {...!io.letsexchange.ILetsExchange.Initialese} init The initialisation options.
 */
io.letsexchange.LetsExchange.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof io.letsexchange.LetsExchange}
 */
io.letsexchange.LetsExchange.__extend = function(...Extensions) {}

/**
 * Fields of the ILetsExchange.
 * @interface io.letsexchange.ILetsExchangeFields
 */
io.letsexchange.ILetsExchangeFields = class { }
/**
 * The _LetsExchange_ instance host. Default `https://api.letsexchange.io`.
 */
io.letsexchange.ILetsExchangeFields.prototype.host = /** @type {string} */ (void 0)
/**
 * The path after the host to use for request. Default `/api`.
 */
io.letsexchange.ILetsExchangeFields.prototype.apiPath = /** @type {string} */ (void 0)
/**
 * The client id. Default empty string.
 */
io.letsexchange.ILetsExchangeFields.prototype.letsExchangeApiKey = /** @type {string} */ (void 0)

/** @typedef {io.letsexchange.ILetsExchange} */
io.letsexchange.RecordILetsExchange

/** @typedef {io.letsexchange.ILetsExchange} io.letsexchange.BoundILetsExchange */

/** @typedef {io.letsexchange.LetsExchange} io.letsexchange.BoundLetsExchange */

/**
 * Contains getters to cast the _ILetsExchange_ interface.
 * @interface io.letsexchange.ILetsExchangeCaster
 */
io.letsexchange.ILetsExchangeCaster = class { }
/**
 * Cast the _ILetsExchange_ instance into the _BoundILetsExchange_ type.
 * @type {!io.letsexchange.BoundILetsExchange}
 */
io.letsexchange.ILetsExchangeCaster.prototype.asILetsExchange
/**
 * Access the _LetsExchange_ prototype.
 * @type {!io.letsexchange.BoundLetsExchange}
 */
io.letsexchange.ILetsExchangeCaster.prototype.superLetsExchange

/**
 * @typedef {(this: THIS, data?: !io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData) => void} io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo
 * @template THIS
 */
/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel.__before_GetInfo<!io.letsexchange.ILetsExchangeJoinpointModel>} io.letsexchange.ILetsExchangeJoinpointModel._before_GetInfo */
/** @typedef {typeof io.letsexchange.ILetsExchangeJoinpointModel.before_GetInfo} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.BeforeGetInfoPointcutData} [data] Metadata passed to the pointcuts of _GetInfo_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ILetsExchange.GetInfoNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `GetInfo` method from being executed.
 * - `sub` _(value: !Promise&lt;ILetsExchange.GetInfo.Return&gt;) =&gt; void_ Cancels a call to `GetInfo` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `args` _ILetsExchange.GetInfoNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.before_GetInfo = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData) => void} io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo
 * @template THIS
 */
/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfo<!io.letsexchange.ILetsExchangeJoinpointModel>} io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfo */
/** @typedef {typeof io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfo} */
/**
 * After the method. `🔗 $combine`
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterGetInfoPointcutData} [data] Metadata passed to the pointcuts of _GetInfo_ at the `after` joinpoint.
 * - `res` _ILetsExchange.GetInfo.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `args` _ILetsExchange.GetInfoNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfo = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData) => void} io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows
 * @template THIS
 */
/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoThrows<!io.letsexchange.ILetsExchangeJoinpointModel>} io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoThrows */
/** @typedef {typeof io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterThrowsGetInfoPointcutData} [data] Metadata passed to the pointcuts of _GetInfo_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `GetInfo` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `args` _ILetsExchange.GetInfoNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData) => void} io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns
 * @template THIS
 */
/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoReturns<!io.letsexchange.ILetsExchangeJoinpointModel>} io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoReturns */
/** @typedef {typeof io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterReturnsGetInfoPointcutData} [data] Metadata passed to the pointcuts of _GetInfo_ at the `afterReturns` joinpoint.
 * - `res` _ILetsExchange.GetInfo.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;ILetsExchange.GetInfo.Return&gt;&vert;ILetsExchange.GetInfo.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `args` _ILetsExchange.GetInfoNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData) => void} io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels
 * @template THIS
 */
/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel.__after_GetInfoCancels<!io.letsexchange.ILetsExchangeJoinpointModel>} io.letsexchange.ILetsExchangeJoinpointModel._after_GetInfoCancels */
/** @typedef {typeof io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.AfterCancelsGetInfoPointcutData} [data] Metadata passed to the pointcuts of _GetInfo_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `args` _ILetsExchange.GetInfoNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.after_GetInfoCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData) => void} io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo
 * @template THIS
 */
/** @typedef {io.letsexchange.ILetsExchangeJoinpointModel.__immediatelyAfter_GetInfo<!io.letsexchange.ILetsExchangeJoinpointModel>} io.letsexchange.ILetsExchangeJoinpointModel._immediatelyAfter_GetInfo */
/** @typedef {typeof io.letsexchange.ILetsExchangeJoinpointModel.immediatelyAfter_GetInfo} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!io.letsexchange.ILetsExchangeJoinpointModel.ImmediatelyAfterGetInfoPointcutData} [data] Metadata passed to the pointcuts of _GetInfo_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;ILetsExchange.GetInfo.Return&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `args` _ILetsExchange.GetInfoNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ILetsExchangeJoinpointModel.GetInfoPointcutData*
 * @return {void}
 */
io.letsexchange.ILetsExchangeJoinpointModel.immediatelyAfter_GetInfo = function(data) {}

/**
 * @typedef {(this: THIS, data: !io.letsexchange.ILetsExchange.GetInfo.Data) => !Promise<io.letsexchange.ILetsExchange.GetInfo.Return>} io.letsexchange.ILetsExchange.__GetInfo
 * @template THIS
 */
/** @typedef {io.letsexchange.ILetsExchange.__GetInfo<!io.letsexchange.ILetsExchange>} io.letsexchange.ILetsExchange._GetInfo */
/** @typedef {typeof io.letsexchange.ILetsExchange.GetInfo} */
/**
 * Gets current rate, calculates final amount that user will get, and also it
 * sets minimum and maximum amounts for deposit.
 * @param {!io.letsexchange.ILetsExchange.GetInfo.Data} data The data.
 * - `from` _string_
 * - `to` _string_
 * - `amount` _string&vert;number_
 * - `[networkFrom]` _string?_
 * - `[networkTo]` _string?_
 * - `[promocode]` _string?_
 * - `[affiliateId]` _string?_
 * - `[float]` _boolean?_
 * @return {!Promise<io.letsexchange.ILetsExchange.GetInfo.Return>}
 */
io.letsexchange.ILetsExchange.GetInfo = function(data) {}

/**
 * @typedef {Object} io.letsexchange.ILetsExchange.GetInfo.Data The data.
 * @prop {string} from
 * @prop {string} to
 * @prop {string|number} amount
 * @prop {string} [networkFrom]
 * @prop {string} [networkTo]
 * @prop {string} [promocode]
 * @prop {string} [affiliateId]
 * @prop {boolean} [float]
 */

/**
 * @typedef {Object} io.letsexchange.ILetsExchange.GetInfo.Return
 * @prop {string} deposit_min_amount Example: `0.003`.
 * @prop {string} deposit_max_amount Example: `1200`.
 * @prop {string} min_amount Example: `0.003`.
 * @prop {string} max_amount Example: `1200`.
 * @prop {string} amount Example: `0.10704518`.
 * @prop {string} fee Example: `0`.
 * @prop {string} rate Example: `16.522349892044`.
 * @prop {string} profit Example: `0.00053256`.
 * @prop {number} withdrawal_fee Example: `0.009143816282467`.
 * @prop {string} extra_fee_amount Example: `0`.
 * @prop {number} rate_id Example: `131439131`. Applicable to fixed rates only.
 * @prop {string} rate_id_expired_at Example: `1705576568000`. Applicable to fixed rates only.
 * @prop {number} applied_promo_code_id Example: `843`.
 * @prop {!Array} networks_from Example: `[]`.
 * @prop {!Array} networks_to Example: `[]`.
 * @prop {string} deposit_amount_usdt Example: `297.34564999`.
 * @prop {string} expired_at Example: `1705576561752`.
 */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeAide.Initialese
 * @prop {string} [output]
 * @prop {boolean} [showHelp]
 * @prop {boolean} [testExchanges]
 * @prop {boolean} [showVersion]
 * @prop {boolean} [debug]
 * @prop {string} [version] The version of the package with the bin.
 */
/** @typedef {$xyz.swapee.ISwapeeAide.Initialese&xyz.swapee.ISwapee.Initialese} xyz.swapee.ISwapeeAide.Initialese */

/** @typedef {function(new: xyz.swapee.SwapeeAide)} xyz.swapee.AbstractSwapeeAide.constructor */
/** @typedef {typeof xyz.swapee.SwapeeAide} xyz.swapee.SwapeeAide.typeof */
/**
 * An abstract class of `xyz.swapee.ISwapeeAide` interface.
 * @constructor xyz.swapee.AbstractSwapeeAide
 */
xyz.swapee.AbstractSwapeeAide = class extends /** @type {xyz.swapee.AbstractSwapeeAide.constructor&xyz.swapee.SwapeeAide.typeof} */ (class {}) { }
xyz.swapee.AbstractSwapeeAide.prototype.constructor = xyz.swapee.AbstractSwapeeAide
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractSwapeeAide.class = /** @type {typeof xyz.swapee.AbstractSwapeeAide} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.ISwapeeAide|typeof xyz.swapee.SwapeeAide)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAide}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeAide.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractSwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.ISwapeeAide|typeof xyz.swapee.SwapeeAide)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.ISwapeeAide|typeof xyz.swapee.SwapeeAide)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.AbstractSwapeeAide.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.ISwapeeAide.Initialese[]) => xyz.swapee.ISwapeeAide} xyz.swapee.SwapeeAideConstructor */

/** @typedef {function(new: xyz.swapee.ISwapeeAideFields&engineering.type.IEngineer&xyz.swapee.ISwapeeAideCaster&xyz.swapee.ISwapee)} xyz.swapee.ISwapeeAide.constructor */
/** @typedef {typeof xyz.swapee.ISwapee} xyz.swapee.ISwapee.typeof */
/** @interface xyz.swapee.ISwapeeAide */
xyz.swapee.ISwapeeAide = class extends /** @type {xyz.swapee.ISwapeeAide.constructor&engineering.type.IEngineer.typeof&xyz.swapee.ISwapee.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeAide* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeAide.Initialese} init The initialisation options.
 */
xyz.swapee.ISwapeeAide.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.ISwapeeAide.ShowHelp} */
xyz.swapee.ISwapeeAide.prototype.ShowHelp = function() {}
/** @type {xyz.swapee.ISwapeeAide.ShowVersion} */
xyz.swapee.ISwapeeAide.prototype.ShowVersion = function() {}

/** @typedef {function(new: xyz.swapee.ISwapeeAide&engineering.type.IInitialiser<!xyz.swapee.ISwapeeAide.Initialese>)} xyz.swapee.SwapeeAide.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeAide} xyz.swapee.ISwapeeAide.typeof */
/**
 * A concrete class of _ISwapeeAide_ instances.
 * @constructor xyz.swapee.SwapeeAide
 * @implements {xyz.swapee.ISwapeeAide} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeAide.Initialese>} ‎
 */
xyz.swapee.SwapeeAide = class extends /** @type {xyz.swapee.SwapeeAide.constructor&xyz.swapee.ISwapeeAide.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeAide* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ISwapeeAide.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeAide* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeAide.Initialese} init The initialisation options.
 */
xyz.swapee.SwapeeAide.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAide}
 */
xyz.swapee.SwapeeAide.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeAide.
 * @interface xyz.swapee.ISwapeeAideFields
 */
xyz.swapee.ISwapeeAideFields = class { }
/**
 * Default empty string.
 */
xyz.swapee.ISwapeeAideFields.prototype.output = /** @type {string} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.ISwapeeAideFields.prototype.showHelp = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.ISwapeeAideFields.prototype.testExchanges = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.ISwapeeAideFields.prototype.showVersion = /** @type {boolean} */ (void 0)
/**
 * Default `false`.
 */
xyz.swapee.ISwapeeAideFields.prototype.debug = /** @type {boolean} */ (void 0)
/**
 * The version of the package with the bin. Default empty string.
 */
xyz.swapee.ISwapeeAideFields.prototype.version = /** @type {string} */ (void 0)

/** @typedef {xyz.swapee.ISwapeeAide} */
xyz.swapee.RecordISwapeeAide

/** @typedef {xyz.swapee.ISwapeeAide} xyz.swapee.BoundISwapeeAide */

/** @typedef {xyz.swapee.SwapeeAide} xyz.swapee.BoundSwapeeAide */

/**
 * @typedef {Object} xyz.swapee.ISwapee.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {string} [text] The Text.
 */

/** @typedef {function(new: xyz.swapee.Swapee)} xyz.swapee.AbstractSwapee.constructor */
/** @typedef {typeof xyz.swapee.Swapee} xyz.swapee.Swapee.typeof */
/**
 * An abstract class of `xyz.swapee.ISwapee` interface.
 * @constructor xyz.swapee.AbstractSwapee
 */
xyz.swapee.AbstractSwapee = class extends /** @type {xyz.swapee.AbstractSwapee.constructor&xyz.swapee.Swapee.typeof} */ (class {}) { }
xyz.swapee.AbstractSwapee.prototype.constructor = xyz.swapee.AbstractSwapee
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractSwapee.class = /** @type {typeof xyz.swapee.AbstractSwapee} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|!xyz.swapee.ISwapeeHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.Swapee}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapee.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractSwapee}
 */
xyz.swapee.AbstractSwapee.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.AbstractSwapee.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|!xyz.swapee.ISwapeeHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.AbstractSwapee.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|!xyz.swapee.ISwapeeHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.AbstractSwapee.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.ISwapeeJoinpointModelHyperslice
 */
xyz.swapee.ISwapeeJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel._beforeRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeRun>} */ (void 0)
    /**
     * After the method.
     */
    this.afterRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRun>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterRunThrows=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterRunThrows|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRunThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterRunReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterRunReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRunReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterRunCancels=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterRunCancels|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterRunCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel._beforeEachRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeEachRun>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterEachRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachRun>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachRunReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterEachRunReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachRunReturns>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterRun|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterRun>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_ShowText=/** @type {!xyz.swapee.ISwapeeJoinpointModel._before_ShowText|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._before_ShowText>} */ (void 0)
    /**
     * After the method.
     */
    this.after_ShowText=/** @type {!xyz.swapee.ISwapeeJoinpointModel._after_ShowText|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowText>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_ShowTextThrows=/** @type {!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextThrows|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_ShowTextReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_ShowTextCancels=/** @type {!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextCancels|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._after_ShowTextCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_ShowText=/** @type {!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfter_ShowText|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfter_ShowText>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel._beforeConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeConnectExchanges>} */ (void 0)
    /**
     * After the method.
     */
    this.afterConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchanges>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterConnectExchangesThrows=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesThrows|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterConnectExchangesReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterConnectExchangesCancels=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesCancels|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel._beforeEachConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._beforeEachConnectExchanges>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchanges>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachConnectExchangesReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchangesReturns|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchangesReturns>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterConnectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterConnectExchanges>} */ (void 0)
  }
}
xyz.swapee.ISwapeeJoinpointModelHyperslice.prototype.constructor = xyz.swapee.ISwapeeJoinpointModelHyperslice

/**
 * A concrete class of _ISwapeeJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.SwapeeJoinpointModelHyperslice
 * @implements {xyz.swapee.ISwapeeJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.SwapeeJoinpointModelHyperslice = class extends xyz.swapee.ISwapeeJoinpointModelHyperslice { }
xyz.swapee.SwapeeJoinpointModelHyperslice.prototype.constructor = xyz.swapee.SwapeeJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.ISwapeeJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__beforeRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeRun<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRun<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterRunThrows=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterRunReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterRunCancels=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterEachRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachRun<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachRunReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterRun=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_ShowText=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__before_ShowText<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__before_ShowText<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_ShowText=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__after_ShowText<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowText<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_ShowTextThrows=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_ShowTextReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_ShowTextCancels=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfter_ShowText=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterConnectExchangesThrows=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterConnectExchangesReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterConnectExchangesCancels=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachConnectExchangesReturns=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterConnectExchanges=/** @type {!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges<THIS>>} */ (void 0)
  }
}
xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.ISwapeeJoinpointModelBindingHyperslice

/**
 * A concrete class of _ISwapeeJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.SwapeeJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.ISwapeeJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.ISwapeeJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.SwapeeJoinpointModelBindingHyperslice = class extends xyz.swapee.ISwapeeJoinpointModelBindingHyperslice { }
xyz.swapee.SwapeeJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.SwapeeJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `ISwapee`'s methods.
 * @interface xyz.swapee.ISwapeeJoinpointModel
 */
xyz.swapee.ISwapeeJoinpointModel = class { }
/** @type {xyz.swapee.ISwapeeJoinpointModel.beforeRun} */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeRun = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterRun} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRun = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterRunThrows} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRunThrows = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterRunReturns} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRunReturns = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterRunCancels} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterRunCancels = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.beforeEachRun} */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeEachRun = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterEachRun} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachRun = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterEachRunReturns} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachRunReturns = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterRun} */
xyz.swapee.ISwapeeJoinpointModel.prototype.immediatelyAfterRun = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.before_ShowText} */
xyz.swapee.ISwapeeJoinpointModel.prototype.before_ShowText = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.after_ShowText} */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowText = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.after_ShowTextThrows} */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowTextThrows = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.after_ShowTextReturns} */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowTextReturns = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.after_ShowTextCancels} */
xyz.swapee.ISwapeeJoinpointModel.prototype.after_ShowTextCancels = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.immediatelyAfter_ShowText} */
xyz.swapee.ISwapeeJoinpointModel.prototype.immediatelyAfter_ShowText = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.beforeConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeConnectExchanges = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchanges = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesThrows} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchangesThrows = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesReturns} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchangesReturns = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesCancels} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterConnectExchangesCancels = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.beforeEachConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.prototype.beforeEachConnectExchanges = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachConnectExchanges = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchangesReturns} */
xyz.swapee.ISwapeeJoinpointModel.prototype.afterEachConnectExchangesReturns = function() {}
/** @type {xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterConnectExchanges} */
xyz.swapee.ISwapeeJoinpointModel.prototype.immediatelyAfterConnectExchanges = function() {}

/**
 * A concrete class of _ISwapeeJoinpointModel_ instances.
 * @constructor xyz.swapee.SwapeeJoinpointModel
 * @implements {xyz.swapee.ISwapeeJoinpointModel} An interface that enumerates the joinpoints of `ISwapee`'s methods.
 */
xyz.swapee.SwapeeJoinpointModel = class extends xyz.swapee.ISwapeeJoinpointModel { }
xyz.swapee.SwapeeJoinpointModel.prototype.constructor = xyz.swapee.SwapeeJoinpointModel

/** @typedef {xyz.swapee.ISwapeeJoinpointModel} */
xyz.swapee.RecordISwapeeJoinpointModel

/** @typedef {xyz.swapee.ISwapeeJoinpointModel} xyz.swapee.BoundISwapeeJoinpointModel */

/** @typedef {xyz.swapee.SwapeeJoinpointModel} xyz.swapee.BoundSwapeeJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.ISwapeeAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.SwapeeAspectsInstaller)} xyz.swapee.AbstractSwapeeAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.SwapeeAspectsInstaller} xyz.swapee.SwapeeAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.ISwapeeAspectsInstaller` interface.
 * @constructor xyz.swapee.AbstractSwapeeAspectsInstaller
 */
xyz.swapee.AbstractSwapeeAspectsInstaller = class extends /** @type {xyz.swapee.AbstractSwapeeAspectsInstaller.constructor&xyz.swapee.SwapeeAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.AbstractSwapeeAspectsInstaller.prototype.constructor = xyz.swapee.AbstractSwapeeAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.class = /** @type {typeof xyz.swapee.AbstractSwapeeAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.ISwapeeAspectsInstaller|typeof xyz.swapee.SwapeeAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractSwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.ISwapeeAspectsInstaller|typeof xyz.swapee.SwapeeAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.ISwapeeAspectsInstaller|typeof xyz.swapee.SwapeeAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.ISwapeeAspectsInstaller.Initialese[]) => xyz.swapee.ISwapeeAspectsInstaller} xyz.swapee.SwapeeAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.ISwapeeAspectsInstaller.constructor */
/** @interface xyz.swapee.ISwapeeAspectsInstaller */
xyz.swapee.ISwapeeAspectsInstaller = class extends /** @type {xyz.swapee.ISwapeeAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeRun=/** @type {number} */ (void 0)
    this.afterRun=/** @type {number} */ (void 0)
    this.afterRunThrows=/** @type {number} */ (void 0)
    this.afterRunReturns=/** @type {number} */ (void 0)
    this.afterRunCancels=/** @type {number} */ (void 0)
    this.beforeEachRun=/** @type {number} */ (void 0)
    this.afterEachRun=/** @type {number} */ (void 0)
    this.afterEachRunReturns=/** @type {number} */ (void 0)
    this.immediateAfterRun=/** @type {number} */ (void 0)
    this.before_ShowText=/** @type {number} */ (void 0)
    this.after_ShowText=/** @type {number} */ (void 0)
    this.after_ShowTextThrows=/** @type {number} */ (void 0)
    this.after_ShowTextReturns=/** @type {number} */ (void 0)
    this.after_ShowTextCancels=/** @type {number} */ (void 0)
    this.immediateAfter_ShowText=/** @type {number} */ (void 0)
    this.beforeConnectExchanges=/** @type {number} */ (void 0)
    this.afterConnectExchanges=/** @type {number} */ (void 0)
    this.afterConnectExchangesThrows=/** @type {number} */ (void 0)
    this.afterConnectExchangesReturns=/** @type {number} */ (void 0)
    this.afterConnectExchangesCancels=/** @type {number} */ (void 0)
    this.beforeEachConnectExchanges=/** @type {number} */ (void 0)
    this.afterEachConnectExchanges=/** @type {number} */ (void 0)
    this.afterEachConnectExchangesReturns=/** @type {number} */ (void 0)
    this.immediateAfterConnectExchanges=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  run() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  ShowText() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  connectExchanges() { }
}
/**
 * Create a new *ISwapeeAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.ISwapeeAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.ISwapeeAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.ISwapeeAspectsInstaller.Initialese>)} xyz.swapee.SwapeeAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeAspectsInstaller} xyz.swapee.ISwapeeAspectsInstaller.typeof */
/**
 * A concrete class of _ISwapeeAspectsInstaller_ instances.
 * @constructor xyz.swapee.SwapeeAspectsInstaller
 * @implements {xyz.swapee.ISwapeeAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.SwapeeAspectsInstaller = class extends /** @type {xyz.swapee.SwapeeAspectsInstaller.constructor&xyz.swapee.ISwapeeAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ISwapeeAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.SwapeeAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAspectsInstaller}
 */
xyz.swapee.SwapeeAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.ISwapee.RunNArgs
 * @prop {!xyz.swapee.swapee_xyz.Config} conf The config to run the method against.
 */

/**
 * @typedef {Object} xyz.swapee.ISwapeeJoinpointModel.RunPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.ISwapee.RunNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.ISwapee.RunNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `run` method from being executed.
 * @prop {(value: !Promise<string>) => void} sub Cancels a call to `run` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData&xyz.swapee.ISwapeeJoinpointModel.RunPointcutData} xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData&xyz.swapee.ISwapeeJoinpointModel.RunPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `run` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData&xyz.swapee.ISwapeeJoinpointModel.RunPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<string>|string) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData&xyz.swapee.ISwapeeJoinpointModel.RunPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData&xyz.swapee.ISwapeeJoinpointModel.RunPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData
 * @prop {!Promise<string>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData&xyz.swapee.ISwapeeJoinpointModel.RunPointcutData} xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} xyz.swapee.ISwapee.ShowTextNArgs
 * @prop {!xyz.swapee.ISwapee.ShowText.Conf} conf The config.
 */

/**
 * @typedef {Object} xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.ISwapee.ShowTextNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.ISwapee.ShowTextNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `ShowText` method from being executed.
 * @prop {(value: !Promise<string>) => void} sub Cancels a call to `ShowText` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData&xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData} xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData&xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `ShowText` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData&xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<string>|string) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData&xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData&xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData
 * @prop {!Promise<string>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData&xyz.swapee.ISwapeeJoinpointModel.ShowTextPointcutData} xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} xyz.swapee.ISwapee.ConnectExchangesNArgs
 * @prop {!xyz.swapee.ISwapee.connectExchanges.Opts} opts The options.
 */

/**
 * @typedef {Object} xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.ISwapee.ConnectExchangesNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.ISwapee.ConnectExchangesNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `connectExchanges` method from being executed.
 * @prop {(value: !Promise<xyz.swapee.ISwapee.connectExchanges.Return>) => void} sub Cancels a call to `connectExchanges` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData&xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData} xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData
 * @prop {xyz.swapee.ISwapee.connectExchanges.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData&xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `connectExchanges` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData&xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData
 * @prop {xyz.swapee.ISwapee.connectExchanges.Return} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<xyz.swapee.ISwapee.connectExchanges.Return>|xyz.swapee.ISwapee.connectExchanges.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData&xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData&xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData} xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData
 * @prop {!Promise<xyz.swapee.ISwapee.connectExchanges.Return>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData&xyz.swapee.ISwapeeJoinpointModel.ConnectExchangesPointcutData} xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/** @typedef {new (...args: !xyz.swapee.ISwapee.Initialese[]) => xyz.swapee.ISwapee} xyz.swapee.SwapeeConstructor */

/** @typedef {function(new: xyz.swapee.BSwapeeAspectsCaster<THIS>&xyz.swapee.ISwapeeJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.BSwapeeAspects.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModelBindingHyperslice} xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *ISwapee* that bind to an instance.
 * @interface xyz.swapee.BSwapeeAspects
 * @template THIS
 */
xyz.swapee.BSwapeeAspects = class extends /** @type {xyz.swapee.BSwapeeAspects.constructor&xyz.swapee.ISwapeeJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.BSwapeeAspects.prototype.constructor = xyz.swapee.BSwapeeAspects

/** @typedef {typeof __$te_plain} xyz.swapee.ISwapeeAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.SwapeeAspects)} xyz.swapee.AbstractSwapeeAspects.constructor */
/** @typedef {typeof xyz.swapee.SwapeeAspects} xyz.swapee.SwapeeAspects.typeof */
/**
 * An abstract class of `xyz.swapee.ISwapeeAspects` interface.
 * @constructor xyz.swapee.AbstractSwapeeAspects
 */
xyz.swapee.AbstractSwapeeAspects = class extends /** @type {xyz.swapee.AbstractSwapeeAspects.constructor&xyz.swapee.SwapeeAspects.typeof} */ (class {}) { }
xyz.swapee.AbstractSwapeeAspects.prototype.constructor = xyz.swapee.AbstractSwapeeAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractSwapeeAspects.class = /** @type {typeof xyz.swapee.AbstractSwapeeAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.ISwapeeAspects|typeof xyz.swapee.SwapeeAspects)|(!xyz.swapee.BSwapeeAspects|typeof xyz.swapee.BSwapeeAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspects}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractSwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.ISwapeeAspects|typeof xyz.swapee.SwapeeAspects)|(!xyz.swapee.BSwapeeAspects|typeof xyz.swapee.BSwapeeAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.ISwapeeAspects|typeof xyz.swapee.SwapeeAspects)|(!xyz.swapee.BSwapeeAspects|typeof xyz.swapee.BSwapeeAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.AbstractSwapeeAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.ISwapeeAspects.Initialese[]) => xyz.swapee.ISwapeeAspects} xyz.swapee.SwapeeAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.ISwapeeAspectsCaster&xyz.swapee.BSwapeeAspects<!xyz.swapee.ISwapeeAspects>)} xyz.swapee.ISwapeeAspects.constructor */
/** @typedef {typeof xyz.swapee.BSwapeeAspects} xyz.swapee.BSwapeeAspects.typeof */
/**
 * The aspects of the *ISwapee*.
 * @interface xyz.swapee.ISwapeeAspects
 */
xyz.swapee.ISwapeeAspects = class extends /** @type {xyz.swapee.ISwapeeAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.BSwapeeAspects.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.props=/** @type {xyz.swapee.ISwapeeAspects} */ (void 0)
  }
}
/**
 * Create a new *ISwapeeAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeAspects.Initialese} init The initialisation options.
 */
xyz.swapee.ISwapeeAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.ISwapeeAspects&engineering.type.IInitialiser<!xyz.swapee.ISwapeeAspects.Initialese>)} xyz.swapee.SwapeeAspects.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeAspects} xyz.swapee.ISwapeeAspects.typeof */
/**
 * A concrete class of _ISwapeeAspects_ instances.
 * @constructor xyz.swapee.SwapeeAspects
 * @implements {xyz.swapee.ISwapeeAspects} The aspects of the *ISwapee*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeAspects.Initialese>} ‎
 */
xyz.swapee.SwapeeAspects = class extends /** @type {xyz.swapee.SwapeeAspects.constructor&xyz.swapee.ISwapeeAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ISwapeeAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeAspects.Initialese} init The initialisation options.
 */
xyz.swapee.SwapeeAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeAspects}
 */
xyz.swapee.SwapeeAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BSwapeeAspects_ interface.
 * @interface xyz.swapee.BSwapeeAspectsCaster
 * @template THIS
 */
xyz.swapee.BSwapeeAspectsCaster = class { }
/**
 * Cast the _BSwapeeAspects_ instance into the _BoundISwapee_ type.
 * @type {!xyz.swapee.BoundISwapee}
 */
xyz.swapee.BSwapeeAspectsCaster.prototype.asISwapee

/**
 * Contains getters to cast the _ISwapeeAspects_ interface.
 * @interface xyz.swapee.ISwapeeAspectsCaster
 */
xyz.swapee.ISwapeeAspectsCaster = class { }
/**
 * Cast the _ISwapeeAspects_ instance into the _BoundISwapee_ type.
 * @type {!xyz.swapee.BoundISwapee}
 */
xyz.swapee.ISwapeeAspectsCaster.prototype.asISwapee

/** @typedef {xyz.swapee.ISwapee.Initialese} xyz.swapee.IHyperSwapee.Initialese */

/** @typedef {function(new: xyz.swapee.HyperSwapee)} xyz.swapee.AbstractHyperSwapee.constructor */
/** @typedef {typeof xyz.swapee.HyperSwapee} xyz.swapee.HyperSwapee.typeof */
/**
 * An abstract class of `xyz.swapee.IHyperSwapee` interface.
 * @constructor xyz.swapee.AbstractHyperSwapee
 */
xyz.swapee.AbstractHyperSwapee = class extends /** @type {xyz.swapee.AbstractHyperSwapee.constructor&xyz.swapee.HyperSwapee.typeof} */ (class {}) { }
xyz.swapee.AbstractHyperSwapee.prototype.constructor = xyz.swapee.AbstractHyperSwapee
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractHyperSwapee.class = /** @type {typeof xyz.swapee.AbstractHyperSwapee} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.IHyperSwapee|typeof xyz.swapee.HyperSwapee)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|(!xyz.swapee.ISwapeeHyperslice|typeof xyz.swapee.SwapeeHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.IHyperSwapee|typeof xyz.swapee.HyperSwapee)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|(!xyz.swapee.ISwapeeHyperslice|typeof xyz.swapee.SwapeeHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.IHyperSwapee|typeof xyz.swapee.HyperSwapee)|(!xyz.swapee.ISwapee|typeof xyz.swapee.Swapee)|(!xyz.swapee.ISwapeeHyperslice|typeof xyz.swapee.SwapeeHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.AbstractHyperSwapee.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.ISwapeeAspects|!Array<!xyz.swapee.ISwapeeAspects>|function(new: xyz.swapee.ISwapeeAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the ISwapee to implement aspects.
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof xyz.swapee.AbstractHyperSwapee}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapee.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !xyz.swapee.IHyperSwapee.Initialese[]) => xyz.swapee.IHyperSwapee} xyz.swapee.HyperSwapeeConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.IHyperSwapeeCaster&xyz.swapee.ISwapee)} xyz.swapee.IHyperSwapee.constructor */
/** @interface xyz.swapee.IHyperSwapee */
xyz.swapee.IHyperSwapee = class extends /** @type {xyz.swapee.IHyperSwapee.constructor&engineering.type.IEngineer.typeof&xyz.swapee.ISwapee.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperSwapee* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IHyperSwapee.Initialese} init The initialisation options.
 */
xyz.swapee.IHyperSwapee.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.IHyperSwapee&engineering.type.IInitialiser<!xyz.swapee.IHyperSwapee.Initialese>)} xyz.swapee.HyperSwapee.constructor */
/** @typedef {typeof xyz.swapee.IHyperSwapee} xyz.swapee.IHyperSwapee.typeof */
/**
 * A concrete class of _IHyperSwapee_ instances.
 * @constructor xyz.swapee.HyperSwapee
 * @implements {xyz.swapee.IHyperSwapee} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IHyperSwapee.Initialese>} ‎
 */
xyz.swapee.HyperSwapee = class extends /** @type {xyz.swapee.HyperSwapee.constructor&xyz.swapee.IHyperSwapee.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperSwapee* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.IHyperSwapee.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperSwapee* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IHyperSwapee.Initialese} init The initialisation options.
 */
xyz.swapee.HyperSwapee.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperSwapee}
 */
xyz.swapee.HyperSwapee.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.IHyperSwapee} */
xyz.swapee.RecordIHyperSwapee

/** @typedef {xyz.swapee.IHyperSwapee} xyz.swapee.BoundIHyperSwapee */

/** @typedef {xyz.swapee.HyperSwapee} xyz.swapee.BoundHyperSwapee */

/**
 * Contains getters to cast the _IHyperSwapee_ interface.
 * @interface xyz.swapee.IHyperSwapeeCaster
 */
xyz.swapee.IHyperSwapeeCaster = class { }
/**
 * Cast the _IHyperSwapee_ instance into the _BoundIHyperSwapee_ type.
 * @type {!xyz.swapee.BoundIHyperSwapee}
 */
xyz.swapee.IHyperSwapeeCaster.prototype.asIHyperSwapee
/**
 * Access the _HyperSwapee_ prototype.
 * @type {!xyz.swapee.BoundHyperSwapee}
 */
xyz.swapee.IHyperSwapeeCaster.prototype.superHyperSwapee

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.ISwapeeHyperslice
 */
xyz.swapee.ISwapeeHyperslice = class {
  constructor() {
    /**
     * Executes the main method via the class.
     */
    this.run=/** @type {!xyz.swapee.ISwapee._run|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee._run>} */ (void 0)
    this.connectExchanges=/** @type {!xyz.swapee.ISwapee._connectExchanges|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee._connectExchanges>} */ (void 0)
  }
}
xyz.swapee.ISwapeeHyperslice.prototype.constructor = xyz.swapee.ISwapeeHyperslice

/**
 * A concrete class of _ISwapeeHyperslice_ instances.
 * @constructor xyz.swapee.SwapeeHyperslice
 * @implements {xyz.swapee.ISwapeeHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.SwapeeHyperslice = class extends xyz.swapee.ISwapeeHyperslice { }
xyz.swapee.SwapeeHyperslice.prototype.constructor = xyz.swapee.SwapeeHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.ISwapeeBindingHyperslice
 * @template THIS
 */
xyz.swapee.ISwapeeBindingHyperslice = class {
  constructor() {
    /**
     * Executes the main method via the class.
     */
    this.run=/** @type {!xyz.swapee.ISwapee.__run<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee.__run<THIS>>} */ (void 0)
    this.connectExchanges=/** @type {!xyz.swapee.ISwapee.__connectExchanges<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.ISwapee.__connectExchanges<THIS>>} */ (void 0)
  }
}
xyz.swapee.ISwapeeBindingHyperslice.prototype.constructor = xyz.swapee.ISwapeeBindingHyperslice

/**
 * A concrete class of _ISwapeeBindingHyperslice_ instances.
 * @constructor xyz.swapee.SwapeeBindingHyperslice
 * @implements {xyz.swapee.ISwapeeBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.ISwapeeBindingHyperslice<THIS>}
 */
xyz.swapee.SwapeeBindingHyperslice = class extends xyz.swapee.ISwapeeBindingHyperslice { }
xyz.swapee.SwapeeBindingHyperslice.prototype.constructor = xyz.swapee.SwapeeBindingHyperslice

/** @typedef {function(new: xyz.swapee.ISwapeeFields&engineering.type.IEngineer&xyz.swapee.ISwapeeCaster)} xyz.swapee.ISwapee.constructor */
/**
 * The package.
 * @interface xyz.swapee.ISwapee
 */
xyz.swapee.ISwapee = class extends /** @type {xyz.swapee.ISwapee.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapee* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapee.Initialese} init The initialisation options.
 */
xyz.swapee.ISwapee.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.ISwapee.run} */
xyz.swapee.ISwapee.prototype.run = function() {}
/** @type {xyz.swapee.ISwapee.ShowText} */
xyz.swapee.ISwapee.prototype.ShowText = function() {}
/** @type {xyz.swapee.ISwapee.connectExchanges} */
xyz.swapee.ISwapee.prototype.connectExchanges = function() {}

/** @typedef {function(new: xyz.swapee.ISwapee&engineering.type.IInitialiser<!xyz.swapee.ISwapee.Initialese>)} xyz.swapee.Swapee.constructor */
/**
 * A concrete class of _ISwapee_ instances.
 * @constructor xyz.swapee.Swapee
 * @implements {xyz.swapee.ISwapee} The package.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapee.Initialese>} ‎
 */
xyz.swapee.Swapee = class extends /** @type {xyz.swapee.Swapee.constructor&xyz.swapee.ISwapee.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapee* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ISwapee.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapee* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapee.Initialese} init The initialisation options.
 */
xyz.swapee.Swapee.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.Swapee}
 */
xyz.swapee.Swapee.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapee.
 * @interface xyz.swapee.ISwapeeFields
 */
xyz.swapee.ISwapeeFields = class { }
/**
 * An example property. Default `ok`.
 */
xyz.swapee.ISwapeeFields.prototype.example = /** @type {string} */ (void 0)
/**
 * The Text. Default empty string.
 */
xyz.swapee.ISwapeeFields.prototype.text = /** @type {string} */ (void 0)

/** @typedef {xyz.swapee.ISwapee} */
xyz.swapee.RecordISwapee

/** @typedef {xyz.swapee.ISwapee} xyz.swapee.BoundISwapee */

/** @typedef {xyz.swapee.Swapee} xyz.swapee.BoundSwapee */

/**
 * Contains getters to cast the _ISwapeeAide_ interface.
 * @interface xyz.swapee.ISwapeeAideCaster
 */
xyz.swapee.ISwapeeAideCaster = class { }
/**
 * Cast the _ISwapeeAide_ instance into the _BoundISwapeeAide_ type.
 * @type {!xyz.swapee.BoundISwapeeAide}
 */
xyz.swapee.ISwapeeAideCaster.prototype.asISwapeeAide
/**
 * Access the _SwapeeAide_ prototype.
 * @type {!xyz.swapee.BoundSwapeeAide}
 */
xyz.swapee.ISwapeeAideCaster.prototype.superSwapeeAide

/**
 * Contains getters to cast the _ISwapee_ interface.
 * @interface xyz.swapee.ISwapeeCaster
 */
xyz.swapee.ISwapeeCaster = class { }
/**
 * Cast the _ISwapee_ instance into the _BoundISwapee_ type.
 * @type {!xyz.swapee.BoundISwapee}
 */
xyz.swapee.ISwapeeCaster.prototype.asISwapee
/**
 * Access the _Swapee_ prototype.
 * @type {!xyz.swapee.BoundSwapee}
 */
xyz.swapee.ISwapeeCaster.prototype.superSwapee

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.ISwapeeAide.__ShowHelp
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeAide.__ShowHelp<!xyz.swapee.ISwapeeAide>} xyz.swapee.ISwapeeAide._ShowHelp */
/** @typedef {typeof xyz.swapee.ISwapeeAide.ShowHelp} */
/** @return {?} */
xyz.swapee.ISwapeeAide.ShowHelp = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.ISwapeeAide.__ShowVersion
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeAide.__ShowVersion<!xyz.swapee.ISwapeeAide>} xyz.swapee.ISwapeeAide._ShowVersion */
/** @typedef {typeof xyz.swapee.ISwapeeAide.ShowVersion} */
/** @return {?} */
xyz.swapee.ISwapeeAide.ShowVersion = function() {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__beforeRun
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__beforeRun<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._beforeRun */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.beforeRun} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ISwapee.RunNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `run` method from being executed.
 * - `sub` _(value: !Promise&lt;string&gt;) =&gt; void_ Cancels a call to `run` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.beforeRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterRun
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterRun<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterRun */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterRun} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterRunThrows<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterRunThrows */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterRunThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `run` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterRunThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterRunReturns<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterRunReturns */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterRunReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `afterReturns` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;string&gt;&vert;string) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterRunReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterRunCancels<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterRunCancels */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterRunCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterRunCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__beforeEachRun<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._beforeEachRun */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.beforeEachRun} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ISwapee.RunNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `run` method from being executed.
 * - `sub` _(value: !Promise&lt;string&gt;) =&gt; void_ Cancels a call to `run` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.beforeEachRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterEachRun
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterEachRun<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterEachRun */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterEachRun} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterEachRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterEachRunReturns<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterEachRunReturns */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterEachRunReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `afterEach` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterEachRunReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterRun<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterRun */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterRun} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;string&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `args` _ISwapee.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.RunPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__before_ShowText
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__before_ShowText<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._before_ShowText */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.before_ShowText} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeShowTextPointcutData} [data] Metadata passed to the pointcuts of _ShowText_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ISwapee.ShowTextNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `ShowText` method from being executed.
 * - `sub` _(value: !Promise&lt;string&gt;) =&gt; void_ Cancels a call to `ShowText` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `args` _ISwapee.ShowTextNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.before_ShowText = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__after_ShowText
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__after_ShowText<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._after_ShowText */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.after_ShowText} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterShowTextPointcutData} [data] Metadata passed to the pointcuts of _ShowText_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `args` _ISwapee.ShowTextNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.after_ShowText = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextThrows<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._after_ShowTextThrows */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.after_ShowTextThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsShowTextPointcutData} [data] Metadata passed to the pointcuts of _ShowText_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `ShowText` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `args` _ISwapee.ShowTextNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.after_ShowTextThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextReturns<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._after_ShowTextReturns */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.after_ShowTextReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsShowTextPointcutData} [data] Metadata passed to the pointcuts of _ShowText_ at the `afterReturns` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;string&gt;&vert;string) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `args` _ISwapee.ShowTextNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.after_ShowTextReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__after_ShowTextCancels<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._after_ShowTextCancels */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.after_ShowTextCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsShowTextPointcutData} [data] Metadata passed to the pointcuts of _ShowText_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `args` _ISwapee.ShowTextNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.after_ShowTextCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfter_ShowText<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._immediatelyAfter_ShowText */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.immediatelyAfter_ShowText} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterShowTextPointcutData} [data] Metadata passed to the pointcuts of _ShowText_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;string&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `args` _ISwapee.ShowTextNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ShowTextPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.immediatelyAfter_ShowText = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__beforeConnectExchanges<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._beforeConnectExchanges */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.beforeConnectExchanges} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ISwapee.ConnectExchangesNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `connectExchanges` method from being executed.
 * - `sub` _(value: !Promise&lt;ISwapee.connectExchanges.Return&gt;) =&gt; void_ Cancels a call to `connectExchanges` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.beforeConnectExchanges = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchanges<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterConnectExchanges */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterConnectExchanges} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `after` joinpoint.
 * - `res` _ISwapee.connectExchanges.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchanges = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesThrows<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesThrows */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterThrowsConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `connectExchanges` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesReturns<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesReturns */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterReturnsConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `afterReturns` joinpoint.
 * - `res` _ISwapee.connectExchanges.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;ISwapee.connectExchanges.Return&gt;&vert;ISwapee.connectExchanges.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterConnectExchangesCancels<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterConnectExchangesCancels */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterCancelsConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterConnectExchangesCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__beforeEachConnectExchanges<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._beforeEachConnectExchanges */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.beforeEachConnectExchanges} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.BeforeConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ISwapee.ConnectExchangesNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `connectExchanges` method from being executed.
 * - `sub` _(value: !Promise&lt;ISwapee.connectExchanges.Return&gt;) =&gt; void_ Cancels a call to `connectExchanges` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.beforeEachConnectExchanges = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchanges<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchanges */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchanges} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `after` joinpoint.
 * - `res` _ISwapee.connectExchanges.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchanges = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__afterEachConnectExchangesReturns<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._afterEachConnectExchangesReturns */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchangesReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.AfterConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `afterEach` joinpoint.
 * - `res` _ISwapee.connectExchanges.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.afterEachConnectExchangesReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData) => void} xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapeeJoinpointModel.__immediatelyAfterConnectExchanges<!xyz.swapee.ISwapeeJoinpointModel>} xyz.swapee.ISwapeeJoinpointModel._immediatelyAfterConnectExchanges */
/** @typedef {typeof xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterConnectExchanges} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!xyz.swapee.ISwapeeJoinpointModel.ImmediatelyAfterConnectExchangesPointcutData} [data] Metadata passed to the pointcuts of _connectExchanges_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;ISwapee.connectExchanges.Return&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `args` _ISwapee.ConnectExchangesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ISwapeeJoinpointModel.ConnectExchangesPointcutData*
 * @return {void}
 */
xyz.swapee.ISwapeeJoinpointModel.immediatelyAfterConnectExchanges = function(data) {}

/**
 * @typedef {(this: THIS, conf?: !xyz.swapee.swapee_xyz.Config) => !Promise<string>} xyz.swapee.ISwapee.__run
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapee.__run<!xyz.swapee.ISwapee>} xyz.swapee.ISwapee._run */
/** @typedef {typeof xyz.swapee.ISwapee.run} */
/**
 * Executes the main method via the class. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.swapee_xyz.Config} [conf] The config to run the method against.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise<string>} The result of transforms.
 * @example
 * ```js
 *
 * ```
 */
xyz.swapee.ISwapee.run = function(conf) {}

/**
 * @typedef {(this: THIS, conf: !xyz.swapee.ISwapee.ShowText.Conf) => !Promise<string>} xyz.swapee.ISwapee.__ShowText
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapee.__ShowText<!xyz.swapee.ISwapee>} xyz.swapee.ISwapee._ShowText */
/** @typedef {typeof xyz.swapee.ISwapee.ShowText} */
/**
 * Shows the text back to the user.
 * @param {!xyz.swapee.ISwapee.ShowText.Conf} conf The config.
 * - `text` _string_ The text to show.
 * - `[output="-"]` _string?_ Where to print text, by default writes to `stdout`. Default `-`.
 * @return {!Promise<string>} The result of the method (will be printed by aspects).
 * @example
 * ```js
 *
 * ```
 */
xyz.swapee.ISwapee.ShowText = function(conf) {}

/**
 * @typedef {Object} xyz.swapee.ISwapee.ShowText.Conf The config.
 * @prop {string} text The text to show.
 * @prop {string} [output="-"] Where to print text, by default writes to `stdout`. Default `-`.
 */

/**
 * @typedef {(this: THIS, opts?: !xyz.swapee.ISwapee.connectExchanges.Opts) => !Promise<xyz.swapee.ISwapee.connectExchanges.Return>} xyz.swapee.ISwapee.__connectExchanges
 * @template THIS
 */
/** @typedef {xyz.swapee.ISwapee.__connectExchanges<!xyz.swapee.ISwapee>} xyz.swapee.ISwapee._connectExchanges */
/** @typedef {typeof xyz.swapee.ISwapee.connectExchanges} */
/**
 *  `🔗 $combine`
 * @param {!xyz.swapee.ISwapee.connectExchanges.Opts} [opts] The options.
 * - `[amount="0.007"]` _string?_ Default `0.007`.
 * - `[cryptoTo="eth"]` _string?_ Default `eth`.
 * - `[cryptoFrom="btc"]` _string?_ Default `btc`.
 * @return {!Promise<xyz.swapee.ISwapee.connectExchanges.Return>}
 */
xyz.swapee.ISwapee.connectExchanges = function(opts) {}

/**
 * @typedef {Object} xyz.swapee.ISwapee.connectExchanges.Opts The options.
 * @prop {string} [amount="0.007"] Default `0.007`.
 * @prop {string} [cryptoTo="eth"] Default `eth`.
 * @prop {string} [cryptoFrom="btc"] Default `btc`.
 */

/**
 * @typedef {Object} xyz.swapee.ISwapee.connectExchanges.Return
 * @prop {!Object<string, Object>} exchanges
 * @prop {!Map} results
 */

/**
 * @typedef {Object} xyz.swapee.swapee_xyz.Config Additional options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */