/** @const {?} */ io.changenow.AbstractChangeNow
/** @const {?} */ io.changenow.AbstractChangeNowUniversal
/** @const {?} */ io.changenow.UChangeNow
/** @const {?} */ io.changenow.IChangeNow
/** @const {?} */ io.letsexchange.AbstractLetsExchange
/** @const {?} */ io.letsexchange.AbstractLetsExchangeUniversal
/** @const {?} */ io.letsexchange.ULetsExchange
/** @const {?} */ io.letsexchange.ILetsExchange
/** @const {?} */ $io.changenow.UChangeNow
/** @const {?} */ $io.changenow.IChangeNow
/** @const {?} */ $io.letsexchange.ULetsExchange
/** @const {?} */ $io.letsexchange.ILetsExchange
/** @const {?} */ xyz.swapee.AbstractSwapeeAide
/** @const {?} */ xyz.swapee.ISwapeeAide
/** @const {?} */ xyz.swapee.AbstractSwapee
/** @const {?} */ xyz.swapee.ISwapee
/** @const {?} */ xyz.swapee.AbstractChangellyConnector
/** @const {?} */ xyz.swapee.IChangellyConnector
/** @const {?} */ $xyz.swapee.ISwapeeAide
/** @const {?} */ $xyz.swapee.ISwapee
/** @const {?} */ $xyz.swapee.IChangellyConnector
/** @typedef {!io.changenow.IChangeNow.getSymbols} */
io.changenow.AbstractChangeNow.getSymbols

/** @typedef {!io.changenow.IChangeNow.setSymbols} */
io.changenow.AbstractChangeNow.setSymbols

/** @typedef {!io.changenow.IChangeNow.Symbols} */
io.changenow.AbstractChangeNow.Symbols

/** @typedef {!io.changenow.UChangeNow.getSymbols} */
io.changenow.AbstractChangeNowUniversal.getSymbols

/** @typedef {!io.changenow.UChangeNow.setSymbols} */
io.changenow.AbstractChangeNowUniversal.setSymbols

/** @typedef {!io.changenow.UChangeNow.Symbols} */
io.changenow.AbstractChangeNowUniversal.Symbols

/** @record */
$io.changenow.UChangeNow.SymbolsIn = function() {}
/** @type {io.changenow.IChangeNow|undefined} */
$io.changenow.UChangeNow.SymbolsIn.prototype._changeNow
/** @typedef {$io.changenow.UChangeNow.SymbolsIn} */
io.changenow.UChangeNow.SymbolsIn

/** @record */
$io.changenow.UChangeNow.Symbols = function() {}
/** @type {symbol} */
$io.changenow.UChangeNow.Symbols.prototype._changeNow
/** @typedef {$io.changenow.UChangeNow.Symbols} */
io.changenow.UChangeNow.Symbols

/** @record */
$io.changenow.UChangeNow.SymbolsOut = function() {}
/** @type {io.changenow.IChangeNow} */
$io.changenow.UChangeNow.SymbolsOut.prototype._changeNow
/** @typedef {$io.changenow.UChangeNow.SymbolsOut} */
io.changenow.UChangeNow.SymbolsOut

/** @typedef {function(!io.changenow.UChangeNow, !io.changenow.UChangeNow.SymbolsIn): void} */
io.changenow.UChangeNow.setSymbols

/** @typedef {function(!io.changenow.UChangeNow): !io.changenow.UChangeNow.SymbolsOut} */
io.changenow.UChangeNow.getSymbols

/** @record */
$io.changenow.IChangeNow.SymbolsIn = function() {}
/** @type {string|undefined} */
$io.changenow.IChangeNow.SymbolsIn.prototype._host
/** @type {string|undefined} */
$io.changenow.IChangeNow.SymbolsIn.prototype._apiPath
/** @type {string|undefined} */
$io.changenow.IChangeNow.SymbolsIn.prototype._changeNowApiKey
/** @typedef {$io.changenow.IChangeNow.SymbolsIn} */
io.changenow.IChangeNow.SymbolsIn

/** @record */
$io.changenow.IChangeNow.Symbols = function() {}
/** @type {symbol} */
$io.changenow.IChangeNow.Symbols.prototype._host
/** @type {symbol} */
$io.changenow.IChangeNow.Symbols.prototype._apiPath
/** @type {symbol} */
$io.changenow.IChangeNow.Symbols.prototype._changeNowApiKey
/** @typedef {$io.changenow.IChangeNow.Symbols} */
io.changenow.IChangeNow.Symbols

/** @record */
$io.changenow.IChangeNow.SymbolsOut = function() {}
/** @type {string} */
$io.changenow.IChangeNow.SymbolsOut.prototype._host
/** @type {string} */
$io.changenow.IChangeNow.SymbolsOut.prototype._apiPath
/** @type {string} */
$io.changenow.IChangeNow.SymbolsOut.prototype._changeNowApiKey
/** @typedef {$io.changenow.IChangeNow.SymbolsOut} */
io.changenow.IChangeNow.SymbolsOut

/** @typedef {function(!io.changenow.IChangeNow, !io.changenow.IChangeNow.SymbolsIn): void} */
io.changenow.IChangeNow.setSymbols

/** @typedef {function(!io.changenow.IChangeNow): !io.changenow.IChangeNow.SymbolsOut} */
io.changenow.IChangeNow.getSymbols

/** @typedef {!io.letsexchange.ILetsExchange.getSymbols} */
io.letsexchange.AbstractLetsExchange.getSymbols

/** @typedef {!io.letsexchange.ILetsExchange.setSymbols} */
io.letsexchange.AbstractLetsExchange.setSymbols

/** @typedef {!io.letsexchange.ILetsExchange.Symbols} */
io.letsexchange.AbstractLetsExchange.Symbols

/** @typedef {!io.letsexchange.ULetsExchange.getSymbols} */
io.letsexchange.AbstractLetsExchangeUniversal.getSymbols

/** @typedef {!io.letsexchange.ULetsExchange.setSymbols} */
io.letsexchange.AbstractLetsExchangeUniversal.setSymbols

/** @typedef {!io.letsexchange.ULetsExchange.Symbols} */
io.letsexchange.AbstractLetsExchangeUniversal.Symbols

/** @record */
$io.letsexchange.ULetsExchange.SymbolsIn = function() {}
/** @type {io.letsexchange.ILetsExchange|undefined} */
$io.letsexchange.ULetsExchange.SymbolsIn.prototype._letsExchange
/** @typedef {$io.letsexchange.ULetsExchange.SymbolsIn} */
io.letsexchange.ULetsExchange.SymbolsIn

/** @record */
$io.letsexchange.ULetsExchange.Symbols = function() {}
/** @type {symbol} */
$io.letsexchange.ULetsExchange.Symbols.prototype._letsExchange
/** @typedef {$io.letsexchange.ULetsExchange.Symbols} */
io.letsexchange.ULetsExchange.Symbols

/** @record */
$io.letsexchange.ULetsExchange.SymbolsOut = function() {}
/** @type {io.letsexchange.ILetsExchange} */
$io.letsexchange.ULetsExchange.SymbolsOut.prototype._letsExchange
/** @typedef {$io.letsexchange.ULetsExchange.SymbolsOut} */
io.letsexchange.ULetsExchange.SymbolsOut

/** @typedef {function(!io.letsexchange.ULetsExchange, !io.letsexchange.ULetsExchange.SymbolsIn): void} */
io.letsexchange.ULetsExchange.setSymbols

/** @typedef {function(!io.letsexchange.ULetsExchange): !io.letsexchange.ULetsExchange.SymbolsOut} */
io.letsexchange.ULetsExchange.getSymbols

/** @record */
$io.letsexchange.ILetsExchange.SymbolsIn = function() {}
/** @type {string|undefined} */
$io.letsexchange.ILetsExchange.SymbolsIn.prototype._host
/** @type {string|undefined} */
$io.letsexchange.ILetsExchange.SymbolsIn.prototype._apiPath
/** @type {string|undefined} */
$io.letsexchange.ILetsExchange.SymbolsIn.prototype._letsExchangeApiKey
/** @typedef {$io.letsexchange.ILetsExchange.SymbolsIn} */
io.letsexchange.ILetsExchange.SymbolsIn

/** @record */
$io.letsexchange.ILetsExchange.Symbols = function() {}
/** @type {symbol} */
$io.letsexchange.ILetsExchange.Symbols.prototype._host
/** @type {symbol} */
$io.letsexchange.ILetsExchange.Symbols.prototype._apiPath
/** @type {symbol} */
$io.letsexchange.ILetsExchange.Symbols.prototype._letsExchangeApiKey
/** @typedef {$io.letsexchange.ILetsExchange.Symbols} */
io.letsexchange.ILetsExchange.Symbols

/** @record */
$io.letsexchange.ILetsExchange.SymbolsOut = function() {}
/** @type {string} */
$io.letsexchange.ILetsExchange.SymbolsOut.prototype._host
/** @type {string} */
$io.letsexchange.ILetsExchange.SymbolsOut.prototype._apiPath
/** @type {string} */
$io.letsexchange.ILetsExchange.SymbolsOut.prototype._letsExchangeApiKey
/** @typedef {$io.letsexchange.ILetsExchange.SymbolsOut} */
io.letsexchange.ILetsExchange.SymbolsOut

/** @typedef {function(!io.letsexchange.ILetsExchange, !io.letsexchange.ILetsExchange.SymbolsIn): void} */
io.letsexchange.ILetsExchange.setSymbols

/** @typedef {function(!io.letsexchange.ILetsExchange): !io.letsexchange.ILetsExchange.SymbolsOut} */
io.letsexchange.ILetsExchange.getSymbols

/** @typedef {!xyz.swapee.ISwapeeAide.getSymbols} */
xyz.swapee.AbstractSwapeeAide.getSymbols

/** @typedef {!xyz.swapee.ISwapeeAide.setSymbols} */
xyz.swapee.AbstractSwapeeAide.setSymbols

/** @typedef {!xyz.swapee.ISwapeeAide.Symbols} */
xyz.swapee.AbstractSwapeeAide.Symbols

/** @record */
$xyz.swapee.ISwapeeAide.SymbolsIn = function() {}
/** @type {string|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._output
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._showHelp
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._testExchanges
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._showVersion
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._debug
/** @type {string|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._version
/** @typedef {$xyz.swapee.ISwapeeAide.SymbolsIn} */
xyz.swapee.ISwapeeAide.SymbolsIn

/** @record */
$xyz.swapee.ISwapeeAide.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._output
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._showHelp
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._testExchanges
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._showVersion
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._debug
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._version
/** @typedef {$xyz.swapee.ISwapeeAide.Symbols} */
xyz.swapee.ISwapeeAide.Symbols

/** @record */
$xyz.swapee.ISwapeeAide.SymbolsOut = function() {}
/** @type {string} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._output
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._showHelp
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._testExchanges
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._showVersion
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._debug
/** @type {string} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._version
/** @typedef {$xyz.swapee.ISwapeeAide.SymbolsOut} */
xyz.swapee.ISwapeeAide.SymbolsOut

/** @typedef {function(!xyz.swapee.ISwapeeAide, !xyz.swapee.ISwapeeAide.SymbolsIn): void} */
xyz.swapee.ISwapeeAide.setSymbols

/** @typedef {function(!xyz.swapee.ISwapeeAide): !xyz.swapee.ISwapeeAide.SymbolsOut} */
xyz.swapee.ISwapeeAide.getSymbols

/** @typedef {!xyz.swapee.ISwapee.getSymbols} */
xyz.swapee.AbstractSwapee.getSymbols

/** @typedef {!xyz.swapee.ISwapee.setSymbols} */
xyz.swapee.AbstractSwapee.setSymbols

/** @typedef {!xyz.swapee.ISwapee.Symbols} */
xyz.swapee.AbstractSwapee.Symbols

/** @record */
$xyz.swapee.ISwapee.SymbolsIn = function() {}
/** @type {string|undefined} */
$xyz.swapee.ISwapee.SymbolsIn.prototype._text
/** @typedef {$xyz.swapee.ISwapee.SymbolsIn} */
xyz.swapee.ISwapee.SymbolsIn

/** @record */
$xyz.swapee.ISwapee.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.ISwapee.Symbols.prototype._text
/** @typedef {$xyz.swapee.ISwapee.Symbols} */
xyz.swapee.ISwapee.Symbols

/** @record */
$xyz.swapee.ISwapee.SymbolsOut = function() {}
/** @type {string} */
$xyz.swapee.ISwapee.SymbolsOut.prototype._text
/** @typedef {$xyz.swapee.ISwapee.SymbolsOut} */
xyz.swapee.ISwapee.SymbolsOut

/** @typedef {function(!xyz.swapee.ISwapee, !xyz.swapee.ISwapee.SymbolsIn): void} */
xyz.swapee.ISwapee.setSymbols

/** @typedef {function(!xyz.swapee.ISwapee): !xyz.swapee.ISwapee.SymbolsOut} */
xyz.swapee.ISwapee.getSymbols

/** @typedef {!xyz.swapee.IChangellyConnector.getSymbols} */
xyz.swapee.AbstractChangellyConnector.getSymbols

/** @typedef {!xyz.swapee.IChangellyConnector.setSymbols} */
xyz.swapee.AbstractChangellyConnector.setSymbols

/** @typedef {!xyz.swapee.IChangellyConnector.Symbols} */
xyz.swapee.AbstractChangellyConnector.Symbols

/** @record */
$xyz.swapee.IChangellyConnector.SymbolsIn = function() {}
/** @type {((boolean|string))|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._cacheChangellyConnectResponse
/** @type {string|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorFrom
/** @type {string|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorTo
/** @type {number|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.SymbolsIn} */
xyz.swapee.IChangellyConnector.SymbolsIn

/** @record */
$xyz.swapee.IChangellyConnector.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._cacheChangellyConnectResponse
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorFrom
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorTo
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.Symbols} */
xyz.swapee.IChangellyConnector.Symbols

/** @record */
$xyz.swapee.IChangellyConnector.SymbolsOut = function() {}
/** @type {(boolean|string)} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._cacheChangellyConnectResponse
/** @type {string} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorFrom
/** @type {string} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorTo
/** @type {number} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.SymbolsOut} */
xyz.swapee.IChangellyConnector.SymbolsOut

/** @typedef {function(!xyz.swapee.IChangellyConnector, !xyz.swapee.IChangellyConnector.SymbolsIn): void} */
xyz.swapee.IChangellyConnector.setSymbols

/** @typedef {function(!xyz.swapee.IChangellyConnector): !xyz.swapee.IChangellyConnector.SymbolsOut} */
xyz.swapee.IChangellyConnector.getSymbols