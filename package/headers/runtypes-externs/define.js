/** @define {string} */
var XYZ_SWAPEE_SWAPEE_XYZ_DEMO_VERSION=''
/** @define {boolean} */
var XYZ_SWAPEE_SWAPEE_XYZ_COMPILE_SHARED=false
/** @define {string} */
var XYZ_SWAPEE_SWAPEE_XYZ_PACKAGE_NAME='@swapee.xyz/swapee_xyz'
/** @define {boolean} */
var XYZ_SWAPEE_SWAPEE_XYZ_BROWSER=false