import {license, LOCAL, placeTypology, removeTypalMeta} from '@artdeco/package'
import {name as _name} from '../package.json'
import {TYPEDEFS} from './common'

export {DESIGNS} from './DESIGNS.env'
export {HEADERS} from './HEADERS.env'
export {LIB} from './LIB.env'
export {LUDDITES} from './LUDDITES.env'
export {license}
export {LOCAL}


export let name=_name

export let keywords=[ 'swapee', 'crypto', 'aggregator' ]

export let author='Art Deco™<packages@artdeco.software>'
export let bugs={
 url: 'https://artdeco.software/issues/',
 email: 'issues+xyz.swapee+swapee_xyz@artdeco.software',
}
export let homepage='https://artdeco.software/xyz.swapee/swapee_xyz/'

LOCAL.copy={
 ...LOCAL.copy,
 ...TYPEDEFS,
 'types/db/typology.js':'types/typology.mjs',
}
LOCAL.postProcessing=() => {
 removeTypalMeta('compile/dist/paid','types/typedefs.js')
 placeTypology('compile/dist/paid')
}
LOCAL.bin={
 "swapee_xyz": "3600135954/bin/swapee_xyz.js",
}