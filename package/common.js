export const TYPEDEFS = {
 // 'node_modules/@type.engineering/type-engineer.h/typedefs/index.js': 'types/type-engineer.typedefs.js',
}

export const TYPOLOGY={
 'compile/dist/paid/types/typology.js':'types/typology.js',
 'types/db/typology.js':'types/typology.mjs',
}

export const EXTERNS = {
 'types/runtypes-externs/ns/xyz.swapee.js':'types/ns/xyz.swapee.js',
 'types/runtypes-externs/ns/io.changenow.js':'types/ns/io.changenow.js',
 'types/runtypes-externs/ns/io.letsexchange.js':'types/ns/io.letsexchange.js',
 'types/runtypes-externs/index.externs.js':'types/externs.js',
}

export const RUNTYPES={
 'types/runtypes-externs/ns/$xyz.ns':'types/ns/$xyz.ns',
 'types/runtypes-externs/ns/$xyz.swapee.ns':'types/ns/$xyz.swapee.ns',
 'types/runtypes-externs/ns/$io.ns':'types/ns/$io.ns',
 'types/runtypes-externs/ns/$io.changenow.ns':'types/ns/$io.changenow.ns',
 'types/runtypes-externs/ns/$io.letsexchange.ns':'types/ns/$io.letsexchange.ns',
 'types/runtypes-externs/define.js':'types/define.js',
}