/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
*/
/*

 @type.engineer/network by Type Engineering (c) 2023
 Commercial License.
*/
const ea=function(){return require(eval('/*dequire*/"@type.engineer/network"'))}().Network;const fa=require("os").EOL;const ha=require("url").parse;require("https");require("http");require("util");require("stream");require("zlib");require("crypto");module.exports={get fa(){return fa},get ea(){return ea},get ha(){return ha}};

//# sourceMappingURL=A.js.map
