# @swapee.xyz/swapee.xyz

The Swapee Crypto Aggregator Website.

A new website made with [Splendid][1]: https://swapee.xyz.

## Copyright

(c) [Art Deco™][2] 2024

[1]: https://www.npmjs.com/package/splendid
[2]: https://artd.eco