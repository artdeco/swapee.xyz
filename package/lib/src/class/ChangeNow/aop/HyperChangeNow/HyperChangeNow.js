import ChangeNow from '../../ChangeNow'
import AbstractHyperChangeNow from '../../gen/hyper/AbstractHyperChangeNow'
import ChangeNowGeneralAspects from '../ChangeNowGeneralAspects'

/** @extends {io.changenow.HyperChangeNow} */
export default class extends AbstractHyperChangeNow
 .consults(
  ChangeNowGeneralAspects,
 )
 .implements(
  ChangeNow,
  // AbstractHyperChangeNow.class.prototype=/**@type {!HyperChangeNow}*/({
  // }),
 )
{}