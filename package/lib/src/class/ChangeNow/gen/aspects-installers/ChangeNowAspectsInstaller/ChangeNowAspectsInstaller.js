import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.changenow.AbstractChangeNowAspectsInstaller}
 */
class _ChangeNowAspectsInstaller { }

_ChangeNowAspectsInstaller.prototype[$advice]=true

/** @extends {io.changenow.AbstractChangeNowAspectsInstaller} ‎ */
class ChangeNowAspectsInstaller extends newAbstract(
 _ChangeNowAspectsInstaller,3600135954174,null,{},false) {}

/** @type {typeof io.changenow.AbstractChangeNowAspectsInstaller} */
ChangeNowAspectsInstaller.class=function(){}
/** @type {typeof io.changenow.AbstractChangeNowAspectsInstaller} */
function ChangeNowAspectsInstallerClass(){}

export default ChangeNowAspectsInstaller


ChangeNowAspectsInstaller[$implementations]=[
 ChangeNowAspectsInstallerClass.prototype=/**@type {!io.changenow.IChangeNowAspectsInstaller}*/({
  GetExchangeAmount(){
   this.before_GetExchangeAmount=1
   this.after_GetExchangeAmount=2
   this.around_GetExchangeAmount=3
   this.after_GetExchangeAmountThrows=4
   this.after_GetExchangeAmountReturns=5
   this.immediatelyAfter_GetExchangeAmount=6
   this.after_GetExchangeAmountCancels=7
   return {
    data:1,
   }
  },
  GetFixedExchangeAmount(){
   this.before_GetFixedExchangeAmount=1
   this.after_GetFixedExchangeAmount=2
   this.around_GetFixedExchangeAmount=3
   this.after_GetFixedExchangeAmountThrows=4
   this.after_GetFixedExchangeAmountReturns=5
   this.immediatelyAfter_GetFixedExchangeAmount=6
   this.after_GetFixedExchangeAmountCancels=7
   return {
    data:1,
   }
  },
 }),
]