import ChangeNowUniversal, {setChangeNowUniversalSymbols,getChangeNowUniversalSymbols} from './anchors/ChangeNowUniversal'
import { newAbstract, $implementations, iu, $initialese } from '@type.engineering/type-engineer'
import '../../../../types/types'

/**
 * @abstract
 * @extends {io.changenow.AbstractChangeNow}
 */
class _AbstractChangeNow { }
/**
 * The _ChangeNow_ client.
 * @extends {io.changenow.AbstractChangeNow} ‎
 */
class AbstractChangeNow extends newAbstract(
 _AbstractChangeNow,3600135954179,null,{
  asIChangeNow:1,
  superChangeNow:2,
 },false,{
  host:{_host:1},
  apiPath:{_apiPath:2},
  changeNowApiKey:{_changeNowApiKey:3},
 }) {}

/** @type {typeof io.changenow.AbstractChangeNow} */
AbstractChangeNow.class=function(){}
/** @type {typeof io.changenow.AbstractChangeNow} */
function AbstractChangeNowClass(){}

export default AbstractChangeNow

/** @type {io.changenow.IChangeNow.Symbols} */
export const ChangeNowSymbols=AbstractChangeNow.Symbols
/** @type {io.changenow.IChangeNow.getSymbols} */
export const getChangeNowSymbols=AbstractChangeNow.getSymbols
/** @type {io.changenow.IChangeNow.setSymbols} */
export const setChangeNowSymbols=AbstractChangeNow.setSymbols

AbstractChangeNow[$implementations]=[
 AbstractChangeNowClass.prototype=/**@type {!io.changenow.ChangeNow}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_host,_apiPath,_changeNowApiKey}=getChangeNowSymbols(this)
   setChangeNowSymbols(this,{
    _host:iu(_host,'https://api.changenow.io'),
    _apiPath:iu(_apiPath,'/v2'),
    _changeNowApiKey:iu(_changeNowApiKey,''),
   })
  },
 }),
 AbstractChangeNowClass.prototype=/**@type {!io.changenow.ChangeNow}*/({
  constructor:function selfUniversalConstructor(){
   setChangeNowUniversalSymbols(this,{_changeNow:this})
  },
 }),
 AbstractChangeNowClass.prototype=/**@type {!io.changenow.ChangeNow}*/({
  constructor:function reverseUniversalConstructor(){
   for(const arg of arguments) {
    const asyms=getChangeNowUniversalSymbols(arg)
    if(asyms._changeNow===null) {
     setChangeNowUniversalSymbols(arg,{_changeNow:this})
    }
   }
  },
 }),
 ChangeNowUniversal,
 /** @type {!io.changenow.ChangeNow} */ ({
  preinitializer() {
   const{
    'IO_CHANGENOW_API_KEY':_changeNowApiKey,
   }=process.env

   return {
    changeNowApiKey: _changeNowApiKey?_changeNowApiKey.split(/\s*#/)[0]:_changeNowApiKey,
   }
  },
 }),
 /** @type {!io.changenow.ChangeNow} */ ({
  [$initialese]:/**@type {!io.changenow.IChangeNow.Initialese}*/({
   host:1,
   apiPath:1,
   changeNowApiKey:1,
  }),
  initializer({
   host:_host,
   apiPath:_apiPath,
   changeNowApiKey:_changeNowApiKey,
  }) {
   setChangeNowSymbols(this, {
    _host:_host,
    _apiPath:_apiPath,
    _changeNowApiKey:_changeNowApiKey,
   })
  },
 }),
]