import ChangeNowAspectsInstaller from '../../aspects-installers/ChangeNowAspectsInstaller'
import { newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.changenow.AbstractHyperChangeNow}
 */
class _AbstractHyperChangeNow { }
/** @extends {io.changenow.AbstractHyperChangeNow} ‎ */
class AbstractHyperChangeNow extends newAbstract(
 _AbstractHyperChangeNow,3600135954178,null,{
  asIHyperChangeNow:ChangeNowAspectsInstaller,
  superHyperChangeNow:2,
 },false) {}

/** @type {typeof io.changenow.AbstractHyperChangeNow} */
AbstractHyperChangeNow.class=function(){}
/** @type {typeof io.changenow.AbstractHyperChangeNow} */
function AbstractHyperChangeNowClass(){}

export default AbstractHyperChangeNow