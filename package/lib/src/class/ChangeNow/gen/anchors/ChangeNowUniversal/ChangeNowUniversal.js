import { newAbstract, $implementations, iu, universalConstructor, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.changenow.AbstractChangeNowUniversal}
 */
class _ChangeNowUniversal { }
/**
 * A trait that allows to access an _IChangeNow_ object via a `.changeNow` field.
 * @extends {io.changenow.AbstractChangeNowUniversal} ‎
 */
class ChangeNowUniversal extends newAbstract(
 _ChangeNowUniversal,3600135954175,null,{
  asIChangeNow:1,
  asUChangeNow:3,
  changeNow:4,
  asChangeNow:5,
  superChangeNowUniversal:2,
 },false,{
  changeNow:{_changeNow:1},
 }) {}

/** @type {typeof io.changenow.UChangeNow} */
ChangeNowUniversal.class=function(){}
/** @type {typeof io.changenow.UChangeNow} */
function ChangeNowUniversalClass(){}

export default ChangeNowUniversal

/** @type {io.changenow.UChangeNow.Symbols} */
export const ChangeNowUniversalSymbols=ChangeNowUniversal.Symbols
/** @type {io.changenow.UChangeNow.getSymbols} */
export const getChangeNowUniversalSymbols=ChangeNowUniversal.getSymbols
/** @type {io.changenow.UChangeNow.setSymbols} */
export const setChangeNowUniversalSymbols=ChangeNowUniversal.setSymbols

ChangeNowUniversal[$implementations]=[
 ChangeNowUniversalClass.prototype=/**@type {!io.changenow.ChangeNowUniversal}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_changeNow}=getChangeNowUniversalSymbols(this)
   setChangeNowUniversalSymbols(this,{
    _changeNow:iu(_changeNow,null),
   })
  },
 }),
 ChangeNowUniversalClass.prototype=/**@type {!io.changenow.ChangeNowUniversal}*/({
  constructor:universalConstructor(ChangeNowUniversal),
 }),
 ChangeNowUniversalClass.prototype=/**@type {!io.changenow.UChangeNow}*/({
  [ChangeNowUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!io.changenow.UChangeNow} */ ({
  [$initialese]:/**@type {!io.changenow.UChangeNow.Initialese}*/({
   changeNow:1,
  }),
 }),
]
/**@type {io.changenow.ChangeNowMetaUniversal}*/
export const ChangeNowMetaUniversal=ChangeNowUniversal.MetaUniversal