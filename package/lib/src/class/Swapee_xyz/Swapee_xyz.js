import AbstractSwapee_xyz from './gen/AbstractSwapee_xyz'
import _run from './methods/run'

/** @extends {xyz.swapee.Swapee_xyz} */
export default class extends AbstractSwapee_xyz.implements(
 AbstractSwapee_xyz.class.prototype=/** @type {!xyz.swapee.Swapee_xyz}*/({
  run:_run,
 }),
) {}