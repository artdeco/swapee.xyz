import { newAbstract, $implementations, iu, $initialese } from '@type.engineering/type-engineer'
import '../../../../types'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapee_xyz}
 */
class _AbstractSwapee_xyz {
  /** @return {void} */
  __$constructor() {
    /** @type {string} */ this.example = 'ok'
  }
}
/**
 * The package.
 * @extends {xyz.swapee.AbstractSwapee_xyz} ‎
 */
class AbstractSwapee_xyz extends newAbstract(
 _AbstractSwapee_xyz, 16817934091,null,{
  asISwapee_xyz:1,
  superSwapee_xyz:2,
 },false,{
  symbol:{_symbol:1},
 }) {}

/** @type {typeof xyz.swapee.AbstractSwapee_xyz} */
AbstractSwapee_xyz.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapee_xyz} */
function AbstractSwapee_xyzClass(){}

export default AbstractSwapee_xyz

/** @type {xyz.swapee.ISwapee_xyz.Symbols} */
export const Swapee_xyzSymbols=AbstractSwapee_xyz.Symbols
/** @type {xyz.swapee.ISwapee_xyz.getSymbols} */
export const getSwapee_xyzSymbols=AbstractSwapee_xyz.getSymbols
/** @type {xyz.swapee.ISwapee_xyz.setSymbols} */
export const setSwapee_xyzSymbols=AbstractSwapee_xyz.setSymbols

AbstractSwapee_xyz[$implementations]=[
 AbstractSwapee_xyzClass.prototype=/**@type {!xyz.swapee.Swapee_xyz}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_symbol}=getSwapee_xyzSymbols(this)
   setSwapee_xyzSymbols(this,{
    _symbol:iu(_symbol,''),
   })
  },
 }),
 /** @type {!xyz.swapee.Swapee_xyz} */ ({
  [$initialese]:/**@type {!xyz.swapee.ISwapee_xyz.Initialese}*/({
   symbol:1,
  }),
  initializer({
   symbol:_symbol,
  }) {
   setSwapee_xyzSymbols(this, {
    _symbol:_symbol,
   })
  },
 }),
]