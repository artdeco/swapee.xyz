import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.letsexchange.AbstractLetsExchangeAspectsInstaller}
 */
class _LetsExchangeAspectsInstaller { }

_LetsExchangeAspectsInstaller.prototype[$advice]=true

/** @extends {io.letsexchange.AbstractLetsExchangeAspectsInstaller} ‎ */
class LetsExchangeAspectsInstaller extends newAbstract(
 _LetsExchangeAspectsInstaller,3600135954163,null,{},false) {}

/** @type {typeof io.letsexchange.AbstractLetsExchangeAspectsInstaller} */
LetsExchangeAspectsInstaller.class=function(){}
/** @type {typeof io.letsexchange.AbstractLetsExchangeAspectsInstaller} */
function LetsExchangeAspectsInstallerClass(){}

export default LetsExchangeAspectsInstaller


LetsExchangeAspectsInstaller[$implementations]=[
 LetsExchangeAspectsInstallerClass.prototype=/**@type {!io.letsexchange.ILetsExchangeAspectsInstaller}*/({
  GetInfo(){
   this.before_GetInfo=1
   this.after_GetInfo=2
   this.around_GetInfo=3
   this.after_GetInfoThrows=4
   this.after_GetInfoReturns=5
   this.immediatelyAfter_GetInfo=6
   this.after_GetInfoCancels=7
   return {
    data:1,
   }
  },
 }),
]