import { newAbstract, $implementations, iu, universalConstructor, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.letsexchange.AbstractLetsExchangeUniversal}
 */
class _LetsExchangeUniversal { }
/**
 * A trait that allows to access an _ILetsExchange_ object via a `.letsExchange` field.
 * @extends {io.letsexchange.AbstractLetsExchangeUniversal} ‎
 */
class LetsExchangeUniversal extends newAbstract(
 _LetsExchangeUniversal,3600135954170,null,{
  asILetsExchange:1,
  asULetsExchange:3,
  letsExchange:4,
  asLetsExchange:5,
  superLetsExchangeUniversal:2,
 },false,{
  letsExchange:{_letsExchange:1},
 }) {}

/** @type {typeof io.letsexchange.ULetsExchange} */
LetsExchangeUniversal.class=function(){}
/** @type {typeof io.letsexchange.ULetsExchange} */
function LetsExchangeUniversalClass(){}

export default LetsExchangeUniversal

/** @type {io.letsexchange.ULetsExchange.Symbols} */
export const LetsExchangeUniversalSymbols=LetsExchangeUniversal.Symbols
/** @type {io.letsexchange.ULetsExchange.getSymbols} */
export const getLetsExchangeUniversalSymbols=LetsExchangeUniversal.getSymbols
/** @type {io.letsexchange.ULetsExchange.setSymbols} */
export const setLetsExchangeUniversalSymbols=LetsExchangeUniversal.setSymbols

LetsExchangeUniversal[$implementations]=[
 LetsExchangeUniversalClass.prototype=/**@type {!io.letsexchange.LetsExchangeUniversal}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_letsExchange}=getLetsExchangeUniversalSymbols(this)
   setLetsExchangeUniversalSymbols(this,{
    _letsExchange:iu(_letsExchange,null),
   })
  },
 }),
 LetsExchangeUniversalClass.prototype=/**@type {!io.letsexchange.LetsExchangeUniversal}*/({
  constructor:universalConstructor(LetsExchangeUniversal),
 }),
 LetsExchangeUniversalClass.prototype=/**@type {!io.letsexchange.ULetsExchange}*/({
  [LetsExchangeUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!io.letsexchange.ULetsExchange} */ ({
  [$initialese]:/**@type {!io.letsexchange.ULetsExchange.Initialese}*/({
   letsExchange:1,
  }),
 }),
]
/**@type {io.letsexchange.LetsExchangeMetaUniversal}*/
export const LetsExchangeMetaUniversal=LetsExchangeUniversal.MetaUniversal