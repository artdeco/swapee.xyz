import { newAbstract, $implementations, iu, prereturnFirst, precombined, $initialese } from '@type.engineering/type-engineer'
import '../../../../types/types'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapee}
 */
class _AbstractSwapee {
  /** @return {void} */
  __$constructor() {
    /** @type {string} */ this.example='ok'
  }
}
/**
 * The package.
 * @extends {xyz.swapee.AbstractSwapee} ‎
 */
class AbstractSwapee extends newAbstract(
 _AbstractSwapee,3600135954238,null,{
  asISwapee:1,
  superSwapee:2,
 },false,{
  text:{_text:3},
 }) {}

/** @type {typeof xyz.swapee.AbstractSwapee} */
AbstractSwapee.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapee} */
function AbstractSwapeeClass(){}

export default AbstractSwapee

/** @type {xyz.swapee.ISwapee.Symbols} */
export const SwapeeSymbols=AbstractSwapee.Symbols
/** @type {xyz.swapee.ISwapee.getSymbols} */
export const getSwapeeSymbols=AbstractSwapee.getSymbols
/** @type {xyz.swapee.ISwapee.setSymbols} */
export const setSwapeeSymbols=AbstractSwapee.setSymbols

AbstractSwapee[$implementations]=[
 AbstractSwapeeClass.prototype=/**@type {!xyz.swapee.Swapee}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_text}=getSwapeeSymbols(this)
   setSwapeeSymbols(this,{
    _text:iu(_text,''),
   })
  },
 }),
 AbstractSwapeeClass.prototype=/**@type {!xyz.swapee.ISwapee}*/({
  run:prereturnFirst,
  connectExchanges:precombined,
 }),
 /** @type {!xyz.swapee.Swapee} */ ({
  [$initialese]:/**@type {!xyz.swapee.ISwapee.Initialese}*/({
   text:1,
  }),
  initializer({
   text:_text,
  }) {
   setSwapeeSymbols(this, {
    _text:_text,
   })
  },
 }),
]