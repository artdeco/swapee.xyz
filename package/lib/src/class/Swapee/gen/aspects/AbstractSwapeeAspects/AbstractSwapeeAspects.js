import { newAspects } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapeeAspects}
 */
class _AbstractSwapeeAspects { }
/**
 * The aspects of the *ISwapee*.
 * @extends {xyz.swapee.AbstractSwapeeAspects} ‎
 */
class AbstractSwapeeAspects extends newAspects(
 _AbstractSwapeeAspects,3600135954244,null,{},false) {}

/** @type {typeof xyz.swapee.AbstractSwapeeAspects} */
AbstractSwapeeAspects.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapeeAspects} */
function AbstractSwapeeAspectsClass(){}

export default AbstractSwapeeAspects