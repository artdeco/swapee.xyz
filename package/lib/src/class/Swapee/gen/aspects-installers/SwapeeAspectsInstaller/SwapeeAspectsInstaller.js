import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapeeAspectsInstaller}
 */
class _SwapeeAspectsInstaller { }

_SwapeeAspectsInstaller.prototype[$advice]=true

/** @extends {xyz.swapee.AbstractSwapeeAspectsInstaller} ‎ */
class SwapeeAspectsInstaller extends newAbstract(
 _SwapeeAspectsInstaller,3600135954242,null,{},false) {}

/** @type {typeof xyz.swapee.AbstractSwapeeAspectsInstaller} */
SwapeeAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapeeAspectsInstaller} */
function SwapeeAspectsInstallerClass(){}

export default SwapeeAspectsInstaller


SwapeeAspectsInstaller[$implementations]=[
 SwapeeAspectsInstallerClass.prototype=/**@type {!xyz.swapee.ISwapeeAspectsInstaller}*/({
  run(){
   this.beforeRun=1
   this.afterRun=2
   this.aroundRun=3
   this.afterRunThrows=4
   this.afterRunReturns=5
   this.immediatelyAfterRun=6
   this.afterRunCancels=7
   this.beforeEachRun=8
   this.afterEachRun=9
   this.afterEachRunReturns=10
   return {
    conf:1,
   }
  },
  ShowText(){
   this.before_ShowText=1
   this.after_ShowText=2
   this.around_ShowText=3
   this.after_ShowTextThrows=4
   this.after_ShowTextReturns=5
   this.immediatelyAfter_ShowText=6
   this.after_ShowTextCancels=7
   return {
    conf:1,
   }
  },
  connectExchanges(){
   this.beforeConnectExchanges=1
   this.afterConnectExchanges=2
   this.aroundConnectExchanges=3
   this.afterConnectExchangesThrows=4
   this.afterConnectExchangesReturns=5
   this.immediatelyAfterConnectExchanges=6
   this.afterConnectExchangesCancels=7
   this.beforeEachConnectExchanges=8
   this.afterEachConnectExchanges=9
   this.afterEachConnectExchangesReturns=10
   return {
    opts:1,
   }
  },
 }),
]