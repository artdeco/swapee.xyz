import SwapeeAide from '../../SwapeeAide'
import AbstractHyperSwapeeAide from '../../gen/hyper/AbstractHyperSwapeeAide'
import SwapeeAideGeneralAspects from '../SwapeeAideGeneralAspects'

/** @extends {xyz.swapee.HyperSwapeeAide} */
export default class extends AbstractHyperSwapeeAide
 .consults(
  SwapeeAideGeneralAspects,
 )
 .implements(
  SwapeeAide,
  // AbstractHyperSwapeeAide.class.prototype=/**@type {!HyperSwapeeAide}*/({
  // }),
 )
{}