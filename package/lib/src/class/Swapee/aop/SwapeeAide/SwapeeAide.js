import {reduceUsage} from '@artdeco/argufy'
import usually from 'usually'
import {argsConfig} from '../../../../bin/get-args'
import AbstractSwapeeAide from './gen/AbstractSwapeeAide'
// import _method from './methods/method'
// import {argsConfig} from '../get-args'

/** @extends {xyz.swapee.SwapeeAide} */
export default class extends AbstractSwapeeAide.implements(
 AbstractSwapeeAide.class.prototype=/**@type {!xyz.swapee.SwapeeAide} */({
  // constructor() {},
  ShowHelp(){
   const usage=usually({
    description:'The server for Swapee Crypto Aggregator.',
    line:'swapee_xyz [start|script <script_name>] [-o output] [-hv]',
    example:'swapee_xyz start',
    usage:reduceUsage(argsConfig),
   })
   console.log(usage)
  },
  ShowVersion() {
   const{asISwapeeAide:{version:version}}=this
   console.log('Version:',version)
  },
 }),
) {}