import '../../../../../types/types' /* A custom license. Please make sure you have the rights to use this software. */
import AbstractSwapeeAspects from '../../gen/aspects/AbstractSwapeeAspects'
import {relative} from 'path'

// const s=Symbol.for(`${3600135954244}::_output`)

/** @extends {xyz.swapee.SwapeeAspects} */
export default class extends AbstractSwapeeAspects.continues(
 AbstractSwapeeAspects.class.prototype=/**@type {!xyz.swapee.BSwapeeAspects<xyz.swapee.ISwapeeAide>}*/({
  // constructor:[
  //  function() {
  //   for(const a of arguments){
  //    if(!a) continue
  //    const{output:output}=a
  //    if(output) this[s]=output
  //   }
  //  },
  // ],
  // this is an aide.
  // afterAllocator:[
  // can this be decided by return-first?
  // allocator aspects are not event extended?
  beforeRun:[
   function(){if(this.debug)console.log('> Running aspects in',relative('',__dirname))},
   // function runShowHelp(){
   //  const{
   //   _showHelp:__showHelp,
   //  }=this
   //  if(!__showHelp) return
   //  const{
   //   asISwapeeAide:{
   //    showHelp:showHelp,
   //   },
   //  }=this
   //  showHelp() // or make optimisations.
   // },
  ],
 }),
 AbstractSwapeeAspects.class.prototype=/**@type {!xyz.swapee.SwapeeAspects}*/({
  afterConnectExchangesReturns:[ // bingo: this is a composition filter on the output.
   function({sub:sub,res:res}) {
    const hash={}
    const results=new Map
    for(const r of res.filter(Boolean)) {
     const k=Object.keys(r)[0]
     /** @type {!Map<Object,Object>} */
     const v=r[k]
     for(const vk of v.keys()) {
      hash[k]=vk
      results.set(vk,v.get(vk))
     }
    }
    // console.log({exchanges:hash,results:results})
    sub({exchanges:hash,results:results})
   },
  ],
  beforeConnectExchanges:[
   function({args:args,setArgs:setArgs}){
    if(!args.opts) {
     setArgs({
      opts:{},
     })
    }
   },
  ],
  before_ShowText:[[
   // ()=>console.log('before run'),
   // args have not been updated after set args
   ({args:{conf:conf},setArgs:setArgs})=>{
    // let c={text:''}
    const c={text:''}
    if(typeof conf=='object'){ // check for consturator
     // console.log(conf)
     for(const key in conf){
      const v=conf[key]
      if(v===undefined) {
       continue
      }
      c[key]=v
     }
     // setArgs({conf:c})
    }
    // set args does not spread to next aspects or what?
    setArgs({conf:c}) // -> todo: in protypes, setArgs needs to get updated
    // for the next aspect instead of having to call setArgs...
    // console.log(args.conf)
    // if(!conf) setArgs({conf:{}})
    // else setArgs({conf:{...conf}})
   },
   // what if typeof text is something else

   ({args:{conf:{text:text},conf:conf},setArgs:setArgs})=>
    // console.log(conf)
    text instanceof String
     ?setArgs({
      conf:{...conf,text:`${text}`},
     })
     :void 0,

   ({args:{conf:{text:text}},cancel})=>
   // conf:conf
   // setArgs:setArgs
    // console.log(conf)
    !text
     ?cancel('no text'):0,

   ({args:{conf:{text:text},conf:conf},setArgs:setArgs})=>
    // console.log(conf)
    !text
     ?setArgs({
      conf:{...conf,text:''},
     })
     :void 0,

   ({args:{conf:{text:text},conf:conf},setArgs:setArgs})=>
    typeof text!='string'
     ?setArgs({
      conf:{...conf,text:''},
     })
     :void 0,

   // ({args:{conf:{text:text},conf:conf},setArgs:setArgs})=>
   //  typeof text!='string'
   //   ?setArgs({
   //    conf:{...conf,text:''},
   //   })
   //   :void 0,
   // {
   //  // },
   //  // ({args,args:{conf:{text},conf}})=>{
   //  // console.log(conf)
   //  // if(!text) conf.text=''
   //  // console.log(conf)
   //  // console.log(args.conf)
   //  if(!text) {
   //   setArgs({
   //    conf:{
   //     ...conf,
   //     text:'',
   //    },
   //   })
   //  }
   // },
  ]],
  after_ShowTextReturns:[
   function({res,args:{conf:{output}}}){
    const{
     // asISwapee:{text},
    }=this
    if(output=='-'){
     console.log(res)
    }
   },
  ],
  afterRunCancels:[
   // ()=>console.log('after run'),
   function({args,args:{conf},setArgs}){
    // const{
    //  asISwapee:{version:version},
    //  [s]:output,
    // }=this
    // if(!conf.showVersion)return

    // if(output=='-'){

    // }else if(output) {
    //  // it might be a path or fd
    // }
    // if(typeof conf=='object'){
    //  setArgs({conf:{...conf}})
    // }
    // else setArgs({conf:{}})
   },
  ],
 }),
) {}