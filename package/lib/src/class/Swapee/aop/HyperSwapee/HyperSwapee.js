import {returnFirst} from '@type.engineering/type-engineer'
import AbstractHyperSwapee from '../../gen/hyper/AbstractHyperSwapee'
import Swapee from '../../Swapee'
import SwapeeAide from '../SwapeeAide'
import AbstractSwapeeAide from '../SwapeeAide/gen/AbstractSwapeeAide'
import SwapeeGeneralAspects from '../SwapeeGeneralAspects'

// const _showHelp=Symbol.for(`${3600135954244}::_showHelp`)
// const _showVersion=Symbol.for(`${3600135954244}::_showVersion`)

/** @extends {xyz.swapee.HyperSwapee} */
export default class extends AbstractHyperSwapee
 .consults(
  SwapeeGeneralAspects,
 )
 .implements(
  Swapee,
  SwapeeAide,
  // AbstractHyperSwapee.class.prototype=/**@type {!xyz.swapee.SwapeeBindingHyperslice<!xyz.swapee.ISwapeeAide>}*/({
  // }),
  // hyper classes need to accept binding hyperslices?
  // why hyperslice not extend binding hyperslice
  /**@type {!xyz.swapee.ISwapeeHyperslice}*/(AbstractSwapeeAide.class.prototype=/**@type {!xyz.swapee.SwapeeAide}*/({
   run(){
    const{
     asISwapeeAide:{showHelp:showHelp},
    }=this
    if(showHelp) returnFirst()
    else return
    const{
     asISwapeeAide:{
      ShowHelp:ShowHelp,
     },
    }=this

    ShowHelp() // or make optimisations.
   },
  })),
  /**@type {!xyz.swapee.ISwapeeHyperslice}*/(AbstractSwapeeAide.class.prototype=/**@type {!xyz.swapee.SwapeeAide}*/({
   async run(){
    const{
     asISwapeeAide:{testExchanges},
    }=this
    if(testExchanges) returnFirst()
    else return

    const{
     asISwapeeAide:{
      // inner api must be differnt from outer api.
      connectExchanges:connectExchanges,
     },
    }=this
    const{results,exchanges}=await connectExchanges() // or make optimisations.
    for(const key of results.keys()) {
     let _k
     for(const k in exchanges){
      const K=exchanges[k]
      if(K===key) {
       _k=k
       break
      }
     }
     console.log(_k,results.get(key))
    }
   },
  })),
  /**@type {!xyz.swapee.ISwapeeHyperslice}*/(AbstractSwapeeAide.class.prototype=/**@type {!xyz.swapee.SwapeeAide}*/({
   run(){
    const{
     asISwapeeAide:{showVersion},
    }=this
    if(showVersion) returnFirst()
    else return

    const{
     asISwapeeAide:{
      ShowVersion:ShowVersion,
     },
     // asISwapeeAide:asISwapeeAide,
    }=this
    // test({asISwapeeAide:asISwapeeAide,this:this})

    ShowVersion() // or make optimisations.
   },
  })),
  /**@type {!xyz.swapee.ISwapeeHyperslice}*/(AbstractSwapeeAide.class.prototype=/**@type {!xyz.swapee.SwapeeAide}*/({
   run(){
    const{
     asISwapee:{text:text}, // text is a command actually
    }=this
    if(text) returnFirst()
    else return

    const{
     asISwapee:{
      ShowText:ShowText,
     },
     asISwapeeAide:{output:output},
     // asISwapeeAide:asISwapeeAide,
    }=this
    // test({asISwapeeAide:asISwapeeAide,this:this})

    ShowText({ // show text can be combined also
     text:text,
     output:output,
    }) // or make optimisations.
   },
  })),
  /**@type {!xyz.swapee.ISwapeeHyperslice}*/(AbstractSwapeeAide.class.prototype=/**@type {!xyz.swapee.SwapeeAide}*/({
   run(){
    const{
     // asISwapeeAide:asISwapeeAide,
     asISwapeeAide:{ShowHelp:ShowHelp},
    }=this
    // test({asISwapeeAide:asISwapeeAide,this:this})
    ShowHelp()
   },
  })),
  // binding hyperslice bidning does not work for types.
  // /**@type {!xyz.swapee.ISwapeeHyperslice}*/(AbstractHyperSwapee.class.prototype=/**@type {!xyz.swapee.SwapeeBindingHyperslice<!xyz.swapee.SwapeeAide>}*/({
  //  run:[
  //   function() {
  //    const{
  //     asISwapeeAide:asISwapeeAide,
  //     asISwapeeAide:{ShowHelp:ShowHelp},
  //    }=this
  //    test({asISwapeeAide:asISwapeeAide,this:this})
  //    ShowHelp()
  //   },
  //  ],
  // })),
 )
{}

/**
 *
 * @param {string} e
 */
function test(e){
 console.log(e)
}

// run() {
//  //
//  const{
//   _showHelp:__showHelp,
//  }=this
//  if(!__showHelp) return
//  const{
//   asISwapeeAide:{
//    showHelp:showHelp,
//   },
//  }=this
//  showHelp() // or make optimisations.
// },
// AbstractHyperSwapee.class.prototype=/**@type
// {!xyz.swapee.SwapeeBindingHyperslice<!xyz.swapee.ISwapeeAide>}*/({
// constructor:[
//  function() {
//   // for(const a of arguments){
//   //  if(!a) continue
//   //  const{showHelp:showHelp}=a
//   //  if(showHelp) this[_showHelp]=showHelp
//   // }
//  },
// ],
//  // constructor:[
//  //  function() {
//  //   for(const a of arguments){
//  //    if(!a) continue
//  //    const{showVersion:showVersion}=a
//  //    if(showVersion) this[_showVersion]=showVersion
//  //   }
//  //  },
//  // ],
//  get _showVersion(){
//   return this[_showVersion]
//  },
// }),
// get _showVersion(){
//  return this[_showVersion]
// },
// get _showHelp(){
//  return this[_showHelp]
// },