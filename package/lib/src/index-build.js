/* A custom license. Please make sure you have the rights to use this software. */

import {c} from '@artdeco/erte'

/**
 * The package.
 * @param {!_swapee_xyz.Config} [config] Options for the program.
 * @param {boolean} [config.shouldRun=true] A boolean option. Default `true`.
 * @param {string} [config.text] A text to return.
 */
export default async function swapee_xyz(config = {}) {
 const {
  shouldRun = true,
  text = '',
 } = config
 if (!shouldRun) return ''
 console.log('@swapee.xyz/swapee_xyz called with %s', c(text, 'yellow'))
 return text
}

/* typal types/index.xml namespace */
/**
 * @typedef {_swapee_xyz.Config} Config `＠record` Options for the program.
 * @typedef {Object} _swapee_xyz.Config `＠record` Options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */
