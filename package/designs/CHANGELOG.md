## 22 March 2024

### [0.1.1](https://gitlab.com/artdeco/swapee.xyz/compare/v0.1.0...v0.1.1)

- [improvement] better error messages.

## 23 January 2024

### [0.1.0](https://gitlab.com/artdeco/swapee.xyz/compare/v0.0.0...v0.1.0)

- [package] publish the package with _ChangeNow_ and _LetsExchange_ apis.

## 8 January 2024

### 0.0.0

- [package] Create a new website with [Maurice](https://mauriceguest.com).