// import { writeFileSync } from 'fs'
// import {} from '@artdeco/crlf'

function CambridgeCase(NAME,includingFirst=true) {
 const cc=NAME.replace(/(^|-)(.)/g,(m,first,letter,pos)=>{
  if(includingFirst) {
   return letter.toUpperCase()
  }
  if(pos==0) return letter
  return letter.toUpperCase()
 })
 return cc
}

import {resolve} from 'path'

const records=[]
const [,,rootPath,Record]=process.argv
const ROOT=require(resolve(rootPath))

const make=(name,object)=>{
 let s=`  <record name="${name}">`
 for (const f in object) {
  const val = object[f]
  let Type='',nullable=false
  let type = 'prop'
  if (typeof val=='number')       type='number'
  else if (typeof val=='string')  {
   if(/\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d\.\d\d\dZ/.test(val)) {
    type='date'
   }else type='string'
  }
  else if (typeof val=='boolean') type='bool'
  else if(typeof val=='object') {
   if(val===null) {
    nullable=true
    // Type="?"
    // testing this
   }else{
    Type=CambridgeCase(f)
    make(Type,val)
   }
  }
  let example = val
  if(type == 'prop') {
   example=JSON.stringify(val)
  }
  s += `
    <${type} name="${f}"${nullable?` nullable`:''}${Type?' type="!.'+Type+'"':''}>
      Example: \`${example}\`.
    </${type}>`
 }
 s+=`
  </record>`
 records.push(s)
 return s
}

make(Record,ROOT)
console.log(records.join('\n'))
