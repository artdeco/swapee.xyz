/** @nocompile */
eval(`var io={}
io.changenow={}
io.changenow.AbstractChangeNow={}
io.changenow.AbstractChangeNowUniversal={}
io.changenow.UChangeNow={}
io.changenow.IChangeNow={}
io.letsexchange={}
io.letsexchange.AbstractLetsExchange={}
io.letsexchange.AbstractLetsExchangeUniversal={}
io.letsexchange.ULetsExchange={}
io.letsexchange.ILetsExchange={}
var xyz={}
xyz.swapee={}
xyz.swapee.AbstractSwapeeAide={}
xyz.swapee.ISwapeeAide={}
xyz.swapee.AbstractSwapee={}
xyz.swapee.ISwapee={}`)

/* @typal-start {types/design/IChangeNow.xml} filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {!io.changenow.IChangeNow.getSymbols} io.changenow.AbstractChangeNow.getSymbols Returns the protected symbols. */

/** @typedef {!io.changenow.IChangeNow.setSymbols} io.changenow.AbstractChangeNow.setSymbols Sets the protected symbols. */

/** @typedef {!io.changenow.IChangeNow.Symbols} io.changenow.AbstractChangeNow.Symbols The list of protected symbols used by the class. */

/** @typedef {!io.changenow.UChangeNow.getSymbols} io.changenow.AbstractChangeNowUniversal.getSymbols Returns the protected symbols. */

/** @typedef {!io.changenow.UChangeNow.setSymbols} io.changenow.AbstractChangeNowUniversal.setSymbols Sets the protected symbols. */

/** @typedef {!io.changenow.UChangeNow.Symbols} io.changenow.AbstractChangeNowUniversal.Symbols The list of protected symbols used by the class. */

/**
 * @typedef {Object} io.changenow.UChangeNow.SymbolsIn
 * @prop {io.changenow.IChangeNow} [_changeNow] The _ChangeNow_ client.
 */

/**
 * @typedef {Object} io.changenow.UChangeNow.Symbols
 * @prop {symbol} _changeNow
 */

/**
 * @typedef {Object} io.changenow.UChangeNow.SymbolsOut
 * @prop {io.changenow.IChangeNow} _changeNow The _ChangeNow_ client.
 */

/**
 * @typedef {Object} io.changenow.IChangeNow.SymbolsIn
 * @prop {string} [_host] The _ChangeNow_ instance host.
 * @prop {string} [_apiPath] The path after the host to use for request.
 * @prop {string} [_changeNowApiKey] The client id.
 */

/**
 * @typedef {Object} io.changenow.IChangeNow.Symbols
 * @prop {symbol} _host
 * @prop {symbol} _apiPath
 * @prop {symbol} _changeNowApiKey
 */

/**
 * @typedef {Object} io.changenow.IChangeNow.SymbolsOut
 * @prop {string} _host The _ChangeNow_ instance host.
 * @prop {string} _apiPath The path after the host to use for request.
 * @prop {string} _changeNowApiKey The client id.
 */

/** @typedef {typeof io.changenow.UChangeNow.setSymbols} */
/**
 * Sets the _UChangeNow_ symbols on the particular.
 * @param {!io.changenow.UChangeNow} particular The _UChangeNow_ particular on which to set the symbols.
 * @param {!io.changenow.UChangeNow.SymbolsIn} symbols The symbols to set on the particular.
 * - `[_changeNow]` _IChangeNow?_ The _ChangeNow_ client.
 * @return {void}
 */
io.changenow.UChangeNow.setSymbols = function(particular, symbols) {}

/** @typedef {typeof io.changenow.UChangeNow.getSymbols} */
/**
 * Gets the _UChangeNow_ symbols currently set on the particular.
 * @param {!io.changenow.UChangeNow} particular The _UChangeNow_ particular from which to read the symbols.
 * @return {!io.changenow.UChangeNow.SymbolsOut} The symbols currently set on the particular.
 */
io.changenow.UChangeNow.getSymbols = function(particular) {}

/** @typedef {typeof io.changenow.IChangeNow.setSymbols} */
/**
 * Sets the _IChangeNow_ symbols on the instance.
 * @param {!io.changenow.IChangeNow} instance The _IChangeNow_ instance on which to set the symbols.
 * @param {!io.changenow.IChangeNow.SymbolsIn} symbols The symbols to set on the instance.
 * - `[_host]` _string?_ The _ChangeNow_ instance host.
 * - `[_apiPath]` _string?_ The path after the host to use for request.
 * - `[_changeNowApiKey]` _string?_ The client id.
 * @return {void}
 */
io.changenow.IChangeNow.setSymbols = function(instance, symbols) {}

/** @typedef {typeof io.changenow.IChangeNow.getSymbols} */
/**
 * Gets the _IChangeNow_ symbols currently set on the instance.
 * @param {!io.changenow.IChangeNow} instance The _IChangeNow_ instance from which to read the symbols.
 * @return {!io.changenow.IChangeNow.SymbolsOut} The symbols currently set on the instance.
 */
io.changenow.IChangeNow.getSymbols = function(instance) {}

// nss:io.changenow.UChangeNow,io.changenow.IChangeNow
/* @typal-end */
/* @typal-start {types/design/ILetsExchange.xml} filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {!io.letsexchange.ILetsExchange.getSymbols} io.letsexchange.AbstractLetsExchange.getSymbols Returns the protected symbols. */

/** @typedef {!io.letsexchange.ILetsExchange.setSymbols} io.letsexchange.AbstractLetsExchange.setSymbols Sets the protected symbols. */

/** @typedef {!io.letsexchange.ILetsExchange.Symbols} io.letsexchange.AbstractLetsExchange.Symbols The list of protected symbols used by the class. */

/** @typedef {!io.letsexchange.ULetsExchange.getSymbols} io.letsexchange.AbstractLetsExchangeUniversal.getSymbols Returns the protected symbols. */

/** @typedef {!io.letsexchange.ULetsExchange.setSymbols} io.letsexchange.AbstractLetsExchangeUniversal.setSymbols Sets the protected symbols. */

/** @typedef {!io.letsexchange.ULetsExchange.Symbols} io.letsexchange.AbstractLetsExchangeUniversal.Symbols The list of protected symbols used by the class. */

/**
 * @typedef {Object} io.letsexchange.ULetsExchange.SymbolsIn
 * @prop {io.letsexchange.ILetsExchange} [_letsExchange] The _LetsExchange_ client.
 */

/**
 * @typedef {Object} io.letsexchange.ULetsExchange.Symbols
 * @prop {symbol} _letsExchange
 */

/**
 * @typedef {Object} io.letsexchange.ULetsExchange.SymbolsOut
 * @prop {io.letsexchange.ILetsExchange} _letsExchange The _LetsExchange_ client.
 */

/**
 * @typedef {Object} io.letsexchange.ILetsExchange.SymbolsIn
 * @prop {string} [_host] The _LetsExchange_ instance host.
 * @prop {string} [_apiPath] The path after the host to use for request.
 * @prop {string} [_letsExchangeApiKey] The client id.
 */

/**
 * @typedef {Object} io.letsexchange.ILetsExchange.Symbols
 * @prop {symbol} _host
 * @prop {symbol} _apiPath
 * @prop {symbol} _letsExchangeApiKey
 */

/**
 * @typedef {Object} io.letsexchange.ILetsExchange.SymbolsOut
 * @prop {string} _host The _LetsExchange_ instance host.
 * @prop {string} _apiPath The path after the host to use for request.
 * @prop {string} _letsExchangeApiKey The client id.
 */

/** @typedef {typeof io.letsexchange.ULetsExchange.setSymbols} */
/**
 * Sets the _ULetsExchange_ symbols on the particular.
 * @param {!io.letsexchange.ULetsExchange} particular The _ULetsExchange_ particular on which to set the symbols.
 * @param {!io.letsexchange.ULetsExchange.SymbolsIn} symbols The symbols to set on the particular.
 * - `[_letsExchange]` _ILetsExchange?_ The _LetsExchange_ client.
 * @return {void}
 */
io.letsexchange.ULetsExchange.setSymbols = function(particular, symbols) {}

/** @typedef {typeof io.letsexchange.ULetsExchange.getSymbols} */
/**
 * Gets the _ULetsExchange_ symbols currently set on the particular.
 * @param {!io.letsexchange.ULetsExchange} particular The _ULetsExchange_ particular from which to read the symbols.
 * @return {!io.letsexchange.ULetsExchange.SymbolsOut} The symbols currently set on the particular.
 */
io.letsexchange.ULetsExchange.getSymbols = function(particular) {}

/** @typedef {typeof io.letsexchange.ILetsExchange.setSymbols} */
/**
 * Sets the _ILetsExchange_ symbols on the instance.
 * @param {!io.letsexchange.ILetsExchange} instance The _ILetsExchange_ instance on which to set the symbols.
 * @param {!io.letsexchange.ILetsExchange.SymbolsIn} symbols The symbols to set on the instance.
 * - `[_host]` _string?_ The _LetsExchange_ instance host.
 * - `[_apiPath]` _string?_ The path after the host to use for request.
 * - `[_letsExchangeApiKey]` _string?_ The client id.
 * @return {void}
 */
io.letsexchange.ILetsExchange.setSymbols = function(instance, symbols) {}

/** @typedef {typeof io.letsexchange.ILetsExchange.getSymbols} */
/**
 * Gets the _ILetsExchange_ symbols currently set on the instance.
 * @param {!io.letsexchange.ILetsExchange} instance The _ILetsExchange_ instance from which to read the symbols.
 * @return {!io.letsexchange.ILetsExchange.SymbolsOut} The symbols currently set on the instance.
 */
io.letsexchange.ILetsExchange.getSymbols = function(instance) {}

// nss:io.letsexchange.ULetsExchange,io.letsexchange.ILetsExchange
/* @typal-end */
/* @typal-start {types/design/ISwapee.xml} filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {!xyz.swapee.ISwapeeAide.getSymbols} xyz.swapee.AbstractSwapeeAide.getSymbols Returns the protected symbols. */

/** @typedef {!xyz.swapee.ISwapeeAide.setSymbols} xyz.swapee.AbstractSwapeeAide.setSymbols Sets the protected symbols. */

/** @typedef {!xyz.swapee.ISwapeeAide.Symbols} xyz.swapee.AbstractSwapeeAide.Symbols The list of protected symbols used by the class. */

/**
 * @typedef {Object} xyz.swapee.ISwapeeAide.SymbolsIn
 * @prop {string} [_output]
 * @prop {boolean} [_showHelp]
 * @prop {boolean} [_testExchanges]
 * @prop {boolean} [_showVersion]
 * @prop {boolean} [_debug]
 * @prop {string} [_version] The version of the package with the bin.
 */

/**
 * @typedef {Object} xyz.swapee.ISwapeeAide.Symbols
 * @prop {symbol} _output
 * @prop {symbol} _showHelp
 * @prop {symbol} _testExchanges
 * @prop {symbol} _showVersion
 * @prop {symbol} _debug
 * @prop {symbol} _version
 */

/**
 * @typedef {Object} xyz.swapee.ISwapeeAide.SymbolsOut
 * @prop {string} _output
 * @prop {boolean} _showHelp
 * @prop {boolean} _testExchanges
 * @prop {boolean} _showVersion
 * @prop {boolean} _debug
 * @prop {string} _version The version of the package with the bin.
 */

/** @typedef {!xyz.swapee.ISwapee.getSymbols} xyz.swapee.AbstractSwapee.getSymbols Returns the protected symbols. */

/** @typedef {!xyz.swapee.ISwapee.setSymbols} xyz.swapee.AbstractSwapee.setSymbols Sets the protected symbols. */

/** @typedef {!xyz.swapee.ISwapee.Symbols} xyz.swapee.AbstractSwapee.Symbols The list of protected symbols used by the class. */

/**
 * @typedef {Object} xyz.swapee.ISwapee.SymbolsIn
 * @prop {string} [_text] The Text.
 */

/**
 * @typedef {Object} xyz.swapee.ISwapee.Symbols
 * @prop {symbol} _text
 */

/**
 * @typedef {Object} xyz.swapee.ISwapee.SymbolsOut
 * @prop {string} _text The Text.
 */

/** @typedef {typeof xyz.swapee.ISwapeeAide.setSymbols} */
/**
 * Sets the _ISwapeeAide_ symbols on the instance.
 * @param {!xyz.swapee.ISwapeeAide} instance The _ISwapeeAide_ instance on which to set the symbols.
 * @param {!xyz.swapee.ISwapeeAide.SymbolsIn} symbols The symbols to set on the instance.
 * - `[_output]` _string?_
 * - `[_showHelp]` _boolean?_
 * - `[_testExchanges]` _boolean?_
 * - `[_showVersion]` _boolean?_
 * - `[_debug]` _boolean?_
 * - `[_version]` _string?_ The version of the package with the bin.
 * @return {void}
 */
xyz.swapee.ISwapeeAide.setSymbols = function(instance, symbols) {}

/** @typedef {typeof xyz.swapee.ISwapeeAide.getSymbols} */
/**
 * Gets the _ISwapeeAide_ symbols currently set on the instance.
 * @param {!xyz.swapee.ISwapeeAide} instance The _ISwapeeAide_ instance from which to read the symbols.
 * @return {!xyz.swapee.ISwapeeAide.SymbolsOut} The symbols currently set on the instance.
 */
xyz.swapee.ISwapeeAide.getSymbols = function(instance) {}

/** @typedef {typeof xyz.swapee.ISwapee.setSymbols} */
/**
 * Sets the _ISwapee_ symbols on the instance.
 * @param {!xyz.swapee.ISwapee} instance The _ISwapee_ instance on which to set the symbols.
 * @param {!xyz.swapee.ISwapee.SymbolsIn} symbols The symbols to set on the instance.
 * - `[_text]` _string?_ The Text.
 * @return {void}
 */
xyz.swapee.ISwapee.setSymbols = function(instance, symbols) {}

/** @typedef {typeof xyz.swapee.ISwapee.getSymbols} */
/**
 * Gets the _ISwapee_ symbols currently set on the instance.
 * @param {!xyz.swapee.ISwapee} instance The _ISwapee_ instance from which to read the symbols.
 * @return {!xyz.swapee.ISwapee.SymbolsOut} The symbols currently set on the instance.
 */
xyz.swapee.ISwapee.getSymbols = function(instance) {}

// nss:xyz.swapee.ISwapeeAide,xyz.swapee.ISwapee
/* @typal-end */