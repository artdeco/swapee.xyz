/** @nocompile */
eval(`var xyz={}
xyz.swapee={}`)

/* @typal-start {types/api.xml} functions 8ebee15380ac6794a77d56f56e761637 */
/** @typedef {typeof xyz.swapee.swapee_xyz} */
/**
 * The package.
 * @param {!xyz.swapee.swapee_xyz.Config} config Additional options for the program.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise<string>} The result of processing.
 */
xyz.swapee.swapee_xyz = function(config) {}

// nss:xyz.swapee
/* @typal-end */