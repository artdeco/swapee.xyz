/** @type {typeof xyz} */
const _xyz={}
/** @type {typeof com} */
const _com={}
/** @type {typeof io} */
const _io={}

module.exports={
 xyz:_xyz,
 com:_com,
 io:_io,
}