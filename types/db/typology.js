/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModelHyperslice': {
  'id': 36001359541,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModelBindingHyperslice': {
  'id': 36001359542,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModel': {
  'id': 36001359543,
  'symbols': {},
  'methods': {
   'beforeGetRpc': 1,
   'afterGetRpc': 2,
   'afterGetRpcThrows': 3,
   'afterGetRpcReturns': 4,
   'afterGetRpcCancels': 5,
   'beforeEachGetRpc': 6,
   'afterEachGetRpc': 7,
   'afterEachGetRpcReturns': 8,
   'immediatelyAfterGetRpc': 9,
   'beforeQueryRpc': 10,
   'afterQueryRpc': 11,
   'afterQueryRpcThrows': 12,
   'afterQueryRpcReturns': 13,
   'afterQueryRpcCancels': 14,
   'beforePutRpc': 15,
   'afterPutRpc': 16,
   'afterPutRpcThrows': 17,
   'afterPutRpcReturns': 18,
   'afterPutRpcCancels': 19,
   'immediatelyAfterPutRpc': 20,
   'beforePostRpc': 21,
   'afterPostRpc': 22,
   'afterPostRpcThrows': 23,
   'afterPostRpcReturns': 24,
   'afterPostRpcCancels': 25,
   'immediatelyAfterPostRpc': 26
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayAspectsInstaller': {
  'id': 36001359544,
  'symbols': {},
  'methods': {
   'getRpc': 1,
   'queryRpc': 2,
   'putRpc': 3,
   'postRpc': 4
  }
 },
 'xyz.swapee.ws.rpc.BRpcGatewayAspects': {
  'id': 36001359545,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayForwardingAspects': {
  'id': 36001359546,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayAspects': {
  'id': 36001359547,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IHyperRpcGateway': {
  'id': 36001359548,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayHyperslice': {
  'id': 36001359549,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayBindingHyperslice': {
  'id': 360013595410,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGateway': {
  'id': 360013595411,
  'symbols': {},
  'methods': {
   'getRpc': 1,
   'queryRpc': 2,
   'putRpc': 3,
   'postRpc': 4
  }
 },
 'xyz.swapee.wc.ITestingComputer': {
  'id': 360013595412,
  'symbols': {},
  'methods': {
   'adaptChangellyFloatingOffer': 1,
   'adaptChangenowOffer': 2,
   'adaptAnyLoading': 3,
   'compute': 4,
   'adaptChangellyFixedOffer': 5
  }
 },
 'xyz.swapee.wc.TestingMemoryPQs': {
  'id': 360013595413,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingOuterCore': {
  'id': 360013595414,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TestingInputsPQs': {
  'id': 360013595415,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingPort': {
  'id': 360013595416,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTestingPort': 2
  }
 },
 'xyz.swapee.wc.TestingCachePQs': {
  'id': 360013595417,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITestingCore': {
  'id': 360013595418,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTestingCore': 2
  }
 },
 'xyz.swapee.wc.ITestingProcessor': {
  'id': 360013595419,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITesting': {
  'id': 360013595420,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingBuffer': {
  'id': 360013595421,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingHtmlComponent': {
  'id': 360013595422,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingElement': {
  'id': 360013595423,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ITestingElementPort': {
  'id': 360013595424,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingRadio': {
  'id': 360013595425,
  'symbols': {},
  'methods': {
   'adaptLoadChangellyFloatingOffer': 1,
   'adaptLoadChangenowOffer': 2,
   'adaptLoadChangellyFixedOffer': 3
  }
 },
 'xyz.swapee.wc.ITestingDesigner': {
  'id': 360013595426,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITestingService': {
  'id': 360013595427,
  'symbols': {},
  'methods': {
   'filterChangellyFloatingOffer': 1,
   'filterChangenowOffer': 2,
   'filterChangellyFixedOffer': 3
  }
 },
 'xyz.swapee.wc.ITestingGPU': {
  'id': 360013595428,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingDisplay': {
  'id': 360013595429,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TestingVdusPQs': {
  'id': 360013595430,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITestingDisplay': {
  'id': 360013595431,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ITestingController': {
  'id': 360013595432,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'increaseAmount': 2,
   'decreaseAmount': 3,
   'setCore': 4,
   'unsetCore': 5,
   'setAmountIn': 6,
   'unsetAmountIn': 7,
   'setCurrencyIn': 8,
   'unsetCurrencyIn': 9,
   'setCurrencyOut': 10,
   'unsetCurrencyOut': 11,
   'loadChangellyFloatingOffer': 12,
   'loadChangenowOffer': 13,
   'loadChangellyFixedOffer': 14
  }
 },
 'xyz.swapee.wc.front.ITestingController': {
  'id': 360013595433,
  'symbols': {},
  'methods': {
   'increaseAmount': 1,
   'decreaseAmount': 2,
   'setCore': 3,
   'unsetCore': 4,
   'setAmountIn': 5,
   'unsetAmountIn': 6,
   'setCurrencyIn': 7,
   'unsetCurrencyIn': 8,
   'setCurrencyOut': 9,
   'unsetCurrencyOut': 10,
   'loadChangellyFloatingOffer': 11,
   'loadChangenowOffer': 12,
   'loadChangellyFixedOffer': 13
  }
 },
 'xyz.swapee.wc.back.ITestingController': {
  'id': 360013595434,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingControllerAR': {
  'id': 360013595435,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITestingControllerAT': {
  'id': 360013595436,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITestingScreen': {
  'id': 360013595437,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingScreen': {
  'id': 360013595438,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITestingScreenAR': {
  'id': 360013595439,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITestingScreenAT': {
  'id': 360013595440,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterComputer': {
  'id': 360013595441,
  'symbols': {},
  'methods': {
   'adaptNextPhrase': 1,
   'compute': 2
  }
 },
 'xyz.swapee.wc.TypeWriterMemoryPQs': {
  'id': 360013595442,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITypeWriterOuterCore': {
  'id': 360013595443,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TypeWriterInputsPQs': {
  'id': 360013595444,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITypeWriterPort': {
  'id': 360013595445,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTypeWriterPort': 2
  }
 },
 'xyz.swapee.wc.ITypeWriterCore': {
  'id': 360013595446,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTypeWriterCore': 2
  }
 },
 'xyz.swapee.wc.ITypeWriterProcessor': {
  'id': 360013595447,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriter': {
  'id': 360013595448,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterBuffer': {
  'id': 360013595449,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterHtmlComponent': {
  'id': 360013595450,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterElement': {
  'id': 360013595451,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ITypeWriterElementPort': {
  'id': 360013595452,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterDesigner': {
  'id': 360013595453,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITypeWriterGPU': {
  'id': 360013595454,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterDisplay': {
  'id': 360013595455,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TypeWriterVdusPQs': {
  'id': 360013595456,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITypeWriterDisplay': {
  'id': 360013595457,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ITypeWriterController': {
  'id': 360013595458,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'next': 2
  }
 },
 'xyz.swapee.wc.front.ITypeWriterController': {
  'id': 360013595459,
  'symbols': {},
  'methods': {
   'next': 1
  }
 },
 'xyz.swapee.wc.back.ITypeWriterController': {
  'id': 360013595460,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITypeWriterControllerAR': {
  'id': 360013595461,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITypeWriterControllerAT': {
  'id': 360013595462,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITypeWriterScreen': {
  'id': 360013595463,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITypeWriterScreen': {
  'id': 360013595464,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITypeWriterScreenAR': {
  'id': 360013595465,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITypeWriterScreenAT': {
  'id': 360013595466,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableComputer': {
  'id': 360013595467,
  'symbols': {},
  'methods': {
   'compute': 2,
   'adaptChangellyFixedLink': 3,
   'adaptChangellyFloatLink': 4
  }
 },
 'xyz.swapee.wc.OffersTableMemoryPQs': {
  'id': 360013595468,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTableOuterCore': {
  'id': 360013595469,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersTableInputsPQs': {
  'id': 360013595470,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTablePort': {
  'id': 360013595471,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersTablePort': 2
  }
 },
 'xyz.swapee.wc.IOffersTableCore': {
  'id': 360013595472,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersTableCore': 2
  }
 },
 'xyz.swapee.wc.IOffersTableProcessor': {
  'id': 360013595473,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTable': {
  'id': 360013595474,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableBuffer': {
  'id': 360013595475,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableHtmlComponentUtil': {
  'id': 360013595476,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IOffersTableHtmlComponent': {
  'id': 360013595477,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableElement': {
  'id': 360013595478,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildProgressCollapsar': 5,
   'short': 6,
   'server': 7,
   'inducer': 8,
   'buildOffersAggregator': 9,
   'buildCryptoSelectOut': 11,
   'buildExchangeIntent': 13
  }
 },
 'xyz.swapee.wc.IOffersTableElementPort': {
  'id': 360013595479,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableDesigner': {
  'id': 360013595480,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersTableGPU': {
  'id': 360013595481,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableDisplay': {
  'id': 360013595482,
  'symbols': {},
  'methods': {
   'paint': 1,
   'paintTableOrder': 2
  }
 },
 'xyz.swapee.wc.OffersTableQueriesPQs': {
  'id': 360013595483,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.OffersTableVdusPQs': {
  'id': 360013595484,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersTableDisplay': {
  'id': 360013595485,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersTableClassesPQs': {
  'id': 360013595486,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTableControllerHyperslice': {
  'id': 360013595487,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableControllerBindingHyperslice': {
  'id': 360013595488,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableController': {
  'id': 360013595489,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTick': 2,
   'reset': 3,
   'onReset': 4
  }
 },
 'xyz.swapee.wc.front.IOffersTableControllerHyperslice': {
  'id': 360013595490,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableControllerBindingHyperslice': {
  'id': 360013595491,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableController': {
  'id': 360013595492,
  'symbols': {},
  'methods': {
   'resetTick': 1,
   'reset': 2,
   'onReset': 3
  }
 },
 'xyz.swapee.wc.back.IOffersTableController': {
  'id': 360013595493,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableControllerAR': {
  'id': 360013595494,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableControllerAT': {
  'id': 360013595495,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableScreen': {
  'id': 360013595496,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableScreen': {
  'id': 360013595497,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableScreenAR': {
  'id': 360013595498,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableScreenAT': {
  'id': 360013595499,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectComputer': {
  'id': 3600135954100,
  'symbols': {},
  'methods': {
   'adaptWideness': 1,
   'adaptSelectedCrypto': 2,
   'adaptSearchInput': 3,
   'adaptPopupShown': 4,
   'adaptMatchedKeys': 5,
   'compute': 8
  }
 },
 'xyz.swapee.wc.CryptoSelectMemoryPQs': {
  'id': 3600135954101,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectOuterCore': {
  'id': 3600135954102,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.CryptoSelectInputsPQs': {
  'id': 3600135954103,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectPort': {
  'id': 3600135954104,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetCryptoSelectPort': 2
  }
 },
 'xyz.swapee.wc.CryptoSelectCachePQs': {
  'id': 3600135954105,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectCore': {
  'id': 3600135954106,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetCryptoSelectCore': 2
  }
 },
 'xyz.swapee.wc.ICryptoSelectProcessor': {
  'id': 3600135954107,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelect': {
  'id': 3600135954108,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectBuffer': {
  'id': 3600135954109,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectHtmlComponentUtil': {
  'id': 3600135954110,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectHtmlComponent': {
  'id': 3600135954111,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectElement': {
  'id': 3600135954112,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildPopup': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ICryptoSelectElementPort': {
  'id': 3600135954113,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectDesigner': {
  'id': 3600135954114,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ICryptoSelectGPU': {
  'id': 3600135954115,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectDisplay': {
  'id': 3600135954116,
  'symbols': {},
  'methods': {
   'resolveMouseItem': 1,
   'resolveItem': 2,
   'resolveItemByKey': 3,
   'resolveItemIndex': 4,
   'scrollItemIntoView': 5,
   'paint': 6,
   'paintSmHeight': 7,
   'paintSearch': 8,
   'paintSelectedImg': 9,
   'paintHoveringIndex': 10,
   'paintBeforeHovering': 11,
   'paintSearchInput': 12
  }
 },
 'xyz.swapee.wc.CryptoSelectVdusPQs': {
  'id': 3600135954117,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectDisplay': {
  'id': 3600135954118,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.CryptoSelectClassesPQs': {
  'id': 3600135954119,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectController': {
  'id': 3600135954120,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'flipMenuExpanded': 2,
   'calibrateDataSource': 3,
   'calibrateResetMouseOver': 4,
   'setSelected': 5,
   'unsetSelected': 6
  }
 },
 'xyz.swapee.wc.front.ICryptoSelectController': {
  'id': 3600135954121,
  'symbols': {},
  'methods': {
   'flipMenuExpanded': 1,
   'setSelected': 2,
   'unsetSelected': 3
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectController': {
  'id': 3600135954122,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ICryptoSelectControllerAR': {
  'id': 3600135954123,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ICryptoSelectControllerAT': {
  'id': 3600135954124,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectTouchscreen': {
  'id': 3600135954125,
  'symbols': {},
  'methods': {
   'stashSelectedInLocalStorage': 1,
   'stashIsMenuExpanded': 2,
   'stashKeyboardItemAfterMenuExpanded': 3,
   'stashVisibleItems': 4
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectTouchscreen': {
  'id': 3600135954126,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ICryptoSelectTouchscreenAR': {
  'id': 3600135954127,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ICryptoSelectTouchscreenAT': {
  'id': 3600135954128,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersTableTouchscreen': {
  'id': 3600135954129,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableTouchscreen': {
  'id': 3600135954130,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersTableTouchscreenAR': {
  'id': 3600135954131,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersTableTouchscreenAT': {
  'id': 3600135954132,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorComputer': {
  'id': 3600135954133,
  'symbols': {},
  'methods': {
   'adaptChangellyFloatingOffer': 1,
   'adaptChangellyFixedOffer': 2,
   'adaptEstimatedOut': 4,
   'adaptBestAndWorstOffers': 5,
   'adaptExchangesLoaded': 6,
   'adaptIsAggregating': 7,
   'compute': 8,
   'adaptLetsExchangeFloatingOffer': 9,
   'adaptLetsExchangeFixedOffer': 10,
   'adaptChangeNowFloatingOffer': 11,
   'adaptChangeNowFixedOffer': 12,
   'adaptMinAmount': 13,
   'adaptMinError': 14,
   'adaptMaxAmount': 15,
   'adaptMaxError': 16,
   'adaptLoadingEstimate': 17
  }
 },
 'xyz.swapee.wc.OffersAggregatorMemoryPQs': {
  'id': 3600135954134,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorOuterCore': {
  'id': 3600135954135,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersAggregatorInputsPQs': {
  'id': 3600135954136,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorPort': {
  'id': 3600135954137,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersAggregatorPort': 2
  }
 },
 'xyz.swapee.wc.OffersAggregatorCachePQs': {
  'id': 3600135954138,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorCore': {
  'id': 3600135954139,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersAggregatorCore': 2
  }
 },
 'xyz.swapee.wc.IOffersAggregatorProcessor': {
  'id': 3600135954140,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregator': {
  'id': 3600135954141,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorBuffer': {
  'id': 3600135954142,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorHtmlComponent': {
  'id': 3600135954143,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorElement': {
  'id': 3600135954144,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4,
   'build': 5,
   'buildExchangeIntent': 6,
   'short': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorElementPort': {
  'id': 3600135954145,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorRadio': {
  'id': 3600135954146,
  'symbols': {},
  'methods': {
   'adaptLoadChangellyFloatingOffer': 1,
   'adaptLoadChangellyFixedOffer': 2,
   'adaptLoadLetsExchangeFloatingOffer': 4,
   'adaptLoadLetsExchangeFixedOffer': 5,
   'adaptLoadChangeNowFloatingOffer': 6,
   'adaptLoadChangeNowFixedOffer': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorDesigner': {
  'id': 3600135954147,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersAggregatorService': {
  'id': 3600135954148,
  'symbols': {},
  'methods': {
   'filterChangellyFloatingOffer': 1,
   'filterChangellyFixedOffer': 2,
   'filterLetsExchangeFloatingOffer': 4,
   'filterLetsExchangeFixedOffer': 5,
   'filterChangeNowFloatingOffer': 6,
   'filterChangeNowFixedOffer': 7
  }
 },
 'xyz.swapee.wc.IOffersAggregatorGPU': {
  'id': 3600135954149,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorDisplay': {
  'id': 3600135954150,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersAggregatorVdusPQs': {
  'id': 3600135954151,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersAggregatorDisplay': {
  'id': 3600135954152,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorController': {
  'id': 3600135954153,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setChangellyFixedOffer': 2,
   'unsetChangellyFixedOffer': 3,
   'setChangellyFixedError': 4,
   'unsetChangellyFixedError': 5,
   'setChangellyFloatingError': 6,
   'unsetChangellyFloatingError': 7,
   'setChangellyFloatingOffer': 8,
   'unsetChangellyFloatingOffer': 9,
   'loadChangellyFloatingOffer': 16,
   'loadChangellyFixedOffer': 17,
   'setLetsExchangeFixedOffer': 19,
   'unsetLetsExchangeFixedOffer': 20,
   'setLetsExchangeFixedError': 21,
   'unsetLetsExchangeFixedError': 22,
   'setLetsExchangeFloatingOffer': 23,
   'unsetLetsExchangeFloatingOffer': 24,
   'setLetsExchangeFloatingError': 25,
   'unsetLetsExchangeFloatingError': 26,
   'loadLetsExchangeFloatingOffer': 27,
   'loadLetsExchangeFixedOffer': 28,
   'setChangeNowFixedOffer': 29,
   'unsetChangeNowFixedOffer': 30,
   'setChangeNowFixedError': 31,
   'unsetChangeNowFixedError': 32,
   'setChangeNowFloatingOffer': 33,
   'unsetChangeNowFloatingOffer': 34,
   'setChangeNowFloatingError': 35,
   'unsetChangeNowFloatingError': 36,
   'loadChangeNowFloatingOffer': 37,
   'loadChangeNowFixedOffer': 38,
   'setChangellyFixedMin': 39,
   'unsetChangellyFixedMin': 40,
   'setChangellyFixedMax': 41,
   'unsetChangellyFixedMax': 42,
   'setChangellyFloatMin': 43,
   'unsetChangellyFloatMin': 44,
   'setChangellyFloatMax': 45,
   'unsetChangellyFloatMax': 46
  }
 },
 'xyz.swapee.wc.front.IOffersAggregatorController': {
  'id': 3600135954154,
  'symbols': {},
  'methods': {
   'setChangellyFixedOffer': 1,
   'unsetChangellyFixedOffer': 2,
   'setChangellyFixedError': 3,
   'unsetChangellyFixedError': 4,
   'setChangellyFloatingError': 5,
   'unsetChangellyFloatingError': 6,
   'setChangellyFloatingOffer': 7,
   'unsetChangellyFloatingOffer': 8,
   'loadChangellyFloatingOffer': 15,
   'loadChangellyFixedOffer': 16,
   'setLetsExchangeFixedOffer': 18,
   'unsetLetsExchangeFixedOffer': 19,
   'setLetsExchangeFixedError': 20,
   'unsetLetsExchangeFixedError': 21,
   'setLetsExchangeFloatingOffer': 22,
   'unsetLetsExchangeFloatingOffer': 23,
   'setLetsExchangeFloatingError': 24,
   'unsetLetsExchangeFloatingError': 25,
   'loadLetsExchangeFloatingOffer': 26,
   'loadLetsExchangeFixedOffer': 27,
   'setChangeNowFixedOffer': 28,
   'unsetChangeNowFixedOffer': 29,
   'setChangeNowFixedError': 30,
   'unsetChangeNowFixedError': 31,
   'setChangeNowFloatingOffer': 32,
   'unsetChangeNowFloatingOffer': 33,
   'setChangeNowFloatingError': 34,
   'unsetChangeNowFloatingError': 35,
   'loadChangeNowFloatingOffer': 36,
   'loadChangeNowFixedOffer': 37,
   'setChangellyFixedMin': 38,
   'unsetChangellyFixedMin': 39,
   'setChangellyFixedMax': 40,
   'unsetChangellyFixedMax': 41,
   'setChangellyFloatMin': 42,
   'unsetChangellyFloatMin': 43,
   'setChangellyFloatMax': 44,
   'unsetChangellyFloatMax': 45
  }
 },
 'xyz.swapee.wc.back.IOffersAggregatorController': {
  'id': 3600135954155,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorControllerAR': {
  'id': 3600135954156,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersAggregatorControllerAT': {
  'id': 3600135954157,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorScreen': {
  'id': 3600135954158,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorScreen': {
  'id': 3600135954159,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersAggregatorScreenAR': {
  'id': 3600135954160,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersAggregatorScreenAT': {
  'id': 3600135954161,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ILetsExchangeJoinpointModel': {
  'id': 3600135954162,
  'symbols': {},
  'methods': {
   'before_GetInfo': 1,
   'after_GetInfo': 2,
   'after_GetInfoThrows': 3,
   'after_GetInfoReturns': 4,
   'after_GetInfoCancels': 5,
   'immediatelyAfter_GetInfo': 6
  }
 },
 'io.letsexchange.ILetsExchangeAspectsInstaller': {
  'id': 3600135954163,
  'symbols': {},
  'methods': {
   'GetInfo': 1
  }
 },
 'io.letsexchange.BLetsExchangeAspects': {
  'id': 3600135954164,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'io.letsexchange.ILetsExchangeAspects': {
  'id': 3600135954165,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.IHyperLetsExchange': {
  'id': 3600135954166,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ILetsExchange': {
  'id': 3600135954167,
  'symbols': {
   'host': 1,
   'apiPath': 2,
   'letsExchangeApiKey': 3
  },
  'methods': {
   'GetInfo': 1
  }
 },
 'io.letsexchange.ILetsExchangeJoinpointModelHyperslice': {
  'id': 3600135954168,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ILetsExchangeJoinpointModelBindingHyperslice': {
  'id': 3600135954169,
  'symbols': {},
  'methods': {}
 },
 'io.letsexchange.ULetsExchange': {
  'id': 3600135954170,
  'symbols': {
   'letsExchange': 1
  },
  'methods': {}
 },
 'io.changenow.IChangeNowJoinpointModelHyperslice': {
  'id': 3600135954171,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IChangeNowJoinpointModelBindingHyperslice': {
  'id': 3600135954172,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IChangeNowJoinpointModel': {
  'id': 3600135954173,
  'symbols': {},
  'methods': {
   'before_GetExchangeAmount': 1,
   'after_GetExchangeAmount': 2,
   'after_GetExchangeAmountThrows': 3,
   'after_GetExchangeAmountReturns': 4,
   'after_GetExchangeAmountCancels': 5,
   'immediatelyAfter_GetExchangeAmount': 6,
   'before_GetFixedExchangeAmount': 7,
   'after_GetFixedExchangeAmount': 8,
   'after_GetFixedExchangeAmountThrows': 9,
   'after_GetFixedExchangeAmountReturns': 10,
   'after_GetFixedExchangeAmountCancels': 11,
   'immediatelyAfter_GetFixedExchangeAmount': 12
  }
 },
 'io.changenow.IChangeNowAspectsInstaller': {
  'id': 3600135954174,
  'symbols': {},
  'methods': {
   'GetExchangeAmount': 1,
   'GetFixedExchangeAmount': 2
  }
 },
 'io.changenow.UChangeNow': {
  'id': 3600135954175,
  'symbols': {
   'changeNow': 1
  },
  'methods': {}
 },
 'io.changenow.BChangeNowAspects': {
  'id': 3600135954176,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'io.changenow.IChangeNowAspects': {
  'id': 3600135954177,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IHyperChangeNow': {
  'id': 3600135954178,
  'symbols': {},
  'methods': {}
 },
 'io.changenow.IChangeNow': {
  'id': 3600135954179,
  'symbols': {
   'host': 1,
   'apiPath': 2,
   'changeNowApiKey': 3
  },
  'methods': {
   'GetExchangeAmount': 4,
   'GetFixedExchangeAmount': 5
  }
 },
 'xyz.swapee.wc.IChangellyExchangeComputer': {
  'id': 3600135954180,
  'symbols': {},
  'methods': {
   'adaptCreateTransaction': 1,
   'compute': 2,
   'adaptCheckPayment': 3,
   'adaptRegion': 4,
   'adaptGetFixedOffer': 5,
   'adaptGetOffer': 6,
   'adaptGetTransaction': 7,
   'adaptCreateFixedTransaction': 8,
   'adaptGettingOffer': 9,
   'adaptGetOfferError': 10,
   'adaptCreatingTransaction': 11,
   'adaptCreateTransactionError': 12
  }
 },
 'xyz.swapee.wc.ChangellyExchangeMemoryPQs': {
  'id': 3600135954181,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeOuterCore': {
  'id': 3600135954182,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ChangellyExchangeInputsPQs': {
  'id': 3600135954183,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangePort': {
  'id': 3600135954184,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetChangellyExchangePort': 2
  }
 },
 'xyz.swapee.wc.ChangellyExchangeCachePQs': {
  'id': 3600135954185,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeCore': {
  'id': 3600135954186,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetChangellyExchangeCore': 2
  }
 },
 'xyz.swapee.wc.IChangellyExchangeProcessor': {
  'id': 3600135954187,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchange': {
  'id': 3600135954188,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeBuffer': {
  'id': 3600135954189,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeHtmlComponent': {
  'id': 3600135954190,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeElement': {
  'id': 3600135954191,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4,
   'build': 5,
   'short': 9,
   'buildExchangeBroker': 10,
   'buildRegionSelector': 11,
   'buildDealBroker': 12,
   'buildExchangeIntent': 13,
   'buildTransactionInfo': 14
  }
 },
 'xyz.swapee.wc.IChangellyExchangeElementPort': {
  'id': 3600135954192,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeRadio': {
  'id': 3600135954193,
  'symbols': {},
  'methods': {
   'adaptLoadCreateTransaction': 1,
   'adaptLoadCheckPayment': 2,
   'adaptLoadGetFixedOffer': 3,
   'adaptLoadGetOffer': 4,
   'adaptLoadGetTransaction': 5,
   'adaptLoadCreateFixedTransaction': 6
  }
 },
 'xyz.swapee.wc.IChangellyExchangeDesigner': {
  'id': 3600135954194,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IChangellyExchangeService': {
  'id': 3600135954195,
  'symbols': {},
  'methods': {
   'filterCreateTransaction': 1,
   'filterCheckPayment': 2,
   'filterGetFixedOffer': 3,
   'filterGetOffer': 4,
   'filterGetTransaction': 5,
   'filterCreateFixedTransaction': 6
  }
 },
 'xyz.swapee.wc.IChangellyExchangeGPU': {
  'id': 3600135954196,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeDisplay': {
  'id': 3600135954197,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ChangellyExchangeVdusPQs': {
  'id': 3600135954198,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IChangellyExchangeDisplay': {
  'id': 3600135954199,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ChangellyExchangeClassesPQs': {
  'id': 3600135954200,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeController': {
  'id': 3600135954201,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'loadCreateTransaction': 2,
   'loadCheckPayment': 16,
   'loadGetFixedOffer': 17,
   'loadGetOffer': 18,
   'loadGetTransaction': 19,
   'loadCreateFixedTransaction': 20
  }
 },
 'xyz.swapee.wc.front.IChangellyExchangeController': {
  'id': 3600135954202,
  'symbols': {},
  'methods': {
   'loadCreateTransaction': 1,
   'loadCheckPayment': 15,
   'loadGetFixedOffer': 16,
   'loadGetOffer': 17,
   'loadGetTransaction': 18,
   'loadCreateFixedTransaction': 19
  }
 },
 'xyz.swapee.wc.back.IChangellyExchangeController': {
  'id': 3600135954203,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeControllerAR': {
  'id': 3600135954204,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IChangellyExchangeControllerAT': {
  'id': 3600135954205,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeTouchscreen': {
  'id': 3600135954206,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeTouchscreen': {
  'id': 3600135954207,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IChangellyExchangeTouchscreenAR': {
  'id': 3600135954208,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeTouchscreenAT': {
  'id': 3600135954209,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnComputer': {
  'id': 3600135954210,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptTransactionStatus': 2,
   'adaptProcessingTransaction': 3
  }
 },
 'xyz.swapee.wc.ProgressColumnMemoryPQs': {
  'id': 3600135954211,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IProgressColumnOuterCore': {
  'id': 3600135954212,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ProgressColumnInputsPQs': {
  'id': 3600135954213,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IProgressColumnPort': {
  'id': 3600135954214,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetProgressColumnPort': 2
  }
 },
 'xyz.swapee.wc.IProgressColumnCore': {
  'id': 3600135954215,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetProgressColumnCore': 2
  }
 },
 'xyz.swapee.wc.IProgressColumnProcessor': {
  'id': 3600135954216,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumn': {
  'id': 3600135954217,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnBuffer': {
  'id': 3600135954218,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnHtmlComponent': {
  'id': 3600135954219,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnElement': {
  'id': 3600135954220,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IProgressColumnElementPort': {
  'id': 3600135954221,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnDesigner': {
  'id': 3600135954222,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IProgressColumnGPU': {
  'id': 3600135954223,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnDisplay': {
  'id': 3600135954224,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ProgressColumnVdusPQs': {
  'id': 3600135954225,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IProgressColumnDisplay': {
  'id': 3600135954226,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ProgressColumnClassesPQs': {
  'id': 3600135954227,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IProgressColumnController': {
  'id': 3600135954228,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IProgressColumnController': {
  'id': 3600135954229,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnController': {
  'id': 3600135954230,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnControllerAR': {
  'id': 3600135954231,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IProgressColumnControllerAT': {
  'id': 3600135954232,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnScreen': {
  'id': 3600135954233,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnScreen': {
  'id': 3600135954234,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IProgressColumnScreenAR': {
  'id': 3600135954235,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IProgressColumnScreenAT': {
  'id': 3600135954236,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil': {
  'id': 3600135954237,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.ISwapee': {
  'id': 3600135954238,
  'symbols': {
   'text': 3
  },
  'methods': {
   'run': 2,
   'ShowText': 6,
   'connectExchanges': 7
  }
 },
 'xyz.swapee.ISwapeeJoinpointModelHyperslice': {
  'id': 3600135954239,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeJoinpointModelBindingHyperslice': {
  'id': 3600135954240,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeJoinpointModel': {
  'id': 3600135954241,
  'symbols': {},
  'methods': {
   'beforeRun': 1,
   'afterRun': 2,
   'afterRunThrows': 3,
   'afterRunReturns': 4,
   'afterRunCancels': 5,
   'immediatelyAfterRun': 6,
   'beforeEachRun': 13,
   'afterEachRun': 14,
   'afterEachRunReturns': 15,
   'before_ShowText': 28,
   'after_ShowText': 29,
   'after_ShowTextThrows': 30,
   'after_ShowTextReturns': 31,
   'after_ShowTextCancels': 32,
   'immediatelyAfter_ShowText': 33,
   'beforeConnectExchanges': 34,
   'afterConnectExchanges': 35,
   'afterConnectExchangesThrows': 36,
   'afterConnectExchangesReturns': 37,
   'afterConnectExchangesCancels': 38,
   'immediatelyAfterConnectExchanges': 39,
   'beforeEachConnectExchanges': 40,
   'afterEachConnectExchanges': 41,
   'afterEachConnectExchangesReturns': 42
  }
 },
 'xyz.swapee.ISwapeeAspectsInstaller': {
  'id': 3600135954242,
  'symbols': {},
  'methods': {
   'run': 1,
   'ShowText': 5,
   'connectExchanges': 6
  }
 },
 'xyz.swapee.BSwapeeAspects': {
  'id': 3600135954243,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.ISwapeeAspects': {
  'id': 3600135954244,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.IHyperSwapee': {
  'id': 3600135954245,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeAide': {
  'id': 3600135954246,
  'symbols': {
   'output': 1,
   'showHelp': 2,
   'showVersion': 3,
   'version': 4,
   'debug': 5,
   'testExchanges': 6
  },
  'methods': {
   'ShowHelp': 3,
   'ShowVersion': 4
  }
 },
 'xyz.swapee.ISwapeeHyperslice': {
  'id': 3600135954247,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeBindingHyperslice': {
  'id': 3600135954248,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageComputer': {
  'id': 3600135954249,
  'symbols': {},
  'methods': {
   'adaptClearError': 1,
   'adaptCredentialsError': 2,
   'adaptLogin': 3,
   'compute': 4
  }
 },
 'xyz.swapee.wc.SwapeeLoginPageMemoryPQs': {
  'id': 3600135954250,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPageOuterCore': {
  'id': 3600135954251,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeLoginPageInputsPQs': {
  'id': 3600135954252,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPagePort': {
  'id': 3600135954253,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeLoginPagePort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPagePortInterface': {
  'id': 3600135954254,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice': {
  'id': 3600135954255,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice': {
  'id': 3600135954256,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageCPU': {
  'id': 3600135954257,
  'symbols': {},
  'methods': {
   'resetCore': 1
  }
 },
 'xyz.swapee.wc.SwapeeLoginPageCachePQs': {
  'id': 3600135954258,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPageCore': {
  'id': 3600135954259,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeLoginPageCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPageProcessor': {
  'id': 3600135954260,
  'symbols': {},
  'methods': {
   'pulseSignIn': 1,
   'pulseSignedIn': 2
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPage': {
  'id': 3600135954261,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageBuffer': {
  'id': 3600135954262,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil': {
  'id': 3600135954263,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPageHtmlComponent': {
  'id': 3600135954264,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageElement': {
  'id': 3600135954265,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildSwapeeMe': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPageElementPort': {
  'id': 3600135954266,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageDesigner': {
  'id': 3600135954267,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPageGPU': {
  'id': 3600135954268,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageDisplay': {
  'id': 3600135954269,
  'symbols': {},
  'methods': {
   'paint': 1,
   'paintCallback': 2
  }
 },
 'xyz.swapee.wc.SwapeeLoginPageQueriesPQs': {
  'id': 3600135954270,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.SwapeeLoginPageVdusPQs': {
  'id': 3600135954271,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeLoginPageDisplay': {
  'id': 3600135954272,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeLoginPageClassesPQs': {
  'id': 3600135954273,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeLoginPageController': {
  'id': 3600135954274,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setUsername': 2,
   'unsetUsername': 3,
   'setPassword': 4,
   'unsetPassword': 5,
   'pulseSignIn': 6,
   'pulseSignedIn': 7
  }
 },
 'xyz.swapee.wc.front.ISwapeeLoginPageController': {
  'id': 3600135954275,
  'symbols': {},
  'methods': {
   'setUsername': 1,
   'unsetUsername': 2,
   'setPassword': 3,
   'unsetPassword': 4,
   'pulseSignIn': 5,
   'pulseSignedIn': 6
  }
 },
 'xyz.swapee.wc.back.ISwapeeLoginPageController': {
  'id': 3600135954276,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeLoginPageControllerAR': {
  'id': 3600135954277,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeLoginPageControllerAT': {
  'id': 3600135954278,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeLoginPageScreen': {
  'id': 3600135954279,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeLoginPageScreen': {
  'id': 3600135954280,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeLoginPageScreenAR': {
  'id': 3600135954281,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeLoginPageScreenAT': {
  'id': 3600135954282,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowComputer': {
  'id': 3600135954283,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.OfferRowMemoryPQs': {
  'id': 3600135954284,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferRowOuterCore': {
  'id': 3600135954285,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferRowInputsPQs': {
  'id': 3600135954286,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferRowPort': {
  'id': 3600135954287,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOfferRowPort': 2
  }
 },
 'xyz.swapee.wc.IOfferRowPortInterface': {
  'id': 3600135954288,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowCore': {
  'id': 3600135954289,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOfferRowCore': 2
  }
 },
 'xyz.swapee.wc.IOfferRowProcessor': {
  'id': 3600135954290,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRow': {
  'id': 3600135954291,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowBuffer': {
  'id': 3600135954292,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowHtmlComponent': {
  'id': 3600135954293,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowElement': {
  'id': 3600135954294,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IOfferRowElementPort': {
  'id': 3600135954295,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowDesigner': {
  'id': 3600135954296,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOfferRowGPU': {
  'id': 3600135954297,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowDisplay': {
  'id': 3600135954298,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OfferRowVdusPQs': {
  'id': 3600135954299,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOfferRowDisplay': {
  'id': 3600135954300,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OfferRowClassesPQs': {
  'id': 3600135954301,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferRowController': {
  'id': 3600135954302,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IOfferRowController': {
  'id': 3600135954303,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowController': {
  'id': 3600135954304,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowControllerAR': {
  'id': 3600135954305,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferRowControllerAT': {
  'id': 3600135954306,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowScreen': {
  'id': 3600135954307,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowScreen': {
  'id': 3600135954308,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferRowScreenAR': {
  'id': 3600135954309,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowScreenAT': {
  'id': 3600135954310,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangePortInterface': {
  'id': 3600135954311,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ChangellyExchangeQueriesPQs': {
  'id': 3600135954312,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeScreen': {
  'id': 3600135954313,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeScreen': {
  'id': 3600135954314,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IChangellyExchangeScreenAR': {
  'id': 3600135954315,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeScreenAT': {
  'id': 3600135954316,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeComputer': {
  'id': 3600135954317,
  'symbols': {},
  'methods': {
   'adaptUserpic': 1,
   'compute': 2
  }
 },
 'xyz.swapee.wc.SwapeeMeMemoryPQs': {
  'id': 3600135954318,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeOuterCore': {
  'id': 3600135954319,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeInputsPQs': {
  'id': 3600135954320,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMePort': {
  'id': 3600135954321,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeMePort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMePortInterface': {
  'id': 3600135954322,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeCachePQs': {
  'id': 3600135954323,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeCore': {
  'id': 3600135954324,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeMeCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMeProcessor': {
  'id': 3600135954325,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMe': {
  'id': 3600135954326,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeBuffer': {
  'id': 3600135954327,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeHtmlComponent': {
  'id': 3600135954328,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeElement': {
  'id': 3600135954329,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ISwapeeMeElementPort': {
  'id': 3600135954330,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeDesigner': {
  'id': 3600135954331,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeMeGPU': {
  'id': 3600135954332,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeDisplay': {
  'id': 3600135954333,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeVdusPQs': {
  'id': 3600135954334,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeDisplay': {
  'id': 3600135954335,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeClassesPQs': {
  'id': 3600135954336,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeControllerHyperslice': {
  'id': 3600135954337,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice': {
  'id': 3600135954338,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeController': {
  'id': 3600135954339,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'signIn': 2,
   'signOut': 3,
   'signedOut': 4,
   'onSignedOut': 5,
   'setToken': 6,
   'unsetToken': 7,
   'setUserid': 8,
   'unsetUserid': 9,
   'setUsername': 10,
   'unsetUsername': 11,
   'setDisplayName': 12,
   'unsetDisplayName': 13,
   'setProfile': 14,
   'unsetProfile': 15
  }
 },
 'xyz.swapee.wc.front.ISwapeeMeControllerHyperslice': {
  'id': 3600135954340,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice': {
  'id': 3600135954341,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeController': {
  'id': 3600135954342,
  'symbols': {},
  'methods': {
   'signIn': 1,
   'signOut': 2,
   'signedOut': 3,
   'onSignedOut': 4,
   'setToken': 5,
   'unsetToken': 6,
   'setUserid': 7,
   'unsetUserid': 8,
   'setUsername': 9,
   'unsetUsername': 10,
   'setDisplayName': 11,
   'unsetDisplayName': 12,
   'setProfile': 13,
   'unsetProfile': 14
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeController': {
  'id': 3600135954343,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeControllerAR': {
  'id': 3600135954344,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeControllerAT': {
  'id': 3600135954345,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeScreen': {
  'id': 3600135954346,
  'symbols': {},
  'methods': {
   'stashTokenInLocalStorage': 1,
   'stashUseridInLocalStorage': 2,
   'stashUsernameInLocalStorage': 3,
   'stashDisplayNameInLocalStorage': 4,
   'stashProfileInLocalStorage': 5,
   'openSignInPage': 6,
   'setLocal': 7
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeScreen': {
  'id': 3600135954347,
  'symbols': {},
  'methods': {
   'openSignInPage': 1
  }
 },
 'xyz.swapee.wc.front.ISwapeeMeScreenAR': {
  'id': 3600135954348,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeScreenAT': {
  'id': 3600135954349,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerComputer': {
  'id': 3600135954350,
  'symbols': {},
  'methods': {
   'adaptCreatedDate': 1,
   'adaptPaymentExpiredOrOverdue': 2,
   'adaptExchangeComplete': 3,
   'adaptNotId': 4,
   'compute': 6,
   'adaptCheckingPaymentStatus': 7,
   'adaptCheckingExchangeStatus': 8,
   'adaptFinishedDate': 9,
   'adaptWaitingForPayment': 10,
   'adaptExchangeFinished': 11,
   'adaptPaymentStatus': 12,
   'adaptProcessingPayment': 13,
   'adaptStatusLastChecked': 14,
   'adaptPaymentReceived': 15,
   'adaptPaymentCompleted': 16
  }
 },
 'xyz.swapee.wc.ExchangeBrokerMemoryPQs': {
  'id': 3600135954351,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeBrokerOuterCore': {
  'id': 3600135954352,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeBrokerInputsPQs': {
  'id': 3600135954353,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeBrokerPort': {
  'id': 3600135954354,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeBrokerPort': 2
  }
 },
 'xyz.swapee.wc.IExchangeBrokerPortInterface': {
  'id': 3600135954355,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeBrokerCachePQs': {
  'id': 3600135954356,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeBrokerCore': {
  'id': 3600135954357,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeBrokerCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeBrokerProcessor': {
  'id': 3600135954358,
  'symbols': {},
  'methods': {
   'pulseCreateTransaction': 1,
   'pulseCheckPayment': 2
  }
 },
 'xyz.swapee.wc.IExchangeBroker': {
  'id': 3600135954359,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerBuffer': {
  'id': 3600135954360,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerHtmlComponent': {
  'id': 3600135954361,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerElement': {
  'id': 3600135954362,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IExchangeBrokerElementPort': {
  'id': 3600135954363,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerDesigner': {
  'id': 3600135954364,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeBrokerGPU': {
  'id': 3600135954365,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerDisplay': {
  'id': 3600135954366,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeBrokerVdusPQs': {
  'id': 3600135954367,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeBrokerDisplay': {
  'id': 3600135954368,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IExchangeBrokerController': {
  'id': 3600135954369,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'reset': 2,
   'onReset': 3,
   'setCheckPaymentError': 4,
   'unsetCheckPaymentError': 5,
   'setAddress': 12,
   'unsetAddress': 13,
   'setExtraId': 14,
   'unsetExtraId': 15,
   'setRefundAddress': 16,
   'unsetRefundAddress': 17,
   'setRefundExtraId': 18,
   'unsetRefundExtraId': 19,
   'setCreateTransactionError': 20,
   'unsetCreateTransactionError': 21,
   'pulseCreateTransaction': 24,
   'pulseCheckPayment': 25,
   'setStatusLastChecked': 26,
   'unsetStatusLastChecked': 27
  }
 },
 'xyz.swapee.wc.front.IExchangeBrokerController': {
  'id': 3600135954370,
  'symbols': {},
  'methods': {
   'reset': 1,
   'onReset': 2,
   'setCheckPaymentError': 3,
   'unsetCheckPaymentError': 4,
   'setAddress': 11,
   'unsetAddress': 12,
   'setExtraId': 13,
   'unsetExtraId': 14,
   'setRefundAddress': 15,
   'unsetRefundAddress': 16,
   'setRefundExtraId': 17,
   'unsetRefundExtraId': 18,
   'setCreateTransactionError': 19,
   'unsetCreateTransactionError': 20,
   'pulseCreateTransaction': 23,
   'pulseCheckPayment': 24,
   'setStatusLastChecked': 25,
   'unsetStatusLastChecked': 26
  }
 },
 'xyz.swapee.wc.back.IExchangeBrokerController': {
  'id': 3600135954371,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeBrokerControllerAR': {
  'id': 3600135954372,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeBrokerControllerAT': {
  'id': 3600135954373,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerScreen': {
  'id': 3600135954374,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeBrokerScreen': {
  'id': 3600135954375,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeBrokerScreenAR': {
  'id': 3600135954376,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeBrokerScreenAT': {
  'id': 3600135954377,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerComputer': {
  'id': 3600135954378,
  'symbols': {},
  'methods': {
   'adaptWalletsRotor': 1,
   'adaptLoadBitcoinAddress': 2,
   'compute': 3
  }
 },
 'xyz.swapee.wc.SwapeeWalletPickerMemoryPQs': {
  'id': 3600135954379,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerOuterCore': {
  'id': 3600135954380,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeWalletPickerInputsPQs': {
  'id': 3600135954381,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerPort': {
  'id': 3600135954382,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeWalletPickerPort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerPortInterface': {
  'id': 3600135954383,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerCPUHyperslice': {
  'id': 3600135954384,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerCPUBindingHyperslice': {
  'id': 3600135954385,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerCPU': {
  'id': 3600135954386,
  'symbols': {},
  'methods': {
   'resetCore': 1
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerCore': {
  'id': 3600135954387,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeWalletPickerCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerProcessor': {
  'id': 3600135954388,
  'symbols': {},
  'methods': {
   'pulseLoadCreateInvoice': 1
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPicker': {
  'id': 3600135954389,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerBuffer': {
  'id': 3600135954390,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerHtmlComponentUtil': {
  'id': 3600135954391,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerHtmlComponent': {
  'id': 3600135954392,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerElement': {
  'id': 3600135954393,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildSwapeeMe': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerElementPort': {
  'id': 3600135954394,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerGeneratorHyperslice': {
  'id': 3600135954395,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerGeneratorBindingHyperslice': {
  'id': 3600135954396,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerGenerator': {
  'id': 3600135954397,
  'symbols': {},
  'methods': {
   'brushWallet': 1,
   'paintWallet': 2,
   'assignWallet': 3,
   'stateWallet': 4,
   'initWallet': 5,
   'WalletDynamo': 6
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerDesigner': {
  'id': 3600135954398,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerGPU': {
  'id': 3600135954399,
  'symbols': {},
  'methods': {
   'makeWalletElement': 1
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerDisplay': {
  'id': 3600135954400,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeWalletPickerQueriesPQs': {
  'id': 3600135954401,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.SwapeeWalletPickerVdusPQs': {
  'id': 3600135954402,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeWalletPickerDisplay': {
  'id': 3600135954403,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeWalletPickerClassesPQs': {
  'id': 3600135954404,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeWalletPickerController': {
  'id': 3600135954405,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'pulseLoadCreateInvoice': 2
  }
 },
 'xyz.swapee.wc.front.ISwapeeWalletPickerController': {
  'id': 3600135954406,
  'symbols': {},
  'methods': {
   'pulseLoadCreateInvoice': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeWalletPickerController': {
  'id': 3600135954407,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeWalletPickerControllerAR': {
  'id': 3600135954408,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeWalletPickerControllerAT': {
  'id': 3600135954409,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeWalletPickerScreen': {
  'id': 3600135954410,
  'symbols': {},
  'methods': {
   'makeWalletElement': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeWalletPickerScreen': {
  'id': 3600135954411,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeWalletPickerScreenAR': {
  'id': 3600135954412,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeWalletPickerScreenAT': {
  'id': 3600135954413,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeComputer': {
  'id': 3600135954414,
  'symbols': {},
  'methods': {
   'adaptCanCreateTransaction': 1,
   'adaptPaymentFailed': 2,
   'compute': 3,
   'adaptPickBtc': 4,
   'adaptBtc': 5
  }
 },
 'xyz.swapee.wc.OfferExchangeMemoryPQs': {
  'id': 3600135954415,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeOuterCore': {
  'id': 3600135954416,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferExchangeInputsPQs': {
  'id': 3600135954417,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangePort': {
  'id': 3600135954418,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOfferExchangePort': 2
  }
 },
 'xyz.swapee.wc.IOfferExchangePortInterface': {
  'id': 3600135954419,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferExchangeCachePQs': {
  'id': 3600135954420,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeCore': {
  'id': 3600135954421,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOfferExchangeCore': 2
  }
 },
 'xyz.swapee.wc.IOfferExchangeProcessor': {
  'id': 3600135954422,
  'symbols': {},
  'methods': {
   'pulseCopyPayinAddress': 1,
   'pulseCopyPayinExtra': 2
  }
 },
 'xyz.swapee.wc.IOfferExchange': {
  'id': 3600135954423,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeBuffer': {
  'id': 3600135954424,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeHtmlComponentUtil': {
  'id': 3600135954425,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeHtmlComponent': {
  'id': 3600135954426,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeElement': {
  'id': 3600135954427,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildAddressPopup': 4,
   'buildRefundPopup': 5,
   'buildSendPaymentCollapsar': 6,
   'buildCreateTransactionCollapsar': 7,
   'buildProgressColumn': 8,
   'buildSwapeeWalletPicker': 9,
   'buildExchangeBroker': 10,
   'buildPaymentLastChecked': 11,
   'short': 12,
   'server': 13,
   'inducer': 14,
   'buildFinishedCollapsar': 15,
   'buildProgressColumnMobTop': 16,
   'buildProgressColumnMobBot': 17,
   'buildExchangeStatusHint': 18,
   'buildExchangeStatusTitle': 19,
   'buildDuration': 20,
   'buildDealBroker': 21,
   'buildExchangeIntent': 22,
   'buildTransactionInfo': 23,
   'buildOfferExchangeInfoRow': 24,
   'buildOfferExchangeInfoRowMob': 25
  }
 },
 'xyz.swapee.wc.IOfferExchangeElementPort': {
  'id': 3600135954428,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeDesigner': {
  'id': 3600135954429,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOfferExchangeGPU': {
  'id': 3600135954430,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeDisplay': {
  'id': 3600135954431,
  'symbols': {},
  'methods': {
   'paint': 1,
   'paintCopyPayinAddress': 2,
   'paintCopyPayinExtra': 3
  }
 },
 'xyz.swapee.wc.OfferExchangeVdusPQs': {
  'id': 3600135954432,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOfferExchangeDisplay': {
  'id': 3600135954433,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OfferExchangeClassesPQs': {
  'id': 3600135954434,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeController': {
  'id': 3600135954435,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'selectAddress': 2,
   'selectRefund': 3,
   'onSelectAddress': 4,
   'onSelectRefund': 5,
   'setTocAccepted': 6,
   'unsetTocAccepted': 7,
   'pulseCopyPayinAddress': 8,
   'pulseCopyPayinExtra': 9,
   'flipTocAccepted': 10,
   'reset': 11,
   'onReset': 12,
   'swapAddresses': 13,
   'onSwapAddresses': 14
  }
 },
 'xyz.swapee.wc.front.IOfferExchangeController': {
  'id': 3600135954436,
  'symbols': {},
  'methods': {
   'selectAddress': 1,
   'selectRefund': 2,
   'onSelectAddress': 3,
   'onSelectRefund': 4,
   'setTocAccepted': 5,
   'unsetTocAccepted': 6,
   'pulseCopyPayinAddress': 7,
   'pulseCopyPayinExtra': 8,
   'flipTocAccepted': 9,
   'reset': 10,
   'onReset': 11,
   'swapAddresses': 12,
   'onSwapAddresses': 13
  }
 },
 'xyz.swapee.wc.back.IOfferExchangeController': {
  'id': 3600135954437,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeControllerAR': {
  'id': 3600135954438,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeControllerAT': {
  'id': 3600135954439,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeTouchscreen': {
  'id': 3600135954440,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeTouchscreen': {
  'id': 3600135954441,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeTouchscreenAR': {
  'id': 3600135954442,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeTouchscreenAT': {
  'id': 3600135954443,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IProgressColumnPortInterface': {
  'id': 3600135954444,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleComputer': {
  'id': 3600135954445,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.ExchangeStatusTitleMemoryPQs': {
  'id': 3600135954446,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeStatusTitleOuterCore': {
  'id': 3600135954447,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeStatusTitleInputsPQs': {
  'id': 3600135954448,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeStatusTitlePort': {
  'id': 3600135954449,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeStatusTitlePort': 2
  }
 },
 'xyz.swapee.wc.IExchangeStatusTitlePortInterface': {
  'id': 3600135954450,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleCore': {
  'id': 3600135954451,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeStatusTitleCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeStatusTitleProcessor': {
  'id': 3600135954452,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitle': {
  'id': 3600135954453,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleBuffer': {
  'id': 3600135954454,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleHtmlComponent': {
  'id': 3600135954455,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleElement': {
  'id': 3600135954456,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IExchangeStatusTitleElementPort': {
  'id': 3600135954457,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleDesigner': {
  'id': 3600135954458,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeStatusTitleGPU': {
  'id': 3600135954459,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleDisplay': {
  'id': 3600135954460,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeStatusTitleVdusPQs': {
  'id': 3600135954461,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeStatusTitleDisplay': {
  'id': 3600135954462,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeStatusTitleClassesPQs': {
  'id': 3600135954463,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeStatusTitleController': {
  'id': 3600135954464,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IExchangeStatusTitleController': {
  'id': 3600135954465,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusTitleController': {
  'id': 3600135954466,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusTitleControllerAR': {
  'id': 3600135954467,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeStatusTitleControllerAT': {
  'id': 3600135954468,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusTitleScreen': {
  'id': 3600135954469,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusTitleScreen': {
  'id': 3600135954470,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeStatusTitleScreenAR': {
  'id': 3600135954471,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusTitleScreenAT': {
  'id': 3600135954472,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintComputer': {
  'id': 3600135954473,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.ExchangeStatusHintMemoryPQs': {
  'id': 3600135954474,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeStatusHintOuterCore': {
  'id': 3600135954475,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeStatusHintInputsPQs': {
  'id': 3600135954476,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeStatusHintPort': {
  'id': 3600135954477,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeStatusHintPort': 2
  }
 },
 'xyz.swapee.wc.IExchangeStatusHintPortInterface': {
  'id': 3600135954478,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintCore': {
  'id': 3600135954479,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeStatusHintCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeStatusHintProcessor': {
  'id': 3600135954480,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHint': {
  'id': 3600135954481,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintBuffer': {
  'id': 3600135954482,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintHtmlComponent': {
  'id': 3600135954483,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintElement': {
  'id': 3600135954484,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IExchangeStatusHintElementPort': {
  'id': 3600135954485,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintDesigner': {
  'id': 3600135954486,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeStatusHintGPU': {
  'id': 3600135954487,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintDisplay': {
  'id': 3600135954488,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeStatusHintVdusPQs': {
  'id': 3600135954489,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeStatusHintDisplay': {
  'id': 3600135954490,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeStatusHintClassesPQs': {
  'id': 3600135954491,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeStatusHintController': {
  'id': 3600135954492,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IExchangeStatusHintController': {
  'id': 3600135954493,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusHintController': {
  'id': 3600135954494,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusHintControllerAR': {
  'id': 3600135954495,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeStatusHintControllerAT': {
  'id': 3600135954496,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeStatusHintScreen': {
  'id': 3600135954497,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusHintScreen': {
  'id': 3600135954498,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeStatusHintScreenAR': {
  'id': 3600135954499,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeStatusHintScreenAT': {
  'id': 3600135954500,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersAggregatorPortInterface': {
  'id': 3600135954501,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ProgressColumnCachePQs': {
  'id': 3600135954502,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersTablePortInterface': {
  'id': 3600135954503,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountComputer': {
  'id': 3600135954504,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeAccountMemoryPQs': {
  'id': 3600135954505,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountOuterCore': {
  'id': 3600135954506,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeAccountInputsPQs': {
  'id': 3600135954507,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountPort': {
  'id': 3600135954508,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeMeAccountPort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountPortInterface': {
  'id': 3600135954509,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountCore': {
  'id': 3600135954510,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeMeAccountCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountProcessor': {
  'id': 3600135954511,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccount': {
  'id': 3600135954512,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountBuffer': {
  'id': 3600135954513,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil': {
  'id': 3600135954514,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountHtmlComponent': {
  'id': 3600135954515,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountElement': {
  'id': 3600135954516,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildSwapeeMe': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountElementPort': {
  'id': 3600135954517,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountDesigner': {
  'id': 3600135954518,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountGPU': {
  'id': 3600135954519,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountDisplay': {
  'id': 3600135954520,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeAccountQueriesPQs': {
  'id': 3600135954521,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeAccountVdusPQs': {
  'id': 3600135954522,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountDisplay': {
  'id': 3600135954523,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeAccountClassesPQs': {
  'id': 3600135954524,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountController': {
  'id': 3600135954525,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ISwapeeMeAccountController': {
  'id': 3600135954526,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountController': {
  'id': 3600135954527,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountControllerAR': {
  'id': 3600135954528,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeAccountControllerAT': {
  'id': 3600135954529,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountScreen': {
  'id': 3600135954530,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountScreen': {
  'id': 3600135954531,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeAccountScreenAR': {
  'id': 3600135954532,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountScreenAT': {
  'id': 3600135954533,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPingComputer': {
  'id': 3600135954534,
  'symbols': {},
  'methods': {
   'adaptPing': 1,
   'adaptDifference': 2,
   'adaptDifferenceRequired': 3,
   'compute': 4
  }
 },
 'xyz.swapee.wc.RegionPingMemoryPQs': {
  'id': 3600135954535,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionPingOuterCore': {
  'id': 3600135954536,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.RegionPingInputsPQs': {
  'id': 3600135954537,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionPingPort': {
  'id': 3600135954538,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetRegionPingPort': 2
  }
 },
 'xyz.swapee.wc.IRegionPingPortInterface': {
  'id': 3600135954539,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.RegionPingCachePQs': {
  'id': 3600135954540,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionPingCore': {
  'id': 3600135954541,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetRegionPingCore': 2
  }
 },
 'xyz.swapee.wc.IRegionPingProcessor': {
  'id': 3600135954542,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPing': {
  'id': 3600135954543,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPingBuffer': {
  'id': 3600135954544,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPingHtmlComponent': {
  'id': 3600135954545,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPingElement': {
  'id': 3600135954546,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IRegionPingElementPort': {
  'id': 3600135954547,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPingRadio': {
  'id': 3600135954548,
  'symbols': {},
  'methods': {
   'adaptLoadPing': 1
  }
 },
 'xyz.swapee.wc.IRegionPingDesigner': {
  'id': 3600135954549,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IRegionPingService': {
  'id': 3600135954550,
  'symbols': {},
  'methods': {
   'filterPing': 1
  }
 },
 'xyz.swapee.wc.IRegionPingGPU': {
  'id': 3600135954551,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPingDisplay': {
  'id': 3600135954552,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.RegionPingVdusPQs': {
  'id': 3600135954553,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IRegionPingDisplay': {
  'id': 3600135954554,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.RegionPingClassesPQs': {
  'id': 3600135954555,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionPingController': {
  'id': 3600135954556,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setStart': 2,
   'unsetStart': 3,
   'setReceived': 4,
   'unsetReceived': 5,
   'setDiff': 6,
   'unsetDiff': 7,
   'setDiff2': 8,
   'unsetDiff2': 9,
   'loadPing': 10
  }
 },
 'xyz.swapee.wc.front.IRegionPingController': {
  'id': 3600135954557,
  'symbols': {},
  'methods': {
   'setStart': 1,
   'unsetStart': 2,
   'setReceived': 3,
   'unsetReceived': 4,
   'setDiff': 5,
   'unsetDiff': 6,
   'setDiff2': 7,
   'unsetDiff2': 8,
   'loadPing': 9
  }
 },
 'xyz.swapee.wc.back.IRegionPingController': {
  'id': 3600135954558,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionPingControllerAR': {
  'id': 3600135954559,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IRegionPingControllerAT': {
  'id': 3600135954560,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionPingScreen': {
  'id': 3600135954561,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionPingScreen': {
  'id': 3600135954562,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IRegionPingScreenAR': {
  'id': 3600135954563,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionPingScreenAT': {
  'id': 3600135954564,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorComputer': {
  'id': 3600135954565,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptRegion': 2
  }
 },
 'xyz.swapee.wc.RegionSelectorMemoryPQs': {
  'id': 3600135954566,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorOuterCore': {
  'id': 3600135954567,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.RegionSelectorInputsPQs': {
  'id': 3600135954568,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorPort': {
  'id': 3600135954569,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetRegionSelectorPort': 2
  }
 },
 'xyz.swapee.wc.IRegionSelectorPortInterface': {
  'id': 3600135954570,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.RegionSelectorCachePQs': {
  'id': 3600135954571,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorCore': {
  'id': 3600135954572,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetRegionSelectorCore': 2
  }
 },
 'xyz.swapee.wc.IRegionSelectorProcessor': {
  'id': 3600135954573,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelector': {
  'id': 3600135954574,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorBuffer': {
  'id': 3600135954575,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorHtmlComponentUtil': {
  'id': 3600135954576,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorHtmlComponent': {
  'id': 3600135954577,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorElement': {
  'id': 3600135954578,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildRegionPingEu': 4,
   'buildRegionPingUs': 5,
   'buildRegionPingLondon': 6,
   'short': 7,
   'server': 8,
   'inducer': 9,
   'buildRegionPingSaoPaulo': 10,
   'buildRegionPingSydney': 11,
   'buildRegionPingTaiwan': 12,
   'buildRegionPingBelgium': 13,
   'buildRegionPingNetherlands': 14,
   'buildRegionPingParis': 15,
   'buildRegionPingFrankfurt': 16,
   'buildRegionPingWarsaw': 17,
   'buildRegionPingZurich': 18,
   'buildRegionPingMumbai': 19,
   'buildRegionPingDelhi': 20,
   'buildRegionPingSingapore': 21,
   'buildRegionPingJakarta': 22,
   'buildRegionPingHongKong': 23,
   'buildRegionPingTokyo': 24,
   'buildRegionPingOsaka': 25,
   'buildRegionPingSeoul': 26,
   'buildRegionPingDoha': 27,
   'buildRegionPingTelAviv': 29
  }
 },
 'xyz.swapee.wc.IRegionSelectorElementPort': {
  'id': 3600135954579,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorDesigner': {
  'id': 3600135954580,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IRegionSelectorGPU': {
  'id': 3600135954581,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorDisplay': {
  'id': 3600135954582,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.RegionSelectorVdusPQs': {
  'id': 3600135954583,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IRegionSelectorDisplay': {
  'id': 3600135954584,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorController': {
  'id': 3600135954585,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IRegionSelectorController': {
  'id': 3600135954586,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorController': {
  'id': 3600135954587,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorControllerAR': {
  'id': 3600135954588,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IRegionSelectorControllerAT': {
  'id': 3600135954589,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorScreen': {
  'id': 3600135954590,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorScreen': {
  'id': 3600135954591,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IRegionSelectorScreenAR': {
  'id': 3600135954592,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorScreenAT': {
  'id': 3600135954593,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonComputer': {
  'id': 3600135954594,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.SwapeeButtonMemoryPQs': {
  'id': 3600135954595,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeButtonOuterCore': {
  'id': 3600135954596,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeButtonInputsPQs': {
  'id': 3600135954597,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeButtonPort': {
  'id': 3600135954598,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeButtonPort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeButtonPortInterface': {
  'id': 3600135954599,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonCore': {
  'id': 3600135954600,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeButtonCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeButtonProcessor': {
  'id': 3600135954601,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButton': {
  'id': 3600135954602,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonBuffer': {
  'id': 3600135954603,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonHtmlComponent': {
  'id': 3600135954604,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonElement': {
  'id': 3600135954605,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ISwapeeButtonElementPort': {
  'id': 3600135954606,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonDesigner': {
  'id': 3600135954607,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeButtonGPU': {
  'id': 3600135954608,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonDisplay': {
  'id': 3600135954609,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeButtonVdusPQs': {
  'id': 3600135954610,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeButtonDisplay': {
  'id': 3600135954611,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ISwapeeButtonController': {
  'id': 3600135954612,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ISwapeeButtonController': {
  'id': 3600135954613,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonController': {
  'id': 3600135954614,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonControllerAR': {
  'id': 3600135954615,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeButtonControllerAT': {
  'id': 3600135954616,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeButtonScreen': {
  'id': 3600135954617,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonScreen': {
  'id': 3600135954618,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeButtonScreenAR': {
  'id': 3600135954619,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeButtonScreenAT': {
  'id': 3600135954620,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsComputer': {
  'id': 3600135954621,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.SwapeeSocialButtonsMemoryPQs': {
  'id': 3600135954622,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsOuterCore': {
  'id': 3600135954623,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeSocialButtonsInputsPQs': {
  'id': 3600135954624,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsPort': {
  'id': 3600135954625,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeSocialButtonsPort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsPortInterface': {
  'id': 3600135954626,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsCore': {
  'id': 3600135954627,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeSocialButtonsCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsProcessor': {
  'id': 3600135954628,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtons': {
  'id': 3600135954629,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsBuffer': {
  'id': 3600135954630,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsHtmlComponent': {
  'id': 3600135954631,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsElement': {
  'id': 3600135954632,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsElementPort': {
  'id': 3600135954633,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsDesigner': {
  'id': 3600135954634,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsGPU': {
  'id': 3600135954635,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsDisplay': {
  'id': 3600135954636,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeSocialButtonsVdusPQs': {
  'id': 3600135954637,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeSocialButtonsDisplay': {
  'id': 3600135954638,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsController': {
  'id': 3600135954639,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ISwapeeSocialButtonsController': {
  'id': 3600135954640,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeSocialButtonsController': {
  'id': 3600135954641,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeSocialButtonsControllerAR': {
  'id': 3600135954642,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeSocialButtonsControllerAT': {
  'id': 3600135954643,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeSocialButtonsScreen': {
  'id': 3600135954644,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeSocialButtonsScreen': {
  'id': 3600135954645,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeSocialButtonsScreenAR': {
  'id': 3600135954646,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeSocialButtonsScreenAT': {
  'id': 3600135954647,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathComputer': {
  'id': 3600135954648,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.MotionPathMemoryPQs': {
  'id': 3600135954649,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IMotionPathOuterCore': {
  'id': 3600135954650,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.MotionPathInputsPQs': {
  'id': 3600135954651,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IMotionPathPort': {
  'id': 3600135954652,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetMotionPathPort': 2
  }
 },
 'xyz.swapee.wc.IMotionPathPortInterface': {
  'id': 3600135954653,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathCore': {
  'id': 3600135954654,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetMotionPathCore': 2
  }
 },
 'xyz.swapee.wc.IMotionPathProcessor': {
  'id': 3600135954655,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPath': {
  'id': 3600135954656,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathBuffer': {
  'id': 3600135954657,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathHtmlComponent': {
  'id': 3600135954658,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathElement': {
  'id': 3600135954659,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IMotionPathElementPort': {
  'id': 3600135954660,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathDesigner': {
  'id': 3600135954661,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IMotionPathGPU': {
  'id': 3600135954662,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathDisplay': {
  'id': 3600135954663,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.MotionPathVdusPQs': {
  'id': 3600135954664,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IMotionPathDisplay': {
  'id': 3600135954665,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IMotionPathController': {
  'id': 3600135954666,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IMotionPathController': {
  'id': 3600135954667,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathController': {
  'id': 3600135954668,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathControllerAR': {
  'id': 3600135954669,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IMotionPathControllerAT': {
  'id': 3600135954670,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathScreen': {
  'id': 3600135954671,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathScreen': {
  'id': 3600135954672,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IMotionPathScreenAR': {
  'id': 3600135954673,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathScreenAT': {
  'id': 3600135954674,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathComputer': {
  'id': 3600135954675,
  'symbols': {},
  'methods': {
   'adaptPath': 1,
   'compute': 2
  }
 },
 'xyz.swapee.wc.BorderMotionPathMemoryPQs': {
  'id': 3600135954676,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IBorderMotionPathOuterCore': {
  'id': 3600135954677,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.BorderMotionPathInputsPQs': {
  'id': 3600135954678,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IBorderMotionPathPort': {
  'id': 3600135954679,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetBorderMotionPathPort': 2
  }
 },
 'xyz.swapee.wc.IBorderMotionPathPortInterface': {
  'id': 3600135954680,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathCore': {
  'id': 3600135954681,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetBorderMotionPathCore': 2
  }
 },
 'xyz.swapee.wc.IBorderMotionPathProcessor': {
  'id': 3600135954682,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPath': {
  'id': 3600135954683,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathBuffer': {
  'id': 3600135954684,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathHtmlComponentUtil': {
  'id': 3600135954685,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IBorderMotionPathHtmlComponent': {
  'id': 3600135954686,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathElement': {
  'id': 3600135954687,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildClientRect': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.IBorderMotionPathElementPort': {
  'id': 3600135954688,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathDesigner': {
  'id': 3600135954689,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IBorderMotionPathGPU': {
  'id': 3600135954690,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathDisplay': {
  'id': 3600135954691,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.BorderMotionPathVdusPQs': {
  'id': 3600135954692,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IBorderMotionPathDisplay': {
  'id': 3600135954693,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.BorderMotionPathClassesPQs': {
  'id': 3600135954694,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IBorderMotionPathController': {
  'id': 3600135954695,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IBorderMotionPathController': {
  'id': 3600135954696,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathController': {
  'id': 3600135954697,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathControllerAR': {
  'id': 3600135954698,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IBorderMotionPathControllerAT': {
  'id': 3600135954699,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IBorderMotionPathScreen': {
  'id': 3600135954700,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathScreen': {
  'id': 3600135954701,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IBorderMotionPathScreenAR': {
  'id': 3600135954702,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IBorderMotionPathScreenAT': {
  'id': 3600135954703,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterComputer': {
  'id': 3600135954704,
  'symbols': {},
  'methods': {
   'adaptSelectorsIntent': 1,
   'compute': 2,
   'adaptIgnoreCryptosOut': 3,
   'adaptIgnoreCryptosIn': 4
  }
 },
 'xyz.swapee.wc.OffersFilterMemoryPQs': {
  'id': 3600135954705,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterOuterCore': {
  'id': 3600135954706,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersFilterInputsPQs': {
  'id': 3600135954707,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterPort': {
  'id': 3600135954708,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersFilterPort': 2
  }
 },
 'xyz.swapee.wc.IOffersFilterPortInterface': {
  'id': 3600135954709,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterCore': {
  'id': 3600135954710,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersFilterCore': 2
  }
 },
 'xyz.swapee.wc.IOffersFilterProcessor': {
  'id': 3600135954711,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilter': {
  'id': 3600135954712,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterBuffer': {
  'id': 3600135954713,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterHtmlComponentUtil': {
  'id': 3600135954714,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterHtmlComponent': {
  'id': 3600135954715,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterElement': {
  'id': 3600135954716,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildCryptoSelectIn': 4,
   'buildCryptoSelectOut': 5,
   'buildExchangeIntent': 6,
   'buildReadyLoadingStripe1': 7,
   'buildReadyLoadingStripe2': 8,
   'buildDealBroker': 9,
   'short': 10,
   'server': 11,
   'inducer': 12
  }
 },
 'xyz.swapee.wc.IOffersFilterElementPort': {
  'id': 3600135954717,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterDesigner': {
  'id': 3600135954718,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersFilterGPU': {
  'id': 3600135954719,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterDisplay': {
  'id': 3600135954720,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersFilterQueriesPQs': {
  'id': 3600135954721,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.OffersFilterVdusPQs': {
  'id': 3600135954722,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersFilterDisplay': {
  'id': 3600135954723,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersFilterClassesPQs': {
  'id': 3600135954724,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterController': {
  'id': 3600135954725,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   '$ExchangeIntent_setAmountFrom_on_AmountIn_input': 4,
   '$ExchangeIntent_setAmountFrom_on_MinAmountLa_click': 6,
   '$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click': 7,
   'swapDirection': 8,
   'onSwapDirection': 9
  }
 },
 'xyz.swapee.wc.front.IOffersFilterController': {
  'id': 3600135954726,
  'symbols': {},
  'methods': {
   '$ExchangeIntent_setAmountFrom_on_AmountIn_input': 1,
   '$ExchangeIntent_setAmountFrom_on_MinAmountLa_click': 3,
   '$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click': 4,
   'swapDirection': 7,
   'onSwapDirection': 8
  }
 },
 'xyz.swapee.wc.back.IOffersFilterController': {
  'id': 3600135954727,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersFilterControllerAR': {
  'id': 3600135954728,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterControllerAT': {
  'id': 3600135954729,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterScreen': {
  'id': 3600135954730,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersFilterScreen': {
  'id': 3600135954731,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterScreenAR': {
  'id': 3600135954732,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersFilterScreenAT': {
  'id': 3600135954733,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerComputer': {
  'id': 3600135954734,
  'symbols': {},
  'methods': {
   'adaptEstimatedAmountTo': 1,
   'compute': 2
  }
 },
 'xyz.swapee.wc.DealBrokerMemoryPQs': {
  'id': 3600135954735,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerOuterCore': {
  'id': 3600135954736,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.DealBrokerInputsPQs': {
  'id': 3600135954737,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerPort': {
  'id': 3600135954738,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetDealBrokerPort': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerPortInterface': {
  'id': 3600135954739,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.DealBrokerCachePQs': {
  'id': 3600135954740,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerCore': {
  'id': 3600135954741,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetDealBrokerCore': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerProcessor': {
  'id': 3600135954742,
  'symbols': {},
  'methods': {
   'pulseGetOffer': 1
  }
 },
 'xyz.swapee.wc.IDealBroker': {
  'id': 3600135954743,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerBuffer': {
  'id': 3600135954744,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerHtmlComponent': {
  'id': 3600135954745,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerElement': {
  'id': 3600135954746,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IDealBrokerElementPort': {
  'id': 3600135954747,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerDesigner': {
  'id': 3600135954748,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IDealBrokerGPU': {
  'id': 3600135954749,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerDisplay': {
  'id': 3600135954750,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.DealBrokerVdusPQs': {
  'id': 3600135954751,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IDealBrokerDisplay': {
  'id': 3600135954752,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerController': {
  'id': 3600135954753,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setGetOfferError': 2,
   'unsetGetOfferError': 3,
   'flipGettingOffer': 4,
   'setGettingOffer': 5,
   'unsetGettingOffer': 6,
   'setNetworkFee': 7,
   'unsetNetworkFee': 8,
   'setPartnerFee': 9,
   'unsetPartnerFee': 10,
   'setEstimatedFloatAmountTo': 11,
   'unsetEstimatedFloatAmountTo': 12,
   'setEstimatedFixedAmountTo': 13,
   'unsetEstimatedFixedAmountTo': 14,
   'setVisibleAmount': 15,
   'unsetVisibleAmount': 16,
   'pulseGetOffer': 17,
   'setRate': 18,
   'unsetRate': 19
  }
 },
 'xyz.swapee.wc.front.IDealBrokerController': {
  'id': 3600135954754,
  'symbols': {},
  'methods': {
   'setGetOfferError': 1,
   'unsetGetOfferError': 2,
   'flipGettingOffer': 3,
   'setGettingOffer': 4,
   'unsetGettingOffer': 5,
   'setNetworkFee': 6,
   'unsetNetworkFee': 7,
   'setPartnerFee': 8,
   'unsetPartnerFee': 9,
   'setEstimatedFloatAmountTo': 10,
   'unsetEstimatedFloatAmountTo': 11,
   'setEstimatedFixedAmountTo': 12,
   'unsetEstimatedFixedAmountTo': 13,
   'setVisibleAmount': 14,
   'unsetVisibleAmount': 15,
   'pulseGetOffer': 16,
   'setRate': 17,
   'unsetRate': 18
  }
 },
 'xyz.swapee.wc.back.IDealBrokerController': {
  'id': 3600135954755,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerControllerAR': {
  'id': 3600135954756,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerControllerAT': {
  'id': 3600135954757,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerScreen': {
  'id': 3600135954758,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerScreen': {
  'id': 3600135954759,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerScreenAR': {
  'id': 3600135954760,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerScreenAT': {
  'id': 3600135954761,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentComputer': {
  'id': 3600135954762,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptInterlocks': 2
  }
 },
 'xyz.swapee.wc.ExchangeIntentMemoryPQs': {
  'id': 3600135954763,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIntentOuterCore': {
  'id': 3600135954764,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeIntentInputsPQs': {
  'id': 3600135954765,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIntentPort': {
  'id': 3600135954766,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeIntentPort': 2
  }
 },
 'xyz.swapee.wc.IExchangeIntentPortInterface': {
  'id': 3600135954767,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentCore': {
  'id': 3600135954768,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeIntentCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeIntentProcessor': {
  'id': 3600135954769,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntent': {
  'id': 3600135954770,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentBuffer': {
  'id': 3600135954771,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentHtmlComponent': {
  'id': 3600135954772,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentElement': {
  'id': 3600135954773,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IExchangeIntentElementPort': {
  'id': 3600135954774,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentDesigner': {
  'id': 3600135954775,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeIntentGPU': {
  'id': 3600135954776,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentDisplay': {
  'id': 3600135954777,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeIntentVdusPQs': {
  'id': 3600135954778,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeIntentDisplay': {
  'id': 3600135954779,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IExchangeIntentController': {
  'id': 3600135954780,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setCurrencyFrom': 2,
   'unsetCurrencyFrom': 3,
   'setAmountFrom': 4,
   'unsetAmountFrom': 5,
   'setCurrencyTo': 6,
   'unsetCurrencyTo': 7,
   'flipFixed': 8,
   'setFixed': 9,
   'unsetFixed': 10,
   'change': 11,
   'onChange': 12,
   'flipFloat': 13,
   'setFloat': 14,
   'unsetFloat': 15,
   'flipAny': 16,
   'setAny': 17,
   'unsetAny': 18,
   'intentChange': 19,
   'onIntentChange': 20
  }
 },
 'xyz.swapee.wc.front.IExchangeIntentController': {
  'id': 3600135954781,
  'symbols': {},
  'methods': {
   'setCurrencyFrom': 1,
   'unsetCurrencyFrom': 2,
   'setAmountFrom': 3,
   'unsetAmountFrom': 4,
   'setCurrencyTo': 5,
   'unsetCurrencyTo': 6,
   'flipFixed': 7,
   'setFixed': 8,
   'unsetFixed': 9,
   'change': 10,
   'onChange': 11,
   'flipFloat': 12,
   'setFloat': 13,
   'unsetFloat': 14,
   'flipAny': 15,
   'setAny': 16,
   'unsetAny': 17,
   'intentChange': 18,
   'onIntentChange': 19
  }
 },
 'xyz.swapee.wc.back.IExchangeIntentController': {
  'id': 3600135954782,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIntentControllerAR': {
  'id': 3600135954783,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIntentControllerAT': {
  'id': 3600135954784,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentScreen': {
  'id': 3600135954785,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIntentScreen': {
  'id': 3600135954786,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIntentScreenAR': {
  'id': 3600135954787,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIntentScreenAT': {
  'id': 3600135954788,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoComputer': {
  'id': 3600135954789,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoMemoryPQs': {
  'id': 3600135954790,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoOuterCore': {
  'id': 3600135954791,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TransactionInfoInputsPQs': {
  'id': 3600135954792,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoPort': {
  'id': 3600135954793,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTransactionInfoPort': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoPortInterface': {
  'id': 3600135954794,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoCore': {
  'id': 3600135954795,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTransactionInfoCore': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoProcessor': {
  'id': 3600135954796,
  'symbols': {},
  'methods': {
   'pulseGetTransaction': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfo': {
  'id': 3600135954797,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoBuffer': {
  'id': 3600135954798,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHtmlComponent': {
  'id': 3600135954799,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoElement': {
  'id': 3600135954800,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ITransactionInfoElementPort': {
  'id': 3600135954801,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoDesigner': {
  'id': 3600135954802,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITransactionInfoGPU': {
  'id': 3600135954803,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoDisplay': {
  'id': 3600135954804,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoVdusPQs': {
  'id': 3600135954805,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITransactionInfoDisplay': {
  'id': 3600135954806,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoClassesPQs': {
  'id': 3600135954807,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoController': {
  'id': 3600135954808,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'pulseGetTransaction': 2
  }
 },
 'xyz.swapee.wc.front.ITransactionInfoController': {
  'id': 3600135954809,
  'symbols': {},
  'methods': {
   'pulseGetTransaction': 1
  }
 },
 'xyz.swapee.wc.back.ITransactionInfoController': {
  'id': 3600135954810,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoControllerAR': {
  'id': 3600135954811,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoControllerAT': {
  'id': 3600135954812,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoScreen': {
  'id': 3600135954813,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoScreen': {
  'id': 3600135954814,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoScreenAR': {
  'id': 3600135954815,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoScreenAT': {
  'id': 3600135954816,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadComputer': {
  'id': 3600135954817,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadMemoryPQs': {
  'id': 3600135954818,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadOuterCore': {
  'id': 3600135954819,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TransactionInfoHeadInputsPQs': {
  'id': 3600135954820,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadPort': {
  'id': 3600135954821,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetTransactionInfoHeadPort': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadPortInterface': {
  'id': 3600135954822,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadCore': {
  'id': 3600135954823,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetTransactionInfoHeadCore': 2
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadProcessor': {
  'id': 3600135954824,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHead': {
  'id': 3600135954825,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadBuffer': {
  'id': 3600135954826,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadHtmlComponentUtil': {
  'id': 3600135954827,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadHtmlComponent': {
  'id': 3600135954828,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadElement': {
  'id': 3600135954829,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildTransactionInfo': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadElementPort': {
  'id': 3600135954830,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadDesigner': {
  'id': 3600135954831,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadGPU': {
  'id': 3600135954832,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadDisplay': {
  'id': 3600135954833,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadQueriesPQs': {
  'id': 3600135954834,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadVdusPQs': {
  'id': 3600135954835,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadDisplay': {
  'id': 3600135954836,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.TransactionInfoHeadClassesPQs': {
  'id': 3600135954837,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ITransactionInfoHeadController': {
  'id': 3600135954838,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ITransactionInfoHeadController': {
  'id': 3600135954839,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadController': {
  'id': 3600135954840,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadControllerAR': {
  'id': 3600135954841,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoHeadControllerAT': {
  'id': 3600135954842,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ITransactionInfoHeadScreen': {
  'id': 3600135954843,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadScreen': {
  'id': 3600135954844,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ITransactionInfoHeadScreenAR': {
  'id': 3600135954845,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ITransactionInfoHeadScreenAT': {
  'id': 3600135954846,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowComputer': {
  'id': 3600135954847,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs': {
  'id': 3600135954848,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowOuterCore': {
  'id': 3600135954849,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferExchangeInfoRowInputsPQs': {
  'id': 3600135954850,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowPort': {
  'id': 3600135954851,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOfferExchangeInfoRowPort': 2
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowPortInterface': {
  'id': 3600135954852,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowCore': {
  'id': 3600135954853,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOfferExchangeInfoRowCore': 2
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowProcessor': {
  'id': 3600135954854,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRow': {
  'id': 3600135954855,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowBuffer': {
  'id': 3600135954856,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil': {
  'id': 3600135954857,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent': {
  'id': 3600135954858,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowElement': {
  'id': 3600135954859,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildDealBroker': 4,
   'buildExchangeIntent': 5,
   'short': 6,
   'server': 7,
   'inducer': 8
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowElementPort': {
  'id': 3600135954860,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowDesigner': {
  'id': 3600135954861,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowGPU': {
  'id': 3600135954862,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowDisplay': {
  'id': 3600135954863,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OfferExchangeInfoRowQueriesPQs': {
  'id': 3600135954864,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.OfferExchangeInfoRowVdusPQs': {
  'id': 3600135954865,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay': {
  'id': 3600135954866,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OfferExchangeInfoRowClassesPQs': {
  'id': 3600135954867,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowController': {
  'id': 3600135954868,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IOfferExchangeInfoRowController': {
  'id': 3600135954869,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeInfoRowController': {
  'id': 3600135954870,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR': {
  'id': 3600135954871,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT': {
  'id': 3600135954872,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeInfoRowScreen': {
  'id': 3600135954873,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeInfoRowScreen': {
  'id': 3600135954874,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR': {
  'id': 3600135954875,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT': {
  'id': 3600135954876,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowComputer': {
  'id': 3600135954877,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.ExchangeIdRowMemoryPQs': {
  'id': 3600135954878,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowOuterCore': {
  'id': 3600135954879,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeIdRowInputsPQs': {
  'id': 3600135954880,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowPort': {
  'id': 3600135954881,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeIdRowPort': 2
  }
 },
 'xyz.swapee.wc.IExchangeIdRowPortInterface': {
  'id': 3600135954882,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowCore': {
  'id': 3600135954883,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeIdRowCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeIdRowProcessor': {
  'id': 3600135954884,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRow': {
  'id': 3600135954885,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowBuffer': {
  'id': 3600135954886,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil': {
  'id': 3600135954887,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowHtmlComponent': {
  'id': 3600135954888,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowElement': {
  'id': 3600135954889,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildExchangeIntent': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.IExchangeIdRowElementPort': {
  'id': 3600135954890,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowDesigner': {
  'id': 3600135954891,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeIdRowGPU': {
  'id': 3600135954892,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowDisplay': {
  'id': 3600135954893,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeIdRowQueriesPQs': {
  'id': 3600135954894,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ExchangeIdRowVdusPQs': {
  'id': 3600135954895,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeIdRowDisplay': {
  'id': 3600135954896,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeIdRowClassesPQs': {
  'id': 3600135954897,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowController': {
  'id': 3600135954898,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IExchangeIdRowController': {
  'id': 3600135954899,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowController': {
  'id': 3600135954900,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowControllerAR': {
  'id': 3600135954901,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIdRowControllerAT': {
  'id': 3600135954902,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowScreen': {
  'id': 3600135954903,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowScreen': {
  'id': 3600135954904,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIdRowScreenAR': {
  'id': 3600135954905,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowScreenAT': {
  'id': 3600135954906,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferExchangeQueriesPQs': {
  'id': 3600135954907,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitComputer': {
  'id': 3600135954908,
  'symbols': {},
  'methods': {
   'adaptId': 1,
   'adaptNotId': 2,
   'adaptType': 3,
   'adaptIntent': 4,
   'compute': 5
  }
 },
 'xyz.swapee.wc.ExchangePageCircuitMemoryPQs': {
  'id': 3600135954909,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitOuterCore': {
  'id': 3600135954910,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangePageCircuitInputsPQs': {
  'id': 3600135954911,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitPort': {
  'id': 3600135954912,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangePageCircuitPort': 2
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitPortInterface': {
  'id': 3600135954913,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitCore': {
  'id': 3600135954914,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangePageCircuitCore': 2
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitProcessor': {
  'id': 3600135954915,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuit': {
  'id': 3600135954916,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitBuffer': {
  'id': 3600135954917,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitHtmlComponentUtil': {
  'id': 3600135954918,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitHtmlComponent': {
  'id': 3600135954919,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitElement': {
  'id': 3600135954920,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildOffersFilter': 4,
   'buildExchangeBroker': 5,
   'buildDealBroker': 6,
   'buildExchangeIntent': 7,
   'buildExchangeIdRow': 8,
   'buildTransactionInfo': 9,
   'buildTransactionInfoHead': 10,
   'buildTransactionInfoHeadMob': 11,
   'short': 12,
   'server': 13,
   'inducer': 14,
   'buildOfferExchange': 15,
   'buildCryptoSelectIn': 16,
   'buildCryptoSelectOut': 17
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitElementPort': {
  'id': 3600135954921,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitDesigner': {
  'id': 3600135954922,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitGPU': {
  'id': 3600135954923,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitDisplay': {
  'id': 3600135954924,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangePageCircuitQueriesPQs': {
  'id': 3600135954925,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ExchangePageCircuitVdusPQs': {
  'id': 3600135954926,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangePageCircuitDisplay': {
  'id': 3600135954927,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangePageCircuitClassesPQs': {
  'id': 3600135954928,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangePageCircuitController': {
  'id': 3600135954929,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setId': 2,
   'unsetId': 3,
   'setAmountFrom': 4,
   'unsetAmountFrom': 5,
   'setCryptoIn': 6,
   'unsetCryptoIn': 7,
   'setCryptoOut': 8,
   'unsetCryptoOut': 9
  }
 },
 'xyz.swapee.wc.front.IExchangePageCircuitController': {
  'id': 3600135954930,
  'symbols': {},
  'methods': {
   'setId': 1,
   'unsetId': 2,
   'setAmountFrom': 3,
   'unsetAmountFrom': 4,
   'setCryptoIn': 5,
   'unsetCryptoIn': 6,
   'setCryptoOut': 7,
   'unsetCryptoOut': 8
  }
 },
 'xyz.swapee.wc.back.IExchangePageCircuitController': {
  'id': 3600135954931,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangePageCircuitControllerAR': {
  'id': 3600135954932,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangePageCircuitControllerAT': {
  'id': 3600135954933,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangePageCircuitScreen': {
  'id': 3600135954934,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangePageCircuitScreen': {
  'id': 3600135954935,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangePageCircuitScreenAR': {
  'id': 3600135954936,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangePageCircuitScreenAT': {
  'id': 3600135954937,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeControllerHyperslice': {
  'id': 3600135954938,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferExchangeControllerBindingHyperslice': {
  'id': 3600135954939,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeControllerHyperslice': {
  'id': 3600135954940,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferExchangeControllerBindingHyperslice': {
  'id': 3600135954941,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.TransactionInfoCachePQs': {
  'id': 3600135954942,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterControllerHyperslice': {
  'id': 3600135954943,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterControllerBindingHyperslice': {
  'id': 3600135954944,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterControllerHyperslice': {
  'id': 3600135954945,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice': {
  'id': 3600135954946,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterComputer': {
  'id': 3600135954947,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterMemoryPQs': {
  'id': 3600135954948,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterOuterCore': {
  'id': 3600135954949,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterInputsPQs': {
  'id': 3600135954950,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterPort': {
  'id': 3600135954951,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetDealBrokerAggregatorAdapterPort': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterPortInterface': {
  'id': 3600135954952,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterCore': {
  'id': 3600135954953,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetDealBrokerAggregatorAdapterCore': 2
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterProcessor': {
  'id': 3600135954954,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapter': {
  'id': 3600135954955,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterBuffer': {
  'id': 3600135954956,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponentUtil': {
  'id': 3600135954957,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterHtmlComponent': {
  'id': 3600135954958,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterElement': {
  'id': 3600135954959,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildOffersAggregator': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterElementPort': {
  'id': 3600135954960,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterDesigner': {
  'id': 3600135954961,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterGPU': {
  'id': 3600135954962,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterDisplay': {
  'id': 3600135954963,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterQueriesPQs': {
  'id': 3600135954964,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.DealBrokerAggregatorAdapterVdusPQs': {
  'id': 3600135954965,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterDisplay': {
  'id': 3600135954966,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterController': {
  'id': 3600135954967,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'flipGettingOffer': 2,
   'setGettingOffer': 3,
   'unsetGettingOffer': 4
  }
 },
 'xyz.swapee.wc.front.IDealBrokerAggregatorAdapterController': {
  'id': 3600135954968,
  'symbols': {},
  'methods': {
   'flipGettingOffer': 1,
   'setGettingOffer': 2,
   'unsetGettingOffer': 3
  }
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterController': {
  'id': 3600135954969,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterControllerAR': {
  'id': 3600135954970,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerAggregatorAdapterControllerAT': {
  'id': 3600135954971,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IDealBrokerAggregatorAdapterScreen': {
  'id': 3600135954972,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreen': {
  'id': 3600135954973,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IDealBrokerAggregatorAdapterScreenAR': {
  'id': 3600135954974,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IDealBrokerAggregatorAdapterScreenAT': {
  'id': 3600135954975,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectPortInterface': {
  'id': 3600135954976,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitComputer': {
  'id': 3600135954977,
  'symbols': {},
  'methods': {
   'adaptIntent': 1,
   'adaptType': 2,
   'adaptInterlocks': 3,
   'compute': 4
  }
 },
 'xyz.swapee.wc.AggregatorPageCircuitMemoryPQs': {
  'id': 3600135954978,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitOuterCore': {
  'id': 3600135954979,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.AggregatorPageCircuitInputsPQs': {
  'id': 3600135954980,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitPort': {
  'id': 3600135954981,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetAggregatorPageCircuitPort': 2
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitPortInterface': {
  'id': 3600135954982,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitCore': {
  'id': 3600135954983,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetAggregatorPageCircuitCore': 2
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitProcessor': {
  'id': 3600135954984,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuit': {
  'id': 3600135954985,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitBuffer': {
  'id': 3600135954986,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitHtmlComponentUtil': {
  'id': 3600135954987,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitHtmlComponent': {
  'id': 3600135954988,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitElement': {
  'id': 3600135954989,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildExchangeIntent': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitElementPort': {
  'id': 3600135954990,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitDesigner': {
  'id': 3600135954991,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitGPU': {
  'id': 3600135954992,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitDisplay': {
  'id': 3600135954993,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.AggregatorPageCircuitQueriesPQs': {
  'id': 3600135954994,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.AggregatorPageCircuitVdusPQs': {
  'id': 3600135954995,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitDisplay': {
  'id': 3600135954996,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IAggregatorPageCircuitController': {
  'id': 3600135954997,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setAmountFrom': 2,
   'unsetAmountFrom': 3,
   'setCryptoIn': 4,
   'unsetCryptoIn': 5,
   'setCryptoOut': 6,
   'unsetCryptoOut': 7,
   'flipFixedRate': 8,
   'setFixedRate': 9,
   'unsetFixedRate': 10,
   'flipFloatRate': 11,
   'setFloatRate': 12,
   'unsetFloatRate': 13,
   'flipAnyRate': 14,
   'setAnyRate': 15,
   'unsetAnyRate': 16
  }
 },
 'xyz.swapee.wc.front.IAggregatorPageCircuitController': {
  'id': 3600135954998,
  'symbols': {},
  'methods': {
   'setAmountFrom': 1,
   'unsetAmountFrom': 2,
   'setCryptoIn': 3,
   'unsetCryptoIn': 4,
   'setCryptoOut': 5,
   'unsetCryptoOut': 6,
   'flipFixedRate': 7,
   'setFixedRate': 8,
   'unsetFixedRate': 9,
   'flipFloatRate': 10,
   'setFloatRate': 11,
   'unsetFloatRate': 12,
   'flipAnyRate': 13,
   'setAnyRate': 14,
   'unsetAnyRate': 15
  }
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitController': {
  'id': 3600135954999,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitControllerAR': {
  'id': 36001359541000,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IAggregatorPageCircuitControllerAT': {
  'id': 36001359541001,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IAggregatorPageCircuitScreen': {
  'id': 36001359541002,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitScreen': {
  'id': 36001359541003,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IAggregatorPageCircuitScreenAR': {
  'id': 36001359541004,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IAggregatorPageCircuitScreenAT': {
  'id': 36001359541005,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersTableCachePQs': {
  'id': 36001359541006,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersAggregatorHtmlComponentUtil': {
  'id': 36001359541007,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.OffersAggregatorQueriesPQs': {
  'id': 36001359541008,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})