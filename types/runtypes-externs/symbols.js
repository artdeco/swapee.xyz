/** @const {?} */ io.changenow.AbstractChangeNow
/** @const {?} */ io.changenow.AbstractChangeNowUniversal
/** @const {?} */ io.changenow.UChangeNow
/** @const {?} */ io.changenow.IChangeNow
/** @const {?} */ io.letsexchange.AbstractLetsExchange
/** @const {?} */ io.letsexchange.AbstractLetsExchangeUniversal
/** @const {?} */ io.letsexchange.ULetsExchange
/** @const {?} */ io.letsexchange.ILetsExchange
/** @const {?} */ $io.changenow.UChangeNow
/** @const {?} */ $io.changenow.IChangeNow
/** @const {?} */ $io.letsexchange.ULetsExchange
/** @const {?} */ $io.letsexchange.ILetsExchange
/** @const {?} */ xyz.swapee.AbstractSwapeeAide
/** @const {?} */ xyz.swapee.ISwapeeAide
/** @const {?} */ xyz.swapee.AbstractSwapee
/** @const {?} */ xyz.swapee.ISwapee
/** @const {?} */ xyz.swapee.AbstractChangellyConnector
/** @const {?} */ xyz.swapee.IChangellyConnector
/** @const {?} */ $xyz.swapee.ISwapeeAide
/** @const {?} */ $xyz.swapee.ISwapee
/** @const {?} */ $xyz.swapee.IChangellyConnector
/* @typal-type {types/design/IChangeNow.xml} io.changenow.AbstractChangeNow.getSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {!io.changenow.IChangeNow.getSymbols} */
io.changenow.AbstractChangeNow.getSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.AbstractChangeNow.setSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {!io.changenow.IChangeNow.setSymbols} */
io.changenow.AbstractChangeNow.setSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.AbstractChangeNow.Symbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {!io.changenow.IChangeNow.Symbols} */
io.changenow.AbstractChangeNow.Symbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.AbstractChangeNowUniversal.getSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {!io.changenow.UChangeNow.getSymbols} */
io.changenow.AbstractChangeNowUniversal.getSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.AbstractChangeNowUniversal.setSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {!io.changenow.UChangeNow.setSymbols} */
io.changenow.AbstractChangeNowUniversal.setSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.AbstractChangeNowUniversal.Symbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {!io.changenow.UChangeNow.Symbols} */
io.changenow.AbstractChangeNowUniversal.Symbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.UChangeNow.SymbolsIn filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @record */
$io.changenow.UChangeNow.SymbolsIn = function() {}
/** @type {io.changenow.IChangeNow|undefined} */
$io.changenow.UChangeNow.SymbolsIn.prototype._changeNow
/** @typedef {$io.changenow.UChangeNow.SymbolsIn} */
io.changenow.UChangeNow.SymbolsIn

// nss:io.changenow,$io.changenow.UChangeNow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.UChangeNow.Symbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @record */
$io.changenow.UChangeNow.Symbols = function() {}
/** @type {symbol} */
$io.changenow.UChangeNow.Symbols.prototype._changeNow
/** @typedef {$io.changenow.UChangeNow.Symbols} */
io.changenow.UChangeNow.Symbols

// nss:io.changenow,$io.changenow.UChangeNow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.UChangeNow.SymbolsOut filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @record */
$io.changenow.UChangeNow.SymbolsOut = function() {}
/** @type {io.changenow.IChangeNow} */
$io.changenow.UChangeNow.SymbolsOut.prototype._changeNow
/** @typedef {$io.changenow.UChangeNow.SymbolsOut} */
io.changenow.UChangeNow.SymbolsOut

// nss:io.changenow,$io.changenow.UChangeNow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.UChangeNow.setSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {function(!io.changenow.UChangeNow, !io.changenow.UChangeNow.SymbolsIn): void} */
io.changenow.UChangeNow.setSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.UChangeNow.getSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {function(!io.changenow.UChangeNow): !io.changenow.UChangeNow.SymbolsOut} */
io.changenow.UChangeNow.getSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.IChangeNow.SymbolsIn filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @record */
$io.changenow.IChangeNow.SymbolsIn = function() {}
/** @type {string|undefined} */
$io.changenow.IChangeNow.SymbolsIn.prototype._host
/** @type {string|undefined} */
$io.changenow.IChangeNow.SymbolsIn.prototype._apiPath
/** @type {string|undefined} */
$io.changenow.IChangeNow.SymbolsIn.prototype._changeNowApiKey
/** @typedef {$io.changenow.IChangeNow.SymbolsIn} */
io.changenow.IChangeNow.SymbolsIn

// nss:io.changenow,$io.changenow.IChangeNow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.IChangeNow.Symbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @record */
$io.changenow.IChangeNow.Symbols = function() {}
/** @type {symbol} */
$io.changenow.IChangeNow.Symbols.prototype._host
/** @type {symbol} */
$io.changenow.IChangeNow.Symbols.prototype._apiPath
/** @type {symbol} */
$io.changenow.IChangeNow.Symbols.prototype._changeNowApiKey
/** @typedef {$io.changenow.IChangeNow.Symbols} */
io.changenow.IChangeNow.Symbols

// nss:io.changenow,$io.changenow.IChangeNow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.IChangeNow.SymbolsOut filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @record */
$io.changenow.IChangeNow.SymbolsOut = function() {}
/** @type {string} */
$io.changenow.IChangeNow.SymbolsOut.prototype._host
/** @type {string} */
$io.changenow.IChangeNow.SymbolsOut.prototype._apiPath
/** @type {string} */
$io.changenow.IChangeNow.SymbolsOut.prototype._changeNowApiKey
/** @typedef {$io.changenow.IChangeNow.SymbolsOut} */
io.changenow.IChangeNow.SymbolsOut

// nss:io.changenow,$io.changenow.IChangeNow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.IChangeNow.setSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {function(!io.changenow.IChangeNow, !io.changenow.IChangeNow.SymbolsIn): void} */
io.changenow.IChangeNow.setSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/IChangeNow.xml} io.changenow.IChangeNow.getSymbols filter:Symbolism 08a4a29c943b7b92220ff943809b67aa */
/** @typedef {function(!io.changenow.IChangeNow): !io.changenow.IChangeNow.SymbolsOut} */
io.changenow.IChangeNow.getSymbols

// nss:io.changenow
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.AbstractLetsExchange.getSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {!io.letsexchange.ILetsExchange.getSymbols} */
io.letsexchange.AbstractLetsExchange.getSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.AbstractLetsExchange.setSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {!io.letsexchange.ILetsExchange.setSymbols} */
io.letsexchange.AbstractLetsExchange.setSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.AbstractLetsExchange.Symbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {!io.letsexchange.ILetsExchange.Symbols} */
io.letsexchange.AbstractLetsExchange.Symbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.AbstractLetsExchangeUniversal.getSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {!io.letsexchange.ULetsExchange.getSymbols} */
io.letsexchange.AbstractLetsExchangeUniversal.getSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.AbstractLetsExchangeUniversal.setSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {!io.letsexchange.ULetsExchange.setSymbols} */
io.letsexchange.AbstractLetsExchangeUniversal.setSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.AbstractLetsExchangeUniversal.Symbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {!io.letsexchange.ULetsExchange.Symbols} */
io.letsexchange.AbstractLetsExchangeUniversal.Symbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ULetsExchange.SymbolsIn filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @record */
$io.letsexchange.ULetsExchange.SymbolsIn = function() {}
/** @type {io.letsexchange.ILetsExchange|undefined} */
$io.letsexchange.ULetsExchange.SymbolsIn.prototype._letsExchange
/** @typedef {$io.letsexchange.ULetsExchange.SymbolsIn} */
io.letsexchange.ULetsExchange.SymbolsIn

// nss:io.letsexchange,$io.letsexchange.ULetsExchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ULetsExchange.Symbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @record */
$io.letsexchange.ULetsExchange.Symbols = function() {}
/** @type {symbol} */
$io.letsexchange.ULetsExchange.Symbols.prototype._letsExchange
/** @typedef {$io.letsexchange.ULetsExchange.Symbols} */
io.letsexchange.ULetsExchange.Symbols

// nss:io.letsexchange,$io.letsexchange.ULetsExchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ULetsExchange.SymbolsOut filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @record */
$io.letsexchange.ULetsExchange.SymbolsOut = function() {}
/** @type {io.letsexchange.ILetsExchange} */
$io.letsexchange.ULetsExchange.SymbolsOut.prototype._letsExchange
/** @typedef {$io.letsexchange.ULetsExchange.SymbolsOut} */
io.letsexchange.ULetsExchange.SymbolsOut

// nss:io.letsexchange,$io.letsexchange.ULetsExchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ULetsExchange.setSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {function(!io.letsexchange.ULetsExchange, !io.letsexchange.ULetsExchange.SymbolsIn): void} */
io.letsexchange.ULetsExchange.setSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ULetsExchange.getSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {function(!io.letsexchange.ULetsExchange): !io.letsexchange.ULetsExchange.SymbolsOut} */
io.letsexchange.ULetsExchange.getSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ILetsExchange.SymbolsIn filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @record */
$io.letsexchange.ILetsExchange.SymbolsIn = function() {}
/** @type {string|undefined} */
$io.letsexchange.ILetsExchange.SymbolsIn.prototype._host
/** @type {string|undefined} */
$io.letsexchange.ILetsExchange.SymbolsIn.prototype._apiPath
/** @type {string|undefined} */
$io.letsexchange.ILetsExchange.SymbolsIn.prototype._letsExchangeApiKey
/** @typedef {$io.letsexchange.ILetsExchange.SymbolsIn} */
io.letsexchange.ILetsExchange.SymbolsIn

// nss:io.letsexchange,$io.letsexchange.ILetsExchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ILetsExchange.Symbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @record */
$io.letsexchange.ILetsExchange.Symbols = function() {}
/** @type {symbol} */
$io.letsexchange.ILetsExchange.Symbols.prototype._host
/** @type {symbol} */
$io.letsexchange.ILetsExchange.Symbols.prototype._apiPath
/** @type {symbol} */
$io.letsexchange.ILetsExchange.Symbols.prototype._letsExchangeApiKey
/** @typedef {$io.letsexchange.ILetsExchange.Symbols} */
io.letsexchange.ILetsExchange.Symbols

// nss:io.letsexchange,$io.letsexchange.ILetsExchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ILetsExchange.SymbolsOut filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @record */
$io.letsexchange.ILetsExchange.SymbolsOut = function() {}
/** @type {string} */
$io.letsexchange.ILetsExchange.SymbolsOut.prototype._host
/** @type {string} */
$io.letsexchange.ILetsExchange.SymbolsOut.prototype._apiPath
/** @type {string} */
$io.letsexchange.ILetsExchange.SymbolsOut.prototype._letsExchangeApiKey
/** @typedef {$io.letsexchange.ILetsExchange.SymbolsOut} */
io.letsexchange.ILetsExchange.SymbolsOut

// nss:io.letsexchange,$io.letsexchange.ILetsExchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ILetsExchange.setSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {function(!io.letsexchange.ILetsExchange, !io.letsexchange.ILetsExchange.SymbolsIn): void} */
io.letsexchange.ILetsExchange.setSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ILetsExchange.xml} io.letsexchange.ILetsExchange.getSymbols filter:Symbolism b6c613ec44f366d48ae1872ae2830b0f */
/** @typedef {function(!io.letsexchange.ILetsExchange): !io.letsexchange.ILetsExchange.SymbolsOut} */
io.letsexchange.ILetsExchange.getSymbols

// nss:io.letsexchange
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.AbstractSwapeeAide.getSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {!xyz.swapee.ISwapeeAide.getSymbols} */
xyz.swapee.AbstractSwapeeAide.getSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.AbstractSwapeeAide.setSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {!xyz.swapee.ISwapeeAide.setSymbols} */
xyz.swapee.AbstractSwapeeAide.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.AbstractSwapeeAide.Symbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {!xyz.swapee.ISwapeeAide.Symbols} */
xyz.swapee.AbstractSwapeeAide.Symbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapeeAide.SymbolsIn filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @record */
$xyz.swapee.ISwapeeAide.SymbolsIn = function() {}
/** @type {string|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._output
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._showHelp
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._testExchanges
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._showVersion
/** @type {boolean|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._debug
/** @type {string|undefined} */
$xyz.swapee.ISwapeeAide.SymbolsIn.prototype._version
/** @typedef {$xyz.swapee.ISwapeeAide.SymbolsIn} */
xyz.swapee.ISwapeeAide.SymbolsIn

// nss:xyz.swapee,$xyz.swapee.ISwapeeAide
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapeeAide.Symbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @record */
$xyz.swapee.ISwapeeAide.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._output
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._showHelp
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._testExchanges
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._showVersion
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._debug
/** @type {symbol} */
$xyz.swapee.ISwapeeAide.Symbols.prototype._version
/** @typedef {$xyz.swapee.ISwapeeAide.Symbols} */
xyz.swapee.ISwapeeAide.Symbols

// nss:xyz.swapee,$xyz.swapee.ISwapeeAide
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapeeAide.SymbolsOut filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @record */
$xyz.swapee.ISwapeeAide.SymbolsOut = function() {}
/** @type {string} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._output
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._showHelp
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._testExchanges
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._showVersion
/** @type {boolean} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._debug
/** @type {string} */
$xyz.swapee.ISwapeeAide.SymbolsOut.prototype._version
/** @typedef {$xyz.swapee.ISwapeeAide.SymbolsOut} */
xyz.swapee.ISwapeeAide.SymbolsOut

// nss:xyz.swapee,$xyz.swapee.ISwapeeAide
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapeeAide.setSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {function(!xyz.swapee.ISwapeeAide, !xyz.swapee.ISwapeeAide.SymbolsIn): void} */
xyz.swapee.ISwapeeAide.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapeeAide.getSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {function(!xyz.swapee.ISwapeeAide): !xyz.swapee.ISwapeeAide.SymbolsOut} */
xyz.swapee.ISwapeeAide.getSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.AbstractSwapee.getSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {!xyz.swapee.ISwapee.getSymbols} */
xyz.swapee.AbstractSwapee.getSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.AbstractSwapee.setSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {!xyz.swapee.ISwapee.setSymbols} */
xyz.swapee.AbstractSwapee.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.AbstractSwapee.Symbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {!xyz.swapee.ISwapee.Symbols} */
xyz.swapee.AbstractSwapee.Symbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapee.SymbolsIn filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @record */
$xyz.swapee.ISwapee.SymbolsIn = function() {}
/** @type {string|undefined} */
$xyz.swapee.ISwapee.SymbolsIn.prototype._text
/** @typedef {$xyz.swapee.ISwapee.SymbolsIn} */
xyz.swapee.ISwapee.SymbolsIn

// nss:xyz.swapee,$xyz.swapee.ISwapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapee.Symbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @record */
$xyz.swapee.ISwapee.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.ISwapee.Symbols.prototype._text
/** @typedef {$xyz.swapee.ISwapee.Symbols} */
xyz.swapee.ISwapee.Symbols

// nss:xyz.swapee,$xyz.swapee.ISwapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapee.SymbolsOut filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @record */
$xyz.swapee.ISwapee.SymbolsOut = function() {}
/** @type {string} */
$xyz.swapee.ISwapee.SymbolsOut.prototype._text
/** @typedef {$xyz.swapee.ISwapee.SymbolsOut} */
xyz.swapee.ISwapee.SymbolsOut

// nss:xyz.swapee,$xyz.swapee.ISwapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapee.setSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {function(!xyz.swapee.ISwapee, !xyz.swapee.ISwapee.SymbolsIn): void} */
xyz.swapee.ISwapee.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapee.xml} xyz.swapee.ISwapee.getSymbols filter:Symbolism a70c940c8d06f95c695a9e9c7e908f52 */
/** @typedef {function(!xyz.swapee.ISwapee): !xyz.swapee.ISwapee.SymbolsOut} */
xyz.swapee.ISwapee.getSymbols

// nss:xyz.swapee
/* @typal-end */

/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnector.getSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {!xyz.swapee.IChangellyConnector.getSymbols} */
xyz.swapee.AbstractChangellyConnector.getSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnector.setSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {!xyz.swapee.IChangellyConnector.setSymbols} */
xyz.swapee.AbstractChangellyConnector.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnector.Symbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {!xyz.swapee.IChangellyConnector.Symbols} */
xyz.swapee.AbstractChangellyConnector.Symbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.SymbolsIn filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
$xyz.swapee.IChangellyConnector.SymbolsIn = function() {}
/** @type {((boolean|string))|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._cacheChangellyConnectResponse
/** @type {string|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorFrom
/** @type {string|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorTo
/** @type {number|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.SymbolsIn} */
xyz.swapee.IChangellyConnector.SymbolsIn

// nss:xyz.swapee,$xyz.swapee.IChangellyConnector
/* @typal-end */
/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.Symbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
$xyz.swapee.IChangellyConnector.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._cacheChangellyConnectResponse
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorFrom
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorTo
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.Symbols} */
xyz.swapee.IChangellyConnector.Symbols

// nss:xyz.swapee,$xyz.swapee.IChangellyConnector
/* @typal-end */
/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.SymbolsOut filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
$xyz.swapee.IChangellyConnector.SymbolsOut = function() {}
/** @type {(boolean|string)} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._cacheChangellyConnectResponse
/** @type {string} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorFrom
/** @type {string} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorTo
/** @type {number} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.SymbolsOut} */
xyz.swapee.IChangellyConnector.SymbolsOut

// nss:xyz.swapee,$xyz.swapee.IChangellyConnector
/* @typal-end */
/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.setSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(!xyz.swapee.IChangellyConnector, !xyz.swapee.IChangellyConnector.SymbolsIn): void} */
xyz.swapee.IChangellyConnector.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {server/types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.getSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(!xyz.swapee.IChangellyConnector): !xyz.swapee.IChangellyConnector.SymbolsOut} */
xyz.swapee.IChangellyConnector.getSymbols

// nss:xyz.swapee
/* @typal-end */