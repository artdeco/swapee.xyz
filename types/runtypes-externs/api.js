/* @typal-type {types/api.xml} xyz.swapee.swapee_xyz functions 8ebee15380ac6794a77d56f56e761637 */
/**
 * @param {!xyz.swapee.swapee_xyz.Config} config
 * @return {!Promise<string>}
 */
$xyz.swapee.swapee_xyz = function(config) {}
/** @typedef {typeof $xyz.swapee.swapee_xyz} */
xyz.swapee.swapee_xyz

// nss:xyz.swapee,$xyz.swapee
/* @typal-end */