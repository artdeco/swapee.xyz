import ControllerPlugin from '@mauriceguest/controller-plugin'

export const Config=process.env.SERVER?{}:{
 abstractObjects:true,
 exportAbstractObjectsConstructorByName(name) {
  if(name.endsWith('OuterCoreConstructor')) {
   return true
  }
  if(name.endsWith('RAMConstructor')) return true
 },
 webComponentsPath:'generated/web-components',
}

export { ControllerPlugin }