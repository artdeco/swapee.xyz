// import RpcGateway from './aop/HyperRpcGateway'
import {RpcGatewayFactory} from '@webcircuits/rpc-gateway'
import {ChangellyExchangePage} from 'xyz/swapee/rc/changelly-exchange/dev'
import {FileTreePage} from 'xyz/swapee/rc/file-tree/dev'
import {FileEditorPage} from 'xyz/swapee/rc/file-editor/dev'
import {OffersAggregatorPage} from 'xyz/swapee/rc/offers-aggregator/dev'
import ChangellyExchangePageAspectsInstaller from '../../../generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/gen/aspects-installers/ChangellyExchangePageAspectsInstaller'
import FileEditorPageAspectsInstaller from '../../../generated/generated/maurice/circuits/file-editor/file-editor.ws/file-editor/FileEditor/gen/aspects-installers/FileEditorPageAspectsInstaller/FileEditorPageAspectsInstaller'
import FileTreePageAspectsInstaller from '../../../generated/generated/maurice/circuits/file-tree/file-tree.ws/file-tree/FileTree/gen/aspects-installers/FileTreePageAspectsInstaller'
import OffersAggregatorPageAspectsInstaller from '../../../generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/gen/aspects-installers/OffersAggregatorPageAspectsInstaller'
import {ChangellyExchangeMemoryQPs} from '../../../generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/pqs/ChangellyExchangeMemoryQPs'
import {ExchangeBrokerMemoryQPs} from '../../../generated/maurice/circuits/exchange-broker/ExchangeBroker.mvc/pqs/ExchangeBrokerMemoryQPs'
import {FileEditorMemoryQPs} from '../../../generated/maurice/circuits/file-editor/FileEditor.mvc/pqs/FileEditorMemoryQPs'
import {FileTreeMemoryQPs} from '../../../generated/maurice/circuits/file-tree/FileTree.mvc/pqs/FileTreeMemoryQPs'
import {OfferExchangeMemoryQPs} from '../../../generated/maurice/circuits/offer-exchange/OfferExchange.mvc/pqs/OfferExchangeMemoryQPs'
import {OffersAggregatorMemoryQPs} from '../../../generated/maurice/circuits/offers-aggregator/OffersAggregator.mvc/pqs/OffersAggregatorMemoryQPs'
import SwapeeRpcGatewayGeneralAspects from '../swapee-rpc/SwapeeRpcGateway/aop/SwapeeRpcGatewayGeneralAspects'
import {TransactionInfoMemoryQPs} from '../../../generated/maurice/circuits/transaction-info/TransactionInfo.mvc/pqs/TransactionInfoMemoryQPs'

// can use md5 pow package from webcircuits/ui?

/** @extends {com.webcircuits.RpcGateway} */
export default class RpcGateway extends RpcGatewayFactory({
 PQs:[
  OffersAggregatorMemoryQPs,
  ChangellyExchangeMemoryQPs,
  OfferExchangeMemoryQPs,
  ExchangeBrokerMemoryQPs,
  FileTreeMemoryQPs,
  FileEditorMemoryQPs,
  TransactionInfoMemoryQPs,
 ],
 pages:[
  OffersAggregatorPage,
  ChangellyExchangePage,
  FileTreePage,
  FileEditorPage,
 ],
 aspectsInstallers:[
  ChangellyExchangePageAspectsInstaller,
  OffersAggregatorPageAspectsInstaller,
  FileTreePageAspectsInstaller,
  FileEditorPageAspectsInstaller,
 ],
 preforwardingAspects:[
  SwapeeRpcGatewayGeneralAspects,
 ],
}) {}

// export default RpcGateway