import '../../../../types'

/** @abstract {xyz.swapee.ws.rpc.ISwapeeRpcGateway} @autoinit */
export default class AbstractSwapeeRpcGateway {}