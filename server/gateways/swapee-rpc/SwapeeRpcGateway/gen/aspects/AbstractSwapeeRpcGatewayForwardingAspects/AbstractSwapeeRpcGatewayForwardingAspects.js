import AbstractSwapeeRpcGatewayAspects from '../AbstractSwapeeRpcGatewayAspects'

/** @extends {xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayForwardingAspects} ‎*/
export default class AbstractSwapeeRpcGatewayForwardingAspects extends /**@type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayForwardingAspects}*/(AbstractSwapeeRpcGatewayAspects.clone()) {}

/** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayForwardingAspects} */
AbstractSwapeeRpcGatewayForwardingAspects.class=function(){}