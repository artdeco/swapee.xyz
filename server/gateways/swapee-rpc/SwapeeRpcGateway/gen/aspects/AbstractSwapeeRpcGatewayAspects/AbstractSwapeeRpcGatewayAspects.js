import { newAspects } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects}
 */
class _AbstractSwapeeRpcGatewayAspects { }
/**
 * The aspects of the *ISwapeeRpcGateway*.
 * @extends {xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects} ‎
 */
class AbstractSwapeeRpcGatewayAspects extends newAspects(
 _AbstractSwapeeRpcGatewayAspects,478904818833,null,{},false) {}

/** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects} */
AbstractSwapeeRpcGatewayAspects.class=function(){}
/** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects} */
function AbstractSwapeeRpcGatewayAspectsClass(){}

export default AbstractSwapeeRpcGatewayAspects