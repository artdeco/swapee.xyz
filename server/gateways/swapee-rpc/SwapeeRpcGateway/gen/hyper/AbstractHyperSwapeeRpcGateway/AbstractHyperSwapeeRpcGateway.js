import SwapeeRpcGatewayAspectsInstaller from '../../aspects-installers/SwapeeRpcGatewayAspectsInstaller'
import { newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway}
 */
class _AbstractHyperSwapeeRpcGateway {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperSwapeeRpcGateway
    .clone({aspectsInstaller:SwapeeRpcGatewayAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway} ‎ */
class AbstractHyperSwapeeRpcGateway extends newAbstract(
 _AbstractHyperSwapeeRpcGateway,478904818834,null,{
  asIHyperSwapeeRpcGateway:1,
  superHyperSwapeeRpcGateway:2,
 },false) {}

/** @type {typeof xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway} */
AbstractHyperSwapeeRpcGateway.class=function(){}
/** @type {typeof xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway} */
function AbstractHyperSwapeeRpcGatewayClass(){}

export default AbstractHyperSwapeeRpcGateway