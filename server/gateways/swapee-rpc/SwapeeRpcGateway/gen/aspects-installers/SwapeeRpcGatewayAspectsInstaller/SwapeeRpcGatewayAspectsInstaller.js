import { $advice, newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller}
 */
class _SwapeeRpcGatewayAspectsInstaller { }

_SwapeeRpcGatewayAspectsInstaller.prototype[$advice]=true

/** @extends {xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller} ‎ */
class SwapeeRpcGatewayAspectsInstaller extends newAbstract(
 _SwapeeRpcGatewayAspectsInstaller,478904818830,null,{},false) {}

/** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller} */
SwapeeRpcGatewayAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller} */
function SwapeeRpcGatewayAspectsInstallerClass(){}

export default SwapeeRpcGatewayAspectsInstaller