import { newAbstract } from '@type.engineering/type-engineer'
import '../../../../types'

/**
 * @abstract
 * @extends {xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway}
 */
class _AbstractSwapeeRpcGateway { }
/**
 * The Swapee Rpc gateway.
 * @extends {xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway} ‎
 */
class AbstractSwapeeRpcGateway extends newAbstract(
 _AbstractSwapeeRpcGateway,478904818835,null,{
  asISwapeeRpcGateway:1,
  superSwapeeRpcGateway:2,
 },false) {}

/** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway} */
AbstractSwapeeRpcGateway.class=function(){}
/** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway} */
function AbstractSwapeeRpcGatewayClass(){}

export default AbstractSwapeeRpcGateway