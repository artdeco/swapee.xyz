/** @nocompile */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel.BeforeListSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel._beforeListSettings */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel.AfterListSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel._afterListSettings */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel.AfterThrowsListSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel._afterListSettingsThrows */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel.AfterReturnsListSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel._afterListSettingsReturns */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel.ImmediatelyAfterListSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageActionsJoinpointModel._immediatelyAfterListSettings */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageActions, form: !ru.beinteam.ws.default.ISettingsPageActions.listSettings.Form) => !Promise<void>} ru.beinteam.ws.default.ISettingsPageActions._listSettings */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.BeforeForwardContextPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._beforeForwardContext */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterForwardContextPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterForwardContext */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterThrowsForwardContextPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterForwardContextThrows */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterReturnsForwardContextPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterForwardContextReturns */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.ImmediatelyAfterForwardContextPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._immediatelyAfterForwardContext */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.BeforeGetSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._beforeGetSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterGetSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterGetSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterThrowsGetSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterGetSettingsPageThrows */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterReturnsGetSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterGetSettingsPageReturns */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.ImmediatelyAfterGetSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._immediatelyAfterGetSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.BeforeQuerySettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._beforeQuerySettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterQuerySettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterQuerySettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterThrowsQuerySettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterQuerySettingsPageThrows */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterReturnsQuerySettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterQuerySettingsPageReturns */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.BeforePutSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._beforePutSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterPutSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterPutSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterThrowsPutSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterPutSettingsPageThrows */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterReturnsPutSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterPutSettingsPageReturns */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.ImmediatelyAfterPutSettingsPagePointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._immediatelyAfterPutSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.BeforePostSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._beforePostSettings */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterPostSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterPostSettings */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterThrowsPostSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterPostSettingsThrows */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.AfterReturnsPostSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._afterPostSettingsReturns */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGateJoinpointModel, data?: !ru.beinteam.ws.default.ISettingsPageGateJoinpointModel.ImmediatelyAfterPostSettingsPointcutData) => void} ru.beinteam.ws.default.ISettingsPageGateJoinpointModel._immediatelyAfterPostSettings */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGate) => !Promise} ru.beinteam.ws.default.ISettingsPageGate._forwardContext */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGate, form: !ru.beinteam.ws.default.ISettingsPageGate.Form) => !Promise<!Array<JSX>>} ru.beinteam.ws.default.ISettingsPageGate._getSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGate, query: !ru.beinteam.ws.default.ISettingsPageGate.querySettingsPage.Query) => ru.beinteam.ws.default.ISettingsPageGate.putSettingsPage.Form} ru.beinteam.ws.default.ISettingsPageGate._querySettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGate, form?: !ru.beinteam.ws.default.ISettingsPageGate.putSettingsPage.Form) => !Promise} ru.beinteam.ws.default.ISettingsPageGate._putSettingsPage */

/** @typedef {(this: ru.beinteam.ws.default.ISettingsPageGate, form?: !ru.beinteam.ws.default.ISettingsPageGate.postSettings.Form) => !Promise<ru.beinteam.ws.default.ISettingsPageGate.putSettings.Form>} ru.beinteam.ws.default.ISettingsPageGate._postSettings */
