/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.ws={}
xyz.swapee.ws.rpc={}
xyz.swapee.ws.rpc.ISwapeeRpcGateway={}
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller={}
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects={}
xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway={}`)

/** */
var __$te_plain={}

/* @typal-start {gateways/swapee-rpc/SwapeeRpcGateway/design/ISwapeeRpcGateway.xml}  193dc0a55e0861ef9da8a54908cfe31e */
/** @typedef {com.websystems.IRpcGateway.Initialese} xyz.swapee.ws.rpc.ISwapeeRpcGateway.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.ws.rpc.SwapeeRpcGateway)} xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.SwapeeRpcGateway} xyz.swapee.ws.rpc.SwapeeRpcGateway.typeof */
/**
 * An abstract class of `xyz.swapee.ws.rpc.ISwapeeRpcGateway` interface.
 * @constructor xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway = class extends /** @type {xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.constructor&xyz.swapee.ws.rpc.SwapeeRpcGateway.typeof} */ (class {}) { }
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.prototype.constructor = xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.class = /** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.ws.rpc.ISwapeeRpcGateway|typeof xyz.swapee.ws.rpc.SwapeeRpcGateway)|(!com.websystems.IRpcGateway|typeof com.websystems.RpcGateway)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGateway}
 * @nosideeffects
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.ws.rpc.ISwapeeRpcGateway|typeof xyz.swapee.ws.rpc.SwapeeRpcGateway)|(!com.websystems.IRpcGateway|typeof com.websystems.RpcGateway)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.ws.rpc.ISwapeeRpcGateway|typeof xyz.swapee.ws.rpc.SwapeeRpcGateway)|(!com.websystems.IRpcGateway|typeof com.websystems.RpcGateway)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGateway.__trait = function(...Implementations) {}

/**
 * An interface that enumerates the joinpoints of `ISwapeeRpcGateway`'s methods.
 * @interface xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel = class { }
xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel.prototype.constructor = xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel

/**
 * A concrete class of _ISwapeeRpcGatewayJoinpointModel_ instances.
 * @constructor xyz.swapee.ws.rpc.SwapeeRpcGatewayJoinpointModel
 * @implements {xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel} An interface that enumerates the joinpoints of `ISwapeeRpcGateway`'s methods.
 */
xyz.swapee.ws.rpc.SwapeeRpcGatewayJoinpointModel = class extends xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel { }
xyz.swapee.ws.rpc.SwapeeRpcGatewayJoinpointModel.prototype.constructor = xyz.swapee.ws.rpc.SwapeeRpcGatewayJoinpointModel

/** @typedef {xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel} */
xyz.swapee.ws.rpc.RecordISwapeeRpcGatewayJoinpointModel

/** @typedef {xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel} xyz.swapee.ws.rpc.BoundISwapeeRpcGatewayJoinpointModel */

/** @typedef {xyz.swapee.ws.rpc.SwapeeRpcGatewayJoinpointModel} xyz.swapee.ws.rpc.BoundSwapeeRpcGatewayJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller)} xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller} xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller` interface.
 * @constructor xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller = class extends /** @type {xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.constructor&xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.prototype.constructor = xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.class = /** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller|typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller|typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller|typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.Initialese[]) => xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller} xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller = class extends /** @type {xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeRpcGatewayAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.Initialese>)} xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller} xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeRpcGatewayAspectsInstaller_ instances.
 * @constructor xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller
 * @implements {xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller = class extends /** @type {xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller.constructor&xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeRpcGatewayAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeRpcGatewayAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller}
 */
xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsInstaller.__extend = function(...Extensions) {}

/** @typedef {function(new: xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspectsCaster<THIS>)} xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects.constructor */
/**
 * The aspects of the *ISwapeeRpcGateway* that bind to an instance.
 * @interface xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects
 * @template THIS
 */
xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects = class extends /** @type {xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects.constructor} */ (class {}) { }
xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects.prototype.constructor = xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects

/** @typedef {typeof __$te_plain} xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects)} xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects} xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects.typeof */
/**
 * An abstract class of `xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects` interface.
 * @constructor xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects = class extends /** @type {xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.constructor&xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects.typeof} */ (class {}) { }
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.prototype.constructor = xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.class = /** @type {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects|typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects)|(!xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects|typeof xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects}
 * @nosideeffects
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects|typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects)|(!xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects|typeof xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects|typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects)|(!xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects|typeof xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects}
 */
xyz.swapee.ws.rpc.AbstractSwapeeRpcGatewayAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.Initialese[]) => xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects} xyz.swapee.ws.rpc.SwapeeRpcGatewayAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsCaster&xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects<!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects>)} xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects} xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects.typeof */
/**
 * The aspects of the *ISwapeeRpcGateway*.
 * @interface xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects = class extends /** @type {xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeRpcGatewayAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.Initialese} init The initialisation options.
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects&engineering.type.IInitialiser<!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.Initialese>)} xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects} xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.typeof */
/**
 * A concrete class of _ISwapeeRpcGatewayAspects_ instances.
 * @constructor xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects
 * @implements {xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects} The aspects of the *ISwapeeRpcGateway*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.Initialese>} ‎
 */
xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects = class extends /** @type {xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects.constructor&xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeRpcGatewayAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeRpcGatewayAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects.Initialese} init The initialisation options.
 */
xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects}
 */
xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BSwapeeRpcGatewayAspects_ interface.
 * @interface xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspectsCaster
 * @template THIS
 */
xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspectsCaster = class { }
/**
 * Cast the _BSwapeeRpcGatewayAspects_ instance into the _BoundISwapeeRpcGateway_ type.
 * @type {!xyz.swapee.ws.rpc.BoundISwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspectsCaster.prototype.asISwapeeRpcGateway

/**
 * Contains getters to cast the _ISwapeeRpcGatewayAspects_ interface.
 * @interface xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsCaster
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsCaster = class { }
/**
 * Cast the _ISwapeeRpcGatewayAspects_ instance into the _BoundISwapeeRpcGateway_ type.
 * @type {!xyz.swapee.ws.rpc.BoundISwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsCaster.prototype.asISwapeeRpcGateway

/** @typedef {xyz.swapee.ws.rpc.ISwapeeRpcGateway.Initialese} xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.ws.rpc.HyperSwapeeRpcGateway)} xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway} xyz.swapee.ws.rpc.HyperSwapeeRpcGateway.typeof */
/**
 * An abstract class of `xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway` interface.
 * @constructor xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway = class extends /** @type {xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.constructor&xyz.swapee.ws.rpc.HyperSwapeeRpcGateway.typeof} */ (class {}) { }
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.prototype.constructor = xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.class = /** @type {typeof xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway|typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway)|(!xyz.swapee.ws.rpc.ISwapeeRpcGateway|typeof xyz.swapee.ws.rpc.SwapeeRpcGateway)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway}
 * @nosideeffects
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway|typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway)|(!xyz.swapee.ws.rpc.ISwapeeRpcGateway|typeof xyz.swapee.ws.rpc.SwapeeRpcGateway)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway|typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway)|(!xyz.swapee.ws.rpc.ISwapeeRpcGateway|typeof xyz.swapee.ws.rpc.SwapeeRpcGateway)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects|function(new: xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects)} aides The list of aides that advise the ISwapeeRpcGateway to implement aspects.
 * @return {typeof xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway}
 * @nosideeffects
 */
xyz.swapee.ws.rpc.AbstractHyperSwapeeRpcGateway.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.Initialese[]) => xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway} xyz.swapee.ws.rpc.HyperSwapeeRpcGatewayConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.ws.rpc.IHyperSwapeeRpcGatewayCaster&xyz.swapee.ws.rpc.ISwapeeRpcGateway)} xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.ISwapeeRpcGateway} xyz.swapee.ws.rpc.ISwapeeRpcGateway.typeof */
/** @interface xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway */
xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway = class extends /** @type {xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.constructor&engineering.type.IEngineer.typeof&xyz.swapee.ws.rpc.ISwapeeRpcGateway.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperSwapeeRpcGateway* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.Initialese} init The initialisation options.
 */
xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway&engineering.type.IInitialiser<!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.Initialese>)} xyz.swapee.ws.rpc.HyperSwapeeRpcGateway.constructor */
/** @typedef {typeof xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway} xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.typeof */
/**
 * A concrete class of _IHyperSwapeeRpcGateway_ instances.
 * @constructor xyz.swapee.ws.rpc.HyperSwapeeRpcGateway
 * @implements {xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.Initialese>} ‎
 */
xyz.swapee.ws.rpc.HyperSwapeeRpcGateway = class extends /** @type {xyz.swapee.ws.rpc.HyperSwapeeRpcGateway.constructor&xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperSwapeeRpcGateway* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperSwapeeRpcGateway* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway.Initialese} init The initialisation options.
 */
xyz.swapee.ws.rpc.HyperSwapeeRpcGateway.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ws.rpc.HyperSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.HyperSwapeeRpcGateway.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway} */
xyz.swapee.ws.rpc.RecordIHyperSwapeeRpcGateway

/** @typedef {xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway} xyz.swapee.ws.rpc.BoundIHyperSwapeeRpcGateway */

/** @typedef {xyz.swapee.ws.rpc.HyperSwapeeRpcGateway} xyz.swapee.ws.rpc.BoundHyperSwapeeRpcGateway */

/**
 * Contains getters to cast the _IHyperSwapeeRpcGateway_ interface.
 * @interface xyz.swapee.ws.rpc.IHyperSwapeeRpcGatewayCaster
 */
xyz.swapee.ws.rpc.IHyperSwapeeRpcGatewayCaster = class { }
/**
 * Cast the _IHyperSwapeeRpcGateway_ instance into the _BoundIHyperSwapeeRpcGateway_ type.
 * @type {!xyz.swapee.ws.rpc.BoundIHyperSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.IHyperSwapeeRpcGatewayCaster.prototype.asIHyperSwapeeRpcGateway
/**
 * Access the _HyperSwapeeRpcGateway_ prototype.
 * @type {!xyz.swapee.ws.rpc.BoundHyperSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.IHyperSwapeeRpcGatewayCaster.prototype.superHyperSwapeeRpcGateway

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.ws.rpc.ISwapeeRpcGatewayCaster&com.websystems.IRpcGateway)} xyz.swapee.ws.rpc.ISwapeeRpcGateway.constructor */
/** @typedef {typeof com.websystems.IRpcGateway} com.websystems.IRpcGateway.typeof */
/**
 * The Swapee Rpc gateway.
 * @interface xyz.swapee.ws.rpc.ISwapeeRpcGateway
 */
xyz.swapee.ws.rpc.ISwapeeRpcGateway = class extends /** @type {xyz.swapee.ws.rpc.ISwapeeRpcGateway.constructor&engineering.type.IEngineer.typeof&com.websystems.IRpcGateway.typeof} */ (class {}) { }
xyz.swapee.ws.rpc.ISwapeeRpcGateway.prototype.constructor = xyz.swapee.ws.rpc.ISwapeeRpcGateway

/** @typedef {function(new: xyz.swapee.ws.rpc.ISwapeeRpcGateway&engineering.type.IInitialiser<!xyz.swapee.ws.rpc.ISwapeeRpcGateway.Initialese>)} xyz.swapee.ws.rpc.SwapeeRpcGateway.constructor */
/**
 * A concrete class of _ISwapeeRpcGateway_ instances.
 * @constructor xyz.swapee.ws.rpc.SwapeeRpcGateway
 * @implements {xyz.swapee.ws.rpc.ISwapeeRpcGateway} The Swapee Rpc gateway.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ws.rpc.ISwapeeRpcGateway.Initialese>} ‎
 */
xyz.swapee.ws.rpc.SwapeeRpcGateway = class extends /** @type {xyz.swapee.ws.rpc.SwapeeRpcGateway.constructor&xyz.swapee.ws.rpc.ISwapeeRpcGateway.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.ws.rpc.SwapeeRpcGateway.prototype.constructor = xyz.swapee.ws.rpc.SwapeeRpcGateway
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ws.rpc.SwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.SwapeeRpcGateway.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.ws.rpc.ISwapeeRpcGateway} */
xyz.swapee.ws.rpc.RecordISwapeeRpcGateway

/** @typedef {xyz.swapee.ws.rpc.ISwapeeRpcGateway} xyz.swapee.ws.rpc.BoundISwapeeRpcGateway */

/** @typedef {xyz.swapee.ws.rpc.SwapeeRpcGateway} xyz.swapee.ws.rpc.BoundSwapeeRpcGateway */

/**
 * Contains getters to cast the _ISwapeeRpcGateway_ interface.
 * @interface xyz.swapee.ws.rpc.ISwapeeRpcGatewayCaster
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayCaster = class { }
/**
 * Cast the _ISwapeeRpcGateway_ instance into the _BoundISwapeeRpcGateway_ type.
 * @type {!xyz.swapee.ws.rpc.BoundISwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayCaster.prototype.asISwapeeRpcGateway
/**
 * Access the _SwapeeRpcGateway_ prototype.
 * @type {!xyz.swapee.ws.rpc.BoundSwapeeRpcGateway}
 */
xyz.swapee.ws.rpc.ISwapeeRpcGatewayCaster.prototype.superSwapeeRpcGateway

// nss:xyz.swapee.ws.rpc
/* @typal-end */