import AbstractSwapeeRpcGatewayAspects from '../../gen/aspects/AbstractSwapeeRpcGatewayAspects'
import {CorsAide} from '@idio2/server'

let i=0

/** @extends {xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects} */
export default class extends AbstractSwapeeRpcGatewayAspects.continues(
 AbstractSwapeeRpcGatewayAspects.class.prototype=/**@type {!xyz.swapee.ws.rpc.SwapeeRpcGatewayAspects}*/({
  beforeForward:[
   CorsAide.CORS({
    // what about multiple origins
    origin:[process.env.ORIGIN,'http://localhost:3000'],
   }),
  ],
 }),
 AbstractSwapeeRpcGatewayAspects.continues({
  async beforeFilterChangellyFloatingOffer() {
   await new Promise((r)=>setTimeout(r,1000))
  },
  async beforeFilterChangellyFixedOffer() {
   await new Promise((r)=>setTimeout(r,1000))
  },
 }),
 AbstractSwapeeRpcGatewayAspects.continues({
  async beforeFilterCheckPayment({args:args,cancel:cancel}) {
   return
   if(args.form.id!='chngl-7vgm07oqf7q44spu') return
   await new Promise((r)=>setTimeout(r,3000))
   // console.log('checking payment')
   let status='waiting'
   if(i==1) status='confirming'
   if(i==2) status='exchanging'
   if(i==3) status='sending'
   if(i==4) status='finished'
   // status='waiting'
   i++
   if(i==5){
    i=0
   }

   args.ctx.body={
    "data": {
     '9acb4':status,
     // "9acb4": "waiting",
     // "9acb4": "confirming",
     // "9acb4": "exchanging",
     // "9acb4": "sending",
     // "9acb4": "finished",
     // "9acb4": "refunded",
     // "9acb4": "overdue",
     // "9acb4": "hold",
     // "9acb4": "failed",
    },
   }
   cancel()
  },
  async beforeFilterCreateTransaction1({args:args,cancel:cancel,sub:sub}){
   // return
   // if(args.form.address!='0x76c5c26ddd5986a4cb3c8854b11fc500ad5f7fe0') return
   await new Promise((r)=>setTimeout(r,100))
   const{
    // address:address,
    // amountFrom:amountFrom,
    // createTransaction:createTransaction    ,
    // currencyFrom:currencyFrom    ,
    // currencyTo:currencyTo    ,
    // refundAddress:refundAddress ,   // cahge the thing
   }=args.form
   // return
   // debugger
   args.ctx.body={
    "data": {
     "b80bb": "chngl-7vgm07oqf7q44spu",
     "97def": "2024-02-14T11:13:15.000Z",
     "59af8": "bc1qwrzl9zzj42a7l6dru06dtyypvdcppwyeuvp775",
     "7a19b": null,
     "9acb4": "new",
     "2b803": false,
     "6103f": "0.1",
     "86cc4": "1.8561725",
    },
   }
   // id:'b80bb',
   // createTransactionError:'601f8',
   // createdAt:'97def',
   // payinAddress:'59af8',
   // payinExtraId:'7a19b',
   cancel()
   // sub({
   //  id: 'i3e2vl7pzkroh389',
   //  createdAt: '2024-01-22T23:16:33.000Z',
   //  payinAddress: 'bc1qfpu8r022yqz2q75a623g22hzewpc4tvpudmn9e'
   // })
  },
 }),
) {}