import SwapeeRpcGateway from '../../SwapeeRpcGateway'
import AbstractHyperSwapeeRpcGateway from '../../gen/hyper/AbstractHyperSwapeeRpcGateway'
import SwapeeRpcGatewayGeneralAspects from '../SwapeeRpcGatewayGeneralAspects'

/** @extends {xyz.swapee.ws.rpc.HyperSwapeeRpcGateway} */
export default class extends AbstractHyperSwapeeRpcGateway
 .consults(
  SwapeeRpcGatewayGeneralAspects,
 )
 .implements(
  SwapeeRpcGateway,
  // AbstractHyperSwapeeRpcGateway.class.prototype=/**@type {!HyperSwapeeRpcGateway}*/({
  // }),
 )
{}