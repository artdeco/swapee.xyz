export default [
 1, // SwapeeRpcGatewayAspectsInstaller
 2, // SwapeeRpcGateway
 3, // AbstractSwapeeRpcGatewayAspects
 4, // SwapeeRpcGatewayAspects
 5, // SwapeeRpcGatewayForwardingAspects
 6, // AbstractHyperSwapeeRpcGateway
 7, // HyperSwapeeRpcGateway
 [
  '8_11_14_16_18_20',
  [
   8, // HyperRpcGateway
   11, // SwapeeServerAspects
   14, // Swapee_Server
   16, // ChangellyConnector
   18, // ChangellyConnectorAspects
   20, // HyperChangellyConnector
  ],
 ],
 9, // RpcGatewayLoggingAide
 10, // AbstractSwapeeServerAspects
 12, // AbstractHyperSwapeeServer
 13, // HyperSwapeeServer
 15, // ChangellyConnectorAspectsInstaller
 17, // AbstractChangellyConnectorAspects
 19, // AbstractHyperChangellyConnector
 21, // SwapeeServerAspectsInstaller
]