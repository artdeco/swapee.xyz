import SwapeeServerAspectsInstaller from '../../aspects-installers/SwapeeServerAspectsInstaller'
import { newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractHyperSwapeeServer}
 */
class _AbstractHyperSwapeeServer {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperSwapeeServer
    .clone({aspectsInstaller:SwapeeServerAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.AbstractHyperSwapeeServer} ‎ */
class AbstractHyperSwapeeServer extends newAbstract(
 _AbstractHyperSwapeeServer,47890481885,null,{
  asIHyperSwapeeServer:1,
  superHyperSwapeeServer:2,
 },false) {}

/** @type {typeof xyz.swapee.AbstractHyperSwapeeServer} */
AbstractHyperSwapeeServer.class=function(){}
/** @type {typeof xyz.swapee.AbstractHyperSwapeeServer} */
function AbstractHyperSwapeeServerClass(){}

export default AbstractHyperSwapeeServer