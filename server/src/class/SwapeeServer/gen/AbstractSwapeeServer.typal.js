import '../../../../types'

/** @abstract {xyz.swapee.ISwapeeServer} @autoinit */
export default class AbstractSwapeeServer {}