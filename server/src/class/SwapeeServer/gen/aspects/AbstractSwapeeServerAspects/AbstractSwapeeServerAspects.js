import { newAspects } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapeeServerAspects}
 */
class _AbstractSwapeeServerAspects { }
/**
 * The aspects of the *ISwapeeServer*.
 * @extends {xyz.swapee.AbstractSwapeeServerAspects} ‎
 */
class AbstractSwapeeServerAspects extends newAspects(
 _AbstractSwapeeServerAspects,47890481884,null,{},false) {}

/** @type {typeof xyz.swapee.AbstractSwapeeServerAspects} */
AbstractSwapeeServerAspects.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapeeServerAspects} */
function AbstractSwapeeServerAspectsClass(){}

export default AbstractSwapeeServerAspects