import { newAbstract } from '@type.engineering/type-engineer'
import '../../../../types'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapeeServer}
 */
class _AbstractSwapeeServer { }
/**
 * The server.
 * @extends {xyz.swapee.AbstractSwapeeServer} ‎
 */
class AbstractSwapeeServer extends newAbstract(
 _AbstractSwapeeServer,47890481886,null,{
  asISwapeeServer:1,
  superSwapeeServer:2,
 },false) {}

/** @type {typeof xyz.swapee.AbstractSwapeeServer} */
AbstractSwapeeServer.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapeeServer} */
function AbstractSwapeeServerClass(){}

export default AbstractSwapeeServer