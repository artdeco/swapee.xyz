import { $advice, newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapeeServerAspectsInstaller}
 */
class _SwapeeServerAspectsInstaller { }

_SwapeeServerAspectsInstaller.prototype[$advice]=true

/** @extends {xyz.swapee.AbstractSwapeeServerAspectsInstaller} ‎ */
class SwapeeServerAspectsInstaller extends newAbstract(
 _SwapeeServerAspectsInstaller,47890481882,null,{},false) {}

/** @type {typeof xyz.swapee.AbstractSwapeeServerAspectsInstaller} */
SwapeeServerAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapeeServerAspectsInstaller} */
function SwapeeServerAspectsInstallerClass(){}

export default SwapeeServerAspectsInstaller