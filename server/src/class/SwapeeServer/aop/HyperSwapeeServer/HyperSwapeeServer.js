import SwapeeServer from '../../SwapeeServer'
import AbstractHyperSwapeeServer from '../../gen/hyper/AbstractHyperSwapeeServer'
import SwapeeServerGeneralAspects from '../SwapeeServerGeneralAspects'

/** @extends {xyz.swapee.HyperSwapeeServer} */
export default class extends AbstractHyperSwapeeServer
 .consults(
  SwapeeServerGeneralAspects,
 )
 .implements(
  SwapeeServer,
  // AbstractHyperSwapeeServer.class.prototype=/**@type {!HyperSwapeeServer}*/({
  // }),
 )
{}