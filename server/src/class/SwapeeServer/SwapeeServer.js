import {Server} from '@idio2/server'
import {JwtUniversal} from '@idio2/jwt'
import ChangellyConnector from '../ChangellyConnector'
import AbstractSwapeeServer from './gen/AbstractSwapeeServer'

/** @extends {xyz.swapee.SwapeeServer} */
export default class extends AbstractSwapeeServer.implements(
 Server,
 JwtUniversal,
 ChangellyConnector,
 AbstractSwapeeServer.class.prototype=/**@type {!xyz.swapee.SwapeeServer} */({
  constructor() {},
  // method:_method,
 }),
) {}

// todo: add changelly connector