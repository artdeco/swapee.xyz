import { newAspects } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractChangellyConnectorAspects}
 */
class _AbstractChangellyConnectorAspects { }
/**
 * The aspects of the *IChangellyConnector*.
 * @extends {xyz.swapee.AbstractChangellyConnectorAspects} ‎
 */
class AbstractChangellyConnectorAspects extends newAspects(
 _AbstractChangellyConnectorAspects,478904818824,null,{},false) {}

/** @type {typeof xyz.swapee.AbstractChangellyConnectorAspects} */
AbstractChangellyConnectorAspects.class=function(){}
/** @type {typeof xyz.swapee.AbstractChangellyConnectorAspects} */
function AbstractChangellyConnectorAspectsClass(){}

export default AbstractChangellyConnectorAspects