import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractChangellyConnectorAspectsInstaller}
 */
class _ChangellyConnectorAspectsInstaller { }

_ChangellyConnectorAspectsInstaller.prototype[$advice]=true

/** @extends {xyz.swapee.AbstractChangellyConnectorAspectsInstaller} ‎ */
class ChangellyConnectorAspectsInstaller extends newAbstract(
 _ChangellyConnectorAspectsInstaller,478904818822,null,{},false) {}

/** @type {typeof xyz.swapee.AbstractChangellyConnectorAspectsInstaller} */
ChangellyConnectorAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.AbstractChangellyConnectorAspectsInstaller} */
function ChangellyConnectorAspectsInstallerClass(){}

export default ChangellyConnectorAspectsInstaller


ChangellyConnectorAspectsInstaller[$implementations]=[
 ChangellyConnectorAspectsInstallerClass.prototype=/**@type {!xyz.swapee.IChangellyConnectorAspectsInstaller}*/({
  ping(){
   this.beforePing=1
   this.afterPing=2
   this.aroundPing=3
   this.afterPingThrows=4
   this.afterPingReturns=5
   this.immediatelyAfterPing=6
   this.afterPingCancels=7
  },
  pingChangelly(){
   this.beforePingChangelly=1
   this.afterPingChangelly=2
   this.aroundPingChangelly=3
   this.afterPingChangellyThrows=4
   this.afterPingChangellyReturns=5
   this.immediatelyAfterPingChangelly=6
   this.afterPingChangellyCancels=7
   return {
    opts:1,
   }
  },
  connectChangelly(){
   this.beforeConnectChangelly=1
   this.afterConnectChangelly=2
   this.aroundConnectChangelly=3
   this.afterConnectChangellyThrows=4
   this.afterConnectChangellyReturns=5
   this.immediatelyAfterConnectChangelly=6
   this.afterConnectChangellyCancels=7
   return {
    opts:1,
   }
  },
  postConnectChangelly(){
   this.beforePostConnectChangelly=1
   this.afterPostConnectChangelly=2
   this.aroundPostConnectChangelly=3
   this.afterPostConnectChangellyThrows=4
   this.afterPostConnectChangellyReturns=5
   this.immediatelyAfterPostConnectChangelly=6
   this.afterPostConnectChangellyCancels=7
   return {
    connectResult:1,
   }
  },
 }),
]