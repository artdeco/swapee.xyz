import ChangellyConnectorAspectsInstaller from '../../aspects-installers/ChangellyConnectorAspectsInstaller'
import { newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractHyperChangellyConnector}
 */
class _AbstractHyperChangellyConnector {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperChangellyConnector
    .clone({aspectsInstaller:ChangellyConnectorAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.AbstractHyperChangellyConnector} ‎ */
class AbstractHyperChangellyConnector extends newAbstract(
 _AbstractHyperChangellyConnector,478904818825,null,{
  asIHyperChangellyConnector:1,
  superHyperChangellyConnector:2,
 },false) {}

/** @type {typeof xyz.swapee.AbstractHyperChangellyConnector} */
AbstractHyperChangellyConnector.class=function(){}
/** @type {typeof xyz.swapee.AbstractHyperChangellyConnector} */
function AbstractHyperChangellyConnectorClass(){}

export default AbstractHyperChangellyConnector