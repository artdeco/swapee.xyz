import { newAbstract, $implementations, iu, $initialese } from '@type.engineering/type-engineer'
import '../../../../types'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractChangellyConnector}
 */
class _AbstractChangellyConnector { }
/**
 * Connects to _Changelly_.
 * @extends {xyz.swapee.AbstractChangellyConnector} ‎
 */
class AbstractChangellyConnector extends newAbstract(
 _AbstractChangellyConnector,478904818826,null,{
  asIChangellyConnector:1,
  superChangellyConnector:2,
 },false,{
  cacheChangellyConnectResponse:{_cacheChangellyConnectResponse:1},
  changellyConnectorFrom:{_changellyConnectorFrom:2},
  changellyConnectorTo:{_changellyConnectorTo:3},
  changellyConnectorAmount:{_changellyConnectorAmount:4},
 }) {}

/** @type {typeof xyz.swapee.AbstractChangellyConnector} */
AbstractChangellyConnector.class=function(){}
/** @type {typeof xyz.swapee.AbstractChangellyConnector} */
function AbstractChangellyConnectorClass(){}

export default AbstractChangellyConnector

/** @type {xyz.swapee.IChangellyConnector.Symbols} */
export const ChangellyConnectorSymbols=AbstractChangellyConnector.Symbols
/** @type {xyz.swapee.IChangellyConnector.getSymbols} */
export const getChangellyConnectorSymbols=AbstractChangellyConnector.getSymbols
/** @type {xyz.swapee.IChangellyConnector.setSymbols} */
export const setChangellyConnectorSymbols=AbstractChangellyConnector.setSymbols

AbstractChangellyConnector[$implementations]=[
 AbstractChangellyConnectorClass.prototype=/**@type {!xyz.swapee.ChangellyConnector}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_cacheChangellyConnectResponse,_changellyConnectorFrom,_changellyConnectorTo,_changellyConnectorAmount}=getChangellyConnectorSymbols(this)
   setChangellyConnectorSymbols(this,{
    _cacheChangellyConnectResponse:iu(_cacheChangellyConnectResponse,true),
    _changellyConnectorFrom:iu(_changellyConnectorFrom,'btc'),
    _changellyConnectorTo:iu(_changellyConnectorTo,'eth'),
    _changellyConnectorAmount:iu(_changellyConnectorAmount,1),
   })
  },
 }),
 /** @type {!xyz.swapee.ChangellyConnector} */ ({
  [$initialese]:/**@type {!xyz.swapee.IChangellyConnector.Initialese}*/({
   changellyClientId:1,
   changellyClientSecret:1,
   connectChangelly:1,
   cacheChangellyConnectResponse:1,
   changellyConnectorFrom:1,
   changellyConnectorTo:1,
   changellyConnectorAmount:1,
  }),
  initializer({
   cacheChangellyConnectResponse:_cacheChangellyConnectResponse,
   changellyConnectorFrom:_changellyConnectorFrom,
   changellyConnectorTo:_changellyConnectorTo,
   changellyConnectorAmount:_changellyConnectorAmount,
  }) {
   setChangellyConnectorSymbols(this, {
    _cacheChangellyConnectResponse:_cacheChangellyConnectResponse,
    _changellyConnectorFrom:_changellyConnectorFrom,
    _changellyConnectorTo:_changellyConnectorTo,
    _changellyConnectorAmount:_changellyConnectorAmount,
   })
  },
 }),
]