import {Changelly, ChangellyUniversal} from '@type.community/changelly.com'
// import {mDuration, mStartChangellyTel} from './aop/ChangellyConnectorAspects/ChangellyConnectorAspects'
import AbstractChangellyConnector from './gen/AbstractChangellyConnector'

const mCliendId=new WeakMap
const mClientSecret=new WeakMap
// if stored as getters this could be displayed in the admin panel
// although can already view via telemetry.
const mConnect=new WeakMap
const mRes=new WeakMap

/** @extends {xyz.swapee.ChangellyConnector} */
export default class extends AbstractChangellyConnector
 .implements(
  ChangellyUniversal, // Changelly universal actually
  AbstractChangellyConnector.class.prototype=/** @type {!xyz.swapee.ChangellyConnector} */ ({
   postConnect() {
    const connRes=mRes.get(this)
    if(!connRes) return
    const{asIChangellyConnector:{postConnectChangelly:postConnectChangelly}}=this
    return postConnectChangelly(connRes)
   },
   postConnectChangelly() {}, // using aspects only
   initializer({
    changellyClientId:changellyClientId,
    changellyClientSecret:changellyClientSecret,
    connectChangelly:connectChangelly=true,
   }) {
    if(changellyClientId)     mCliendId.set(this,changellyClientId)
    if(changellyClientSecret) mClientSecret.set(this,changellyClientSecret)
    mConnect.set(this,connectChangelly)
   },
   allocator() {
    new Changelly({
     ...(mCliendId.has(this)?{
      clientId:mCliendId.get(this),
     }:{}),
     ...(mClientSecret.has(this)?{
      clientSecret:mClientSecret.get(this),
     }:{}),
    },this)
   },
   async connect() {
    const{
     asIChangellyConnector:{
      connectChangelly:connectChangelly,
      changellyConnectorFrom:  changellyConnectorFrom='btc',
      changellyConnectorTo:    changellyConnectoTo   ='eth',
      changellyConnectorAmount:changellyConnectorAmount  =1,
     },
    }=this
    const connect=mConnect.get(this)
    if(!connect) return

    const RES=await connectChangelly({
     from:changellyConnectorFrom,
     to:changellyConnectoTo,
     amount:changellyConnectorAmount,
    })
    mRes.set(this,RES)
    return RES
   },
   // make aspect of this
   async connectChangelly({
    from:from,
    to:to,
    amount:amount,
   }) {
    // debugger
    const{
     asUChangelly:{
      changelly:{
       asIChangellyExchange:{GetExchangeAmount:GetExchangeAmount},
      },
     },
     asILogger:{
      INFO:INFO,
      c:c,
     },
    }=this

    // todo: set timeout???
    // debugger
    const res=await GetExchangeAmount({
     from:   from,
     to:     to,
     amount: amount,
    })
    INFO('💱 Changelly connected: %s btc is %s eth',c(1,'yellow'),c(res,'cyan'))
    return res
   },
  }),
 )
{}
