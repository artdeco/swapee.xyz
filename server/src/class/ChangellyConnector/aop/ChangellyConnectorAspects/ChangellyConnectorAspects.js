import {ensurePathSync} from '@wrote/wrote'
import {writeFileSync} from 'fs'
import {hostname} from 'os'
import {join, sep} from 'path'
import AbstractChangellyConnectorAspects from "../../gen/aspects/AbstractChangellyConnectorAspects"

// const mSpeedMap=new WeakMap
const mTickets=new Map
/** @type {WeakMap<Object,number>} */
export const mDuration=new WeakMap
/** @type {WeakMap<Object,xyz.swapee.IConnectorTelemetry>} */
export const mStartChangellyTel=new WeakMap

/** @extends {xyz.swapee.ChangellyConnectorAspects} */
export default class extends AbstractChangellyConnectorAspects.continues(
 AbstractChangellyConnectorAspects.class.prototype =/**@type {!xyz.swapee.ChangellyConnectorAspects} */ ({
  // afterConnectMongo
  afterPostConnectChangelly:[// could be before as well
   async function({ticket,args:{
    connectResult:connectResult,
   }}) {
    // console.log(ticket)
    // debugger
    // return['ok']
    const{asIChangellyConnector:{asIServer}}=this
    const RES=connectResult
    const duration=mDuration.get(asIServer)
    return
    const{
     // the model should be found on the connector
     asIChangellyConnector:{
      asISwapeeServer:{finalizeTelemetryRecord:finalizeTelemetryRecord},
     },
    }=this
    const telRecord=mStartChangellyTel.get(asIServer)
    await finalizeTelemetryRecord(telRecord,RES,duration)
   },
  ],
  afterConnectChangelly:[
   // async function({res}) {
   //  const{
   //  // the model should be found on the connector
   //   asIChangellyConnector:{
   //    // postConnectChangelly:postConnectChangelly,
   //    // asIServer:{mongoClient,env},
   //   },
   //  }=this
   // },
   async function({ticket:ticket}) {
    // mSpeedMap.set(this,ticket)
    // debugger
   // return 'ok'
    const{
    // the model should be found on the connector
     asIChangellyConnector:{
      asIServer:{mongoClient,env},
     },
    }=this
    return // todo: change this to profiling aspects.
    // const swapeeModel=new SwapeeModel({
    //  client:mongoClient,
    // })
    // // const RES=mRes.get(this)
    // // const duration=mDuration.get(this)
    // // const connectorTelementry=
    // const HOSTNAME=hostname() // this can be done on an abstract level to avoid
    // // having to call hostname etc
    // // debugger
    // const telemetryRecord=await swapeeModel.asIConnectorTelemetries.CreateConnectorTelemetry({
    //  connectorName:'changelly',
    //  date:new Date,
    //  hostname:HOSTNAME,
    //  // duration: duration,
    //  env:env,
    //  // data:RES,
    //  // host:publicIp,
    // })
    // // then it's updated manually in chanchelly connector's postconnect
    // mStartChangellyTel.set(this,telemetryRecord)
   },
   // /** @this {xyz.swapee.ChangellyConnectorAspects} */
   // function({ticket:ticket}) {
   //  // mSpeedMap.set(this,ticket)
   // },
  ],
  beforeConnectChangelly:[
   function({ticket:ticket}) { // speed ticket
    const d=new Date
    mTickets.set(ticket,d)
   },
  ],
 }),
 AbstractChangellyConnectorAspects.class.prototype = /** @type {!xyz.swapee.ChangellyConnectorAspects} */ ({
  afterConnectChangelly:[
   // /** @this {xyz.swapee.ChangellyConnectorAspects} */
   // function({
   //  ticket:ticket,
   // }) {
   //  const ticket1=mSpeedMap.get(this)
   //  mSpeedMap.delete(this)
   //  console.log(ticket1==ticket)
   // },
   /** @this {xyz.swapee.ChangellyConnectorAspects} */
   function({ticket:ticket,res:res,err:err}) {
    const d=new Date
    const startD=mTickets.get(ticket)
    mTickets.delete(ticket)
    const{
     asIChangellyConnector:{
      INFO:INFO,c:c,
      // b,
     },
    }=this
    const DURATION=d.getTime()-startD.getTime()
    mDuration.set(this,DURATION)
    INFO('💱 Changelly loaded in %sms',c(DURATION+'','white',{reverse:true,'bold':true}))
    // console.log('', )
    // mTickets.set(ticket,d)
   },
  ],
  afterConnectChangellyThrows:[
   function({err}){
    const{
     asIChangellyConnector:{
      asUChangelly:{changelly},
     },
     asILogger:{
      CRITICAL:CRITICAL,ALERT:ALERT,WARN:WARN,
      EMERGENCY:EMERGENCY,
      c:c,
     },
    }=this
    let STACK=err.stack.replace(err.message,'').replace('Error: ','').trim()
    let shouldReplace=sep=='/'
    while(shouldReplace) {
     let prevStack=STACK
     STACK=STACK.replace(process.cwd()+'/','')
     if(prevStack==STACK) shouldReplace=false
    }

    STACK=STACK
     .replace(
      /^.+?eval at <anonymous> \(node_modules\/@type.engineering\/type-engineer.+?\r?\n/gm,
      '',
     )

    EMERGENCY('⛽️ Changelly connection failed: %s\n%s',c(err.message,'yellow'),c(STACK,'grey'))

    if (/Unauthorized/.test(err.message)) {
     CRITICAL('Changelly credentials don\'t match (%s), requests will fail.',err.message)
     if(!changelly.clientId) {
      ALERT('Changelly clientId is missing.')
     }
     if(!changelly.clientSecret) {
      ALERT('Changelly clientSecret is missing.')
     }
     if(!changelly.clientSecret&&!changelly.clientId) {
      WARN('Changelly credentials seems to be missing, have you loaded the server from the project dir?')
     }
    }

    err.stack=STACK
    err.warned=true
    throw err
   },
  ],
  afterConnectChangellyReturns: [
   function cacheConnectChangellyReturn({res:res,args}) {
    // console.log(JSON.stringify(res,null,2))
    const{
     asIChangellyConnector:{
      cacheChangellyConnectResponse:cacheChangellyConnectResponse,
     },
    }=this
    if(!cacheChangellyConnectResponse) return
    const PATH=join(__dirname,'../../data/connect-changelly.json')
    ensurePathSync(PATH)
    writeFileSync(PATH,JSON.stringify({
     time:(new Date).getTime(),
     date:(new Date).toLocaleString('en-gb'),
     args:args,
     desc:"The method tests for changelly connections to check if client id is valid.",
     data:res,
    },null,1))
   },
   async function saveChangellyConnectorTelemetry({res:res,args}) {
    // const{
    //  // the model should be found on the connector
    //  asIChangellyConnector:{
    //   asIServer:{publicIp},
    //  },
    // }=this
    // const telRecord=mStartTel.get(this)
    // const duration=mDuration.get(this)
    // telRecord.duration=duration
    // telRecord.host=publicIp
    // telRecord.data=data

    // telRecord.host=publicIp
    // const{
    //  // the model should be found on the connector
    //  asIChangellyConnector:{
    //   asIServer:{mongoClient,env,publicIp},
    //  },
    // }=this
    // // debugger
    // const swapeeModel=new SwapeeModel({
    //  client:mongoClient,
    // })
    // // debugger
    // await swapeeModel.asIConnectorTelemetries.CreateConnectorTelemetry({
    //  connectorName: 'changelly',
    //  date: new Date,
    //  duration: duration,
    //  env:env,
    //  host:publicIp,
    // })
    // debugger
   },
  ],
 }),
) {}