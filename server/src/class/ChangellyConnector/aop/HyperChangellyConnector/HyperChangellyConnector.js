import ChangellyConnector from "../../ChangellyConnector"
import AbstractHyperChangellyConnector from "../../gen/hyper/AbstractHyperChangellyConnector"
import ChangellyConnectorAspects from "../ChangellyConnectorAspects"

export default class HyperChangellyConnector extends AbstractHyperChangellyConnector
 .consults(
  ChangellyConnectorAspects,
 )
 .implements(
  ChangellyConnector,
 )
{}