import {Jwt} from '@idio2/jwt'
import {ActionMod} from '@idio2/server'
import {StaticAssetGatewayFactory} from '@idio2/server/static-asset-gateway'
import {HyperSwapee} from '@swapee.xyz/swapee.xyz' // default swapee
import {readDirStructureSync} from '@wrote/wrote'
import {readFileSync} from 'fs'
import {join} from 'path'
// import {Swapee} from '../../../../src'
import RpcGateway from '../../../gateways/rpc'
import '../../../types'
import SwapeeServer from '../../class/SwapeeServer'

const{content}=readDirStructureSync('generated/web-components')
const circuitsMap=new Map
for(const key in content){
 // const v=content[key]
 const json=readFileSync(join('generated/web-components',key))
 const j=JSON.parse(json)
 const{tags:[{name}]}=j
 circuitsMap.set(key.replace(/.json$/,''),name)
}

console.log(circuitsMap.size,'known circuits')

/** @type {xyz.swapee.Swapee_Server} */
export default async function Swapee_Server() {
 const swapee=new HyperSwapee
 const swapeeServer=new SwapeeServer({
  appName:'Swapee',
  ngrok:false,
  // ngrok:process.env.NODE_ENV=='production'?void 0:'eu',
  connectIpify:false,
  port:5000,
 })
 await swapeeServer.connect()
 await swapeeServer.listen()

 new Jwt(swapeeServer) // if gateways now implement jwt, they can encode messages etc
 // const swapeeServer=await swapee.createServer()
 // pattern - connector pattern.

 const{exchanges:{letsExchange,changeNow},exchanges,results}=await swapee.connectExchanges({
  amount:0.01,
 })
 // debugger
 swapeeServer.INFO('LetsExchange rate 0.01btc/eth:',parseFloat(results.get(letsExchange).amount))
 swapeeServer.INFO('ChangeNow rate 0.01btc/eth',parseFloat(results.get(changeNow).toAmount))
 // for(const key in exchanges){
 //  const v=exchanges[key]
 //  const r=results.get(v)
 //  console.log(key,r)
 // }
 if(swapeeServer.env=='prod'){
  swapeeServer.app.proxy=true
 }

 swapeeServer.addGateway({
  path:'**',
  Gateway:StaticAssetGatewayFactory({
   ifModifiedSinceCaching:true,
  }),
  init:{
   dir:'maurice/docs',
  },
 },
 ).addGateway({
  paths:[
   ['_rpc',{circuitId:/^[0-9]{10}$/i}],
  ],
  Mods:[
   ActionMod,
   // LayoutMod,
  ],
  getMount({}={}){
   return'/'
  },
  Gateway:RpcGateway,
  inits:[
   exchanges,
   /**@type {xyz.swapee.ws.rpc.IRpcGateway.Initialese}*/({
    circuitsMap:circuitsMap,
    // ...exchanges,
    // letsExchange,
    // changeNow,
    // changelly:changelly,
   }),
  ],
 })

 const addr=swapeeServer.server.address()
 console.log('🆙 Started on %s:%s',addr.address,addr.port,
  (swapeeServer.publicUrl||swapeeServer.publicIp)?`[${swapeeServer.publicUrl||swapeeServer.publicIp}]`:'')
}

export const TOKENS={
 // '6f01661a-9539-4b6f-bb2c-963c3fe13df3':{
 //  user:'4r7d3c0',
 // },
}

export const USERS={
}
