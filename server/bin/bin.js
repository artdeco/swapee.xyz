import {envariable} from '@idio2/server'
import {readdirSync} from 'fs'
import {join} from 'path'

const SCRIPT=process.argv[2]
const dir=readdirSync(join(__dirname,'scripts')).map(s=>s.replace(/.js$/,''))
if(!SCRIPT) {
 console.error('Usage: %s', `node bin [${dir.join('|')}]`)
 process.exit(1)
}else if(!dir.includes(SCRIPT)) {
 console.error('Script "%s" not found. Available scripts:',SCRIPT,dir.map(d=>`
- ${d}`).join('\n'))
 process.exit(1)
}

if(process.env.NODE_ENV!='production') {
 envariable({
  name: '.env/local.env',
 })
}

const fn=require(`./scripts/${SCRIPT}`)

;(async()=>{
 try {
  await fn()
 }catch (err) {
  if(!err.warned) console.log(err)
  process.exit(1)
 }
})()