#!/usr/bin/env node
// Error.stackTraceLimit=Infinity
require('@type.engineering/web-computing').alamode({
 voidTypedefs:true,
 // measureEval:true,
 // measurePerf:true,

 // enableJSXCache:false,
 // enableJSCache:false,
 // resetJSCache:true,

 enableJSXCache:true,
 enableJSCache:true,
 // resetJSCache:true,

 metamodule:true,
 tag:true,
 // metamodule:true,
 jsx: {
 },
})
require('./bin')