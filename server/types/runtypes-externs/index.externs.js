/**
 * @fileoverview
 * @externs
 */

xyz.swapee.IChangellyConnectorJoinpointModel={}
xyz.swapee.IChangellyConnector={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.IChangellyConnectorJoinpointModel={}
$$xyz.swapee.IChangellyConnector={}
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.Initialese  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {engineer.type.IConnector.Initialese}
 * @extends {com.changelly.UChangelly.Initialese}
 */
xyz.swapee.IChangellyConnector.Initialese = function() {}
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyClientId
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyClientSecret
/** @type {boolean|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.connectChangelly
/** @type {((boolean|string))|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.cacheChangellyConnectResponse
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyConnectorFrom
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyConnectorTo
/** @type {number|undefined} */
xyz.swapee.IChangellyConnector.Initialese.prototype.changellyConnectorAmount

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorFields  7a7e0756f4e46807b6a8d14081259b68 */
/** @interface */
xyz.swapee.IChangellyConnectorFields
/** @type {(boolean|string)} */
xyz.swapee.IChangellyConnectorFields.prototype.cacheChangellyConnectResponse
/** @type {string} */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorFrom
/** @type {string} */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorTo
/** @type {number} */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorAmount

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorCaster  7a7e0756f4e46807b6a8d14081259b68 */
/** @interface */
xyz.swapee.IChangellyConnectorCaster
/** @type {!xyz.swapee.BoundIChangellyConnector} */
xyz.swapee.IChangellyConnectorCaster.prototype.asIChangellyConnector
/** @type {!xyz.swapee.BoundChangellyConnector} */
xyz.swapee.IChangellyConnectorCaster.prototype.superChangellyConnector

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @interface
 * @extends {xyz.swapee.IChangellyConnectorFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IChangellyConnectorCaster}
 * @extends {engineer.type.IConnector}
 * @extends {com.changelly.UChangelly}
 */
xyz.swapee.IChangellyConnector = function() {}
/** @param {...!xyz.swapee.IChangellyConnector.Initialese} init */
xyz.swapee.IChangellyConnector.prototype.constructor = function(...init) {}
/** @return {!Promise} */
xyz.swapee.IChangellyConnector.prototype.ping = function() {}
/**
 * @param {!xyz.swapee.IChangellyConnector.pingChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
xyz.swapee.IChangellyConnector.prototype.pingChangelly = function(opts) {}
/**
 * @param {!xyz.swapee.IChangellyConnector.connectChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
xyz.swapee.IChangellyConnector.prototype.connectChangelly = function(opts) {}
/**
 * @param {!Array<!com.changelly.ExchangeInfo>} connectResult
 * @return {!Promise<void>}
 */
xyz.swapee.IChangellyConnector.prototype.postConnectChangelly = function(connectResult) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @param {...!xyz.swapee.IChangellyConnector.Initialese} init
 * @implements {xyz.swapee.IChangellyConnector}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnector.Initialese>}
 */
xyz.swapee.ChangellyConnector = function(...init) {}
/** @param {...!xyz.swapee.IChangellyConnector.Initialese} init */
xyz.swapee.ChangellyConnector.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.ChangellyConnector.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @extends {xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector = function() {}
/**
 * @param {...((!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnector.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.__trait = function(...Implementations) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModelHyperslice  7a7e0756f4e46807b6a8d14081259b68 */
/** @interface */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforePing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePing>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforePing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPing>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforePingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPingChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforeConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.beforePostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.afterPostConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly>)} */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.immediatelyAfterPostConnectChangelly

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorJoinpointModelHyperslice  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModelHyperslice}
 */
xyz.swapee.ChangellyConnectorJoinpointModelHyperslice = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforePing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterPing
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforePingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPingChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterPingChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforeConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.beforePostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangelly
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangellyThrows
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangellyReturns
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.afterPostConnectChangellyCancels
/** @type {(!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly<THIS>>)} */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.immediatelyAfterPostConnectChangelly

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorJoinpointModelBindingHyperslice  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.ChangellyConnectorJoinpointModelBindingHyperslice = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel  7a7e0756f4e46807b6a8d14081259b68 */
/** @interface */
xyz.swapee.IChangellyConnectorJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePing = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPing = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPing = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePingChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPingChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforeConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePostConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangelly = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyThrows = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyReturns = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyCancels = function(data) {}
/**
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPostConnectChangelly = function(data) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorJoinpointModel  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModel}
 */
xyz.swapee.ChangellyConnectorJoinpointModel = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.RecordIChangellyConnectorJoinpointModel  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ beforePing: xyz.swapee.IChangellyConnectorJoinpointModel.beforePing, afterPing: xyz.swapee.IChangellyConnectorJoinpointModel.afterPing, afterPingThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows, afterPingReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns, afterPingCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels, immediatelyAfterPing: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing, beforePingChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly, afterPingChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly, afterPingChangellyThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows, afterPingChangellyReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns, afterPingChangellyCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels, immediatelyAfterPingChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly, beforeConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly, afterConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly, afterConnectChangellyThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows, afterConnectChangellyReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns, afterConnectChangellyCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels, immediatelyAfterConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly, beforePostConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly, afterPostConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly, afterPostConnectChangellyThrows: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows, afterPostConnectChangellyReturns: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns, afterPostConnectChangellyCancels: xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels, immediatelyAfterPostConnectChangelly: xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly }} */
xyz.swapee.RecordIChangellyConnectorJoinpointModel

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BoundIChangellyConnectorJoinpointModel  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.RecordIChangellyConnectorJoinpointModel}
 */
xyz.swapee.BoundIChangellyConnectorJoinpointModel = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BoundChangellyConnectorJoinpointModel  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.BoundIChangellyConnectorJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundChangellyConnectorJoinpointModel = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.beforePing  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePing
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforePing
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPing  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPing
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPing
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly = function(data) {}
/** @typedef {function(!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnectorJoinpointModel, !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData=): void} */
xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly

// nss:xyz.swapee.IChangellyConnectorJoinpointModel,$$xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese  7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorAspectsInstaller  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
xyz.swapee.IChangellyConnectorAspectsInstaller = function() {}
/** @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforePing
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPing
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterPing
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforePingChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangellyThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangellyReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPingChangellyCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterPingChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforeConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangellyThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangellyReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterConnectChangellyCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.beforePostConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangelly
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangellyThrows
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangellyReturns
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.afterPostConnectChangellyCancels
/** @type {number} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.immediateAfterPostConnectChangelly
/** @return {void} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.ping = function() {}
/** @return {?} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.pingChangelly = function() {}
/** @return {?} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.connectChangelly = function() {}
/** @return {?} */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.postConnectChangelly = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorAspectsInstaller  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.IChangellyConnectorAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese>}
 */
xyz.swapee.ChangellyConnectorAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init */
xyz.swapee.ChangellyConnectorAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.ChangellyConnectorAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnectorAspectsInstaller  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @extends {xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller = function() {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorAspectsInstallerConstructor  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(new: xyz.swapee.IChangellyConnectorAspectsInstaller, ...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese)} */
xyz.swapee.ChangellyConnectorAspectsInstallerConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData.prototype.cancel = function(reason) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData.prototype.hide = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData.prototype.reasons

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData = function() {}
/** @type {!Promise<void>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData.prototype.promise

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.PingChangellyNArgs  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ opts: !xyz.swapee.IChangellyConnector.pingChangelly.Opts }} */
xyz.swapee.IChangellyConnector.PingChangellyNArgs

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.IChangellyConnector.PingChangellyNArgs, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.cond
/**
 * @param {xyz.swapee.IChangellyConnector.PingChangellyNArgs} args
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<!Array<!com.changelly.ExchangeInfo>>} value
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData.prototype.res

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData.prototype.hide = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData.prototype.res
/**
 * @param {(!Promise<!Array<!com.changelly.ExchangeInfo>>|!Array<!com.changelly.ExchangeInfo>)} value
 * @return {?}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData.prototype.reasons

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData = function() {}
/** @type {!Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData.prototype.promise

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.ConnectChangellyNArgs  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ opts: !xyz.swapee.IChangellyConnector.connectChangelly.Opts }} */
xyz.swapee.IChangellyConnector.ConnectChangellyNArgs

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.IChangellyConnector.ConnectChangellyNArgs, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.cond
/**
 * @param {xyz.swapee.IChangellyConnector.ConnectChangellyNArgs} args
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<!Array<!com.changelly.ExchangeInfo>>} value
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData.prototype.res

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData.prototype.hide = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData = function() {}
/** @type {!Array<!com.changelly.ExchangeInfo>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData.prototype.res
/**
 * @param {(!Promise<!Array<!com.changelly.ExchangeInfo>>|!Array<!com.changelly.ExchangeInfo>)} value
 * @return {?}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData.prototype.reasons

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData = function() {}
/** @type {!Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData.prototype.promise

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ connectResult: !Array<!com.changelly.ExchangeInfo> }} */
xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs, proc: !Function }} */
xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData.prototype.cond
/**
 * @param {xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs} args
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData.prototype.cancel = function(reason) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData.prototype.err
/** @return {void} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData.prototype.hide = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData.prototype.reasons

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData = function() {}
/** @type {!Promise<void>} */
xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData.prototype.promise

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorConstructor  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(new: xyz.swapee.IChangellyConnector, ...!xyz.swapee.IChangellyConnector.Initialese)} */
xyz.swapee.ChangellyConnectorConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BChangellyConnectorAspectsCaster  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.BChangellyConnectorAspectsCaster
/** @type {!xyz.swapee.BoundIChangellyConnector} */
xyz.swapee.BChangellyConnectorAspectsCaster.prototype.asIChangellyConnector

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BChangellyConnectorAspects  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @interface
 * @extends {xyz.swapee.BChangellyConnectorAspectsCaster<THIS>}
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.BChangellyConnectorAspects = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorAspects.Initialese  7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
xyz.swapee.IChangellyConnectorAspects.Initialese = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorAspectsCaster  7a7e0756f4e46807b6a8d14081259b68 */
/** @interface */
xyz.swapee.IChangellyConnectorAspectsCaster
/** @type {!xyz.swapee.BoundIChangellyConnector} */
xyz.swapee.IChangellyConnectorAspectsCaster.prototype.asIChangellyConnector

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnectorAspects  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IChangellyConnectorAspectsCaster}
 * @extends {xyz.swapee.BChangellyConnectorAspects<!xyz.swapee.IChangellyConnectorAspects>}
 */
xyz.swapee.IChangellyConnectorAspects = function() {}
/** @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init */
xyz.swapee.IChangellyConnectorAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorAspects  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init
 * @implements {xyz.swapee.IChangellyConnectorAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspects.Initialese>}
 */
xyz.swapee.ChangellyConnectorAspects = function(...init) {}
/** @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init */
xyz.swapee.ChangellyConnectorAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.ChangellyConnectorAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnectorAspects  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @extends {xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects = function() {}
/**
 * @param {...((!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnectorAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects))} Implementations
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.ChangellyConnectorAspectsConstructor  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(new: xyz.swapee.IChangellyConnectorAspects, ...!xyz.swapee.IChangellyConnectorAspects.Initialese)} */
xyz.swapee.ChangellyConnectorAspectsConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IHyperChangellyConnector.Initialese  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnector.Initialese}
 */
xyz.swapee.IHyperChangellyConnector.Initialese = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IHyperChangellyConnectorCaster  7a7e0756f4e46807b6a8d14081259b68 */
/** @interface */
xyz.swapee.IHyperChangellyConnectorCaster
/** @type {!xyz.swapee.BoundIHyperChangellyConnector} */
xyz.swapee.IHyperChangellyConnectorCaster.prototype.asIHyperChangellyConnector
/** @type {!xyz.swapee.BoundHyperChangellyConnector} */
xyz.swapee.IHyperChangellyConnectorCaster.prototype.superHyperChangellyConnector

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IHyperChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IHyperChangellyConnectorCaster}
 * @extends {xyz.swapee.IChangellyConnector}
 */
xyz.swapee.IHyperChangellyConnector = function() {}
/** @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init */
xyz.swapee.IHyperChangellyConnector.prototype.constructor = function(...init) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.HyperChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init
 * @implements {xyz.swapee.IHyperChangellyConnector}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IHyperChangellyConnector.Initialese>}
 */
xyz.swapee.HyperChangellyConnector = function(...init) {}
/** @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init */
xyz.swapee.HyperChangellyConnector.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.HyperChangellyConnector.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.AbstractHyperChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @constructor
 * @extends {xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector = function() {}
/**
 * @param {...((!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector))} Implementations
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector))} Implementations
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector))} Implementations
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.IChangellyConnectorAspects|function(new: xyz.swapee.IChangellyConnectorAspects))} aides
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.consults = function(...aides) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.HyperChangellyConnectorConstructor  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(new: xyz.swapee.IHyperChangellyConnector, ...!xyz.swapee.IHyperChangellyConnector.Initialese)} */
xyz.swapee.HyperChangellyConnectorConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.RecordIHyperChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.RecordIHyperChangellyConnector

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BoundIHyperChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.RecordIHyperChangellyConnector}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.IHyperChangellyConnectorCaster}
 */
xyz.swapee.BoundIHyperChangellyConnector = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BoundHyperChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.BoundIHyperChangellyConnector}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundHyperChangellyConnector = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.RecordIChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {{ ping: xyz.swapee.IChangellyConnector.ping, pingChangelly: xyz.swapee.IChangellyConnector.pingChangelly, connectChangelly: xyz.swapee.IChangellyConnector.connectChangelly, postConnectChangelly: xyz.swapee.IChangellyConnector.postConnectChangelly }} */
xyz.swapee.RecordIChangellyConnector

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BoundIChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.IChangellyConnectorFields}
 * @extends {xyz.swapee.RecordIChangellyConnector}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.IChangellyConnectorCaster}
 * @extends {engineer.type.BoundIConnector}
 * @extends {com.changelly.BoundUChangelly}
 */
xyz.swapee.BoundIChangellyConnector = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.BoundChangellyConnector  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @record
 * @extends {xyz.swapee.BoundIChangellyConnector}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundChangellyConnector = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.ping  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @return {!Promise}
 */
$$xyz.swapee.IChangellyConnector.__ping = function() {}
/** @typedef {function(): !Promise} */
xyz.swapee.IChangellyConnector.ping
/** @typedef {function(this: xyz.swapee.IChangellyConnector): !Promise} */
xyz.swapee.IChangellyConnector._ping
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__ping} */
xyz.swapee.IChangellyConnector.__ping

// nss:xyz.swapee.IChangellyConnector,$$xyz.swapee.IChangellyConnector,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.pingChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnector.pingChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
$$xyz.swapee.IChangellyConnector.__pingChangelly = function(opts) {}
/** @typedef {function(!xyz.swapee.IChangellyConnector.pingChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector.pingChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnector, !xyz.swapee.IChangellyConnector.pingChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector._pingChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__pingChangelly} */
xyz.swapee.IChangellyConnector.__pingChangelly

// nss:xyz.swapee.IChangellyConnector,$$xyz.swapee.IChangellyConnector,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.connectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.IChangellyConnector.connectChangelly.Opts} [opts]
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>}
 */
$$xyz.swapee.IChangellyConnector.__connectChangelly = function(opts) {}
/** @typedef {function(!xyz.swapee.IChangellyConnector.connectChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector.connectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnector, !xyz.swapee.IChangellyConnector.connectChangelly.Opts=): !Promise<!Array<!com.changelly.ExchangeInfo>>} */
xyz.swapee.IChangellyConnector._connectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__connectChangelly} */
xyz.swapee.IChangellyConnector.__connectChangelly

// nss:xyz.swapee.IChangellyConnector,$$xyz.swapee.IChangellyConnector,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.postConnectChangelly  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Array<!com.changelly.ExchangeInfo>} connectResult
 * @return {!Promise<void>}
 */
$$xyz.swapee.IChangellyConnector.__postConnectChangelly = function(connectResult) {}
/** @typedef {function(!Array<!com.changelly.ExchangeInfo>): !Promise<void>} */
xyz.swapee.IChangellyConnector.postConnectChangelly
/** @typedef {function(this: xyz.swapee.IChangellyConnector, !Array<!com.changelly.ExchangeInfo>): !Promise<void>} */
xyz.swapee.IChangellyConnector._postConnectChangelly
/** @typedef {typeof $$xyz.swapee.IChangellyConnector.__postConnectChangelly} */
xyz.swapee.IChangellyConnector.__postConnectChangelly

// nss:xyz.swapee.IChangellyConnector,$$xyz.swapee.IChangellyConnector,xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.pingChangelly.Opts  7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
xyz.swapee.IChangellyConnector.pingChangelly.Opts = function() {}
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.pingChangelly.Opts.prototype.from
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.pingChangelly.Opts.prototype.to
/** @type {number|undefined} */
xyz.swapee.IChangellyConnector.pingChangelly.Opts.prototype.amount

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.connectChangelly.Opts  7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
xyz.swapee.IChangellyConnector.connectChangelly.Opts = function() {}
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.connectChangelly.Opts.prototype.from
/** @type {string|undefined} */
xyz.swapee.IChangellyConnector.connectChangelly.Opts.prototype.to
/** @type {number|undefined} */
xyz.swapee.IChangellyConnector.connectChangelly.Opts.prototype.amount

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServer.Initialese  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {_idio.IServer.Initialese}
 */
xyz.swapee.ISwapeeServer.Initialese = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServerCaster  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @interface */
xyz.swapee.ISwapeeServerCaster
/** @type {!xyz.swapee.BoundISwapeeServer} */
xyz.swapee.ISwapeeServerCaster.prototype.asISwapeeServer
/** @type {!xyz.swapee.BoundSwapeeServer} */
xyz.swapee.ISwapeeServerCaster.prototype.superSwapeeServer

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.ISwapeeServerCaster}
 * @extends {_idio.IServer}
 */
xyz.swapee.ISwapeeServer = function() {}
/** @param {...!xyz.swapee.ISwapeeServer.Initialese} init */
xyz.swapee.ISwapeeServer.prototype.constructor = function(...init) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.SwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @param {...!xyz.swapee.ISwapeeServer.Initialese} init
 * @implements {xyz.swapee.ISwapeeServer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeServer.Initialese>}
 */
xyz.swapee.SwapeeServer = function(...init) {}
/** @param {...!xyz.swapee.ISwapeeServer.Initialese} init */
xyz.swapee.SwapeeServer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.SwapeeServer.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.AbstractSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @extends {xyz.swapee.SwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer = function() {}
/**
 * @param {...((!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)|(!_idio.IServer|typeof _idio.Server))} Implementations
 * @return {typeof xyz.swapee.SwapeeServer}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeServer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractSwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)|(!_idio.IServer|typeof _idio.Server))} Implementations
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)|(!_idio.IServer|typeof _idio.Server))} Implementations
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.__trait = function(...Implementations) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServerJoinpointModel  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @interface */
xyz.swapee.ISwapeeServerJoinpointModel = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.SwapeeServerJoinpointModel  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @implements {xyz.swapee.ISwapeeServerJoinpointModel}
 */
xyz.swapee.SwapeeServerJoinpointModel = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.RecordISwapeeServerJoinpointModel  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.RecordISwapeeServerJoinpointModel

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BoundISwapeeServerJoinpointModel  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {xyz.swapee.RecordISwapeeServerJoinpointModel}
 */
xyz.swapee.BoundISwapeeServerJoinpointModel = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BoundSwapeeServerJoinpointModel  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {xyz.swapee.BoundISwapeeServerJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundSwapeeServerJoinpointModel = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServerAspectsInstaller.Initialese  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @record */
xyz.swapee.ISwapeeServerAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServerAspectsInstaller  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
xyz.swapee.ISwapeeServerAspectsInstaller = function() {}
/** @param {...!xyz.swapee.ISwapeeServerAspectsInstaller.Initialese} init */
xyz.swapee.ISwapeeServerAspectsInstaller.prototype.constructor = function(...init) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.SwapeeServerAspectsInstaller  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @param {...!xyz.swapee.ISwapeeServerAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.ISwapeeServerAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeServerAspectsInstaller.Initialese>}
 */
xyz.swapee.SwapeeServerAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.ISwapeeServerAspectsInstaller.Initialese} init */
xyz.swapee.SwapeeServerAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.SwapeeServerAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.AbstractSwapeeServerAspectsInstaller  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @extends {xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller = function() {}
/**
 * @param {...(!xyz.swapee.ISwapeeServerAspectsInstaller|typeof xyz.swapee.SwapeeServerAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractSwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.ISwapeeServerAspectsInstaller|typeof xyz.swapee.SwapeeServerAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.ISwapeeServerAspectsInstaller|typeof xyz.swapee.SwapeeServerAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.SwapeeServerAspectsInstallerConstructor  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {function(new: xyz.swapee.ISwapeeServerAspectsInstaller, ...!xyz.swapee.ISwapeeServerAspectsInstaller.Initialese)} */
xyz.swapee.SwapeeServerAspectsInstallerConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.SwapeeServerConstructor  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {function(new: xyz.swapee.ISwapeeServer, ...!xyz.swapee.ISwapeeServer.Initialese)} */
xyz.swapee.SwapeeServerConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BSwapeeServerAspectsCaster  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.BSwapeeServerAspectsCaster
/** @type {!xyz.swapee.BoundISwapeeServer} */
xyz.swapee.BSwapeeServerAspectsCaster.prototype.asISwapeeServer

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BSwapeeServerAspects  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @interface
 * @extends {xyz.swapee.BSwapeeServerAspectsCaster<THIS>}
 * @template THIS
 */
xyz.swapee.BSwapeeServerAspects = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServerAspects.Initialese  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @record */
xyz.swapee.ISwapeeServerAspects.Initialese = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServerAspectsCaster  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @interface */
xyz.swapee.ISwapeeServerAspectsCaster
/** @type {!xyz.swapee.BoundISwapeeServer} */
xyz.swapee.ISwapeeServerAspectsCaster.prototype.asISwapeeServer

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.ISwapeeServerAspects  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.ISwapeeServerAspectsCaster}
 * @extends {xyz.swapee.BSwapeeServerAspects<!xyz.swapee.ISwapeeServerAspects>}
 */
xyz.swapee.ISwapeeServerAspects = function() {}
/** @param {...!xyz.swapee.ISwapeeServerAspects.Initialese} init */
xyz.swapee.ISwapeeServerAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.SwapeeServerAspects  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @param {...!xyz.swapee.ISwapeeServerAspects.Initialese} init
 * @implements {xyz.swapee.ISwapeeServerAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeServerAspects.Initialese>}
 */
xyz.swapee.SwapeeServerAspects = function(...init) {}
/** @param {...!xyz.swapee.ISwapeeServerAspects.Initialese} init */
xyz.swapee.SwapeeServerAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.SwapeeServerAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.AbstractSwapeeServerAspects  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @extends {xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects = function() {}
/**
 * @param {...((!xyz.swapee.ISwapeeServerAspects|typeof xyz.swapee.SwapeeServerAspects)|(!xyz.swapee.BSwapeeServerAspects|typeof xyz.swapee.BSwapeeServerAspects))} Implementations
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeServerAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractSwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.ISwapeeServerAspects|typeof xyz.swapee.SwapeeServerAspects)|(!xyz.swapee.BSwapeeServerAspects|typeof xyz.swapee.BSwapeeServerAspects))} Implementations
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.ISwapeeServerAspects|typeof xyz.swapee.SwapeeServerAspects)|(!xyz.swapee.BSwapeeServerAspects|typeof xyz.swapee.BSwapeeServerAspects))} Implementations
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.SwapeeServerAspectsConstructor  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {function(new: xyz.swapee.ISwapeeServerAspects, ...!xyz.swapee.ISwapeeServerAspects.Initialese)} */
xyz.swapee.SwapeeServerAspectsConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.IHyperSwapeeServer.Initialese  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {xyz.swapee.ISwapeeServer.Initialese}
 */
xyz.swapee.IHyperSwapeeServer.Initialese = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.IHyperSwapeeServerCaster  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @interface */
xyz.swapee.IHyperSwapeeServerCaster
/** @type {!xyz.swapee.BoundIHyperSwapeeServer} */
xyz.swapee.IHyperSwapeeServerCaster.prototype.asIHyperSwapeeServer
/** @type {!xyz.swapee.BoundHyperSwapeeServer} */
xyz.swapee.IHyperSwapeeServerCaster.prototype.superHyperSwapeeServer

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.IHyperSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.IHyperSwapeeServerCaster}
 * @extends {xyz.swapee.ISwapeeServer}
 */
xyz.swapee.IHyperSwapeeServer = function() {}
/** @param {...!xyz.swapee.IHyperSwapeeServer.Initialese} init */
xyz.swapee.IHyperSwapeeServer.prototype.constructor = function(...init) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.HyperSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @param {...!xyz.swapee.IHyperSwapeeServer.Initialese} init
 * @implements {xyz.swapee.IHyperSwapeeServer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IHyperSwapeeServer.Initialese>}
 */
xyz.swapee.HyperSwapeeServer = function(...init) {}
/** @param {...!xyz.swapee.IHyperSwapeeServer.Initialese} init */
xyz.swapee.HyperSwapeeServer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.HyperSwapeeServer.__extend = function(...Extensions) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.AbstractHyperSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @constructor
 * @extends {xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer = function() {}
/**
 * @param {...((!xyz.swapee.IHyperSwapeeServer|typeof xyz.swapee.HyperSwapeeServer)|(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer))} Implementations
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapeeServer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.AbstractHyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.IHyperSwapeeServer|typeof xyz.swapee.HyperSwapeeServer)|(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer))} Implementations
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.IHyperSwapeeServer|typeof xyz.swapee.HyperSwapeeServer)|(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer))} Implementations
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.ISwapeeServerAspects|function(new: xyz.swapee.ISwapeeServerAspects))} aides
 * @return {typeof xyz.swapee.AbstractHyperSwapeeServer}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapeeServer.consults = function(...aides) {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.HyperSwapeeServerConstructor  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {function(new: xyz.swapee.IHyperSwapeeServer, ...!xyz.swapee.IHyperSwapeeServer.Initialese)} */
xyz.swapee.HyperSwapeeServerConstructor

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.RecordIHyperSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.RecordIHyperSwapeeServer

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BoundIHyperSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {xyz.swapee.RecordIHyperSwapeeServer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.IHyperSwapeeServerCaster}
 */
xyz.swapee.BoundIHyperSwapeeServer = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BoundHyperSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {xyz.swapee.BoundIHyperSwapeeServer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundHyperSwapeeServer = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.RecordISwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.RecordISwapeeServer

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BoundISwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {xyz.swapee.RecordISwapeeServer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.ISwapeeServerCaster}
 * @extends {_idio.BoundIServer}
 */
xyz.swapee.BoundISwapeeServer = function() {}

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/ISwapeeServer.xml} xyz.swapee.BoundSwapeeServer  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/**
 * @record
 * @extends {xyz.swapee.BoundISwapeeServer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.BoundSwapeeServer = function() {}

// nss:xyz.swapee
/* @typal-end */

/* @typal-type {types/api.xml} xyz.swapee.Swapee_Server.Config no-functions a6aec48825b49b6dd71c2746d32b645e */
/** @record */
xyz.swapee.Swapee_Server.Config = function() {}

// nss:xyz.swapee
/* @typal-end */