/* @typal-type {types/api.xml} xyz.swapee.Swapee_Server functions a6aec48825b49b6dd71c2746d32b645e */
/**
 * @param {!xyz.swapee.Swapee_Server.Config} config
 * @return {!Promise}
 */
$xyz.swapee.Swapee_Server = function(config) {}
/** @typedef {typeof $xyz.swapee.Swapee_Server} */
xyz.swapee.Swapee_Server

// nss:xyz.swapee,$xyz.swapee
/* @typal-end */