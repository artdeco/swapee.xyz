/** @const {?} */ $xyz.swapee.IChangellyConnector
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnector.getSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {!xyz.swapee.IChangellyConnector.getSymbols} */
xyz.swapee.AbstractChangellyConnector.getSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnector.setSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {!xyz.swapee.IChangellyConnector.setSymbols} */
xyz.swapee.AbstractChangellyConnector.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.AbstractChangellyConnector.Symbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {!xyz.swapee.IChangellyConnector.Symbols} */
xyz.swapee.AbstractChangellyConnector.Symbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.SymbolsIn filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
$xyz.swapee.IChangellyConnector.SymbolsIn = function() {}
/** @type {((boolean|string))|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._cacheChangellyConnectResponse
/** @type {string|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorFrom
/** @type {string|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorTo
/** @type {number|undefined} */
$xyz.swapee.IChangellyConnector.SymbolsIn.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.SymbolsIn} */
xyz.swapee.IChangellyConnector.SymbolsIn

// nss:xyz.swapee,$xyz.swapee.IChangellyConnector
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.Symbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
$xyz.swapee.IChangellyConnector.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._cacheChangellyConnectResponse
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorFrom
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorTo
/** @type {symbol} */
$xyz.swapee.IChangellyConnector.Symbols.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.Symbols} */
xyz.swapee.IChangellyConnector.Symbols

// nss:xyz.swapee,$xyz.swapee.IChangellyConnector
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.SymbolsOut filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @record */
$xyz.swapee.IChangellyConnector.SymbolsOut = function() {}
/** @type {(boolean|string)} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._cacheChangellyConnectResponse
/** @type {string} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorFrom
/** @type {string} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorTo
/** @type {number} */
$xyz.swapee.IChangellyConnector.SymbolsOut.prototype._changellyConnectorAmount
/** @typedef {$xyz.swapee.IChangellyConnector.SymbolsOut} */
xyz.swapee.IChangellyConnector.SymbolsOut

// nss:xyz.swapee,$xyz.swapee.IChangellyConnector
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.setSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(!xyz.swapee.IChangellyConnector, !xyz.swapee.IChangellyConnector.SymbolsIn): void} */
xyz.swapee.IChangellyConnector.setSymbols

// nss:xyz.swapee
/* @typal-end */
/* @typal-type {types/design/IChangellyConnector.xml} xyz.swapee.IChangellyConnector.getSymbols filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {function(!xyz.swapee.IChangellyConnector): !xyz.swapee.IChangellyConnector.SymbolsOut} */
xyz.swapee.IChangellyConnector.getSymbols

// nss:xyz.swapee
/* @typal-end */