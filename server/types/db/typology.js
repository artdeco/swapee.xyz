/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.ISwapeeServerJoinpointModel': {
  'id': 47890481881,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeServerAspectsInstaller': {
  'id': 47890481882,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeServerAspectsPointcuts': {
  'id': 47890481883,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeServerAspects': {
  'id': 47890481884,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.IHyperSwapeeServer': {
  'id': 47890481885,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ISwapeeServer': {
  'id': 47890481886,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.BSwapeeServerAspects': {
  'id': 47890481887,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModelHyperslice': {
  'id': 47890481888,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModelBindingHyperslice': {
  'id': 47890481889,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayJoinpointModel': {
  'id': 478904818810,
  'symbols': {},
  'methods': {
   'beforeGetRpc': 1,
   'afterGetRpc': 2,
   'afterGetRpcThrows': 3,
   'afterGetRpcReturns': 4,
   'afterGetRpcCancels': 5,
   'beforeEachGetRpc': 6,
   'afterEachGetRpc': 7,
   'afterEachGetRpcReturns': 8,
   'immediatelyAfterGetRpc': 9,
   'beforeQueryRpc': 10,
   'afterQueryRpc': 11,
   'afterQueryRpcThrows': 12,
   'afterQueryRpcReturns': 13,
   'afterQueryRpcCancels': 14,
   'beforePutRpc': 15,
   'afterPutRpc': 16,
   'afterPutRpcThrows': 17,
   'afterPutRpcReturns': 18,
   'afterPutRpcCancels': 19,
   'immediatelyAfterPutRpc': 20,
   'beforePostRpc': 21,
   'afterPostRpc': 22,
   'afterPostRpcThrows': 23,
   'afterPostRpcReturns': 24,
   'afterPostRpcCancels': 25,
   'immediatelyAfterPostRpc': 26
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayAspectsInstaller': {
  'id': 478904818811,
  'symbols': {},
  'methods': {
   'getRpc': 1,
   'queryRpc': 2,
   'putRpc': 3,
   'postRpc': 4
  }
 },
 'xyz.swapee.ws.rpc.BRpcGatewayAspects': {
  'id': 478904818812,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayForwardingAspects': {
  'id': 478904818813,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayAspects': {
  'id': 478904818814,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IHyperRpcGateway': {
  'id': 478904818815,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayHyperslice': {
  'id': 478904818816,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayBindingHyperslice': {
  'id': 478904818817,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGateway': {
  'id': 478904818818,
  'symbols': {},
  'methods': {
   'getRpc': 1,
   'queryRpc': 2,
   'putRpc': 3,
   'postRpc': 4
  }
 },
 'xyz.swapee.IChangellyConnectorJoinpointModelHyperslice': {
  'id': 478904818819,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice': {
  'id': 478904818820,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.IChangellyConnectorJoinpointModel': {
  'id': 478904818821,
  'symbols': {},
  'methods': {
   'beforePing': 1,
   'afterPing': 2,
   'afterPingThrows': 3,
   'afterPingReturns': 4,
   'afterPingCancels': 5,
   'immediatelyAfterPing': 6,
   'beforePingChangelly': 7,
   'afterPingChangelly': 8,
   'afterPingChangellyThrows': 9,
   'afterPingChangellyReturns': 10,
   'afterPingChangellyCancels': 11,
   'immediatelyAfterPingChangelly': 12,
   'beforeConnectChangelly': 13,
   'afterConnectChangelly': 14,
   'afterConnectChangellyThrows': 15,
   'afterConnectChangellyReturns': 16,
   'afterConnectChangellyCancels': 17,
   'beforePostConnectChangelly': 18,
   'afterPostConnectChangelly': 19,
   'afterPostConnectChangellyThrows': 20,
   'afterPostConnectChangellyReturns': 21,
   'afterPostConnectChangellyCancels': 22,
   'immediatelyAfterPostConnectChangelly': 23,
   'immediatelyAfterConnectChangelly': 24
  }
 },
 'xyz.swapee.IChangellyConnectorAspectsInstaller': {
  'id': 478904818822,
  'symbols': {},
  'methods': {
   'ping': 1,
   'pingChangelly': 2,
   'connectChangelly': 3,
   'postConnectChangelly': 4
  }
 },
 'xyz.swapee.BChangellyConnectorAspects': {
  'id': 478904818823,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.IChangellyConnectorAspects': {
  'id': 478904818824,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.IHyperChangellyConnector': {
  'id': 478904818825,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.IChangellyConnector': {
  'id': 478904818826,
  'symbols': {
   'cacheChangellyConnectResponse': 1,
   'changellyConnectorFrom': 2,
   'changellyConnectorTo': 3,
   'changellyConnectorAmount': 4
  },
  'methods': {
   'ping': 5,
   'pingChangelly': 6,
   'connectChangelly': 7,
   'postConnectChangelly': 8
  }
 },
 'xyz.swapee.ws.rpc.IRpcGatewayLoggingAspects': {
  'id': 478904818827,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IRpcGatewayLoggingAide': {
  'id': 478904818828,
  'symbols': {
   'circuitsMap': 1
  },
  'methods': {}
 },
 'xyz.swapee.ws.rpc.ISwapeeRpcGatewayJoinpointModel': {
  'id': 478904818829,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspectsInstaller': {
  'id': 478904818830,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.BSwapeeRpcGatewayAspects': {
  'id': 478904818831,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.ws.rpc.ISwapeeRpcGatewayForwardingAspects': {
  'id': 478904818832,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.ISwapeeRpcGatewayAspects': {
  'id': 478904818833,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.IHyperSwapeeRpcGateway': {
  'id': 478904818834,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.ws.rpc.ISwapeeRpcGateway': {
  'id': 478904818835,
  'symbols': {},
  'methods': {}
 }
})