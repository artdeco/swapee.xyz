import {XMLReaderPluginV0} from '@type.engineering/plugin'

/**@extends {XMLReaderPluginV0}*/
export default class ParthenonPlugin extends XMLReaderPluginV0.__extend(
 /** @type {engineering.type.IPluginHyperslice}*/({
  getAdditionalImplementations({protype}) {
   const data=protype[PAGE_DATA]
   if(!data) return
   const {asIPlugin:{api:{
    // importTemplate:importTemplate,
   }}}=/**@type {XMLReaderPluginV0}*/(this)

   const R=[]

   // const{actions}=data
   // for(const actionKey of actions.keys()){
   //  const actionData=actions.get(actionKey)
   //  const{Pagelet}=actionData
   //  const Universal=`${Pagelet}Universal`
   //  importTemplate(Universal)
   //  R.push(Universal)
   // }
   return R
  },
 }),
 /** @type {engineering.type.IPluginHyperslice}*/({
  getAdditionalImplementations({protype}){
   const data=protype[PAGE_DATA]
   if(!data) return
   const {asIPlugin:{api:{
    importTemplate:importTemplate,
   }}}=/** @type {XMLReaderPluginV0}*/(this)

   const R=[]

   const{actions,Page}=data
   return
  },
  // getAdditionalImplementations:[
  //  function() {
  //   console.log(333)
  //  },
  // ],
 }),
 /** @type {XMLReaderPluginV0}*/({
  name:'Parthenon',
  fullName:'ms.websyste.Parthenon',
  tags:['is-pagelet','is-pagelet-view'],
  preprocess(node,opts) { // todo: combine preprocess in IPlugin
  },
  parseProtype(tags,{item:item,node:node},meta){
   // console.log(321,{viewMethodNode,ViewMethodNode,getMethodNode})
   for(let{tag:tag,props}of tags) {
    if(tag=='is-pagelet-view') {
     const[ViewMethodNode]=node.queryChildren('[parthenon_View]')
     const[viewMethodNode]=node.queryChildren('[parthenon_view]')
     const[getMethodNode]=node.queryChildren('[parthenon_get]')
     const[setCtxMethodNode]=node.queryChildren('[parthenon_setCtx]')

     item[PAGELET_DATA]={
      action:props['action'],
      View:ViewMethodNode?ViewMethodNode.getAttribute('name'):null,
      view:viewMethodNode.getAttribute('name'),
      get:getMethodNode.getAttribute('name'),
      setCtx:setCtxMethodNode?setCtxMethodNode.getAttribute('name'):'',
      IPagelet:props['pagelet'],
      IPageletView:meta.NAME,
      ...meta,
     }
     // console.log(100,item[PAGELET_DATA])
     // something something
    }
   }
   // need to assign data so that it can be included as additionalImplementation later...
  },
  itemInit(item,node) {
   if(PAGE_DATA in node) item[PAGE_DATA]=node[PAGE_DATA]
  },
  rootPreprocess(node,_,{NAME:NAME,NS,FILE}) {
   const actionNodes=node.queryChildren('[action]')
   if(!actionNodes.length) return

   const {
    asIPlugin: { registry: { upreadXML: upreadXML } },
    // asIXMLReaderPluginV0: { api: { makeNode } } ,
    // asICardsPlugin:{cardsDataMap:cardsDataMap},
   } = this

   const actions=new Map
   for(const actionNode of actionNodes) {
    const actionName=actionNode.getAttribute('name')
    const hasView=actionNode.hasAttribute('view')
    const Action=actionName.replace(/./,(m)=>m.toUpperCase())
    const Entity=actionName.replace(/^(open|pull)/,'')
    const viewAction=`view${actionName.replace(/./,m=>m.toUpperCase())}` // openScanner -> viewScanner -> ScannerView
    const View=`${Entity}View`
    const sourceAction=`${NAME}.${actionName}`
    const IPagelet=`${NAME}_${Action}`
    const Pagelet=IPagelet.replace(/I/,'')
    actions.set(actionName,{
     Pagelet:Pagelet,
     sourceAction:sourceAction,viewAction:viewAction,
     actionName:actionName,IPage:NAME,View:View,Action:Action,
     IPagelet:IPagelet,hasView:hasView,ns:NS,
    })
   }
   node[PAGE_DATA]={actions,Page:NAME.replace(/I/,'')}
   // const mainMenus=node.queryChildren('[main-menu]')
  },
  addProtypeCode({addTemplate,asFunction,className,protype,typalPath,writeAdjacentFile}){
   const data=protype[PAGE_DATA]
   if(!data) return

   const {asIPlugin:{api:{
    // importTemplate:importTemplate,
    importFrom:importFrom,
   }}}=/** @type {XMLReaderPluginV0}*/(this)

   // const{Page}=data
   // importFrom([`${Page}MethodsIds`],'./methods-ids')

   //    for(const actionKey of actions.keys()) {
   //     const{Pagelet:Pagelet,ns,IPagelet:IPagelet}=actions.get(actionKey)
   //     let s=''
   //     const AbstractPagelet=`Abstract${Pagelet}`
   //     s+= `/** @abstract {${ns}.${IPagelet}} */
   // `+`export default class ${AbstractPagelet} {}

  // `
  //     // +`
  //     // /** @extends {${ns}.${Pagelet}} */
  //     // `+`export default class Delayed${Pagelet} extends ${AbstractPagelet}.implements() {}
  //     // `
  //     addTemplate(`pagelets/${AbstractPagelet}`,s,AbstractPagelet,true)
  //    }
   // return
  },
  getAdditionalImplementations({protype}){
   const data=protype[PAGELET_DATA]
   if(!data) return
   // debugger
   // console.log(333,data)
   const{View:View,get:get,setCtx:setCtx,view:view,IPagelet:IPagelet,IPageletView,action:action}=data
   return{
    [get]:[{
     name:'assignCtx',
     body:`
const{as${IPagelet}:{${action}Ctx:${action}Ctx}}=this
Object.assign(ctx,${action}Ctx)
`.trim(),
     args:['ctx'],
    },{
     args:['ctx'],
     body:`const{as${IPageletView}:{${setCtx}:${setCtx}}}=this
let translation
const{locale:locale='en'}=ctx.answers
const{${action}Translations:${action}Translations}=this
if(!${action}Translations) return
if(!(locale in ${action}Translations)) {
 translation=${action}Translations['en']
 return
}
translation=${action}Translations[locale]
${setCtx}(ctx.answers,translation)`,
    },View?{
     args:['ctx'],
     body:`const{as${IPageletView}:{${View}:${View}}}=this
scheduleLast(this,()=>{
 if(ctx.redirected) return null
 return ${View}
})`,
    }:null].filter(Boolean),
    [view]:{
     args:['ctx','form'],
     body:(View?`
const{as${IPageletView}:{${get}:${get}}}=this

const ${View}=/**@type {!Function}*/(/**@type {!Array<!Function>}*/(/**@type {*}*/(${get}(ctx,form)))
 .find(a=>typeof a=='function'))

return ${View}`:`const{as${IPageletView}:{${get}:${get}}}=this

${get}(ctx,form)`).trim(),
    },
   }
  },
  // getAdditionalImplementations:[
  //  function(){
  //  },
  // ],
 }),
){}

const PAGELET_DATA=Symbol()
const PAGE_DATA=Symbol()

// then automatically generate abstract class for it.
//
// const makePageletXml=({
//  IPagelet:IPagelet,sourceAction:sourceAction,View:View,
//  actionName:actionName,Action:Action,viewAction:viewAction,IPage:IPage,hasView,
// },ns)=>`
// <types ns="${ns}">
//   <${IPagelet} module="server" aspects>

//   </${IPagelet}>
// </types>`

// add the above pagelet xml to the modulated pagelet

// console.log('parthenon loaded')