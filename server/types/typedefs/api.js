/** @nocompile */
eval(`var xyz={}
xyz.swapee={}`)

/* @typal-start {types/api.xml} functions a6aec48825b49b6dd71c2746d32b645e */
/** @typedef {typeof xyz.swapee.Swapee_Server} */
/**
 * The function to start the server.
 * @param {!xyz.swapee.Swapee_Server.Config} config Additional options for the program.
 * @return {!Promise}
 */
xyz.swapee.Swapee_Server = function(config) {}

// nss:xyz.swapee
/* @typal-end */