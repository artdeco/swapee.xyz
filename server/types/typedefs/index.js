/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.IChangellyConnector={}
xyz.swapee.IChangellyConnector.pingChangelly={}
xyz.swapee.IChangellyConnector.connectChangelly={}
xyz.swapee.IChangellyConnectorJoinpointModel={}
xyz.swapee.IChangellyConnectorAspectsInstaller={}
xyz.swapee.IChangellyConnectorAspects={}
xyz.swapee.IHyperChangellyConnector={}
xyz.swapee.ISwapeeServer={}
xyz.swapee.ISwapeeServerAspectsInstaller={}
xyz.swapee.ISwapeeServerAspects={}
xyz.swapee.IHyperSwapeeServer={}
xyz.swapee.Swapee_Server={}`)

/** */
var __$te_plain={}

/* @typal-start {types/design/IChangellyConnector.xml}  7a7e0756f4e46807b6a8d14081259b68 */
/**
 * @typedef {Object} $xyz.swapee.IChangellyConnector.Initialese
 * @prop {string} [changellyClientId] The client id for Changelly, reads from env variable if not passed.
 * @prop {string} [changellyClientSecret] The client secret for Changelly, reads from env variable if not passed.
 * @prop {boolean} [connectChangelly=true] Whether changelly should connect to start the server. Set to _false_ to
 * disable connectivity check. Default `true`.
 * @prop {boolean|string} [cacheChangellyConnectResponse] Whether to save _Changelly_ response in cache. If string is passed, uses
 * it as path to the file where to save latest responses.
 * @prop {string} [changellyConnectorFrom] The default currency to connect with to exchange from.
 * @prop {string} [changellyConnectorTo] The default currency to connect with to exchange into.
 * @prop {number} [changellyConnectorAmount] The amount for the exchange. If the default amount fails, it might be too
 * high and require manual change.
 */
/** @typedef {$xyz.swapee.IChangellyConnector.Initialese&engineer.type.IConnector.Initialese&com.changelly.UChangelly.Initialese} xyz.swapee.IChangellyConnector.Initialese */

/** @typedef {function(new: xyz.swapee.ChangellyConnector)} xyz.swapee.AbstractChangellyConnector.constructor */
/** @typedef {typeof xyz.swapee.ChangellyConnector} xyz.swapee.ChangellyConnector.typeof */
/**
 * An abstract class of `xyz.swapee.IChangellyConnector` interface.
 * @constructor xyz.swapee.AbstractChangellyConnector
 */
xyz.swapee.AbstractChangellyConnector = class extends /** @type {xyz.swapee.AbstractChangellyConnector.constructor&xyz.swapee.ChangellyConnector.typeof} */ (class {}) { }
xyz.swapee.AbstractChangellyConnector.prototype.constructor = xyz.swapee.AbstractChangellyConnector
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractChangellyConnector.class = /** @type {typeof xyz.swapee.AbstractChangellyConnector} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnector.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)|(!engineer.type.IConnector|typeof engineer.type.Connector)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.AbstractChangellyConnector.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.IChangellyConnectorJoinpointModelHyperslice
 */
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePing=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._beforePing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePing>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPing=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPing>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPingThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPingReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPingCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPing=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePingChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPingChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPingChangellyThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPingChangellyReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPingChangellyCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPingChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly>} */ (void 0)
    /**
     * After the method.
     */
    this.afterConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterConnectChangellyThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterConnectChangellyReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterConnectChangellyCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePostConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPostConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPostConnectChangellyThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPostConnectChangellyReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPostConnectChangellyCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPostConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly>} */ (void 0)
  }
}
xyz.swapee.IChangellyConnectorJoinpointModelHyperslice.prototype.constructor = xyz.swapee.IChangellyConnectorJoinpointModelHyperslice

/**
 * A concrete class of _IChangellyConnectorJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.ChangellyConnectorJoinpointModelHyperslice
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.ChangellyConnectorJoinpointModelHyperslice = class extends xyz.swapee.IChangellyConnectorJoinpointModelHyperslice { }
xyz.swapee.ChangellyConnectorJoinpointModelHyperslice.prototype.constructor = xyz.swapee.ChangellyConnectorJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePing=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPing=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPingThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPingReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPingCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPing=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePingChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPingChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPingChangellyThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPingChangellyReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPingChangellyCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPingChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterConnectChangellyThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterConnectChangellyReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterConnectChangellyCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePostConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPostConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPostConnectChangellyThrows=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPostConnectChangellyReturns=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPostConnectChangellyCancels=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPostConnectChangelly=/** @type {!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly<THIS>>} */ (void 0)
  }
}
xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice

/**
 * A concrete class of _IChangellyConnectorJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.ChangellyConnectorJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.ChangellyConnectorJoinpointModelBindingHyperslice = class extends xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice { }
xyz.swapee.ChangellyConnectorJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.ChangellyConnectorJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IChangellyConnector`'s methods.
 * @interface xyz.swapee.IChangellyConnectorJoinpointModel
 */
xyz.swapee.IChangellyConnectorJoinpointModel = class { }
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.beforePing} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePing = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPing} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPing = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingThrows = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingReturns = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingCancels = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPing = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePingChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyThrows = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyReturns = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPingChangellyCancels = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPingChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforeConnectChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyThrows = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyReturns = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterConnectChangellyCancels = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterConnectChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.beforePostConnectChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyThrows = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyReturns = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.afterPostConnectChangellyCancels = function() {}
/** @type {xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly} */
xyz.swapee.IChangellyConnectorJoinpointModel.prototype.immediatelyAfterPostConnectChangelly = function() {}

/**
 * A concrete class of _IChangellyConnectorJoinpointModel_ instances.
 * @constructor xyz.swapee.ChangellyConnectorJoinpointModel
 * @implements {xyz.swapee.IChangellyConnectorJoinpointModel} An interface that enumerates the joinpoints of `IChangellyConnector`'s methods.
 */
xyz.swapee.ChangellyConnectorJoinpointModel = class extends xyz.swapee.IChangellyConnectorJoinpointModel { }
xyz.swapee.ChangellyConnectorJoinpointModel.prototype.constructor = xyz.swapee.ChangellyConnectorJoinpointModel

/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel} */
xyz.swapee.RecordIChangellyConnectorJoinpointModel

/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel} xyz.swapee.BoundIChangellyConnectorJoinpointModel */

/** @typedef {xyz.swapee.ChangellyConnectorJoinpointModel} xyz.swapee.BoundChangellyConnectorJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.ChangellyConnectorAspectsInstaller)} xyz.swapee.AbstractChangellyConnectorAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.ChangellyConnectorAspectsInstaller} xyz.swapee.ChangellyConnectorAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.IChangellyConnectorAspectsInstaller` interface.
 * @constructor xyz.swapee.AbstractChangellyConnectorAspectsInstaller
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller = class extends /** @type {xyz.swapee.AbstractChangellyConnectorAspectsInstaller.constructor&xyz.swapee.ChangellyConnectorAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.prototype.constructor = xyz.swapee.AbstractChangellyConnectorAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.class = /** @type {typeof xyz.swapee.AbstractChangellyConnectorAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller|typeof xyz.swapee.ChangellyConnectorAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.AbstractChangellyConnectorAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese[]) => xyz.swapee.IChangellyConnectorAspectsInstaller} xyz.swapee.ChangellyConnectorAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.IChangellyConnectorAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.IChangellyConnectorAspectsInstaller */
xyz.swapee.IChangellyConnectorAspectsInstaller = class extends /** @type {xyz.swapee.IChangellyConnectorAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforePing=/** @type {number} */ (void 0)
    this.afterPing=/** @type {number} */ (void 0)
    this.afterPingThrows=/** @type {number} */ (void 0)
    this.afterPingReturns=/** @type {number} */ (void 0)
    this.afterPingCancels=/** @type {number} */ (void 0)
    this.immediateAfterPing=/** @type {number} */ (void 0)
    this.beforePingChangelly=/** @type {number} */ (void 0)
    this.afterPingChangelly=/** @type {number} */ (void 0)
    this.afterPingChangellyThrows=/** @type {number} */ (void 0)
    this.afterPingChangellyReturns=/** @type {number} */ (void 0)
    this.afterPingChangellyCancels=/** @type {number} */ (void 0)
    this.immediateAfterPingChangelly=/** @type {number} */ (void 0)
    this.beforeConnectChangelly=/** @type {number} */ (void 0)
    this.afterConnectChangelly=/** @type {number} */ (void 0)
    this.afterConnectChangellyThrows=/** @type {number} */ (void 0)
    this.afterConnectChangellyReturns=/** @type {number} */ (void 0)
    this.afterConnectChangellyCancels=/** @type {number} */ (void 0)
    this.immediateAfterConnectChangelly=/** @type {number} */ (void 0)
    this.beforePostConnectChangelly=/** @type {number} */ (void 0)
    this.afterPostConnectChangelly=/** @type {number} */ (void 0)
    this.afterPostConnectChangellyThrows=/** @type {number} */ (void 0)
    this.afterPostConnectChangellyReturns=/** @type {number} */ (void 0)
    this.afterPostConnectChangellyCancels=/** @type {number} */ (void 0)
    this.immediateAfterPostConnectChangelly=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {void}
   */
  ping() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  pingChangelly() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  connectChangelly() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  postConnectChangelly() { }
}
/**
 * Create a new *IChangellyConnectorAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.IChangellyConnectorAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.IChangellyConnectorAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese>)} xyz.swapee.ChangellyConnectorAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.IChangellyConnectorAspectsInstaller} xyz.swapee.IChangellyConnectorAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IChangellyConnectorAspectsInstaller_ instances.
 * @constructor xyz.swapee.ChangellyConnectorAspectsInstaller
 * @implements {xyz.swapee.IChangellyConnectorAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.ChangellyConnectorAspectsInstaller = class extends /** @type {xyz.swapee.ChangellyConnectorAspectsInstaller.constructor&xyz.swapee.IChangellyConnectorAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyConnectorAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyConnectorAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IChangellyConnectorAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.ChangellyConnectorAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspectsInstaller}
 */
xyz.swapee.ChangellyConnectorAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `ping` method from being executed.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `ping` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.PingChangellyNArgs
 * @prop {!xyz.swapee.IChangellyConnector.pingChangelly.Opts} opts
 */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.IChangellyConnector.PingChangellyNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.IChangellyConnector.PingChangellyNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `pingChangelly` method from being executed.
 * @prop {(value: !Promise<!Array<!com.changelly.ExchangeInfo>>) => void} sub Cancels a call to `pingChangelly` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData
 * @prop {!Array<!com.changelly.ExchangeInfo>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `pingChangelly` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData
 * @prop {!Array<!com.changelly.ExchangeInfo>} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<!Array<!com.changelly.ExchangeInfo>>|!Array<!com.changelly.ExchangeInfo>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData
 * @prop {!Promise<!Array<!com.changelly.ExchangeInfo>>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PingChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.ConnectChangellyNArgs
 * @prop {!xyz.swapee.IChangellyConnector.connectChangelly.Opts} opts
 */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.IChangellyConnector.ConnectChangellyNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.IChangellyConnector.ConnectChangellyNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `connectChangelly` method from being executed.
 * @prop {(value: !Promise<!Array<!com.changelly.ExchangeInfo>>) => void} sub Cancels a call to `connectChangelly` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData
 * @prop {!Array<!com.changelly.ExchangeInfo>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `connectChangelly` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData
 * @prop {!Array<!com.changelly.ExchangeInfo>} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<!Array<!com.changelly.ExchangeInfo>>|!Array<!com.changelly.ExchangeInfo>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData
 * @prop {!Promise<!Array<!com.changelly.ExchangeInfo>>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs
 * @prop {!Array<!com.changelly.ExchangeInfo>} connectResult The exchange info that the connection was tested with.
 */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.IChangellyConnector.PostConnectChangellyNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `postConnectChangelly` method from being executed.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `postConnectChangelly` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData&xyz.swapee.IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData} xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/** @typedef {new (...args: !xyz.swapee.IChangellyConnector.Initialese[]) => xyz.swapee.IChangellyConnector} xyz.swapee.ChangellyConnectorConstructor */

/** @typedef {function(new: xyz.swapee.BChangellyConnectorAspectsCaster<THIS>&xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.BChangellyConnectorAspects.constructor */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice} xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IChangellyConnector* that bind to an instance.
 * @interface xyz.swapee.BChangellyConnectorAspects
 * @template THIS
 */
xyz.swapee.BChangellyConnectorAspects = class extends /** @type {xyz.swapee.BChangellyConnectorAspects.constructor&xyz.swapee.IChangellyConnectorJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.BChangellyConnectorAspects.prototype.constructor = xyz.swapee.BChangellyConnectorAspects

/** @typedef {typeof __$te_plain} xyz.swapee.IChangellyConnectorAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.ChangellyConnectorAspects)} xyz.swapee.AbstractChangellyConnectorAspects.constructor */
/** @typedef {typeof xyz.swapee.ChangellyConnectorAspects} xyz.swapee.ChangellyConnectorAspects.typeof */
/**
 * An abstract class of `xyz.swapee.IChangellyConnectorAspects` interface.
 * @constructor xyz.swapee.AbstractChangellyConnectorAspects
 */
xyz.swapee.AbstractChangellyConnectorAspects = class extends /** @type {xyz.swapee.AbstractChangellyConnectorAspects.constructor&xyz.swapee.ChangellyConnectorAspects.typeof} */ (class {}) { }
xyz.swapee.AbstractChangellyConnectorAspects.prototype.constructor = xyz.swapee.AbstractChangellyConnectorAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractChangellyConnectorAspects.class = /** @type {typeof xyz.swapee.AbstractChangellyConnectorAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 * @nosideeffects
 */
xyz.swapee.AbstractChangellyConnectorAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.IChangellyConnectorAspects|typeof xyz.swapee.ChangellyConnectorAspects)|(!xyz.swapee.BChangellyConnectorAspects|typeof xyz.swapee.BChangellyConnectorAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.AbstractChangellyConnectorAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.IChangellyConnectorAspects.Initialese[]) => xyz.swapee.IChangellyConnectorAspects} xyz.swapee.ChangellyConnectorAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.IChangellyConnectorAspectsCaster&xyz.swapee.BChangellyConnectorAspects<!xyz.swapee.IChangellyConnectorAspects>)} xyz.swapee.IChangellyConnectorAspects.constructor */
/** @typedef {typeof xyz.swapee.BChangellyConnectorAspects} xyz.swapee.BChangellyConnectorAspects.typeof */
/**
 * The aspects of the *IChangellyConnector*.
 * @interface xyz.swapee.IChangellyConnectorAspects
 */
xyz.swapee.IChangellyConnectorAspects = class extends /** @type {xyz.swapee.IChangellyConnectorAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.BChangellyConnectorAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IChangellyConnectorAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init The initialisation options.
 */
xyz.swapee.IChangellyConnectorAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.IChangellyConnectorAspects&engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspects.Initialese>)} xyz.swapee.ChangellyConnectorAspects.constructor */
/** @typedef {typeof xyz.swapee.IChangellyConnectorAspects} xyz.swapee.IChangellyConnectorAspects.typeof */
/**
 * A concrete class of _IChangellyConnectorAspects_ instances.
 * @constructor xyz.swapee.ChangellyConnectorAspects
 * @implements {xyz.swapee.IChangellyConnectorAspects} The aspects of the *IChangellyConnector*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnectorAspects.Initialese>} ‎
 */
xyz.swapee.ChangellyConnectorAspects = class extends /** @type {xyz.swapee.ChangellyConnectorAspects.constructor&xyz.swapee.IChangellyConnectorAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyConnectorAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyConnectorAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IChangellyConnectorAspects.Initialese} init The initialisation options.
 */
xyz.swapee.ChangellyConnectorAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnectorAspects}
 */
xyz.swapee.ChangellyConnectorAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BChangellyConnectorAspects_ interface.
 * @interface xyz.swapee.BChangellyConnectorAspectsCaster
 * @template THIS
 */
xyz.swapee.BChangellyConnectorAspectsCaster = class { }
/**
 * Cast the _BChangellyConnectorAspects_ instance into the _BoundIChangellyConnector_ type.
 * @type {!xyz.swapee.BoundIChangellyConnector}
 */
xyz.swapee.BChangellyConnectorAspectsCaster.prototype.asIChangellyConnector

/**
 * Contains getters to cast the _IChangellyConnectorAspects_ interface.
 * @interface xyz.swapee.IChangellyConnectorAspectsCaster
 */
xyz.swapee.IChangellyConnectorAspectsCaster = class { }
/**
 * Cast the _IChangellyConnectorAspects_ instance into the _BoundIChangellyConnector_ type.
 * @type {!xyz.swapee.BoundIChangellyConnector}
 */
xyz.swapee.IChangellyConnectorAspectsCaster.prototype.asIChangellyConnector

/** @typedef {xyz.swapee.IChangellyConnector.Initialese} xyz.swapee.IHyperChangellyConnector.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.HyperChangellyConnector)} xyz.swapee.AbstractHyperChangellyConnector.constructor */
/** @typedef {typeof xyz.swapee.HyperChangellyConnector} xyz.swapee.HyperChangellyConnector.typeof */
/**
 * An abstract class of `xyz.swapee.IHyperChangellyConnector` interface.
 * @constructor xyz.swapee.AbstractHyperChangellyConnector
 */
xyz.swapee.AbstractHyperChangellyConnector = class extends /** @type {xyz.swapee.AbstractHyperChangellyConnector.constructor&xyz.swapee.HyperChangellyConnector.typeof} */ (class {}) { }
xyz.swapee.AbstractHyperChangellyConnector.prototype.constructor = xyz.swapee.AbstractHyperChangellyConnector
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractHyperChangellyConnector.class = /** @type {typeof xyz.swapee.AbstractHyperChangellyConnector} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.IHyperChangellyConnector|typeof xyz.swapee.HyperChangellyConnector)|(!xyz.swapee.IChangellyConnector|typeof xyz.swapee.ChangellyConnector)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.AbstractHyperChangellyConnector.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.IChangellyConnectorAspects|function(new: xyz.swapee.IChangellyConnectorAspects)} aides The list of aides that advise the IChangellyConnector to implement aspects.
 * @return {typeof xyz.swapee.AbstractHyperChangellyConnector}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperChangellyConnector.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.IHyperChangellyConnector.Initialese[]) => xyz.swapee.IHyperChangellyConnector} xyz.swapee.HyperChangellyConnectorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.IHyperChangellyConnectorCaster&xyz.swapee.IChangellyConnector)} xyz.swapee.IHyperChangellyConnector.constructor */
/** @typedef {typeof xyz.swapee.IChangellyConnector} xyz.swapee.IChangellyConnector.typeof */
/** @interface xyz.swapee.IHyperChangellyConnector */
xyz.swapee.IHyperChangellyConnector = class extends /** @type {xyz.swapee.IHyperChangellyConnector.constructor&engineering.type.IEngineer.typeof&xyz.swapee.IChangellyConnector.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperChangellyConnector* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init The initialisation options.
 */
xyz.swapee.IHyperChangellyConnector.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.IHyperChangellyConnector&engineering.type.IInitialiser<!xyz.swapee.IHyperChangellyConnector.Initialese>)} xyz.swapee.HyperChangellyConnector.constructor */
/** @typedef {typeof xyz.swapee.IHyperChangellyConnector} xyz.swapee.IHyperChangellyConnector.typeof */
/**
 * A concrete class of _IHyperChangellyConnector_ instances.
 * @constructor xyz.swapee.HyperChangellyConnector
 * @implements {xyz.swapee.IHyperChangellyConnector} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IHyperChangellyConnector.Initialese>} ‎
 */
xyz.swapee.HyperChangellyConnector = class extends /** @type {xyz.swapee.HyperChangellyConnector.constructor&xyz.swapee.IHyperChangellyConnector.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperChangellyConnector* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperChangellyConnector* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IHyperChangellyConnector.Initialese} init The initialisation options.
 */
xyz.swapee.HyperChangellyConnector.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperChangellyConnector}
 */
xyz.swapee.HyperChangellyConnector.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.IHyperChangellyConnector} */
xyz.swapee.RecordIHyperChangellyConnector

/** @typedef {xyz.swapee.IHyperChangellyConnector} xyz.swapee.BoundIHyperChangellyConnector */

/** @typedef {xyz.swapee.HyperChangellyConnector} xyz.swapee.BoundHyperChangellyConnector */

/**
 * Contains getters to cast the _IHyperChangellyConnector_ interface.
 * @interface xyz.swapee.IHyperChangellyConnectorCaster
 */
xyz.swapee.IHyperChangellyConnectorCaster = class { }
/**
 * Cast the _IHyperChangellyConnector_ instance into the _BoundIHyperChangellyConnector_ type.
 * @type {!xyz.swapee.BoundIHyperChangellyConnector}
 */
xyz.swapee.IHyperChangellyConnectorCaster.prototype.asIHyperChangellyConnector
/**
 * Access the _HyperChangellyConnector_ prototype.
 * @type {!xyz.swapee.BoundHyperChangellyConnector}
 */
xyz.swapee.IHyperChangellyConnectorCaster.prototype.superHyperChangellyConnector

/** @typedef {function(new: xyz.swapee.IChangellyConnectorFields&engineering.type.IEngineer&xyz.swapee.IChangellyConnectorCaster&engineer.type.IConnector&com.changelly.UChangelly)} xyz.swapee.IChangellyConnector.constructor */
/** @typedef {typeof engineer.type.IConnector} engineer.type.IConnector.typeof */
/** @typedef {typeof com.changelly.UChangelly} com.changelly.UChangelly.typeof */
/**
 * Connects to _Changelly_.
 * @interface xyz.swapee.IChangellyConnector
 */
xyz.swapee.IChangellyConnector = class extends /** @type {xyz.swapee.IChangellyConnector.constructor&engineering.type.IEngineer.typeof&engineer.type.IConnector.typeof&com.changelly.UChangelly.typeof} */ (class {}) {
}
/**
 * Create a new *IChangellyConnector* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IChangellyConnector.Initialese} init The initialisation options.
 */
xyz.swapee.IChangellyConnector.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.IChangellyConnector.ping} */
xyz.swapee.IChangellyConnector.prototype.ping = function() {}
/** @type {xyz.swapee.IChangellyConnector.pingChangelly} */
xyz.swapee.IChangellyConnector.prototype.pingChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnector.connectChangelly} */
xyz.swapee.IChangellyConnector.prototype.connectChangelly = function() {}
/** @type {xyz.swapee.IChangellyConnector.postConnectChangelly} */
xyz.swapee.IChangellyConnector.prototype.postConnectChangelly = function() {}

/** @typedef {function(new: xyz.swapee.IChangellyConnector&engineering.type.IInitialiser<!xyz.swapee.IChangellyConnector.Initialese>)} xyz.swapee.ChangellyConnector.constructor */
/**
 * A concrete class of _IChangellyConnector_ instances.
 * @constructor xyz.swapee.ChangellyConnector
 * @implements {xyz.swapee.IChangellyConnector} Connects to _Changelly_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IChangellyConnector.Initialese>} ‎
 */
xyz.swapee.ChangellyConnector = class extends /** @type {xyz.swapee.ChangellyConnector.constructor&xyz.swapee.IChangellyConnector.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyConnector* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.IChangellyConnector.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyConnector* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IChangellyConnector.Initialese} init The initialisation options.
 */
xyz.swapee.ChangellyConnector.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.ChangellyConnector}
 */
xyz.swapee.ChangellyConnector.__extend = function(...Extensions) {}

/**
 * Fields of the IChangellyConnector.
 * @interface xyz.swapee.IChangellyConnectorFields
 */
xyz.swapee.IChangellyConnectorFields = class { }
/**
 * Whether to save _Changelly_ response in cache. If string is passed, uses
 * it as path to the file where to save latest responses. Default `true`.
 */
xyz.swapee.IChangellyConnectorFields.prototype.cacheChangellyConnectResponse = /** @type {boolean|string} */ (void 0)
/**
 * The default currency to connect with to exchange from. Default `btc`.
 */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorFrom = /** @type {string} */ (void 0)
/**
 * The default currency to connect with to exchange into. Default `eth`.
 */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorTo = /** @type {string} */ (void 0)
/**
 * The amount for the exchange. If the default amount fails, it might be too
 * high and require manual change. Default `1`.
 */
xyz.swapee.IChangellyConnectorFields.prototype.changellyConnectorAmount = /** @type {number} */ (void 0)

/** @typedef {xyz.swapee.IChangellyConnector} */
xyz.swapee.RecordIChangellyConnector

/** @typedef {xyz.swapee.IChangellyConnector} xyz.swapee.BoundIChangellyConnector */

/** @typedef {xyz.swapee.ChangellyConnector} xyz.swapee.BoundChangellyConnector */

/**
 * Contains getters to cast the _IChangellyConnector_ interface.
 * @interface xyz.swapee.IChangellyConnectorCaster
 */
xyz.swapee.IChangellyConnectorCaster = class { }
/**
 * Cast the _IChangellyConnector_ instance into the _BoundIChangellyConnector_ type.
 * @type {!xyz.swapee.BoundIChangellyConnector}
 */
xyz.swapee.IChangellyConnectorCaster.prototype.asIChangellyConnector
/**
 * Access the _ChangellyConnector_ prototype.
 * @type {!xyz.swapee.BoundChangellyConnector}
 */
xyz.swapee.IChangellyConnectorCaster.prototype.superChangellyConnector

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__beforePing<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._beforePing */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.beforePing} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingPointcutData} [data] Metadata passed to the pointcuts of _ping_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `ping` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePing = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPing<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPing */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPing} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingPointcutData} [data] Metadata passed to the pointcuts of _ping_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPing = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingThrows<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPingThrows */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingPointcutData} [data] Metadata passed to the pointcuts of _ping_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `ping` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingReturns<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPingReturns */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingPointcutData} [data] Metadata passed to the pointcuts of _ping_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingCancels<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPingCancels */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingPointcutData} [data] Metadata passed to the pointcuts of _ping_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPing<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPing */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingPointcutData} [data] Metadata passed to the pointcuts of _ping_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPing = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__beforePingChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._beforePingChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePingChangellyPointcutData} [data] Metadata passed to the pointcuts of _pingChangelly_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyConnector.PingChangellyNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `pingChangelly` method from being executed.
 * - `sub` _(value: !Promise&lt;!Array&lt;!com.changelly.ExchangeInfo&gt;&gt;) =&gt; void_ Cancels a call to `pingChangelly` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `args` _IChangellyConnector.PingChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePingChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPingChangellyPointcutData} [data] Metadata passed to the pointcuts of _pingChangelly_ at the `after` joinpoint.
 * - `res` _!Array&lt;!com.changelly.ExchangeInfo&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `args` _IChangellyConnector.PingChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyThrows<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyThrows */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPingChangellyPointcutData} [data] Metadata passed to the pointcuts of _pingChangelly_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `pingChangelly` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `args` _IChangellyConnector.PingChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyReturns<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyReturns */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPingChangellyPointcutData} [data] Metadata passed to the pointcuts of _pingChangelly_ at the `afterReturns` joinpoint.
 * - `res` _!Array&lt;!com.changelly.ExchangeInfo&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;!Array&lt;!com.changelly.ExchangeInfo&gt;&gt;&vert;!Array&lt;!com.changelly.ExchangeInfo&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `args` _IChangellyConnector.PingChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPingChangellyCancels<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPingChangellyCancels */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPingChangellyPointcutData} [data] Metadata passed to the pointcuts of _pingChangelly_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `args` _IChangellyConnector.PingChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPingChangellyCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPingChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPingChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPingChangellyPointcutData} [data] Metadata passed to the pointcuts of _pingChangelly_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;!Array&lt;!com.changelly.ExchangeInfo&gt;&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `args` _IChangellyConnector.PingChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PingChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPingChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__beforeConnectChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._beforeConnectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforeConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _connectChangelly_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyConnector.ConnectChangellyNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `connectChangelly` method from being executed.
 * - `sub` _(value: !Promise&lt;!Array&lt;!com.changelly.ExchangeInfo&gt;&gt;) =&gt; void_ Cancels a call to `connectChangelly` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.ConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.beforeConnectChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _connectChangelly_ at the `after` joinpoint.
 * - `res` _!Array&lt;!com.changelly.ExchangeInfo&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.ConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyThrows<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyThrows */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _connectChangelly_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `connectChangelly` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.ConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyReturns<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyReturns */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _connectChangelly_ at the `afterReturns` joinpoint.
 * - `res` _!Array&lt;!com.changelly.ExchangeInfo&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;!Array&lt;!com.changelly.ExchangeInfo&gt;&gt;&vert;!Array&lt;!com.changelly.ExchangeInfo&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.ConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterConnectChangellyCancels<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterConnectChangellyCancels */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _connectChangelly_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.ConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterConnectChangellyCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterConnectChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterConnectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _connectChangelly_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;!Array&lt;!com.changelly.ExchangeInfo&gt;&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.ConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.ConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterConnectChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__beforePostConnectChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._beforePostConnectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.BeforePostConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _postConnectChangelly_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyConnector.PostConnectChangellyNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `postConnectChangelly` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.PostConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.beforePostConnectChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterPostConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _postConnectChangelly_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.PostConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangelly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyThrows<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyThrows */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterThrowsPostConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _postConnectChangelly_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `postConnectChangelly` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.PostConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyReturns<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyReturns */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterReturnsPostConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _postConnectChangelly_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.PostConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__afterPostConnectChangellyCancels<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._afterPostConnectChangellyCancels */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.AfterCancelsPostConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _postConnectChangelly_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.PostConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.afterPostConnectChangellyCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData) => void} xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnectorJoinpointModel.__immediatelyAfterPostConnectChangelly<!xyz.swapee.IChangellyConnectorJoinpointModel>} xyz.swapee.IChangellyConnectorJoinpointModel._immediatelyAfterPostConnectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!xyz.swapee.IChangellyConnectorJoinpointModel.ImmediatelyAfterPostConnectChangellyPointcutData} [data] Metadata passed to the pointcuts of _postConnectChangelly_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `args` _IChangellyConnector.PostConnectChangellyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyConnectorJoinpointModel.PostConnectChangellyPointcutData*
 * @return {void}
 */
xyz.swapee.IChangellyConnectorJoinpointModel.immediatelyAfterPostConnectChangelly = function(data) {}

/**
 * @typedef {(this: THIS) => !Promise} xyz.swapee.IChangellyConnector.__ping
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnector.__ping<!xyz.swapee.IChangellyConnector>} xyz.swapee.IChangellyConnector._ping */
/** @typedef {typeof xyz.swapee.IChangellyConnector.ping} */
/**
 * To ping.
 * @return {!Promise}
 */
xyz.swapee.IChangellyConnector.ping = function() {}

/**
 * @typedef {(this: THIS, opts?: !xyz.swapee.IChangellyConnector.pingChangelly.Opts) => !Promise<!Array<!com.changelly.ExchangeInfo>>} xyz.swapee.IChangellyConnector.__pingChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnector.__pingChangelly<!xyz.swapee.IChangellyConnector>} xyz.swapee.IChangellyConnector._pingChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnector.pingChangelly} */
/**
 * After the initial connection has been established, tests for connection
 * maintenance and correctness of keys. This method is spawned by the
 * IConnector's _ping_ method.
 * @param {!xyz.swapee.IChangellyConnector.pingChangelly.Opts} [opts]
 * - `[from="btc"]` _string?_ Default `btc`.
 * - `[to="eth"]` _string?_ Default `eth`.
 * - `[amount=1]` _number?_ Default `1`.
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>} The exchange info that the connection was tested with.
 */
xyz.swapee.IChangellyConnector.pingChangelly = function(opts) {}

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.pingChangelly.Opts
 * @prop {string} [from="btc"] Default `btc`.
 * @prop {string} [to="eth"] Default `eth`.
 * @prop {number} [amount=1] Default `1`.
 */

/**
 * @typedef {(this: THIS, opts?: !xyz.swapee.IChangellyConnector.connectChangelly.Opts) => !Promise<!Array<!com.changelly.ExchangeInfo>>} xyz.swapee.IChangellyConnector.__connectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnector.__connectChangelly<!xyz.swapee.IChangellyConnector>} xyz.swapee.IChangellyConnector._connectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnector.connectChangelly} */
/**
 * Tests the connection to the _Changelly_ servers and the client keys.
 * @param {!xyz.swapee.IChangellyConnector.connectChangelly.Opts} [opts]
 * - `[from="btc"]` _string?_ Default `btc`.
 * - `[to="eth"]` _string?_ Default `eth`.
 * - `[amount=1]` _number?_ Default `1`.
 * @return {!Promise<!Array<!com.changelly.ExchangeInfo>>} The exchange info that the connection was tested with.
 */
xyz.swapee.IChangellyConnector.connectChangelly = function(opts) {}

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.connectChangelly.Opts
 * @prop {string} [from="btc"] Default `btc`.
 * @prop {string} [to="eth"] Default `eth`.
 * @prop {number} [amount=1] Default `1`.
 */

/**
 * @typedef {(this: THIS, connectResult: !Array<!com.changelly.ExchangeInfo>) => !Promise<void>} xyz.swapee.IChangellyConnector.__postConnectChangelly
 * @template THIS
 */
/** @typedef {xyz.swapee.IChangellyConnector.__postConnectChangelly<!xyz.swapee.IChangellyConnector>} xyz.swapee.IChangellyConnector._postConnectChangelly */
/** @typedef {typeof xyz.swapee.IChangellyConnector.postConnectChangelly} */
/**
 * Fired when changelly has been connected.
 * @param {!Array<!com.changelly.ExchangeInfo>} connectResult The exchange info that the connection was tested with.
 * @return {!Promise<void>}
 */
xyz.swapee.IChangellyConnector.postConnectChangelly = function(connectResult) {}

// nss:xyz.swapee,xyz.swapee.IChangellyConnectorJoinpointModel,xyz.swapee.IChangellyConnector
/* @typal-end */
/* @typal-start {types/design/ISwapeeServer.xml}  3fb5c3313ab1ac39fc87b88dcbf293e0 */
/** @typedef {_idio.IServer.Initialese} xyz.swapee.ISwapeeServer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.SwapeeServer)} xyz.swapee.AbstractSwapeeServer.constructor */
/** @typedef {typeof xyz.swapee.SwapeeServer} xyz.swapee.SwapeeServer.typeof */
/**
 * An abstract class of `xyz.swapee.ISwapeeServer` interface.
 * @constructor xyz.swapee.AbstractSwapeeServer
 */
xyz.swapee.AbstractSwapeeServer = class extends /** @type {xyz.swapee.AbstractSwapeeServer.constructor&xyz.swapee.SwapeeServer.typeof} */ (class {}) { }
xyz.swapee.AbstractSwapeeServer.prototype.constructor = xyz.swapee.AbstractSwapeeServer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractSwapeeServer.class = /** @type {typeof xyz.swapee.AbstractSwapeeServer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)|(!_idio.IServer|typeof _idio.Server)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServer}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeServer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractSwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)|(!_idio.IServer|typeof _idio.Server)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)|(!_idio.IServer|typeof _idio.Server)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.AbstractSwapeeServer.__trait = function(...Implementations) {}

/**
 * An interface that enumerates the joinpoints of `ISwapeeServer`'s methods.
 * @interface xyz.swapee.ISwapeeServerJoinpointModel
 */
xyz.swapee.ISwapeeServerJoinpointModel = class { }
xyz.swapee.ISwapeeServerJoinpointModel.prototype.constructor = xyz.swapee.ISwapeeServerJoinpointModel

/**
 * A concrete class of _ISwapeeServerJoinpointModel_ instances.
 * @constructor xyz.swapee.SwapeeServerJoinpointModel
 * @implements {xyz.swapee.ISwapeeServerJoinpointModel} An interface that enumerates the joinpoints of `ISwapeeServer`'s methods.
 */
xyz.swapee.SwapeeServerJoinpointModel = class extends xyz.swapee.ISwapeeServerJoinpointModel { }
xyz.swapee.SwapeeServerJoinpointModel.prototype.constructor = xyz.swapee.SwapeeServerJoinpointModel

/** @typedef {xyz.swapee.ISwapeeServerJoinpointModel} */
xyz.swapee.RecordISwapeeServerJoinpointModel

/** @typedef {xyz.swapee.ISwapeeServerJoinpointModel} xyz.swapee.BoundISwapeeServerJoinpointModel */

/** @typedef {xyz.swapee.SwapeeServerJoinpointModel} xyz.swapee.BoundSwapeeServerJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.ISwapeeServerAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.SwapeeServerAspectsInstaller)} xyz.swapee.AbstractSwapeeServerAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.SwapeeServerAspectsInstaller} xyz.swapee.SwapeeServerAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.ISwapeeServerAspectsInstaller` interface.
 * @constructor xyz.swapee.AbstractSwapeeServerAspectsInstaller
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller = class extends /** @type {xyz.swapee.AbstractSwapeeServerAspectsInstaller.constructor&xyz.swapee.SwapeeServerAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.AbstractSwapeeServerAspectsInstaller.prototype.constructor = xyz.swapee.AbstractSwapeeServerAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.class = /** @type {typeof xyz.swapee.AbstractSwapeeServerAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.ISwapeeServerAspectsInstaller|typeof xyz.swapee.SwapeeServerAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractSwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.ISwapeeServerAspectsInstaller|typeof xyz.swapee.SwapeeServerAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.ISwapeeServerAspectsInstaller|typeof xyz.swapee.SwapeeServerAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.AbstractSwapeeServerAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.ISwapeeServerAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.ISwapeeServerAspectsInstaller */
xyz.swapee.ISwapeeServerAspectsInstaller = class extends /** @type {xyz.swapee.ISwapeeServerAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.ISwapeeServerAspectsInstaller.prototype.constructor = xyz.swapee.ISwapeeServerAspectsInstaller

/** @typedef {function(new: xyz.swapee.ISwapeeServerAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.ISwapeeServerAspectsInstaller.Initialese>)} xyz.swapee.SwapeeServerAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeServerAspectsInstaller} xyz.swapee.ISwapeeServerAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeServerAspectsInstaller_ instances.
 * @constructor xyz.swapee.SwapeeServerAspectsInstaller
 * @implements {xyz.swapee.ISwapeeServerAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeServerAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.SwapeeServerAspectsInstaller = class extends /** @type {xyz.swapee.SwapeeServerAspectsInstaller.constructor&xyz.swapee.ISwapeeServerAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.SwapeeServerAspectsInstaller.prototype.constructor = xyz.swapee.SwapeeServerAspectsInstaller
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServerAspectsInstaller}
 */
xyz.swapee.SwapeeServerAspectsInstaller.__extend = function(...Extensions) {}

/** @typedef {new (...args: !xyz.swapee.ISwapeeServer.Initialese[]) => xyz.swapee.ISwapeeServer} xyz.swapee.SwapeeServerConstructor */

/** @typedef {typeof __$te_plain} xyz.swapee.ISwapeeServerAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.SwapeeServerAspects)} xyz.swapee.AbstractSwapeeServerAspects.constructor */
/** @typedef {typeof xyz.swapee.SwapeeServerAspects} xyz.swapee.SwapeeServerAspects.typeof */
/**
 * An abstract class of `xyz.swapee.ISwapeeServerAspects` interface.
 * @constructor xyz.swapee.AbstractSwapeeServerAspects
 */
xyz.swapee.AbstractSwapeeServerAspects = class extends /** @type {xyz.swapee.AbstractSwapeeServerAspects.constructor&xyz.swapee.SwapeeServerAspects.typeof} */ (class {}) { }
xyz.swapee.AbstractSwapeeServerAspects.prototype.constructor = xyz.swapee.AbstractSwapeeServerAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractSwapeeServerAspects.class = /** @type {typeof xyz.swapee.AbstractSwapeeServerAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.ISwapeeServerAspects|typeof xyz.swapee.SwapeeServerAspects} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 * @nosideeffects
 */
xyz.swapee.AbstractSwapeeServerAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractSwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.ISwapeeServerAspects|typeof xyz.swapee.SwapeeServerAspects} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.ISwapeeServerAspects|typeof xyz.swapee.SwapeeServerAspects} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.AbstractSwapeeServerAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.ISwapeeServerAspects.Initialese[]) => xyz.swapee.ISwapeeServerAspects} xyz.swapee.SwapeeServerAspectsConstructor */

/** @typedef {function(new: xyz.swapee.ISwapeeServerJoinpointModel)} xyz.swapee.ISwapeeServerAspectsPointcuts.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeServerJoinpointModel} xyz.swapee.ISwapeeServerJoinpointModel.typeof */
/**
 * Pointcuts for _ISwapeeServerAspects_ to hook onto at joinpoints with its business process _ISwapeeServer_.
 * @interface xyz.swapee.ISwapeeServerAspectsPointcuts
 */
xyz.swapee.ISwapeeServerAspectsPointcuts = class extends /** @type {xyz.swapee.ISwapeeServerAspectsPointcuts.constructor&xyz.swapee.ISwapeeServerJoinpointModel.typeof} */ (class {}) { }
xyz.swapee.ISwapeeServerAspectsPointcuts.prototype.constructor = xyz.swapee.ISwapeeServerAspectsPointcuts

/**
 * A concrete class of _ISwapeeServerAspectsPointcuts_ instances.
 * @constructor xyz.swapee.SwapeeServerAspectsPointcuts
 * @implements {xyz.swapee.ISwapeeServerAspectsPointcuts} Pointcuts for _ISwapeeServerAspects_ to hook onto at joinpoints with its business process _ISwapeeServer_.
 */
xyz.swapee.SwapeeServerAspectsPointcuts = class extends xyz.swapee.ISwapeeServerAspectsPointcuts { }
xyz.swapee.SwapeeServerAspectsPointcuts.prototype.constructor = xyz.swapee.SwapeeServerAspectsPointcuts

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.ISwapeeServerAspectsCaster)} xyz.swapee.ISwapeeServerAspects.constructor */
/**
 * The aspects of the *ISwapeeServer*.
 * @interface xyz.swapee.ISwapeeServerAspects
 */
xyz.swapee.ISwapeeServerAspects = class extends /** @type {xyz.swapee.ISwapeeServerAspects.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeServerAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeServerAspects.Initialese} init The initialisation options.
 */
xyz.swapee.ISwapeeServerAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.ISwapeeServerAspects&engineering.type.IInitialiser<!xyz.swapee.ISwapeeServerAspects.Initialese>)} xyz.swapee.SwapeeServerAspects.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeServerAspects} xyz.swapee.ISwapeeServerAspects.typeof */
/**
 * A concrete class of _ISwapeeServerAspects_ instances.
 * @constructor xyz.swapee.SwapeeServerAspects
 * @implements {xyz.swapee.ISwapeeServerAspects} The aspects of the *ISwapeeServer*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeServerAspects.Initialese>} ‎
 */
xyz.swapee.SwapeeServerAspects = class extends /** @type {xyz.swapee.SwapeeServerAspects.constructor&xyz.swapee.ISwapeeServerAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeServerAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ISwapeeServerAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeServerAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeServerAspects.Initialese} init The initialisation options.
 */
xyz.swapee.SwapeeServerAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServerAspects}
 */
xyz.swapee.SwapeeServerAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _ISwapeeServerAspects_ interface.
 * @interface xyz.swapee.ISwapeeServerAspectsCaster
 */
xyz.swapee.ISwapeeServerAspectsCaster = class { }
/**
 * Cast the _ISwapeeServerAspects_ instance into the _BoundISwapeeServer_ type.
 * @type {!xyz.swapee.BoundISwapeeServer}
 */
xyz.swapee.ISwapeeServerAspectsCaster.prototype.asISwapeeServer

/** @typedef {xyz.swapee.ISwapeeServer.Initialese} xyz.swapee.IHyperSwapeeServer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.HyperSwapeeServer)} xyz.swapee.AbstractHyperSwapeeServer.constructor */
/** @typedef {typeof xyz.swapee.HyperSwapeeServer} xyz.swapee.HyperSwapeeServer.typeof */
/**
 * An abstract class of `xyz.swapee.IHyperSwapeeServer` interface.
 * @constructor xyz.swapee.AbstractHyperSwapeeServer
 */
xyz.swapee.AbstractHyperSwapeeServer = class extends /** @type {xyz.swapee.AbstractHyperSwapeeServer.constructor&xyz.swapee.HyperSwapeeServer.typeof} */ (class {}) { }
xyz.swapee.AbstractHyperSwapeeServer.prototype.constructor = xyz.swapee.AbstractHyperSwapeeServer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.AbstractHyperSwapeeServer.class = /** @type {typeof xyz.swapee.AbstractHyperSwapeeServer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.IHyperSwapeeServer|typeof xyz.swapee.HyperSwapeeServer)|(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapeeServer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.AbstractHyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.IHyperSwapeeServer|typeof xyz.swapee.HyperSwapeeServer)|(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.IHyperSwapeeServer|typeof xyz.swapee.HyperSwapeeServer)|(!xyz.swapee.ISwapeeServer|typeof xyz.swapee.SwapeeServer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.AbstractHyperSwapeeServer.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.ISwapeeServerAspects|function(new: xyz.swapee.ISwapeeServerAspects)} aides The list of aides that advise the ISwapeeServer to implement aspects.
 * @return {typeof xyz.swapee.AbstractHyperSwapeeServer}
 * @nosideeffects
 */
xyz.swapee.AbstractHyperSwapeeServer.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.IHyperSwapeeServer.Initialese[]) => xyz.swapee.IHyperSwapeeServer} xyz.swapee.HyperSwapeeServerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.IHyperSwapeeServerCaster&xyz.swapee.ISwapeeServer)} xyz.swapee.IHyperSwapeeServer.constructor */
/** @typedef {typeof xyz.swapee.ISwapeeServer} xyz.swapee.ISwapeeServer.typeof */
/** @interface xyz.swapee.IHyperSwapeeServer */
xyz.swapee.IHyperSwapeeServer = class extends /** @type {xyz.swapee.IHyperSwapeeServer.constructor&engineering.type.IEngineer.typeof&xyz.swapee.ISwapeeServer.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperSwapeeServer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IHyperSwapeeServer.Initialese} init The initialisation options.
 */
xyz.swapee.IHyperSwapeeServer.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.IHyperSwapeeServer&engineering.type.IInitialiser<!xyz.swapee.IHyperSwapeeServer.Initialese>)} xyz.swapee.HyperSwapeeServer.constructor */
/** @typedef {typeof xyz.swapee.IHyperSwapeeServer} xyz.swapee.IHyperSwapeeServer.typeof */
/**
 * A concrete class of _IHyperSwapeeServer_ instances.
 * @constructor xyz.swapee.HyperSwapeeServer
 * @implements {xyz.swapee.IHyperSwapeeServer} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.IHyperSwapeeServer.Initialese>} ‎
 */
xyz.swapee.HyperSwapeeServer = class extends /** @type {xyz.swapee.HyperSwapeeServer.constructor&xyz.swapee.IHyperSwapeeServer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperSwapeeServer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.IHyperSwapeeServer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperSwapeeServer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.IHyperSwapeeServer.Initialese} init The initialisation options.
 */
xyz.swapee.HyperSwapeeServer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.HyperSwapeeServer}
 */
xyz.swapee.HyperSwapeeServer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.IHyperSwapeeServer} */
xyz.swapee.RecordIHyperSwapeeServer

/** @typedef {xyz.swapee.IHyperSwapeeServer} xyz.swapee.BoundIHyperSwapeeServer */

/** @typedef {xyz.swapee.HyperSwapeeServer} xyz.swapee.BoundHyperSwapeeServer */

/**
 * Contains getters to cast the _IHyperSwapeeServer_ interface.
 * @interface xyz.swapee.IHyperSwapeeServerCaster
 */
xyz.swapee.IHyperSwapeeServerCaster = class { }
/**
 * Cast the _IHyperSwapeeServer_ instance into the _BoundIHyperSwapeeServer_ type.
 * @type {!xyz.swapee.BoundIHyperSwapeeServer}
 */
xyz.swapee.IHyperSwapeeServerCaster.prototype.asIHyperSwapeeServer
/**
 * Access the _HyperSwapeeServer_ prototype.
 * @type {!xyz.swapee.BoundHyperSwapeeServer}
 */
xyz.swapee.IHyperSwapeeServerCaster.prototype.superHyperSwapeeServer

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.ISwapeeServerCaster&_idio.IServer)} xyz.swapee.ISwapeeServer.constructor */
/** @typedef {typeof _idio.IServer} _idio.IServer.typeof */
/**
 * The server.
 * @interface xyz.swapee.ISwapeeServer
 */
xyz.swapee.ISwapeeServer = class extends /** @type {xyz.swapee.ISwapeeServer.constructor&engineering.type.IEngineer.typeof&_idio.IServer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeServer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeServer.Initialese} init The initialisation options.
 */
xyz.swapee.ISwapeeServer.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.ISwapeeServer&engineering.type.IInitialiser<!xyz.swapee.ISwapeeServer.Initialese>)} xyz.swapee.SwapeeServer.constructor */
/**
 * A concrete class of _ISwapeeServer_ instances.
 * @constructor xyz.swapee.SwapeeServer
 * @implements {xyz.swapee.ISwapeeServer} The server.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.ISwapeeServer.Initialese>} ‎
 */
xyz.swapee.SwapeeServer = class extends /** @type {xyz.swapee.SwapeeServer.constructor&xyz.swapee.ISwapeeServer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeServer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.ISwapeeServer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeServer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.ISwapeeServer.Initialese} init The initialisation options.
 */
xyz.swapee.SwapeeServer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.SwapeeServer}
 */
xyz.swapee.SwapeeServer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.ISwapeeServer} */
xyz.swapee.RecordISwapeeServer

/** @typedef {xyz.swapee.ISwapeeServer} xyz.swapee.BoundISwapeeServer */

/** @typedef {xyz.swapee.SwapeeServer} xyz.swapee.BoundSwapeeServer */

/**
 * Contains getters to cast the _ISwapeeServer_ interface.
 * @interface xyz.swapee.ISwapeeServerCaster
 */
xyz.swapee.ISwapeeServerCaster = class { }
/**
 * Cast the _ISwapeeServer_ instance into the _BoundISwapeeServer_ type.
 * @type {!xyz.swapee.BoundISwapeeServer}
 */
xyz.swapee.ISwapeeServerCaster.prototype.asISwapeeServer
/**
 * Access the _SwapeeServer_ prototype.
 * @type {!xyz.swapee.BoundSwapeeServer}
 */
xyz.swapee.ISwapeeServerCaster.prototype.superSwapeeServer

// nss:xyz.swapee
/* @typal-end */

/* @typal-start {types/api.xml} no-functions a6aec48825b49b6dd71c2746d32b645e */
/** @typedef {typeof __$te_plain} xyz.swapee.Swapee_Server.Config Additional options for the program. */

/* @typal-end */