/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.AbstractChangellyConnector={}
xyz.swapee.IChangellyConnector={}`)

/* @typal-start {types/design/IChangellyConnector.xml} filter:Symbolism 7a7e0756f4e46807b6a8d14081259b68 */
/** @typedef {!xyz.swapee.IChangellyConnector.getSymbols} xyz.swapee.AbstractChangellyConnector.getSymbols Returns the protected symbols. */

/** @typedef {!xyz.swapee.IChangellyConnector.setSymbols} xyz.swapee.AbstractChangellyConnector.setSymbols Sets the protected symbols. */

/** @typedef {!xyz.swapee.IChangellyConnector.Symbols} xyz.swapee.AbstractChangellyConnector.Symbols The list of protected symbols used by the class. */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.SymbolsIn
 * @prop {boolean|string} [_cacheChangellyConnectResponse] Whether to save _Changelly_ response in cache. If string is passed, uses
 * it as path to the file where to save latest responses.
 * @prop {string} [_changellyConnectorFrom] The default currency to connect with to exchange from.
 * @prop {string} [_changellyConnectorTo] The default currency to connect with to exchange into.
 * @prop {number} [_changellyConnectorAmount] The amount for the exchange. If the default amount fails, it might be too
 * high and require manual change.
 */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.Symbols
 * @prop {symbol} _cacheChangellyConnectResponse
 * @prop {symbol} _changellyConnectorFrom
 * @prop {symbol} _changellyConnectorTo
 * @prop {symbol} _changellyConnectorAmount
 */

/**
 * @typedef {Object} xyz.swapee.IChangellyConnector.SymbolsOut
 * @prop {boolean|string} _cacheChangellyConnectResponse Whether to save _Changelly_ response in cache. If string is passed, uses
 * it as path to the file where to save latest responses.
 * @prop {string} _changellyConnectorFrom The default currency to connect with to exchange from.
 * @prop {string} _changellyConnectorTo The default currency to connect with to exchange into.
 * @prop {number} _changellyConnectorAmount The amount for the exchange. If the default amount fails, it might be too
 * high and require manual change.
 */

/** @typedef {typeof xyz.swapee.IChangellyConnector.setSymbols} */
/**
 * Sets the _IChangellyConnector_ symbols on the instance.
 * @param {!xyz.swapee.IChangellyConnector} instance The _IChangellyConnector_ instance on which to set the symbols.
 * @param {!xyz.swapee.IChangellyConnector.SymbolsIn} symbols The symbols to set on the instance.
 * - `[_cacheChangellyConnectResponse]` _boolean&vert;string?_ Whether to save _Changelly_ response in cache. If string is passed, uses
 * it as path to the file where to save latest responses.
 * - `[_changellyConnectorFrom]` _string?_ The default currency to connect with to exchange from.
 * - `[_changellyConnectorTo]` _string?_ The default currency to connect with to exchange into.
 * - `[_changellyConnectorAmount]` _number?_ The amount for the exchange. If the default amount fails, it might be too
 * high and require manual change.
 * @return {void}
 */
xyz.swapee.IChangellyConnector.setSymbols = function(instance, symbols) {}

/** @typedef {typeof xyz.swapee.IChangellyConnector.getSymbols} */
/**
 * Gets the _IChangellyConnector_ symbols currently set on the instance.
 * @param {!xyz.swapee.IChangellyConnector} instance The _IChangellyConnector_ instance from which to read the symbols.
 * @return {!xyz.swapee.IChangellyConnector.SymbolsOut} The symbols currently set on the instance.
 */
xyz.swapee.IChangellyConnector.getSymbols = function(instance) {}

// nss:xyz.swapee.IChangellyConnector
/* @typal-end */