import argufy from '@artdeco/argufy'

/** @type {!_argufy.Config} */
export const argsConfig = {
  'script': {
    description: 'The name of the script to run.',
    command: true,
  },
  'test': {
    description: 'Test exchanges.',
    boolean: true,
    short: 't',
  },
  'help': {
    description: 'Print the help information and exit.',
    boolean: true,
    short: 'h',
  },
  'version': {
    description: 'Show the version\'s number and exit.',
    boolean: true,
    short: 'v',
  },
  'debug': {
    description: 'Enable logging some debug info.',
    boolean: true,
    short: 'd',
  },
}

const args = argufy(argsConfig)

/**
 * The name of the script to run.
 */
export const _script = /** @type {string} */ (args['script'])

/**
 * Test exchanges.
 */
export const _test = /** @type {boolean} */ (args['test'])

/**
 * Print the help information and exit.
 */
export const _help = /** @type {boolean} */ (args['help'])

/**
 * Show the version's number and exit.
 */
export const _version = /** @type {boolean} */ (args['version'])

/**
 * Enable logging some debug info.
 */
export const _debug = /** @type {boolean} */ (args['debug'])

/**
 * The additional arguments passed to the program.
 */
export const _argv = /** @type {!Array<string>} */ (args['_argv'])