/* A custom license. Please make sure you have the rights to use this software. */
import HyperSwapee from '../class/Swapee'
// import Init from './commands/init'
import {_script, _help, _version,_debug,_test} from './get-args'

const v=require(eval("'../../package.json'"))['version']

// if(_help) {
//
//  console.log(usage)
//  process.exit(0)
// }
// else if (_version) {
//  console.log(v)
//  process.exit(0)
// }

;(async () => {
 try {
  // if(_init) return await Init()
  // if(!_input) throw new Error('Please enter some text.')
  // const content = /** @type {string} */ (readFileSync(_input, 'utf8'))
  const{
   asISwapee:{run:run},
  }=new HyperSwapee({
   version:v,
  },/**@type {xyz.swapee.ISwapeeAide.Initialese}*/({
   output:'-',
   testExchanges: _test,
   text:        _script, // run a script from the bin category.
   showVersion:_version,
   showHelp:      _help,
   debug:        _debug,
  }))

  await run() // wtf

  // but
  // if(_output=='-')console.log(output)
  // else if(_output){
  //  writeFileSync(_output,output)
  //  console.error('Result saved to',c(_input,'yellow'),_output)
  // }
 }catch(err){
  if(process.env['DEBUG']||_debug) console.error(err.stack)
  else console.log(err.message)
 }
})()