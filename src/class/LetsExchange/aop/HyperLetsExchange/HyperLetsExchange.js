import LetsExchange from '../../LetsExchange'
import AbstractHyperLetsExchange from '../../gen/hyper/AbstractHyperLetsExchange'
import LetsExchangeGeneralAspects from '../LetsExchangeGeneralAspects'

/** @extends {io.letsexchange.HyperLetsExchange} */
export default class extends AbstractHyperLetsExchange
 .consults(
  LetsExchangeGeneralAspects,
 )
 .implements(
  LetsExchange,
  // AbstractHyperLetsExchange.class.prototype=/**@type {!HyperLetsExchange}*/({
  // }),
 )
{}