import LetsExchangeUniversal, {setLetsExchangeUniversalSymbols,getLetsExchangeUniversalSymbols} from './anchors/LetsExchangeUniversal'
import { newAbstract, $implementations, iu, $initialese } from '@type.engineering/type-engineer'
import '../../../../types/types'

/**
 * @abstract
 * @extends {io.letsexchange.AbstractLetsExchange}
 */
class _AbstractLetsExchange { }
/**
 * The _LetsExchange_ client.
 * @extends {io.letsexchange.AbstractLetsExchange} ‎
 */
class AbstractLetsExchange extends newAbstract(
 _AbstractLetsExchange,3600135954167,null,{
  asILetsExchange:1,
  superLetsExchange:2,
 },false,{
  host:{_host:1},
  apiPath:{_apiPath:2},
  letsExchangeApiKey:{_letsExchangeApiKey:3},
 }) {}

/** @type {typeof io.letsexchange.AbstractLetsExchange} */
AbstractLetsExchange.class=function(){}
/** @type {typeof io.letsexchange.AbstractLetsExchange} */
function AbstractLetsExchangeClass(){}

export default AbstractLetsExchange

/** @type {io.letsexchange.ILetsExchange.Symbols} */
export const LetsExchangeSymbols=AbstractLetsExchange.Symbols
/** @type {io.letsexchange.ILetsExchange.getSymbols} */
export const getLetsExchangeSymbols=AbstractLetsExchange.getSymbols
/** @type {io.letsexchange.ILetsExchange.setSymbols} */
export const setLetsExchangeSymbols=AbstractLetsExchange.setSymbols

AbstractLetsExchange[$implementations]=[
 AbstractLetsExchangeClass.prototype=/**@type {!io.letsexchange.LetsExchange}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_host,_apiPath,_letsExchangeApiKey}=getLetsExchangeSymbols(this)
   setLetsExchangeSymbols(this,{
    _host:iu(_host,'https://api.letsexchange.io'),
    _apiPath:iu(_apiPath,'/api'),
    _letsExchangeApiKey:iu(_letsExchangeApiKey,''),
   })
  },
 }),
 AbstractLetsExchangeClass.prototype=/**@type {!io.letsexchange.LetsExchange}*/({
  constructor:function selfUniversalConstructor(){
   setLetsExchangeUniversalSymbols(this,{_letsExchange:this})
  },
 }),
 AbstractLetsExchangeClass.prototype=/**@type {!io.letsexchange.LetsExchange}*/({
  constructor:function reverseUniversalConstructor(){
   for(const arg of arguments) {
    const asyms=getLetsExchangeUniversalSymbols(arg)
    if(asyms._letsExchange===null) {
     setLetsExchangeUniversalSymbols(arg,{_letsExchange:this})
    }
   }
  },
 }),
 LetsExchangeUniversal,
 /** @type {!io.letsexchange.LetsExchange} */ ({
  preinitializer() {
   const{
    'IO_LETSEXCHANGE_API_KEY':_letsExchangeApiKey,
   }=process.env

   return {
    letsExchangeApiKey: _letsExchangeApiKey?_letsExchangeApiKey.split(/\s*#/)[0]:_letsExchangeApiKey,
   }
  },
 }),
 /** @type {!io.letsexchange.LetsExchange} */ ({
  [$initialese]:/**@type {!io.letsexchange.ILetsExchange.Initialese}*/({
   host:1,
   apiPath:1,
   letsExchangeApiKey:1,
  }),
  initializer({
   host:_host,
   apiPath:_apiPath,
   letsExchangeApiKey:_letsExchangeApiKey,
  }) {
   setLetsExchangeSymbols(this, {
    _host:_host,
    _apiPath:_apiPath,
    _letsExchangeApiKey:_letsExchangeApiKey,
   })
  },
 }),
]