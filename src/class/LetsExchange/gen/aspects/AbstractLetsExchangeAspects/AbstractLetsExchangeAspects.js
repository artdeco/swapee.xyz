import { newAspects } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.letsexchange.AbstractLetsExchangeAspects}
 */
class _AbstractLetsExchangeAspects { }
/**
 * The aspects of the *ILetsExchange*.
 * @extends {io.letsexchange.AbstractLetsExchangeAspects} ‎
 */
class AbstractLetsExchangeAspects extends newAspects(
 _AbstractLetsExchangeAspects,3600135954165,null,{},false) {}

/** @type {typeof io.letsexchange.AbstractLetsExchangeAspects} */
AbstractLetsExchangeAspects.class=function(){}
/** @type {typeof io.letsexchange.AbstractLetsExchangeAspects} */
function AbstractLetsExchangeAspectsClass(){}

export default AbstractLetsExchangeAspects