import '../../../../types/types'

/** @abstract {io.letsexchange.ILetsExchange} @autoinit */
export default class AbstractLetsExchange {}