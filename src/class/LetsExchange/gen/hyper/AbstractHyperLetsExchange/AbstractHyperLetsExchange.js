import LetsExchangeAspectsInstaller from '../../aspects-installers/LetsExchangeAspectsInstaller'
import { newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.letsexchange.AbstractHyperLetsExchange}
 */
class _AbstractHyperLetsExchange { }
/** @extends {io.letsexchange.AbstractHyperLetsExchange} ‎ */
class AbstractHyperLetsExchange extends newAbstract(
 _AbstractHyperLetsExchange,3600135954166,null,{
  asIHyperLetsExchange:LetsExchangeAspectsInstaller,
  superHyperLetsExchange:2,
 },false) {}

/** @type {typeof io.letsexchange.AbstractHyperLetsExchange} */
AbstractHyperLetsExchange.class=function(){}
/** @type {typeof io.letsexchange.AbstractHyperLetsExchange} */
function AbstractHyperLetsExchangeClass(){}

export default AbstractHyperLetsExchange