import {Network} from '@type.engineer/network'
import {parse} from 'url'
import AbstractLetsExchange, {setLetsExchangeSymbols} from './gen/AbstractLetsExchange'

/** @extends {io.letsexchange.LetsExchange} */
export default class extends AbstractLetsExchange.implements(
 Network,
 AbstractLetsExchange.class.prototype=/**@type {!io.letsexchange.LetsExchange} */({
  preinitializer({
   apiPath:apiPath,host:host,API:API,letsExchangeApiKey:letsExchangeApiKey,
  }) {
   const{asILetsExchange:{host:_host,apiPath:_apiPath}}=this
   host=host||_host
   apiPath=apiPath||_apiPath
   if(!API&&host&&apiPath) API=`${host}${apiPath}`
   return{
    userAgent:'LetsExchange.io Type.Community Client',
    API:API,
    token:letsExchangeApiKey,
   }
  },
  initializer() {
   let{
    asINetwork:{API:API},
    asILetsExchange:{apiPath:apiPath,host:host},
   }=this
   if(API&&!host&&!apiPath){
    const{hostname:hostname,path:path}=parse(API)
    host=hostname
    apiPath=path
    setLetsExchangeSymbols(this,{
     _apiPath:apiPath,
     _host:host,
    })
   }
  },
  handleNetworkError(response,method,data){
   throw new Error(`LetsExchange error ${response.statusCode}`)
  },
  async GetInfo(data) {
   const{asINetwork:{POST}}=this
   const res=await POST('/v1/info',data)
   return res
  },
 }),
) {}