import { newAbstract, $implementations, iu, $initialese } from '@type.engineering/type-engineer'
import '../../../../../../types/types'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractSwapeeAide}
 */
class _AbstractSwapeeAide { }
/** @extends {xyz.swapee.AbstractSwapeeAide} ‎ */
class AbstractSwapeeAide extends newAbstract(
 _AbstractSwapeeAide,3600135954246,null,{
  asISwapeeAide:1,
  superSwapeeAide:2,
 },false,{
  output:{_output:1},
  showHelp:{_showHelp:2},
  testExchanges:{_testExchanges:6},
  showVersion:{_showVersion:3},
  debug:{_debug:5},
  version:{_version:4},
 }) {}

/** @type {typeof xyz.swapee.AbstractSwapeeAide} */
AbstractSwapeeAide.class=function(){}
/** @type {typeof xyz.swapee.AbstractSwapeeAide} */
function AbstractSwapeeAideClass(){}

export default AbstractSwapeeAide

/** @type {xyz.swapee.ISwapeeAide.Symbols} */
export const SwapeeAideSymbols=AbstractSwapeeAide.Symbols
/** @type {xyz.swapee.ISwapeeAide.getSymbols} */
export const getSwapeeAideSymbols=AbstractSwapeeAide.getSymbols
/** @type {xyz.swapee.ISwapeeAide.setSymbols} */
export const setSwapeeAideSymbols=AbstractSwapeeAide.setSymbols

AbstractSwapeeAide[$implementations]=[
 AbstractSwapeeAideClass.prototype=/**@type {!xyz.swapee.SwapeeAide}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_output,_showHelp,_testExchanges,_showVersion,_debug,_version}=getSwapeeAideSymbols(this)
   setSwapeeAideSymbols(this,{
    _output:iu(_output,''),
    _showHelp:iu(_showHelp,false),
    _testExchanges:iu(_testExchanges,false),
    _showVersion:iu(_showVersion,false),
    _debug:iu(_debug,false),
    _version:iu(_version,''),
   })
  },
 }),
 /** @type {!xyz.swapee.SwapeeAide} */ ({
  [$initialese]:/**@type {!xyz.swapee.ISwapeeAide.Initialese}*/({
   output:1,
   showHelp:1,
   testExchanges:1,
   showVersion:1,
   debug:1,
   version:1,
  }),
  initializer({
   output:_output,
   showHelp:_showHelp,
   testExchanges:_testExchanges,
   showVersion:_showVersion,
   debug:_debug,
   version:_version,
  }) {
   setSwapeeAideSymbols(this, {
    _output:_output,
    _showHelp:_showHelp,
    _testExchanges:_testExchanges,
    _showVersion:_showVersion,
    _debug:_debug,
    _version:_version,
   })
  },
 }),
]