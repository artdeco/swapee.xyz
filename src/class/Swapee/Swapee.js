import {c} from '@artdeco/erte/colors'
// import SwapeeServer from '../../../server/src/class/SwapeeServer'
import ChangeNow from '../ChangeNow'
import LetsExchange from '../LetsExchange'
import AbstractSwapee from './gen/AbstractSwapee'

/** @extends {xyz.swapee.Swapee} */
export default class extends AbstractSwapee.implements(
 AbstractSwapee.class.prototype=/**@type {!xyz.swapee.Swapee} */({
  connectExchanges:async function connectLetsExchange({
   amount:amount=0.007,
   cryptoTo:cryptoTo='eth',
   cryptoFrom:cryptoFrom='btc',
  }){
   const letsExchange=new LetsExchange
   const res=await letsExchange.GetInfo({
    float:                         false,
    amount:                       amount,
    to:                         cryptoTo,
    from:                     cryptoFrom,
    affiliateId:      'Zp1gafk2sd4goqJq',
    promocode:             'swapee_test',
   })

   return/**@type {xyz.swapee.ISwapee.connectExchanges.Return} */({letsExchange:new Map([[letsExchange,res]])})
   // return {
   //  letsExchange:res,
   // }
   // afterEachConnectExchange, assign to the res, the return the res.
  },
 }),
 AbstractSwapee.class.prototype=/**@type {!xyz.swapee.Swapee} */({
  // constructor() {},
  connectExchanges:async function connectChangeNow({
   amount:amount=0.007,
   cryptoTo:cryptoTo='eth',
   cryptoFrom:cryptoFrom='btc',
  }){
   // those are like connectors that ultimatly need to be written.
   const changeNow=                 new ChangeNow
   const res2=await changeNow.GetExchangeAmount({
    fromAmount:                            parseFloat(`${amount}`),
    fromCurrency:                      cryptoFrom,
    toCurrency:                          cryptoTo,
   })

   return/**@type {xyz.swapee.ISwapee.connectExchanges.Return} */({changeNow:new Map([[changeNow,res2]])})
  },
 }),
 AbstractSwapee.class.prototype=/**@type {!xyz.swapee.Swapee} */({
  async ShowText({
   text:text, // @ what if text is guaranteed -> private api for development purpose
  }){
   // if command is start, run the script which starts the server.
   // console.log(arguments)
   return`Text: ${c(text,'yellow',{bold:false})}`
  },
  // async createServer(){
  //  const swapeeServer=new SwapeeServer({
  //   appName:'Swapee',
  //   ngrok:false,
  //   // ngrok:process.env.NODE_ENV=='production'?void 0:'eu',
  //   connectIpify:false,
  //   port:5000,
  //  })
  //  await swapeeServer.connect()
  //  await swapeeServer.listen()
  //  return swapeeServer
  // },
 }),
) {}

// const exchanges={letsExchange,changeNow}
// const results=new Map([[letsExchange,res],[changeNow,res2]])
// console.log({exchanges,results})//,res,res2)

// return{
//  exchanges:exchanges,
//  results:results,
// }
// return[{
//  letsExchange,changeNow,
// },
// ,
// ]
// return new Map([
//  [letsExchange,res],
//  [changeNow,res2],
// ])

// changeNow.GetFixedExchangeAmount({
//  fromAmount:0.007,
//  fromCurrency:'btc',
//  toCurrency:'eth',
// }),
// letsExchange.GetInfo({
//  amount:0.007,
//  from:'btc',
//  to:'eth',
//  affiliateId:'Zp1gafk2sd4goqJq',
//  promocode:'swapee_test',
//  float:true,
// }),
// method:_method,
