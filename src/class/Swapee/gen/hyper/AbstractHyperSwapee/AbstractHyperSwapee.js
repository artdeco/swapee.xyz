import SwapeeAspectsInstaller from '../../aspects-installers/SwapeeAspectsInstaller'
import { newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {xyz.swapee.AbstractHyperSwapee}
 */
class _AbstractHyperSwapee { }
/** @extends {xyz.swapee.AbstractHyperSwapee} ‎ */
class AbstractHyperSwapee extends newAbstract(
 _AbstractHyperSwapee,3600135954245,null,{
  asIHyperSwapee:SwapeeAspectsInstaller,
  superHyperSwapee:2,
 },false) {}

/** @type {typeof xyz.swapee.AbstractHyperSwapee} */
AbstractHyperSwapee.class=function(){}
/** @type {typeof xyz.swapee.AbstractHyperSwapee} */
function AbstractHyperSwapeeClass(){}

export default AbstractHyperSwapee