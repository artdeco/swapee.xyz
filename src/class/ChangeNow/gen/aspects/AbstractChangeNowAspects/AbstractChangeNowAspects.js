import { newAspects } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {io.changenow.AbstractChangeNowAspects}
 */
class _AbstractChangeNowAspects { }
/**
 * The aspects of the *IChangeNow*.
 * @extends {io.changenow.AbstractChangeNowAspects} ‎
 */
class AbstractChangeNowAspects extends newAspects(
 _AbstractChangeNowAspects,3600135954177,null,{},false) {}

/** @type {typeof io.changenow.AbstractChangeNowAspects} */
AbstractChangeNowAspects.class=function(){}
/** @type {typeof io.changenow.AbstractChangeNowAspects} */
function AbstractChangeNowAspectsClass(){}

export default AbstractChangeNowAspects