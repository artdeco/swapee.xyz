import '../../../../types/types'

/** @abstract {io.changenow.IChangeNow} @autoinit */
export default class AbstractChangeNow {}