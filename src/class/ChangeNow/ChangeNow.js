import {Network} from '@type.engineer/network'
import {parse} from 'url'
import AbstractChangeNow, {setChangeNowSymbols} from './gen/AbstractChangeNow'
// import _method from './methods/method'

/** @extends {io.changenow.ChangeNow} */
export default class extends AbstractChangeNow.implements(
 Network,
 AbstractChangeNow.class.prototype=/**@type {!io.changenow.ChangeNow} */({
  preinitializer({
   apiPath:apiPath,host:host,API:API,changeNowApiKey:changeNowApiKey,
  }) {
   const{asIChangeNow:{host:_host,apiPath:_apiPath}}=this
   host=host||_host
   apiPath=apiPath||_apiPath
   if(!API&&host&&apiPath) API=`${host}${apiPath}`
   return{
    userAgent:'ChangeNow.io Type.Community Client',
    API:API,
    token:changeNowApiKey,
   }
  },
  initializer() {
   let{
    asINetwork:{API:API},
    asIChangeNow:{apiPath:apiPath,host:host},
   }=this
   if(API&&!host&&!apiPath){
    const{hostname:hostname,path:path}=parse(API)
    host=hostname
    apiPath=path
    setChangeNowSymbols(this,{
     _apiPath:apiPath,
     _host:host,
    })
   }
  },
  getHeaders(headers){
   const{asINetwork:{token:token}}=this
   return {
    'x-changenow-api-key':token,
    ...headers,
   }
  },
  handleNetworkError(response,method,data){
   const{body,statusCode}=response
   // {error,message}
   if(typeof body=='object'&&body['message']){
    const{'error':error,'message':message}=body
    throw new Error(`${error} ${message}`)
   }
   throw new Error(`ChangeNow error: ${statusCode}`)
  },
  async GetExchangeAmount(data) {
   const{asINetwork:{GET:GET}}=this
   const{
    'flow':f,'rateId':r,'validUntil':v,...rest
   }=await GET('/exchange/estimated-amount',{
    ...data,
    'flow':'standard',
   })
   const rr=/** @type {io.changenow.IChangeNow.GetExchangeAmount.Return}*/({
    ...rest,
    // validUntil:new Date(v),
   })
   return rr
  },
  async GetFixedExchangeAmount(data) {
   const{asINetwork:{GET:GET}}=this
   const{'flow':f,'validUntil':v,...res}=await GET('/exchange/estimated-amount',{
    ...data,
    'flow':'fixed-rate',
   })
   const r=/** @type {io.changenow.IChangeNow.GetFixedExchangeAmount.Return}*/({
    ...res,
    validUntil:new Date(v),
   })
   return r
  },
 }),
) {}