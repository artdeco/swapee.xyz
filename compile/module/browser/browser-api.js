import { Swapee, SwapeeAspectsInstaller, AbstractSwapeeAspects, AbstractHyperSwapee,
 HyperSwapee } from './browser-exports'

/** @lazy @api {xyz.swapee.Swapee} */
export { Swapee }
/** @lazy @api {xyz.swapee.SwapeeAspectsInstaller} */
export { SwapeeAspectsInstaller }
/** @lazy @api {xyz.swapee.AbstractSwapeeAspects} */
export { AbstractSwapeeAspects }
/** @lazy @api {xyz.swapee.AbstractHyperSwapee} */
export { AbstractHyperSwapee }
/** @lazy @api {xyz.swapee.HyperSwapee} */
export { HyperSwapee }