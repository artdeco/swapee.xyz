/**
 * Assigs lazy requires to the module.
 * @param {string} P The serial number of the package.
 * @param {!Iterable<number|Array<number,number>>} MODULES
 */
export default function assignModule(P,MODULES,mod=module) {
 for(let n of MODULES) {
  let M,C
  if(Array.isArray(n)) {
   const[_,__]=n
   M=__.map((p)=>P+p)
   C=_
  } else {
   M=[P+n]
   C=n
  }
  for(const m of M) {
   Object.defineProperty(mod.exports,m,{
    get() {
     return require(eval(`'../${P}/c${C}'`))[m]
    },
   })
  }
 }
}
