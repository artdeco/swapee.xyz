export default [
 [
  '1_4_14_21_25_26_27_28_29_30_31_32_33_34_35_36',
  [
   1, // OffersAggregatorService
   4, // LetsExchangeAspects
   14, // ChangeNowAspects
   21, // SwapeeAspects
   25, // ChangellyConnectorAspectsInstaller
   26, // ChangellyConnector
   27, // AbstractChangellyConnectorAspects
   28, // ChangellyConnectorAspects
   29, // AbstractHyperChangellyConnector
   30, // HyperChangellyConnector
   31, // SwapeeServerAspectsInstaller
   32, // SwapeeServer
   33, // AbstractSwapeeServerAspects
   34, // SwapeeServerAspects
   35, // AbstractHyperSwapeeServer
   36, // HyperSwapeeServer
  ],
 ],
 2, // LetsExchange
 3, // AbstractLetsExchangeAspects
 5, // AbstractHyperLetsExchange
 6, // HyperLetsExchange
 [
  '7_8',
  [
   7, // LetsExchangeMetaUniversal
   8, // LetsExchangeUniversal
  ],
 ],
 9, // ChangeNowAspectsInstaller
 10, // ChangeNow
 [
  '11_12',
  [
   11, // ChangeNowMetaUniversal
   12, // ChangeNowUniversal
  ],
 ],
 13, // AbstractChangeNowAspects
 15, // AbstractHyperChangeNow
 16, // HyperChangeNow
 17, // LetsExchangeAspectsInstaller
 18, // Swapee
 19, // SwapeeAspectsInstaller
 20, // AbstractSwapeeAspects
 22, // AbstractHyperSwapee
 23, // HyperSwapee
 24, // SwapeeAide
 1, // OffersAggregatorService
]