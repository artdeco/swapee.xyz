import '@swapee.xyz/swapee_xyz.h'
import '../types/typology.mjs'
import Module from './browser'

/**
@license
@LICENSE @swapee.xyz/swapee_xyz (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @type {typeof xyz.swapee.Swapee} */
export const Swapee=Module['360013595418']
/** @type {typeof xyz.swapee.SwapeeAspectsInstaller} */
export const SwapeeAspectsInstaller=Module['360013595419']
/**@extends {xyz.swapee.AbstractSwapeeAspects}*/
export class AbstractSwapeeAspects extends Module['360013595420'] {}
/** @type {typeof xyz.swapee.AbstractSwapeeAspects} */
AbstractSwapeeAspects.class=function(){}
/**@extends {xyz.swapee.AbstractHyperSwapee}*/
export class AbstractHyperSwapee extends Module['360013595422'] {}
/** @type {typeof xyz.swapee.AbstractHyperSwapee} */
AbstractHyperSwapee.class=function(){}
/** @type {typeof xyz.swapee.HyperSwapee} */
export const HyperSwapee=Module['360013595423']