import '@swapee.xyz/swapee_xyz.h'
import '../types/typology.mjs'
import Module from './browser' /* compiler fn:../precompile/browser renameReport:../../../module/browser/browser-modules.txt:../../../module/server/server-modules.txt mod packageName:@swapee.xyz/swapee_xyz */

/** @export {../../../api/license.js} */

/** @export {../../../module/browser/browser-api.js} */
