/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}

const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const e=d.iu;class f{__$constructor(){this.example="ok";const {g:c}=g(this);h(this,{g:e(c,"")})}}class k extends d["372700389812"](f,16817934092,null,{asISwapee_xyz:1,superSwapee_xyz:2},!1,{symbol:{g:1}}){}function l(){}const g=k.getSymbols,h=k.setSymbols;k[d["372700389811"]]=[{[d["372700389810"]]:{symbol:1},initializer({symbol:c}){h(this,{g:c})}}];async function m(){return"example"};var n=class extends k.implements(l.prototype={run:m}){};module.exports["36001359540"]=n;module.exports["36001359541"]=n;

//# sourceMappingURL=browser.js.map
self.DEPACK_REQUIRE['@swapee.xyz/swapee_xyz']=module.exports