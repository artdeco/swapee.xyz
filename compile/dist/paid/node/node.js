import getModule from '../precompile/index-upd'

/**
@license
@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
*/

const Module = getModule({}, require)

export default Module