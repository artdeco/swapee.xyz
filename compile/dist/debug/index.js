require('@type.engineering/type-engineer/types/typedefs')
require('./types/typedefs')

/**
@license
@LICENSE @swapee.xyz/swapee_xyz (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/**
 * The package.
 * @extends {xyz.swapee.Swapee_xyz}
 */
class Swapee_xyz extends (class {/* lazy-loaded */}) {}

module.exports.Swapee_xyz = Swapee_xyz

Object.defineProperties(module.exports, {
 'Swapee_xyz': {get: () => require('./compile')[36001359541]},
 [36001359541]: {get: () => module.exports['Swapee_xyz']},
})