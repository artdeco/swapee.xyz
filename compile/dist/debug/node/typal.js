import '@swapee.xyz/swapee_xyz.h'
import Module from './node' /* compiler fn:../precompile/index mod renameReport:../../../module/server/server-modules.txt packageName:@swapee.xyz/swapee_xyz */

/** @export {../../../api/license.js} */

/** @export {../../../module/server/server-api.js} */

/** Allows to embed the object code directly into other packages. */
