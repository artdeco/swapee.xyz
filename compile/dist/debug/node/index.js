import '@swapee.xyz/swapee_xyz.h'
import Module from './node'

/**
@license
@LICENSE @swapee.xyz/swapee_xyz (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @type {typeof xyz.swapee.Swapee_xyz} */
export const Swapee_xyz=Module['36001359541']

/** Allows to embed the object code directly into other packages. */