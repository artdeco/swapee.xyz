export
const changelly={
 focus:true,
 url:'changelly.html',
 layout:'layout/MainLayout.html',
 links:{
  changelly:'https://changelly.com',
 },
 og:{},
 seo:`Exchange coins and tokens with Changelly. Compare cryptocurrency exchange rates and earn free Lightning cashback on all completed transactions with Swapee.`,
}

export
const index={
 // focus:true,
 // url:'changelly.html',
 // file:'changelly',
 // layout:'layout/MainLayout.html',
 links:{
  changelly:'https://changelly.com',
 },
 og:{},
 seo:`Compare cryptocurrency exchanges and earn free Lightning cashback on completed transactions from Swapee. Choose from among a dozen of partners sorted by rate.`,
}