/**
 * @type {import('splendid').Page}
 */
export
const index={
 // focus:true,
 title:'Crypto Aggregator — Swapee',
 seo:'Exchange cryptocurrency at the best rate.',
 // og:{
 //  image:'/img/splash.jpg',
 // },
}
/**
 * @type {import('splendid').Page}
 */
export
const about={
 // focus:true,
 url:'about.html',
 title:'About Swapee — Friendly Crypto Aggregator',
 seo:'Learn why we created Swapee - a crypto-exchange aggregator and part of a larger ecosystem to make cryptocurrency accessible to each and all.',
 // og:{
 //  image:'/img/splash.jpg',
 // },
}

export const benefits={
 // focus:true,
 url:'benefits.html',
 title:'Why Choose Crypto-Currency',
 seo:'Are you ready to take the crypto-dive? Learn why you shouldn\'t wait to start waiting crypto today.',
}

/**
 * @type {import('splendid').Page}
 */
// export
const motionPath={
 url:'motion-path.html',
 file:'motion-path',
 title:'Crypto Aggregator — Swapee',
 seo:'Exchange cryptocurrency at the best rate.',
 // og:{
 //  image:'/img/splash.jpg',
 // },
}

// export
const test={
 url:'test.html',
 title:'Crypto Aggregator — Swapee',
 seo:'Exchange cryptocurrency at the best rate.',
 og:{
  image:'/img/splash.jpg',
 },
}
// export
const ping={
 url:'ping.html',
 title:'Crypto Aggregator — Swapee',
 seo:'Exchange cryptocurrency at the best rate.',
 og:{
  image:'/img/splash.jpg',
 },
}

export
const swapeeLogin={
 file:'swapee-login',
 // focus:true,
 url:'swapee-me.html',
 title:'Swapee.me Login',
 layout:'layout/SansLayout.html',
 // seo:'Exchange cryptocurrency at the best rate.',
 // og:{
 //  image:'/img/splash.jpg',
 // },
}

// export
const template={
 file:'template',
 url:'template.html',
 title:'Swapee',
 // layout:'layout/SansLayout.html',
}

// additional directories
// export
const admin='~/pages/admin'

module.exports['dev-blog']=
'~/pages/dev-blog'

export const partners='./partners'
