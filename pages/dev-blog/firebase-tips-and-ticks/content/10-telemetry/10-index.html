<h2 id="collect-telemetry">1. Collect Telemetry Data into Firestore</h2>

Let's start with the basics. Although functions' execution times can be seen in
logs, we'd like to keep a copy of essential metrics in _Firestore_, so that it
can be monitored daily. As we invoke RPC code, the name for the collection will
consist of the name of the class plus the name of the method. The `.t` namespace
(`t` for _telemetry_) is prefixed to the <abbr title="Fully qualified
name">fqn</abbr>, or in case when running localy, the `.lt` (local telemetry)
too. This is to keep telemtry names short as one of the downsides of the
_Firebase_ console is that longer names disappear with `...` and it's not
possible to expand the column.

<row BlogFigIm mb-3>
 <column col-12>
  <img src="./img/telem.png" alt="telemetry" img-fluid BlogImage />
 </column>
 <column col-12>
  <p font-family="Source Code Pro; monospace">
  Fig 1: an overview of a Telemetry collection with a record with essential
  measurements.
 </p>
 </column>
</row>

After numerous iterations, the following list of data points that are to be
collected has been derived. Overall, they can be categorised as _operational_
(project, region, host, date), _profiling_ (download and invoke time),
_method-metadata_ (version, last modified date) and _request-response_ raw data.
The full structure is summarised in the tables below.

<!-- The production data record structure after a number of iterations and
interfacing with the dev-ops team who raised their requirements in the process,
emerged are listed in the tables below with explanation what the field means.
The fields can be grouped into informational, header-extracted, timer-profiling,
methods metadata and response-request fields. -->

<!-- The good data record structure to store as seen after a number of days of
experiments with a couple of iterations with additions and removals has been
agreed as following table shows: -->

<container TableContainer>

<row mb-4 border-bottom="1px solid lightgrey" pb-2 box-shadow="inset 0 -1px 0 0px
black" font-weight="500">
 <column col-4>
  Field Name
 </column>
 <column col-4>
  Type
 </column>
 <column col-4>
  Example Value(s)
 </column>
</row>

<row TableRow FirstRow>
 <column col-12 col-md-4 TitleCol>
  `cloudFunction`
 </column>
 <column col-12 col-md-3>
  String
 </column>
 <column col-12 col-md-5>
  ``rpc.index`` <span class="tm2">emulator<sup font-family="math">𖤐</sup></span>
 </column>
 <column col-12>
  <span Comment>The ID of the cloud function, and its group, read from `process.env.FUNCTION_TARGET`.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `date`
 </column>
 <column col-12 col-md-3>
  Timestamp
 </column>
 <column col-12 col-md-5>
  ``March 3, 2024``<br d-lg-none /> ``7:31:14 AM UTC+3``
 </column>
 <column col-12 col-md-12>
  <span Comment>The date when the request has reached the server and after the
  cloud function was spawned.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `downloadMethodTime`
 </column>
 <column col-12 col-md-3>
  Number
 </column>
 <column col-12 col-md-5>
  ``18``
 </column>
 <column col-12>
  <span Comment>Time to download cloud method from CDN, in ms.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `host`
 </column>
 <column col-12 col-md-3>
  String
 </column>
 <column col-12 col-md-5>
  <div>
   ``sw4p33.web.app``
  </div>
 </column>
 <column col-12 >
  <span Comment>The Firebase hosting domain, extracted from `X-Forwarded-Host`.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `downloadTime`
 </column>
 <column col-12 col-md-3>
  Number
 </column>
 <column col-12 col-md-5>
  ``20``
 </column>
 <column col-12>
  <span Comment>The total download time including obtaining method-metadata.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `loadTime`
 </column>
 <column col-12 col-md-3>
  string
 </column>
 <column col-12 col-md-5>
  ``1``
 </column>
 <column col-12>
  <span Comment>Evaluation time of the function after the download or require.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `time`
 </column>
 <column col-12 col-md-3>
  Number
 </column>
 <column col-12 col-md-5>
  ``156``
 </column>
 <column col-12>
  <span Comment>Time in ms to invoke the function and fulfill the request.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `project`
 </column>
 <column col-12 col-md-3>
  String
 </column>
 <column col-12 col-md-5>
  ``sw4p33``
 </column>
 <column col-12>
  <span Comment>Firebase project, as extracted from the `process.env.GCLOUD_PROJECT`.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `region`
 </column>
 <column col-12 col-md-3>
  String
 </column>
 <column col-12 col-md-5>
  ``us-central1``
 </column>
 <column col-12>
  <span Comment>Obtained from parsing forwarded routes such as `/rpc-us/`.</span>
 </column>
</row>
</container>

<p mt-2>
 <sup font-family="math">𖤐</sup> The ``emulator`` cloud function id will be
 populated when `process.env.FUNCTIONS_EMULATOR` is _True_.
</p>

<!-- - `project` "sw4p33" (string)  <span Comment>Firebase project</span> -->
<!-- - `region` "us-central1" (string) <span Comment>_see *forwarding* tip_</span> -->

<row align-items-center mb-2 mt-3 TableRow box-shadow="none">
 <column col-lg-4>
  #### Method Metadata
 </column>
 <column font-size="small">
  The methods are invoked according the their encoded id for example the
  `createTransaction` method will have an id of `321e2` and `checkPayment`
  &mdash; `c3eae`. Each method also has a class-id or `cid` eg `2013392380`
  which acts like a serialization id.
 </column>
</row>


<container TableContainer>
 <row TableRow>
  <column col-12 col-md-4 TitleCol>
   `methodId`
  </column>
  <column col-12 col-md-3>
   String
  </column>
  <column col-12 col-md-5>
   ``c3eae``
  </column>
  <column col-12>
   <span Comment>The method id generated with a model transformer.</span>
  </column>
 </row>

 <row TableRow>
 <column col-12 col-md-4 TitleCol>
  `methodCid`
 </column>
 <column col-12 col-md-3>
  Number
 </column>
 <column col-12 col-md-5>
  ``2013392380``
 </column>
 <column col-12>
  <span Comment>The class id that the method belongs to.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `methodDate`
 </column>
 <column col-12 col-md-3>
  Timestamp
 </column>
 <column col-12 col-md-5>
  ``March 2, 2024``<br d-lg-none /> ``9:00:12 PM``
 </column>
 <column col-12>
  <span Comment>When downloading, will be read from Last-Modified header.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `methodEtag`
 </column>
 <column col-12 col-md-3>
  String
 </column>
 <column col-12 col-md-5>
  ``3178b80...cf827b9``
 </column>
 <column col-12>
  <span Comment>When downloading, will be read from ETag header.</span>
 </column>
</row>

<row TableRow>
 <column col-12 col-md-4 TitleCol>
  `methodVersion`
 </column>
 <column col-12 col-md-3>
  Number
 </column>
 <column col-12 col-md-5>
  ``5913591679``
 </column>
 <column col-12>
  <span Comment>The method version obtained during the writing of the method out.</span>
 </column>
</row>

</container>
<!-- or during -->
  <!-- the require process when using sync loading via deployed functions -->

<!-- - `cloudFunction` "rpc.index" (string) <span Comment>deployed function</span> -->
<!-- - `date` March 3, 2024 at 7:31:14 AM UTC+3 (timestamp) -->
<!-- - `downloadMethodDirectlyTime` 18 (number) <span Comment>see download tip</span> -->
<!-- - `downloadTime` 20 (number) <span Comment>_see *download* tip_</span> -->
<!-- - `host` "sw4p33.web.app" (string) <span Comment>extracted from x-forwarded-host</span> -->
<!-- - `loadTime` 0 (number) <span Comment>evaluation time</span> -->
<!-- - `methodCid` 2013392380 (number) <span Comment>class id</span> -->
<!-- - `methodDate` March 2, 2024 at 9:00:12 PM UTC+3 (timestamp) <span Comment>last-modified</span> -->
<!-- - `methodEtag` ""3178b805ead71f0acf827b9-br"" (string) <span Comment>etag</span> -->
<!-- - `methodId` "c3eae" (string) -->
<!-- - `methodVersion` 5913591679 (number) <span Comment>the code revision</span> -->

<row align-items-center mb-2 mt-3 TableRow box-shadow="none">
 <column col-lg-4>
  #### Request / Response
 </column>
 <column font-size="small">
  The inputs and outputs of each method are also stored to be able to dig into
  in case of problems. Although encoded for the transfer, the data points are
  decoded and stored in _Firestore_.
 </column>
</row>

<container TableContainer mb-3>
 <row TableRow>
  <column col-12 col-md-4>
   `request`
  </column>
  <column col-12 col-md-3>
   Map
  </column>
  <column col-12 col-md-5>
   ``{id:'pu3lgpam'}``
  </column>
  <column col-12>
   <span Comment>The request data includes arguments passed to RPC.</span>
  </column>
 </row>

 <row TableRow>
  <column col-12 col-md-4>
   `response`
  </column>
  <column col-12 col-md-3>
   Map
  </column>
  <column col-12 col-md-5>
   ``{status:'waiting'}``
  </column>
  <column col-12>
   <span Comment>The response data contains all fields that an RPC returned.</span>
  </column>
 </row>
</container>

<!-- - `request` (map) <span Comment>data received from client</span>
- `request.id` "chngl-pf1abro0edulgpam" (string)
- `result` (map) <span Comment>data sent to client</span>
- `result.status` "waiting" (string) -->

<!-- Having this data in store is helpful to the analysis of the operation of the
system's dependencies such as API latencies. -->

<!-- Those will be plotted as graphs in
the admin panel. Obtaining some data can be tricky, for example the following
need to read from the headers: -->
<!-- 𖤐 -->
<!-- - ``project``: process.env.GCLOUD_PROJECT -->
<!-- - ``cloudFunction``: process.env.FUNCTION_TARGET -->
<!-- The `host` needs to be read from the _X-Forwarded-Host_ header. The region
depends on the path the function, as will be seen in the forwarding section. The
method metadata includes: `cid`, `date`, `etag`, `id` and `version` and are
deduced when loading the source code of the method. Logging of the request and
result records could be helpful when debugging problems. -->

<!-- obtaining the metadata
such as their last modified date and version which surely can be helpful in
debuging,  -->

This seems to be a good set of data points to measure that will help us later.
But before talking more about loading methods dynamically, I'd like to give a
short tip that helps me to preview incoming data in real time easily in the
console.

<!-- - result (map) -->
<!-- - request {id:"chngl-pf1abro0edulgpam"} (map) -->
<!-- id
"chngl-pf1abro0edulgpam"
(string) -->

<p w-100 text-center>
 <img src="../../img/sep1.png" alt x2 x3 resize-to="30" />
</p>