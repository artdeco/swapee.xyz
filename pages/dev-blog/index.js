export
const firebaseTips={
 // url:'5-real-firebase-tips-and-tricks.html',
 file:'firebase-tips-and-ticks',
 layout:'layout/blog/BlogLayout.html',
 links:{
  wallets:'https://swapee.me',
  fb:'https://firebase.google.com/docs/functions/tips',
  f150:'https://groups.google.com/g/firebase-talk/c/Ym14sCZXHMA',
  objectId:'https://www.mongodb.com/docs/manual/reference/method/ObjectId/',
  globalCache:'https://firebase.google.com/docs/functions/tips#use_global_variables_to_reuse_objects_in_future_invocations',
  hostingConf:'https://firebase.google.com/docs/hosting/full-config',
 },
 og:{
  type:'article',
 },
 seo:`Firebase documentation stops short of explaining how to distribute your web app across regions. We fix this by suggesting 5 tips to deploying multi-zone sites.`,
}

export
const index={
 // url:'index.html',
 // file:'index',
 layout:'layout/blog/BlogLayout.html',
 links:{},
}

// maurice/docs/blog/firebasetips.html