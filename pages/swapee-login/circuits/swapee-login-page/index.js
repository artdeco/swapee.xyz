import {makePlaceholder} from "@mauriceguest/guest2"

// const Placeholder=makePlaceholder('xyz.swapee.wc.swapee-login-page','SwapeeLoginPage',true)
const Placeholder=makePlaceholder('xyz.swapee.wc.swapee-login-page','SwapeeLoginPage',__dirname)

export default Placeholder
