import {makeTestSuite} from '@type.engineering/web-computing'
import Context from '../context'

export default makeTestSuite(
 'test/result/bin',{fork:{
  module:   Context.BIN,
  normaliseOutputs:true,
 },
 },
)