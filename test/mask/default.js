import {makeTestSuite} from '@type.engineering/web-computing'
import {EOL} from 'os'
import Context from '../context'
import {Swapee_xyz} from '../../src'

// export default
makeTestSuite('test/result/default', {
 /**
  * @param {Context} ctx
  */
 async getResults({fixture, readFile}) {
  const text = readFile(fixture`${this.input}.txt`)
  const swapee_xyz=new Swapee_xyz({symbol:text})
  return `${this.input}:${EOL}${swapee_xyz.symbol}`
 },
 context: Context,
})