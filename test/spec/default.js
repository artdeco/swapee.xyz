import {deepEqual as equal} from '@type.engineering/web-computing'
import {ok} from 'assert'
import {envariable} from '@idio2/server'
import {HyperSwapee} from '../../src'

envariable({
 location:'.env',
 name:'test.env',
})

export const Default={
 async'starts'(){
  const swapee=new HyperSwapee()
  const{exchanges,results}=await swapee.connectExchanges()
  ok(results instanceof Map)
  equal(Object.keys(exchanges),['letsExchange','changeNow'])
  // console.log(exchanges,results)
  // const c=new ChangeNow
  // const l=new LetsExchange
 },
}