## Runs the binary
test/fixture/test.txt

/* stdout */
Text: test/fixture/test.txt
/**/

## !Tests connection
-t

/* stdout */
letsExchange {
  deposit_min_amount: '0.003',
  deposit_max_amount: '1500',
  min_amount: '0.003',
  max_amount: '1500',
  amount: '0.10911134',
  fee: '0',
  rate: '16.834232490357',
  profit: '0.00054284',
  withdrawal_fee: 0.009271115302186,
  extra_fee_amount: '0',
  rate_id: 132025867,
  rate_id_expired_at: '1705959127000',
  applied_promo_code_id: 843,
  networks_from: [],
  networks_to: [],
  deposit_amount_usdt: '280.53045999',
  expired_at: '1705959059003'
}
changeNow {
  fromCurrency: 'btc',
  fromNetwork: 'btc',
  toCurrency: 'eth',
  toNetwork: 'eth',
  type: 'direct',
  transactionSpeedForecast: '10-60',
  warningMessage: null,
  depositFee: 0.000066,
  withdrawalFee: 0.00033,
  userId: null,
  fromAmount: 0.007,
  toAmount: 0.1180272
}
/**/