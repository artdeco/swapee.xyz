/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ISwapeeLoginPageComputer={}
xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError={}
xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError={}
xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin={}
xyz.swapee.wc.ISwapeeLoginPageComputer.compute={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Host={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignIn={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignedIn={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Username={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Password={}
xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel={}
xyz.swapee.wc.ISwapeeLoginPagePort={}
xyz.swapee.wc.ISwapeeLoginPagePort.Inputs={}
xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs={}
xyz.swapee.wc.ISwapeeLoginPageCore={}
xyz.swapee.wc.ISwapeeLoginPageCore.Model={}
xyz.swapee.wc.ISwapeeLoginPageCore.Model.CredentialsError={}
xyz.swapee.wc.ISwapeeLoginPagePortInterface={}
xyz.swapee.wc.ISwapeeLoginPageCPU={}
xyz.swapee.wc.ISwapeeLoginPageProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ISwapeeLoginPageController={}
xyz.swapee.wc.front.ISwapeeLoginPageControllerAT={}
xyz.swapee.wc.front.ISwapeeLoginPageScreenAR={}
xyz.swapee.wc.ISwapeeLoginPage={}
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil={}
xyz.swapee.wc.ISwapeeLoginPageHtmlComponent={}
xyz.swapee.wc.ISwapeeLoginPageElement={}
xyz.swapee.wc.ISwapeeLoginPageElement.build={}
xyz.swapee.wc.ISwapeeLoginPageElement.short={}
xyz.swapee.wc.ISwapeeLoginPageElementPort={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts={}
xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs={}
xyz.swapee.wc.ISwapeeLoginPageDesigner={}
xyz.swapee.wc.ISwapeeLoginPageDesigner.communicator={}
xyz.swapee.wc.ISwapeeLoginPageDesigner.relay={}
xyz.swapee.wc.ISwapeeLoginPageDisplay={}
xyz.swapee.wc.ISwapeeLoginPageDisplay.paintCallback={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ISwapeeLoginPageDisplay={}
xyz.swapee.wc.back.ISwapeeLoginPageController={}
xyz.swapee.wc.back.ISwapeeLoginPageControllerAR={}
xyz.swapee.wc.back.ISwapeeLoginPageScreen={}
xyz.swapee.wc.back.ISwapeeLoginPageScreenAT={}
xyz.swapee.wc.ISwapeeLoginPageController={}
xyz.swapee.wc.ISwapeeLoginPageScreen={}
xyz.swapee.wc.ISwapeeLoginPageGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/02-ISwapeeLoginPageComputer.xml}  e6751f92e6d9a61fb79067cabe016d79 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageComputer)} xyz.swapee.wc.AbstractSwapeeLoginPageComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageComputer} xyz.swapee.wc.SwapeeLoginPageComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageComputer` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageComputer
 */
xyz.swapee.wc.AbstractSwapeeLoginPageComputer = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageComputer.constructor&xyz.swapee.wc.SwapeeLoginPageComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageComputer.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageComputer.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageComputer}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageComputer} xyz.swapee.wc.SwapeeLoginPageComputerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageComputerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.SwapeeLoginPageMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeLoginPageLand>)} xyz.swapee.wc.ISwapeeLoginPageComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ISwapeeLoginPageComputer
 */
xyz.swapee.wc.ISwapeeLoginPageComputer = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError} */
xyz.swapee.wc.ISwapeeLoginPageComputer.prototype.adaptClearError = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError} */
xyz.swapee.wc.ISwapeeLoginPageComputer.prototype.adaptCredentialsError = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin} */
xyz.swapee.wc.ISwapeeLoginPageComputer.prototype.adaptLogin = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageComputer.compute} */
xyz.swapee.wc.ISwapeeLoginPageComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese>)} xyz.swapee.wc.SwapeeLoginPageComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageComputer} xyz.swapee.wc.ISwapeeLoginPageComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeLoginPageComputer_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageComputer
 * @implements {xyz.swapee.wc.ISwapeeLoginPageComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageComputer = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageComputer.constructor&xyz.swapee.wc.ISwapeeLoginPageComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageComputer}
 */
xyz.swapee.wc.SwapeeLoginPageComputer.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageComputer.
 * @interface xyz.swapee.wc.ISwapeeLoginPageComputerFields
 */
xyz.swapee.wc.ISwapeeLoginPageComputerFields = class { }
/**
 * The list of props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeLoginPageComputerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeLoginPageComputer.Props} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageComputer} */
xyz.swapee.wc.RecordISwapeeLoginPageComputer

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageComputer} xyz.swapee.wc.BoundISwapeeLoginPageComputer */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageComputer} xyz.swapee.wc.BoundSwapeeLoginPageComputer */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageComputer.Props The list of props for _VSCode_.
 * @prop {*} error-clear Sets the `error` property of the _xyz.swapee.wc.ISwapeeLoginPageCore_.
 */

/**
 * Contains getters to cast the _ISwapeeLoginPageComputer_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageComputerCaster
 */
xyz.swapee.wc.ISwapeeLoginPageComputerCaster = class { }
/**
 * Cast the _ISwapeeLoginPageComputer_ instance into the _BoundISwapeeLoginPageComputer_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageComputer}
 */
xyz.swapee.wc.ISwapeeLoginPageComputerCaster.prototype.asISwapeeLoginPageComputer
/**
 * Access the _SwapeeLoginPageComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageComputer}
 */
xyz.swapee.wc.ISwapeeLoginPageComputerCaster.prototype.superSwapeeLoginPageComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Form, changes: xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Form) => (void|xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Return)} xyz.swapee.wc.ISwapeeLoginPageComputer.__adaptClearError
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageComputer.__adaptClearError<!xyz.swapee.wc.ISwapeeLoginPageComputer>} xyz.swapee.wc.ISwapeeLoginPageComputer._adaptClearError */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError} */
/**
 * @param {!xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Form} form The form with inputs.
 * - `username` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Username_Safe*
 * - `password` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Password_Safe*
 * @param {xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Form} changes The previous values of the form.
 * - `username` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Username_Safe*
 * - `password` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Password_Safe*
 * @return {void|xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Return} The form with outputs.
 */
xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.Model.Username_Safe&xyz.swapee.wc.ISwapeeLoginPageCore.Model.Password_Safe} xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.Model.Error} xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Form, changes: xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Form) => (void|xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Return)} xyz.swapee.wc.ISwapeeLoginPageComputer.__adaptCredentialsError
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageComputer.__adaptCredentialsError<!xyz.swapee.wc.ISwapeeLoginPageComputer>} xyz.swapee.wc.ISwapeeLoginPageComputer._adaptCredentialsError */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError} */
/**
 * @param {!xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Form} form The form with inputs.
 * @param {xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Form} changes The previous values of the form.
 * @return {void|xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Return} The form with outputs.
 */
xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.Model.ErrorText_Safe} xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.Model.CredentialsError} xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Form, changes: xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Return|void)>)} xyz.swapee.wc.ISwapeeLoginPageComputer.__adaptLogin
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageComputer.__adaptLogin<!xyz.swapee.wc.ISwapeeLoginPageComputer>} xyz.swapee.wc.ISwapeeLoginPageComputer._adaptLogin */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin} */
/**
 * @param {!xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Form} form The form with inputs.
 * - `host` _string_ The Swapee.me host. ⤴ *ISwapeeLoginPageOuterCore.Model.Host_Safe*
 * - `username` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Username_Safe*
 * - `password` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Password_Safe*
 * - `signIn` _boolean_ The signin action. ⤴ *ISwapeeLoginPageOuterCore.Model.SignIn_Safe*
 * @param {xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Form} changes The previous values of the form.
 * - `host` _string_ The Swapee.me host. ⤴ *ISwapeeLoginPageOuterCore.Model.Host_Safe*
 * - `username` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Username_Safe*
 * - `password` _string_ ⤴ *ISwapeeLoginPageOuterCore.Model.Password_Safe*
 * - `signIn` _boolean_ The signin action. ⤴ *ISwapeeLoginPageOuterCore.Model.SignIn_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.Model.Host_Safe&xyz.swapee.wc.ISwapeeLoginPageCore.Model.Username_Safe&xyz.swapee.wc.ISwapeeLoginPageCore.Model.Password_Safe&xyz.swapee.wc.ISwapeeLoginPageCore.Model.SignIn_Safe} xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.Model.SignedIn&xyz.swapee.wc.ISwapeeMePort.Inputs.Token&xyz.swapee.wc.ISwapeeMePort.Inputs.Username&xyz.swapee.wc.ISwapeeMePort.Inputs.Userid&xyz.swapee.wc.ISwapeeMePort.Inputs.DisplayName&xyz.swapee.wc.ISwapeeMePort.Inputs.Profile} xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.SwapeeLoginPageMemory, land: !xyz.swapee.wc.ISwapeeLoginPageComputer.compute.Land) => void} xyz.swapee.wc.ISwapeeLoginPageComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageComputer.__compute<!xyz.swapee.wc.ISwapeeLoginPageComputer>} xyz.swapee.wc.ISwapeeLoginPageComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.SwapeeLoginPageMemory} mem The memory.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageComputer.compute.Land} land The land.
 * - `SwapeeMe` _!xyz.swapee.wc.SwapeeMeMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/03-ISwapeeLoginPageOuterCore.xml}  1554236c429ce86e0af9c1ff80f24cc5 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageOuterCore)} xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageOuterCore} xyz.swapee.wc.SwapeeLoginPageOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore
 */
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.constructor&xyz.swapee.wc.SwapeeLoginPageOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageOuterCore|typeof xyz.swapee.wc.SwapeeLoginPageOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageOuterCore|typeof xyz.swapee.wc.SwapeeLoginPageOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageOuterCore|typeof xyz.swapee.wc.SwapeeLoginPageOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageOuterCoreCaster)} xyz.swapee.wc.ISwapeeLoginPageOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _ISwapeeLoginPage_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ISwapeeLoginPageOuterCore
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCore = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoginPageOuterCore.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPageOuterCore

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageOuterCore.Initialese>)} xyz.swapee.wc.SwapeeLoginPageOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageOuterCore} xyz.swapee.wc.ISwapeeLoginPageOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeLoginPageOuterCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageOuterCore
 * @implements {xyz.swapee.wc.ISwapeeLoginPageOuterCore} The _ISwapeeLoginPage_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageOuterCore = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageOuterCore.constructor&xyz.swapee.wc.ISwapeeLoginPageOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeLoginPageOuterCore.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageOuterCore}
 */
xyz.swapee.wc.SwapeeLoginPageOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageOuterCore.
 * @interface xyz.swapee.wc.ISwapeeLoginPageOuterCoreFields
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCoreFields = class { }
/**
 * The _ISwapeeLoginPage_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore} */
xyz.swapee.wc.RecordISwapeeLoginPageOuterCore

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore} xyz.swapee.wc.BoundISwapeeLoginPageOuterCore */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageOuterCore} xyz.swapee.wc.BoundSwapeeLoginPageOuterCore */

/**
 * The Swapee.me host.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Host.host

/**
 * The signin action.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignIn.signIn

/**
 * Private pulse issued by the adapter after successful signin.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignedIn.signedIn

/** @typedef {string} */
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Username.username

/** @typedef {string} */
xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Password.password

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Host&xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignIn&xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignedIn&xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Username&xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Password} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model The _ISwapeeLoginPage_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Host&xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignIn&xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignedIn&xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Username&xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Password} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel The _ISwapeeLoginPage_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ISwapeeLoginPageOuterCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageOuterCoreCaster
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCoreCaster = class { }
/**
 * Cast the _ISwapeeLoginPageOuterCore_ instance into the _BoundISwapeeLoginPageOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageOuterCore}
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCoreCaster.prototype.asISwapeeLoginPageOuterCore
/**
 * Access the _SwapeeLoginPageOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageOuterCore}
 */
xyz.swapee.wc.ISwapeeLoginPageOuterCoreCaster.prototype.superSwapeeLoginPageOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Host The Swapee.me host (optional overlay).
 * @prop {string} [host="https://swapee.me/api"] The Swapee.me host. Default `https://swapee.me/api`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Host_Safe The Swapee.me host (required overlay).
 * @prop {string} host The Swapee.me host.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignIn The signin action (optional overlay).
 * @prop {boolean} [signIn=false] The signin action. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignIn_Safe The signin action (required overlay).
 * @prop {boolean} signIn The signin action.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignedIn Private pulse issued by the adapter after successful signin (optional overlay).
 * @prop {boolean} [signedIn=false] Private pulse issued by the adapter after successful signin. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignedIn_Safe Private pulse issued by the adapter after successful signin (required overlay).
 * @prop {boolean} signedIn Private pulse issued by the adapter after successful signin.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Username  (optional overlay).
 * @prop {string} [username=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Username_Safe  (required overlay).
 * @prop {string} username
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Password  (optional overlay).
 * @prop {string} [password=""] Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Password_Safe  (required overlay).
 * @prop {string} password
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Host The Swapee.me host (optional overlay).
 * @prop {*} [host="https://swapee.me/api"] The Swapee.me host. Default `https://swapee.me/api`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Host_Safe The Swapee.me host (required overlay).
 * @prop {*} host The Swapee.me host.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignIn The signin action (optional overlay).
 * @prop {*} [signIn=null] The signin action. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignIn_Safe The signin action (required overlay).
 * @prop {*} signIn The signin action.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignedIn Private pulse issued by the adapter after successful signin (optional overlay).
 * @prop {*} [signedIn=null] Private pulse issued by the adapter after successful signin. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignedIn_Safe Private pulse issued by the adapter after successful signin (required overlay).
 * @prop {*} signedIn Private pulse issued by the adapter after successful signin.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Username  (optional overlay).
 * @prop {*} [username=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Username_Safe  (required overlay).
 * @prop {*} username
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Password  (optional overlay).
 * @prop {*} [password=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Password_Safe  (required overlay).
 * @prop {*} password
 */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Host} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.Host The Swapee.me host (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.Host_Safe The Swapee.me host (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignIn} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.SignIn The signin action (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignIn_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.SignIn_Safe The signin action (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignedIn} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.SignedIn Private pulse issued by the adapter after successful signin (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignedIn_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.SignedIn_Safe Private pulse issued by the adapter after successful signin (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Username} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.Username  (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Username_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.Username_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Password} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.Password  (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Password_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.Password_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Host} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.Host The Swapee.me host (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.Host_Safe The Swapee.me host (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignIn} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.SignIn The signin action (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignIn_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.SignIn_Safe The signin action (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignedIn} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.SignedIn Private pulse issued by the adapter after successful signin (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.SignedIn_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.SignedIn_Safe Private pulse issued by the adapter after successful signin (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Username} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.Username  (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Username_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.Username_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Password} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.Password  (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.Password_Safe} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.Password_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Host} xyz.swapee.wc.ISwapeeLoginPageCore.Model.Host The Swapee.me host (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Host_Safe} xyz.swapee.wc.ISwapeeLoginPageCore.Model.Host_Safe The Swapee.me host (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignIn} xyz.swapee.wc.ISwapeeLoginPageCore.Model.SignIn The signin action (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignIn_Safe} xyz.swapee.wc.ISwapeeLoginPageCore.Model.SignIn_Safe The signin action (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignedIn} xyz.swapee.wc.ISwapeeLoginPageCore.Model.SignedIn Private pulse issued by the adapter after successful signin (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.SignedIn_Safe} xyz.swapee.wc.ISwapeeLoginPageCore.Model.SignedIn_Safe Private pulse issued by the adapter after successful signin (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Username} xyz.swapee.wc.ISwapeeLoginPageCore.Model.Username  (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Username_Safe} xyz.swapee.wc.ISwapeeLoginPageCore.Model.Username_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Password} xyz.swapee.wc.ISwapeeLoginPageCore.Model.Password  (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model.Password_Safe} xyz.swapee.wc.ISwapeeLoginPageCore.Model.Password_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/04-ISwapeeLoginPagePort.xml}  f6f38c5d48d445f93574c68207744c71 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeLoginPagePort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPagePort)} xyz.swapee.wc.AbstractSwapeeLoginPagePort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPagePort} xyz.swapee.wc.SwapeeLoginPagePort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPagePort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPagePort
 */
xyz.swapee.wc.AbstractSwapeeLoginPagePort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPagePort.constructor&xyz.swapee.wc.SwapeeLoginPagePort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPagePort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPagePort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPagePort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPagePort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPagePort|typeof xyz.swapee.wc.SwapeeLoginPagePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPagePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPagePort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPagePort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPagePort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPagePort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPagePort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPagePort|typeof xyz.swapee.wc.SwapeeLoginPagePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPagePort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPagePort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPagePort|typeof xyz.swapee.wc.SwapeeLoginPagePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPagePort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPagePort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPagePort.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPagePort} xyz.swapee.wc.SwapeeLoginPagePortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPagePortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPagePortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeLoginPagePort.Inputs>)} xyz.swapee.wc.ISwapeeLoginPagePort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ISwapeeLoginPage_, providing input
 * pins.
 * @interface xyz.swapee.wc.ISwapeeLoginPagePort
 */
xyz.swapee.wc.ISwapeeLoginPagePort = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPagePort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPagePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPagePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPagePort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoginPagePort.resetPort} */
xyz.swapee.wc.ISwapeeLoginPagePort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPagePort.resetSwapeeLoginPagePort} */
xyz.swapee.wc.ISwapeeLoginPagePort.prototype.resetSwapeeLoginPagePort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPagePort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPagePort.Initialese>)} xyz.swapee.wc.SwapeeLoginPagePort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPagePort} xyz.swapee.wc.ISwapeeLoginPagePort.typeof */
/**
 * A concrete class of _ISwapeeLoginPagePort_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPagePort
 * @implements {xyz.swapee.wc.ISwapeeLoginPagePort} The port that serves as an interface to the _ISwapeeLoginPage_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPagePort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPagePort = class extends /** @type {xyz.swapee.wc.SwapeeLoginPagePort.constructor&xyz.swapee.wc.ISwapeeLoginPagePort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPagePort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPagePort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPagePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPagePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPagePort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPagePort}
 */
xyz.swapee.wc.SwapeeLoginPagePort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPagePort.
 * @interface xyz.swapee.wc.ISwapeeLoginPagePortFields
 */
xyz.swapee.wc.ISwapeeLoginPagePortFields = class { }
/**
 * The inputs to the _ISwapeeLoginPage_'s controller via its port.
 */
xyz.swapee.wc.ISwapeeLoginPagePortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoginPagePort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeLoginPagePortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeLoginPagePort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPagePort} */
xyz.swapee.wc.RecordISwapeeLoginPagePort

/** @typedef {xyz.swapee.wc.ISwapeeLoginPagePort} xyz.swapee.wc.BoundISwapeeLoginPagePort */

/** @typedef {xyz.swapee.wc.SwapeeLoginPagePort} xyz.swapee.wc.BoundSwapeeLoginPagePort */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel} xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ISwapeeLoginPage_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeLoginPagePort.Inputs
 */
xyz.swapee.wc.ISwapeeLoginPagePort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.constructor&xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoginPagePort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPagePort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.constructor */
/**
 * The inputs to the _ISwapeeLoginPage_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs
 */
xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPagePortInterface
 */
xyz.swapee.wc.ISwapeeLoginPagePortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ISwapeeLoginPagePortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeLoginPagePortInterface.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPagePortInterface

/**
 * A concrete class of _ISwapeeLoginPagePortInterface_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPagePortInterface
 * @implements {xyz.swapee.wc.ISwapeeLoginPagePortInterface} The port interface.
 */
xyz.swapee.wc.SwapeeLoginPagePortInterface = class extends xyz.swapee.wc.ISwapeeLoginPagePortInterface { }
xyz.swapee.wc.SwapeeLoginPagePortInterface.prototype.constructor = xyz.swapee.wc.SwapeeLoginPagePortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPagePortInterface.Props
 * @prop {boolean} signIn The signin action.
 * @prop {boolean} signedIn Private pulse issued by the adapter after successful signin.
 * @prop {string} username
 * @prop {string} password
 * @prop {string} [host="https://swapee.me/api"] The Swapee.me host. Default `https://swapee.me/api`.
 */

/**
 * Contains getters to cast the _ISwapeeLoginPagePort_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPagePortCaster
 */
xyz.swapee.wc.ISwapeeLoginPagePortCaster = class { }
/**
 * Cast the _ISwapeeLoginPagePort_ instance into the _BoundISwapeeLoginPagePort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPagePort}
 */
xyz.swapee.wc.ISwapeeLoginPagePortCaster.prototype.asISwapeeLoginPagePort
/**
 * Access the _SwapeeLoginPagePort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPagePort}
 */
xyz.swapee.wc.ISwapeeLoginPagePortCaster.prototype.superSwapeeLoginPagePort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPagePort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPagePort.__resetPort<!xyz.swapee.wc.ISwapeeLoginPagePort>} xyz.swapee.wc.ISwapeeLoginPagePort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPagePort.resetPort} */
/**
 * Resets the _ISwapeeLoginPage_ port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPagePort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPagePort.__resetSwapeeLoginPagePort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPagePort.__resetSwapeeLoginPagePort<!xyz.swapee.wc.ISwapeeLoginPagePort>} xyz.swapee.wc.ISwapeeLoginPagePort._resetSwapeeLoginPagePort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPagePort.resetSwapeeLoginPagePort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPagePort.resetSwapeeLoginPagePort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPagePort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/08-ISwapeeLoginPageCPU.xml}  61173515e6ea8a9d7ded14db264e7bb3 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeLoginPageCPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageCPU)} xyz.swapee.wc.AbstractSwapeeLoginPageCPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageCPU} xyz.swapee.wc.SwapeeLoginPageCPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageCPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageCPU
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageCPU.constructor&xyz.swapee.wc.SwapeeLoginPageCPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageCPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageCPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageCPU|typeof xyz.swapee.wc.SwapeeLoginPageCPU)|(!guest.maurice.IStatefulLoadable|typeof guest.maurice.StatefulLoadable)|(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|!xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageCPU|typeof xyz.swapee.wc.SwapeeLoginPageCPU)|(!guest.maurice.IStatefulLoadable|typeof guest.maurice.StatefulLoadable)|(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|!xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageCPU|typeof xyz.swapee.wc.SwapeeLoginPageCPU)|(!guest.maurice.IStatefulLoadable|typeof guest.maurice.StatefulLoadable)|(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|!xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageCPU.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageCPU} xyz.swapee.wc.SwapeeLoginPageCPUConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice
 */
xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice = class {
  constructor() {
    /**
     * Resets all cores.
     */
    this.resetCore=/** @type {!xyz.swapee.wc.ISwapeeLoginPageCPU._resetCore|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeLoginPageCPU._resetCore>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice

/**
 * A concrete class of _ISwapeeLoginPageCPUHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageCPUHyperslice
 * @implements {xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.SwapeeLoginPageCPUHyperslice = class extends xyz.swapee.wc.ISwapeeLoginPageCPUHyperslice { }
xyz.swapee.wc.SwapeeLoginPageCPUHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageCPUHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice = class {
  constructor() {
    /**
     * Resets all cores.
     */
    this.resetCore=/** @type {!xyz.swapee.wc.ISwapeeLoginPageCPU.__resetCore<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeLoginPageCPU.__resetCore<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice

/**
 * A concrete class of _ISwapeeLoginPageCPUBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageCPUBindingHyperslice
 * @implements {xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice<THIS>}
 */
xyz.swapee.wc.SwapeeLoginPageCPUBindingHyperslice = class extends xyz.swapee.wc.ISwapeeLoginPageCPUBindingHyperslice { }
xyz.swapee.wc.SwapeeLoginPageCPUBindingHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageCPUBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageCPUFields&guest.maurice.IStatefulLoadable&xyz.swapee.wc.ISwapeeLoginPageCore&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageCPUCaster)} xyz.swapee.wc.ISwapeeLoginPageCPU.constructor */
/** @typedef {typeof guest.maurice.IStatefulLoadable} guest.maurice.IStatefulLoadable.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageCore} xyz.swapee.wc.ISwapeeLoginPageCore.typeof */
/**
 * The _ISwapeeLoginPage_'s CPU that encapsulates the component's native atomic
 * core, as well as additional embedded cores.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCPU
 */
xyz.swapee.wc.ISwapeeLoginPageCPU = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageCPU.constructor&guest.maurice.IStatefulLoadable.typeof&xyz.swapee.wc.ISwapeeLoginPageCore.typeof&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageCPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageCPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageCPU.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageCPU.resetCore} */
xyz.swapee.wc.ISwapeeLoginPageCPU.prototype.resetCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageCPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageCPU.Initialese>)} xyz.swapee.wc.SwapeeLoginPageCPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageCPU} xyz.swapee.wc.ISwapeeLoginPageCPU.typeof */
/**
 * A concrete class of _ISwapeeLoginPageCPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageCPU
 * @implements {xyz.swapee.wc.ISwapeeLoginPageCPU} The _ISwapeeLoginPage_'s CPU that encapsulates the component's native atomic
 * core, as well as additional embedded cores.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageCPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageCPU = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageCPU.constructor&xyz.swapee.wc.ISwapeeLoginPageCPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageCPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageCPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageCPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageCPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageCPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCPU}
 */
xyz.swapee.wc.SwapeeLoginPageCPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageCPU.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCPUFields
 */
xyz.swapee.wc.ISwapeeLoginPageCPUFields = class { }
/**
 * The model comprises of 2 cores.
 */
xyz.swapee.wc.ISwapeeLoginPageCPUFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeLoginPageCPU.Model} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeLoginPageCPUFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeLoginPageCPU.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCPU} */
xyz.swapee.wc.RecordISwapeeLoginPageCPU

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCPU} xyz.swapee.wc.BoundISwapeeLoginPageCPU */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageCPU} xyz.swapee.wc.BoundSwapeeLoginPageCPU */

/** @typedef {guest.maurice.IStatefulLoadable.State&xyz.swapee.wc.ISwapeeLoginPageCore.Model} xyz.swapee.wc.ISwapeeLoginPageCPU.Model The model comprises of 2 cores. */

/**
 * Contains getters to cast the _ISwapeeLoginPageCPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCPUCaster
 */
xyz.swapee.wc.ISwapeeLoginPageCPUCaster = class { }
/**
 * Cast the _ISwapeeLoginPageCPU_ instance into the _BoundISwapeeLoginPageCPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageCPU}
 */
xyz.swapee.wc.ISwapeeLoginPageCPUCaster.prototype.asISwapeeLoginPageCPU
/**
 * Access the _SwapeeLoginPageCPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageCPU}
 */
xyz.swapee.wc.ISwapeeLoginPageCPUCaster.prototype.superSwapeeLoginPageCPU

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageCPU.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCPU.__resetCore<!xyz.swapee.wc.ISwapeeLoginPageCPU>} xyz.swapee.wc.ISwapeeLoginPageCPU._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageCPU.resetCore} */
/**
 * Resets all cores. `🔗 $combine`
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageCPU.resetCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageCPU
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/09-ISwapeeLoginPageCore.xml}  302311e53d39d827628ddd1a2e0f9c0f */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeLoginPageCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageCore)} xyz.swapee.wc.AbstractSwapeeLoginPageCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageCore} xyz.swapee.wc.SwapeeLoginPageCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageCore
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageCore.constructor&xyz.swapee.wc.SwapeeLoginPageCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|(!xyz.swapee.wc.ISwapeeLoginPageOuterCore|typeof xyz.swapee.wc.SwapeeLoginPageOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|(!xyz.swapee.wc.ISwapeeLoginPageOuterCore|typeof xyz.swapee.wc.SwapeeLoginPageOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|(!xyz.swapee.wc.ISwapeeLoginPageOuterCore|typeof xyz.swapee.wc.SwapeeLoginPageOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCore}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageCoreCaster&xyz.swapee.wc.ISwapeeLoginPageOuterCore)} xyz.swapee.wc.ISwapeeLoginPageCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageOuterCore} xyz.swapee.wc.ISwapeeLoginPageOuterCore.typeof */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCore
 */
xyz.swapee.wc.ISwapeeLoginPageCore = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoginPageOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeLoginPageCore.resetCore} */
xyz.swapee.wc.ISwapeeLoginPageCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageCore.resetSwapeeLoginPageCore} */
xyz.swapee.wc.ISwapeeLoginPageCore.prototype.resetSwapeeLoginPageCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageCore.Initialese>)} xyz.swapee.wc.SwapeeLoginPageCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageCore} xyz.swapee.wc.ISwapeeLoginPageCore.typeof */
/**
 * A concrete class of _ISwapeeLoginPageCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageCore
 * @implements {xyz.swapee.wc.ISwapeeLoginPageCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageCore = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageCore.constructor&xyz.swapee.wc.ISwapeeLoginPageCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeLoginPageCore.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageCore}
 */
xyz.swapee.wc.SwapeeLoginPageCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageCore.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCoreFields
 */
xyz.swapee.wc.ISwapeeLoginPageCoreFields = class { }
/**
 * The _ISwapeeLoginPage_'s memory.
 */
xyz.swapee.wc.ISwapeeLoginPageCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeLoginPageCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ISwapeeLoginPageCoreFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeLoginPageCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore} */
xyz.swapee.wc.RecordISwapeeLoginPageCore

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore} xyz.swapee.wc.BoundISwapeeLoginPageCore */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageCore} xyz.swapee.wc.BoundSwapeeLoginPageCore */

/**
 * Whether the username/password pair is incorrect.
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeLoginPageCore.Model.CredentialsError.credentialsError

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model&xyz.swapee.wc.ISwapeeLoginPageCore.Model.CredentialsError} xyz.swapee.wc.ISwapeeLoginPageCore.Model The _ISwapeeLoginPage_'s memory. */

/**
 * Contains getters to cast the _ISwapeeLoginPageCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCoreCaster
 */
xyz.swapee.wc.ISwapeeLoginPageCoreCaster = class { }
/**
 * Cast the _ISwapeeLoginPageCore_ instance into the _BoundISwapeeLoginPageCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageCore}
 */
xyz.swapee.wc.ISwapeeLoginPageCoreCaster.prototype.asISwapeeLoginPageCore
/**
 * Access the _SwapeeLoginPageCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageCore}
 */
xyz.swapee.wc.ISwapeeLoginPageCoreCaster.prototype.superSwapeeLoginPageCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageCore.Model.CredentialsError Whether the username/password pair is incorrect (optional overlay).
 * @prop {boolean} [credentialsError=false] Whether the username/password pair is incorrect. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageCore.Model.CredentialsError_Safe Whether the username/password pair is incorrect (required overlay).
 * @prop {boolean} credentialsError Whether the username/password pair is incorrect.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.__resetCore<!xyz.swapee.wc.ISwapeeLoginPageCore>} xyz.swapee.wc.ISwapeeLoginPageCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageCore.resetCore} */
/**
 * Resets the _ISwapeeLoginPage_ core.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageCore.__resetSwapeeLoginPageCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageCore.__resetSwapeeLoginPageCore<!xyz.swapee.wc.ISwapeeLoginPageCore>} xyz.swapee.wc.ISwapeeLoginPageCore._resetSwapeeLoginPageCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageCore.resetSwapeeLoginPageCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageCore.resetSwapeeLoginPageCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/10-ISwapeeLoginPageProcessor.xml}  5c6cf44aab12c0714a4dd38d65044af5 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese&xyz.swapee.wc.ISwapeeLoginPageController.Initialese} xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageProcessor)} xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageProcessor} xyz.swapee.wc.SwapeeLoginPageProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageProcessor
 */
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.constructor&xyz.swapee.wc.SwapeeLoginPageProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!xyz.swapee.wc.ISwapeeLoginPageCore|typeof xyz.swapee.wc.SwapeeLoginPageCore)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageProcessor}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageProcessor} xyz.swapee.wc.SwapeeLoginPageProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageProcessorCaster&xyz.swapee.wc.ISwapeeLoginPageComputer&xyz.swapee.wc.ISwapeeLoginPageCore&xyz.swapee.wc.ISwapeeLoginPageController)} xyz.swapee.wc.ISwapeeLoginPageProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageCore} xyz.swapee.wc.ISwapeeLoginPageCore.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController} xyz.swapee.wc.ISwapeeLoginPageController.typeof */
/**
 * The processor to compute changes to the memory for the _ISwapeeLoginPage_.
 * @interface xyz.swapee.wc.ISwapeeLoginPageProcessor
 */
xyz.swapee.wc.ISwapeeLoginPageProcessor = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoginPageComputer.typeof&xyz.swapee.wc.ISwapeeLoginPageCore.typeof&xyz.swapee.wc.ISwapeeLoginPageController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageProcessor.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageProcessor.pulseSignIn} */
xyz.swapee.wc.ISwapeeLoginPageProcessor.prototype.pulseSignIn = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageProcessor.pulseSignedIn} */
xyz.swapee.wc.ISwapeeLoginPageProcessor.prototype.pulseSignedIn = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese>)} xyz.swapee.wc.SwapeeLoginPageProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageProcessor} xyz.swapee.wc.ISwapeeLoginPageProcessor.typeof */
/**
 * A concrete class of _ISwapeeLoginPageProcessor_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageProcessor
 * @implements {xyz.swapee.wc.ISwapeeLoginPageProcessor} The processor to compute changes to the memory for the _ISwapeeLoginPage_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageProcessor = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageProcessor.constructor&xyz.swapee.wc.ISwapeeLoginPageProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageProcessor}
 */
xyz.swapee.wc.SwapeeLoginPageProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageProcessor} */
xyz.swapee.wc.RecordISwapeeLoginPageProcessor

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageProcessor} xyz.swapee.wc.BoundISwapeeLoginPageProcessor */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageProcessor} xyz.swapee.wc.BoundSwapeeLoginPageProcessor */

/**
 * Contains getters to cast the _ISwapeeLoginPageProcessor_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageProcessorCaster
 */
xyz.swapee.wc.ISwapeeLoginPageProcessorCaster = class { }
/**
 * Cast the _ISwapeeLoginPageProcessor_ instance into the _BoundISwapeeLoginPageProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageProcessor}
 */
xyz.swapee.wc.ISwapeeLoginPageProcessorCaster.prototype.asISwapeeLoginPageProcessor
/**
 * Access the _SwapeeLoginPageProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageProcessor}
 */
xyz.swapee.wc.ISwapeeLoginPageProcessorCaster.prototype.superSwapeeLoginPageProcessor

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageProcessor.__pulseSignIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageProcessor.__pulseSignIn<!xyz.swapee.wc.ISwapeeLoginPageProcessor>} xyz.swapee.wc.ISwapeeLoginPageProcessor._pulseSignIn */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageProcessor.pulseSignIn} */
/**
 * A method called to set the `signIn` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageProcessor.pulseSignIn = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageProcessor.__pulseSignedIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageProcessor.__pulseSignedIn<!xyz.swapee.wc.ISwapeeLoginPageProcessor>} xyz.swapee.wc.ISwapeeLoginPageProcessor._pulseSignedIn */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageProcessor.pulseSignedIn} */
/**
 * A method called to set the `signedIn` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageProcessor.pulseSignedIn = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageProcessor
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/100-SwapeeLoginPageMemory.xml}  09fc0e37c5c69b15dff525891de6407a */
/**
 * The memory of the _ISwapeeLoginPage_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.SwapeeLoginPageMemory
 */
xyz.swapee.wc.SwapeeLoginPageMemory = class { }
/**
 * The Swapee.me host. Default empty string.
 */
xyz.swapee.wc.SwapeeLoginPageMemory.prototype.host = /** @type {string} */ (void 0)
/**
 * The signin action. Default `false`.
 */
xyz.swapee.wc.SwapeeLoginPageMemory.prototype.signIn = /** @type {boolean} */ (void 0)
/**
 * Private pulse issued by the adapter after successful signin. Default `false`.
 */
xyz.swapee.wc.SwapeeLoginPageMemory.prototype.signedIn = /** @type {boolean} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.SwapeeLoginPageMemory.prototype.username = /** @type {string} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.SwapeeLoginPageMemory.prototype.password = /** @type {string} */ (void 0)
/**
 * Whether the username/password pair is incorrect. Default `false`.
 */
xyz.swapee.wc.SwapeeLoginPageMemory.prototype.credentialsError = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/102-SwapeeLoginPageInputs.xml}  645f0ce24d9bf60d71acf84f66d52543 */
/**
 * The inputs of the _ISwapeeLoginPage_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.SwapeeLoginPageInputs
 */
xyz.swapee.wc.front.SwapeeLoginPageInputs = class { }
/**
 * The Swapee.me host. Default `https://swapee.me/api`.
 */
xyz.swapee.wc.front.SwapeeLoginPageInputs.prototype.host = /** @type {string|undefined} */ (void 0)
/**
 * The signin action. Default `false`.
 */
xyz.swapee.wc.front.SwapeeLoginPageInputs.prototype.signIn = /** @type {boolean|undefined} */ (void 0)
/**
 * Private pulse issued by the adapter after successful signin. Default `false`.
 */
xyz.swapee.wc.front.SwapeeLoginPageInputs.prototype.signedIn = /** @type {boolean|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.SwapeeLoginPageInputs.prototype.username = /** @type {string|undefined} */ (void 0)
/**
 * Default empty string.
 */
xyz.swapee.wc.front.SwapeeLoginPageInputs.prototype.password = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/11-ISwapeeLoginPage.xml}  c2a75e36df8f419b2020351b1f7169b2 */
/**
 * An atomic wrapper for the _ISwapeeLoginPage_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.SwapeeLoginPageEnv
 */
xyz.swapee.wc.SwapeeLoginPageEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.SwapeeLoginPageEnv.prototype.swapeeLoginPage = /** @type {xyz.swapee.wc.ISwapeeLoginPage} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.ISwapeeLoginPageController.Inputs>&xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese&xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese&xyz.swapee.wc.ISwapeeLoginPageController.Initialese} xyz.swapee.wc.ISwapeeLoginPage.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPage)} xyz.swapee.wc.AbstractSwapeeLoginPage.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPage} xyz.swapee.wc.SwapeeLoginPage.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPage` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPage
 */
xyz.swapee.wc.AbstractSwapeeLoginPage = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPage.constructor&xyz.swapee.wc.SwapeeLoginPage.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPage.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPage.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPage|typeof xyz.swapee.wc.SwapeeLoginPage)|(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPage}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPage}
 */
xyz.swapee.wc.AbstractSwapeeLoginPage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPage}
 */
xyz.swapee.wc.AbstractSwapeeLoginPage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPage|typeof xyz.swapee.wc.SwapeeLoginPage)|(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPage}
 */
xyz.swapee.wc.AbstractSwapeeLoginPage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPage|typeof xyz.swapee.wc.SwapeeLoginPage)|(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPage}
 */
xyz.swapee.wc.AbstractSwapeeLoginPage.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPage.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPage} xyz.swapee.wc.SwapeeLoginPageConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPage.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ISwapeeLoginPage.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ISwapeeLoginPage.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ISwapeeLoginPage.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.SwapeeLoginPageMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.SwapeeLoginPageClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageCaster&xyz.swapee.wc.ISwapeeLoginPageProcessor&xyz.swapee.wc.ISwapeeLoginPageComputer&xyz.swapee.wc.ISwapeeLoginPageController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.ISwapeeLoginPageController.Inputs, !xyz.swapee.wc.SwapeeLoginPageLand>)} xyz.swapee.wc.ISwapeeLoginPage.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageProcessor} xyz.swapee.wc.ISwapeeLoginPageProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController} xyz.swapee.wc.ISwapeeLoginPageController.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ISwapeeLoginPage
 */
xyz.swapee.wc.ISwapeeLoginPage = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoginPageProcessor.typeof&xyz.swapee.wc.ISwapeeLoginPageComputer.typeof&xyz.swapee.wc.ISwapeeLoginPageController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPage.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPage&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPage.Initialese>)} xyz.swapee.wc.SwapeeLoginPage.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPage} xyz.swapee.wc.ISwapeeLoginPage.typeof */
/**
 * A concrete class of _ISwapeeLoginPage_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPage
 * @implements {xyz.swapee.wc.ISwapeeLoginPage} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPage.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPage = class extends /** @type {xyz.swapee.wc.SwapeeLoginPage.constructor&xyz.swapee.wc.ISwapeeLoginPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPage.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPage}
 */
xyz.swapee.wc.SwapeeLoginPage.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPage.
 * @interface xyz.swapee.wc.ISwapeeLoginPageFields
 */
xyz.swapee.wc.ISwapeeLoginPageFields = class { }
/**
 * The input pins of the _ISwapeeLoginPage_ port.
 */
xyz.swapee.wc.ISwapeeLoginPageFields.prototype.pinout = /** @type {!xyz.swapee.wc.ISwapeeLoginPage.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPage} */
xyz.swapee.wc.RecordISwapeeLoginPage

/** @typedef {xyz.swapee.wc.ISwapeeLoginPage} xyz.swapee.wc.BoundISwapeeLoginPage */

/** @typedef {xyz.swapee.wc.SwapeeLoginPage} xyz.swapee.wc.BoundSwapeeLoginPage */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.Inputs} xyz.swapee.wc.ISwapeeLoginPage.Pinout The input pins of the _ISwapeeLoginPage_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs>)} xyz.swapee.wc.ISwapeeLoginPageBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ISwapeeLoginPageBuffer
 */
xyz.swapee.wc.ISwapeeLoginPageBuffer = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoginPageBuffer.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPageBuffer

/**
 * A concrete class of _ISwapeeLoginPageBuffer_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageBuffer
 * @implements {xyz.swapee.wc.ISwapeeLoginPageBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.SwapeeLoginPageBuffer = class extends xyz.swapee.wc.ISwapeeLoginPageBuffer { }
xyz.swapee.wc.SwapeeLoginPageBuffer.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageBuffer

/**
 * Contains getters to cast the _ISwapeeLoginPage_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageCaster
 */
xyz.swapee.wc.ISwapeeLoginPageCaster = class { }
/**
 * Cast the _ISwapeeLoginPage_ instance into the _BoundISwapeeLoginPage_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPage}
 */
xyz.swapee.wc.ISwapeeLoginPageCaster.prototype.asISwapeeLoginPage
/**
 * Access the _SwapeeLoginPage_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPage}
 */
xyz.swapee.wc.ISwapeeLoginPageCaster.prototype.superSwapeeLoginPage

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/110-SwapeeLoginPageSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeLoginPageMemoryPQs
 */
xyz.swapee.wc.SwapeeLoginPageMemoryPQs = class {
  constructor() {
    /**
     * `b4c4b`
     */
    this.username=/** @type {string} */ (void 0)
    /**
     * `ff4dc`
     */
    this.password=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeLoginPageMemoryPQs.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeLoginPageMemoryQPs
 * @dict
 */
xyz.swapee.wc.SwapeeLoginPageMemoryQPs = class { }
/**
 * `username`
 */
xyz.swapee.wc.SwapeeLoginPageMemoryQPs.prototype.b4c4b = /** @type {string} */ (void 0)
/**
 * `password`
 */
xyz.swapee.wc.SwapeeLoginPageMemoryQPs.prototype.ff4dc = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageMemoryPQs)} xyz.swapee.wc.SwapeeLoginPageInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageMemoryPQs} xyz.swapee.wc.SwapeeLoginPageMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeLoginPageInputsPQs
 */
xyz.swapee.wc.SwapeeLoginPageInputsPQs = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageInputsPQs.constructor&xyz.swapee.wc.SwapeeLoginPageMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeLoginPageInputsPQs.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageInputsPQs

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageMemoryPQs)} xyz.swapee.wc.SwapeeLoginPageInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeLoginPageInputsQPs
 * @dict
 */
xyz.swapee.wc.SwapeeLoginPageInputsQPs = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageInputsQPs.constructor&xyz.swapee.wc.SwapeeLoginPageMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeLoginPageInputsQPs.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeLoginPageVdusPQs
 */
xyz.swapee.wc.SwapeeLoginPageVdusPQs = class {
  constructor() {
    /**
     * `g54e1`
     */
    this.UsernameIn=/** @type {string} */ (void 0)
    /**
     * `g54e2`
     */
    this.LoginBu=/** @type {string} */ (void 0)
    /**
     * `g54e3`
     */
    this.PasswordIn=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeLoginPageVdusPQs.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeLoginPageVdusQPs
 * @dict
 */
xyz.swapee.wc.SwapeeLoginPageVdusQPs = class { }
/**
 * `UsernameIn`
 */
xyz.swapee.wc.SwapeeLoginPageVdusQPs.prototype.g54e1 = /** @type {string} */ (void 0)
/**
 * `LoginBu`
 */
xyz.swapee.wc.SwapeeLoginPageVdusQPs.prototype.g54e2 = /** @type {string} */ (void 0)
/**
 * `PasswordIn`
 */
xyz.swapee.wc.SwapeeLoginPageVdusQPs.prototype.g54e3 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/12-ISwapeeLoginPageHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtilFields)} xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.router} */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _ISwapeeLoginPageHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageHtmlComponentUtil
 * @implements {xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil} ‎
 */
xyz.swapee.wc.SwapeeLoginPageHtmlComponentUtil = class extends xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil { }
xyz.swapee.wc.SwapeeLoginPageHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageHtmlComponentUtil

/**
 * Fields of the ISwapeeLoginPageHtmlComponentUtil.
 * @interface xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtilFields
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil} */
xyz.swapee.wc.RecordISwapeeLoginPageHtmlComponentUtil

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil} xyz.swapee.wc.BoundISwapeeLoginPageHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageHtmlComponentUtil} xyz.swapee.wc.BoundSwapeeLoginPageHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.ISwapeeMePort} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeLoginPagePort} SwapeeLoginPage The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe
 * @prop {!xyz.swapee.wc.SwapeeLoginPageMemory} SwapeeLoginPage
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.ISwapeeMe.Pinout} SwapeeMe
 * @prop {!xyz.swapee.wc.ISwapeeLoginPage.Pinout} SwapeeLoginPage
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.__router<!xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil>} xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `SwapeeMe` _typeof ISwapeeMePort_
 * - `SwapeeLoginPage` _typeof ISwapeeLoginPagePort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `SwapeeMe` _!SwapeeMeMemory_
 * - `SwapeeLoginPage` _!SwapeeLoginPageMemory_
 * @param {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `SwapeeMe` _!ISwapeeMe.Pinout_
 * - `SwapeeLoginPage` _!ISwapeeLoginPage.Pinout_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/12-ISwapeeLoginPageHtmlComponent.xml}  32c12f113fac93a536f1e1f4fcd8e32a */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese&xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese&xyz.swapee.wc.ISwapeeLoginPage.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ISwapeeLoginPageProcessor.Initialese&xyz.swapee.wc.ISwapeeLoginPageComputer.Initialese} xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageHtmlComponent)} xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent} xyz.swapee.wc.SwapeeLoginPageHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent
 */
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.constructor&xyz.swapee.wc.SwapeeLoginPageHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent|typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeLoginPageController|typeof xyz.swapee.wc.back.SwapeeLoginPageController)|(!xyz.swapee.wc.back.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.back.SwapeeLoginPageScreen)|(!xyz.swapee.wc.ISwapeeLoginPage|typeof xyz.swapee.wc.SwapeeLoginPage)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeLoginPageGPU|typeof xyz.swapee.wc.SwapeeLoginPageGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent|typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeLoginPageController|typeof xyz.swapee.wc.back.SwapeeLoginPageController)|(!xyz.swapee.wc.back.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.back.SwapeeLoginPageScreen)|(!xyz.swapee.wc.ISwapeeLoginPage|typeof xyz.swapee.wc.SwapeeLoginPage)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeLoginPageGPU|typeof xyz.swapee.wc.SwapeeLoginPageGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent|typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeLoginPageController|typeof xyz.swapee.wc.back.SwapeeLoginPageController)|(!xyz.swapee.wc.back.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.back.SwapeeLoginPageScreen)|(!xyz.swapee.wc.ISwapeeLoginPage|typeof xyz.swapee.wc.SwapeeLoginPage)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeLoginPageGPU|typeof xyz.swapee.wc.SwapeeLoginPageGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeLoginPageProcessor|typeof xyz.swapee.wc.SwapeeLoginPageProcessor)|(!xyz.swapee.wc.ISwapeeLoginPageComputer|typeof xyz.swapee.wc.SwapeeLoginPageComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageHtmlComponent} xyz.swapee.wc.SwapeeLoginPageHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageHtmlComponentCaster&xyz.swapee.wc.back.ISwapeeLoginPageController&xyz.swapee.wc.back.ISwapeeLoginPageScreen&xyz.swapee.wc.ISwapeeLoginPage&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeLoginPageLand>&xyz.swapee.wc.ISwapeeLoginPageGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.ISwapeeLoginPageController.Inputs, !HTMLDivElement, !xyz.swapee.wc.SwapeeLoginPageLand>&xyz.swapee.wc.ISwapeeLoginPageProcessor&xyz.swapee.wc.ISwapeeLoginPageComputer)} xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoginPageController} xyz.swapee.wc.back.ISwapeeLoginPageController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoginPageScreen} xyz.swapee.wc.back.ISwapeeLoginPageScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPage} xyz.swapee.wc.ISwapeeLoginPage.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageGPU} xyz.swapee.wc.ISwapeeLoginPageGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageProcessor} xyz.swapee.wc.ISwapeeLoginPageProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageComputer} xyz.swapee.wc.ISwapeeLoginPageComputer.typeof */
/**
 * The _ISwapeeLoginPage_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ISwapeeLoginPageHtmlComponent
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponent = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeLoginPageController.typeof&xyz.swapee.wc.back.ISwapeeLoginPageScreen.typeof&xyz.swapee.wc.ISwapeeLoginPage.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.ISwapeeLoginPageGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ISwapeeLoginPageProcessor.typeof&xyz.swapee.wc.ISwapeeLoginPageComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.Initialese>)} xyz.swapee.wc.SwapeeLoginPageHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageHtmlComponent} xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeLoginPageHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageHtmlComponent
 * @implements {xyz.swapee.wc.ISwapeeLoginPageHtmlComponent} The _ISwapeeLoginPage_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageHtmlComponent = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageHtmlComponent.constructor&xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent}
 */
xyz.swapee.wc.SwapeeLoginPageHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageHtmlComponent} */
xyz.swapee.wc.RecordISwapeeLoginPageHtmlComponent

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageHtmlComponent} xyz.swapee.wc.BoundISwapeeLoginPageHtmlComponent */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageHtmlComponent} xyz.swapee.wc.BoundSwapeeLoginPageHtmlComponent */

/**
 * Contains getters to cast the _ISwapeeLoginPageHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageHtmlComponentCaster
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentCaster = class { }
/**
 * Cast the _ISwapeeLoginPageHtmlComponent_ instance into the _BoundISwapeeLoginPageHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageHtmlComponent}
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentCaster.prototype.asISwapeeLoginPageHtmlComponent
/**
 * Access the _SwapeeLoginPageHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageHtmlComponent}
 */
xyz.swapee.wc.ISwapeeLoginPageHtmlComponentCaster.prototype.superSwapeeLoginPageHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/130-ISwapeeLoginPageElement.xml}  9ef19e9caf3f60c63e0f8a96ac6d4ae1 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.SwapeeLoginPageLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.ISwapeeMe>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.ISwapeeLoginPageElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ISwapeeLoginPageElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageElement)} xyz.swapee.wc.AbstractSwapeeLoginPageElement.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageElement} xyz.swapee.wc.SwapeeLoginPageElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageElement` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageElement
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElement = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageElement.constructor&xyz.swapee.wc.SwapeeLoginPageElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageElement.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElement.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageElement|typeof xyz.swapee.wc.SwapeeLoginPageElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElement}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElement}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageElement|typeof xyz.swapee.wc.SwapeeLoginPageElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElement}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageElement|typeof xyz.swapee.wc.SwapeeLoginPageElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElement}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageElement.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageElement} xyz.swapee.wc.SwapeeLoginPageElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageElementFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.ISwapeeLoginPageElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.ISwapeeLoginPageElement.Inputs, !xyz.swapee.wc.SwapeeLoginPageLand>&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeLoginPageLand>&guest.maurice.IMilleu<!xyz.swapee.wc.ISwapeeMe>)} xyz.swapee.wc.ISwapeeLoginPageElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _ISwapeeLoginPage_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ISwapeeLoginPageElement
 */
xyz.swapee.wc.ISwapeeLoginPageElement = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageElement.solder} */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageElement.render} */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageElement.build} */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageElement.buildSwapeeMe} */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.buildSwapeeMe = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageElement.short} */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageElement.server} */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageElement.inducer} */
xyz.swapee.wc.ISwapeeLoginPageElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageElement&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageElement.Initialese>)} xyz.swapee.wc.SwapeeLoginPageElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement} xyz.swapee.wc.ISwapeeLoginPageElement.typeof */
/**
 * A concrete class of _ISwapeeLoginPageElement_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageElement
 * @implements {xyz.swapee.wc.ISwapeeLoginPageElement} A component description.
 *
 * The _ISwapeeLoginPage_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageElement.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageElement = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageElement.constructor&xyz.swapee.wc.ISwapeeLoginPageElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElement}
 */
xyz.swapee.wc.SwapeeLoginPageElement.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageElement.
 * @interface xyz.swapee.wc.ISwapeeLoginPageElementFields
 */
xyz.swapee.wc.ISwapeeLoginPageElementFields = class { }
/**
 * The element-specific inputs to the _ISwapeeLoginPage_ component.
 */
xyz.swapee.wc.ISwapeeLoginPageElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoginPageElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.ISwapeeLoginPageElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement} */
xyz.swapee.wc.RecordISwapeeLoginPageElement

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement} xyz.swapee.wc.BoundISwapeeLoginPageElement */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageElement} xyz.swapee.wc.BoundSwapeeLoginPageElement */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPagePort.Inputs&xyz.swapee.wc.ISwapeeLoginPageDisplay.Queries&xyz.swapee.wc.ISwapeeLoginPageController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs} xyz.swapee.wc.ISwapeeLoginPageElement.Inputs The element-specific inputs to the _ISwapeeLoginPage_ component. */

/**
 * Contains getters to cast the _ISwapeeLoginPageElement_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageElementCaster
 */
xyz.swapee.wc.ISwapeeLoginPageElementCaster = class { }
/**
 * Cast the _ISwapeeLoginPageElement_ instance into the _BoundISwapeeLoginPageElement_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageElement}
 */
xyz.swapee.wc.ISwapeeLoginPageElementCaster.prototype.asISwapeeLoginPageElement
/**
 * Access the _SwapeeLoginPageElement_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageElement}
 */
xyz.swapee.wc.ISwapeeLoginPageElementCaster.prototype.superSwapeeLoginPageElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeLoginPageMemory, props: !xyz.swapee.wc.ISwapeeLoginPageElement.Inputs) => Object<string, *>} xyz.swapee.wc.ISwapeeLoginPageElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement.__solder<!xyz.swapee.wc.ISwapeeLoginPageElement>} xyz.swapee.wc.ISwapeeLoginPageElement._solder */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.SwapeeLoginPageMemory} model The model.
 * - `host` _string_ The Swapee.me host. Default empty string.
 * - `signIn` _boolean_ The signin action. Default `false`.
 * - `username` _string_ Default empty string.
 * - `password` _string_ Default empty string.
 * - `token` _string_ The token is assigned upon login. Default empty string.
 * - `user` _&#42;_ Metadata about the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageElement.Inputs} props The element props.
 * - `[host="https://swapee.me/api"]` _&#42;?_ The Swapee.me host. ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Host* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Host* Default `https://swapee.me/api`.
 * - `[signIn=null]` _&#42;?_ The signin action. ⤴ *ISwapeeLoginPageOuterCore.WeakModel.SignIn* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.SignIn* Default `null`.
 * - `[username=null]` _&#42;?_ ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Username* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Username* Default `null`.
 * - `[password=null]` _&#42;?_ ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Password* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Password* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeLoginPageDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeLoginPageElementPort.Inputs.NoSolder* Default `false`.
 * - `[errorLaOpts]` _!Object?_ The options to pass to the _ErrorLa_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts* Default `{}`.
 * - `[usernameInOpts]` _!Object?_ The options to pass to the _UsernameIn_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.UsernameInOpts* Default `{}`.
 * - `[passwordInOpts]` _!Object?_ The options to pass to the _PasswordIn_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.PasswordInOpts* Default `{}`.
 * - `[loginBuOpts]` _!Object?_ The options to pass to the _LoginBu_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.LoginBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ISwapeeLoginPageElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeLoginPageMemory, instance?: !xyz.swapee.wc.ISwapeeLoginPageScreen&xyz.swapee.wc.ISwapeeLoginPageController) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeLoginPageElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement.__render<!xyz.swapee.wc.ISwapeeLoginPageElement>} xyz.swapee.wc.ISwapeeLoginPageElement._render */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.SwapeeLoginPageMemory} [model] The model for the view.
 * - `host` _string_ The Swapee.me host. Default empty string.
 * - `signIn` _boolean_ The signin action. Default `false`.
 * - `username` _string_ Default empty string.
 * - `password` _string_ Default empty string.
 * - `token` _string_ The token is assigned upon login. Default empty string.
 * - `user` _&#42;_ Metadata about the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageScreen&xyz.swapee.wc.ISwapeeLoginPageController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeLoginPageElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.ISwapeeLoginPageElement.build.Cores, instances: !xyz.swapee.wc.ISwapeeLoginPageElement.build.Instances) => ?} xyz.swapee.wc.ISwapeeLoginPageElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement.__build<!xyz.swapee.wc.ISwapeeLoginPageElement>} xyz.swapee.wc.ISwapeeLoginPageElement._build */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageElement.build.Cores} cores The models of components on the land.
 * - `SwapeeMe` _!xyz.swapee.wc.ISwapeeMeCore.Model_
 * @param {!xyz.swapee.wc.ISwapeeLoginPageElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `SwapeeMe` _!xyz.swapee.wc.ISwapeeMe_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeLoginPageElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.ISwapeeMeCore.Model} SwapeeMe
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.ISwapeeMe} SwapeeMe
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ISwapeeMeCore.Model, instance: !xyz.swapee.wc.ISwapeeMe) => ?} xyz.swapee.wc.ISwapeeLoginPageElement.__buildSwapeeMe
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement.__buildSwapeeMe<!xyz.swapee.wc.ISwapeeLoginPageElement>} xyz.swapee.wc.ISwapeeLoginPageElement._buildSwapeeMe */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement.buildSwapeeMe} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ISwapeeMe_ component.
 * @param {!xyz.swapee.wc.ISwapeeMeCore.Model} model
 * - `[token=""]` _string?_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Token* Default empty string.
 * - `[userid=""]` _string?_ The id of the user. ⤴ *ISwapeeMeOuterCore.Model.Userid* Default empty string.
 * - `[username=""]` _string?_ The username obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Username* Default empty string.
 * - `[userpic=""]` _string?_ The picture of the user. ⤴ *ISwapeeMeCore.Model.Userpic* Default empty string.
 * @param {!xyz.swapee.wc.ISwapeeMe} instance
 * @return {?}
 */
xyz.swapee.wc.ISwapeeLoginPageElement.buildSwapeeMe = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeLoginPageMemory, ports: !xyz.swapee.wc.ISwapeeLoginPageElement.short.Ports, cores: !xyz.swapee.wc.ISwapeeLoginPageElement.short.Cores) => ?} xyz.swapee.wc.ISwapeeLoginPageElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement.__short<!xyz.swapee.wc.ISwapeeLoginPageElement>} xyz.swapee.wc.ISwapeeLoginPageElement._short */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.SwapeeLoginPageMemory} model The model from which to feed properties to peer's ports.
 * - `host` _string_ The Swapee.me host. Default empty string.
 * - `signIn` _boolean_ The signin action. Default `false`.
 * - `username` _string_ Default empty string.
 * - `password` _string_ Default empty string.
 * - `token` _string_ The token is assigned upon login. Default empty string.
 * - `user` _&#42;_ Metadata about the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageElement.short.Ports} ports The ports of the peers.
 * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMePortInterface_
 * @param {!xyz.swapee.wc.ISwapeeLoginPageElement.short.Cores} cores The cores of the peers.
 * - `SwapeeMe` _xyz.swapee.wc.ISwapeeMeCore.Model_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeLoginPageElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMePortInterface} SwapeeMe
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.ISwapeeMeCore.Model} SwapeeMe
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeLoginPageMemory, inputs: !xyz.swapee.wc.ISwapeeLoginPageElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeLoginPageElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement.__server<!xyz.swapee.wc.ISwapeeLoginPageElement>} xyz.swapee.wc.ISwapeeLoginPageElement._server */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.SwapeeLoginPageMemory} memory The memory registers.
 * - `host` _string_ The Swapee.me host. Default empty string.
 * - `signIn` _boolean_ The signin action. Default `false`.
 * - `username` _string_ Default empty string.
 * - `password` _string_ Default empty string.
 * - `token` _string_ The token is assigned upon login. Default empty string.
 * - `user` _&#42;_ Metadata about the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageElement.Inputs} inputs The inputs to the port.
 * - `[host="https://swapee.me/api"]` _&#42;?_ The Swapee.me host. ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Host* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Host* Default `https://swapee.me/api`.
 * - `[signIn=null]` _&#42;?_ The signin action. ⤴ *ISwapeeLoginPageOuterCore.WeakModel.SignIn* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.SignIn* Default `null`.
 * - `[username=null]` _&#42;?_ ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Username* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Username* Default `null`.
 * - `[password=null]` _&#42;?_ ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Password* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Password* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeLoginPageDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeLoginPageElementPort.Inputs.NoSolder* Default `false`.
 * - `[errorLaOpts]` _!Object?_ The options to pass to the _ErrorLa_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts* Default `{}`.
 * - `[usernameInOpts]` _!Object?_ The options to pass to the _UsernameIn_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.UsernameInOpts* Default `{}`.
 * - `[passwordInOpts]` _!Object?_ The options to pass to the _PasswordIn_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.PasswordInOpts* Default `{}`.
 * - `[loginBuOpts]` _!Object?_ The options to pass to the _LoginBu_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.LoginBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeLoginPageElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeLoginPageMemory, port?: !xyz.swapee.wc.ISwapeeLoginPageElement.Inputs) => ?} xyz.swapee.wc.ISwapeeLoginPageElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElement.__inducer<!xyz.swapee.wc.ISwapeeLoginPageElement>} xyz.swapee.wc.ISwapeeLoginPageElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.SwapeeLoginPageMemory} [model] The model of the component into which to induce the state.
 * - `host` _string_ The Swapee.me host. Default empty string.
 * - `signIn` _boolean_ The signin action. Default `false`.
 * - `username` _string_ Default empty string.
 * - `password` _string_ Default empty string.
 * - `token` _string_ The token is assigned upon login. Default empty string.
 * - `user` _&#42;_ Metadata about the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[host="https://swapee.me/api"]` _&#42;?_ The Swapee.me host. ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Host* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Host* Default `https://swapee.me/api`.
 * - `[signIn=null]` _&#42;?_ The signin action. ⤴ *ISwapeeLoginPageOuterCore.WeakModel.SignIn* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.SignIn* Default `null`.
 * - `[username=null]` _&#42;?_ ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Username* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Username* Default `null`.
 * - `[password=null]` _&#42;?_ ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Password* ⤴ *ISwapeeLoginPageOuterCore.WeakModel.Password* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeLoginPageDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeLoginPageElementPort.Inputs.NoSolder* Default `false`.
 * - `[errorLaOpts]` _!Object?_ The options to pass to the _ErrorLa_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts* Default `{}`.
 * - `[usernameInOpts]` _!Object?_ The options to pass to the _UsernameIn_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.UsernameInOpts* Default `{}`.
 * - `[passwordInOpts]` _!Object?_ The options to pass to the _PasswordIn_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.PasswordInOpts* Default `{}`.
 * - `[loginBuOpts]` _!Object?_ The options to pass to the _LoginBu_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.LoginBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeLoginPageElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/140-ISwapeeLoginPageElementPort.xml}  36c5f93ec69b263897013bfa96141a26 */
/**
 * The options to pass to the _Error401La_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.Error401LaOpts.error401LaOpts

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.Error401LaOpts The options to pass to the _Error401La_ vdu (optional overlay).
 * @prop {!Object} [error401LaOpts] The options to pass to the _Error401La_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.Error401LaOpts_Safe The options to pass to the _Error401La_ vdu (required overlay).
 * @prop {!Object} error401LaOpts The options to pass to the _Error401La_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.Error401LaOpts The options to pass to the _Error401La_ vdu (optional overlay).
 * @prop {*} [error401LaOpts=null] The options to pass to the _Error401La_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.Error401LaOpts_Safe The options to pass to the _Error401La_ vdu (required overlay).
 * @prop {*} error401LaOpts The options to pass to the _Error401La_ vdu.
 */

/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeLoginPageElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageElementPort)} xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageElementPort} xyz.swapee.wc.SwapeeLoginPageElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageElementPort
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.constructor&xyz.swapee.wc.SwapeeLoginPageElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageElementPort|typeof xyz.swapee.wc.SwapeeLoginPageElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageElementPort|typeof xyz.swapee.wc.SwapeeLoginPageElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageElementPort|typeof xyz.swapee.wc.SwapeeLoginPageElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElementPort}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageElementPort.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageElementPort} xyz.swapee.wc.SwapeeLoginPageElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs>)} xyz.swapee.wc.ISwapeeLoginPageElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ISwapeeLoginPageElementPort
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageElementPort.Initialese>)} xyz.swapee.wc.SwapeeLoginPageElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort} xyz.swapee.wc.ISwapeeLoginPageElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeLoginPageElementPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageElementPort
 * @implements {xyz.swapee.wc.ISwapeeLoginPageElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageElementPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageElementPort = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageElementPort.constructor&xyz.swapee.wc.ISwapeeLoginPageElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageElementPort}
 */
xyz.swapee.wc.SwapeeLoginPageElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageElementPort.
 * @interface xyz.swapee.wc.ISwapeeLoginPageElementPortFields
 */
xyz.swapee.wc.ISwapeeLoginPageElementPortFields = class { }
/**
 * The inputs to the _ISwapeeLoginPageElement_'s controller via its element port.
 */
xyz.swapee.wc.ISwapeeLoginPageElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeLoginPageElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElementPort} */
xyz.swapee.wc.RecordISwapeeLoginPageElementPort

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageElementPort} xyz.swapee.wc.BoundISwapeeLoginPageElementPort */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageElementPort} xyz.swapee.wc.BoundSwapeeLoginPageElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _ErrorLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts.errorLaOpts

/**
 * The options to pass to the _CredentialsErrorLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts.credentialsErrorLaOpts

/**
 * The options to pass to the _UsernameIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts.usernameInOpts

/**
 * The options to pass to the _PasswordIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts.passwordInOpts

/**
 * The options to pass to the _LoginBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts.loginBuOpts

/**
 * The options to pass to the _SwapeeMe_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts.swapeeMeOpts

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts)} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts.typeof */
/**
 * The inputs to the _ISwapeeLoginPageElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.constructor&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.NoSolder&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.ErrorLaOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.CredentialsErrorLaOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.UsernameInOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.PasswordInOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.LoginBuOpts&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.SwapeeMeOpts)} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.ErrorLaOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.ErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.CredentialsErrorLaOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.CredentialsErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.UsernameInOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.UsernameInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.PasswordInOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.PasswordInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.LoginBuOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.LoginBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.SwapeeMeOpts} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.SwapeeMeOpts.typeof */
/**
 * The inputs to the _ISwapeeLoginPageElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.ErrorLaOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.CredentialsErrorLaOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.UsernameInOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.PasswordInOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.LoginBuOpts.typeof&xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.SwapeeMeOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs

/**
 * Contains getters to cast the _ISwapeeLoginPageElementPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageElementPortCaster
 */
xyz.swapee.wc.ISwapeeLoginPageElementPortCaster = class { }
/**
 * Cast the _ISwapeeLoginPageElementPort_ instance into the _BoundISwapeeLoginPageElementPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageElementPort}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPortCaster.prototype.asISwapeeLoginPageElementPort
/**
 * Access the _SwapeeLoginPageElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageElementPort}
 */
xyz.swapee.wc.ISwapeeLoginPageElementPortCaster.prototype.superSwapeeLoginPageElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts The options to pass to the _ErrorLa_ vdu (optional overlay).
 * @prop {!Object} [errorLaOpts] The options to pass to the _ErrorLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.ErrorLaOpts_Safe The options to pass to the _ErrorLa_ vdu (required overlay).
 * @prop {!Object} errorLaOpts The options to pass to the _ErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts The options to pass to the _CredentialsErrorLa_ vdu (optional overlay).
 * @prop {!Object} [credentialsErrorLaOpts] The options to pass to the _CredentialsErrorLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.CredentialsErrorLaOpts_Safe The options to pass to the _CredentialsErrorLa_ vdu (required overlay).
 * @prop {!Object} credentialsErrorLaOpts The options to pass to the _CredentialsErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts The options to pass to the _UsernameIn_ vdu (optional overlay).
 * @prop {!Object} [usernameInOpts] The options to pass to the _UsernameIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.UsernameInOpts_Safe The options to pass to the _UsernameIn_ vdu (required overlay).
 * @prop {!Object} usernameInOpts The options to pass to the _UsernameIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts The options to pass to the _PasswordIn_ vdu (optional overlay).
 * @prop {!Object} [passwordInOpts] The options to pass to the _PasswordIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.PasswordInOpts_Safe The options to pass to the _PasswordIn_ vdu (required overlay).
 * @prop {!Object} passwordInOpts The options to pass to the _PasswordIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts The options to pass to the _LoginBu_ vdu (optional overlay).
 * @prop {!Object} [loginBuOpts] The options to pass to the _LoginBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.LoginBuOpts_Safe The options to pass to the _LoginBu_ vdu (required overlay).
 * @prop {!Object} loginBuOpts The options to pass to the _LoginBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts The options to pass to the _SwapeeMe_ vdu (optional overlay).
 * @prop {!Object} [swapeeMeOpts] The options to pass to the _SwapeeMe_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs.SwapeeMeOpts_Safe The options to pass to the _SwapeeMe_ vdu (required overlay).
 * @prop {!Object} swapeeMeOpts The options to pass to the _SwapeeMe_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.ErrorLaOpts The options to pass to the _ErrorLa_ vdu (optional overlay).
 * @prop {*} [errorLaOpts=null] The options to pass to the _ErrorLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.ErrorLaOpts_Safe The options to pass to the _ErrorLa_ vdu (required overlay).
 * @prop {*} errorLaOpts The options to pass to the _ErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.CredentialsErrorLaOpts The options to pass to the _CredentialsErrorLa_ vdu (optional overlay).
 * @prop {*} [credentialsErrorLaOpts=null] The options to pass to the _CredentialsErrorLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.CredentialsErrorLaOpts_Safe The options to pass to the _CredentialsErrorLa_ vdu (required overlay).
 * @prop {*} credentialsErrorLaOpts The options to pass to the _CredentialsErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.UsernameInOpts The options to pass to the _UsernameIn_ vdu (optional overlay).
 * @prop {*} [usernameInOpts=null] The options to pass to the _UsernameIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.UsernameInOpts_Safe The options to pass to the _UsernameIn_ vdu (required overlay).
 * @prop {*} usernameInOpts The options to pass to the _UsernameIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.PasswordInOpts The options to pass to the _PasswordIn_ vdu (optional overlay).
 * @prop {*} [passwordInOpts=null] The options to pass to the _PasswordIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.PasswordInOpts_Safe The options to pass to the _PasswordIn_ vdu (required overlay).
 * @prop {*} passwordInOpts The options to pass to the _PasswordIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.LoginBuOpts The options to pass to the _LoginBu_ vdu (optional overlay).
 * @prop {*} [loginBuOpts=null] The options to pass to the _LoginBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.LoginBuOpts_Safe The options to pass to the _LoginBu_ vdu (required overlay).
 * @prop {*} loginBuOpts The options to pass to the _LoginBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.SwapeeMeOpts The options to pass to the _SwapeeMe_ vdu (optional overlay).
 * @prop {*} [swapeeMeOpts=null] The options to pass to the _SwapeeMe_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageElementPort.WeakInputs.SwapeeMeOpts_Safe The options to pass to the _SwapeeMe_ vdu (required overlay).
 * @prop {*} swapeeMeOpts The options to pass to the _SwapeeMe_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/170-ISwapeeLoginPageDesigner.xml}  83de0638cc34b65518ff59bc9d86087c */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ISwapeeLoginPageDesigner
 */
xyz.swapee.wc.ISwapeeLoginPageDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.SwapeeLoginPageClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeLoginPage />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeLoginPageClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeLoginPage />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeLoginPageClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeLoginPageDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMeController_
   * - `SwapeeLoginPage` _typeof ISwapeeLoginPageController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeLoginPageDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMeController_
   * - `SwapeeLoginPage` _typeof ISwapeeLoginPageController_
   * - `This` _typeof ISwapeeLoginPageController_
   * @param {!xyz.swapee.wc.ISwapeeLoginPageDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `SwapeeMe` _!xyz.swapee.wc.SwapeeMeMemory_
   * - `SwapeeLoginPage` _!SwapeeLoginPageMemory_
   * - `This` _!SwapeeLoginPageMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeLoginPageClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeLoginPageClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ISwapeeLoginPageDesigner.prototype.constructor = xyz.swapee.wc.ISwapeeLoginPageDesigner

/**
 * A concrete class of _ISwapeeLoginPageDesigner_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageDesigner
 * @implements {xyz.swapee.wc.ISwapeeLoginPageDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.SwapeeLoginPageDesigner = class extends xyz.swapee.wc.ISwapeeLoginPageDesigner { }
xyz.swapee.wc.SwapeeLoginPageDesigner.prototype.constructor = xyz.swapee.wc.SwapeeLoginPageDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeLoginPageController} SwapeeLoginPage
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeLoginPageController} SwapeeLoginPage
 * @prop {typeof xyz.swapee.wc.ISwapeeLoginPageController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe
 * @prop {!xyz.swapee.wc.SwapeeLoginPageMemory} SwapeeLoginPage
 * @prop {!xyz.swapee.wc.SwapeeLoginPageMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/200-SwapeeLoginPageLand.xml}  66b56b168c2ea4cd22af6a47582fa7b3 */
/**
 * The surrounding of the _ISwapeeLoginPage_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.SwapeeLoginPageLand
 */
xyz.swapee.wc.SwapeeLoginPageLand = class { }
/**
 *
 */
xyz.swapee.wc.SwapeeLoginPageLand.prototype.SwapeeMe = /** @type {xyz.swapee.wc.ISwapeeMe} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/40-ISwapeeLoginPageDisplay.xml}  0c81ef7d7e82e777ae17cac8a97a3999 */
/**
 * @typedef {Object} $xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese
 * @prop {HTMLSpanElement} [ErrorLa]
 * @prop {HTMLSpanElement} [CredentialsErrorLa]
 * @prop {HTMLInputElement} [UsernameIn]
 * @prop {HTMLInputElement} [PasswordIn]
 * @prop {HTMLButtonElement} [LoginBu]
 * @prop {HTMLElement} [SwapeeMe] The via for the _SwapeeMe_ peer.
 */
/** @typedef {$xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ISwapeeLoginPageDisplay.Settings>} xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageDisplay)} xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageDisplay} xyz.swapee.wc.SwapeeLoginPageDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageDisplay
 */
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.constructor&xyz.swapee.wc.SwapeeLoginPageDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.SwapeeLoginPageDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.SwapeeLoginPageDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.SwapeeLoginPageDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageDisplay} xyz.swapee.wc.SwapeeLoginPageDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.SwapeeLoginPageMemory, !HTMLDivElement, !xyz.swapee.wc.ISwapeeLoginPageDisplay.Settings, xyz.swapee.wc.ISwapeeLoginPageDisplay.Queries, null>)} xyz.swapee.wc.ISwapeeLoginPageDisplay.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ISwapeeLoginPage_.
 * @interface xyz.swapee.wc.ISwapeeLoginPageDisplay
 */
xyz.swapee.wc.ISwapeeLoginPageDisplay = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageDisplay.paint} */
xyz.swapee.wc.ISwapeeLoginPageDisplay.prototype.paint = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageDisplay.paintCallback} */
xyz.swapee.wc.ISwapeeLoginPageDisplay.prototype.paintCallback = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese>)} xyz.swapee.wc.SwapeeLoginPageDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageDisplay} xyz.swapee.wc.ISwapeeLoginPageDisplay.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeLoginPageDisplay_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageDisplay
 * @implements {xyz.swapee.wc.ISwapeeLoginPageDisplay} Display for presenting information from the _ISwapeeLoginPage_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageDisplay = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageDisplay.constructor&xyz.swapee.wc.ISwapeeLoginPageDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.SwapeeLoginPageDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageDisplay.
 * @interface xyz.swapee.wc.ISwapeeLoginPageDisplayFields
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ISwapeeLoginPageDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ISwapeeLoginPageDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.ErrorLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.CredentialsErrorLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.UsernameIn = /** @type {HTMLInputElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.PasswordIn = /** @type {HTMLInputElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.LoginBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * The via for the _SwapeeMe_ peer. Default `null`.
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayFields.prototype.SwapeeMe = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageDisplay} */
xyz.swapee.wc.RecordISwapeeLoginPageDisplay

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageDisplay} xyz.swapee.wc.BoundISwapeeLoginPageDisplay */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageDisplay} xyz.swapee.wc.BoundSwapeeLoginPageDisplay */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageDisplay.Queries} xyz.swapee.wc.ISwapeeLoginPageDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeLoginPageDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [swapeeMeSel=""] The query to discover the _SwapeeMe_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _ISwapeeLoginPageDisplay_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageDisplayCaster
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayCaster = class { }
/**
 * Cast the _ISwapeeLoginPageDisplay_ instance into the _BoundISwapeeLoginPageDisplay_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageDisplay}
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayCaster.prototype.asISwapeeLoginPageDisplay
/**
 * Cast the _ISwapeeLoginPageDisplay_ instance into the _BoundISwapeeLoginPageScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageScreen}
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayCaster.prototype.asISwapeeLoginPageScreen
/**
 * Access the _SwapeeLoginPageDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageDisplay}
 */
xyz.swapee.wc.ISwapeeLoginPageDisplayCaster.prototype.superSwapeeLoginPageDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeLoginPageMemory, land: null) => void} xyz.swapee.wc.ISwapeeLoginPageDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageDisplay.__paint<!xyz.swapee.wc.ISwapeeLoginPageDisplay>} xyz.swapee.wc.ISwapeeLoginPageDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeLoginPageMemory} memory The display data.
 * - `host` _string_ The Swapee.me host. Default empty string.
 * - `signIn` _boolean_ The signin action. Default `false`.
 * - `signedIn` _boolean_ Private pulse issued by the adapter after successful signin. Default `false`.
 * - `username` _string_ Default empty string.
 * - `password` _string_ Default empty string.
 * - `credentialsError` _boolean_ Whether the username/password pair is incorrect. Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageDisplay.paint = function(memory, land) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ISwapeeLoginPageDisplay.paintCallback.Memory, land: { SwapeeMe: { token: xyz.swapee.wc.ISwapeeMeCore.Model.Token_Safe } }) => ?} xyz.swapee.wc.ISwapeeLoginPageDisplay.__paintCallback
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageDisplay.__paintCallback<!xyz.swapee.wc.ISwapeeLoginPageDisplay>} xyz.swapee.wc.ISwapeeLoginPageDisplay._paintCallback */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageDisplay.paintCallback} */
/**
 * Invokes the callback after signed in.
 * @param {!xyz.swapee.wc.ISwapeeLoginPageDisplay.paintCallback.Memory} memory The memory.
 * @param {{ SwapeeMe: { token: xyz.swapee.wc.ISwapeeMeCore.Model.Token_Safe } }} land The land for the painter.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeLoginPageDisplay.paintCallback = function(memory, land) {}

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeLoginPageDisplay.paintCallback.Memory The memory. */

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/40-ISwapeeLoginPageDisplayBack.xml}  d6db1ecdab6370a82f640e8022fbbffe */
/**
 * @typedef {Object} $xyz.swapee.wc.back.ISwapeeLoginPageDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [ErrorLa]
 * @prop {!com.webcircuits.IHtmlTwin} [CredentialsErrorLa]
 * @prop {!com.webcircuits.IHtmlTwin} [UsernameIn]
 * @prop {!com.webcircuits.IHtmlTwin} [PasswordIn]
 * @prop {!com.webcircuits.IHtmlTwin} [LoginBu]
 * @prop {!com.webcircuits.IHtmlTwin} [SwapeeMe] The via for the _SwapeeMe_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.ISwapeeLoginPageDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.SwapeeLoginPageClasses>} xyz.swapee.wc.back.ISwapeeLoginPageDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoginPageDisplay)} xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay} xyz.swapee.wc.back.SwapeeLoginPageDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoginPageDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.constructor&xyz.swapee.wc.back.SwapeeLoginPageDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoginPageDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.SwapeeLoginPageClasses, !xyz.swapee.wc.SwapeeLoginPageLand>)} xyz.swapee.wc.back.ISwapeeLoginPageDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageDisplay
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplay = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoginPageDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ISwapeeLoginPageDisplay.paint} */
xyz.swapee.wc.back.ISwapeeLoginPageDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoginPageDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageDisplay.Initialese>)} xyz.swapee.wc.back.SwapeeLoginPageDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoginPageDisplay} xyz.swapee.wc.back.ISwapeeLoginPageDisplay.typeof */
/**
 * A concrete class of _ISwapeeLoginPageDisplay_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoginPageDisplay
 * @implements {xyz.swapee.wc.back.ISwapeeLoginPageDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoginPageDisplay = class extends /** @type {xyz.swapee.wc.back.SwapeeLoginPageDisplay.constructor&xyz.swapee.wc.back.ISwapeeLoginPageDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.SwapeeLoginPageDisplay.prototype.constructor = xyz.swapee.wc.back.SwapeeLoginPageDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay}
 */
xyz.swapee.wc.back.SwapeeLoginPageDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageDisplay.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields.prototype.ErrorLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields.prototype.CredentialsErrorLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields.prototype.UsernameIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields.prototype.PasswordIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields.prototype.LoginBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _SwapeeMe_ peer.
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayFields.prototype.SwapeeMe = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageDisplay} */
xyz.swapee.wc.back.RecordISwapeeLoginPageDisplay

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageDisplay} xyz.swapee.wc.back.BoundISwapeeLoginPageDisplay */

/** @typedef {xyz.swapee.wc.back.SwapeeLoginPageDisplay} xyz.swapee.wc.back.BoundSwapeeLoginPageDisplay */

/**
 * Contains getters to cast the _ISwapeeLoginPageDisplay_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageDisplayCaster
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayCaster = class { }
/**
 * Cast the _ISwapeeLoginPageDisplay_ instance into the _BoundISwapeeLoginPageDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoginPageDisplay}
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayCaster.prototype.asISwapeeLoginPageDisplay
/**
 * Access the _SwapeeLoginPageDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoginPageDisplay}
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplayCaster.prototype.superSwapeeLoginPageDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.SwapeeLoginPageMemory, land?: !xyz.swapee.wc.SwapeeLoginPageLand) => void} xyz.swapee.wc.back.ISwapeeLoginPageDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageDisplay.__paint<!xyz.swapee.wc.back.ISwapeeLoginPageDisplay>} xyz.swapee.wc.back.ISwapeeLoginPageDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoginPageDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeLoginPageMemory} [memory] The display data.
 * - `host` _string_ The Swapee.me host. Default empty string.
 * - `signIn` _boolean_ The signin action. Default `false`.
 * - `username` _string_ Default empty string.
 * - `password` _string_ Default empty string.
 * - `credentialsError` _boolean_ Whether the username/password pair is incorrect. Default `false`.
 * @param {!xyz.swapee.wc.SwapeeLoginPageLand} [land] The land data.
 * - `SwapeeMe` _xyz.swapee.wc.ISwapeeMe_
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeLoginPageDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeLoginPageDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/41-SwapeeLoginPageClasses.xml}  c06fab411077f868f718f7b14464771d */
/**
 * The classes of the _ISwapeeLoginPageDisplay_.
 * @record xyz.swapee.wc.SwapeeLoginPageClasses
 */
xyz.swapee.wc.SwapeeLoginPageClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.SwapeeLoginPageClasses.prototype.Label = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.SwapeeLoginPageClasses.prototype.props = /** @type {xyz.swapee.wc.SwapeeLoginPageClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/50-ISwapeeLoginPageController.xml}  bce0ea7c0092e00a14c2b4b1ccaca58e */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs, !xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel>} xyz.swapee.wc.ISwapeeLoginPageController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageController)} xyz.swapee.wc.AbstractSwapeeLoginPageController.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageController} xyz.swapee.wc.SwapeeLoginPageController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageController` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageController
 */
xyz.swapee.wc.AbstractSwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageController.constructor&xyz.swapee.wc.SwapeeLoginPageController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageController.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageController.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageController}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageController}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageController}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageController}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageController.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageController} xyz.swapee.wc.SwapeeLoginPageControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs, !xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ISwapeeLoginPageOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs, !xyz.swapee.wc.ISwapeeLoginPageController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs, !xyz.swapee.wc.SwapeeLoginPageMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs>)} xyz.swapee.wc.ISwapeeLoginPageController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ISwapeeLoginPageController
 */
xyz.swapee.wc.ISwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageController.resetPort} */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageController.setUsername} */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.setUsername = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageController.unsetUsername} */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.unsetUsername = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageController.setPassword} */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.setPassword = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageController.unsetPassword} */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.unsetPassword = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageController.pulseSignIn} */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.pulseSignIn = function() {}
/** @type {xyz.swapee.wc.ISwapeeLoginPageController.pulseSignedIn} */
xyz.swapee.wc.ISwapeeLoginPageController.prototype.pulseSignedIn = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageController&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageController.Initialese>)} xyz.swapee.wc.SwapeeLoginPageController.constructor */
/**
 * A concrete class of _ISwapeeLoginPageController_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageController
 * @implements {xyz.swapee.wc.ISwapeeLoginPageController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageController.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageController.constructor&xyz.swapee.wc.ISwapeeLoginPageController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageController}
 */
xyz.swapee.wc.SwapeeLoginPageController.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageController.
 * @interface xyz.swapee.wc.ISwapeeLoginPageControllerFields
 */
xyz.swapee.wc.ISwapeeLoginPageControllerFields = class { }
/**
 * The inputs to the _ISwapeeLoginPage_'s controller.
 */
xyz.swapee.wc.ISwapeeLoginPageControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeLoginPageController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeLoginPageControllerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeLoginPageController} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController} */
xyz.swapee.wc.RecordISwapeeLoginPageController

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController} xyz.swapee.wc.BoundISwapeeLoginPageController */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageController} xyz.swapee.wc.BoundSwapeeLoginPageController */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPagePort.Inputs} xyz.swapee.wc.ISwapeeLoginPageController.Inputs The inputs to the _ISwapeeLoginPage_'s controller. */

/** @typedef {xyz.swapee.wc.ISwapeeLoginPagePort.WeakInputs} xyz.swapee.wc.ISwapeeLoginPageController.WeakInputs The inputs to the _ISwapeeLoginPage_'s controller. */

/**
 * Contains getters to cast the _ISwapeeLoginPageController_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageControllerCaster
 */
xyz.swapee.wc.ISwapeeLoginPageControllerCaster = class { }
/**
 * Cast the _ISwapeeLoginPageController_ instance into the _BoundISwapeeLoginPageController_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageController}
 */
xyz.swapee.wc.ISwapeeLoginPageControllerCaster.prototype.asISwapeeLoginPageController
/**
 * Cast the _ISwapeeLoginPageController_ instance into the _BoundISwapeeLoginPageProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageProcessor}
 */
xyz.swapee.wc.ISwapeeLoginPageControllerCaster.prototype.asISwapeeLoginPageProcessor
/**
 * Access the _SwapeeLoginPageController_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageController}
 */
xyz.swapee.wc.ISwapeeLoginPageControllerCaster.prototype.superSwapeeLoginPageController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.__resetPort<!xyz.swapee.wc.ISwapeeLoginPageController>} xyz.swapee.wc.ISwapeeLoginPageController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageController.resetPort = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ISwapeeLoginPageController.__setUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.__setUsername<!xyz.swapee.wc.ISwapeeLoginPageController>} xyz.swapee.wc.ISwapeeLoginPageController._setUsername */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController.setUsername} */
/**
 * Sets the `username` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageController.setUsername = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageController.__unsetUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.__unsetUsername<!xyz.swapee.wc.ISwapeeLoginPageController>} xyz.swapee.wc.ISwapeeLoginPageController._unsetUsername */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController.unsetUsername} */
/**
 * Clears the `username` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageController.unsetUsername = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ISwapeeLoginPageController.__setPassword
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.__setPassword<!xyz.swapee.wc.ISwapeeLoginPageController>} xyz.swapee.wc.ISwapeeLoginPageController._setPassword */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController.setPassword} */
/**
 * Sets the `password` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageController.setPassword = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageController.__unsetPassword
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.__unsetPassword<!xyz.swapee.wc.ISwapeeLoginPageController>} xyz.swapee.wc.ISwapeeLoginPageController._unsetPassword */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController.unsetPassword} */
/**
 * Clears the `password` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageController.unsetPassword = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageController.__pulseSignIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.__pulseSignIn<!xyz.swapee.wc.ISwapeeLoginPageController>} xyz.swapee.wc.ISwapeeLoginPageController._pulseSignIn */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController.pulseSignIn} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageController.pulseSignIn = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeLoginPageController.__pulseSignedIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeLoginPageController.__pulseSignedIn<!xyz.swapee.wc.ISwapeeLoginPageController>} xyz.swapee.wc.ISwapeeLoginPageController._pulseSignedIn */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageController.pulseSignedIn} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeLoginPageController.pulseSignedIn = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeLoginPageController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/51-ISwapeeLoginPageControllerFront.xml}  8bbc624aa1021a5d17159891eacc641a */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ISwapeeLoginPageController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeLoginPageController)} xyz.swapee.wc.front.AbstractSwapeeLoginPageController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeLoginPageController} xyz.swapee.wc.front.SwapeeLoginPageController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeLoginPageController` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeLoginPageController
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeLoginPageController.constructor&xyz.swapee.wc.front.SwapeeLoginPageController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeLoginPageController.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeLoginPageController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageController.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageController|typeof xyz.swapee.wc.front.SwapeeLoginPageController)|(!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT|typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageController|typeof xyz.swapee.wc.front.SwapeeLoginPageController)|(!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT|typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageController|typeof xyz.swapee.wc.front.SwapeeLoginPageController)|(!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT|typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageController}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeLoginPageController.Initialese[]) => xyz.swapee.wc.front.ISwapeeLoginPageController} xyz.swapee.wc.front.SwapeeLoginPageControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeLoginPageControllerCaster&xyz.swapee.wc.front.ISwapeeLoginPageControllerAT)} xyz.swapee.wc.front.ISwapeeLoginPageController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageControllerAT} xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ISwapeeLoginPageController
 */
xyz.swapee.wc.front.ISwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.front.ISwapeeLoginPageController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeLoginPageController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.ISwapeeLoginPageController.setUsername} */
xyz.swapee.wc.front.ISwapeeLoginPageController.prototype.setUsername = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeLoginPageController.unsetUsername} */
xyz.swapee.wc.front.ISwapeeLoginPageController.prototype.unsetUsername = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeLoginPageController.setPassword} */
xyz.swapee.wc.front.ISwapeeLoginPageController.prototype.setPassword = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeLoginPageController.unsetPassword} */
xyz.swapee.wc.front.ISwapeeLoginPageController.prototype.unsetPassword = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeLoginPageController.pulseSignIn} */
xyz.swapee.wc.front.ISwapeeLoginPageController.prototype.pulseSignIn = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeLoginPageController.pulseSignedIn} */
xyz.swapee.wc.front.ISwapeeLoginPageController.prototype.pulseSignedIn = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeLoginPageController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoginPageController.Initialese>)} xyz.swapee.wc.front.SwapeeLoginPageController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageController} xyz.swapee.wc.front.ISwapeeLoginPageController.typeof */
/**
 * A concrete class of _ISwapeeLoginPageController_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeLoginPageController
 * @implements {xyz.swapee.wc.front.ISwapeeLoginPageController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoginPageController.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.front.SwapeeLoginPageController.constructor&xyz.swapee.wc.front.ISwapeeLoginPageController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeLoginPageController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageController}
 */
xyz.swapee.wc.front.SwapeeLoginPageController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController} */
xyz.swapee.wc.front.RecordISwapeeLoginPageController

/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController} xyz.swapee.wc.front.BoundISwapeeLoginPageController */

/** @typedef {xyz.swapee.wc.front.SwapeeLoginPageController} xyz.swapee.wc.front.BoundSwapeeLoginPageController */

/**
 * Contains getters to cast the _ISwapeeLoginPageController_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeLoginPageControllerCaster
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerCaster = class { }
/**
 * Cast the _ISwapeeLoginPageController_ instance into the _BoundISwapeeLoginPageController_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeLoginPageController}
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerCaster.prototype.asISwapeeLoginPageController
/**
 * Access the _SwapeeLoginPageController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeLoginPageController}
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerCaster.prototype.superSwapeeLoginPageController

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ISwapeeLoginPageController.__setUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController.__setUsername<!xyz.swapee.wc.front.ISwapeeLoginPageController>} xyz.swapee.wc.front.ISwapeeLoginPageController._setUsername */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageController.setUsername} */
/**
 * Sets the `username` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeLoginPageController.setUsername = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeLoginPageController.__unsetUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController.__unsetUsername<!xyz.swapee.wc.front.ISwapeeLoginPageController>} xyz.swapee.wc.front.ISwapeeLoginPageController._unsetUsername */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageController.unsetUsername} */
/**
 * Clears the `username` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeLoginPageController.unsetUsername = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ISwapeeLoginPageController.__setPassword
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController.__setPassword<!xyz.swapee.wc.front.ISwapeeLoginPageController>} xyz.swapee.wc.front.ISwapeeLoginPageController._setPassword */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageController.setPassword} */
/**
 * Sets the `password` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeLoginPageController.setPassword = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeLoginPageController.__unsetPassword
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController.__unsetPassword<!xyz.swapee.wc.front.ISwapeeLoginPageController>} xyz.swapee.wc.front.ISwapeeLoginPageController._unsetPassword */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageController.unsetPassword} */
/**
 * Clears the `password` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeLoginPageController.unsetPassword = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeLoginPageController.__pulseSignIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController.__pulseSignIn<!xyz.swapee.wc.front.ISwapeeLoginPageController>} xyz.swapee.wc.front.ISwapeeLoginPageController._pulseSignIn */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageController.pulseSignIn} */
/** @return {void} */
xyz.swapee.wc.front.ISwapeeLoginPageController.pulseSignIn = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeLoginPageController.__pulseSignedIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageController.__pulseSignedIn<!xyz.swapee.wc.front.ISwapeeLoginPageController>} xyz.swapee.wc.front.ISwapeeLoginPageController._pulseSignedIn */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageController.pulseSignedIn} */
/** @return {void} */
xyz.swapee.wc.front.ISwapeeLoginPageController.pulseSignedIn = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.ISwapeeLoginPageController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/52-ISwapeeLoginPageControllerBack.xml}  8cbecfbd36df0a33a1879b845bdf250a */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs>&xyz.swapee.wc.ISwapeeLoginPageController.Initialese} xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoginPageController)} xyz.swapee.wc.back.AbstractSwapeeLoginPageController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoginPageController} xyz.swapee.wc.back.SwapeeLoginPageController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoginPageController` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoginPageController
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoginPageController.constructor&xyz.swapee.wc.back.SwapeeLoginPageController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoginPageController.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoginPageController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageController.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageController|typeof xyz.swapee.wc.back.SwapeeLoginPageController)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageController|typeof xyz.swapee.wc.back.SwapeeLoginPageController)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageController|typeof xyz.swapee.wc.back.SwapeeLoginPageController)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageController}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoginPageController} xyz.swapee.wc.back.SwapeeLoginPageControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoginPageControllerCaster&xyz.swapee.wc.ISwapeeLoginPageController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ISwapeeLoginPageController.Inputs>)} xyz.swapee.wc.back.ISwapeeLoginPageController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageController
 */
xyz.swapee.wc.back.ISwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoginPageController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeLoginPageController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoginPageController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoginPageController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese>)} xyz.swapee.wc.back.SwapeeLoginPageController.constructor */
/**
 * A concrete class of _ISwapeeLoginPageController_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoginPageController
 * @implements {xyz.swapee.wc.back.ISwapeeLoginPageController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoginPageController = class extends /** @type {xyz.swapee.wc.back.SwapeeLoginPageController.constructor&xyz.swapee.wc.back.ISwapeeLoginPageController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoginPageController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageController}
 */
xyz.swapee.wc.back.SwapeeLoginPageController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageController} */
xyz.swapee.wc.back.RecordISwapeeLoginPageController

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageController} xyz.swapee.wc.back.BoundISwapeeLoginPageController */

/** @typedef {xyz.swapee.wc.back.SwapeeLoginPageController} xyz.swapee.wc.back.BoundSwapeeLoginPageController */

/**
 * Contains getters to cast the _ISwapeeLoginPageController_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageControllerCaster
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerCaster = class { }
/**
 * Cast the _ISwapeeLoginPageController_ instance into the _BoundISwapeeLoginPageController_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoginPageController}
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerCaster.prototype.asISwapeeLoginPageController
/**
 * Access the _SwapeeLoginPageController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoginPageController}
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerCaster.prototype.superSwapeeLoginPageController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/53-ISwapeeLoginPageControllerAR.xml}  16caf1b43a67684b408c77c6e875ea78 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeLoginPageController.Initialese} xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoginPageControllerAR)} xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR} xyz.swapee.wc.back.SwapeeLoginPageControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoginPageControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.constructor&xyz.swapee.wc.back.SwapeeLoginPageControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR|typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR|typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR|typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoginPageController|typeof xyz.swapee.wc.SwapeeLoginPageController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoginPageControllerAR} xyz.swapee.wc.back.SwapeeLoginPageControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoginPageControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeLoginPageController)} xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeLoginPageControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageControllerAR
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerAR = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeLoginPageController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoginPageControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.Initialese>)} xyz.swapee.wc.back.SwapeeLoginPageControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoginPageControllerAR} xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.typeof */
/**
 * A concrete class of _ISwapeeLoginPageControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoginPageControllerAR
 * @implements {xyz.swapee.wc.back.ISwapeeLoginPageControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeLoginPageControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoginPageControllerAR = class extends /** @type {xyz.swapee.wc.back.SwapeeLoginPageControllerAR.constructor&xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoginPageControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageControllerAR}
 */
xyz.swapee.wc.back.SwapeeLoginPageControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageControllerAR} */
xyz.swapee.wc.back.RecordISwapeeLoginPageControllerAR

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageControllerAR} xyz.swapee.wc.back.BoundISwapeeLoginPageControllerAR */

/** @typedef {xyz.swapee.wc.back.SwapeeLoginPageControllerAR} xyz.swapee.wc.back.BoundSwapeeLoginPageControllerAR */

/**
 * Contains getters to cast the _ISwapeeLoginPageControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageControllerARCaster
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerARCaster = class { }
/**
 * Cast the _ISwapeeLoginPageControllerAR_ instance into the _BoundISwapeeLoginPageControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoginPageControllerAR}
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerARCaster.prototype.asISwapeeLoginPageControllerAR
/**
 * Access the _SwapeeLoginPageControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoginPageControllerAR}
 */
xyz.swapee.wc.back.ISwapeeLoginPageControllerARCaster.prototype.superSwapeeLoginPageControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/54-ISwapeeLoginPageControllerAT.xml}  e655123de4107c9d5a3855b21dda65be */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeLoginPageControllerAT)} xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT} xyz.swapee.wc.front.SwapeeLoginPageControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeLoginPageControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.constructor&xyz.swapee.wc.front.SwapeeLoginPageControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT|typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT|typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT|typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.Initialese[]) => xyz.swapee.wc.front.ISwapeeLoginPageControllerAT} xyz.swapee.wc.front.SwapeeLoginPageControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeLoginPageControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeLoginPageControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ISwapeeLoginPageControllerAT
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerAT = class extends /** @type {xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeLoginPageControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.Initialese>)} xyz.swapee.wc.front.SwapeeLoginPageControllerAT.constructor */
/**
 * A concrete class of _ISwapeeLoginPageControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeLoginPageControllerAT
 * @implements {xyz.swapee.wc.front.ISwapeeLoginPageControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeLoginPageControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeLoginPageControllerAT = class extends /** @type {xyz.swapee.wc.front.SwapeeLoginPageControllerAT.constructor&xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeLoginPageControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageControllerAT}
 */
xyz.swapee.wc.front.SwapeeLoginPageControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageControllerAT} */
xyz.swapee.wc.front.RecordISwapeeLoginPageControllerAT

/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageControllerAT} xyz.swapee.wc.front.BoundISwapeeLoginPageControllerAT */

/** @typedef {xyz.swapee.wc.front.SwapeeLoginPageControllerAT} xyz.swapee.wc.front.BoundSwapeeLoginPageControllerAT */

/**
 * Contains getters to cast the _ISwapeeLoginPageControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeLoginPageControllerATCaster
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerATCaster = class { }
/**
 * Cast the _ISwapeeLoginPageControllerAT_ instance into the _BoundISwapeeLoginPageControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeLoginPageControllerAT}
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerATCaster.prototype.asISwapeeLoginPageControllerAT
/**
 * Access the _SwapeeLoginPageControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeLoginPageControllerAT}
 */
xyz.swapee.wc.front.ISwapeeLoginPageControllerATCaster.prototype.superSwapeeLoginPageControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/70-ISwapeeLoginPageScreen.xml}  124d273344004a8d4ac6914aecc8a0d2 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.front.SwapeeLoginPageInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeLoginPageDisplay.Settings, !xyz.swapee.wc.ISwapeeLoginPageDisplay.Queries, null>&xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese} xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageScreen)} xyz.swapee.wc.AbstractSwapeeLoginPageScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageScreen} xyz.swapee.wc.SwapeeLoginPageScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageScreen` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageScreen
 */
xyz.swapee.wc.AbstractSwapeeLoginPageScreen = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageScreen.constructor&xyz.swapee.wc.SwapeeLoginPageScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageScreen.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageScreen.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.SwapeeLoginPageScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeLoginPageController|typeof xyz.swapee.wc.front.SwapeeLoginPageController)|(!xyz.swapee.wc.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.SwapeeLoginPageDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.SwapeeLoginPageScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeLoginPageController|typeof xyz.swapee.wc.front.SwapeeLoginPageController)|(!xyz.swapee.wc.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.SwapeeLoginPageDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.SwapeeLoginPageScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeLoginPageController|typeof xyz.swapee.wc.front.SwapeeLoginPageController)|(!xyz.swapee.wc.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.SwapeeLoginPageDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageScreen} xyz.swapee.wc.SwapeeLoginPageScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.SwapeeLoginPageMemory, !xyz.swapee.wc.front.SwapeeLoginPageInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeLoginPageDisplay.Settings, !xyz.swapee.wc.ISwapeeLoginPageDisplay.Queries, null, null>&xyz.swapee.wc.front.ISwapeeLoginPageController&xyz.swapee.wc.ISwapeeLoginPageDisplay)} xyz.swapee.wc.ISwapeeLoginPageScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ISwapeeLoginPageScreen
 */
xyz.swapee.wc.ISwapeeLoginPageScreen = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ISwapeeLoginPageController.typeof&xyz.swapee.wc.ISwapeeLoginPageDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese>)} xyz.swapee.wc.SwapeeLoginPageScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeLoginPageScreen} xyz.swapee.wc.ISwapeeLoginPageScreen.typeof */
/**
 * A concrete class of _ISwapeeLoginPageScreen_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageScreen
 * @implements {xyz.swapee.wc.ISwapeeLoginPageScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageScreen = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageScreen.constructor&xyz.swapee.wc.ISwapeeLoginPageScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.SwapeeLoginPageScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageScreen} */
xyz.swapee.wc.RecordISwapeeLoginPageScreen

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageScreen} xyz.swapee.wc.BoundISwapeeLoginPageScreen */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageScreen} xyz.swapee.wc.BoundSwapeeLoginPageScreen */

/**
 * Contains getters to cast the _ISwapeeLoginPageScreen_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageScreenCaster
 */
xyz.swapee.wc.ISwapeeLoginPageScreenCaster = class { }
/**
 * Cast the _ISwapeeLoginPageScreen_ instance into the _BoundISwapeeLoginPageScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageScreen}
 */
xyz.swapee.wc.ISwapeeLoginPageScreenCaster.prototype.asISwapeeLoginPageScreen
/**
 * Access the _SwapeeLoginPageScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageScreen}
 */
xyz.swapee.wc.ISwapeeLoginPageScreenCaster.prototype.superSwapeeLoginPageScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/70-ISwapeeLoginPageScreenBack.xml}  dcf6f68b6b4b6bc107b882873a18aa65 */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese} xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoginPageScreen)} xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoginPageScreen} xyz.swapee.wc.back.SwapeeLoginPageScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoginPageScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.constructor&xyz.swapee.wc.back.SwapeeLoginPageScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.back.SwapeeLoginPageScreen)|(!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT|typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.back.SwapeeLoginPageScreen)|(!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT|typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.back.SwapeeLoginPageScreen)|(!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT|typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoginPageScreen} xyz.swapee.wc.back.SwapeeLoginPageScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoginPageScreenCaster&xyz.swapee.wc.back.ISwapeeLoginPageScreenAT)} xyz.swapee.wc.back.ISwapeeLoginPageScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeLoginPageScreenAT} xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageScreen
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreen = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoginPageScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoginPageScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese>)} xyz.swapee.wc.back.SwapeeLoginPageScreen.constructor */
/**
 * A concrete class of _ISwapeeLoginPageScreen_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoginPageScreen
 * @implements {xyz.swapee.wc.back.ISwapeeLoginPageScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoginPageScreen = class extends /** @type {xyz.swapee.wc.back.SwapeeLoginPageScreen.constructor&xyz.swapee.wc.back.ISwapeeLoginPageScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoginPageScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreen}
 */
xyz.swapee.wc.back.SwapeeLoginPageScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageScreen} */
xyz.swapee.wc.back.RecordISwapeeLoginPageScreen

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageScreen} xyz.swapee.wc.back.BoundISwapeeLoginPageScreen */

/** @typedef {xyz.swapee.wc.back.SwapeeLoginPageScreen} xyz.swapee.wc.back.BoundSwapeeLoginPageScreen */

/**
 * Contains getters to cast the _ISwapeeLoginPageScreen_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageScreenCaster
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenCaster = class { }
/**
 * Cast the _ISwapeeLoginPageScreen_ instance into the _BoundISwapeeLoginPageScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoginPageScreen}
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenCaster.prototype.asISwapeeLoginPageScreen
/**
 * Access the _SwapeeLoginPageScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoginPageScreen}
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenCaster.prototype.superSwapeeLoginPageScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/73-ISwapeeLoginPageScreenAR.xml}  c5ca05b9518dfbcab63bc58c4bdfe8ae */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeLoginPageScreen.Initialese} xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeLoginPageScreenAR)} xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR} xyz.swapee.wc.front.SwapeeLoginPageScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeLoginPageScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.constructor&xyz.swapee.wc.front.SwapeeLoginPageScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR|typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.SwapeeLoginPageScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR|typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.SwapeeLoginPageScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR|typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeLoginPageScreen|typeof xyz.swapee.wc.SwapeeLoginPageScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.Initialese[]) => xyz.swapee.wc.front.ISwapeeLoginPageScreenAR} xyz.swapee.wc.front.SwapeeLoginPageScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeLoginPageScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeLoginPageScreen)} xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeLoginPageScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ISwapeeLoginPageScreenAR
 */
xyz.swapee.wc.front.ISwapeeLoginPageScreenAR = class extends /** @type {xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeLoginPageScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeLoginPageScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.Initialese>)} xyz.swapee.wc.front.SwapeeLoginPageScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeLoginPageScreenAR} xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.typeof */
/**
 * A concrete class of _ISwapeeLoginPageScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeLoginPageScreenAR
 * @implements {xyz.swapee.wc.front.ISwapeeLoginPageScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeLoginPageScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeLoginPageScreenAR = class extends /** @type {xyz.swapee.wc.front.SwapeeLoginPageScreenAR.constructor&xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeLoginPageScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeLoginPageScreenAR}
 */
xyz.swapee.wc.front.SwapeeLoginPageScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageScreenAR} */
xyz.swapee.wc.front.RecordISwapeeLoginPageScreenAR

/** @typedef {xyz.swapee.wc.front.ISwapeeLoginPageScreenAR} xyz.swapee.wc.front.BoundISwapeeLoginPageScreenAR */

/** @typedef {xyz.swapee.wc.front.SwapeeLoginPageScreenAR} xyz.swapee.wc.front.BoundSwapeeLoginPageScreenAR */

/**
 * Contains getters to cast the _ISwapeeLoginPageScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeLoginPageScreenARCaster
 */
xyz.swapee.wc.front.ISwapeeLoginPageScreenARCaster = class { }
/**
 * Cast the _ISwapeeLoginPageScreenAR_ instance into the _BoundISwapeeLoginPageScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeLoginPageScreenAR}
 */
xyz.swapee.wc.front.ISwapeeLoginPageScreenARCaster.prototype.asISwapeeLoginPageScreenAR
/**
 * Access the _SwapeeLoginPageScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeLoginPageScreenAR}
 */
xyz.swapee.wc.front.ISwapeeLoginPageScreenARCaster.prototype.superSwapeeLoginPageScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/74-ISwapeeLoginPageScreenAT.xml}  f0ae795851cc8b46c16b02d8165a9826 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeLoginPageScreenAT)} xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT} xyz.swapee.wc.back.SwapeeLoginPageScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeLoginPageScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.constructor&xyz.swapee.wc.back.SwapeeLoginPageScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT|typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT|typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT|typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese[]) => xyz.swapee.wc.back.ISwapeeLoginPageScreenAT} xyz.swapee.wc.back.SwapeeLoginPageScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeLoginPageScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeLoginPageScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageScreenAT
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenAT = class extends /** @type {xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeLoginPageScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese>)} xyz.swapee.wc.back.SwapeeLoginPageScreenAT.constructor */
/**
 * A concrete class of _ISwapeeLoginPageScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeLoginPageScreenAT
 * @implements {xyz.swapee.wc.back.ISwapeeLoginPageScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeLoginPageScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeLoginPageScreenAT = class extends /** @type {xyz.swapee.wc.back.SwapeeLoginPageScreenAT.constructor&xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeLoginPageScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeLoginPageScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeLoginPageScreenAT}
 */
xyz.swapee.wc.back.SwapeeLoginPageScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageScreenAT} */
xyz.swapee.wc.back.RecordISwapeeLoginPageScreenAT

/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageScreenAT} xyz.swapee.wc.back.BoundISwapeeLoginPageScreenAT */

/** @typedef {xyz.swapee.wc.back.SwapeeLoginPageScreenAT} xyz.swapee.wc.back.BoundSwapeeLoginPageScreenAT */

/**
 * Contains getters to cast the _ISwapeeLoginPageScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeLoginPageScreenATCaster
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenATCaster = class { }
/**
 * Cast the _ISwapeeLoginPageScreenAT_ instance into the _BoundISwapeeLoginPageScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeLoginPageScreenAT}
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenATCaster.prototype.asISwapeeLoginPageScreenAT
/**
 * Access the _SwapeeLoginPageScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeLoginPageScreenAT}
 */
xyz.swapee.wc.back.ISwapeeLoginPageScreenATCaster.prototype.superSwapeeLoginPageScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/pages/swapee-login/circuits/swapee-login-page/SwapeeLoginPage.mvc/design/80-ISwapeeLoginPageGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ISwapeeLoginPageDisplay.Initialese} xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeLoginPageGPU)} xyz.swapee.wc.AbstractSwapeeLoginPageGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeLoginPageGPU} xyz.swapee.wc.SwapeeLoginPageGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageGPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeLoginPageGPU
 */
xyz.swapee.wc.AbstractSwapeeLoginPageGPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeLoginPageGPU.constructor&xyz.swapee.wc.SwapeeLoginPageGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeLoginPageGPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeLoginPageGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeLoginPageGPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageGPU|typeof xyz.swapee.wc.SwapeeLoginPageGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeLoginPageGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeLoginPageGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageGPU|typeof xyz.swapee.wc.SwapeeLoginPageGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeLoginPageGPU|typeof xyz.swapee.wc.SwapeeLoginPageGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeLoginPageDisplay|typeof xyz.swapee.wc.back.SwapeeLoginPageDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageGPU}
 */
xyz.swapee.wc.AbstractSwapeeLoginPageGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese[]) => xyz.swapee.wc.ISwapeeLoginPageGPU} xyz.swapee.wc.SwapeeLoginPageGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeLoginPageGPUCaster&com.webcircuits.IBrowserView<.!SwapeeLoginPageMemory,>&xyz.swapee.wc.back.ISwapeeLoginPageDisplay)} xyz.swapee.wc.ISwapeeLoginPageGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!SwapeeLoginPageMemory,>} com.webcircuits.IBrowserView<.!SwapeeLoginPageMemory,>.typeof */
/**
 * Handles the periphery of the _ISwapeeLoginPageDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ISwapeeLoginPageGPU
 */
xyz.swapee.wc.ISwapeeLoginPageGPU = class extends /** @type {xyz.swapee.wc.ISwapeeLoginPageGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!SwapeeLoginPageMemory,>.typeof&xyz.swapee.wc.back.ISwapeeLoginPageDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeLoginPageGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeLoginPageGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeLoginPageGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese>)} xyz.swapee.wc.SwapeeLoginPageGPU.constructor */
/**
 * A concrete class of _ISwapeeLoginPageGPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeLoginPageGPU
 * @implements {xyz.swapee.wc.ISwapeeLoginPageGPU} Handles the periphery of the _ISwapeeLoginPageDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeLoginPageGPU = class extends /** @type {xyz.swapee.wc.SwapeeLoginPageGPU.constructor&xyz.swapee.wc.ISwapeeLoginPageGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeLoginPageGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeLoginPageGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeLoginPageGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeLoginPageGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeLoginPageGPU}
 */
xyz.swapee.wc.SwapeeLoginPageGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeLoginPageGPU.
 * @interface xyz.swapee.wc.ISwapeeLoginPageGPUFields
 */
xyz.swapee.wc.ISwapeeLoginPageGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ISwapeeLoginPageGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageGPU} */
xyz.swapee.wc.RecordISwapeeLoginPageGPU

/** @typedef {xyz.swapee.wc.ISwapeeLoginPageGPU} xyz.swapee.wc.BoundISwapeeLoginPageGPU */

/** @typedef {xyz.swapee.wc.SwapeeLoginPageGPU} xyz.swapee.wc.BoundSwapeeLoginPageGPU */

/**
 * Contains getters to cast the _ISwapeeLoginPageGPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeLoginPageGPUCaster
 */
xyz.swapee.wc.ISwapeeLoginPageGPUCaster = class { }
/**
 * Cast the _ISwapeeLoginPageGPU_ instance into the _BoundISwapeeLoginPageGPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeLoginPageGPU}
 */
xyz.swapee.wc.ISwapeeLoginPageGPUCaster.prototype.asISwapeeLoginPageGPU
/**
 * Access the _SwapeeLoginPageGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeLoginPageGPU}
 */
xyz.swapee.wc.ISwapeeLoginPageGPUCaster.prototype.superSwapeeLoginPageGPU

// nss:xyz.swapee.wc
/* @typal-end */