import {makeBuffers} from '@webcircuits/webcircuits'

export const SwapeeLoginPageBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  host:String,
  signIn:Boolean,
  signedIn:Boolean,
  username:String,
  password:String,
 }),
})

export default SwapeeLoginPageBuffer