
import AbstractSwapeeLoginPage from '../AbstractSwapeeLoginPage'

/** @abstract {xyz.swapee.wc.ISwapeeLoginPageElement} */
export default class AbstractSwapeeLoginPageElement { }



AbstractSwapeeLoginPageElement[$implementations]=[AbstractSwapeeLoginPage,
 /** @type {!AbstractSwapeeLoginPageElement} */ ({
  rootId:'SwapeeLoginPage',
  __$id:9759586345,
  fqn:'xyz.swapee.wc.ISwapeeLoginPage',
  maurice_element_v3:true,
 }),
]