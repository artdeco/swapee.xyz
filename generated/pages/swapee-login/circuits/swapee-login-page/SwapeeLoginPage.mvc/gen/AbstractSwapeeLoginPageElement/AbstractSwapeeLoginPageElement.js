import SwapeeLoginPageRenderVdus from './methods/render-vdus'
import SwapeeLoginPageElementPort from '../SwapeeLoginPageElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {SwapeeLoginPageInputsPQs} from '../../pqs/SwapeeLoginPageInputsPQs'
import {SwapeeLoginPageQueriesPQs} from '../../pqs/SwapeeLoginPageQueriesPQs'
import {SwapeeLoginPageCachePQs} from '../../pqs/SwapeeLoginPageCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeLoginPage from '../AbstractSwapeeLoginPage'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageElement}
 */
function __AbstractSwapeeLoginPageElement() {}
__AbstractSwapeeLoginPageElement.prototype = /** @type {!_AbstractSwapeeLoginPageElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageElement}
 */
class _AbstractSwapeeLoginPageElement { }
/**
 * A component description.
 *
 * The _ISwapeeLoginPage_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageElement} ‎
 */
class AbstractSwapeeLoginPageElement extends newAbstract(
 _AbstractSwapeeLoginPageElement,975958634512,null,{
  asISwapeeLoginPageElement:1,
  superSwapeeLoginPageElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElement} */
AbstractSwapeeLoginPageElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElement} */
function AbstractSwapeeLoginPageElementClass(){}

export default AbstractSwapeeLoginPageElement


AbstractSwapeeLoginPageElement[$implementations]=[
 __AbstractSwapeeLoginPageElement,
 ElementBase,
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':host':hostColAttr,
   ':sign-in':signInColAttr,
   ':signed-in':signedInColAttr,
   ':username':usernameColAttr,
   ':password':passwordColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'host':hostAttr,
    'sign-in':signInAttr,
    'signed-in':signedInAttr,
    'username':usernameAttr,
    'password':passwordAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(hostAttr===undefined?{'host':hostColAttr}:{}),
    ...(signInAttr===undefined?{'sign-in':signInColAttr}:{}),
    ...(signedInAttr===undefined?{'signed-in':signedInColAttr}:{}),
    ...(usernameAttr===undefined?{'username':usernameColAttr}:{}),
    ...(passwordAttr===undefined?{'password':passwordColAttr}:{}),
   }
  },
 }),
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'host':hostAttr,
   'sign-in':signInAttr,
   'signed-in':signedInAttr,
   'username':usernameAttr,
   'password':passwordAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    host:hostAttr,
    signIn:signInAttr,
    signedIn:signedInAttr,
    username:usernameAttr,
    password:passwordAttr,
   }
  },
 }),
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  render:SwapeeLoginPageRenderVdus,
 }),
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  classes:{
   'Label': 'b021d',
  },
  inputsPQs:SwapeeLoginPageInputsPQs,
  queriesPQs:SwapeeLoginPageQueriesPQs,
  cachePQs:SwapeeLoginPageCachePQs,
  vdus:{
   'UsernameIn': 'g54e1',
   'LoginBu': 'g54e2',
   'PasswordIn': 'g54e3',
   'ErrorLa': 'g54e4',
   'SwapeeMe': 'g54e5',
   'CredentialsErrorLa': 'g54e7',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','host','signIn','signedIn','username','password','query:swapee-me','no-solder',':no-solder',':host','sign-in',':sign-in','signed-in',':signed-in',':username',':password','fe646','47d7d','fe0e9','fbdef','2de57','c941f','4ccfe','67b3d','c5b5f','dcd0a','14c4b','5f4dc','children']),
   })
  },
  get Port(){
   return SwapeeLoginPageElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:swapee-me':swapeeMeSel}){
   const _ret={}
   if(swapeeMeSel) _ret.swapeeMeSel=swapeeMeSel
   return _ret
  },
 }),
 Landed,
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  constructor(){
   this.land={
    SwapeeMe:null,
   }
  },
 }),
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  calibrate:async function awaitOnSwapeeMe({swapeeMeSel:swapeeMeSel}){
   if(!swapeeMeSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const SwapeeMe=await milleu(swapeeMeSel)
   if(!SwapeeMe) {
    console.warn('❗️ swapeeMeSel %s must be present on the page for %s to work',swapeeMeSel,fqn)
    return{}
   }
   land.SwapeeMe=SwapeeMe
  },
 }),
 AbstractSwapeeLoginPageElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
  solder:(_,{
   swapeeMeSel:swapeeMeSel,
  })=>{
   return{
    swapeeMeSel:swapeeMeSel,
   }
  },
 }),
]



AbstractSwapeeLoginPageElement[$implementations]=[AbstractSwapeeLoginPage,
 /** @type {!AbstractSwapeeLoginPageElement} */ ({
  rootId:'SwapeeLoginPage',
  __$id:9759586345,
  fqn:'xyz.swapee.wc.ISwapeeLoginPage',
  maurice_element_v3:true,
 }),
]