import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {SwapeeLoginPageInputsPQs} from '../../pqs/SwapeeLoginPageInputsPQs'
import {SwapeeLoginPageOuterCoreConstructor} from '../SwapeeLoginPageCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoginPagePort}
 */
function __SwapeeLoginPagePort() {}
__SwapeeLoginPagePort.prototype = /** @type {!_SwapeeLoginPagePort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoginPagePort} */ function SwapeeLoginPagePortConstructor() {
  const self=/** @type {!xyz.swapee.wc.SwapeeLoginPageOuterCore} */ ({model:null})
  SwapeeLoginPageOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPagePort}
 */
class _SwapeeLoginPagePort { }
/**
 * The port that serves as an interface to the _ISwapeeLoginPage_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPagePort} ‎
 */
export class SwapeeLoginPagePort extends newAbstract(
 _SwapeeLoginPagePort,97595863455,SwapeeLoginPagePortConstructor,{
  asISwapeeLoginPagePort:1,
  superSwapeeLoginPagePort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPagePort} */
SwapeeLoginPagePort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPagePort} */
function SwapeeLoginPagePortClass(){}

export const SwapeeLoginPagePortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ISwapeeLoginPage.Pinout>}*/({
 get Port() { return SwapeeLoginPagePort },
})

SwapeeLoginPagePort[$implementations]=[
 SwapeeLoginPagePortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPagePort}*/({
  resetPort(){
   this.resetSwapeeLoginPagePort()
  },
  resetSwapeeLoginPagePort(){
   SwapeeLoginPagePortConstructor.call(this)
  },
 }),
 __SwapeeLoginPagePort,
 Parametric,
 SwapeeLoginPagePortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPagePort}*/({
  constructor(){
   mountPins(this.inputs,SwapeeLoginPageInputsPQs)
  },
 }),
]


export default SwapeeLoginPagePort