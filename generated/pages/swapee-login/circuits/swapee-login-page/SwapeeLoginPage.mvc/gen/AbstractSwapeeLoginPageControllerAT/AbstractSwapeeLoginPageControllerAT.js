import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageControllerAT}
 */
function __AbstractSwapeeLoginPageControllerAT() {}
__AbstractSwapeeLoginPageControllerAT.prototype = /** @type {!_AbstractSwapeeLoginPageControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT}
 */
class _AbstractSwapeeLoginPageControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeLoginPageControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT} ‎
 */
class AbstractSwapeeLoginPageControllerAT extends newAbstract(
 _AbstractSwapeeLoginPageControllerAT,975958634523,null,{
  asISwapeeLoginPageControllerAT:1,
  superSwapeeLoginPageControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT} */
AbstractSwapeeLoginPageControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageControllerAT} */
function AbstractSwapeeLoginPageControllerATClass(){}

export default AbstractSwapeeLoginPageControllerAT


AbstractSwapeeLoginPageControllerAT[$implementations]=[
 __AbstractSwapeeLoginPageControllerAT,
 UartUniversal,
 AbstractSwapeeLoginPageControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeLoginPageControllerAT}*/({
  get asISwapeeLoginPageController(){
   return this
  },
  setUsername(){
   this.uart.t("inv",{mid:'f5c47',args:[...arguments]})
  },
  unsetUsername(){
   this.uart.t("inv",{mid:'6b85a'})
  },
  setPassword(){
   this.uart.t("inv",{mid:'81be8',args:[...arguments]})
  },
  unsetPassword(){
   this.uart.t("inv",{mid:'fcf65'})
  },
  pulseSignIn(){
   this.uart.t("inv",{mid:'f5743'})
  },
  pulseSignedIn(){
   this.uart.t("inv",{mid:'70b37'})
  },
 }),
]