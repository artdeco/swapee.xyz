import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageDisplay}
 */
function __AbstractSwapeeLoginPageDisplay() {}
__AbstractSwapeeLoginPageDisplay.prototype = /** @type {!_AbstractSwapeeLoginPageDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay}
 */
class _AbstractSwapeeLoginPageDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay} ‎
 */
class AbstractSwapeeLoginPageDisplay extends newAbstract(
 _AbstractSwapeeLoginPageDisplay,975958634518,null,{
  asISwapeeLoginPageDisplay:1,
  superSwapeeLoginPageDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay} */
AbstractSwapeeLoginPageDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageDisplay} */
function AbstractSwapeeLoginPageDisplayClass(){}

export default AbstractSwapeeLoginPageDisplay


AbstractSwapeeLoginPageDisplay[$implementations]=[
 __AbstractSwapeeLoginPageDisplay,
 GraphicsDriverBack,
 AbstractSwapeeLoginPageDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeLoginPageDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.ISwapeeLoginPageDisplay}*/({
    UsernameIn:twinMock,
    PasswordIn:twinMock,
    CredentialsErrorLa:twinMock,
    ErrorLa:twinMock,
    LoginBu:twinMock,
    SwapeeMe:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.SwapeeLoginPageDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.ISwapeeLoginPageDisplay.Initialese}*/({
   ErrorLa:1,
   CredentialsErrorLa:1,
   UsernameIn:1,
   PasswordIn:1,
   LoginBu:1,
   SwapeeMe:1,
  }),
  initializer({
   ErrorLa:_ErrorLa,
   CredentialsErrorLa:_CredentialsErrorLa,
   UsernameIn:_UsernameIn,
   PasswordIn:_PasswordIn,
   LoginBu:_LoginBu,
   SwapeeMe:_SwapeeMe,
  }) {
   if(_ErrorLa!==undefined) this.ErrorLa=_ErrorLa
   if(_CredentialsErrorLa!==undefined) this.CredentialsErrorLa=_CredentialsErrorLa
   if(_UsernameIn!==undefined) this.UsernameIn=_UsernameIn
   if(_PasswordIn!==undefined) this.PasswordIn=_PasswordIn
   if(_LoginBu!==undefined) this.LoginBu=_LoginBu
   if(_SwapeeMe!==undefined) this.SwapeeMe=_SwapeeMe
  },
 }),
]