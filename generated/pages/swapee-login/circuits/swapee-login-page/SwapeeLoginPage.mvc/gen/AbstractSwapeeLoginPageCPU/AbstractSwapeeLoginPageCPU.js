import {StatefulLoadable} from '@mauriceguest/guest2'
import {SwapeeLoginPageCore} from '../SwapeeLoginPageCore'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageCPU}
 */
function __AbstractSwapeeLoginPageCPU() {}
__AbstractSwapeeLoginPageCPU.prototype = /** @type {!_AbstractSwapeeLoginPageCPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageCPU}
 */
class _AbstractSwapeeLoginPageCPU { }
/**
 * The _ISwapeeLoginPage_'s CPU that encapsulates the component's native atomic
 * core, as well as additional embedded cores.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageCPU} ‎
 */
export class AbstractSwapeeLoginPageCPU extends newAbstract(
 _AbstractSwapeeLoginPageCPU,975958634531,null,{
  asISwapeeLoginPageCPU:1,
  superSwapeeLoginPageCPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCPU} */
AbstractSwapeeLoginPageCPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCPU} */
function AbstractSwapeeLoginPageCPUClass(){}


AbstractSwapeeLoginPageCPU[$implementations]=[
 __AbstractSwapeeLoginPageCPU,
 AbstractSwapeeLoginPageCPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageCPU}*/({
  resetCore:precombined,
 }),
 StatefulLoadable,
 SwapeeLoginPageCore,
]


export default AbstractSwapeeLoginPageCPU