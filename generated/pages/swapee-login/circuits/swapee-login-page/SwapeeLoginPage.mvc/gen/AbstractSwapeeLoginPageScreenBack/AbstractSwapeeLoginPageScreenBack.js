import AbstractSwapeeLoginPageScreenAT from '../AbstractSwapeeLoginPageScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageScreenBack}
 */
function __AbstractSwapeeLoginPageScreenBack() {}
__AbstractSwapeeLoginPageScreenBack.prototype = /** @type {!_AbstractSwapeeLoginPageScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen}
 */
class _AbstractSwapeeLoginPageScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen} ‎
 */
class AbstractSwapeeLoginPageScreenBack extends newAbstract(
 _AbstractSwapeeLoginPageScreenBack,975958634525,null,{
  asISwapeeLoginPageScreen:1,
  superSwapeeLoginPageScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen} */
AbstractSwapeeLoginPageScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreen} */
function AbstractSwapeeLoginPageScreenBackClass(){}

export default AbstractSwapeeLoginPageScreenBack


AbstractSwapeeLoginPageScreenBack[$implementations]=[
 __AbstractSwapeeLoginPageScreenBack,
 AbstractSwapeeLoginPageScreenAT,
]