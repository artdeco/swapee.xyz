
/**@this {xyz.swapee.wc.ISwapeeLoginPageComputer}*/
export function preadaptClearError(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ISwapeeLoginPageComputer.adaptClearError.Form}*/
 const _inputs={
  username:inputs.username,
  password:inputs.password,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptClearError(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ISwapeeLoginPageComputer}*/
export function preadaptCredentialsError(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ISwapeeLoginPageComputer.adaptCredentialsError.Form}*/
 const _inputs={
  errorText:inputs.errorText,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCredentialsError(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ISwapeeLoginPageComputer}*/
export function preadaptLogin(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ISwapeeLoginPageComputer.adaptLogin.Form}*/
 const _inputs={
  host:inputs.host,
  username:inputs.username,
  password:inputs.password,
  signIn:inputs.signIn,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.host)||(!__inputs.username)||(!__inputs.password)||(!__inputs.signIn)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLogin(__inputs,__changes)
 return RET.then(R=>{
  const{token:token,username:username,userid:userid,displayName:displayName,profile:profile,...REST}=R
  const{land:{SwapeeMe:SwapeeMe}}=this
  if(SwapeeMe) SwapeeMe.port.inputs['94a08']=token
  if(SwapeeMe) SwapeeMe.port.inputs['14c4b']=username
  if(SwapeeMe) SwapeeMe.port.inputs['ea8f5']=userid
  if(SwapeeMe) SwapeeMe.port.inputs['4498e']=displayName
  if(SwapeeMe) SwapeeMe.port.inputs['7d974']=profile
  return REST
 })
}