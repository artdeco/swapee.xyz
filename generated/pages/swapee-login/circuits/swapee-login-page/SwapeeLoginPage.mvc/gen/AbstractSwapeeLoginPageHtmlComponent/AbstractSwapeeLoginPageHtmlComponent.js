import AbstractSwapeeLoginPageGPU from '../AbstractSwapeeLoginPageGPU'
import AbstractSwapeeLoginPageScreenBack from '../AbstractSwapeeLoginPageScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {SwapeeLoginPageInputsQPs} from '../../pqs/SwapeeLoginPageInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractSwapeeLoginPage from '../AbstractSwapeeLoginPage'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageHtmlComponent}
 */
function __AbstractSwapeeLoginPageHtmlComponent() {}
__AbstractSwapeeLoginPageHtmlComponent.prototype = /** @type {!_AbstractSwapeeLoginPageHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent}
 */
class _AbstractSwapeeLoginPageHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.SwapeeLoginPageHtmlComponent} */ (res)
  }
}
/**
 * The _ISwapeeLoginPage_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent} ‎
 */
export class AbstractSwapeeLoginPageHtmlComponent extends newAbstract(
 _AbstractSwapeeLoginPageHtmlComponent,975958634511,null,{
  asISwapeeLoginPageHtmlComponent:1,
  superSwapeeLoginPageHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent} */
AbstractSwapeeLoginPageHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent} */
function AbstractSwapeeLoginPageHtmlComponentClass(){}


AbstractSwapeeLoginPageHtmlComponent[$implementations]=[
 __AbstractSwapeeLoginPageHtmlComponent,
 HtmlComponent,
 AbstractSwapeeLoginPage,
 AbstractSwapeeLoginPageGPU,
 AbstractSwapeeLoginPageScreenBack,
 Landed,
 AbstractSwapeeLoginPageHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent}*/({
  constructor(){
   this.land={
    SwapeeMe:null,
   }
  },
 }),
 AbstractSwapeeLoginPageHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent}*/({
  inputsQPs:SwapeeLoginPageInputsQPs,
 }),

/** @type {!AbstractSwapeeLoginPageHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent}*/function paintErrorLa() {
   this.ErrorLa.setText(this.model.errorText)
  }
 ] }),
 AbstractSwapeeLoginPageHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent}*/({
  paint:function $reveal_CredentialsErrorLa({error:error,credentialsError:credentialsError}){
   const{
    asISwapeeLoginPageGPU:{CredentialsErrorLa:CredentialsErrorLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(CredentialsErrorLa,error&&credentialsError)
  },
 }),
 AbstractSwapeeLoginPageHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent}*/({
  paint:function $reveal_ErrorLa({error:error,credentialsError:credentialsError}){
   const{
    asISwapeeLoginPageGPU:{ErrorLa:ErrorLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ErrorLa,error&&!credentialsError)
  },
 }),
 AbstractSwapeeLoginPageHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent}*/({
  paint:function paint_disabled_on_LoginBu({username:username,password:password,loading:loading}){
   const{asISwapeeLoginPageGPU:{LoginBu:LoginBu},asIBrowserView:{attr:attr}}=this
   attr(LoginBu,'disabled',(!username||!password||loading)||null)
  },
 }),
 AbstractSwapeeLoginPageHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asISwapeeLoginPageGPU:{
     SwapeeMe:SwapeeMe,
    },
   }=this
   complete(6200449439,{SwapeeMe:SwapeeMe})
  },
 }),
]