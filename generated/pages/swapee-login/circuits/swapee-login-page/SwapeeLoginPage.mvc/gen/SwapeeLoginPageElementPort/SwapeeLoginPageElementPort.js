import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoginPageElementPort}
 */
function __SwapeeLoginPageElementPort() {}
__SwapeeLoginPageElementPort.prototype = /** @type {!_SwapeeLoginPageElementPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoginPageElementPort} */ function SwapeeLoginPageElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeLoginPageElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    errorLaOpts: {},
    credentialsErrorLaOpts: {},
    usernameInOpts: {},
    passwordInOpts: {},
    loginBuOpts: {},
    swapeeMeOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageElementPort}
 */
class _SwapeeLoginPageElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageElementPort} ‎
 */
class SwapeeLoginPageElementPort extends newAbstract(
 _SwapeeLoginPageElementPort,975958634513,SwapeeLoginPageElementPortConstructor,{
  asISwapeeLoginPageElementPort:1,
  superSwapeeLoginPageElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElementPort} */
SwapeeLoginPageElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageElementPort} */
function SwapeeLoginPageElementPortClass(){}

export default SwapeeLoginPageElementPort


SwapeeLoginPageElementPort[$implementations]=[
 __SwapeeLoginPageElementPort,
 SwapeeLoginPageElementPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'error-la-opts':undefined,
    'credentials-error-la-opts':undefined,
    'username-in-opts':undefined,
    'password-in-opts':undefined,
    'login-bu-opts':undefined,
    'swapee-me-opts':undefined,
    'sign-in':undefined,
    'signed-in':undefined,
   })
  },
 }),
]