import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageScreenAR}
 */
function __AbstractSwapeeLoginPageScreenAR() {}
__AbstractSwapeeLoginPageScreenAR.prototype = /** @type {!_AbstractSwapeeLoginPageScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR}
 */
class _AbstractSwapeeLoginPageScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeLoginPageScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR} ‎
 */
class AbstractSwapeeLoginPageScreenAR extends newAbstract(
 _AbstractSwapeeLoginPageScreenAR,975958634526,null,{
  asISwapeeLoginPageScreenAR:1,
  superSwapeeLoginPageScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR} */
AbstractSwapeeLoginPageScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeLoginPageScreenAR} */
function AbstractSwapeeLoginPageScreenARClass(){}

export default AbstractSwapeeLoginPageScreenAR


AbstractSwapeeLoginPageScreenAR[$implementations]=[
 __AbstractSwapeeLoginPageScreenAR,
 AR,
 AbstractSwapeeLoginPageScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeLoginPageScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractSwapeeLoginPageScreenAR}