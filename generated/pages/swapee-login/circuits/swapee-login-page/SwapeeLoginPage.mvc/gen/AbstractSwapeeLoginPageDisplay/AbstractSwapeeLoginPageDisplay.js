import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageDisplay}
 */
function __AbstractSwapeeLoginPageDisplay() {}
__AbstractSwapeeLoginPageDisplay.prototype = /** @type {!_AbstractSwapeeLoginPageDisplay} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoginPageDisplay} */ function SwapeeLoginPageDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.ErrorLa=null
  /** @type {HTMLSpanElement} */ this.CredentialsErrorLa=null
  /** @type {HTMLInputElement} */ this.UsernameIn=null
  /** @type {HTMLInputElement} */ this.PasswordIn=null
  /** @type {HTMLButtonElement} */ this.LoginBu=null
  /** @type {HTMLElement} */ this.SwapeeMe=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageDisplay}
 */
class _AbstractSwapeeLoginPageDisplay { }
/**
 * Display for presenting information from the _ISwapeeLoginPage_.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageDisplay} ‎
 */
class AbstractSwapeeLoginPageDisplay extends newAbstract(
 _AbstractSwapeeLoginPageDisplay,975958634516,SwapeeLoginPageDisplayConstructor,{
  asISwapeeLoginPageDisplay:1,
  superSwapeeLoginPageDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageDisplay} */
AbstractSwapeeLoginPageDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageDisplay} */
function AbstractSwapeeLoginPageDisplayClass(){}

export default AbstractSwapeeLoginPageDisplay


AbstractSwapeeLoginPageDisplay[$implementations]=[
 __AbstractSwapeeLoginPageDisplay,
 Display,
 AbstractSwapeeLoginPageDisplayClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{swapeeMeScopeSel:swapeeMeScopeSel}}=this
    this.scan({
     swapeeMeSel:swapeeMeScopeSel,
    })
   })
  },
  scan:function vduScan({swapeeMeSel:swapeeMeSelScope}){
   const{element:element,asISwapeeLoginPageScreen:{vdusPQs:{
    UsernameIn:UsernameIn,
    PasswordIn:PasswordIn,
    CredentialsErrorLa:CredentialsErrorLa,
    ErrorLa:ErrorLa,
    LoginBu:LoginBu,
   }},queries:{swapeeMeSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const SwapeeMe=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _SwapeeMe=swapeeMeSel?root.querySelector(swapeeMeSel):void 0
    return _SwapeeMe
   })(swapeeMeSelScope)
   Object.assign(this,{
    UsernameIn:/**@type {HTMLInputElement}*/(children[UsernameIn]),
    PasswordIn:/**@type {HTMLInputElement}*/(children[PasswordIn]),
    CredentialsErrorLa:/**@type {HTMLSpanElement}*/(children[CredentialsErrorLa]),
    ErrorLa:/**@type {HTMLSpanElement}*/(children[ErrorLa]),
    LoginBu:/**@type {HTMLButtonElement}*/(children[LoginBu]),
    SwapeeMe:SwapeeMe,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.SwapeeLoginPageDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.ISwapeeLoginPageDisplay.Initialese}*/({
   ErrorLa:1,
   CredentialsErrorLa:1,
   UsernameIn:1,
   PasswordIn:1,
   LoginBu:1,
   SwapeeMe:1,
  }),
  initializer({
   ErrorLa:_ErrorLa,
   CredentialsErrorLa:_CredentialsErrorLa,
   UsernameIn:_UsernameIn,
   PasswordIn:_PasswordIn,
   LoginBu:_LoginBu,
   SwapeeMe:_SwapeeMe,
  }) {
   if(_ErrorLa!==undefined) this.ErrorLa=_ErrorLa
   if(_CredentialsErrorLa!==undefined) this.CredentialsErrorLa=_CredentialsErrorLa
   if(_UsernameIn!==undefined) this.UsernameIn=_UsernameIn
   if(_PasswordIn!==undefined) this.PasswordIn=_PasswordIn
   if(_LoginBu!==undefined) this.LoginBu=_LoginBu
   if(_SwapeeMe!==undefined) this.SwapeeMe=_SwapeeMe
  },
 }),
]