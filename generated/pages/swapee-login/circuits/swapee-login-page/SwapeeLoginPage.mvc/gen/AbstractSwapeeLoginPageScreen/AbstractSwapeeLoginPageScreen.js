import SwapeeLoginPageClassesPQs from '../../pqs/SwapeeLoginPageClassesPQs'
import AbstractSwapeeLoginPageScreenAR from '../AbstractSwapeeLoginPageScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {SwapeeLoginPageInputsPQs} from '../../pqs/SwapeeLoginPageInputsPQs'
import {SwapeeLoginPageQueriesPQs} from '../../pqs/SwapeeLoginPageQueriesPQs'
import {SwapeeLoginPageMemoryQPs} from '../../pqs/SwapeeLoginPageMemoryQPs'
import {SwapeeLoginPageCacheQPs} from '../../pqs/SwapeeLoginPageCacheQPs'
import {SwapeeLoginPageVdusPQs} from '../../pqs/SwapeeLoginPageVdusPQs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageScreen}
 */
function __AbstractSwapeeLoginPageScreen() {}
__AbstractSwapeeLoginPageScreen.prototype = /** @type {!_AbstractSwapeeLoginPageScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageScreen}
 */
class _AbstractSwapeeLoginPageScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageScreen} ‎
 */
class AbstractSwapeeLoginPageScreen extends newAbstract(
 _AbstractSwapeeLoginPageScreen,975958634524,null,{
  asISwapeeLoginPageScreen:1,
  superSwapeeLoginPageScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageScreen} */
AbstractSwapeeLoginPageScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageScreen} */
function AbstractSwapeeLoginPageScreenClass(){}

export default AbstractSwapeeLoginPageScreen


AbstractSwapeeLoginPageScreen[$implementations]=[
 __AbstractSwapeeLoginPageScreen,
 AbstractSwapeeLoginPageScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageScreen}*/({
  deduceInputs(){
   const{asISwapeeLoginPageDisplay:{
    ErrorLa:ErrorLa,
   }}=this
   return{
    errorText:ErrorLa?.innerText,
   }
  },
 }),
 AbstractSwapeeLoginPageScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageScreen}*/({
  inputsPQs:SwapeeLoginPageInputsPQs,
  classesPQs:SwapeeLoginPageClassesPQs,
  queriesPQs:SwapeeLoginPageQueriesPQs,
  memoryQPs:SwapeeLoginPageMemoryQPs,
  cacheQPs:SwapeeLoginPageCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractSwapeeLoginPageScreenAR,
 AbstractSwapeeLoginPageScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageScreen}*/({
  vdusPQs:SwapeeLoginPageVdusPQs,
 }),
 AbstractSwapeeLoginPageScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageScreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const UsernameIn=this.UsernameIn
    if(UsernameIn){
     UsernameIn.addEventListener('input',(ev)=>{
      this.setUsername(ev.target.value)
     })
    }
    const PasswordIn=this.PasswordIn
    if(PasswordIn){
     PasswordIn.addEventListener('input',(ev)=>{
      this.setPassword(ev.target.value)
     })
    }
    const LoginBu=this.LoginBu
    if(LoginBu){
     LoginBu.addEventListener('click',(ev)=>{
     ev.preventDefault()
      this.pulseSignIn()
     return false
     })
    }
   })
  },
 }),
]