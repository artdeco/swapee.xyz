import AbstractSwapeeLoginPageDisplay from '../AbstractSwapeeLoginPageDisplayBack'
import SwapeeLoginPageClassesPQs from '../../pqs/SwapeeLoginPageClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {SwapeeLoginPageClassesQPs} from '../../pqs/SwapeeLoginPageClassesQPs'
import {SwapeeLoginPageVdusPQs} from '../../pqs/SwapeeLoginPageVdusPQs'
import {SwapeeLoginPageVdusQPs} from '../../pqs/SwapeeLoginPageVdusQPs'
import {SwapeeLoginPageMemoryPQs} from '../../pqs/SwapeeLoginPageMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageGPU}
 */
function __AbstractSwapeeLoginPageGPU() {}
__AbstractSwapeeLoginPageGPU.prototype = /** @type {!_AbstractSwapeeLoginPageGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageGPU}
 */
class _AbstractSwapeeLoginPageGPU { }
/**
 * Handles the periphery of the _ISwapeeLoginPageDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageGPU} ‎
 */
class AbstractSwapeeLoginPageGPU extends newAbstract(
 _AbstractSwapeeLoginPageGPU,975958634515,null,{
  asISwapeeLoginPageGPU:1,
  superSwapeeLoginPageGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageGPU} */
AbstractSwapeeLoginPageGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageGPU} */
function AbstractSwapeeLoginPageGPUClass(){}

export default AbstractSwapeeLoginPageGPU


AbstractSwapeeLoginPageGPU[$implementations]=[
 __AbstractSwapeeLoginPageGPU,
 AbstractSwapeeLoginPageGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageGPU}*/({
  classesQPs:SwapeeLoginPageClassesQPs,
  vdusPQs:SwapeeLoginPageVdusPQs,
  vdusQPs:SwapeeLoginPageVdusQPs,
  memoryPQs:SwapeeLoginPageMemoryPQs,
 }),
 AbstractSwapeeLoginPageDisplay,
 BrowserView,
 AbstractSwapeeLoginPageGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageGPU}*/({
  allocator(){
   pressFit(this.classes,'',SwapeeLoginPageClassesPQs)
  },
 }),
]