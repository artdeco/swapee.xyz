import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageScreenAT}
 */
function __AbstractSwapeeLoginPageScreenAT() {}
__AbstractSwapeeLoginPageScreenAT.prototype = /** @type {!_AbstractSwapeeLoginPageScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT}
 */
class _AbstractSwapeeLoginPageScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeLoginPageScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT} ‎
 */
class AbstractSwapeeLoginPageScreenAT extends newAbstract(
 _AbstractSwapeeLoginPageScreenAT,975958634527,null,{
  asISwapeeLoginPageScreenAT:1,
  superSwapeeLoginPageScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT} */
AbstractSwapeeLoginPageScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageScreenAT} */
function AbstractSwapeeLoginPageScreenATClass(){}

export default AbstractSwapeeLoginPageScreenAT


AbstractSwapeeLoginPageScreenAT[$implementations]=[
 __AbstractSwapeeLoginPageScreenAT,
 UartUniversal,
]