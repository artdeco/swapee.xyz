import SwapeeLoginPageBuffer from '../SwapeeLoginPageBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {SwapeeLoginPagePortConnector} from '../SwapeeLoginPagePort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageController}
 */
function __AbstractSwapeeLoginPageController() {}
__AbstractSwapeeLoginPageController.prototype = /** @type {!_AbstractSwapeeLoginPageController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageController}
 */
class _AbstractSwapeeLoginPageController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageController} ‎
 */
export class AbstractSwapeeLoginPageController extends newAbstract(
 _AbstractSwapeeLoginPageController,975958634519,null,{
  asISwapeeLoginPageController:1,
  superSwapeeLoginPageController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageController} */
AbstractSwapeeLoginPageController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageController} */
function AbstractSwapeeLoginPageControllerClass(){}


AbstractSwapeeLoginPageController[$implementations]=[
 AbstractSwapeeLoginPageControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.ISwapeeLoginPagePort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractSwapeeLoginPageController,
 /**@type {!xyz.swapee.wc.ISwapeeLoginPageController}*/({
  calibrate:[
    /**@this {!xyz.swapee.wc.ISwapeeLoginPageController}*/ function calibrateSignIn({signIn:signIn}){
     if(!signIn) return
     setTimeout(()=>{
      const{asISwapeeLoginPageController:{setInputs:setInputs}}=this
      setInputs({
       signIn:false,
      })
     },1)
    },
    /**@this {!xyz.swapee.wc.ISwapeeLoginPageController}*/ function calibrateSignedIn({signedIn:signedIn}){
     if(!signedIn) return
     setTimeout(()=>{
      const{asISwapeeLoginPageController:{setInputs:setInputs}}=this
      setInputs({
       signedIn:false,
      })
     },1)
    },
  ],
 }),
 SwapeeLoginPageBuffer,
 IntegratedController,
 /**@type {!AbstractSwapeeLoginPageController}*/(SwapeeLoginPagePortConnector),
 AbstractSwapeeLoginPageControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageController}*/({
  setUsername(val){
   const{asISwapeeLoginPageController:{setInputs:setInputs}}=this
   setInputs({username:val})
  },
  setPassword(val){
   const{asISwapeeLoginPageController:{setInputs:setInputs}}=this
   setInputs({password:val})
  },
  unsetUsername(){
   const{asISwapeeLoginPageController:{setInputs:setInputs}}=this
   setInputs({username:''})
  },
  unsetPassword(){
   const{asISwapeeLoginPageController:{setInputs:setInputs}}=this
   setInputs({password:''})
  },
 }),
]


export default AbstractSwapeeLoginPageController