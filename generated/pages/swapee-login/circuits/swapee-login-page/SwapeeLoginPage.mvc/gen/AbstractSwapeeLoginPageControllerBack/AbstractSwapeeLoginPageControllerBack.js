import AbstractSwapeeLoginPageControllerAR from '../AbstractSwapeeLoginPageControllerAR'
import {AbstractSwapeeLoginPageController} from '../AbstractSwapeeLoginPageController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageControllerBack}
 */
function __AbstractSwapeeLoginPageControllerBack() {}
__AbstractSwapeeLoginPageControllerBack.prototype = /** @type {!_AbstractSwapeeLoginPageControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageController}
 */
class _AbstractSwapeeLoginPageControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeLoginPageController} ‎
 */
class AbstractSwapeeLoginPageControllerBack extends newAbstract(
 _AbstractSwapeeLoginPageControllerBack,975958634521,null,{
  asISwapeeLoginPageController:1,
  superSwapeeLoginPageController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageController} */
AbstractSwapeeLoginPageControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeLoginPageController} */
function AbstractSwapeeLoginPageControllerBackClass(){}

export default AbstractSwapeeLoginPageControllerBack


AbstractSwapeeLoginPageControllerBack[$implementations]=[
 __AbstractSwapeeLoginPageControllerBack,
 AbstractSwapeeLoginPageController,
 AbstractSwapeeLoginPageControllerAR,
 DriverBack,
]