import AbstractSwapeeLoginPageProcessor from '../AbstractSwapeeLoginPageProcessor'
import {AbstractSwapeeLoginPageCPU} from '../AbstractSwapeeLoginPageCPU'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractSwapeeLoginPageComputer} from '../AbstractSwapeeLoginPageComputer'
import {AbstractSwapeeLoginPageController} from '../AbstractSwapeeLoginPageController'
import {regulateSwapeeLoginPageCache} from './methods/regulateSwapeeLoginPageCache'
import {SwapeeLoginPageCacheQPs} from '../../pqs/SwapeeLoginPageCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPage}
 */
function __AbstractSwapeeLoginPage() {}
__AbstractSwapeeLoginPage.prototype = /** @type {!_AbstractSwapeeLoginPage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPage}
 */
class _AbstractSwapeeLoginPage { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPage} ‎
 */
class AbstractSwapeeLoginPage extends newAbstract(
 _AbstractSwapeeLoginPage,97595863459,null,{
  asISwapeeLoginPage:1,
  superSwapeeLoginPage:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPage} */
AbstractSwapeeLoginPage.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPage} */
function AbstractSwapeeLoginPageClass(){}

export default AbstractSwapeeLoginPage


AbstractSwapeeLoginPage[$implementations]=[
 __AbstractSwapeeLoginPage,
 AbstractSwapeeLoginPageCPU,
 AbstractSwapeeLoginPageProcessor,
 IntegratedComponent,
 AbstractSwapeeLoginPageComputer,
 AbstractSwapeeLoginPageController,
 AbstractSwapeeLoginPageClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPage}*/({
  regulateState:regulateSwapeeLoginPageCache,
  stateQPs:SwapeeLoginPageCacheQPs,
 }),
]


export {AbstractSwapeeLoginPage}