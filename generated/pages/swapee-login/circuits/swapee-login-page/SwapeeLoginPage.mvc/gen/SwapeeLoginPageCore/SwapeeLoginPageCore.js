import {mountPins} from '@type.engineering/seers'
import {SwapeeLoginPageMemoryPQs} from '../../pqs/SwapeeLoginPageMemoryPQs'
import {SwapeeLoginPageCachePQs} from '../../pqs/SwapeeLoginPageCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoginPageCore}
 */
function __SwapeeLoginPageCore() {}
__SwapeeLoginPageCore.prototype = /** @type {!_SwapeeLoginPageCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoginPageCore} */ function SwapeeLoginPageCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeLoginPageCore.Model}*/
  this.model={
    credentialsError: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageCore}
 */
class _SwapeeLoginPageCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageCore} ‎
 */
class SwapeeLoginPageCore extends newAbstract(
 _SwapeeLoginPageCore,97595863457,SwapeeLoginPageCoreConstructor,{
  asISwapeeLoginPageCore:1,
  superSwapeeLoginPageCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCore} */
SwapeeLoginPageCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCore} */
function SwapeeLoginPageCoreClass(){}

export default SwapeeLoginPageCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeLoginPageOuterCore}
 */
function __SwapeeLoginPageOuterCore() {}
__SwapeeLoginPageOuterCore.prototype = /** @type {!_SwapeeLoginPageOuterCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeLoginPageOuterCore} */
export function SwapeeLoginPageOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeLoginPageOuterCore.Model}*/
  this.model={
    host: 'https://swapee.me/api',
    signIn: false,
    signedIn: false,
    username: '',
    password: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore}
 */
class _SwapeeLoginPageOuterCore { }
/**
 * The _ISwapeeLoginPage_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore} ‎
 */
export class SwapeeLoginPageOuterCore extends newAbstract(
 _SwapeeLoginPageOuterCore,97595863453,SwapeeLoginPageOuterCoreConstructor,{
  asISwapeeLoginPageOuterCore:1,
  superSwapeeLoginPageOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore} */
SwapeeLoginPageOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageOuterCore} */
function SwapeeLoginPageOuterCoreClass(){}


SwapeeLoginPageOuterCore[$implementations]=[
 __SwapeeLoginPageOuterCore,
 SwapeeLoginPageOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageOuterCore}*/({
  constructor(){
   mountPins(this.model,SwapeeLoginPageMemoryPQs)
   mountPins(this.model,SwapeeLoginPageCachePQs)
  },
 }),
]

SwapeeLoginPageCore[$implementations]=[
 SwapeeLoginPageCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeLoginPageCore}*/({
  resetCore(){
   this.resetSwapeeLoginPageCore()
  },
  resetSwapeeLoginPageCore(){
   SwapeeLoginPageCoreConstructor.call(
    /**@type {xyz.swapee.wc.SwapeeLoginPageCore}*/(this),
   )
   SwapeeLoginPageOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.SwapeeLoginPageOuterCore}*/(
     /**@type {!xyz.swapee.wc.ISwapeeLoginPageOuterCore}*/(this)),
   )
  },
 }),
 __SwapeeLoginPageCore,
 SwapeeLoginPageOuterCore,
]

export {SwapeeLoginPageCore}