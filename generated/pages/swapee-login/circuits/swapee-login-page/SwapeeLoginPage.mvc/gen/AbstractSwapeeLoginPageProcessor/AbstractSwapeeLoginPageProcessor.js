import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeLoginPageProcessor}
 */
function __AbstractSwapeeLoginPageProcessor() {}
__AbstractSwapeeLoginPageProcessor.prototype = /** @type {!_AbstractSwapeeLoginPageProcessor} */ ({
  /** @return {void} */
  pulseSignIn() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     signIn:true,
    })
  },
  /** @return {void} */
  pulseSignedIn() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     signedIn:true,
    })
  },
})
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageProcessor}
 */
class _AbstractSwapeeLoginPageProcessor { }
/**
 * The processor to compute changes to the memory for the _ISwapeeLoginPage_.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageProcessor} ‎
 */
class AbstractSwapeeLoginPageProcessor extends newAbstract(
 _AbstractSwapeeLoginPageProcessor,97595863458,null,{
  asISwapeeLoginPageProcessor:1,
  superSwapeeLoginPageProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageProcessor} */
AbstractSwapeeLoginPageProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageProcessor} */
function AbstractSwapeeLoginPageProcessorClass(){}

export default AbstractSwapeeLoginPageProcessor


AbstractSwapeeLoginPageProcessor[$implementations]=[
 __AbstractSwapeeLoginPageProcessor,
]