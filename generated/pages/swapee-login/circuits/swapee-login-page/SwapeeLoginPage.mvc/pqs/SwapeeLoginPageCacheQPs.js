import {SwapeeLoginPageCachePQs} from './SwapeeLoginPageCachePQs'
export const SwapeeLoginPageCacheQPs=/**@type {!xyz.swapee.wc.SwapeeLoginPageCacheQPs}*/(Object.keys(SwapeeLoginPageCachePQs)
 .reduce((a,k)=>{a[SwapeeLoginPageCachePQs[k]]=k;return a},{}))