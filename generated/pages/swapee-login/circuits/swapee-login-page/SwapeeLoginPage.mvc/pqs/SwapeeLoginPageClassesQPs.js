import SwapeeLoginPageClassesPQs from './SwapeeLoginPageClassesPQs'
export const SwapeeLoginPageClassesQPs=/**@type {!xyz.swapee.wc.SwapeeLoginPageClassesQPs}*/(Object.keys(SwapeeLoginPageClassesPQs)
 .reduce((a,k)=>{a[SwapeeLoginPageClassesPQs[k]]=k;return a},{}))