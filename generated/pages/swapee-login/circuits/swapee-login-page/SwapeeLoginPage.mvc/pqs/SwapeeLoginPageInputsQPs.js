import {SwapeeLoginPageInputsPQs} from './SwapeeLoginPageInputsPQs'
export const SwapeeLoginPageInputsQPs=/**@type {!xyz.swapee.wc.SwapeeLoginPageInputsQPs}*/(Object.keys(SwapeeLoginPageInputsPQs)
 .reduce((a,k)=>{a[SwapeeLoginPageInputsPQs[k]]=k;return a},{}))