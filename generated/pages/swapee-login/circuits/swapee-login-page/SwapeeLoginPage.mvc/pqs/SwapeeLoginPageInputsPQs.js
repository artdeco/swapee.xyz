import {SwapeeLoginPageMemoryPQs} from './SwapeeLoginPageMemoryPQs'
export const SwapeeLoginPageInputsPQs=/**@type {!xyz.swapee.wc.SwapeeLoginPageInputsQPs}*/({
 ...SwapeeLoginPageMemoryPQs,
})