import {SwapeeLoginPageMemoryPQs} from './SwapeeLoginPageMemoryPQs'
export const SwapeeLoginPageMemoryQPs=/**@type {!xyz.swapee.wc.SwapeeLoginPageMemoryQPs}*/(Object.keys(SwapeeLoginPageMemoryPQs)
 .reduce((a,k)=>{a[SwapeeLoginPageMemoryPQs[k]]=k;return a},{}))