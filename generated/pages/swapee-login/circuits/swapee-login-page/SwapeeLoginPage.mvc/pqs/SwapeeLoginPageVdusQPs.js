import {SwapeeLoginPageVdusPQs} from './SwapeeLoginPageVdusPQs'
export const SwapeeLoginPageVdusQPs=/**@type {!xyz.swapee.wc.SwapeeLoginPageVdusQPs}*/(Object.keys(SwapeeLoginPageVdusPQs)
 .reduce((a,k)=>{a[SwapeeLoginPageVdusPQs[k]]=k;return a},{}))