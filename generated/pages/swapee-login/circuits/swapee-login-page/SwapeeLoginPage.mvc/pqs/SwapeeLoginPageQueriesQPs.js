import {SwapeeLoginPageQueriesPQs} from './SwapeeLoginPageQueriesPQs'
export const SwapeeLoginPageQueriesQPs=/**@type {!xyz.swapee.wc.SwapeeLoginPageQueriesQPs}*/(Object.keys(SwapeeLoginPageQueriesPQs)
 .reduce((a,k)=>{a[SwapeeLoginPageQueriesPQs[k]]=k;return a},{}))