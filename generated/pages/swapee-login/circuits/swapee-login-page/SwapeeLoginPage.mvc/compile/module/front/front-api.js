import { SwapeeLoginPageDisplay, SwapeeLoginPageScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.SwapeeLoginPageDisplay} */
export { SwapeeLoginPageDisplay }
/** @lazy @api {xyz.swapee.wc.SwapeeLoginPageScreen} */
export { SwapeeLoginPageScreen }