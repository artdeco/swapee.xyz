import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractSwapeeLoginPage}*/
export class AbstractSwapeeLoginPage extends Module['97595863451'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPage} */
AbstractSwapeeLoginPage.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeLoginPagePort} */
export const SwapeeLoginPagePort=Module['97595863453']
/**@extends {xyz.swapee.wc.AbstractSwapeeLoginPageController}*/
export class AbstractSwapeeLoginPageController extends Module['97595863454'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageController} */
AbstractSwapeeLoginPageController.class=function(){}
/**@extends {xyz.swapee.wc.AbstractSwapeeLoginPageCPU}*/
export class AbstractSwapeeLoginPageCPU extends Module['97595863455'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageCPU} */
AbstractSwapeeLoginPageCPU.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeLoginPageHtmlComponent} */
export const SwapeeLoginPageHtmlComponent=Module['975958634510']
/** @type {typeof xyz.swapee.wc.SwapeeLoginPageBuffer} */
export const SwapeeLoginPageBuffer=Module['975958634511']
/**@extends {xyz.swapee.wc.AbstractSwapeeLoginPageComputer}*/
export class AbstractSwapeeLoginPageComputer extends Module['975958634530'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeLoginPageComputer} */
AbstractSwapeeLoginPageComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeLoginPageComputer} */
export const SwapeeLoginPageComputer=Module['975958634531']
/** @type {typeof xyz.swapee.wc.back.SwapeeLoginPageController} */
export const SwapeeLoginPageController=Module['975958634561']