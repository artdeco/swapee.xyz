/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPage` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPage}
 */
class AbstractSwapeeLoginPage extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeLoginPage_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeLoginPagePort}
 */
class SwapeeLoginPagePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageController}
 */
class AbstractSwapeeLoginPageController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageCPU` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageCPU}
 */
class AbstractSwapeeLoginPageCPU extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ISwapeeLoginPage_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.SwapeeLoginPageElement}
 */
class SwapeeLoginPageElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeLoginPageBuffer}
 */
class SwapeeLoginPageBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageComputer}
 */
class AbstractSwapeeLoginPageComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.SwapeeLoginPageController}
 */
class SwapeeLoginPageController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeLoginPage = AbstractSwapeeLoginPage
module.exports.SwapeeLoginPagePort = SwapeeLoginPagePort
module.exports.AbstractSwapeeLoginPageController = AbstractSwapeeLoginPageController
module.exports.AbstractSwapeeLoginPageCPU = AbstractSwapeeLoginPageCPU
module.exports.SwapeeLoginPageElement = SwapeeLoginPageElement
module.exports.SwapeeLoginPageBuffer = SwapeeLoginPageBuffer
module.exports.AbstractSwapeeLoginPageComputer = AbstractSwapeeLoginPageComputer
module.exports.SwapeeLoginPageController = SwapeeLoginPageController

Object.defineProperties(module.exports, {
 'AbstractSwapeeLoginPage': {get: () => require('./precompile/internal')[97595863451]},
 [97595863451]: {get: () => module.exports['AbstractSwapeeLoginPage']},
 'SwapeeLoginPagePort': {get: () => require('./precompile/internal')[97595863453]},
 [97595863453]: {get: () => module.exports['SwapeeLoginPagePort']},
 'AbstractSwapeeLoginPageController': {get: () => require('./precompile/internal')[97595863454]},
 [97595863454]: {get: () => module.exports['AbstractSwapeeLoginPageController']},
 'AbstractSwapeeLoginPageCPU': {get: () => require('./precompile/internal')[97595863455]},
 [97595863455]: {get: () => module.exports['AbstractSwapeeLoginPageCPU']},
 'SwapeeLoginPageElement': {get: () => require('./precompile/internal')[97595863458]},
 [97595863458]: {get: () => module.exports['SwapeeLoginPageElement']},
 'SwapeeLoginPageBuffer': {get: () => require('./precompile/internal')[975958634511]},
 [975958634511]: {get: () => module.exports['SwapeeLoginPageBuffer']},
 'AbstractSwapeeLoginPageComputer': {get: () => require('./precompile/internal')[975958634530]},
 [975958634530]: {get: () => module.exports['AbstractSwapeeLoginPageComputer']},
 'SwapeeLoginPageController': {get: () => require('./precompile/internal')[975958634561]},
 [975958634561]: {get: () => module.exports['SwapeeLoginPageController']},
})