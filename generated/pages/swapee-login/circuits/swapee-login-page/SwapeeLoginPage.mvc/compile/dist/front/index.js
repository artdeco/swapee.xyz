/**
 * Display for presenting information from the _ISwapeeLoginPage_.
 * @extends {xyz.swapee.wc.SwapeeLoginPageDisplay}
 */
class SwapeeLoginPageDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.SwapeeLoginPageScreen}
 */
class SwapeeLoginPageScreen extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeLoginPageDisplay = SwapeeLoginPageDisplay
module.exports.SwapeeLoginPageScreen = SwapeeLoginPageScreen