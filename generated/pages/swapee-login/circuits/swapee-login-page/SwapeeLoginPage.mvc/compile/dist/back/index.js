/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPage` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPage}
 */
class AbstractSwapeeLoginPage extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeLoginPage_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeLoginPagePort}
 */
class SwapeeLoginPagePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageController}
 */
class AbstractSwapeeLoginPageController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageCPU` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageCPU}
 */
class AbstractSwapeeLoginPageCPU extends (class {/* lazy-loaded */}) {}
/**
 * The _ISwapeeLoginPage_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.SwapeeLoginPageHtmlComponent}
 */
class SwapeeLoginPageHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeLoginPageBuffer}
 */
class SwapeeLoginPageBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeLoginPageComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeLoginPageComputer}
 */
class AbstractSwapeeLoginPageComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.SwapeeLoginPageComputer}
 */
class SwapeeLoginPageComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.SwapeeLoginPageController}
 */
class SwapeeLoginPageController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeLoginPage = AbstractSwapeeLoginPage
module.exports.SwapeeLoginPagePort = SwapeeLoginPagePort
module.exports.AbstractSwapeeLoginPageController = AbstractSwapeeLoginPageController
module.exports.AbstractSwapeeLoginPageCPU = AbstractSwapeeLoginPageCPU
module.exports.SwapeeLoginPageHtmlComponent = SwapeeLoginPageHtmlComponent
module.exports.SwapeeLoginPageBuffer = SwapeeLoginPageBuffer
module.exports.AbstractSwapeeLoginPageComputer = AbstractSwapeeLoginPageComputer
module.exports.SwapeeLoginPageComputer = SwapeeLoginPageComputer
module.exports.SwapeeLoginPageController = SwapeeLoginPageController