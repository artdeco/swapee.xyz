import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {9759586345} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function g(a,b,e,f){return c["372700389812"](a,b,e,f,!1,void 0)};function k(){}k.prototype={s(){const {asIIntegratedController:{setInputs:a}}=this;a({g:!0})}};class aa{}class m extends g(aa,97595863458,null,{M:1,fa:2}){}m[d]=[k];

const n=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ba=n.StatefulLoadable,ca=n.IntegratedComponentInitialiser,p=n.IntegratedComponent,da=n["95173443851"],ea=n["95173443852"];
const q=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const r=q["615055805212"],fa=q["615055805218"],t=q["615055805233"],ha=q["615055805235"];const u={username:"14c4b",password:"5f4dc",g:"c5b5f",host:"67b3d",i:"dcd0a"};const v={ha:"94a08",U:"e32bb",user:"ee11c",m:"7da26"};function w(){}w.prototype={};function ia(){this.model={m:!1}}class ja{}class x extends g(ja,97595863457,ia,{G:1,$:2}){}function B(){}B.prototype={};function C(){this.model={host:"https://swapee.me/api",g:!1,i:!1,username:"",password:""}}class ka{}class D extends g(ka,97595863453,C,{J:1,da:2}){}D[d]=[B,{constructor(){t(this.model,"",u);t(this.model,"",v)}}];x[d]=[{},w,D];function E(){}E.prototype={};class la{}class F extends g(la,975958634531,null,{D:1,X:2}){}F[d]=[E,{},ba,x];function G(){}G.prototype={};class ma{}class H extends g(ma,97595863451,null,{F:1,Y:2}){}H[d]=[G,fa];const I={regulate:r({host:String,g:Boolean,i:Boolean,username:String,password:String})};
const J=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const na=J.IntegratedController,oa=J.Parametric;const K={...u};function L(){}L.prototype={};function pa(){const a={model:null};C.call(a);this.inputs=a.model}class qa{}class M extends g(qa,97595863455,pa,{K:1,ea:2}){}function N(){}M[d]=[N.prototype={},L,oa,N.prototype={constructor(){t(this.inputs,"",K)}}];function O(){}O.prototype={};class ra{}class P extends g(ra,975958634519,null,{j:1,Z:2}){}function Q(){}P[d]=[Q.prototype={},O,{calibrate:[function({g:a}){a&&setTimeout(()=>{const {j:{setInputs:b}}=this;b({g:!1})},1)},function({i:a}){a&&setTimeout(()=>{const {j:{setInputs:b}}=this;b({i:!1})},1)}]},I,na,{get Port(){return M}},Q.prototype={v(a){const {j:{setInputs:b}}=this;b({username:a})},u(a){const {j:{setInputs:b}}=this;b({password:a})}}];const sa=r({m:Boolean},{silent:!0});const ta=Object.keys(v).reduce((a,b)=>{a[v[b]]=b;return a},{});function R(){}R.prototype={};class ua{}class S extends g(ua,97595863459,null,{A:1,W:2}){}S[d]=[R,F,m,p,H,P,{regulateState:sa,stateQPs:ta}];function va({host:a}){return{host:a}};require(eval('"@type.engineering/web-computing"'));const wa=require(eval('"@type.engineering/web-computing"')).h;function xa(){return wa("div",{$id:"SwapeeLoginPage"})};require(eval('"@type.engineering/web-computing"'));const T=require(eval('"@type.engineering/web-computing"')).h;function ya({username:a,password:b,error:e,errorText:f,loading:h,m:l},{s:y,v:z,u:A}){return T("div",{$id:"SwapeeLoginPage"},T("input",{$id:"UsernameIn",onInput:z}),T("input",{$id:"PasswordIn",onInput:A}),T("span",{$id:"CredentialsErrorLa",$reveal:e&&l}),T("span",{$id:"ErrorLa",$reveal:e&&!l},f),T("button",{$id:"LoginBu",disabled:!a||!b||h,onClick:y}))};var U=class extends P.implements(){};require("https");require("http");const za=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}za("aqt");require("fs");require("child_process");
const V=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const Aa=V.ElementBase,Ba=V.HTMLBlocker;require(eval('"@type.engineering/web-computing"'));const W=require(eval('"@type.engineering/web-computing"')).h;function X(){}X.prototype={};function Ca(){this.inputs={noSolder:!1,R:{},P:{},ia:{},V:{},T:{},ga:{}}}class Da{}class Ea extends g(Da,975958634513,Ca,{I:1,ba:2}){}Ea[d]=[X,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"error-la-opts":void 0,"credentials-error-la-opts":void 0,"username-in-opts":void 0,"password-in-opts":void 0,"login-bu-opts":void 0,"swapee-me-opts":void 0,"sign-in":void 0,"signed-in":void 0})}}];function Fa(){}Fa.prototype={};class Ga{}class Y extends g(Ga,975958634512,null,{H:1,aa:2}){}function Z(){}
Y[d]=[Fa,Aa,Z.prototype={calibrate:function({":no-solder":a,":host":b,":sign-in":e,":signed-in":f,":username":h,":password":l}){const {attributes:{"no-solder":y,host:z,"sign-in":A,"signed-in":Ha,username:Ia,password:Ja}}=this;return{...(void 0===y?{"no-solder":a}:{}),...(void 0===z?{host:b}:{}),...(void 0===A?{"sign-in":e}:{}),...(void 0===Ha?{"signed-in":f}:{}),...(void 0===Ia?{username:h}:{}),...(void 0===Ja?{password:l}:{})}}},Z.prototype={calibrate:({"no-solder":a,host:b,"sign-in":e,"signed-in":f,
username:h,password:l})=>({noSolder:a,host:b,g:e,i:f,username:h,password:l})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return W("div",{$id:"SwapeeLoginPage"},W("vdu",{$id:"UsernameIn"}),W("vdu",{$id:"PasswordIn"}),W("vdu",{$id:"CredentialsErrorLa"}),W("vdu",{$id:"ErrorLa"}),W("vdu",{$id:"LoginBu"}))}},Z.prototype={classes:{Label:"b021d"},inputsPQs:K,queriesPQs:{l:"50b0c"},cachePQs:v,vdus:{UsernameIn:"g54e1",LoginBu:"g54e2",PasswordIn:"g54e3",ErrorLa:"g54e4",
SwapeeMe:"g54e5",CredentialsErrorLa:"g54e7"}},p,ea,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder host signIn signedIn username password query:swapee-me no-solder :no-solder :host sign-in :sign-in signed-in :signed-in :username :password fe646 47d7d fe0e9 fbdef 2de57 c941f 4ccfe 67b3d c5b5f dcd0a 14c4b 5f4dc children".split(" "))})},get Port(){return Ea},calibrate:function({"query:swapee-me":a}){const b={};a&&(b.l=a);return b}},ha,Z.prototype={constructor(){this.land=
{o:null}}},Z.prototype={calibrate:async function({l:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:e},asIElement:{fqn:f}}=this,h=await b(a);if(!h)return console.warn("\\u2757\\ufe0f swapeeMeSel %s must be present on the page for %s to work",a,f),{};e.o=h}},Z.prototype={solder:(a,{l:b})=>({l:b})}];Y[d]=[S,{rootId:"SwapeeLoginPage",__$id:9759586345,fqn:"xyz.swapee.wc.ISwapeeLoginPage",maurice_element_v3:!0}];class Ka extends Y.implements(U,da,Ba,ca,{allocator:function(){}},{solder:va,server:xa,render:ya},{classesMap:!0,rootSelector:".SwapeeLoginPage",stylesheet:"html/styles/SwapeeLoginPage.css",blockName:"html/SwapeeLoginPageBlock.html"},{}){};module.exports["97595863450"]=S;module.exports["97595863451"]=S;module.exports["97595863453"]=M;module.exports["97595863454"]=P;module.exports["97595863455"]=F;module.exports["97595863458"]=Ka;module.exports["975958634511"]=I;module.exports["975958634530"]=H;module.exports["975958634561"]=U;
/*! @embed-object-end {9759586345} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule