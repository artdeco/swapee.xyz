import {StatefulLoader} from '@mauriceguest/guest2'
import __$constructor from './methods/__$constructor'
import SwapeeLoginPageHtmlController from '../SwapeeLoginPageHtmlController'
import SwapeeLoginPageHtmlComputer from '../SwapeeLoginPageHtmlComputer'
import SwapeeLoginPageVirtualDisplay from '../SwapeeLoginPageVirtualDisplay'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractSwapeeLoginPageHtmlComponent} from '../../gen/AbstractSwapeeLoginPageHtmlComponent'

/** @extends {xyz.swapee.wc.SwapeeLoginPageHtmlComponent} */
export default class extends AbstractSwapeeLoginPageHtmlComponent.implements(
 SwapeeLoginPageHtmlController,
 SwapeeLoginPageHtmlComputer,
 SwapeeLoginPageVirtualDisplay,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ISwapeeLoginPageHtmlComponent} */ ({
  __$constructor:__$constructor,
 }),

  StatefulLoader,
){}