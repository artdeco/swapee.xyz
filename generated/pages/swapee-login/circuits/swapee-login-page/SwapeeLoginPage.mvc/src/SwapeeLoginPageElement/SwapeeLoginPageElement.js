import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import SwapeeLoginPageServerController from '../SwapeeLoginPageServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractSwapeeLoginPageElement from '../../gen/AbstractSwapeeLoginPageElement'

/** @extends {xyz.swapee.wc.SwapeeLoginPageElement} */
export default class SwapeeLoginPageElement extends AbstractSwapeeLoginPageElement.implements(
 SwapeeLoginPageServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ISwapeeLoginPageElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ISwapeeLoginPageElement}*/({
   classesMap: true,
   rootSelector:     `.SwapeeLoginPage`,
   stylesheet:       'html/styles/SwapeeLoginPage.css',
   blockName:        'html/SwapeeLoginPageBlock.html',
  }),
  /**@type {xyz.swapee.wc.ISwapeeLoginPageDesigner}*/({
  }),
){}

// thank you for using web circuits
