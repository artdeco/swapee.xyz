/** @type {xyz.swapee.wc.ISwapeeLoginPageElement._render} */
export default function SwapeeLoginPageRender({
 username:username,password:password,error:error,errorText:errorText,
 loading:loading,credentialsError,
},{pulseSignIn,setUsername,setPassword}) {
 return (<div $id="SwapeeLoginPage">
  {/* todo: automatically apply the irq actions to this */}
  <input $id="UsernameIn" onInput={setUsername} />
  <input $id="PasswordIn" onInput={setPassword} />

  <span $id="CredentialsErrorLa" $reveal={error&&credentialsError} />
  <span $id="ErrorLa" $reveal={error&&!credentialsError}>{errorText}</span>

  <button $id="LoginBu" disabled={!username||!password||loading} onClick={pulseSignIn} />
 </div>)
}