/** @type {!com.webcircuits.IPortFrontend._deduceInputs} */
export default function deduceInputs(el) {
 const{asISwapeeLoginPageDisplay:{
  UsernameIn:UsernameIn,PasswordIn:PasswordIn,
 }}=/**@type {!xyz.swapee.wc.ISwapeeLoginPageScreen}*/(this)
 return {
  username:UsernameIn.value,
  password:PasswordIn.value,
 }
}