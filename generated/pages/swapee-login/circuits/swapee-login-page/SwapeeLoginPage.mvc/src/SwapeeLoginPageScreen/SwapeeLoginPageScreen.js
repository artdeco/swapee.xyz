import deduceInputs from './methods/deduce-inputs'
import AbstractSwapeeLoginPageControllerAT from '../../gen/AbstractSwapeeLoginPageControllerAT'
import SwapeeLoginPageDisplay from '../SwapeeLoginPageDisplay'
import AbstractSwapeeLoginPageScreen from '../../gen/AbstractSwapeeLoginPageScreen'

/** @extends {xyz.swapee.wc.SwapeeLoginPageScreen} */
export default class extends AbstractSwapeeLoginPageScreen.implements(
 AbstractSwapeeLoginPageControllerAT,
 SwapeeLoginPageDisplay,
 /**@type {!xyz.swapee.wc.ISwapeeLoginPageScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ISwapeeLoginPageScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:9759586345,
 }),
){}