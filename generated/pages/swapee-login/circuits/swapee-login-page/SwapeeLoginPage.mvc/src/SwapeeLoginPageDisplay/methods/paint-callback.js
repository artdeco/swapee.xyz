/** @type {xyz.swapee.wc.ISwapeeLoginPageDisplay._paintCallback} */
export default function paintCallback(){
 if(!window.opener) return
 const cb=window.opener['_swapee_me_cb']
 if(!cb) return
 setTimeout(()=>{
  cb()
  window.close()
 },1)
}