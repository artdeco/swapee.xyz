import paintCallback from './methods/paint-callback'
import prepaintCallback from '../../../parts/40-display/src/methods/prepaint-callback'
import AbstractSwapeeLoginPageDisplay from '../../gen/AbstractSwapeeLoginPageDisplay'

/** @extends {xyz.swapee.wc.SwapeeLoginPageDisplay} */
export default class extends AbstractSwapeeLoginPageDisplay.implements(
 /**@type {!xyz.swapee.wc.ISwapeeLoginPageDisplay}*/({
  paint:[
   prepaintCallback,
  ],
 }),
 /** @type {!xyz.swapee.wc.ISwapeeLoginPageDisplay} */ ({
  paintCallback:paintCallback,
 }),
){}