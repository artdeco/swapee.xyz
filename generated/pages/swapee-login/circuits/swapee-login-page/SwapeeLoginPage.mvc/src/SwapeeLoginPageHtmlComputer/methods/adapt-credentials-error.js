/** @type {xyz.swapee.wc.ISwapeeLoginPageComputer._adaptCredentialsError} */
export default function adaptCredentialsError({errorText}) {
 if(!errorText) return {credentialsError:false}
 return{
  credentialsError:/401/.test(errorText),
 }
}