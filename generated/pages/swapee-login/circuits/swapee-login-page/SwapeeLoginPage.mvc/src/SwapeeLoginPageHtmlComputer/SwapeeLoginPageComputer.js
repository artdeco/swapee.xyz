import adaptClearError from './methods/adapt-clear-error'
import adaptCredentialsError from './methods/adapt-credentials-error'
import adaptLogin from './methods/adapt-login'
import {preadaptClearError,preadaptCredentialsError,preadaptLogin} from '../../gen/AbstractSwapeeLoginPageComputer/preadapters'
import AbstractSwapeeLoginPageComputer from '../../gen/AbstractSwapeeLoginPageComputer'

/** @extends {xyz.swapee.wc.SwapeeLoginPageComputer} */
export default class SwapeeLoginPageHtmlComputer extends AbstractSwapeeLoginPageComputer.implements(
 /** @type {!xyz.swapee.wc.ISwapeeLoginPageComputer} */ ({
  adaptClearError:adaptClearError,
  adaptCredentialsError:adaptCredentialsError,
  adaptLogin:adaptLogin,
  adapt:[preadaptClearError,preadaptCredentialsError,preadaptLogin],
 }),
){}