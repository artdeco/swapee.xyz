/** @type {xyz.swapee.wc.ISwapeeLoginPageComputer._adaptLogin} */
export default async function adaptLogin({host:host,username:username,password:password}) {
 const {
  asIStatefulLoader:{
   fetchJSON:fetchJSON, // this can be applied to the best rates too via onramper
  },
 }=/**@type {!xyz.swapee.wc.ISwapeeLoginPageComputer}*/(this)
 const data=await fetchJSON(`${host}/login`,{
  method:'POST',
  headers:{
   'Content-Type':'application/json',
  },
  body:JSON.stringify({
   'username':username,
   'password':password,
  }),
 })
 if(!data) return {}

 const{'token':token,'user':{
  // 'address':address,
  // 'banner':banner,
  // 'cipher':cipher,
  // 'currencies':currencies,
  // 'currency':currency,
  'display':display,
  // 'email':email,
  // 'fiat':fiat,
  // 'haspin':haspin,
  'id':id,
  // 'language':language,
  // 'profile':profile,
  // 'prompt':prompt,
  // 'pubkey':pubkey,
  // 'salt':salt,
  'username':_username,
  'profile':profile,
 }}=data

 return {
  signedIn:true,
  token:token,
  username:_username,
  userid:id,
  displayName:display,
  profile:profile,
  // user:{
  //  username:_username,
  //  display:display,
  //  id:id,
  // },
 }
 //    {
 //     "address": "what's going on ",
 //     "banner": "ba1e038d-02c8-45d0-8cd3-0e8d69c7e4c9-banner",
 //     "cipher": "en1ks6dnq5x2hc2hh0y7ua49mhke4n843zjz4u5e0077z6z90lnyyws8famnh",
 //     "currencies": [
 //         "USD",
 //         "CAD"
 //     ],
 //     "currency": "GBP",
 //     "display": "neda",
 //     "email": "",
 //     "fiat": false,
 //     "haspin": false,
 //     "id": "ba1e038d-02c8-45d0-8cd3-0e8d69c7e4c9",
 //     "language": "en",
 //     "profile": "eb497436af8df5c34db28fbb78c387585d58773b980b36b09bc3de2f9e6f99f5",
 //     "prompt": false,
 //     "pubkey": "dd9d0749bd1e3b39c4e926e8de6cb2edca4530e40c1799d13478d6140063daa3",
 //     "salt": "335ba7609b2efdc2851f70a111d52c1e",
 //     "username": "neda"
 // }
 // hello world
 // return {
 //  lastUpdated: new Date,
 //  tickersList: tickersList,
 // }
 // await log using the JSON loader
}