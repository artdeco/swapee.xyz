import {Painter} from '@webcircuits/webcircuits'

const SwapeeLoginPageVirtualDisplay=Painter.__trait(
 /**@type {!xyz.swapee.wc.ISwapeeLoginPageVirtualDisplay}*/({
  paint:[
   function paintCallback({},{SwapeeMe:SwapeeMe}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _lan={}
    if(SwapeeMe) {
     const{'94a08':token}=SwapeeMe.model
     _lan['4c807']={'94a08':token}
    }else _lan['4c807']={}
    if(!_lan['4c807']['94a08']) return
    const _mem=serMemory({})
    t_pa({
     pid:'463af76',
     mem:_mem,
     lan:_lan,
    })
   },
  ],
 }),
)
export default SwapeeLoginPageVirtualDisplay