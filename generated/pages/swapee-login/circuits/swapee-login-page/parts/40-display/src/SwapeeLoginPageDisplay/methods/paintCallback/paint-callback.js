import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ISwapeeLoginPageDisplay._paintCallback} */
export default function paintCallback(){
 if(!window.opener) return
 const cb=window.opener['_swapee_me_cb']
 if(!cb) return
 setTimeout(()=>{
  cb()
  window.close()
 },1)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3BhZ2VzL3N3YXBlZS1sb2dpbi9jaXJjdWl0cy9zd2FwZWUtbG9naW4tcGFnZS9zd2FwZWUtbG9naW4tcGFnZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBdUhHLFNBQVMsYUFBYSxDQUFDO0NBQ3RCLEVBQUUsQ0FBQyxPQUFPLENBQUMsUUFBUTtDQUNuQixNQUFNLEVBQUUsQ0FBQyxNQUFNLENBQUM7Q0FDaEIsRUFBRSxDQUFDLEtBQUs7Q0FDUixVQUFVLENBQUMsQ0FBQztFQUNYLEVBQUUsQ0FBQztFQUNILE1BQU0sQ0FBQyxLQUFLLENBQUM7QUFDZixHQUFHO0FBQ0gsQ0FBRiJ9