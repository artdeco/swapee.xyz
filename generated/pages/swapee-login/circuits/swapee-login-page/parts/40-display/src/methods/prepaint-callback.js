/**@type {xyz.swapee.wc.ISwapeeLoginPageDisplay._paintCallback} */
export default function prepaintCallback(memory,land) {
 const{asISwapeeLoginPageDisplay:{paintCallback:paintCallback}}=this

 paintCallback(memory,null)
}
prepaintCallback['_id']='463af76'