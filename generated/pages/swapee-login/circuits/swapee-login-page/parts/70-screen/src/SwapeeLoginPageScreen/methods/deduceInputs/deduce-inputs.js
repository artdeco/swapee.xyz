import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ISwapeeLoginPageScreen._deduceInputs} */
export default function deduceInputs(el) {
 const{asISwapeeLoginPageDisplay:{
  UsernameIn:UsernameIn,PasswordIn:PasswordIn,
 }}=this
 return {
  username:UsernameIn.value,
  password:PasswordIn.value,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3BhZ2VzL3N3YXBlZS1sb2dpbi9jaXJjdWl0cy9zd2FwZWUtbG9naW4tcGFnZS9zd2FwZWUtbG9naW4tcGFnZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBNEdHLFNBQVMsWUFBWSxDQUFDO0NBQ3JCLE1BQU07RUFDTCxxQkFBcUIsQ0FBQyxxQkFBcUI7QUFDN0MsSUFBSTtDQUNIO0VBQ0MsbUJBQW1CLENBQUMsS0FBSztFQUN6QixtQkFBbUIsQ0FBQyxLQUFLO0FBQzNCO0FBQ0EsQ0FBRiJ9