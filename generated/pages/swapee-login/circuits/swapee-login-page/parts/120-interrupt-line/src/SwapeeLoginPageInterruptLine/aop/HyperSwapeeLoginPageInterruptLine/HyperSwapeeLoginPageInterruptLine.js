import AbstractHyperSwapeeLoginPageInterruptLine from '../../../../gen/AbstractSwapeeLoginPageInterruptLine/hyper/AbstractHyperSwapeeLoginPageInterruptLine'
import SwapeeLoginPageInterruptLine from '../../SwapeeLoginPageInterruptLine'
import SwapeeLoginPageInterruptLineGeneralAspects from '../SwapeeLoginPageInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperSwapeeLoginPageInterruptLine} */
export default class extends AbstractHyperSwapeeLoginPageInterruptLine
 .consults(
  SwapeeLoginPageInterruptLineGeneralAspects,
 )
 .implements(
  SwapeeLoginPageInterruptLine,
 )
{}