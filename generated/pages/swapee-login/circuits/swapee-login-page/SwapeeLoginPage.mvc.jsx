import {StatefulLoadable} from '@mauriceguest/guest2'

/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPage} */
export default class AbstractSwapeeLoginPage extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPageComputer} */
export class AbstractSwapeeLoginPageComputer extends (<computer>
   <adapter name="adaptClearError">
    <xyz.swapee.wc.ISwapeeLoginPageCore username password />
    <outputs>
     <xyz.swapee.wc.ISwapeeLoginPageCore error />
    </outputs>
   </adapter>
   <adapter name="adaptCredentialsError">
    <xyz.swapee.wc.ISwapeeLoginPageCore errorText />
    <outputs>
     <xyz.swapee.wc.ISwapeeLoginPageCore credentialsError />
    </outputs>
   </adapter>
   <adapter name="adaptLogin" async>
    <xyz.swapee.wc.ISwapeeLoginPageCore host="required" username="required" password="required" signIn="required" />
    <outputs>
     <xyz.swapee.wc.ISwapeeLoginPageCore signedIn />
     <xyz.swapee.wc.ISwapeeMePort token username userid displayName profile />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPageCPU} */
export class AbstractSwapeeLoginPageCPU extends (<cpu>


  <cores>
   <guest.maurice.IStatefulLoadable.State $={StatefulLoadable} />
  </cores>
</cpu>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPageController} */
export class AbstractSwapeeLoginPageController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPagePort} */
export class SwapeeLoginPagePort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPageView} */
export class AbstractSwapeeLoginPageView extends (<view>
  <classes>
   <string opt name="Label">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPageElement} */
export class AbstractSwapeeLoginPageElement extends (<element v3 html mv>
 <block src="./SwapeeLoginPage.mvc/src/SwapeeLoginPageElement/methods/render.jsx" />
 <inducer src="./SwapeeLoginPage.mvc/src/SwapeeLoginPageElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeLoginPageHtmlComponent} */
export class AbstractSwapeeLoginPageHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.ISwapeeMe via="SwapeeMe" />
  </connectors>

</html-ic>) { }
// </class-end>