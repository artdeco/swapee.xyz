import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorPageViewUniversal}
 */
function __OffersAggregatorPageViewUniversal() {}
__OffersAggregatorPageViewUniversal.prototype = /** @type {!_OffersAggregatorPageViewUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal}
 */
class _OffersAggregatorPageViewUniversal { }
/**
 * A trait that allows to access an _IOffersAggregatorPageView_ object via a `.offersAggregatorPageView` field.
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal} ‎
 */
class OffersAggregatorPageViewUniversal extends newAbstract(
 _OffersAggregatorPageViewUniversal,'UOffersAggregatorPageView',null,{
  asIOffersAggregatorPageView:1,
  asUOffersAggregatorPageView:3,
  offersAggregatorPageView:4,
  asOffersAggregatorPageView:5,
  superOffersAggregatorPageViewUniversal:2,
 },false,{
  offersAggregatorPageView:{_offersAggregatorPageView:'_offersAggregatorPageView'},
 }) {}

/** @type {typeof xyz.swapee.rc.UOffersAggregatorPageView} */
OffersAggregatorPageViewUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UOffersAggregatorPageView} */
function OffersAggregatorPageViewUniversalClass(){}

export default OffersAggregatorPageViewUniversal

/** @type {xyz.swapee.rc.UOffersAggregatorPageView.Symbols} */
export const OffersAggregatorPageViewUniversalSymbols=OffersAggregatorPageViewUniversal.Symbols
/** @type {xyz.swapee.rc.UOffersAggregatorPageView.getSymbols} */
export const getOffersAggregatorPageViewUniversalSymbols=OffersAggregatorPageViewUniversal.getSymbols
/** @type {xyz.swapee.rc.UOffersAggregatorPageView.setSymbols} */
export const setOffersAggregatorPageViewUniversalSymbols=OffersAggregatorPageViewUniversal.setSymbols

OffersAggregatorPageViewUniversal[$implementations]=[
 __OffersAggregatorPageViewUniversal,
 OffersAggregatorPageViewUniversalClass.prototype=/**@type {!xyz.swapee.rc.UOffersAggregatorPageView}*/({
  [OffersAggregatorPageViewUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UOffersAggregatorPageView} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UOffersAggregatorPageView.Initialese}*/({
   offersAggregatorPageView:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.OffersAggregatorPageViewMetaUniversal}*/
export const OffersAggregatorPageViewMetaUniversal=OffersAggregatorPageViewUniversal.MetaUniversal