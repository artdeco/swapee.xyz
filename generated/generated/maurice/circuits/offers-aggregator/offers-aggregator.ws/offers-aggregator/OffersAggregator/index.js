require('./types')
module.exports={
 toString(){return 'offersAggregator'},
 get OffersAggregatorPage(){
  const d=Date.now();
  const R=require('./src/HyperOffersAggregatorPage');
  console.log("✅ Loaded dev %s in %s",'OffersAggregator',Date.now()-d,'ms');
  return R
 },
 get offersAggregatorMethodsIds() {
  const methodsIds={}
  const o=require('./gen/methods-ids')
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') methodsIds[key]=val
  }
  return methodsIds
 },
 get offersAggregatorArcsIds() {
  const arcsIds={}
  let o={}
  try{o=require('./gen/arcs-ids')}catch(err){}
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') arcsIds[val]=key
  }
  return arcsIds
 },
 get getOffersAggregator(){
  const GET=require('./src/api/getOffersAggregator/getOffersAggregator')
  return GET
 },
 get OffersAggregatorNav() {
  return require('./src/navs/offersAggregator')
 },
 get FilterChangellyFloatingOfferNav() {
  return require('./src/navs/filterChangellyFloatingOffer')
 },
 get FilterChangellyFixedOfferNav() {
  return require('./src/navs/filterChangellyFixedOffer')
 },
 get FilterLetsExchangeFloatingOfferNav() {
  return require('./src/navs/filterLetsExchangeFloatingOffer')
 },
 get FilterLetsExchangeFixedOfferNav() {
  return require('./src/navs/filterLetsExchangeFixedOffer')
 },
 get FilterChangeNowFloatingOfferNav() {
  return require('./src/navs/filterChangeNowFloatingOffer')
 },
 get FilterChangeNowFixedOfferNav() {
  return require('./src/navs/filterChangeNowFixedOffer')
 },
}