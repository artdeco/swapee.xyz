/**
 * @fileoverview
 * @externs
 */

xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel={}
xyz.swapee.rc.IOffersAggregatorPageView={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.rc={}
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel={}
$$xyz.swapee.rc.IOffersAggregatorPageView={}
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel={}
$$xyz.swapee.rc.IOffersAggregatorPage={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.Initialese  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 * @extends {io.letsexchange.ULetsExchange.Initialese}
 * @extends {io.changenow.UChangeNow.Initialese}
 */
xyz.swapee.rc.IOffersAggregatorPage.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageFields  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageFields
/** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} */
xyz.swapee.rc.IOffersAggregatorPageFields.prototype.offersAggregatorCtx

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageCaster  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageCaster
/** @type {!xyz.swapee.rc.BoundIOffersAggregatorPage} */
xyz.swapee.rc.IOffersAggregatorPageCaster.prototype.asIOffersAggregatorPage
/** @type {!xyz.swapee.rc.BoundOffersAggregatorPage} */
xyz.swapee.rc.IOffersAggregatorPageCaster.prototype.superOffersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPageFields  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.UOffersAggregatorPageFields
/** @type {xyz.swapee.rc.IOffersAggregatorPage} */
xyz.swapee.rc.UOffersAggregatorPageFields.prototype.offersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPageCaster  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.UOffersAggregatorPageCaster
/** @type {!xyz.swapee.rc.BoundOffersAggregatorPage} */
xyz.swapee.rc.UOffersAggregatorPageCaster.prototype.asOffersAggregatorPage
/** @type {!xyz.swapee.rc.BoundUOffersAggregatorPage} */
xyz.swapee.rc.UOffersAggregatorPageCaster.prototype.asUOffersAggregatorPage
/** @type {!xyz.swapee.rc.BoundOffersAggregatorPageUniversal} */
xyz.swapee.rc.UOffersAggregatorPageCaster.prototype.superOffersAggregatorPageUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @extends {xyz.swapee.rc.UOffersAggregatorPageFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.UOffersAggregatorPageCaster}
 */
xyz.swapee.rc.UOffersAggregatorPage = function() {}
/** @param {...!xyz.swapee.rc.UOffersAggregatorPage.Initialese} init */
xyz.swapee.rc.UOffersAggregatorPage.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @extends {xyz.swapee.rc.IOffersAggregatorPageFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageCaster}
 * @extends {com.changelly.UChangelly}
 * @extends {io.letsexchange.ULetsExchange}
 * @extends {io.changenow.UChangeNow}
 * @extends {xyz.swapee.rc.UOffersAggregatorPage}
 */
xyz.swapee.rc.IOffersAggregatorPage = function() {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPage.Initialese} init */
xyz.swapee.rc.IOffersAggregatorPage.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx
 * @return {(undefined|!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)}
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.offersAggregator = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)}
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangellyFloatingOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)}
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangellyFixedOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)}
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterLetsExchangeFloatingOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)}
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterLetsExchangeFixedOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)}
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangeNowFloatingOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)}
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangeNowFixedOffer = function(form, answers, validation, errors, ctx) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IOffersAggregatorPage.Initialese} init
 * @implements {xyz.swapee.rc.IOffersAggregatorPage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPage.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPage = function(...init) {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPage.Initialese} init */
xyz.swapee.rc.OffersAggregatorPage.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.OffersAggregatorPage.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage = function() {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)|!xyz.swapee.rc.IOffersAggregatorPageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)|!xyz.swapee.rc.IOffersAggregatorPageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)|!xyz.swapee.rc.IOffersAggregatorPageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterOffersAggregatorThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterOffersAggregatorCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeEachOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregatorReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFloatingOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFloatingOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeEachFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterLetsExchangeFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFixedOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterLetsExchangeFixedOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeEachFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterLetsExchangeFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFloatingOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFloatingOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeEachFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangeNowFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFixedOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterFilterChangeNowFixedOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.beforeEachFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOfferReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.afterEachFilterChangeNowFixedOfferReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageJoinpointModelHyperslice  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice}
 */
xyz.swapee.rc.OffersAggregatorPageJoinpointModelHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterOffersAggregatorThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterOffersAggregatorCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeEachOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFloatingOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFloatingOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterLetsExchangeFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFixedOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterLetsExchangeFixedOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterLetsExchangeFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFloatingOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFloatingOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangeNowFloatingOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFixedOfferThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFixedOfferReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterFilterChangeNowFixedOfferCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangeNowFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangeNowFixedOfferReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageJoinpointModelBindingHyperslice  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.OffersAggregatorPageJoinpointModelBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregatorThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregatorReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregatorCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachOffersAggregatorReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterLetsExchangeFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterLetsExchangeFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterLetsExchangeFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterLetsExchangeFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangeNowFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangeNowFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangeNowFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangeNowFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFixedOfferReturns = function(data) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageJoinpointModel  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel}
 */
xyz.swapee.rc.OffersAggregatorPageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.RecordIOffersAggregatorPageJoinpointModel  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ beforeOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeOffersAggregator, afterOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregator, afterOffersAggregatorThrows: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorThrows, afterOffersAggregatorReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorReturns, afterOffersAggregatorCancels: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorCancels, beforeEachOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachOffersAggregator, afterEachOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregator, afterEachOffersAggregatorReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregatorReturns, beforeFilterChangellyFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFloatingOffer, afterFilterChangellyFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOffer, afterFilterChangellyFloatingOfferThrows: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferThrows, afterFilterChangellyFloatingOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferReturns, afterFilterChangellyFloatingOfferCancels: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferCancels, beforeEachFilterChangellyFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFloatingOffer, afterEachFilterChangellyFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOffer, afterEachFilterChangellyFloatingOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns, beforeFilterChangellyFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFixedOffer, afterFilterChangellyFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOffer, afterFilterChangellyFixedOfferThrows: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferThrows, afterFilterChangellyFixedOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferReturns, afterFilterChangellyFixedOfferCancels: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferCancels, beforeEachFilterChangellyFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFixedOffer, afterEachFilterChangellyFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOffer, afterEachFilterChangellyFixedOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns, beforeFilterLetsExchangeFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFloatingOffer, afterFilterLetsExchangeFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOffer, afterFilterLetsExchangeFloatingOfferThrows: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferThrows, afterFilterLetsExchangeFloatingOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferReturns, afterFilterLetsExchangeFloatingOfferCancels: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferCancels, beforeEachFilterLetsExchangeFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFloatingOffer, afterEachFilterLetsExchangeFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOffer, afterEachFilterLetsExchangeFloatingOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOfferReturns, beforeFilterLetsExchangeFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFixedOffer, afterFilterLetsExchangeFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOffer, afterFilterLetsExchangeFixedOfferThrows: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferThrows, afterFilterLetsExchangeFixedOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferReturns, afterFilterLetsExchangeFixedOfferCancels: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferCancels, beforeEachFilterLetsExchangeFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFixedOffer, afterEachFilterLetsExchangeFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOffer, afterEachFilterLetsExchangeFixedOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOfferReturns, beforeFilterChangeNowFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFloatingOffer, afterFilterChangeNowFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOffer, afterFilterChangeNowFloatingOfferThrows: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferThrows, afterFilterChangeNowFloatingOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferReturns, afterFilterChangeNowFloatingOfferCancels: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferCancels, beforeEachFilterChangeNowFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFloatingOffer, afterEachFilterChangeNowFloatingOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOffer, afterEachFilterChangeNowFloatingOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOfferReturns, beforeFilterChangeNowFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFixedOffer, afterFilterChangeNowFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOffer, afterFilterChangeNowFixedOfferThrows: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferThrows, afterFilterChangeNowFixedOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferReturns, afterFilterChangeNowFixedOfferCancels: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferCancels, beforeEachFilterChangeNowFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFixedOffer, afterEachFilterChangeNowFixedOffer: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOffer, afterEachFilterChangeNowFixedOfferReturns: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOfferReturns }} */
xyz.swapee.rc.RecordIOffersAggregatorPageJoinpointModel

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundIOffersAggregatorPageJoinpointModel  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIOffersAggregatorPageJoinpointModel}
 */
xyz.swapee.rc.BoundIOffersAggregatorPageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundOffersAggregatorPageJoinpointModel  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIOffersAggregatorPageJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundOffersAggregatorPageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeOffersAggregator  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregator  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorThrows  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorCancels  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachOffersAggregator  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregator  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregatorReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregatorReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregatorReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferThrows  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferCancels  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferThrows  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferCancels  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferThrows  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferCancels  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferThrows  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferCancels  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferThrows  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferCancels  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferThrows  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferCancels  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOfferReturns  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller = function() {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese} init */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterOffersAggregatorThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterOffersAggregatorReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterOffersAggregatorCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeEachOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachOffersAggregatorReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFloatingOfferThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFloatingOfferCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeEachFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangellyFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFixedOfferThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangellyFixedOfferCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeEachFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangellyFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeFilterLetsExchangeFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFloatingOfferThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFloatingOfferCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeEachFilterLetsExchangeFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterLetsExchangeFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterLetsExchangeFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeFilterLetsExchangeFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFixedOfferThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterLetsExchangeFixedOfferCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeEachFilterLetsExchangeFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterLetsExchangeFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterLetsExchangeFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeFilterChangeNowFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFloatingOfferThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFloatingOfferCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeEachFilterChangeNowFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangeNowFloatingOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangeNowFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeFilterChangeNowFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFixedOfferThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterFilterChangeNowFixedOfferCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.beforeEachFilterChangeNowFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangeNowFixedOffer
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.afterEachFilterChangeNowFixedOfferReturns
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.offersAggregator = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.filterChangellyFloatingOffer = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.filterChangellyFixedOffer = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.filterLetsExchangeFloatingOffer = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.filterLetsExchangeFixedOffer = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.filterChangeNowFloatingOffer = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.filterChangeNowFixedOffer = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageAspectsInstaller  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPageAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese} init */
xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller = function() {}
/**
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageAspectsInstallerConstructor  a7c66343ca2b0911b260ff795094988e */
/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller, ...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese)} */
xyz.swapee.rc.OffersAggregatorPageAspectsInstallerConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.OffersAggregatorNArgs  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ form: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx }} */
xyz.swapee.rc.IOffersAggregatorPage.OffersAggregatorNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPage.OffersAggregatorNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPage.OffersAggregatorNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData.prototype.res
/**
 * @param {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx }} */
xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFixedOfferNArgs  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx }} */
xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFixedOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFixedOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFixedOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ form: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx }} */
xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ form: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx }} */
xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx }} */
xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx }} */
xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageConstructor  a7c66343ca2b0911b260ff795094988e */
/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage, ...!xyz.swapee.rc.IOffersAggregatorPage.Initialese)} */
xyz.swapee.rc.OffersAggregatorPageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageMetaUniversal  a7c66343ca2b0911b260ff795094988e */
/** @typedef {symbol} */
xyz.swapee.rc.OffersAggregatorPageMetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPage.Initialese  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.UOffersAggregatorPage.Initialese = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage|undefined} */
xyz.swapee.rc.UOffersAggregatorPage.Initialese.prototype.offersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageUniversal  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.UOffersAggregatorPage.Initialese} init
 * @implements {xyz.swapee.rc.UOffersAggregatorPage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UOffersAggregatorPage.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPageUniversal = function(...init) {}
/** @param {...!xyz.swapee.rc.UOffersAggregatorPage.Initialese} init */
xyz.swapee.rc.OffersAggregatorPageUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.OffersAggregatorPageUniversal.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPageUniversal  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal = function() {}
/**
 * @param {...(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.MetaUniversal  a7c66343ca2b0911b260ff795094988e */
/** @typedef {xyz.swapee.rc.OffersAggregatorPageMetaUniversal} */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.MetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPageConstructor  a7c66343ca2b0911b260ff795094988e */
/** @typedef {function(new: xyz.swapee.rc.UOffersAggregatorPage, ...!xyz.swapee.rc.UOffersAggregatorPage.Initialese)} */
xyz.swapee.rc.UOffersAggregatorPageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.RecordUOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordUOffersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundUOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.UOffersAggregatorPageFields}
 * @extends {xyz.swapee.rc.RecordUOffersAggregatorPage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.UOffersAggregatorPageCaster}
 */
xyz.swapee.rc.BoundUOffersAggregatorPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundOffersAggregatorPageUniversal  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundUOffersAggregatorPage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundOffersAggregatorPageUniversal = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BOffersAggregatorPageAspectsCaster  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageAspectsCaster
/** @type {!xyz.swapee.rc.BoundIOffersAggregatorPage} */
xyz.swapee.rc.BOffersAggregatorPageAspectsCaster.prototype.asIOffersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BOffersAggregatorPageAspects  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @extends {xyz.swapee.rc.BOffersAggregatorPageAspectsCaster<THIS>}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageAspects = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageAspectsCaster  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageAspectsCaster
/** @type {!xyz.swapee.rc.BoundIOffersAggregatorPage} */
xyz.swapee.rc.IOffersAggregatorPageAspectsCaster.prototype.asIOffersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageAspects  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageAspectsCaster}
 * @extends {xyz.swapee.rc.BOffersAggregatorPageAspects<!xyz.swapee.rc.IOffersAggregatorPageAspects>}
 * @extends {com.changelly.UChangelly}
 * @extends {io.letsexchange.ULetsExchange}
 * @extends {io.changenow.UChangeNow}
 */
xyz.swapee.rc.IOffersAggregatorPageAspects = function() {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese} init */
xyz.swapee.rc.IOffersAggregatorPageAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageAspects  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese} init
 * @implements {xyz.swapee.rc.IOffersAggregatorPageAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPageAspects = function(...init) {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese} init */
xyz.swapee.rc.OffersAggregatorPageAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.OffersAggregatorPageAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPageAspects  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects = function() {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageAspectsConstructor  a7c66343ca2b0911b260ff795094988e */
/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageAspects, ...!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese)} */
xyz.swapee.rc.OffersAggregatorPageAspectsConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.Initialese}
 */
xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IHyperOffersAggregatorPageCaster  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.IHyperOffersAggregatorPageCaster
/** @type {!xyz.swapee.rc.BoundIHyperOffersAggregatorPage} */
xyz.swapee.rc.IHyperOffersAggregatorPageCaster.prototype.asIHyperOffersAggregatorPage
/** @type {!xyz.swapee.rc.BoundHyperOffersAggregatorPage} */
xyz.swapee.rc.IHyperOffersAggregatorPageCaster.prototype.superHyperOffersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IHyperOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IHyperOffersAggregatorPageCaster}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage}
 */
xyz.swapee.rc.IHyperOffersAggregatorPage = function() {}
/** @param {...!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese} init */
xyz.swapee.rc.IHyperOffersAggregatorPage.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.HyperOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese} init
 * @implements {xyz.swapee.rc.IHyperOffersAggregatorPage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese>}
 */
xyz.swapee.rc.HyperOffersAggregatorPage = function(...init) {}
/** @param {...!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese} init */
xyz.swapee.rc.HyperOffersAggregatorPage.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.HyperOffersAggregatorPage.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractHyperOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @extends {xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage = function() {}
/**
 * @param {...((!xyz.swapee.rc.IHyperOffersAggregatorPage|typeof xyz.swapee.rc.HyperOffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperOffersAggregatorPage|typeof xyz.swapee.rc.HyperOffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperOffersAggregatorPage|typeof xyz.swapee.rc.HyperOffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageAspects|!Array<!xyz.swapee.rc.IOffersAggregatorPageAspects>|function(new: xyz.swapee.rc.IOffersAggregatorPageAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.installs = function(...aspectsInstallers) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.HyperOffersAggregatorPageConstructor  a7c66343ca2b0911b260ff795094988e */
/** @typedef {function(new: xyz.swapee.rc.IHyperOffersAggregatorPage, ...!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese)} */
xyz.swapee.rc.HyperOffersAggregatorPageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.RecordIHyperOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordIHyperOffersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundIHyperOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIHyperOffersAggregatorPage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IHyperOffersAggregatorPageCaster}
 */
xyz.swapee.rc.BoundIHyperOffersAggregatorPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundHyperOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIHyperOffersAggregatorPage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundHyperOffersAggregatorPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageHyperslice  a7c66343ca2b0911b260ff795094988e */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage._offersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._offersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.offersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.filterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.filterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.filterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.filterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFloatingOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.filterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFixedOffer>)} */
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.filterChangeNowFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageHyperslice  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageHyperslice}
 */
xyz.swapee.rc.OffersAggregatorPageHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice  a7c66343ca2b0911b260ff795094988e */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.offersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.filterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.filterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.filterLetsExchangeFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.filterLetsExchangeFixedOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.filterChangeNowFloatingOffer
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.filterChangeNowFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.OffersAggregatorPageBindingHyperslice  a7c66343ca2b0911b260ff795094988e */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.OffersAggregatorPageBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.RecordIOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/** @typedef {{ offersAggregator: xyz.swapee.rc.IOffersAggregatorPage.offersAggregator, filterChangellyFloatingOffer: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer, filterChangellyFixedOffer: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer, filterLetsExchangeFloatingOffer: xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer, filterLetsExchangeFixedOffer: xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer, filterChangeNowFloatingOffer: xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer, filterChangeNowFixedOffer: xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer }} */
xyz.swapee.rc.RecordIOffersAggregatorPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundIOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageFields}
 * @extends {xyz.swapee.rc.RecordIOffersAggregatorPage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageCaster}
 * @extends {com.changelly.BoundUChangelly}
 * @extends {io.letsexchange.BoundULetsExchange}
 * @extends {io.changenow.BoundUChangeNow}
 */
xyz.swapee.rc.BoundIOffersAggregatorPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.BoundOffersAggregatorPage  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIOffersAggregatorPage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundOffersAggregatorPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx
 * @return {(undefined|!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)}
 */
$$xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx): (undefined|!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPage, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx): (undefined|!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage._offersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator} */
xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPage,$$xyz.swapee.rc.IOffersAggregatorPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPage, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPage,$$xyz.swapee.rc.IOffersAggregatorPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPage, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPage,$$xyz.swapee.rc.IOffersAggregatorPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPage, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPage,$$xyz.swapee.rc.IOffersAggregatorPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPage, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPage,$$xyz.swapee.rc.IOffersAggregatorPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPage, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer

// nss:xyz.swapee.rc.IOffersAggregatorPage,$$xyz.swapee.rc.IOffersAggregatorPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer  a7c66343ca2b0911b260ff795094988e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPage, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors, !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx): (undefined|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer

// nss:xyz.swapee.rc.IOffersAggregatorPage,$$xyz.swapee.rc.IOffersAggregatorPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form = function() {}
/** @type {string} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatingOffer
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatingError
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatMin
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatMax

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedOffer
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedError
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedMin
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedMax

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers.prototype.letsExchangeFloatingOffer
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers.prototype.letsExchangeFloatingError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers.prototype.letsExchangeFixedOffer
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers.prototype.letsExchangeFixedError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers.prototype.changeNowFloatingOffer
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers.prototype.changeNowFloatingError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers.prototype.changeNowFixedOffer
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers.prototype.changeNowFixedError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers}
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers = function() {}
/** @type {string} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatingOffer
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatingError
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatMin
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatMax

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedOffer
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedError
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedMin
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedMax

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers.prototype.letsExchangeFloatingOffer
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers.prototype.letsExchangeFloatingError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers.prototype.letsExchangeFixedOffer
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers.prototype.letsExchangeFixedError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers.prototype.changeNowFloatingOffer
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers.prototype.changeNowFloatingError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers.prototype.changeNowFixedOffer
/** @type {(*)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers.prototype.changeNowFixedError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers  a7c66343ca2b0911b260ff795094988e */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers}
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers = function() {}
/** @type {string|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx = function() {}
/** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx.prototype.validation
/** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx.prototype.errors
/** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx.prototype.answers

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx.prototype.validation
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx.prototype.errors
/** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers)|undefined} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx.prototype.answers

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptCtx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptCtx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptCtx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptCtx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptCtx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptCtx  a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator  892e7982c4937eed6167bbaef4f6726b */
/** @constructor */
xyz.swapee.rc.OffersAggregator = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Answers  892e7982c4937eed6167bbaef4f6726b */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers}
 */
xyz.swapee.rc.OffersAggregator.Answers = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Answers.Props  892e7982c4937eed6167bbaef4f6726b */
/** @record */
xyz.swapee.rc.OffersAggregator.Answers.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Form  892e7982c4937eed6167bbaef4f6726b */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form}
 */
xyz.swapee.rc.OffersAggregator.Form = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Form.Props  892e7982c4937eed6167bbaef4f6726b */
/** @record */
xyz.swapee.rc.OffersAggregator.Form.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Errors  892e7982c4937eed6167bbaef4f6726b */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors}
 */
xyz.swapee.rc.OffersAggregator.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Errors.Props  892e7982c4937eed6167bbaef4f6726b */
/** @record */
xyz.swapee.rc.OffersAggregator.Errors.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Validation  892e7982c4937eed6167bbaef4f6726b */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation}
 */
xyz.swapee.rc.OffersAggregator.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Validation.Props  892e7982c4937eed6167bbaef4f6726b */
/** @record */
xyz.swapee.rc.OffersAggregator.Validation.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Ctx  892e7982c4937eed6167bbaef4f6726b */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx}
 */
xyz.swapee.rc.OffersAggregator.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml} xyz.swapee.rc.OffersAggregator.Ctx.Props  892e7982c4937eed6167bbaef4f6726b */
/** @record */
xyz.swapee.rc.OffersAggregator.Ctx.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.Initialese  56ee5a51133988c924c4a43c45db78bc */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageView.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewFields  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageViewFields
/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.GET} */
xyz.swapee.rc.IOffersAggregatorPageViewFields.prototype.GET
/** @type {!Object<string, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation>} */
xyz.swapee.rc.IOffersAggregatorPageViewFields.prototype.offersAggregatorTranslations

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewCaster  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageViewCaster
/** @type {!xyz.swapee.rc.BoundIOffersAggregatorPageView} */
xyz.swapee.rc.IOffersAggregatorPageViewCaster.prototype.asIOffersAggregatorPageView
/** @type {!xyz.swapee.rc.BoundIOffersAggregatorPage} */
xyz.swapee.rc.IOffersAggregatorPageViewCaster.prototype.asIOffersAggregatorPage
/** @type {!xyz.swapee.rc.BoundOffersAggregatorPageView} */
xyz.swapee.rc.IOffersAggregatorPageViewCaster.prototype.superOffersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageViewFields  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.UOffersAggregatorPageViewFields
/** @type {xyz.swapee.rc.IOffersAggregatorPageView} */
xyz.swapee.rc.UOffersAggregatorPageViewFields.prototype.offersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageViewCaster  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.UOffersAggregatorPageViewCaster
/** @type {!xyz.swapee.rc.BoundOffersAggregatorPageView} */
xyz.swapee.rc.UOffersAggregatorPageViewCaster.prototype.asOffersAggregatorPageView
/** @type {!xyz.swapee.rc.BoundUOffersAggregatorPageView} */
xyz.swapee.rc.UOffersAggregatorPageViewCaster.prototype.asUOffersAggregatorPageView
/** @type {!xyz.swapee.rc.BoundOffersAggregatorPageViewUniversal} */
xyz.swapee.rc.UOffersAggregatorPageViewCaster.prototype.superOffersAggregatorPageViewUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @extends {xyz.swapee.rc.UOffersAggregatorPageViewFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.UOffersAggregatorPageViewCaster}
 */
xyz.swapee.rc.UOffersAggregatorPageView = function() {}
/** @param {...!xyz.swapee.rc.UOffersAggregatorPageView.Initialese} init */
xyz.swapee.rc.UOffersAggregatorPageView.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewCaster}
 * @extends {_idio.IRedirectMod}
 * @extends {xyz.swapee.rc.UOffersAggregatorPageView}
 */
xyz.swapee.rc.IOffersAggregatorPageView = function() {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageView.Initialese} init */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form
 * @return {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView}
 */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.viewOffersAggregator = function(ctx, form) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form
 * @return {(undefined|!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView)}
 */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.getOffersAggregator = function(ctx, form) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation
 * @return {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx}
 */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.setOffersAggregatorCtx = function(answers, translation) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions} actions
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.OffersAggregatorPartial = function(answers, errors, actions, translation) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions} actions
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.OffersAggregatorView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageView.Initialese} init
 * @implements {xyz.swapee.rc.IOffersAggregatorPageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageView.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPageView = function(...init) {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageView.Initialese} init */
xyz.swapee.rc.OffersAggregatorPageView.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.OffersAggregatorPageView.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView = function() {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)|!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)|!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)|!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeViewOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.beforeViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterViewOffersAggregatorThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterViewOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterViewOffersAggregatorCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachViewOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.beforeEachViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEachViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregatorReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEachViewOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeGetOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.beforeGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterGetOffersAggregatorThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterGetOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterGetOffersAggregatorCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachGetOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.beforeEachGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEachGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregatorReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEachGetOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeSetOffersAggregatorCtx>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.beforeSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtx>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterSetOffersAggregatorCtxThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterSetOffersAggregatorCtxReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterSetOffersAggregatorCtxCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachSetOffersAggregatorCtx>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.beforeEachSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtx>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEachSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtxReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEachSetOffersAggregatorCtxReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorPartial>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.before_OffersAggregatorPartial
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartial>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorPartial
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorPartialThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorPartialReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorPartialCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorView>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.before_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorView>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewThrows>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorViewThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorViewReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewCancels>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.after_OffersAggregatorViewCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEach_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEach_OffersAggregatorView>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.beforeEach_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorView>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEach_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorViewReturns>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.afterEach_OffersAggregatorViewReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelHyperslice  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice}
 */
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.beforeViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterViewOffersAggregatorThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterViewOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterViewOffersAggregatorCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.beforeEachViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEachViewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEachViewOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.beforeGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterGetOffersAggregatorThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterGetOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterGetOffersAggregatorCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.beforeEachGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEachGetOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEachGetOffersAggregatorReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.beforeSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterSetOffersAggregatorCtxThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterSetOffersAggregatorCtxReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterSetOffersAggregatorCtxCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.beforeEachSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEachSetOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEachSetOffersAggregatorCtxReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.before_OffersAggregatorPartial
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorPartial
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorPartialThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorPartialReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorPartialCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.before_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorViewThrows
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorViewReturns
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.after_OffersAggregatorViewCancels
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.beforeEach_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEach_OffersAggregatorView
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.afterEach_OffersAggregatorViewReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelBindingHyperslice  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeViewOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregatorThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregatorReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregatorCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEachViewOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachViewOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachViewOffersAggregatorReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeGetOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregatorThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregatorReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregatorCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEachGetOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachGetOffersAggregator = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachGetOffersAggregatorReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeSetOffersAggregatorCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtxThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtxReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtxCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEachSetOffersAggregatorCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachSetOffersAggregatorCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachSetOffersAggregatorCtxReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.before_OffersAggregatorPartial = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartial = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartialThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartialReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartialCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.before_OffersAggregatorView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorViewThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorViewReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorViewCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEach_OffersAggregatorView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEach_OffersAggregatorView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEach_OffersAggregatorViewReturns = function(data) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewJoinpointModel  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel}
 */
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.RecordIOffersAggregatorPageViewJoinpointModel  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ beforeViewOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeViewOffersAggregator, afterViewOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregator, afterViewOffersAggregatorThrows: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorThrows, afterViewOffersAggregatorReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorReturns, afterViewOffersAggregatorCancels: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorCancels, beforeEachViewOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachViewOffersAggregator, afterEachViewOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregator, afterEachViewOffersAggregatorReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregatorReturns, beforeGetOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeGetOffersAggregator, afterGetOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregator, afterGetOffersAggregatorThrows: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorThrows, afterGetOffersAggregatorReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorReturns, afterGetOffersAggregatorCancels: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorCancels, beforeEachGetOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachGetOffersAggregator, afterEachGetOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregator, afterEachGetOffersAggregatorReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregatorReturns, beforeSetOffersAggregatorCtx: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeSetOffersAggregatorCtx, afterSetOffersAggregatorCtx: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtx, afterSetOffersAggregatorCtxThrows: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxThrows, afterSetOffersAggregatorCtxReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxReturns, afterSetOffersAggregatorCtxCancels: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxCancels, beforeEachSetOffersAggregatorCtx: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachSetOffersAggregatorCtx, afterEachSetOffersAggregatorCtx: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtx, afterEachSetOffersAggregatorCtxReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtxReturns, before_OffersAggregatorPartial: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorPartial, after_OffersAggregatorPartial: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartial, after_OffersAggregatorPartialThrows: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialThrows, after_OffersAggregatorPartialReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialReturns, after_OffersAggregatorPartialCancels: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialCancels, before_OffersAggregatorView: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorView, after_OffersAggregatorView: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorView, after_OffersAggregatorViewThrows: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewThrows, after_OffersAggregatorViewReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewReturns, after_OffersAggregatorViewCancels: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewCancels, beforeEach_OffersAggregatorView: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEach_OffersAggregatorView, afterEach_OffersAggregatorView: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorView, afterEach_OffersAggregatorViewReturns: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorViewReturns }} */
xyz.swapee.rc.RecordIOffersAggregatorPageViewJoinpointModel

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundIOffersAggregatorPageViewJoinpointModel  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIOffersAggregatorPageViewJoinpointModel}
 */
xyz.swapee.rc.BoundIOffersAggregatorPageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundOffersAggregatorPageViewJoinpointModel  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIOffersAggregatorPageViewJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundOffersAggregatorPageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeViewOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeViewOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeViewOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorThrows  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorCancels  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachViewOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachViewOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachViewOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregatorReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregatorReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregatorReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeGetOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeGetOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeGetOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorThrows  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorCancels  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachGetOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachGetOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachGetOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregatorReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregatorReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregatorReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeSetOffersAggregatorCtx  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeSetOffersAggregatorCtx
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeSetOffersAggregatorCtx
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtx  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtx
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtx
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxThrows  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxCancels  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachSetOffersAggregatorCtx  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachSetOffersAggregatorCtx
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachSetOffersAggregatorCtx
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtx  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtx
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtx
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtxReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtxReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtxReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorPartial  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorPartial
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorPartial
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartial  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartial
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartial
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialThrows  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialCancels  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorView
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorView
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorView
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorView
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewThrows  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewThrows
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewThrows
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewCancels  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewCancels
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewCancels
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEach_OffersAggregatorView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEach_OffersAggregatorView
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEach_OffersAggregatorView
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorView
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorView
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorViewReturns  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorViewReturns
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel, !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData=): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorViewReturns
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns

// nss:xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,$$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese  56ee5a51133988c924c4a43c45db78bc */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageInstaller}
 */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller = function() {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese} init */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.beforeViewOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterViewOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterViewOffersAggregatorThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterViewOffersAggregatorReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterViewOffersAggregatorCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.beforeEachViewOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEachViewOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEachViewOffersAggregatorReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.beforeGetOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterGetOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterGetOffersAggregatorThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterGetOffersAggregatorReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterGetOffersAggregatorCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.beforeEachGetOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEachGetOffersAggregator
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEachGetOffersAggregatorReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.beforeSetOffersAggregatorCtx
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterSetOffersAggregatorCtx
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterSetOffersAggregatorCtxThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterSetOffersAggregatorCtxReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterSetOffersAggregatorCtxCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.beforeEachSetOffersAggregatorCtx
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEachSetOffersAggregatorCtx
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEachSetOffersAggregatorCtxReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.before_OffersAggregatorPartial
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorPartial
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorPartialThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorPartialReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorPartialCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.before_OffersAggregatorView
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorView
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorViewThrows
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorViewReturns
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.after_OffersAggregatorViewCancels
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.beforeEach_OffersAggregatorView
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEach_OffersAggregatorView
/** @type {number} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.afterEach_OffersAggregatorViewReturns
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.viewOffersAggregator = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.getOffersAggregator = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.setOffersAggregatorCtx = function() {}
/** @return {?} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.OffersAggregatorPartial = function() {}
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.OffersAggregatorView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese} init */
xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller = function() {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller)|(!xyz.swapee.rc.IOffersAggregatorPageInstaller|typeof xyz.swapee.rc.OffersAggregatorPageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller)|(!xyz.swapee.rc.IOffersAggregatorPageInstaller|typeof xyz.swapee.rc.OffersAggregatorPageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller)|(!xyz.swapee.rc.IOffersAggregatorPageInstaller|typeof xyz.swapee.rc.OffersAggregatorPageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewAspectsInstallerConstructor  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller, ...!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese)} */
xyz.swapee.rc.OffersAggregatorPageViewAspectsInstallerConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.ViewOffersAggregatorNArgs  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ ctx: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, form: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form }} */
xyz.swapee.rc.IOffersAggregatorPageView.ViewOffersAggregatorNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPageView.ViewOffersAggregatorNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPageView.ViewOffersAggregatorNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData = function() {}
/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData = function() {}
/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.GetOffersAggregatorNArgs  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ ctx: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, form: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form }} */
xyz.swapee.rc.IOffersAggregatorPageView.GetOffersAggregatorNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPageView.GetOffersAggregatorNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPageView.GetOffersAggregatorNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData = function() {}
/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData = function() {}
/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ answers: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, translation: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation }} */
xyz.swapee.rc.IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData = function() {}
/** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData = function() {}
/** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartialNArgs  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ answers: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, errors: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors, actions: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions, translation: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation }} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartialNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartialNArgs, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartialNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {engineering.type.VNode} value
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData = function() {}
/** @type {engineering.type.VNode} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData = function() {}
/** @type {engineering.type.VNode} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData.prototype.res
/**
 * @param {engineering.type.VNode} value
 * @return {?}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData.prototype.cancel = function(reason) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewConstructor  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageView, ...!xyz.swapee.rc.IOffersAggregatorPageView.Initialese)} */
xyz.swapee.rc.OffersAggregatorPageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewMetaUniversal  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {symbol} */
xyz.swapee.rc.OffersAggregatorPageViewMetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageView.Initialese  56ee5a51133988c924c4a43c45db78bc */
/** @record */
xyz.swapee.rc.UOffersAggregatorPageView.Initialese = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView|undefined} */
xyz.swapee.rc.UOffersAggregatorPageView.Initialese.prototype.offersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewUniversal  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.UOffersAggregatorPageView.Initialese} init
 * @implements {xyz.swapee.rc.UOffersAggregatorPageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UOffersAggregatorPageView.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPageViewUniversal = function(...init) {}
/** @param {...!xyz.swapee.rc.UOffersAggregatorPageView.Initialese} init */
xyz.swapee.rc.OffersAggregatorPageViewUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.OffersAggregatorPageViewUniversal.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal = function() {}
/**
 * @param {...(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.MetaUniversal  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {xyz.swapee.rc.OffersAggregatorPageViewMetaUniversal} */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.MetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageViewConstructor  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {function(new: xyz.swapee.rc.UOffersAggregatorPageView, ...!xyz.swapee.rc.UOffersAggregatorPageView.Initialese)} */
xyz.swapee.rc.UOffersAggregatorPageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.RecordUOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordUOffersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundUOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.UOffersAggregatorPageViewFields}
 * @extends {xyz.swapee.rc.RecordUOffersAggregatorPageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.UOffersAggregatorPageViewCaster}
 */
xyz.swapee.rc.BoundUOffersAggregatorPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundOffersAggregatorPageViewUniversal  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundUOffersAggregatorPageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundOffersAggregatorPageViewUniversal = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster
/** @type {!xyz.swapee.rc.BoundIOffersAggregatorPageView} */
xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster.prototype.asIOffersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BOffersAggregatorPageViewAspects  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @extends {xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster<THIS>}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageViewAspects = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese  56ee5a51133988c924c4a43c45db78bc */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster
/** @type {!xyz.swapee.rc.BoundIOffersAggregatorPageView} */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster.prototype.asIOffersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewAspects  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster}
 * @extends {xyz.swapee.rc.BOffersAggregatorPageViewAspects<!xyz.swapee.rc.IOffersAggregatorPageViewAspects>}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage}
 * @extends {_idio.IRedirectMod}
 * @extends {xyz.swapee.rc.BOffersAggregatorPageAspects}
 */
xyz.swapee.rc.IOffersAggregatorPageViewAspects = function() {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese} init */
xyz.swapee.rc.IOffersAggregatorPageViewAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewAspects  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese} init
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorPageViewAspects = function(...init) {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese} init */
xyz.swapee.rc.OffersAggregatorPageViewAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.OffersAggregatorPageViewAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects = function() {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.OffersAggregatorPageViewAspects)|(!xyz.swapee.rc.BOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.BOffersAggregatorPageViewAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.OffersAggregatorPageViewAspects)|(!xyz.swapee.rc.BOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.BOffersAggregatorPageViewAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.OffersAggregatorPageViewAspects)|(!xyz.swapee.rc.BOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.BOffersAggregatorPageViewAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewAspectsConstructor  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageViewAspects, ...!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese)} */
xyz.swapee.rc.OffersAggregatorPageViewAspectsConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageView.Initialese}
 */
xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster
/** @type {!xyz.swapee.rc.BoundIHyperOffersAggregatorPageView} */
xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster.prototype.asIHyperOffersAggregatorPageView
/** @type {!xyz.swapee.rc.BoundHyperOffersAggregatorPageView} */
xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster.prototype.superHyperOffersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IHyperOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageView}
 */
xyz.swapee.rc.IHyperOffersAggregatorPageView = function() {}
/** @param {...!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese} init */
xyz.swapee.rc.IHyperOffersAggregatorPageView.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.HyperOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese} init
 * @implements {xyz.swapee.rc.IHyperOffersAggregatorPageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese>}
 */
xyz.swapee.rc.HyperOffersAggregatorPageView = function(...init) {}
/** @param {...!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese} init */
xyz.swapee.rc.HyperOffersAggregatorPageView.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.HyperOffersAggregatorPageView.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractHyperOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @extends {xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView = function() {}
/**
 * @param {...((!xyz.swapee.rc.IHyperOffersAggregatorPageView|typeof xyz.swapee.rc.HyperOffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperOffersAggregatorPageView|typeof xyz.swapee.rc.HyperOffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperOffersAggregatorPageView|typeof xyz.swapee.rc.HyperOffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageViewAspects|function(new: xyz.swapee.rc.IOffersAggregatorPageViewAspects))} aides
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.consults = function(...aides) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.HyperOffersAggregatorPageViewConstructor  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {function(new: xyz.swapee.rc.IHyperOffersAggregatorPageView, ...!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese)} */
xyz.swapee.rc.HyperOffersAggregatorPageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.RecordIHyperOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordIHyperOffersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundIHyperOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIHyperOffersAggregatorPageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster}
 */
xyz.swapee.rc.BoundIHyperOffersAggregatorPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundHyperOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIHyperOffersAggregatorPageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundHyperOffersAggregatorPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewHyperslice  56ee5a51133988c924c4a43c45db78bc */
/** @interface */
xyz.swapee.rc.IOffersAggregatorPageViewHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView._viewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._viewOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewHyperslice.prototype.viewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView._getOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._getOffersAggregator>)} */
xyz.swapee.rc.IOffersAggregatorPageViewHyperslice.prototype.getOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView._setOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._setOffersAggregatorCtx>)} */
xyz.swapee.rc.IOffersAggregatorPageViewHyperslice.prototype.setOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorView>)} */
xyz.swapee.rc.IOffersAggregatorPageViewHyperslice.prototype.OffersAggregatorView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewHyperslice  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewHyperslice}
 */
xyz.swapee.rc.OffersAggregatorPageViewHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice  56ee5a51133988c924c4a43c45db78bc */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice.prototype.viewOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice.prototype.getOffersAggregator
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice.prototype.setOffersAggregatorCtx
/** @type {(!xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView<THIS>>)} */
xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice.prototype.OffersAggregatorView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.OffersAggregatorPageViewBindingHyperslice  56ee5a51133988c924c4a43c45db78bc */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.OffersAggregatorPageViewBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.RecordIOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {{ viewOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageView.viewOffersAggregator, getOffersAggregator: xyz.swapee.rc.IOffersAggregatorPageView.getOffersAggregator, setOffersAggregatorCtx: xyz.swapee.rc.IOffersAggregatorPageView.setOffersAggregatorCtx, OffersAggregatorPartial: xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial, OffersAggregatorView: xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView }} */
xyz.swapee.rc.RecordIOffersAggregatorPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundIOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewFields}
 * @extends {xyz.swapee.rc.RecordIOffersAggregatorPageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewCaster}
 * @extends {_idio.BoundIRedirectMod}
 */
xyz.swapee.rc.BoundIOffersAggregatorPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.BoundOffersAggregatorPageView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIOffersAggregatorPageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundOffersAggregatorPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.viewOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form
 * @return {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView}
 */
$$xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator = function(ctx, form) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form): !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageView.viewOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageView, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form): !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageView._viewOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageView,$$xyz.swapee.rc.IOffersAggregatorPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.getOffersAggregator  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form
 * @return {(undefined|!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView)}
 */
$$xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator = function(ctx, form) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form): (undefined|!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView)} */
xyz.swapee.rc.IOffersAggregatorPageView.getOffersAggregator
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageView, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form): (undefined|!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView)} */
xyz.swapee.rc.IOffersAggregatorPageView._getOffersAggregator
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator

// nss:xyz.swapee.rc.IOffersAggregatorPageView,$$xyz.swapee.rc.IOffersAggregatorPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.setOffersAggregatorCtx  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation
 * @return {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx}
 */
$$xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx = function(answers, translation) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation): !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} */
xyz.swapee.rc.IOffersAggregatorPageView.setOffersAggregatorCtx
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageView, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation): !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} */
xyz.swapee.rc.IOffersAggregatorPageView._setOffersAggregatorCtx
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx

// nss:xyz.swapee.rc.IOffersAggregatorPageView,$$xyz.swapee.rc.IOffersAggregatorPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions} actions
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
$$xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorPartial = function(answers, errors, actions, translation) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageView, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorPartial
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorPartial} */
xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorPartial

// nss:xyz.swapee.rc.IOffersAggregatorPageView,$$xyz.swapee.rc.IOffersAggregatorPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView  56ee5a51133988c924c4a43c45db78bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors} errors
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions} actions
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
$$xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView = function(answers, errors, actions, translation) {}
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView
/** @typedef {function(this: xyz.swapee.rc.IOffersAggregatorPageView, !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorView
/** @typedef {typeof $$xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView

// nss:xyz.swapee.rc.IOffersAggregatorPageView,$$xyz.swapee.rc.IOffersAggregatorPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.GET  56ee5a51133988c924c4a43c45db78bc */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageView.GET = function() {}
/** @type {xyz.swapee.rc.OffersAggregator.offersAggregatorNav} */
xyz.swapee.rc.IOffersAggregatorPageView.GET.prototype.offersAggregator

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors  56ee5a51133988c924c4a43c45db78bc */
/**
 * @record
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation}
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions  56ee5a51133988c924c4a43c45db78bc */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions = function() {}
/** @type {xyz.swapee.rc.OffersAggregator.offersAggregatorNav} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.offersAggregator
/** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._offersAggregator
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangellyFloatingOffer
/** @type {!xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangellyFloatingOffer
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangellyFixedOffer
/** @type {!xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangellyFixedOffer
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterLetsExchangeFloatingOffer
/** @type {!xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterLetsExchangeFloatingOffer
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterLetsExchangeFixedOffer
/** @type {!xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterLetsExchangeFixedOffer
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangeNowFloatingOffer
/** @type {!xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangeNowFloatingOffer
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangeNowFixedOffer
/** @type {!xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav} */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangeNowFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation  56ee5a51133988c924c4a43c45db78bc */
/** @record */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.offersAggregatorNav  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form): void} */
xyz.swapee.rc.OffersAggregator.offersAggregatorNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.OffersAggregatorNav  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.OffersAggregator.OffersAggregatorNav = function() {}
/** @type {number} */
xyz.swapee.rc.OffersAggregator.OffersAggregatorNav.prototype.offersAggregator

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.FilterChangellyFloatingOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.OffersAggregator.FilterChangellyFloatingOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.OffersAggregator.FilterChangellyFloatingOfferNav.prototype.filterChangellyFloatingOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.FilterChangellyFixedOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.OffersAggregator.FilterChangellyFixedOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.OffersAggregator.FilterChangellyFixedOfferNav.prototype.filterChangellyFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav.prototype.filterLetsExchangeFloatingOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav.prototype.filterLetsExchangeFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav.prototype.filterChangeNowFloatingOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav.prototype.filterChangeNowFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.offersAggregatorMethodsIds  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.offersAggregatorMethodsIds = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.offersAggregatorArcsIds  ef52c7010e8070821695b2fff28031c9 */
/** @constructor */
xyz.swapee.rc.offersAggregatorArcsIds = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.getOffersAggregator  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(): void} */
xyz.swapee.rc.getOffersAggregator

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.offersAggregator  ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorImpl): !xyz.swapee.rc.IOffersAggregatorPage} */
xyz.swapee.rc.offersAggregator

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.IOffersAggregatorImpl.Initialese  ef52c7010e8070821695b2fff28031c9 */
/** @record */
xyz.swapee.rc.IOffersAggregatorImpl.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.IOffersAggregatorImpl  ef52c7010e8070821695b2fff28031c9 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageAspects}
 * @extends {xyz.swapee.rc.IOffersAggregatorPage}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageHyperslice}
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewHyperslice}
 */
xyz.swapee.rc.IOffersAggregatorImpl = function() {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorImpl.Initialese} init */
xyz.swapee.rc.IOffersAggregatorImpl.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregatorImpl  ef52c7010e8070821695b2fff28031c9 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IOffersAggregatorImpl.Initialese} init
 * @implements {xyz.swapee.rc.IOffersAggregatorImpl}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorImpl.Initialese>}
 */
xyz.swapee.rc.OffersAggregatorImpl = function(...init) {}
/** @param {...!xyz.swapee.rc.IOffersAggregatorImpl.Initialese} init */
xyz.swapee.rc.OffersAggregatorImpl.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.OffersAggregatorImpl.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.AbstractOffersAggregatorImpl  ef52c7010e8070821695b2fff28031c9 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl = function() {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorImpl|typeof xyz.swapee.rc.OffersAggregatorImpl)|(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorImpl|typeof xyz.swapee.rc.OffersAggregatorImpl)|(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IOffersAggregatorImpl|typeof xyz.swapee.rc.OffersAggregatorImpl)|(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */