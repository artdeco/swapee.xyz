import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav} */
export const FilterChangeNowFixedOfferNav={
 toString(){return'filterChangeNowFixedOffer'},
 filterChangeNowFixedOffer:methodsIds.filterChangeNowFixedOffer,
}
export default FilterChangeNowFixedOfferNav