import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav} */
export const FilterLetsExchangeFixedOfferNav={
 toString(){return'filterLetsExchangeFixedOffer'},
 filterLetsExchangeFixedOffer:methodsIds.filterLetsExchangeFixedOffer,
}
export default FilterLetsExchangeFixedOfferNav