import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorPageAspectsInstaller}
 */
function __OffersAggregatorPageAspectsInstaller() {}
__OffersAggregatorPageAspectsInstaller.prototype = /** @type {!_OffersAggregatorPageAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller}
 */
class _OffersAggregatorPageAspectsInstaller { }

_OffersAggregatorPageAspectsInstaller.prototype[$advice]=__OffersAggregatorPageAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller} ‎ */
class OffersAggregatorPageAspectsInstaller extends newAbstract(
 _OffersAggregatorPageAspectsInstaller,'IOffersAggregatorPageAspectsInstaller',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller} */
OffersAggregatorPageAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller} */
function OffersAggregatorPageAspectsInstallerClass(){}

export default OffersAggregatorPageAspectsInstaller


OffersAggregatorPageAspectsInstaller[$implementations]=[
 OffersAggregatorPageAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller}*/({
  offersAggregator(){
   this.beforeOffersAggregator=1
   this.afterOffersAggregator=2
   this.aroundOffersAggregator=3
   this.afterOffersAggregatorThrows=4
   this.afterOffersAggregatorReturns=5
   this.afterOffersAggregatorCancels=7
   this.beforeEachOffersAggregator=8
   this.afterEachOffersAggregator=9
   this.afterEachOffersAggregatorReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterChangellyFloatingOffer(){
   this.beforeFilterChangellyFloatingOffer=1
   this.afterFilterChangellyFloatingOffer=2
   this.aroundFilterChangellyFloatingOffer=3
   this.afterFilterChangellyFloatingOfferThrows=4
   this.afterFilterChangellyFloatingOfferReturns=5
   this.afterFilterChangellyFloatingOfferCancels=7
   this.beforeEachFilterChangellyFloatingOffer=8
   this.afterEachFilterChangellyFloatingOffer=9
   this.afterEachFilterChangellyFloatingOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterChangellyFixedOffer(){
   this.beforeFilterChangellyFixedOffer=1
   this.afterFilterChangellyFixedOffer=2
   this.aroundFilterChangellyFixedOffer=3
   this.afterFilterChangellyFixedOfferThrows=4
   this.afterFilterChangellyFixedOfferReturns=5
   this.afterFilterChangellyFixedOfferCancels=7
   this.beforeEachFilterChangellyFixedOffer=8
   this.afterEachFilterChangellyFixedOffer=9
   this.afterEachFilterChangellyFixedOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterLetsExchangeFloatingOffer(){
   this.beforeFilterLetsExchangeFloatingOffer=1
   this.afterFilterLetsExchangeFloatingOffer=2
   this.aroundFilterLetsExchangeFloatingOffer=3
   this.afterFilterLetsExchangeFloatingOfferThrows=4
   this.afterFilterLetsExchangeFloatingOfferReturns=5
   this.afterFilterLetsExchangeFloatingOfferCancels=7
   this.beforeEachFilterLetsExchangeFloatingOffer=8
   this.afterEachFilterLetsExchangeFloatingOffer=9
   this.afterEachFilterLetsExchangeFloatingOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterLetsExchangeFixedOffer(){
   this.beforeFilterLetsExchangeFixedOffer=1
   this.afterFilterLetsExchangeFixedOffer=2
   this.aroundFilterLetsExchangeFixedOffer=3
   this.afterFilterLetsExchangeFixedOfferThrows=4
   this.afterFilterLetsExchangeFixedOfferReturns=5
   this.afterFilterLetsExchangeFixedOfferCancels=7
   this.beforeEachFilterLetsExchangeFixedOffer=8
   this.afterEachFilterLetsExchangeFixedOffer=9
   this.afterEachFilterLetsExchangeFixedOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterChangeNowFloatingOffer(){
   this.beforeFilterChangeNowFloatingOffer=1
   this.afterFilterChangeNowFloatingOffer=2
   this.aroundFilterChangeNowFloatingOffer=3
   this.afterFilterChangeNowFloatingOfferThrows=4
   this.afterFilterChangeNowFloatingOfferReturns=5
   this.afterFilterChangeNowFloatingOfferCancels=7
   this.beforeEachFilterChangeNowFloatingOffer=8
   this.afterEachFilterChangeNowFloatingOffer=9
   this.afterEachFilterChangeNowFloatingOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterChangeNowFixedOffer(){
   this.beforeFilterChangeNowFixedOffer=1
   this.afterFilterChangeNowFixedOffer=2
   this.aroundFilterChangeNowFixedOffer=3
   this.afterFilterChangeNowFixedOfferThrows=4
   this.afterFilterChangeNowFixedOfferReturns=5
   this.afterFilterChangeNowFixedOfferCancels=7
   this.beforeEachFilterChangeNowFixedOffer=8
   this.afterEachFilterChangeNowFixedOffer=9
   this.afterEachFilterChangeNowFixedOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
 }),
 __OffersAggregatorPageAspectsInstaller,
]