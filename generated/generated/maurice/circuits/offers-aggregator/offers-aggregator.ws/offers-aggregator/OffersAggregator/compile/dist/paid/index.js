require('@type.engineering/type-engineer/types/typedefs')
//require('./types/typology')
require('./types/typedefs')

/**
@license
@LICENSE @xyz.swapee.rc/offers-aggregator (c) by X 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @extends {xyz.swapee.rc.OffersAggregatorPage} */
class OffersAggregatorPage extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the methods.
 * @extends {xyz.swapee.rc.offersAggregatorMethodsIds}
 */
class offersAggregatorMethodsIds extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the arcs.
 * @extends {xyz.swapee.rc.offersAggregatorArcsIds}
 */
class offersAggregatorArcsIds extends (class {/* lazy-loaded */}) {}
/**
 * Allows to render the pagelet after it's been processed.
 */
function getOffersAggregator() {
  // lazy-loaded()
}
/** @extends {xyz.swapee.rc.OffersAggregatorPageView} */
class OffersAggregatorPageView extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.rc.HyperOffersAggregatorPageView} */
class HyperOffersAggregatorPageView extends (class {/* lazy-loaded */}) {}
/**
 * The `offersAggregator` navigation metadata.
 * @extends {xyz.swapee.rc.OffersAggregator.OffersAggregatorNav}
 */
class OffersAggregatorNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterChangellyFloatingOffer` navigation metadata.
 * @extends {xyz.swapee.rc.OffersAggregator.FilterChangellyFloatingOfferNav}
 */
class FilterChangellyFloatingOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterChangellyFixedOffer` navigation metadata.
 * @extends {xyz.swapee.rc.OffersAggregator.FilterChangellyFixedOfferNav}
 */
class FilterChangellyFixedOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterLetsExchangeFloatingOffer` navigation metadata.
 * @extends {xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav}
 */
class FilterLetsExchangeFloatingOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterLetsExchangeFixedOffer` navigation metadata.
 * @extends {xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav}
 */
class FilterLetsExchangeFixedOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterChangeNowFloatingOffer` navigation metadata.
 * @extends {xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav}
 */
class FilterChangeNowFloatingOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterChangeNowFixedOffer` navigation metadata.
 * @extends {xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav}
 */
class FilterChangeNowFixedOfferNav extends (class {/* lazy-loaded */}) {}

module.exports.OffersAggregatorPage = OffersAggregatorPage
module.exports.offersAggregatorMethodsIds = offersAggregatorMethodsIds
module.exports.offersAggregatorArcsIds = offersAggregatorArcsIds
module.exports.getOffersAggregator = getOffersAggregator
module.exports.OffersAggregatorPageView = OffersAggregatorPageView
module.exports.HyperOffersAggregatorPageView = HyperOffersAggregatorPageView
module.exports.OffersAggregatorNav = OffersAggregatorNav
module.exports.FilterChangellyFloatingOfferNav = FilterChangellyFloatingOfferNav
module.exports.FilterChangellyFixedOfferNav = FilterChangellyFixedOfferNav
module.exports.FilterLetsExchangeFloatingOfferNav = FilterLetsExchangeFloatingOfferNav
module.exports.FilterLetsExchangeFixedOfferNav = FilterLetsExchangeFixedOfferNav
module.exports.FilterChangeNowFloatingOfferNav = FilterChangeNowFloatingOfferNav
module.exports.FilterChangeNowFixedOfferNav = FilterChangeNowFixedOfferNav

Object.defineProperties(module.exports, {
 'OffersAggregatorPage': {get: () => require('./compile')[37963205541]},
 [37963205541]: {get: () => module.exports['OffersAggregatorPage']},
 'offersAggregatorMethodsIds': {get: () => require('./compile')[37963205542]},
 [37963205542]: {get: () => module.exports['offersAggregatorMethodsIds']},
 'offersAggregatorArcsIds': {get: () => require('./compile')[37963205543]},
 [37963205543]: {get: () => module.exports['offersAggregatorArcsIds']},
 'getOffersAggregator': {get: () => require('./compile')[37963205544]},
 [37963205544]: {get: () => module.exports['getOffersAggregator']},
 'OffersAggregatorPageView': {get: () => require('./compile')[379632055410]},
 [379632055410]: {get: () => module.exports['OffersAggregatorPageView']},
 'HyperOffersAggregatorPageView': {get: () => require('./compile')[379632055413]},
 [379632055413]: {get: () => module.exports['HyperOffersAggregatorPageView']},
 'OffersAggregatorNav': {get: () => require('./compile')[3796320554100]},
 [3796320554100]: {get: () => module.exports['OffersAggregatorNav']},
 'FilterChangellyFloatingOfferNav': {get: () => require('./compile')[3796320554101]},
 [3796320554101]: {get: () => module.exports['FilterChangellyFloatingOfferNav']},
 'FilterChangellyFixedOfferNav': {get: () => require('./compile')[3796320554102]},
 [3796320554102]: {get: () => module.exports['FilterChangellyFixedOfferNav']},
 'FilterLetsExchangeFloatingOfferNav': {get: () => require('./compile')[3796320554103]},
 [3796320554103]: {get: () => module.exports['FilterLetsExchangeFloatingOfferNav']},
 'FilterLetsExchangeFixedOfferNav': {get: () => require('./compile')[3796320554104]},
 [3796320554104]: {get: () => module.exports['FilterLetsExchangeFixedOfferNav']},
 'FilterChangeNowFloatingOfferNav': {get: () => require('./compile')[3796320554105]},
 [3796320554105]: {get: () => module.exports['FilterChangeNowFloatingOfferNav']},
 'FilterChangeNowFixedOfferNav': {get: () => require('./compile')[3796320554106]},
 [3796320554106]: {get: () => module.exports['FilterChangeNowFixedOfferNav']},
})