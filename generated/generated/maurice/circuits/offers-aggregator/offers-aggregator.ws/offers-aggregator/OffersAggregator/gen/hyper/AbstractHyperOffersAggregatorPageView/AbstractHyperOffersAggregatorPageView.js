import OffersAggregatorPageViewAspectsInstaller from '../../aspects-installers/OffersAggregatorPageViewAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperOffersAggregatorPageView}
 */
function __AbstractHyperOffersAggregatorPageView() {}
__AbstractHyperOffersAggregatorPageView.prototype = /** @type {!_AbstractHyperOffersAggregatorPageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperOffersAggregatorPageView}
 */
class _AbstractHyperOffersAggregatorPageView { }
/** @extends {xyz.swapee.rc.AbstractHyperOffersAggregatorPageView} ‎ */
class AbstractHyperOffersAggregatorPageView extends newAbstract(
 _AbstractHyperOffersAggregatorPageView,'IHyperOffersAggregatorPageView',null,{
  asIHyperOffersAggregatorPageView:OffersAggregatorPageViewAspectsInstaller,
  superHyperOffersAggregatorPageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPageView} */
AbstractHyperOffersAggregatorPageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPageView} */
function AbstractHyperOffersAggregatorPageViewClass(){}

export default AbstractHyperOffersAggregatorPageView


AbstractHyperOffersAggregatorPageView[$implementations]=[
 __AbstractHyperOffersAggregatorPageView,
]