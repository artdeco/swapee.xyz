export default [
 1, // OffersAggregatorPage
 [
  '2_3',
  [
   2, // methodsIds
   3, // arcsIds
  ],
 ],
 4, // getOffersAggregator
 10, // OffersAggregatorPageView
 13, // HyperOffersAggregatorPageView
 100, // OffersAggregatorNav
 101, // FilterChangellyFloatingOfferNav
 102, // FilterChangellyFixedOfferNav
 103, // FilterLetsExchangeFloatingOfferNav
 104, // FilterLetsExchangeFixedOfferNav
 105, // FilterChangeNowFloatingOfferNav
 106, // FilterChangeNowFixedOfferNav
]