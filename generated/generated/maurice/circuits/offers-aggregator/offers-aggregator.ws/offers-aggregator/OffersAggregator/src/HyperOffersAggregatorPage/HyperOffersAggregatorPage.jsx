import '../../types'
import {ChangellyUniversal} from '@type.community/changelly.com'
import {LetsExchangeUniversal} from '@swapee.xyz/swapee.xyz'
import {ChangeNowUniversal} from '@swapee.xyz/swapee.xyz'
import filterChangeNowFixedOffer from '../../../../../../../../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/src/OffersAggregatorService/methods/filterChangeNowFixedOffer/filter-change-now-fixed-offer'
import filterChangeNowFloatingOffer from '../../../../../../../../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/src/OffersAggregatorService/methods/filterChangeNowFloatingOffer/filter-change-now-floating-offer'
import filterLetsExchangeFixedOffer from '../../../../../../../../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/src/OffersAggregatorService/methods/filterLetsExchangeFixedOffer/filter-lets-exchange-fixed-offer'
import filterLetsExchangeFloatingOffer from '../../../../../../../../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/src/OffersAggregatorService/methods/filterLetsExchangeFloatingOffer/filter-lets-exchange-floating-offer'
import filterChangellyFloatingOffer from '../../../../../../../../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/src/OffersAggregatorService/methods/filterChangellyFloatingOffer/filter-changelly-floating-offer'
import filterChangellyFixedOffer from '../../../../../../../../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/src/OffersAggregatorService/methods/filterChangellyFixedOffer/filter-changelly-fixed-offer'
import {ser} from '@type.engineering/type-engineer'
import AbstractHyperOffersAggregatorPage from '../../gen/hyper/AbstractHyperOffersAggregatorPage'
import HyperOffersAggregatorPageView from '../HyperOffersAggregatorPageView'
import {AssignAnswers} from '@websystems/parthenon'
import AbstractOffersAggregatorPage from '../../gen/AbstractOffersAggregatorPage'
import AbstractOffersAggregatorPageAspects from '../../gen/aspects/AbstractOffersAggregatorPageAspects'
import OffersAggregatorPageMethodsIds from '../../gen/methods-ids'

/** @extends {xyz.swapee.rc.HyperOffersAggregatorPage}  */
export default class HyperOffersAggregatorPage extends AbstractHyperOffersAggregatorPage.consults(
 HyperOffersAggregatorPageView,
 AbstractOffersAggregatorPageAspects.class.prototype=/**@type {!xyz.swapee.rc.OffersAggregatorPageAspects} */({
  afterEachFilterChangellyFloatingOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterChangellyFixedOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterLetsExchangeFloatingOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterLetsExchangeFixedOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterChangeNowFloatingOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterChangeNowFixedOfferReturns:[
   AssignAnswers,
  ],
  afterEachOffersAggregatorReturns:[
   AssignAnswers,
  ],
 }),
 // todo: add HyperView here
 AbstractOffersAggregatorPageAspects.class.prototype=/**@type {!xyz.swapee.rc.OffersAggregatorPageAspects} */({ 
  afterFilterChangeNowFixedOffer:[
   ServeData,
  ],
  afterFilterChangeNowFixedOfferThrows:[
   ServeError,
  ],
  afterFilterChangeNowFloatingOffer:[
   ServeData,
  ],
  afterFilterChangeNowFloatingOfferThrows:[
   ServeError,
  ],
  afterFilterLetsExchangeFixedOffer:[
   ServeData,
  ],
  afterFilterLetsExchangeFixedOfferThrows:[
   ServeError,
  ],
  afterFilterLetsExchangeFloatingOffer:[
   ServeData,
  ],
  afterFilterLetsExchangeFloatingOfferThrows:[
   ServeError,
  ],
  afterFilterChangellyFloatingOffer:[
   ServeData,
  ],
  afterFilterChangellyFloatingOfferThrows:[
   ServeError,
  ],
  afterFilterChangellyFixedOffer:[
   ServeData,
  ],
  afterFilterChangellyFixedOfferThrows:[
   ServeError,
  ],
 }),
).implements(
 AbstractOffersAggregatorPage,
 ChangellyUniversal,
 LetsExchangeUniversal,
 ChangeNowUniversal,
 AbstractHyperOffersAggregatorPage.class.prototype=/**@type {!xyz.swapee.rc.OffersAggregatorPageHyperslice} */({
  get _methodIds(){return OffersAggregatorPageMethodsIds},
 }),
 AbstractHyperOffersAggregatorPage.class.prototype=/**@type {!xyz.swapee.rc.OffersAggregatorPageHyperslice} */({
  filterChangeNowFixedOffer:[
   filterChangeNowFixedOffer,
  ],
  filterChangeNowFloatingOffer:[
   filterChangeNowFloatingOffer,
  ],
  filterLetsExchangeFixedOffer:[
   filterLetsExchangeFixedOffer,
  ],
  filterLetsExchangeFloatingOffer:[
   filterLetsExchangeFloatingOffer,
  ],
  filterChangellyFloatingOffer:[
   filterChangellyFloatingOffer,
  ],
  filterChangellyFixedOffer:[
   filterChangellyFixedOffer,
  ],
 }),
){}

function ServeError({hide:hide,err:err,args:{ctx:ctx}}){
 if(err.message.startsWith('!')) {
  hide(err)
  ctx.body={error:err.message.replace('!','')}
 }
}
function ServeData({args:{ctx:ctx,answers:answers}}){
 const data=ser(answers,PQs)
 ctx.body={data:data}
}

const PQs={
 changellyFloatingOffer:'2a73d',
 changellyFloatingError:'18c0d',
 changellyFloatMin:'17034',
 changellyFloatMax:'88705',
 changellyFixedOffer:'7b5f4',
 changellyFixedError:'5b11e',
 changellyFixedMin:'5617d',
 changellyFixedMax:'5ca90',
 letsExchangeFloatingOffer:'a0ea0',
 letsExchangeFloatingError:'821c9',
 letsExchangeFixedOffer:'26339',
 letsExchangeFixedError:'80525',
 changeNowFloatingOffer:'9f4fb',
 changeNowFloatingError:'17536',
 changeNowFixedOffer:'b89f1',
 changeNowFixedError:'1c43c',
}