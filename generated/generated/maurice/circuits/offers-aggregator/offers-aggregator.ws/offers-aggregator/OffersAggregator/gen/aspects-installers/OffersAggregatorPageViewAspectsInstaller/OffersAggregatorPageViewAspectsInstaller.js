import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorPageViewAspectsInstaller}
 */
function __OffersAggregatorPageViewAspectsInstaller() {}
__OffersAggregatorPageViewAspectsInstaller.prototype = /** @type {!_OffersAggregatorPageViewAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller}
 */
class _OffersAggregatorPageViewAspectsInstaller { }

_OffersAggregatorPageViewAspectsInstaller.prototype[$advice]=__OffersAggregatorPageViewAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller} ‎ */
class OffersAggregatorPageViewAspectsInstaller extends newAbstract(
 _OffersAggregatorPageViewAspectsInstaller,'IOffersAggregatorPageViewAspectsInstaller',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller} */
OffersAggregatorPageViewAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller} */
function OffersAggregatorPageViewAspectsInstallerClass(){}

export default OffersAggregatorPageViewAspectsInstaller


OffersAggregatorPageViewAspectsInstaller[$implementations]=[
 OffersAggregatorPageViewAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller}*/({
  viewOffersAggregator(){
   this.beforeViewOffersAggregator=1
   this.afterViewOffersAggregator=2
   this.aroundViewOffersAggregator=3
   this.afterViewOffersAggregatorThrows=4
   this.afterViewOffersAggregatorReturns=5
   this.afterViewOffersAggregatorCancels=7
   this.beforeEachViewOffersAggregator=8
   this.afterEachViewOffersAggregator=9
   this.afterEachViewOffersAggregatorReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  getOffersAggregator(){
   this.beforeGetOffersAggregator=1
   this.afterGetOffersAggregator=2
   this.aroundGetOffersAggregator=3
   this.afterGetOffersAggregatorThrows=4
   this.afterGetOffersAggregatorReturns=5
   this.afterGetOffersAggregatorCancels=7
   this.beforeEachGetOffersAggregator=8
   this.afterEachGetOffersAggregator=9
   this.afterEachGetOffersAggregatorReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  setOffersAggregatorCtx(){
   this.beforeSetOffersAggregatorCtx=1
   this.afterSetOffersAggregatorCtx=2
   this.aroundSetOffersAggregatorCtx=3
   this.afterSetOffersAggregatorCtxThrows=4
   this.afterSetOffersAggregatorCtxReturns=5
   this.afterSetOffersAggregatorCtxCancels=7
   this.beforeEachSetOffersAggregatorCtx=8
   this.afterEachSetOffersAggregatorCtx=9
   this.afterEachSetOffersAggregatorCtxReturns=10
   return {
    answers:1,
    translation:2,
   }
  },
  OffersAggregatorPartial(){
   this.before_OffersAggregatorPartial=1
   this.after_OffersAggregatorPartial=2
   this.around_OffersAggregatorPartial=3
   this.after_OffersAggregatorPartialThrows=4
   this.after_OffersAggregatorPartialReturns=5
   this.after_OffersAggregatorPartialCancels=7
   return {
    answers:1,
    errors:2,
    actions:3,
    translation:4,
   }
  },
  OffersAggregatorView(){
   this.before_OffersAggregatorView=1
   this.after_OffersAggregatorView=2
   this.around_OffersAggregatorView=3
   this.after_OffersAggregatorViewThrows=4
   this.after_OffersAggregatorViewReturns=5
   this.after_OffersAggregatorViewCancels=7
   this.beforeEach_OffersAggregatorView=8
   this.afterEach_OffersAggregatorView=9
   this.afterEach_OffersAggregatorViewReturns=10
  },
 }),
 __OffersAggregatorPageViewAspectsInstaller,
]