/** @const {?} */ xyz.swapee.rc.AbstractOffersAggregatorPageUniversal
/** @const {?} */ xyz.swapee.rc.UOffersAggregatorPage
/** @const {?} */ $xyz.swapee.rc.UOffersAggregatorPage
/** @const {?} */ $xyz.swapee.rc.UOffersAggregatorPageView
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.getSymbols filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @typedef {!xyz.swapee.rc.UOffersAggregatorPage.getSymbols} */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.setSymbols filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @typedef {!xyz.swapee.rc.UOffersAggregatorPage.setSymbols} */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.Symbols filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @typedef {!xyz.swapee.rc.UOffersAggregatorPage.Symbols} */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.Symbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPage.SymbolsIn filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @record */
$xyz.swapee.rc.UOffersAggregatorPage.SymbolsIn = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage|undefined} */
$xyz.swapee.rc.UOffersAggregatorPage.SymbolsIn.prototype._offersAggregatorPage
/** @typedef {$xyz.swapee.rc.UOffersAggregatorPage.SymbolsIn} */
xyz.swapee.rc.UOffersAggregatorPage.SymbolsIn

// nss:xyz.swapee.rc,$xyz.swapee.rc.UOffersAggregatorPage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPage.Symbols filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @record */
$xyz.swapee.rc.UOffersAggregatorPage.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.rc.UOffersAggregatorPage.Symbols.prototype._offersAggregatorPage
/** @typedef {$xyz.swapee.rc.UOffersAggregatorPage.Symbols} */
xyz.swapee.rc.UOffersAggregatorPage.Symbols

// nss:xyz.swapee.rc,$xyz.swapee.rc.UOffersAggregatorPage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPage.SymbolsOut filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @record */
$xyz.swapee.rc.UOffersAggregatorPage.SymbolsOut = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage} */
$xyz.swapee.rc.UOffersAggregatorPage.SymbolsOut.prototype._offersAggregatorPage
/** @typedef {$xyz.swapee.rc.UOffersAggregatorPage.SymbolsOut} */
xyz.swapee.rc.UOffersAggregatorPage.SymbolsOut

// nss:xyz.swapee.rc,$xyz.swapee.rc.UOffersAggregatorPage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPage.setSymbols filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @typedef {function(!xyz.swapee.rc.UOffersAggregatorPage, !xyz.swapee.rc.UOffersAggregatorPage.SymbolsIn): void} */
xyz.swapee.rc.UOffersAggregatorPage.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.UOffersAggregatorPage.getSymbols filter:Symbolism a7c66343ca2b0911b260ff795094988e */
/** @typedef {function(!xyz.swapee.rc.UOffersAggregatorPage): !xyz.swapee.rc.UOffersAggregatorPage.SymbolsOut} */
xyz.swapee.rc.UOffersAggregatorPage.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.getSymbols filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @typedef {!xyz.swapee.rc.UOffersAggregatorPageView.getSymbols} */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.setSymbols filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @typedef {!xyz.swapee.rc.UOffersAggregatorPageView.setSymbols} */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.Symbols filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @typedef {!xyz.swapee.rc.UOffersAggregatorPageView.Symbols} */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.Symbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageView.SymbolsIn filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @record */
$xyz.swapee.rc.UOffersAggregatorPageView.SymbolsIn = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView|undefined} */
$xyz.swapee.rc.UOffersAggregatorPageView.SymbolsIn.prototype._offersAggregatorPageView
/** @typedef {$xyz.swapee.rc.UOffersAggregatorPageView.SymbolsIn} */
xyz.swapee.rc.UOffersAggregatorPageView.SymbolsIn

// nss:xyz.swapee.rc,$xyz.swapee.rc.UOffersAggregatorPageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageView.Symbols filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @record */
$xyz.swapee.rc.UOffersAggregatorPageView.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.rc.UOffersAggregatorPageView.Symbols.prototype._offersAggregatorPageView
/** @typedef {$xyz.swapee.rc.UOffersAggregatorPageView.Symbols} */
xyz.swapee.rc.UOffersAggregatorPageView.Symbols

// nss:xyz.swapee.rc,$xyz.swapee.rc.UOffersAggregatorPageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageView.SymbolsOut filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @record */
$xyz.swapee.rc.UOffersAggregatorPageView.SymbolsOut = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView} */
$xyz.swapee.rc.UOffersAggregatorPageView.SymbolsOut.prototype._offersAggregatorPageView
/** @typedef {$xyz.swapee.rc.UOffersAggregatorPageView.SymbolsOut} */
xyz.swapee.rc.UOffersAggregatorPageView.SymbolsOut

// nss:xyz.swapee.rc,$xyz.swapee.rc.UOffersAggregatorPageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageView.setSymbols filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @typedef {function(!xyz.swapee.rc.UOffersAggregatorPageView, !xyz.swapee.rc.UOffersAggregatorPageView.SymbolsIn): void} */
xyz.swapee.rc.UOffersAggregatorPageView.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml} xyz.swapee.rc.UOffersAggregatorPageView.getSymbols filter:Symbolism 56ee5a51133988c924c4a43c45db78bc */
/** @typedef {function(!xyz.swapee.rc.UOffersAggregatorPageView): !xyz.swapee.rc.UOffersAggregatorPageView.SymbolsOut} */
xyz.swapee.rc.UOffersAggregatorPageView.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */