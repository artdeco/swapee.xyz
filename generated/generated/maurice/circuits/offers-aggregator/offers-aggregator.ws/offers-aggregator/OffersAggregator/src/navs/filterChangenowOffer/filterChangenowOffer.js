import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.OffersAggregator.FilterChangenowOfferNav} */
export const FilterChangenowOfferNav={
 toString(){return'filterChangenowOffer'},
 filterChangenowOffer:methodsIds.filterChangenowOffer,
}
export default FilterChangenowOfferNav