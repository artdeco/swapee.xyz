const OffersAggregatorPageArcsIds={
 locale:3545013,
 amountFrom:4102859,
 currencyFrom:1365305,
 currencyTo:8365870,
}
export const OffersAggregatorPageLiteralArcsIds={
 'locale':3545013,
 'amountFrom':4102859,
 'currencyFrom':1365305,
 'currencyTo':8365870,
}
export const ReverseOffersAggregatorPageArcsIds=Object.keys(OffersAggregatorPageArcsIds)
 .reduce((a,k)=>{a[OffersAggregatorPageArcsIds[`${k}`]]=k;return a},{})

export default OffersAggregatorPageArcsIds