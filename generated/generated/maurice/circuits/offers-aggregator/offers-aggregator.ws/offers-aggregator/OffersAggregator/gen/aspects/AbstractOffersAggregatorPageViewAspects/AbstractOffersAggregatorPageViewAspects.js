import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorPageViewAspects}
 */
function __AbstractOffersAggregatorPageViewAspects() {}
__AbstractOffersAggregatorPageViewAspects.prototype = /** @type {!_AbstractOffersAggregatorPageViewAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects}
 */
class _AbstractOffersAggregatorPageViewAspects { }
/**
 * The aspects of the *IOffersAggregatorPageView*.
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects} ‎
 */
class AbstractOffersAggregatorPageViewAspects extends newAspects(
 _AbstractOffersAggregatorPageViewAspects,'IOffersAggregatorPageViewAspects',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects} */
AbstractOffersAggregatorPageViewAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects} */
function AbstractOffersAggregatorPageViewAspectsClass(){}

export default AbstractOffersAggregatorPageViewAspects


AbstractOffersAggregatorPageViewAspects[$implementations]=[
 __AbstractOffersAggregatorPageViewAspects,
]