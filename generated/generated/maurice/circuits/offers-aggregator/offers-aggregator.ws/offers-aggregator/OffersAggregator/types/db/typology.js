/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice': {
  'id': 37963205541,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice': {
  'id': 37963205542,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageJoinpointModel': {
  'id': 37963205543,
  'symbols': {},
  'methods': {
   'beforeOffersAggregator': 1,
   'afterOffersAggregator': 2,
   'afterOffersAggregatorThrows': 3,
   'afterOffersAggregatorReturns': 4,
   'afterOffersAggregatorCancels': 5,
   'beforeEachOffersAggregator': 6,
   'afterEachOffersAggregator': 7,
   'afterEachOffersAggregatorReturns': 8,
   'beforeFilterChangellyFloatingOffer': 9,
   'afterFilterChangellyFloatingOffer': 10,
   'afterFilterChangellyFloatingOfferThrows': 11,
   'afterFilterChangellyFloatingOfferReturns': 12,
   'afterFilterChangellyFloatingOfferCancels': 13,
   'beforeEachFilterChangellyFloatingOffer': 14,
   'afterEachFilterChangellyFloatingOffer': 15,
   'afterEachFilterChangellyFloatingOfferReturns': 16,
   'beforeFilterChangellyFixedOffer': 17,
   'afterFilterChangellyFixedOffer': 18,
   'afterFilterChangellyFixedOfferThrows': 19,
   'afterFilterChangellyFixedOfferReturns': 20,
   'afterFilterChangellyFixedOfferCancels': 21,
   'beforeEachFilterChangellyFixedOffer': 22,
   'afterEachFilterChangellyFixedOffer': 23,
   'afterEachFilterChangellyFixedOfferReturns': 24,
   'beforeFilterLetsExchangeFloatingOffer': 33,
   'afterFilterLetsExchangeFloatingOffer': 34,
   'afterFilterLetsExchangeFloatingOfferThrows': 35,
   'afterFilterLetsExchangeFloatingOfferReturns': 36,
   'afterFilterLetsExchangeFloatingOfferCancels': 37,
   'beforeEachFilterLetsExchangeFloatingOffer': 38,
   'afterEachFilterLetsExchangeFloatingOffer': 39,
   'afterEachFilterLetsExchangeFloatingOfferReturns': 40,
   'beforeFilterLetsExchangeFixedOffer': 41,
   'afterFilterLetsExchangeFixedOffer': 42,
   'afterFilterLetsExchangeFixedOfferThrows': 43,
   'afterFilterLetsExchangeFixedOfferReturns': 44,
   'afterFilterLetsExchangeFixedOfferCancels': 45,
   'beforeEachFilterLetsExchangeFixedOffer': 46,
   'afterEachFilterLetsExchangeFixedOffer': 47,
   'afterEachFilterLetsExchangeFixedOfferReturns': 48,
   'beforeFilterChangeNowFloatingOffer': 49,
   'afterFilterChangeNowFloatingOffer': 50,
   'afterFilterChangeNowFloatingOfferThrows': 51,
   'afterFilterChangeNowFloatingOfferReturns': 52,
   'afterFilterChangeNowFloatingOfferCancels': 53,
   'beforeEachFilterChangeNowFloatingOffer': 54,
   'afterEachFilterChangeNowFloatingOffer': 55,
   'afterEachFilterChangeNowFloatingOfferReturns': 56,
   'beforeFilterChangeNowFixedOffer': 57,
   'afterFilterChangeNowFixedOffer': 58,
   'afterFilterChangeNowFixedOfferThrows': 59,
   'afterFilterChangeNowFixedOfferReturns': 60,
   'afterFilterChangeNowFixedOfferCancels': 61,
   'beforeEachFilterChangeNowFixedOffer': 62,
   'afterEachFilterChangeNowFixedOffer': 63,
   'afterEachFilterChangeNowFixedOfferReturns': 64
  }
 },
 'xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller': {
  'id': 37963205544,
  'symbols': {},
  'methods': {
   'offersAggregator': 1,
   'filterChangellyFloatingOffer': 2,
   'filterChangellyFixedOffer': 3,
   'filterLetsExchangeFloatingOffer': 5,
   'filterLetsExchangeFixedOffer': 6,
   'filterChangeNowFloatingOffer': 7,
   'filterChangeNowFixedOffer': 8
  }
 },
 'xyz.swapee.rc.UOffersAggregatorPage': {
  'id': 37963205545,
  'symbols': {
   'offersAggregatorPage': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BOffersAggregatorPageAspects': {
  'id': 37963205546,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IOffersAggregatorPageAspects': {
  'id': 37963205547,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperOffersAggregatorPage': {
  'id': 37963205548,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageHyperslice': {
  'id': 37963205549,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice': {
  'id': 379632055410,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPage': {
  'id': 379632055411,
  'symbols': {},
  'methods': {
   'offersAggregator': 1,
   'filterChangellyFloatingOffer': 2,
   'filterChangellyFixedOffer': 3,
   'filterLetsExchangeFloatingOffer': 5,
   'filterLetsExchangeFixedOffer': 6,
   'filterChangeNowFloatingOffer': 7,
   'filterChangeNowFixedOffer': 8
  }
 },
 'xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice': {
  'id': 379632055412,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice': {
  'id': 379632055413,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel': {
  'id': 379632055414,
  'symbols': {},
  'methods': {
   'beforeViewOffersAggregator': 1,
   'afterViewOffersAggregator': 2,
   'afterViewOffersAggregatorThrows': 3,
   'afterViewOffersAggregatorReturns': 4,
   'afterViewOffersAggregatorCancels': 5,
   'beforeEachViewOffersAggregator': 6,
   'afterEachViewOffersAggregator': 7,
   'afterEachViewOffersAggregatorReturns': 8,
   'beforeGetOffersAggregator': 9,
   'afterGetOffersAggregator': 10,
   'afterGetOffersAggregatorThrows': 11,
   'afterGetOffersAggregatorReturns': 12,
   'afterGetOffersAggregatorCancels': 13,
   'beforeEachGetOffersAggregator': 14,
   'afterEachGetOffersAggregator': 15,
   'afterEachGetOffersAggregatorReturns': 16,
   'beforeSetOffersAggregatorCtx': 17,
   'afterSetOffersAggregatorCtx': 18,
   'afterSetOffersAggregatorCtxThrows': 19,
   'afterSetOffersAggregatorCtxReturns': 20,
   'afterSetOffersAggregatorCtxCancels': 21,
   'beforeEachSetOffersAggregatorCtx': 22,
   'afterEachSetOffersAggregatorCtx': 23,
   'afterEachSetOffersAggregatorCtxReturns': 24,
   'before_OffersAggregatorPartial': 25,
   'after_OffersAggregatorPartial': 26,
   'after_OffersAggregatorPartialThrows': 27,
   'after_OffersAggregatorPartialReturns': 28,
   'after_OffersAggregatorPartialCancels': 29,
   'before_OffersAggregatorView': 30,
   'after_OffersAggregatorView': 31,
   'after_OffersAggregatorViewThrows': 32,
   'after_OffersAggregatorViewReturns': 33,
   'after_OffersAggregatorViewCancels': 34,
   'beforeEach_OffersAggregatorView': 35,
   'afterEach_OffersAggregatorView': 36,
   'afterEach_OffersAggregatorViewReturns': 37
  }
 },
 'xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller': {
  'id': 379632055415,
  'symbols': {},
  'methods': {
   'viewOffersAggregator': 1,
   'getOffersAggregator': 2,
   'setOffersAggregatorCtx': 3,
   'OffersAggregatorPartial': 4,
   'OffersAggregatorView': 5
  }
 },
 'xyz.swapee.rc.UOffersAggregatorPageView': {
  'id': 379632055416,
  'symbols': {
   'offersAggregatorPageView': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BOffersAggregatorPageViewAspects': {
  'id': 379632055417,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IOffersAggregatorPageViewAspects': {
  'id': 379632055418,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperOffersAggregatorPageView': {
  'id': 379632055419,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageViewHyperslice': {
  'id': 379632055420,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice': {
  'id': 379632055421,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IOffersAggregatorPageView': {
  'id': 379632055422,
  'symbols': {},
  'methods': {
   'viewOffersAggregator': 1,
   'getOffersAggregator': 2,
   'setOffersAggregatorCtx': 3,
   'OffersAggregatorPartial': 4,
   'OffersAggregatorView': 5
  }
 },
 'xyz.swapee.rc.IOffersAggregatorImpl': {
  'id': 379632055423,
  'symbols': {},
  'methods': {}
 }
})