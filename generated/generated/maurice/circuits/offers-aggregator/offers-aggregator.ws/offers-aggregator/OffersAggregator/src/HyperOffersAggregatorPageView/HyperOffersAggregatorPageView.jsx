import '../../types'
import OffersAggregatorPageView from '../OffersAggregatorPageView'
import AbstractHyperOffersAggregatorPageView from '../../gen/hyper/AbstractHyperOffersAggregatorPageView'
import {AssignLocale} from '@websystems/parthenon'
import OffersAggregatorPageArcsIds from '../../gen/arcs-ids'
import OffersAggregatorPageMethodsIds from '../../gen/methods-ids'
import {RenderAide} from '@idio2/server'
import AbstractOffersAggregatorPageViewAspects from '../../gen/aspects/AbstractOffersAggregatorPageViewAspects'

/** @extends {xyz.swapee.rc.HyperOffersAggregatorPageView}  */
export default class HyperOffersAggregatorPageView extends AbstractHyperOffersAggregatorPageView.consults(
 AbstractOffersAggregatorPageViewAspects.class.prototype=/**@type {!xyz.swapee.rc.OffersAggregatorPageViewAspects} */({
  beforeOffersAggregator:[
   AssignLocale,
  ],
  afterEachSetOffersAggregatorCtxReturns:[
   function({res:res}) {
    const{asIOffersAggregatorPage:{asCContext:ctx}}=this
    Object.assign(ctx,res)
   },
  ],
  afterViewOffersAggregatorReturns:[
   RenderAide.RenderActionableJSXCtx({
    ...OffersAggregatorPageMethodsIds,
   },{
    attrsProcess:[
    ],
    arcs:OffersAggregatorPageArcsIds,
   },{
    offersAggregatorTranslations:1,
   }),
  ],
 }),
).implements(
 OffersAggregatorPageView,
){}