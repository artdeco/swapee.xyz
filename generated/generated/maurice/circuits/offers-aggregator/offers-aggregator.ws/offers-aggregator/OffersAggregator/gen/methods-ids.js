export const OffersAggregatorPageMethodsIds={
 offersAggregator:2944605,
 filterChangellyFloatingOffer:'9ec7d',
 filterChangellyFixedOffer:'542a9',
 filterLetsExchangeFloatingOffer:'bfa1d',
 filterLetsExchangeFixedOffer:'5a51e',
 filterChangeNowFloatingOffer:'e5a7f',
 filterChangeNowFixedOffer:'a9da8',
}
export const OffersAggregatorPageLiteralMethodsIds={
 'offersAggregator':2944605,
 'filterChangellyFloatingOffer':'9ec7d',
 'filterChangellyFixedOffer':'542a9',
 'filterLetsExchangeFloatingOffer':'bfa1d',
 'filterLetsExchangeFixedOffer':'5a51e',
 'filterChangeNowFloatingOffer':'e5a7f',
 'filterChangeNowFixedOffer':'a9da8',
}

export default OffersAggregatorPageMethodsIds