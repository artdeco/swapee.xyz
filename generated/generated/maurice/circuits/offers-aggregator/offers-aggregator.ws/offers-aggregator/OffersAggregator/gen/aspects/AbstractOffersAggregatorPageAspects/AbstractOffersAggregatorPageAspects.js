import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorPageAspects}
 */
function __AbstractOffersAggregatorPageAspects() {}
__AbstractOffersAggregatorPageAspects.prototype = /** @type {!_AbstractOffersAggregatorPageAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageAspects}
 */
class _AbstractOffersAggregatorPageAspects { }
/**
 * The aspects of the *IOffersAggregatorPage*.
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageAspects} ‎
 */
class AbstractOffersAggregatorPageAspects extends newAspects(
 _AbstractOffersAggregatorPageAspects,'IOffersAggregatorPageAspects',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspects} */
AbstractOffersAggregatorPageAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspects} */
function AbstractOffersAggregatorPageAspectsClass(){}

export default AbstractOffersAggregatorPageAspects


AbstractOffersAggregatorPageAspects[$implementations]=[
 __AbstractOffersAggregatorPageAspects,
]