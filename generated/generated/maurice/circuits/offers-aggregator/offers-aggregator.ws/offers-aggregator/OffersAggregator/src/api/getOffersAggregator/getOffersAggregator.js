import OffersAggregatorPageMethodsIds from '../../../gen/methods-ids'
import '../../../types'

const methodsIds=Object.values(OffersAggregatorPageMethodsIds)

/**@type {xyz.swapee.rc.IOffersAggregatorPageView._getOffersAggregator} */
export default function _getOffersAggregator(ctx,form) {
 const actionId=parseFloat(form['action'])
 if(methodsIds.includes(actionId)) {
  const{asOffersAggregatorPageView:{viewOffersAggregator:viewOffersAggregator}}=this
  const ev=viewOffersAggregator(ctx,form)
  return ev
 }
}