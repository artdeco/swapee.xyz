/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
xyz.swapee.rc.IOffersAggregatorPage={}
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer={}
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller={}
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel={}
xyz.swapee.rc.UOffersAggregatorPage={}
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal={}
xyz.swapee.rc.IOffersAggregatorPageAspects={}
xyz.swapee.rc.IHyperOffersAggregatorPage={}
xyz.swapee.rc.OffersAggregator={}
xyz.swapee.rc.OffersAggregator.Answers={}
xyz.swapee.rc.OffersAggregator.Form={}
xyz.swapee.rc.OffersAggregator.Errors={}
xyz.swapee.rc.OffersAggregator.Validation={}
xyz.swapee.rc.OffersAggregator.Ctx={}
xyz.swapee.rc.IOffersAggregatorPageView={}
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial={}
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel={}
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller={}
xyz.swapee.rc.UOffersAggregatorPageView={}
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal={}
xyz.swapee.rc.IOffersAggregatorPageViewAspects={}
xyz.swapee.rc.IHyperOffersAggregatorPageView={}
xyz.swapee.rc.IOffersAggregatorImpl={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml}  a7c66343ca2b0911b260ff795094988e */
/** @typedef {com.changelly.UChangelly.Initialese&io.letsexchange.ULetsExchange.Initialese&io.changenow.UChangeNow.Initialese} xyz.swapee.rc.IOffersAggregatorPage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPage)} xyz.swapee.rc.AbstractOffersAggregatorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPage} xyz.swapee.rc.OffersAggregatorPage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IOffersAggregatorPage` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPage
 */
xyz.swapee.rc.AbstractOffersAggregatorPage = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPage.constructor&xyz.swapee.rc.OffersAggregatorPage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPage.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)|!xyz.swapee.rc.IOffersAggregatorPageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)|!xyz.swapee.rc.IOffersAggregatorPageHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage)|!xyz.swapee.rc.IOffersAggregatorPageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.AbstractOffersAggregatorPage.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeOffersAggregator>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregator>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOffersAggregatorThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOffersAggregatorCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachOffersAggregator>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregator>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregatorReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFloatingOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFloatingOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFixedOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFixedOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFloatingOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterLetsExchangeFloatingOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterLetsExchangeFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterLetsExchangeFloatingOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFloatingOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFixedOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterLetsExchangeFixedOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterLetsExchangeFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterLetsExchangeFixedOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFixedOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFloatingOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangeNowFloatingOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangeNowFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangeNowFloatingOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFloatingOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangeNowFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFixedOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangeNowFixedOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangeNowFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangeNowFixedOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFixedOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangeNowFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOfferReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice

/**
 * A concrete class of _IOffersAggregatorPageJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.OffersAggregatorPageJoinpointModelHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageJoinpointModelHyperslice { }
xyz.swapee.rc.OffersAggregatorPageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOffersAggregatorThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOffersAggregatorCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFloatingOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFloatingOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFixedOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFixedOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterLetsExchangeFloatingOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterLetsExchangeFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterLetsExchangeFloatingOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterLetsExchangeFixedOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterLetsExchangeFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterLetsExchangeFixedOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterLetsExchangeFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangeNowFloatingOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangeNowFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangeNowFloatingOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangeNowFloatingOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangeNowFixedOfferThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangeNowFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangeNowFixedOfferCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangeNowFixedOfferReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice

/**
 * A concrete class of _IOffersAggregatorPageJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.OffersAggregatorPageJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice { }
xyz.swapee.rc.OffersAggregatorPageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IOffersAggregatorPage`'s methods.
 * @interface xyz.swapee.rc.IOffersAggregatorPageJoinpointModel
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel = class { }
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregatorThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregatorReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterOffersAggregatorCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachOffersAggregatorReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOfferThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangellyFixedOfferCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangellyFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterLetsExchangeFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOfferThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFloatingOfferCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterLetsExchangeFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterLetsExchangeFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOfferThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterLetsExchangeFixedOfferCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterLetsExchangeFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterLetsExchangeFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangeNowFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOfferThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFloatingOfferCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangeNowFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeFilterChangeNowFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferThrows} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOfferThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferCancels} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterFilterChangeNowFixedOfferCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.beforeEachFilterChangeNowFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOfferReturns} */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.prototype.afterEachFilterChangeNowFixedOfferReturns = function() {}

/**
 * A concrete class of _IOffersAggregatorPageJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageJoinpointModel
 * @implements {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel} An interface that enumerates the joinpoints of `IOffersAggregatorPage`'s methods.
 */
xyz.swapee.rc.OffersAggregatorPageJoinpointModel = class extends xyz.swapee.rc.IOffersAggregatorPageJoinpointModel { }
xyz.swapee.rc.OffersAggregatorPageJoinpointModel.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageJoinpointModel

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel} */
xyz.swapee.rc.RecordIOffersAggregatorPageJoinpointModel

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel} xyz.swapee.rc.BoundIOffersAggregatorPageJoinpointModel */

/** @typedef {xyz.swapee.rc.OffersAggregatorPageJoinpointModel} xyz.swapee.rc.BoundOffersAggregatorPageJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPageAspectsInstaller)} xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller} xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.constructor&xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese[]) => xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller} xyz.swapee.rc.OffersAggregatorPageAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeOffersAggregator=/** @type {number} */ (void 0)
    this.afterOffersAggregator=/** @type {number} */ (void 0)
    this.afterOffersAggregatorThrows=/** @type {number} */ (void 0)
    this.afterOffersAggregatorReturns=/** @type {number} */ (void 0)
    this.afterOffersAggregatorCancels=/** @type {number} */ (void 0)
    this.beforeEachOffersAggregator=/** @type {number} */ (void 0)
    this.afterEachOffersAggregator=/** @type {number} */ (void 0)
    this.afterEachOffersAggregatorReturns=/** @type {number} */ (void 0)
    this.beforeFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFloatingOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFixedOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterLetsExchangeFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFloatingOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFloatingOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFloatingOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterLetsExchangeFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterLetsExchangeFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterLetsExchangeFloatingOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterLetsExchangeFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFixedOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFixedOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterLetsExchangeFixedOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterLetsExchangeFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterLetsExchangeFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterLetsExchangeFixedOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterChangeNowFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFloatingOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFloatingOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFloatingOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterChangeNowFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangeNowFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangeNowFloatingOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterChangeNowFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFixedOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFixedOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterChangeNowFixedOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterChangeNowFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangeNowFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangeNowFixedOfferReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  offersAggregator() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterChangellyFloatingOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterChangellyFixedOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterLetsExchangeFloatingOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterLetsExchangeFixedOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterChangeNowFloatingOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterChangeNowFixedOffer() { }
}
/**
 * Create a new *IOffersAggregatorPageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese>)} xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller} xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersAggregatorPageAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageAspectsInstaller
 * @implements {xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.constructor&xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersAggregatorPageAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersAggregatorPageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspectsInstaller}
 */
xyz.swapee.rc.OffersAggregatorPageAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPage.OffersAggregatorNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form The form.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation} validation The validation.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors} errors ,,
 *
 *         The errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx The ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.OffersAggregatorNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPage.OffersAggregatorNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `offersAggregator` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>) => void} sub Cancels a call to `offersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `offersAggregator` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterChangellyFloatingOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>) => void} sub Cancels a call to `filterChangellyFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterChangellyFloatingOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFixedOfferNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFixedOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangellyFixedOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterChangellyFixedOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>) => void} sub Cancels a call to `filterChangellyFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterChangellyFixedOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterLetsExchangeFloatingOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>) => void} sub Cancels a call to `filterLetsExchangeFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterLetsExchangeFloatingOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterLetsExchangeFixedOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>) => void} sub Cancels a call to `filterLetsExchangeFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterLetsExchangeFixedOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterChangeNowFloatingOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>) => void} sub Cancels a call to `filterChangeNowFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterChangeNowFloatingOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterChangeNowFixedOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>) => void} sub Cancels a call to `filterChangeNowFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterChangeNowFixedOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData&xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IOffersAggregatorPage.Initialese[]) => xyz.swapee.rc.IOffersAggregatorPage} xyz.swapee.rc.OffersAggregatorPageConstructor */

/** @typedef {symbol} xyz.swapee.rc.OffersAggregatorPageMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UOffersAggregatorPage.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IOffersAggregatorPage} [offersAggregatorPage]
 */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPageUniversal)} xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPageUniversal} xyz.swapee.rc.OffersAggregatorPageUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UOffersAggregatorPage` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPageUniversal
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.constructor&xyz.swapee.rc.OffersAggregatorPageUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPageUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPage|typeof xyz.swapee.rc.UOffersAggregatorPage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.OffersAggregatorPageMetaUniversal} xyz.swapee.rc.AbstractOffersAggregatorPageUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UOffersAggregatorPage.Initialese[]) => xyz.swapee.rc.UOffersAggregatorPage} xyz.swapee.rc.UOffersAggregatorPageConstructor */

/** @typedef {function(new: xyz.swapee.rc.UOffersAggregatorPageFields&engineering.type.IEngineer&xyz.swapee.rc.UOffersAggregatorPageCaster)} xyz.swapee.rc.UOffersAggregatorPage.constructor */
/**
 * A trait that allows to access an _IOffersAggregatorPage_ object via a `.offersAggregatorPage` field.
 * @interface xyz.swapee.rc.UOffersAggregatorPage
 */
xyz.swapee.rc.UOffersAggregatorPage = class extends /** @type {xyz.swapee.rc.UOffersAggregatorPage.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UOffersAggregatorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UOffersAggregatorPage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UOffersAggregatorPage&engineering.type.IInitialiser<!xyz.swapee.rc.UOffersAggregatorPage.Initialese>)} xyz.swapee.rc.OffersAggregatorPageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UOffersAggregatorPage} xyz.swapee.rc.UOffersAggregatorPage.typeof */
/**
 * A concrete class of _UOffersAggregatorPage_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageUniversal
 * @implements {xyz.swapee.rc.UOffersAggregatorPage} A trait that allows to access an _IOffersAggregatorPage_ object via a `.offersAggregatorPage` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UOffersAggregatorPage.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPageUniversal = class extends /** @type {xyz.swapee.rc.OffersAggregatorPageUniversal.constructor&xyz.swapee.rc.UOffersAggregatorPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UOffersAggregatorPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UOffersAggregatorPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UOffersAggregatorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPageUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageUniversal}
 */
xyz.swapee.rc.OffersAggregatorPageUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UOffersAggregatorPage.
 * @interface xyz.swapee.rc.UOffersAggregatorPageFields
 */
xyz.swapee.rc.UOffersAggregatorPageFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UOffersAggregatorPageFields.prototype.offersAggregatorPage = /** @type {xyz.swapee.rc.IOffersAggregatorPage} */ (void 0)

/** @typedef {xyz.swapee.rc.UOffersAggregatorPage} */
xyz.swapee.rc.RecordUOffersAggregatorPage

/** @typedef {xyz.swapee.rc.UOffersAggregatorPage} xyz.swapee.rc.BoundUOffersAggregatorPage */

/** @typedef {xyz.swapee.rc.OffersAggregatorPageUniversal} xyz.swapee.rc.BoundOffersAggregatorPageUniversal */

/**
 * Contains getters to cast the _UOffersAggregatorPage_ interface.
 * @interface xyz.swapee.rc.UOffersAggregatorPageCaster
 */
xyz.swapee.rc.UOffersAggregatorPageCaster = class { }
/**
 * Provides direct access to _UOffersAggregatorPage_ via the _BoundUOffersAggregatorPage_ universal.
 * @type {!xyz.swapee.rc.BoundOffersAggregatorPage}
 */
xyz.swapee.rc.UOffersAggregatorPageCaster.prototype.asOffersAggregatorPage
/**
 * Cast the _UOffersAggregatorPage_ instance into the _BoundUOffersAggregatorPage_ type.
 * @type {!xyz.swapee.rc.BoundUOffersAggregatorPage}
 */
xyz.swapee.rc.UOffersAggregatorPageCaster.prototype.asUOffersAggregatorPage
/**
 * Access the _OffersAggregatorPageUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundOffersAggregatorPageUniversal}
 */
xyz.swapee.rc.UOffersAggregatorPageCaster.prototype.superOffersAggregatorPageUniversal

/** @typedef {function(new: xyz.swapee.rc.BOffersAggregatorPageAspectsCaster<THIS>&xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BOffersAggregatorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice} xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IOffersAggregatorPage* that bind to an instance.
 * @interface xyz.swapee.rc.BOffersAggregatorPageAspects
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageAspects = class extends /** @type {xyz.swapee.rc.BOffersAggregatorPageAspects.constructor&xyz.swapee.rc.IOffersAggregatorPageJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BOffersAggregatorPageAspects.prototype.constructor = xyz.swapee.rc.BOffersAggregatorPageAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPageAspects)} xyz.swapee.rc.AbstractOffersAggregatorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPageAspects} xyz.swapee.rc.OffersAggregatorPageAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IOffersAggregatorPageAspects` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPageAspects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPageAspects.constructor&xyz.swapee.rc.OffersAggregatorPageAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPageAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese[]) => xyz.swapee.rc.IOffersAggregatorPageAspects} xyz.swapee.rc.OffersAggregatorPageAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IOffersAggregatorPageAspectsCaster&xyz.swapee.rc.BOffersAggregatorPageAspects<!xyz.swapee.rc.IOffersAggregatorPageAspects>&com.changelly.UChangelly&io.letsexchange.ULetsExchange&io.changenow.UChangeNow)} xyz.swapee.rc.IOffersAggregatorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BOffersAggregatorPageAspects} xyz.swapee.rc.BOffersAggregatorPageAspects.typeof */
/** @typedef {typeof com.changelly.UChangelly} com.changelly.UChangelly.typeof */
/** @typedef {typeof io.letsexchange.ULetsExchange} io.letsexchange.ULetsExchange.typeof */
/** @typedef {typeof io.changenow.UChangeNow} io.changenow.UChangeNow.typeof */
/**
 * The aspects of the *IOffersAggregatorPage*.
 * @interface xyz.swapee.rc.IOffersAggregatorPageAspects
 */
xyz.swapee.rc.IOffersAggregatorPageAspects = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPageAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BOffersAggregatorPageAspects.typeof&com.changelly.UChangelly.typeof&io.letsexchange.ULetsExchange.typeof&io.changenow.UChangeNow.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.props=/** @type {xyz.swapee.rc.IOffersAggregatorPageAspects} */ (void 0)
  }
}
/**
 * Create a new *IOffersAggregatorPageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IOffersAggregatorPageAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese>)} xyz.swapee.rc.OffersAggregatorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageAspects} xyz.swapee.rc.IOffersAggregatorPageAspects.typeof */
/**
 * A concrete class of _IOffersAggregatorPageAspects_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageAspects
 * @implements {xyz.swapee.rc.IOffersAggregatorPageAspects} The aspects of the *IOffersAggregatorPage*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPageAspects = class extends /** @type {xyz.swapee.rc.OffersAggregatorPageAspects.constructor&xyz.swapee.rc.IOffersAggregatorPageAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersAggregatorPageAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersAggregatorPageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPageAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageAspects}
 */
xyz.swapee.rc.OffersAggregatorPageAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BOffersAggregatorPageAspects_ interface.
 * @interface xyz.swapee.rc.BOffersAggregatorPageAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageAspectsCaster = class { }
/**
 * Cast the _BOffersAggregatorPageAspects_ instance into the _BoundIOffersAggregatorPage_ type.
 * @type {!xyz.swapee.rc.BoundIOffersAggregatorPage}
 */
xyz.swapee.rc.BOffersAggregatorPageAspectsCaster.prototype.asIOffersAggregatorPage

/**
 * Contains getters to cast the _IOffersAggregatorPageAspects_ interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageAspectsCaster
 */
xyz.swapee.rc.IOffersAggregatorPageAspectsCaster = class { }
/**
 * Cast the _IOffersAggregatorPageAspects_ instance into the _BoundIOffersAggregatorPage_ type.
 * @type {!xyz.swapee.rc.BoundIOffersAggregatorPage}
 */
xyz.swapee.rc.IOffersAggregatorPageAspectsCaster.prototype.asIOffersAggregatorPage

/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.Initialese} xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese */

/** @typedef {function(new: xyz.swapee.rc.HyperOffersAggregatorPage)} xyz.swapee.rc.AbstractHyperOffersAggregatorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperOffersAggregatorPage} xyz.swapee.rc.HyperOffersAggregatorPage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperOffersAggregatorPage` interface.
 * @constructor xyz.swapee.rc.AbstractHyperOffersAggregatorPage
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage = class extends /** @type {xyz.swapee.rc.AbstractHyperOffersAggregatorPage.constructor&xyz.swapee.rc.HyperOffersAggregatorPage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.prototype.constructor = xyz.swapee.rc.AbstractHyperOffersAggregatorPage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.class = /** @type {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperOffersAggregatorPage|typeof xyz.swapee.rc.HyperOffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperOffersAggregatorPage|typeof xyz.swapee.rc.HyperOffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperOffersAggregatorPage|typeof xyz.swapee.rc.HyperOffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageAspects|!Array<!xyz.swapee.rc.IOffersAggregatorPageAspects>|function(new: xyz.swapee.rc.IOffersAggregatorPageAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the IOffersAggregatorPage to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPage.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese[]) => xyz.swapee.rc.IHyperOffersAggregatorPage} xyz.swapee.rc.HyperOffersAggregatorPageConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperOffersAggregatorPageCaster&xyz.swapee.rc.IOffersAggregatorPage)} xyz.swapee.rc.IHyperOffersAggregatorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage} xyz.swapee.rc.IOffersAggregatorPage.typeof */
/** @interface xyz.swapee.rc.IHyperOffersAggregatorPage */
xyz.swapee.rc.IHyperOffersAggregatorPage = class extends /** @type {xyz.swapee.rc.IHyperOffersAggregatorPage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IOffersAggregatorPage.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperOffersAggregatorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperOffersAggregatorPage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperOffersAggregatorPage&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese>)} xyz.swapee.rc.HyperOffersAggregatorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperOffersAggregatorPage} xyz.swapee.rc.IHyperOffersAggregatorPage.typeof */
/**
 * A concrete class of _IHyperOffersAggregatorPage_ instances.
 * @constructor xyz.swapee.rc.HyperOffersAggregatorPage
 * @implements {xyz.swapee.rc.IHyperOffersAggregatorPage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese>} ‎
 */
xyz.swapee.rc.HyperOffersAggregatorPage = class extends /** @type {xyz.swapee.rc.HyperOffersAggregatorPage.constructor&xyz.swapee.rc.IHyperOffersAggregatorPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperOffersAggregatorPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperOffersAggregatorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperOffersAggregatorPage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPage}
 */
xyz.swapee.rc.HyperOffersAggregatorPage.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperOffersAggregatorPage} */
xyz.swapee.rc.RecordIHyperOffersAggregatorPage

/** @typedef {xyz.swapee.rc.IHyperOffersAggregatorPage} xyz.swapee.rc.BoundIHyperOffersAggregatorPage */

/** @typedef {xyz.swapee.rc.HyperOffersAggregatorPage} xyz.swapee.rc.BoundHyperOffersAggregatorPage */

/**
 * Contains getters to cast the _IHyperOffersAggregatorPage_ interface.
 * @interface xyz.swapee.rc.IHyperOffersAggregatorPageCaster
 */
xyz.swapee.rc.IHyperOffersAggregatorPageCaster = class { }
/**
 * Cast the _IHyperOffersAggregatorPage_ instance into the _BoundIHyperOffersAggregatorPage_ type.
 * @type {!xyz.swapee.rc.BoundIHyperOffersAggregatorPage}
 */
xyz.swapee.rc.IHyperOffersAggregatorPageCaster.prototype.asIHyperOffersAggregatorPage
/**
 * Access the _HyperOffersAggregatorPage_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperOffersAggregatorPage}
 */
xyz.swapee.rc.IHyperOffersAggregatorPageCaster.prototype.superHyperOffersAggregatorPage

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageHyperslice
 */
xyz.swapee.rc.IOffersAggregatorPageHyperslice = class {
  constructor() {
    this.offersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPage._offersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._offersAggregator>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFloatingOffer>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFixedOffer>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFloatingOffer>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFixedOffer>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFloatingOffer>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFixedOffer>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageHyperslice

/**
 * A concrete class of _IOffersAggregatorPageHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.OffersAggregatorPageHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageHyperslice { }
xyz.swapee.rc.OffersAggregatorPageHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice = class {
  constructor() {
    this.offersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterChangellyFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterLetsExchangeFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterLetsExchangeFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterChangeNowFloatingOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterChangeNowFixedOffer=/** @type {!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice

/**
 * A concrete class of _IOffersAggregatorPageBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageBindingHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice<THIS>}
 */
xyz.swapee.rc.OffersAggregatorPageBindingHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageBindingHyperslice { }
xyz.swapee.rc.OffersAggregatorPageBindingHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageFields&engineering.type.IEngineer&xyz.swapee.rc.IOffersAggregatorPageCaster&com.changelly.UChangelly&io.letsexchange.ULetsExchange&io.changenow.UChangeNow&xyz.swapee.rc.UOffersAggregatorPage)} xyz.swapee.rc.IOffersAggregatorPage.constructor */
/** @interface xyz.swapee.rc.IOffersAggregatorPage */
xyz.swapee.rc.IOffersAggregatorPage = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPage.constructor&engineering.type.IEngineer.typeof&com.changelly.UChangelly.typeof&io.letsexchange.ULetsExchange.typeof&io.changenow.UChangeNow.typeof&xyz.swapee.rc.UOffersAggregatorPage.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersAggregatorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IOffersAggregatorPage.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator} */
xyz.swapee.rc.IOffersAggregatorPage.prototype.offersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterLetsExchangeFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterLetsExchangeFixedOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer} */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangeNowFloatingOffer = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer} */
xyz.swapee.rc.IOffersAggregatorPage.prototype.filterChangeNowFixedOffer = function() {}

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage&engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPage.Initialese>)} xyz.swapee.rc.OffersAggregatorPage.constructor */
/**
 * A concrete class of _IOffersAggregatorPage_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPage
 * @implements {xyz.swapee.rc.IOffersAggregatorPage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPage.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPage = class extends /** @type {xyz.swapee.rc.OffersAggregatorPage.constructor&xyz.swapee.rc.IOffersAggregatorPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersAggregatorPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IOffersAggregatorPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersAggregatorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPage}
 */
xyz.swapee.rc.OffersAggregatorPage.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersAggregatorPage.
 * @interface xyz.swapee.rc.IOffersAggregatorPageFields
 */
xyz.swapee.rc.IOffersAggregatorPageFields = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageFields.prototype.offersAggregatorCtx = /** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} */ (void 0)

/** @typedef {xyz.swapee.rc.IOffersAggregatorPage} */
xyz.swapee.rc.RecordIOffersAggregatorPage

/** @typedef {xyz.swapee.rc.IOffersAggregatorPage} xyz.swapee.rc.BoundIOffersAggregatorPage */

/** @typedef {xyz.swapee.rc.OffersAggregatorPage} xyz.swapee.rc.BoundOffersAggregatorPage */

/**
 * Contains getters to cast the _IOffersAggregatorPage_ interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageCaster
 */
xyz.swapee.rc.IOffersAggregatorPageCaster = class { }
/**
 * Cast the _IOffersAggregatorPage_ instance into the _BoundIOffersAggregatorPage_ type.
 * @type {!xyz.swapee.rc.BoundIOffersAggregatorPage}
 */
xyz.swapee.rc.IOffersAggregatorPageCaster.prototype.asIOffersAggregatorPage
/**
 * Access the _OffersAggregatorPage_ prototype.
 * @type {!xyz.swapee.rc.BoundOffersAggregatorPage}
 */
xyz.swapee.rc.IOffersAggregatorPageCaster.prototype.superOffersAggregatorPage

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeOffersAggregator} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.OffersAggregatorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `offersAggregator` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPage.offersAggregator.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.offersAggregator.OptAnswers&gt;) =&gt; void_ Cancels a call to `offersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregator} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.offersAggregator.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorThrows<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `offersAggregator` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `afterReturns` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.offersAggregator.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: !IOffersAggregatorPage.offersAggregator.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.offersAggregator.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterOffersAggregatorCancels<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterOffersAggregatorCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterOffersAggregatorCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachOffersAggregator} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.OffersAggregatorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `offersAggregator` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPage.offersAggregator.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.offersAggregator.OptAnswers&gt;) =&gt; void_ Cancels a call to `offersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregator} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.offersAggregator.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachOffersAggregatorReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachOffersAggregatorReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregatorReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _offersAggregator_ at the `afterEach` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.offersAggregator.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPage.OffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.OffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachOffersAggregatorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFloatingOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFloatingOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterChangellyFloatingOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterReturns` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFloatingOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFloatingOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFloatingOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFloatingOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterEach` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangellyFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFixedOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangellyFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFixedOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterChangellyFixedOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterReturns` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangellyFixedOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangellyFixedOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFixedOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangellyFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFixedOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterEach` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFloatingOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterLetsExchangeFloatingOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterLetsExchangeFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferThrows<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterLetsExchangeFloatingOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `afterReturns` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFloatingOfferCancels<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFloatingOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFloatingOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFloatingOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterLetsExchangeFloatingOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterLetsExchangeFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFloatingOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFloatingOffer_ at the `afterEach` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterLetsExchangeFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterLetsExchangeFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFixedOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterLetsExchangeFixedOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterLetsExchangeFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterLetsExchangeFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferThrows<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterLetsExchangeFixedOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `afterReturns` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterLetsExchangeFixedOfferCancels<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterLetsExchangeFixedOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterLetsExchangeFixedOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterLetsExchangeFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterLetsExchangeFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFixedOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterLetsExchangeFixedOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterLetsExchangeFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterLetsExchangeFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterLetsExchangeFixedOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterLetsExchangeFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterLetsExchangeFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterLetsExchangeFixedOffer_ at the `afterEach` joinpoint.
 * - `res` _IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterLetsExchangeFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterLetsExchangeFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterLetsExchangeFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFloatingOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangeNowFloatingOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangeNowFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferThrows<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterChangeNowFloatingOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `afterReturns` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFloatingOfferCancels<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFloatingOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFloatingOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFloatingOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangeNowFloatingOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangeNowFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFloatingOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFloatingOffer_ at the `afterEach` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeFilterChangeNowFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeFilterChangeNowFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFixedOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangeNowFixedOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangeNowFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeFilterChangeNowFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferThrows<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterThrowsFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterChangeNowFixedOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterReturnsFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `afterReturns` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterFilterChangeNowFixedOfferCancels<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterFilterChangeNowFixedOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterCancelsFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterFilterChangeNowFixedOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__beforeEachFilterChangeNowFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._beforeEachFilterChangeNowFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFixedOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.BeforeFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangeNowFixedOffer` method from being executed.
 * - `sub` _(value: IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangeNowFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.beforeEachFilterChangeNowFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOffer<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `after` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.__afterEachFilterChangeNowFixedOfferReturns<!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageJoinpointModel._afterEachFilterChangeNowFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.AfterFilterChangeNowFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangeNowFixedOffer_ at the `afterEach` joinpoint.
 * - `res` _IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `args` _IOffersAggregatorPage.FilterChangeNowFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageJoinpointModel.FilterChangeNowFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageJoinpointModel.afterEachFilterChangeNowFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx) => (void|!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>)} xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.__offersAggregator<!xyz.swapee.rc.IOffersAggregatorPage>} xyz.swapee.rc.IOffersAggregatorPage._offersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.offersAggregator} */
/**
 *  `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form The form.
 * - `locale` _string_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFloatMin` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFloatMax` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `changellyFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `changellyFixedMin` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `changellyFixedMax` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `letsExchangeFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `letsExchangeFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `changeNowFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * - `changeNowFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation} validation The validation.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors} errors ,,
 *
 *         The errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx The ctx.
 * - `validation` _!IOffersAggregatorPage.offersAggregator.Validation_ The validation.
 * - `errors` _!IOffersAggregatorPage.offersAggregator.Errors_ The errors.
 * - `answers` _!IOffersAggregatorPage.offersAggregator.Answers_ The answers.
 * @return {void|!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers>}
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator = function(form, answers, validation, errors, ctx) {}

/**
 * The form.
 * @record xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers&xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers)} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers.constructor&xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers&xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers)} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers.constructor&xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers.typeof&xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptAnswers.prototype.locale = /** @type {string|undefined} */ (void 0)

/**
 * The validation.
 * @record xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation = class { }
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation

/**
 * ,,
 *
 *         The errors.
 * @record xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors = class { }
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors

/**
 * The ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx.prototype.validation = /** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx.prototype.errors = /** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx.prototype.answers = /** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} */ (void 0)

/**
 * The ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx.prototype.validation = /** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation)|undefined} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx.prototype.errors = /** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors)|undefined} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx.prototype.answers = /** @type {(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers)|undefined} */ (void 0)

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx) => (void|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>)} xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPage>} xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer} */
/**
 * Loads the _Changelly_ floating offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers} answers The action answers.
 * - `changellyFloatingOffer` _&#42;_
 * - `changellyFloatingError` _&#42;_
 * - `changellyFloatMin` _&#42;_
 * - `changellyFloatMax` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers>}
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatingOffer = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatingError = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatMin = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatMax = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatingOffer = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatingError = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatMin = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatMax = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptCtx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptCtx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx) => (void|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>)} xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.__filterChangellyFixedOffer<!xyz.swapee.rc.IOffersAggregatorPage>} xyz.swapee.rc.IOffersAggregatorPage._filterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer} */
/**
 * Loads the _Changelly_ fixed offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers} answers The action answers.
 * - `changellyFixedOffer` _&#42;_
 * - `changellyFixedError` _&#42;_
 * - `changellyFixedMin` _&#42;_
 * - `changellyFixedMax` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers>}
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedOffer = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedError = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedMin = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedMax = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedOffer = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedError = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedMin = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedMax = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptCtx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptCtx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx) => (void|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>)} xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPage>} xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer} */
/**
 * Loads the _Changelly_ floating offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers} answers The action answers.
 * - `letsExchangeFloatingOffer` _&#42;_
 * - `letsExchangeFloatingError` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers>}
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers.prototype.letsExchangeFloatingOffer = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers.prototype.letsExchangeFloatingError = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers.prototype.letsExchangeFloatingOffer = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptAnswers.prototype.letsExchangeFloatingError = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptCtx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptCtx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx) => (void|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>)} xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.__filterLetsExchangeFixedOffer<!xyz.swapee.rc.IOffersAggregatorPage>} xyz.swapee.rc.IOffersAggregatorPage._filterLetsExchangeFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer} */
/**
 * Loads the _Changelly_ fixed offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers} answers The action answers.
 * - `letsExchangeFixedOffer` _&#42;_
 * - `letsExchangeFixedError` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers>}
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers.prototype.letsExchangeFixedOffer = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers.prototype.letsExchangeFixedError = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers.prototype.letsExchangeFixedOffer = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptAnswers.prototype.letsExchangeFixedError = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptCtx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptCtx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx) => (void|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>)} xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFloatingOffer<!xyz.swapee.rc.IOffersAggregatorPage>} xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer} */
/**
 * Loads the _Changelly_ floating offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers} answers The action answers.
 * - `changeNowFloatingOffer` _&#42;_
 * - `changeNowFloatingError` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers>}
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers.prototype.changeNowFloatingOffer = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers.prototype.changeNowFloatingError = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers.prototype.changeNowFloatingOffer = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptAnswers.prototype.changeNowFloatingError = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptCtx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptCtx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form, answers: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers, validation: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation, errors: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors, ctx: !xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx) => (void|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>)} xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.__filterChangeNowFixedOffer<!xyz.swapee.rc.IOffersAggregatorPage>} xyz.swapee.rc.IOffersAggregatorPage._filterChangeNowFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer} */
/**
 * Loads the _Changelly_ fixed offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers} answers The action answers.
 * - `changeNowFixedOffer` _&#42;_
 * - `changeNowFixedError` _&#42;_
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers>}
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers.prototype.changeNowFixedOffer = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Answers.prototype.changeNowFixedError = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers.prototype.changeNowFixedOffer = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptAnswers.prototype.changeNowFixedError = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptCtx
 */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptCtx = class { }
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.OptCtx

// nss:xyz.swapee.rc,xyz.swapee.rc.IOffersAggregatorPageJoinpointModel,xyz.swapee.rc.IOffersAggregatorPage
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageAliases.xml}  892e7982c4937eed6167bbaef4f6726b */
/** @constructor xyz.swapee.rc.OffersAggregator */
xyz.swapee.rc.OffersAggregator = class { }
xyz.swapee.rc.OffersAggregator.prototype.constructor = xyz.swapee.rc.OffersAggregator

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers)} xyz.swapee.rc.OffersAggregator.Answers.constructor */
/**
 * The answers.
 * @record xyz.swapee.rc.OffersAggregator.Answers
 */
xyz.swapee.rc.OffersAggregator.Answers = class extends /** @type {xyz.swapee.rc.OffersAggregator.Answers.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.OffersAggregator.Answers.prototype.props = /** @type {!xyz.swapee.rc.OffersAggregator.Answers.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.OffersAggregator.Answers.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form)} xyz.swapee.rc.OffersAggregator.Form.constructor */
/**
 * The form.
 * @record xyz.swapee.rc.OffersAggregator.Form
 */
xyz.swapee.rc.OffersAggregator.Form = class extends /** @type {xyz.swapee.rc.OffersAggregator.Form.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.OffersAggregator.Form.prototype.props = /** @type {!xyz.swapee.rc.OffersAggregator.Form.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.OffersAggregator.Form.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors)} xyz.swapee.rc.OffersAggregator.Errors.constructor */
/**
 * The errors.
 * @record xyz.swapee.rc.OffersAggregator.Errors
 */
xyz.swapee.rc.OffersAggregator.Errors = class extends /** @type {xyz.swapee.rc.OffersAggregator.Errors.constructor} */ (class {}) { }
/**
 * ,,
 *       The props for VSCode.
 */
xyz.swapee.rc.OffersAggregator.Errors.prototype.props = /** @type {!xyz.swapee.rc.OffersAggregator.Errors.Props} */ (void 0)

/**
 * @typedef {typeof __$te_plain} xyz.swapee.rc.OffersAggregator.Errors.Props ,,
 *       The props for VSCode.
 */

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation)} xyz.swapee.rc.OffersAggregator.Validation.constructor */
/**
 * The validation.
 * @record xyz.swapee.rc.OffersAggregator.Validation
 */
xyz.swapee.rc.OffersAggregator.Validation = class extends /** @type {xyz.swapee.rc.OffersAggregator.Validation.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.OffersAggregator.Validation.prototype.props = /** @type {!xyz.swapee.rc.OffersAggregator.Validation.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.OffersAggregator.Validation.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx)} xyz.swapee.rc.OffersAggregator.Ctx.constructor */
/**
 * The ctx.
 * @record xyz.swapee.rc.OffersAggregator.Ctx
 */
xyz.swapee.rc.OffersAggregator.Ctx = class extends /** @type {xyz.swapee.rc.OffersAggregator.Ctx.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.OffersAggregator.Ctx.prototype.props = /** @type {!xyz.swapee.rc.OffersAggregator.Ctx.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.OffersAggregator.Ctx.Props The props for VSCode. */

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPageView.xml}  56ee5a51133988c924c4a43c45db78bc */
/** @typedef {typeof __$te_plain} xyz.swapee.rc.IOffersAggregatorPageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPageView)} xyz.swapee.rc.AbstractOffersAggregatorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPageView} xyz.swapee.rc.OffersAggregatorPageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IOffersAggregatorPageView` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPageView
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPageView.constructor&xyz.swapee.rc.OffersAggregatorPageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPageView.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)|!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)|!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView)|!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageView.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeViewOffersAggregator>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregator>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewOffersAggregatorThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewOffersAggregatorCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachViewOffersAggregator>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregator>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregatorReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeGetOffersAggregator>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregator>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetOffersAggregatorThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetOffersAggregatorCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachGetOffersAggregator>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregator>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregatorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregatorReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeSetOffersAggregatorCtx>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtx>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetOffersAggregatorCtxThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetOffersAggregatorCtxReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetOffersAggregatorCtxCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachSetOffersAggregatorCtx>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtx>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetOffersAggregatorCtxReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtxReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_OffersAggregatorPartial=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorPartial>} */ (void 0)
    /**
     * After the method.
     */
    this.after_OffersAggregatorPartial=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartial>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_OffersAggregatorPartialThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_OffersAggregatorPartialReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_OffersAggregatorPartialCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorView>} */ (void 0)
    /**
     * After the method.
     */
    this.after_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorView>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_OffersAggregatorViewThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_OffersAggregatorViewReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_OffersAggregatorViewCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEach_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEach_OffersAggregatorView>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorView>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_OffersAggregatorViewReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorViewReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice

/**
 * A concrete class of _IOffersAggregatorPageViewJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelHyperslice { }
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewOffersAggregatorThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewOffersAggregatorCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetOffersAggregatorThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetOffersAggregatorCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetOffersAggregatorReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetOffersAggregatorCtxThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetOffersAggregatorCtxReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetOffersAggregatorCtxCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetOffersAggregatorCtxReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_OffersAggregatorPartial=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_OffersAggregatorPartial=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_OffersAggregatorPartialThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_OffersAggregatorPartialReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_OffersAggregatorPartialCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_OffersAggregatorViewThrows=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_OffersAggregatorViewReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_OffersAggregatorViewCancels=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_OffersAggregatorViewReturns=/** @type {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice

/**
 * A concrete class of _IOffersAggregatorPageViewJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice { }
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageViewJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IOffersAggregatorPageView`'s methods.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel = class { }
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeViewOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregatorThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregatorReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterViewOffersAggregatorCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEachViewOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachViewOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachViewOffersAggregatorReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeGetOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregatorThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregatorReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterGetOffersAggregatorCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEachGetOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachGetOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregatorReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachGetOffersAggregatorReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeSetOffersAggregatorCtx = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtx = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtxThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtxReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterSetOffersAggregatorCtxCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEachSetOffersAggregatorCtx = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachSetOffersAggregatorCtx = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtxReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEachSetOffersAggregatorCtxReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorPartial} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.before_OffersAggregatorPartial = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartial} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartial = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartialThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartialReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorPartialCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.before_OffersAggregatorView = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorView = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewThrows} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorViewThrows = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorViewReturns = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewCancels} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.after_OffersAggregatorViewCancels = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEach_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.beforeEach_OffersAggregatorView = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEach_OffersAggregatorView = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorViewReturns} */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.prototype.afterEach_OffersAggregatorViewReturns = function() {}

/**
 * A concrete class of _IOffersAggregatorPageViewJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewJoinpointModel
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel} An interface that enumerates the joinpoints of `IOffersAggregatorPageView`'s methods.
 */
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModel = class extends xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel { }
xyz.swapee.rc.OffersAggregatorPageViewJoinpointModel.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel} */
xyz.swapee.rc.RecordIOffersAggregatorPageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel} xyz.swapee.rc.BoundIOffersAggregatorPageViewJoinpointModel */

/** @typedef {xyz.swapee.rc.OffersAggregatorPageViewJoinpointModel} xyz.swapee.rc.BoundOffersAggregatorPageViewJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller)} xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller} xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.constructor&xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller)|(!xyz.swapee.rc.IOffersAggregatorPageInstaller|typeof xyz.swapee.rc.OffersAggregatorPageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller)|(!xyz.swapee.rc.IOffersAggregatorPageInstaller|typeof xyz.swapee.rc.OffersAggregatorPageInstaller)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller|typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller)|(!xyz.swapee.rc.IOffersAggregatorPageInstaller|typeof xyz.swapee.rc.OffersAggregatorPageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese[]) => xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller} xyz.swapee.rc.OffersAggregatorPageViewAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IOffersAggregatorPageInstaller)} xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageInstaller} xyz.swapee.rc.IOffersAggregatorPageInstaller.typeof */
/** @interface xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IOffersAggregatorPageInstaller.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeViewOffersAggregator=/** @type {number} */ (void 0)
    this.afterViewOffersAggregator=/** @type {number} */ (void 0)
    this.afterViewOffersAggregatorThrows=/** @type {number} */ (void 0)
    this.afterViewOffersAggregatorReturns=/** @type {number} */ (void 0)
    this.afterViewOffersAggregatorCancels=/** @type {number} */ (void 0)
    this.beforeEachViewOffersAggregator=/** @type {number} */ (void 0)
    this.afterEachViewOffersAggregator=/** @type {number} */ (void 0)
    this.afterEachViewOffersAggregatorReturns=/** @type {number} */ (void 0)
    this.beforeGetOffersAggregator=/** @type {number} */ (void 0)
    this.afterGetOffersAggregator=/** @type {number} */ (void 0)
    this.afterGetOffersAggregatorThrows=/** @type {number} */ (void 0)
    this.afterGetOffersAggregatorReturns=/** @type {number} */ (void 0)
    this.afterGetOffersAggregatorCancels=/** @type {number} */ (void 0)
    this.beforeEachGetOffersAggregator=/** @type {number} */ (void 0)
    this.afterEachGetOffersAggregator=/** @type {number} */ (void 0)
    this.afterEachGetOffersAggregatorReturns=/** @type {number} */ (void 0)
    this.beforeSetOffersAggregatorCtx=/** @type {number} */ (void 0)
    this.afterSetOffersAggregatorCtx=/** @type {number} */ (void 0)
    this.afterSetOffersAggregatorCtxThrows=/** @type {number} */ (void 0)
    this.afterSetOffersAggregatorCtxReturns=/** @type {number} */ (void 0)
    this.afterSetOffersAggregatorCtxCancels=/** @type {number} */ (void 0)
    this.beforeEachSetOffersAggregatorCtx=/** @type {number} */ (void 0)
    this.afterEachSetOffersAggregatorCtx=/** @type {number} */ (void 0)
    this.afterEachSetOffersAggregatorCtxReturns=/** @type {number} */ (void 0)
    this.before_OffersAggregatorPartial=/** @type {number} */ (void 0)
    this.after_OffersAggregatorPartial=/** @type {number} */ (void 0)
    this.after_OffersAggregatorPartialThrows=/** @type {number} */ (void 0)
    this.after_OffersAggregatorPartialReturns=/** @type {number} */ (void 0)
    this.after_OffersAggregatorPartialCancels=/** @type {number} */ (void 0)
    this.before_OffersAggregatorView=/** @type {number} */ (void 0)
    this.after_OffersAggregatorView=/** @type {number} */ (void 0)
    this.after_OffersAggregatorViewThrows=/** @type {number} */ (void 0)
    this.after_OffersAggregatorViewReturns=/** @type {number} */ (void 0)
    this.after_OffersAggregatorViewCancels=/** @type {number} */ (void 0)
    this.beforeEach_OffersAggregatorView=/** @type {number} */ (void 0)
    this.afterEach_OffersAggregatorView=/** @type {number} */ (void 0)
    this.afterEach_OffersAggregatorViewReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  viewOffersAggregator() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  getOffersAggregator() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  setOffersAggregatorCtx() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  OffersAggregatorPartial() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  OffersAggregatorView() { }
}
/**
 * Create a new *IOffersAggregatorPageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese>)} xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller} xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.typeof */
/**
 * A concrete class of _IOffersAggregatorPageViewAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.constructor&xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersAggregatorPageViewAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersAggregatorPageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller}
 */
xyz.swapee.rc.OffersAggregatorPageViewAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageView.ViewOffersAggregatorNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPageView.ViewOffersAggregatorNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPageView.ViewOffersAggregatorNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `viewOffersAggregator` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView) => void} sub Cancels a call to `viewOffersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `viewOffersAggregator` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageView.GetOffersAggregatorNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPageView.GetOffersAggregatorNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPageView.GetOffersAggregatorNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getOffersAggregator` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView) => void} sub Cancels a call to `getOffersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getOffersAggregator` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation The translation for the user's locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `setOffersAggregatorCtx` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx) => void} sub Cancels a call to `setOffersAggregatorCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `setOffersAggregatorCtx` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartialNArgs
 * @prop {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions} actions The actions.
 * @prop {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation The translation for the chosen user locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartialNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartialNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `OffersAggregatorPartial` method from being executed.
 * @prop {(value: engineering.type.VNode) => void} sub Cancels a call to `OffersAggregatorPartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `OffersAggregatorPartial` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 * @prop {(value: engineering.type.VNode) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `OffersAggregatorView` method from being executed.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `OffersAggregatorView` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IOffersAggregatorPageView.Initialese[]) => xyz.swapee.rc.IOffersAggregatorPageView} xyz.swapee.rc.OffersAggregatorPageViewConstructor */

/** @typedef {symbol} xyz.swapee.rc.OffersAggregatorPageViewMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UOffersAggregatorPageView.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IOffersAggregatorPageView} [offersAggregatorPageView]
 */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPageViewUniversal)} xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal} xyz.swapee.rc.OffersAggregatorPageViewUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UOffersAggregatorPageView` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.constructor&xyz.swapee.rc.OffersAggregatorPageViewUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPageView|typeof xyz.swapee.rc.UOffersAggregatorPageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.OffersAggregatorPageViewMetaUniversal} xyz.swapee.rc.AbstractOffersAggregatorPageViewUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UOffersAggregatorPageView.Initialese[]) => xyz.swapee.rc.UOffersAggregatorPageView} xyz.swapee.rc.UOffersAggregatorPageViewConstructor */

/** @typedef {function(new: xyz.swapee.rc.UOffersAggregatorPageViewFields&engineering.type.IEngineer&xyz.swapee.rc.UOffersAggregatorPageViewCaster)} xyz.swapee.rc.UOffersAggregatorPageView.constructor */
/**
 * A trait that allows to access an _IOffersAggregatorPageView_ object via a `.offersAggregatorPageView` field.
 * @interface xyz.swapee.rc.UOffersAggregatorPageView
 */
xyz.swapee.rc.UOffersAggregatorPageView = class extends /** @type {xyz.swapee.rc.UOffersAggregatorPageView.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UOffersAggregatorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UOffersAggregatorPageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UOffersAggregatorPageView&engineering.type.IInitialiser<!xyz.swapee.rc.UOffersAggregatorPageView.Initialese>)} xyz.swapee.rc.OffersAggregatorPageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UOffersAggregatorPageView} xyz.swapee.rc.UOffersAggregatorPageView.typeof */
/**
 * A concrete class of _UOffersAggregatorPageView_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewUniversal
 * @implements {xyz.swapee.rc.UOffersAggregatorPageView} A trait that allows to access an _IOffersAggregatorPageView_ object via a `.offersAggregatorPageView` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UOffersAggregatorPageView.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPageViewUniversal = class extends /** @type {xyz.swapee.rc.OffersAggregatorPageViewUniversal.constructor&xyz.swapee.rc.UOffersAggregatorPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UOffersAggregatorPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UOffersAggregatorPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UOffersAggregatorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UOffersAggregatorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPageViewUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.OffersAggregatorPageViewUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UOffersAggregatorPageView.
 * @interface xyz.swapee.rc.UOffersAggregatorPageViewFields
 */
xyz.swapee.rc.UOffersAggregatorPageViewFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UOffersAggregatorPageViewFields.prototype.offersAggregatorPageView = /** @type {xyz.swapee.rc.IOffersAggregatorPageView} */ (void 0)

/** @typedef {xyz.swapee.rc.UOffersAggregatorPageView} */
xyz.swapee.rc.RecordUOffersAggregatorPageView

/** @typedef {xyz.swapee.rc.UOffersAggregatorPageView} xyz.swapee.rc.BoundUOffersAggregatorPageView */

/** @typedef {xyz.swapee.rc.OffersAggregatorPageViewUniversal} xyz.swapee.rc.BoundOffersAggregatorPageViewUniversal */

/**
 * Contains getters to cast the _UOffersAggregatorPageView_ interface.
 * @interface xyz.swapee.rc.UOffersAggregatorPageViewCaster
 */
xyz.swapee.rc.UOffersAggregatorPageViewCaster = class { }
/**
 * Provides direct access to _UOffersAggregatorPageView_ via the _BoundUOffersAggregatorPageView_ universal.
 * @type {!xyz.swapee.rc.BoundOffersAggregatorPageView}
 */
xyz.swapee.rc.UOffersAggregatorPageViewCaster.prototype.asOffersAggregatorPageView
/**
 * Cast the _UOffersAggregatorPageView_ instance into the _BoundUOffersAggregatorPageView_ type.
 * @type {!xyz.swapee.rc.BoundUOffersAggregatorPageView}
 */
xyz.swapee.rc.UOffersAggregatorPageViewCaster.prototype.asUOffersAggregatorPageView
/**
 * Access the _OffersAggregatorPageViewUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundOffersAggregatorPageViewUniversal}
 */
xyz.swapee.rc.UOffersAggregatorPageViewCaster.prototype.superOffersAggregatorPageViewUniversal

/** @typedef {function(new: xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster<THIS>&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BOffersAggregatorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IOffersAggregatorPageView* that bind to an instance.
 * @interface xyz.swapee.rc.BOffersAggregatorPageViewAspects
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageViewAspects = class extends /** @type {xyz.swapee.rc.BOffersAggregatorPageViewAspects.constructor&xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BOffersAggregatorPageViewAspects.prototype.constructor = xyz.swapee.rc.BOffersAggregatorPageViewAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorPageViewAspects)} xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects} xyz.swapee.rc.OffersAggregatorPageViewAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IOffersAggregatorPageViewAspects` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.constructor&xyz.swapee.rc.OffersAggregatorPageViewAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.OffersAggregatorPageViewAspects)|(!xyz.swapee.rc.BOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.BOffersAggregatorPageViewAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.OffersAggregatorPageViewAspects)|(!xyz.swapee.rc.BOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.BOffersAggregatorPageViewAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.OffersAggregatorPageViewAspects)|(!xyz.swapee.rc.BOffersAggregatorPageViewAspects|typeof xyz.swapee.rc.BOffersAggregatorPageViewAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BOffersAggregatorPageAspects|typeof xyz.swapee.rc.BOffersAggregatorPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.AbstractOffersAggregatorPageViewAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese[]) => xyz.swapee.rc.IOffersAggregatorPageViewAspects} xyz.swapee.rc.OffersAggregatorPageViewAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster&xyz.swapee.rc.BOffersAggregatorPageViewAspects<!xyz.swapee.rc.IOffersAggregatorPageViewAspects>&xyz.swapee.rc.IOffersAggregatorPage&_idio.IRedirectMod&xyz.swapee.rc.BOffersAggregatorPageAspects)} xyz.swapee.rc.IOffersAggregatorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BOffersAggregatorPageViewAspects} xyz.swapee.rc.BOffersAggregatorPageViewAspects.typeof */
/** @typedef {typeof _idio.IRedirectMod} _idio.IRedirectMod.typeof */
/**
 * The aspects of the *IOffersAggregatorPageView*.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewAspects
 */
xyz.swapee.rc.IOffersAggregatorPageViewAspects = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPageViewAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BOffersAggregatorPageViewAspects.typeof&xyz.swapee.rc.IOffersAggregatorPage.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.BOffersAggregatorPageAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersAggregatorPageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IOffersAggregatorPageViewAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageViewAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese>)} xyz.swapee.rc.OffersAggregatorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewAspects} xyz.swapee.rc.IOffersAggregatorPageViewAspects.typeof */
/**
 * A concrete class of _IOffersAggregatorPageViewAspects_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewAspects
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewAspects} The aspects of the *IOffersAggregatorPageView*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPageViewAspects = class extends /** @type {xyz.swapee.rc.OffersAggregatorPageViewAspects.constructor&xyz.swapee.rc.IOffersAggregatorPageViewAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersAggregatorPageViewAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersAggregatorPageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPageViewAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageViewAspects}
 */
xyz.swapee.rc.OffersAggregatorPageViewAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BOffersAggregatorPageViewAspects_ interface.
 * @interface xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster = class { }
/**
 * Cast the _BOffersAggregatorPageViewAspects_ instance into the _BoundIOffersAggregatorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIOffersAggregatorPageView}
 */
xyz.swapee.rc.BOffersAggregatorPageViewAspectsCaster.prototype.asIOffersAggregatorPageView

/**
 * Contains getters to cast the _IOffersAggregatorPageViewAspects_ interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster
 */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster = class { }
/**
 * Cast the _IOffersAggregatorPageViewAspects_ instance into the _BoundIOffersAggregatorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIOffersAggregatorPageView}
 */
xyz.swapee.rc.IOffersAggregatorPageViewAspectsCaster.prototype.asIOffersAggregatorPageView

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView.Initialese} xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.HyperOffersAggregatorPageView)} xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperOffersAggregatorPageView} xyz.swapee.rc.HyperOffersAggregatorPageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperOffersAggregatorPageView` interface.
 * @constructor xyz.swapee.rc.AbstractHyperOffersAggregatorPageView
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView = class extends /** @type {xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.constructor&xyz.swapee.rc.HyperOffersAggregatorPageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.prototype.constructor = xyz.swapee.rc.AbstractHyperOffersAggregatorPageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.class = /** @type {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperOffersAggregatorPageView|typeof xyz.swapee.rc.HyperOffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperOffersAggregatorPageView|typeof xyz.swapee.rc.HyperOffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperOffersAggregatorPageView|typeof xyz.swapee.rc.HyperOffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageView|typeof xyz.swapee.rc.OffersAggregatorPageView)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageViewAspects|function(new: xyz.swapee.rc.IOffersAggregatorPageViewAspects)} aides The list of aides that advise the IOffersAggregatorPageView to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperOffersAggregatorPageView.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese[]) => xyz.swapee.rc.IHyperOffersAggregatorPageView} xyz.swapee.rc.HyperOffersAggregatorPageViewConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster&xyz.swapee.rc.IOffersAggregatorPageView)} xyz.swapee.rc.IHyperOffersAggregatorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageView} xyz.swapee.rc.IOffersAggregatorPageView.typeof */
/** @interface xyz.swapee.rc.IHyperOffersAggregatorPageView */
xyz.swapee.rc.IHyperOffersAggregatorPageView = class extends /** @type {xyz.swapee.rc.IHyperOffersAggregatorPageView.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IOffersAggregatorPageView.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperOffersAggregatorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperOffersAggregatorPageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperOffersAggregatorPageView&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese>)} xyz.swapee.rc.HyperOffersAggregatorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperOffersAggregatorPageView} xyz.swapee.rc.IHyperOffersAggregatorPageView.typeof */
/**
 * A concrete class of _IHyperOffersAggregatorPageView_ instances.
 * @constructor xyz.swapee.rc.HyperOffersAggregatorPageView
 * @implements {xyz.swapee.rc.IHyperOffersAggregatorPageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese>} ‎
 */
xyz.swapee.rc.HyperOffersAggregatorPageView = class extends /** @type {xyz.swapee.rc.HyperOffersAggregatorPageView.constructor&xyz.swapee.rc.IHyperOffersAggregatorPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperOffersAggregatorPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperOffersAggregatorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperOffersAggregatorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperOffersAggregatorPageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperOffersAggregatorPageView}
 */
xyz.swapee.rc.HyperOffersAggregatorPageView.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperOffersAggregatorPageView} */
xyz.swapee.rc.RecordIHyperOffersAggregatorPageView

/** @typedef {xyz.swapee.rc.IHyperOffersAggregatorPageView} xyz.swapee.rc.BoundIHyperOffersAggregatorPageView */

/** @typedef {xyz.swapee.rc.HyperOffersAggregatorPageView} xyz.swapee.rc.BoundHyperOffersAggregatorPageView */

/**
 * Contains getters to cast the _IHyperOffersAggregatorPageView_ interface.
 * @interface xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster
 */
xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster = class { }
/**
 * Cast the _IHyperOffersAggregatorPageView_ instance into the _BoundIHyperOffersAggregatorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIHyperOffersAggregatorPageView}
 */
xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster.prototype.asIHyperOffersAggregatorPageView
/**
 * Access the _HyperOffersAggregatorPageView_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperOffersAggregatorPageView}
 */
xyz.swapee.rc.IHyperOffersAggregatorPageViewCaster.prototype.superHyperOffersAggregatorPageView

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewHyperslice
 */
xyz.swapee.rc.IOffersAggregatorPageViewHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView._viewOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._viewOffersAggregator>} */ (void 0)
    /**
     * The internal view handler for the `offersAggregator` action.
     */
    this.getOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView._getOffersAggregator|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._getOffersAggregator>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView._setOffersAggregatorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._setOffersAggregatorCtx>} */ (void 0)
    /**
     * The _OffersAggregator_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorView>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageViewHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageViewHyperslice

/**
 * A concrete class of _IOffersAggregatorPageViewHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.OffersAggregatorPageViewHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageViewHyperslice { }
xyz.swapee.rc.OffersAggregatorPageViewHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageViewHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator<THIS>>} */ (void 0)
    /**
     * The internal view handler for the `offersAggregator` action.
     */
    this.getOffersAggregator=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator<THIS>>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setOffersAggregatorCtx=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx<THIS>>} */ (void 0)
    /**
     * The _OffersAggregator_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.OffersAggregatorView=/** @type {!xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice

/**
 * A concrete class of _IOffersAggregatorPageViewBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageViewBindingHyperslice
 * @implements {xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice<THIS>}
 */
xyz.swapee.rc.OffersAggregatorPageViewBindingHyperslice = class extends xyz.swapee.rc.IOffersAggregatorPageViewBindingHyperslice { }
xyz.swapee.rc.OffersAggregatorPageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.OffersAggregatorPageViewBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageViewFields&engineering.type.IEngineer&xyz.swapee.rc.IOffersAggregatorPageViewCaster&_idio.IRedirectMod&xyz.swapee.rc.UOffersAggregatorPageView)} xyz.swapee.rc.IOffersAggregatorPageView.constructor */
/** @interface xyz.swapee.rc.IOffersAggregatorPageView */
xyz.swapee.rc.IOffersAggregatorPageView = class extends /** @type {xyz.swapee.rc.IOffersAggregatorPageView.constructor&engineering.type.IEngineer.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.UOffersAggregatorPageView.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersAggregatorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView.viewOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.viewOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView.getOffersAggregator} */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.getOffersAggregator = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView.setOffersAggregatorCtx} */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.setOffersAggregatorCtx = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial} */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.OffersAggregatorPartial = function() {}
/** @type {xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
xyz.swapee.rc.IOffersAggregatorPageView.prototype.OffersAggregatorView = function() {}

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorPageView&engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageView.Initialese>)} xyz.swapee.rc.OffersAggregatorPageView.constructor */
/**
 * A concrete class of _IOffersAggregatorPageView_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorPageView
 * @implements {xyz.swapee.rc.IOffersAggregatorPageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorPageView.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorPageView = class extends /** @type {xyz.swapee.rc.OffersAggregatorPageView.constructor&xyz.swapee.rc.IOffersAggregatorPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersAggregatorPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IOffersAggregatorPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersAggregatorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorPageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorPageView}
 */
xyz.swapee.rc.OffersAggregatorPageView.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersAggregatorPageView.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewFields
 */
xyz.swapee.rc.IOffersAggregatorPageViewFields = class { }
/**
 * The navs for _GET_ paths (e.g., to use for redirects).
 */
xyz.swapee.rc.IOffersAggregatorPageViewFields.prototype.GET = /** @type {!xyz.swapee.rc.IOffersAggregatorPageView.GET} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageViewFields.prototype.offersAggregatorTranslations = /** @type {!Object<string, !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation>} */ (void 0)

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView} */
xyz.swapee.rc.RecordIOffersAggregatorPageView

/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView} xyz.swapee.rc.BoundIOffersAggregatorPageView */

/** @typedef {xyz.swapee.rc.OffersAggregatorPageView} xyz.swapee.rc.BoundOffersAggregatorPageView */

/**
 * @typedef {Object} xyz.swapee.rc.IOffersAggregatorPageView.GET The navs for _GET_ paths (e.g., to use for redirects).
 * @prop {xyz.swapee.rc.OffersAggregator.offersAggregatorNav} offersAggregator
 */

/**
 * Contains getters to cast the _IOffersAggregatorPageView_ interface.
 * @interface xyz.swapee.rc.IOffersAggregatorPageViewCaster
 */
xyz.swapee.rc.IOffersAggregatorPageViewCaster = class { }
/**
 * Cast the _IOffersAggregatorPageView_ instance into the _BoundIOffersAggregatorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIOffersAggregatorPageView}
 */
xyz.swapee.rc.IOffersAggregatorPageViewCaster.prototype.asIOffersAggregatorPageView
/**
 * Cast the _IOffersAggregatorPageView_ instance into the _BoundIOffersAggregatorPage_ type.
 * @type {!xyz.swapee.rc.BoundIOffersAggregatorPage}
 */
xyz.swapee.rc.IOffersAggregatorPageViewCaster.prototype.asIOffersAggregatorPage
/**
 * Access the _OffersAggregatorPageView_ prototype.
 * @type {!xyz.swapee.rc.BoundOffersAggregatorPageView}
 */
xyz.swapee.rc.IOffersAggregatorPageViewCaster.prototype.superOffersAggregatorPageView

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeViewOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeViewOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeViewOffersAggregator} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPageView.ViewOffersAggregatorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewOffersAggregator` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPageView.OffersAggregatorView) =&gt; void_ Cancels a call to `viewOffersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeViewOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregator} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorThrows<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `viewOffersAggregator` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `afterReturns` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IOffersAggregatorPageView.OffersAggregatorView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterViewOffersAggregatorCancels<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterViewOffersAggregatorCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterViewOffersAggregatorCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachViewOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachViewOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachViewOffersAggregator} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPageView.ViewOffersAggregatorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewOffersAggregator` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPageView.OffersAggregatorView) =&gt; void_ Cancels a call to `viewOffersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachViewOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregator} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachViewOffersAggregatorReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachViewOffersAggregatorReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregatorReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterViewOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _viewOffersAggregator_ at the `afterEach` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.ViewOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.ViewOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachViewOffersAggregatorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeGetOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeGetOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeGetOffersAggregator} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPageView.GetOffersAggregatorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getOffersAggregator` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPageView.OffersAggregatorView) =&gt; void_ Cancels a call to `getOffersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeGetOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregator} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorThrows<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getOffersAggregator` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `afterReturns` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IOffersAggregatorPageView.OffersAggregatorView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterGetOffersAggregatorCancels<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterGetOffersAggregatorCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterGetOffersAggregatorCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachGetOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachGetOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachGetOffersAggregator} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPageView.GetOffersAggregatorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getOffersAggregator` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPageView.OffersAggregatorView) =&gt; void_ Cancels a call to `getOffersAggregator` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachGetOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregator} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregator = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachGetOffersAggregatorReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachGetOffersAggregatorReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregatorReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterGetOffersAggregatorPointcutData} [data] Metadata passed to the pointcuts of _getOffersAggregator_ at the `afterEach` joinpoint.
 * - `res` _!IOffersAggregatorPageView.OffersAggregatorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `args` _IOffersAggregatorPageView.GetOffersAggregatorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.GetOffersAggregatorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachGetOffersAggregatorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeSetOffersAggregatorCtx<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeSetOffersAggregatorCtx */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeSetOffersAggregatorCtx} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setOffersAggregatorCtx` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPage.offersAggregator.OptCtx) =&gt; void_ Cancels a call to `setOffersAggregatorCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeSetOffersAggregatorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtx<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtx */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtx} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxThrows<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `setOffersAggregatorCtx` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `afterReturns` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptCtx_ The return of the method after it's successfully run.
 * - `sub` _(value: !IOffersAggregatorPage.offersAggregator.OptCtx) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterSetOffersAggregatorCtxCancels<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterSetOffersAggregatorCtxCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterSetOffersAggregatorCtxCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEachSetOffersAggregatorCtx<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEachSetOffersAggregatorCtx */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachSetOffersAggregatorCtx} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setOffersAggregatorCtx` method from being executed.
 * - `sub` _(value: !IOffersAggregatorPage.offersAggregator.OptCtx) =&gt; void_ Cancels a call to `setOffersAggregatorCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEachSetOffersAggregatorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtx<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtx */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtx} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `after` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEachSetOffersAggregatorCtxReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEachSetOffersAggregatorCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtxReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterSetOffersAggregatorCtxPointcutData} [data] Metadata passed to the pointcuts of _setOffersAggregatorCtx_ at the `afterEach` joinpoint.
 * - `res` _!IOffersAggregatorPage.offersAggregator.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `args` _IOffersAggregatorPageView.SetOffersAggregatorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.SetOffersAggregatorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEachSetOffersAggregatorCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorPartial<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorPartial */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorPartial} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorPartialPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorPartial_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IOffersAggregatorPageView.OffersAggregatorPartialNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `OffersAggregatorPartial` method from being executed.
 * - `sub` _(value: engineering.type.VNode) =&gt; void_ Cancels a call to `OffersAggregatorPartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `args` _IOffersAggregatorPageView.OffersAggregatorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorPartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartial<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartial */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartial} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorPartialPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorPartial_ at the `after` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `args` _IOffersAggregatorPageView.OffersAggregatorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialThrows<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorPartialPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorPartial_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `OffersAggregatorPartial` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `args` _IOffersAggregatorPageView.OffersAggregatorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorPartialPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorPartial_ at the `afterReturns` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `sub` _(value: engineering.type.VNode) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `args` _IOffersAggregatorPageView.OffersAggregatorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorPartialCancels<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorPartialCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorPartialPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorPartial_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `args` _IOffersAggregatorPageView.OffersAggregatorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorPartialCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__before_OffersAggregatorView<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._before_OffersAggregatorView */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorView} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `OffersAggregatorView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.before_OffersAggregatorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorView<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorView */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorView} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewThrows<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewThrows */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterThrowsOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `OffersAggregatorView` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterReturnsOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__after_OffersAggregatorViewCancels<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._after_OffersAggregatorViewCancels */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterCancelsOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.after_OffersAggregatorViewCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__beforeEach_OffersAggregatorView<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._beforeEach_OffersAggregatorView */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEach_OffersAggregatorView} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.BeforeOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `OffersAggregatorView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.beforeEach_OffersAggregatorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorView<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorView */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorView} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData) => void} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.__afterEach_OffersAggregatorViewReturns<!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel>} xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel._afterEach_OffersAggregatorViewReturns */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorViewReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.AfterOffersAggregatorViewPointcutData} [data] Metadata passed to the pointcuts of _OffersAggregatorView_ at the `afterEach` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IOffersAggregatorPageViewJoinpointModel.OffersAggregatorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel.afterEach_OffersAggregatorViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, form: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form) => !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView.__viewOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageView>} xyz.swapee.rc.IOffersAggregatorPageView._viewOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageView.viewOffersAggregator} */
/**
 * Assigns required view data to the context, then redirects, or invokes another pagelet. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx The context.
 * - `validation` _!IOffersAggregatorPage.offersAggregator.Validation_ The validation.
 * - `errors` _!IOffersAggregatorPage.offersAggregator.Errors_ The errors.
 * - `answers` _!IOffersAggregatorPage.offersAggregator.Answers_ The answers.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form The form.
 * - `locale` _string_
 * @return {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView}
 */
xyz.swapee.rc.IOffersAggregatorPageView.viewOffersAggregator = function(ctx, form) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx, form: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form) => (void|!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView)} xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView.__getOffersAggregator<!xyz.swapee.rc.IOffersAggregatorPageView>} xyz.swapee.rc.IOffersAggregatorPageView._getOffersAggregator */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageView.getOffersAggregator} */
/**
 * The internal view handler for the `offersAggregator` action. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Ctx} ctx The context.
 * - `validation` _!IOffersAggregatorPage.offersAggregator.Validation_ The validation.
 * - `errors` _!IOffersAggregatorPage.offersAggregator.Errors_ The errors.
 * - `answers` _!IOffersAggregatorPage.offersAggregator.Answers_ The answers.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form The form.
 * - `locale` _string_
 * @return {void|!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView}
 */
xyz.swapee.rc.IOffersAggregatorPageView.getOffersAggregator = function(ctx, form) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, translation: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation) => !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx} xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView.__setOffersAggregatorCtx<!xyz.swapee.rc.IOffersAggregatorPageView>} xyz.swapee.rc.IOffersAggregatorPageView._setOffersAggregatorCtx */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageView.setOffersAggregatorCtx} */
/**
 * Assigns the context based on answers and translations. `🔗 $combine`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `changellyFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `letsExchangeFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `letsExchangeFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `changeNowFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * - `changeNowFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation The translation for the user's locale.
 * @return {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.OptCtx}
 */
xyz.swapee.rc.IOffersAggregatorPageView.setOffersAggregatorCtx = function(answers, translation) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, errors: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors, actions: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions, translation: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorPartial<!xyz.swapee.rc.IOffersAggregatorPageView>} xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorPartial */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial} */
/**
 * The partial.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `changellyFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `letsExchangeFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `letsExchangeFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `changeNowFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * - `changeNowFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions} actions The actions.
 * - `offersAggregator` _OffersAggregator.offersAggregatorNav_
 * - `_offersAggregator` _!IOffersAggregatorPage.offersAggregator.Form_
 * - `_filterChangellyFloatingOffer` _IOffersAggregatorPage.filterChangellyFloatingOffer.Form_ The form of the `filterChangellyFloatingOffer` action.
 * - `filterChangellyFloatingOffer` _!OffersAggregator.filterChangellyFloatingOfferNav_
 * - `_filterChangellyFixedOffer` _IOffersAggregatorPage.filterChangellyFixedOffer.Form_ The form of the `filterChangellyFixedOffer` action.
 * - `filterChangellyFixedOffer` _!OffersAggregator.filterChangellyFixedOfferNav_
 * - `_filterLetsExchangeFloatingOffer` _IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form_ The form of the `filterLetsExchangeFloatingOffer` action.
 * - `filterLetsExchangeFloatingOffer` _!OffersAggregator.filterLetsExchangeFloatingOfferNav_
 * - `_filterLetsExchangeFixedOffer` _IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form_ The form of the `filterLetsExchangeFixedOffer` action.
 * - `filterLetsExchangeFixedOffer` _!OffersAggregator.filterLetsExchangeFixedOfferNav_
 * - `_filterChangeNowFloatingOffer` _IOffersAggregatorPage.filterChangeNowFloatingOffer.Form_ The form of the `filterChangeNowFloatingOffer` action.
 * - `filterChangeNowFloatingOffer` _!OffersAggregator.filterChangeNowFloatingOfferNav_
 * - `_filterChangeNowFixedOffer` _IOffersAggregatorPage.filterChangeNowFixedOffer.Form_ The form of the `filterChangeNowFixedOffer` action.
 * - `filterChangeNowFixedOffer` _!OffersAggregator.filterChangeNowFixedOfferNav_
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial = function(answers, errors, actions, translation) {}

/** @typedef {xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Errors&xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Validation} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors The errors. */

/**
 * The actions.
 * @record xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions = class { }
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.offersAggregator = /** @type {xyz.swapee.rc.OffersAggregator.offersAggregatorNav} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._offersAggregator = /** @type {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} */ (void 0)
/**
 * The form of the `filterChangellyFloatingOffer` action.
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangellyFloatingOffer = /** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangellyFloatingOffer = /** @type {!xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav} */ (void 0)
/**
 * The form of the `filterChangellyFixedOffer` action.
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangellyFixedOffer = /** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangellyFixedOffer = /** @type {!xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav} */ (void 0)
/**
 * The form of the `filterLetsExchangeFloatingOffer` action.
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterLetsExchangeFloatingOffer = /** @type {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterLetsExchangeFloatingOffer = /** @type {!xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav} */ (void 0)
/**
 * The form of the `filterLetsExchangeFixedOffer` action.
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterLetsExchangeFixedOffer = /** @type {xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterLetsExchangeFixedOffer = /** @type {!xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav} */ (void 0)
/**
 * The form of the `filterChangeNowFloatingOffer` action.
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangeNowFloatingOffer = /** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangeNowFloatingOffer = /** @type {!xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav} */ (void 0)
/**
 * The form of the `filterChangeNowFixedOffer` action.
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype._filterChangeNowFixedOffer = /** @type {xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions.prototype.filterChangeNowFixedOffer = /** @type {!xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation The translation for the chosen user locale. */

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers, errors: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors, actions: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions, translation: !xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IOffersAggregatorPageView.__OffersAggregatorView<!xyz.swapee.rc.IOffersAggregatorPageView>} xyz.swapee.rc.IOffersAggregatorPageView._OffersAggregatorView */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView} */
/**
 * The _OffersAggregator_ view with partials and translation mechanics that has
 * access to answers written by the controller. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `changellyFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangellyFixedOffer.Answers*
 * - `letsExchangeFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Answers*
 * - `letsExchangeFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `letsExchangeFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterLetsExchangeFixedOffer.Answers*
 * - `changeNowFloatingOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFloatingError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFloatingOffer.Answers*
 * - `changeNowFixedOffer` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * - `changeNowFixedError` _&#42;_ ⤴ *IOffersAggregatorPage.filterChangeNowFixedOffer.Answers*
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Actions} actions The actions.
 * - `offersAggregator` _OffersAggregator.offersAggregatorNav_
 * - `_offersAggregator` _!IOffersAggregatorPage.offersAggregator.Form_
 * - `_filterChangellyFloatingOffer` _IOffersAggregatorPage.filterChangellyFloatingOffer.Form_ The form of the `filterChangellyFloatingOffer` action.
 * - `filterChangellyFloatingOffer` _!OffersAggregator.filterChangellyFloatingOfferNav_
 * - `_filterChangellyFixedOffer` _IOffersAggregatorPage.filterChangellyFixedOffer.Form_ The form of the `filterChangellyFixedOffer` action.
 * - `filterChangellyFixedOffer` _!OffersAggregator.filterChangellyFixedOfferNav_
 * - `_filterLetsExchangeFloatingOffer` _IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form_ The form of the `filterLetsExchangeFloatingOffer` action.
 * - `filterLetsExchangeFloatingOffer` _!OffersAggregator.filterLetsExchangeFloatingOfferNav_
 * - `_filterLetsExchangeFixedOffer` _IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form_ The form of the `filterLetsExchangeFixedOffer` action.
 * - `filterLetsExchangeFixedOffer` _!OffersAggregator.filterLetsExchangeFixedOfferNav_
 * - `_filterChangeNowFloatingOffer` _IOffersAggregatorPage.filterChangeNowFloatingOffer.Form_ The form of the `filterChangeNowFloatingOffer` action.
 * - `filterChangeNowFloatingOffer` _!OffersAggregator.filterChangeNowFloatingOfferNav_
 * - `_filterChangeNowFixedOffer` _IOffersAggregatorPage.filterChangeNowFixedOffer.Form_ The form of the `filterChangeNowFixedOffer` action.
 * - `filterChangeNowFixedOffer` _!OffersAggregator.filterChangeNowFixedOfferNav_
 * @param {!xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorPartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IOffersAggregatorPageView.OffersAggregatorView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.IOffersAggregatorPageViewJoinpointModel,xyz.swapee.rc.IOffersAggregatorPageView
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml}  ef52c7010e8070821695b2fff28031c9 */
/**
 * The `offersAggregator` navigation metadata.
 * @constructor xyz.swapee.rc.OffersAggregator.OffersAggregatorNav
 */
xyz.swapee.rc.OffersAggregator.OffersAggregatorNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.OffersAggregator.OffersAggregatorNav.prototype.offersAggregator = /** @type {number} */ (void 0)

/**
 * The `filterChangellyFloatingOffer` navigation metadata.
 * @constructor xyz.swapee.rc.OffersAggregator.FilterChangellyFloatingOfferNav
 */
xyz.swapee.rc.OffersAggregator.FilterChangellyFloatingOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.OffersAggregator.FilterChangellyFloatingOfferNav.prototype.filterChangellyFloatingOffer = /** @type {number} */ (void 0)

/**
 * The `filterChangellyFixedOffer` navigation metadata.
 * @constructor xyz.swapee.rc.OffersAggregator.FilterChangellyFixedOfferNav
 */
xyz.swapee.rc.OffersAggregator.FilterChangellyFixedOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.OffersAggregator.FilterChangellyFixedOfferNav.prototype.filterChangellyFixedOffer = /** @type {number} */ (void 0)

/**
 * The `filterLetsExchangeFloatingOffer` navigation metadata.
 * @constructor xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav
 */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav.prototype.filterLetsExchangeFloatingOffer = /** @type {number} */ (void 0)

/**
 * The `filterLetsExchangeFixedOffer` navigation metadata.
 * @constructor xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav
 */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFixedOfferNav.prototype.filterLetsExchangeFixedOffer = /** @type {number} */ (void 0)

/**
 * The `filterChangeNowFloatingOffer` navigation metadata.
 * @constructor xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav
 */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav.prototype.filterChangeNowFloatingOffer = /** @type {number} */ (void 0)

/**
 * The `filterChangeNowFixedOffer` navigation metadata.
 * @constructor xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav
 */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.OffersAggregator.FilterChangeNowFixedOfferNav.prototype.filterChangeNowFixedOffer = /** @type {number} */ (void 0)

/**
 * The ids of the methods.
 * @constructor xyz.swapee.rc.offersAggregatorMethodsIds
 */
xyz.swapee.rc.offersAggregatorMethodsIds = class { }
xyz.swapee.rc.offersAggregatorMethodsIds.prototype.constructor = xyz.swapee.rc.offersAggregatorMethodsIds

/**
 * The ids of the arcs.
 * @constructor xyz.swapee.rc.offersAggregatorArcsIds
 */
xyz.swapee.rc.offersAggregatorArcsIds = class { }
xyz.swapee.rc.offersAggregatorArcsIds.prototype.constructor = xyz.swapee.rc.offersAggregatorArcsIds

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IOffersAggregatorImpl.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.OffersAggregatorImpl)} xyz.swapee.rc.AbstractOffersAggregatorImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.OffersAggregatorImpl} xyz.swapee.rc.OffersAggregatorImpl.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IOffersAggregatorImpl` interface.
 * @constructor xyz.swapee.rc.AbstractOffersAggregatorImpl
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl = class extends /** @type {xyz.swapee.rc.AbstractOffersAggregatorImpl.constructor&xyz.swapee.rc.OffersAggregatorImpl.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractOffersAggregatorImpl.prototype.constructor = xyz.swapee.rc.AbstractOffersAggregatorImpl
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.class = /** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorImpl} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorImpl|typeof xyz.swapee.rc.OffersAggregatorImpl)|(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractOffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorImpl|typeof xyz.swapee.rc.OffersAggregatorImpl)|(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IOffersAggregatorImpl|typeof xyz.swapee.rc.OffersAggregatorImpl)|(!xyz.swapee.rc.IOffersAggregatorPageAspects|typeof xyz.swapee.rc.OffersAggregatorPageAspects)|(!xyz.swapee.rc.IOffersAggregatorPage|typeof xyz.swapee.rc.OffersAggregatorPage)|(!xyz.swapee.rc.IOffersAggregatorPageHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageHyperslice)|(!xyz.swapee.rc.IOffersAggregatorPageViewHyperslice|typeof xyz.swapee.rc.OffersAggregatorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.AbstractOffersAggregatorImpl.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IOffersAggregatorPageAspects&xyz.swapee.rc.IOffersAggregatorPage&xyz.swapee.rc.IOffersAggregatorPageHyperslice&xyz.swapee.rc.IOffersAggregatorPageViewHyperslice)} xyz.swapee.rc.IOffersAggregatorImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageHyperslice} xyz.swapee.rc.IOffersAggregatorPageHyperslice.typeof */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorPageViewHyperslice} xyz.swapee.rc.IOffersAggregatorPageViewHyperslice.typeof */
/** @interface xyz.swapee.rc.IOffersAggregatorImpl */
xyz.swapee.rc.IOffersAggregatorImpl = class extends /** @type {xyz.swapee.rc.IOffersAggregatorImpl.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IOffersAggregatorPageAspects.typeof&xyz.swapee.rc.IOffersAggregatorPage.typeof&xyz.swapee.rc.IOffersAggregatorPageHyperslice.typeof&xyz.swapee.rc.IOffersAggregatorPageViewHyperslice.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersAggregatorImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IOffersAggregatorImpl.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IOffersAggregatorImpl&engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorImpl.Initialese>)} xyz.swapee.rc.OffersAggregatorImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IOffersAggregatorImpl} xyz.swapee.rc.IOffersAggregatorImpl.typeof */
/**
 * A concrete class of _IOffersAggregatorImpl_ instances.
 * @constructor xyz.swapee.rc.OffersAggregatorImpl
 * @implements {xyz.swapee.rc.IOffersAggregatorImpl} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IOffersAggregatorImpl.Initialese>} ‎
 */
xyz.swapee.rc.OffersAggregatorImpl = class extends /** @type {xyz.swapee.rc.OffersAggregatorImpl.constructor&xyz.swapee.rc.IOffersAggregatorImpl.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersAggregatorImpl* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IOffersAggregatorImpl.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersAggregatorImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IOffersAggregatorImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.OffersAggregatorImpl.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.OffersAggregatorImpl}
 */
xyz.swapee.rc.OffersAggregatorImpl.__extend = function(...Extensions) {}

/** @typedef {typeof xyz.swapee.rc.OffersAggregator.offersAggregatorNav} */
/**
 * Navigates to `offersAggregator`.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form} form
 * - `locale` _string_
 * @return {void}
 */
xyz.swapee.rc.OffersAggregator.offersAggregatorNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav} */
/**
 * Invokes the `filterChangellyFloatingOffer` action.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav} */
/**
 * Invokes the `filterChangellyFixedOffer` action.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav} */
/**
 * Invokes the `filterLetsExchangeFloatingOffer` action.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav} */
/**
 * Invokes the `filterLetsExchangeFixedOffer` action.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav} */
/**
 * Invokes the `filterChangeNowFloatingOffer` action.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav} */
/**
 * Invokes the `filterChangeNowFixedOffer` action.
 * @param {!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.getOffersAggregator} */
/**
 * Allows to render the pagelet after it's been processed.
 * @return {void}
 */
xyz.swapee.rc.getOffersAggregator = function() {}

/** @typedef {typeof xyz.swapee.rc.offersAggregator} */
/**
 * @param {!xyz.swapee.rc.IOffersAggregatorImpl} implementation
 * @return {!xyz.swapee.rc.IOffersAggregatorPage}
 */
xyz.swapee.rc.offersAggregator = function(implementation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.OffersAggregator
/* @typal-end */