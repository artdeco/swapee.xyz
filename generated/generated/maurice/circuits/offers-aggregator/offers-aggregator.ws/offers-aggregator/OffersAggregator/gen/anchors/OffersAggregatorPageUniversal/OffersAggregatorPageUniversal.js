import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersAggregatorPageUniversal}
 */
function __OffersAggregatorPageUniversal() {}
__OffersAggregatorPageUniversal.prototype = /** @type {!_OffersAggregatorPageUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageUniversal}
 */
class _OffersAggregatorPageUniversal { }
/**
 * A trait that allows to access an _IOffersAggregatorPage_ object via a `.offersAggregatorPage` field.
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageUniversal} ‎
 */
class OffersAggregatorPageUniversal extends newAbstract(
 _OffersAggregatorPageUniversal,'UOffersAggregatorPage',null,{
  asIOffersAggregatorPage:1,
  asUOffersAggregatorPage:3,
  offersAggregatorPage:4,
  asOffersAggregatorPage:5,
  superOffersAggregatorPageUniversal:2,
 },false,{
  offersAggregatorPage:{_offersAggregatorPage:'_offersAggregatorPage'},
 }) {}

/** @type {typeof xyz.swapee.rc.UOffersAggregatorPage} */
OffersAggregatorPageUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UOffersAggregatorPage} */
function OffersAggregatorPageUniversalClass(){}

export default OffersAggregatorPageUniversal

/** @type {xyz.swapee.rc.UOffersAggregatorPage.Symbols} */
export const OffersAggregatorPageUniversalSymbols=OffersAggregatorPageUniversal.Symbols
/** @type {xyz.swapee.rc.UOffersAggregatorPage.getSymbols} */
export const getOffersAggregatorPageUniversalSymbols=OffersAggregatorPageUniversal.getSymbols
/** @type {xyz.swapee.rc.UOffersAggregatorPage.setSymbols} */
export const setOffersAggregatorPageUniversalSymbols=OffersAggregatorPageUniversal.setSymbols

OffersAggregatorPageUniversal[$implementations]=[
 __OffersAggregatorPageUniversal,
 OffersAggregatorPageUniversalClass.prototype=/**@type {!xyz.swapee.rc.UOffersAggregatorPage}*/({
  [OffersAggregatorPageUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UOffersAggregatorPage} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UOffersAggregatorPage.Initialese}*/({
   offersAggregatorPage:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.OffersAggregatorPageMetaUniversal}*/
export const OffersAggregatorPageMetaUniversal=OffersAggregatorPageUniversal.MetaUniversal