import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.OffersAggregator.FilterLetsExchangeFloatingOfferNav} */
export const FilterLetsExchangeFloatingOfferNav={
 toString(){return'filterLetsExchangeFloatingOffer'},
 filterLetsExchangeFloatingOffer:methodsIds.filterLetsExchangeFloatingOffer,
}
export default FilterLetsExchangeFloatingOfferNav