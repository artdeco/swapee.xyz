import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.OffersAggregator.FilterChangeNowFloatingOfferNav} */
export const FilterChangeNowFloatingOfferNav={
 toString(){return'filterChangeNowFloatingOffer'},
 filterChangeNowFloatingOffer:methodsIds.filterChangeNowFloatingOffer,
}
export default FilterChangeNowFloatingOfferNav