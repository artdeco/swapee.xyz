import OffersAggregatorPageViewUniversal, {setOffersAggregatorPageViewUniversalSymbols} from './anchors/OffersAggregatorPageViewUniversal'
import { newAbstract, $implementations, prereturnFirst, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorPageView}
 */
function __AbstractOffersAggregatorPageView() {}
__AbstractOffersAggregatorPageView.prototype = /** @type {!_AbstractOffersAggregatorPageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPageView}
 */
class _AbstractOffersAggregatorPageView { }
/** @extends {xyz.swapee.rc.AbstractOffersAggregatorPageView} ‎ */
class AbstractOffersAggregatorPageView extends newAbstract(
 _AbstractOffersAggregatorPageView,'IOffersAggregatorPageView',null,{
  asIOffersAggregatorPageView:1,
  superOffersAggregatorPageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageView} */
AbstractOffersAggregatorPageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPageView} */
function AbstractOffersAggregatorPageViewClass(){}

export default AbstractOffersAggregatorPageView


AbstractOffersAggregatorPageView[$implementations]=[
 __AbstractOffersAggregatorPageView,
 OffersAggregatorPageViewUniversal,
 AbstractOffersAggregatorPageViewClass.prototype=/**@type {!xyz.swapee.rc.IOffersAggregatorPageView}*/({
  viewOffersAggregator:prereturnFirst,
  getOffersAggregator:precombined,
  setOffersAggregatorCtx:precombined,
  OffersAggregatorView:prereturnFirst,
 }),
]