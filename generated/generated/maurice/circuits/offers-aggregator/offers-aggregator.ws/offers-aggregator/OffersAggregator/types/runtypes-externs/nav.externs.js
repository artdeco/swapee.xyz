/**
 * @fileoverview
 * @externs
 */

/** @const */
var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
xyz.swapee.rc.IOffersAggregatorPage={}
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer={}
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form = function() {}
/** @type {string} */
xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/IOffersAggregatorPage.xml} xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form a7c66343ca2b0911b260ff795094988e */
/** @record */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form.prototype.currencyTo

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.offersAggregatorNav only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form): void} */
xyz.swapee.rc.OffersAggregator.offersAggregatorNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangellyFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangellyFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterLetsExchangeFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterLetsExchangeFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangeNowFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator/design/api.xml} xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav only:OffersAggregator.offersAggregatorNav,xyz.swapee.rc.IOffersAggregatorPage.offersAggregator.Form,OffersAggregator.filterChangellyFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFloatingOffer.Form,OffersAggregator.filterChangellyFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangellyFixedOffer.Form,OffersAggregator.filterLetsExchangeFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFloatingOffer.Form,OffersAggregator.filterLetsExchangeFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterLetsExchangeFixedOffer.Form,OffersAggregator.filterChangeNowFloatingOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFloatingOffer.Form,OffersAggregator.filterChangeNowFixedOfferNav,xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form ef52c7010e8070821695b2fff28031c9 */
/** @typedef {function(!xyz.swapee.rc.IOffersAggregatorPage.filterChangeNowFixedOffer.Form): void} */
xyz.swapee.rc.OffersAggregator.filterChangeNowFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */