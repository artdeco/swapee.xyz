import methodsIds from '../../../gen/methods-ids'
/** @type {xyz.swapee.rc.OffersAggregator.OffersAggregatorNav} */
export const OffersAggregatorNav={
 toString(){return'offersAggregator'},
 offersAggregator:methodsIds.offersAggregator,
}
export default OffersAggregatorNav