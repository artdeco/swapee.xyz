import OffersAggregatorPageUniversal, {setOffersAggregatorPageUniversalSymbols} from './anchors/OffersAggregatorPageUniversal'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersAggregatorPage}
 */
function __AbstractOffersAggregatorPage() {}
__AbstractOffersAggregatorPage.prototype = /** @type {!_AbstractOffersAggregatorPage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractOffersAggregatorPage}
 */
class _AbstractOffersAggregatorPage { }
/** @extends {xyz.swapee.rc.AbstractOffersAggregatorPage} ‎ */
class AbstractOffersAggregatorPage extends newAbstract(
 _AbstractOffersAggregatorPage,'IOffersAggregatorPage',null,{
  asIOffersAggregatorPage:1,
  superOffersAggregatorPage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPage} */
AbstractOffersAggregatorPage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractOffersAggregatorPage} */
function AbstractOffersAggregatorPageClass(){}

export default AbstractOffersAggregatorPage


AbstractOffersAggregatorPage[$implementations]=[
 __AbstractOffersAggregatorPage,
 OffersAggregatorPageUniversal,
 AbstractOffersAggregatorPageClass.prototype=/**@type {!xyz.swapee.rc.IOffersAggregatorPage}*/({
  offersAggregator:precombined,
  filterChangellyFloatingOffer:precombined,
  filterChangellyFixedOffer:precombined,
  filterLetsExchangeFloatingOffer:precombined,
  filterLetsExchangeFixedOffer:precombined,
  filterChangeNowFloatingOffer:precombined,
  filterChangeNowFixedOffer:precombined,
 }),
]