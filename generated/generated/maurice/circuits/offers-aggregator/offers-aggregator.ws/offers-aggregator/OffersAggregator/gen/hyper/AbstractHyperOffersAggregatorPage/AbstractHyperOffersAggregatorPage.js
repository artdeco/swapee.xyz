import OffersAggregatorPageAspectsInstaller from '../../aspects-installers/OffersAggregatorPageAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperOffersAggregatorPage}
 */
function __AbstractHyperOffersAggregatorPage() {}
__AbstractHyperOffersAggregatorPage.prototype = /** @type {!_AbstractHyperOffersAggregatorPage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperOffersAggregatorPage}
 */
class _AbstractHyperOffersAggregatorPage { }
/** @extends {xyz.swapee.rc.AbstractHyperOffersAggregatorPage} ‎ */
class AbstractHyperOffersAggregatorPage extends newAbstract(
 _AbstractHyperOffersAggregatorPage,'IHyperOffersAggregatorPage',null,{
  asIHyperOffersAggregatorPage:OffersAggregatorPageAspectsInstaller,
  superHyperOffersAggregatorPage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage} */
AbstractHyperOffersAggregatorPage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperOffersAggregatorPage} */
function AbstractHyperOffersAggregatorPageClass(){}

export default AbstractHyperOffersAggregatorPage


AbstractHyperOffersAggregatorPage[$implementations]=[
 __AbstractHyperOffersAggregatorPage,
]