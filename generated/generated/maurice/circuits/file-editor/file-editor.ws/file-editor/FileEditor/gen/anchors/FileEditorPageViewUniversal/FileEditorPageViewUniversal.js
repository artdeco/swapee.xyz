import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorPageViewUniversal}
 */
function __FileEditorPageViewUniversal() {}
__FileEditorPageViewUniversal.prototype = /** @type {!_FileEditorPageViewUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPageViewUniversal}
 */
class _FileEditorPageViewUniversal { }
/**
 * A trait that allows to access an _IFileEditorPageView_ object via a `.fileEditorPageView` field.
 * @extends {xyz.swapee.rc.AbstractFileEditorPageViewUniversal} ‎
 */
class FileEditorPageViewUniversal extends newAbstract(
 _FileEditorPageViewUniversal,399991590216,null,{
  asIFileEditorPageView:1,
  asUFileEditorPageView:3,
  fileEditorPageView:4,
  asFileEditorPageView:5,
  superFileEditorPageViewUniversal:2,
 },false,{
  fileEditorPageView:{_fileEditorPageView:1},
 }) {}

/** @type {typeof xyz.swapee.rc.UFileEditorPageView} */
FileEditorPageViewUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UFileEditorPageView} */
function FileEditorPageViewUniversalClass(){}

export default FileEditorPageViewUniversal

/** @type {xyz.swapee.rc.UFileEditorPageView.Symbols} */
export const FileEditorPageViewUniversalSymbols=FileEditorPageViewUniversal.Symbols
/** @type {xyz.swapee.rc.UFileEditorPageView.getSymbols} */
export const getFileEditorPageViewUniversalSymbols=FileEditorPageViewUniversal.getSymbols
/** @type {xyz.swapee.rc.UFileEditorPageView.setSymbols} */
export const setFileEditorPageViewUniversalSymbols=FileEditorPageViewUniversal.setSymbols

FileEditorPageViewUniversal[$implementations]=[
 __FileEditorPageViewUniversal,
 FileEditorPageViewUniversalClass.prototype=/**@type {!xyz.swapee.rc.UFileEditorPageView}*/({
  [FileEditorPageViewUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UFileEditorPageView} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UFileEditorPageView.Initialese}*/({
   fileEditorPageView:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.FileEditorPageViewMetaUniversal}*/
export const FileEditorPageViewMetaUniversal=FileEditorPageViewUniversal.MetaUniversal