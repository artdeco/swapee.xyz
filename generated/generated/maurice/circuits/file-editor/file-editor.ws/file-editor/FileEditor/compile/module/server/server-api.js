import { FileEditorPage, methodsIds, arcsIds, getFileEditor, FileEditorPageView,
 HyperFileEditorPageView, FileEditorNav, FilterReadFileNav, FilterSaveFileNav } from './server-exports'

/** @lazy @api {xyz.swapee.rc.FileEditorPage} */
export { FileEditorPage }
/** @lazy @api {xyz.swapee.rc.fileEditorMethodsIds} */
export { methodsIds }
/** @lazy @api {xyz.swapee.rc.fileEditorArcsIds} */
export { arcsIds }
/** @lazy @api {xyz.swapee.rc.getFileEditor} */
export { getFileEditor }
/** @lazy @api {xyz.swapee.rc.FileEditorPageView} */
export { FileEditorPageView }
/** @lazy @api {xyz.swapee.rc.HyperFileEditorPageView} */
export { HyperFileEditorPageView }
/** @lazy @api {xyz.swapee.rc.FileEditor.FileEditorNav} */
export { FileEditorNav }
/** @lazy @api {xyz.swapee.rc.FileEditor.FilterReadFileNav} */
export { FilterReadFileNav }
/** @lazy @api {xyz.swapee.rc.FileEditor.FilterSaveFileNav} */
export { FilterSaveFileNav }