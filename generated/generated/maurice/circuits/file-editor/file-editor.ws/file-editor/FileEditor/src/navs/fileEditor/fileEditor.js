import methodsIds from '../../../gen/methods-ids'
/** @type {xyz.swapee.rc.FileEditor.FileEditorNav} */
export const FileEditorNav={
 toString(){return'fileEditor'},
 fileEditor:methodsIds.fileEditor,
}
export default FileEditorNav