import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorPageAspectsInstaller}
 */
function __FileEditorPageAspectsInstaller() {}
__FileEditorPageAspectsInstaller.prototype = /** @type {!_FileEditorPageAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller}
 */
class _FileEditorPageAspectsInstaller { }

_FileEditorPageAspectsInstaller.prototype[$advice]=__FileEditorPageAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller} ‎ */
class FileEditorPageAspectsInstaller extends newAbstract(
 _FileEditorPageAspectsInstaller,39999159024,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller} */
FileEditorPageAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller} */
function FileEditorPageAspectsInstallerClass(){}

export default FileEditorPageAspectsInstaller


FileEditorPageAspectsInstaller[$implementations]=[
 FileEditorPageAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IFileEditorPageAspectsInstaller}*/({
  fileEditor(){
   this.beforeFileEditor=1
   this.afterFileEditor=2
   this.aroundFileEditor=3
   this.afterFileEditorThrows=4
   this.afterFileEditorReturns=5
   this.afterFileEditorCancels=7
   this.beforeEachFileEditor=8
   this.afterEachFileEditor=9
   this.afterEachFileEditorReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterReadFile(){
   this.beforeFilterReadFile=1
   this.afterFilterReadFile=2
   this.aroundFilterReadFile=3
   this.afterFilterReadFileThrows=4
   this.afterFilterReadFileReturns=5
   this.afterFilterReadFileCancels=7
   this.beforeEachFilterReadFile=8
   this.afterEachFilterReadFile=9
   this.afterEachFilterReadFileReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterSaveFile(){
   this.beforeFilterSaveFile=1
   this.afterFilterSaveFile=2
   this.aroundFilterSaveFile=3
   this.afterFilterSaveFileThrows=4
   this.afterFilterSaveFileReturns=5
   this.afterFilterSaveFileCancels=7
   this.beforeEachFilterSaveFile=8
   this.afterEachFilterSaveFile=9
   this.afterEachFilterSaveFileReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
 }),
 __FileEditorPageAspectsInstaller,
]