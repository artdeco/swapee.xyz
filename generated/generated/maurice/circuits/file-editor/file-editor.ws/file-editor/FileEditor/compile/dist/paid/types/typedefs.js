/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
xyz.swapee.rc.IFileEditorPage={}
xyz.swapee.rc.IFileEditorPage.fileEditor={}
xyz.swapee.rc.IFileEditorPage.filterReadFile={}
xyz.swapee.rc.IFileEditorPage.filterSaveFile={}
xyz.swapee.rc.IFileEditorPageAspectsInstaller={}
xyz.swapee.rc.IFileEditorPageJoinpointModel={}
xyz.swapee.rc.UFileEditorPage={}
xyz.swapee.rc.AbstractFileEditorPageUniversal={}
xyz.swapee.rc.IFileEditorPageAspects={}
xyz.swapee.rc.IHyperFileEditorPage={}
xyz.swapee.rc.FileEditor={}
xyz.swapee.rc.FileEditor.Answers={}
xyz.swapee.rc.FileEditor.Form={}
xyz.swapee.rc.FileEditor.Errors={}
xyz.swapee.rc.FileEditor.Validation={}
xyz.swapee.rc.FileEditor.Ctx={}
xyz.swapee.rc.IFileEditorPageView={}
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial={}
xyz.swapee.rc.IFileEditorPageViewAspectsInstaller={}
xyz.swapee.rc.IFileEditorPageViewJoinpointModel={}
xyz.swapee.rc.UFileEditorPageView={}
xyz.swapee.rc.AbstractFileEditorPageViewUniversal={}
xyz.swapee.rc.IFileEditorPageViewAspects={}
xyz.swapee.rc.IHyperFileEditorPageView={}
xyz.swapee.rc.IFileEditorImpl={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-editor/file-editor.ws/file-editor/FileEditor/design/IFileEditorPage.xml}  ca46a8d51adbd03f62cf2fc14a323653 */
/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorPage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPage)} xyz.swapee.rc.AbstractFileEditorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPage} xyz.swapee.rc.FileEditorPage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileEditorPage` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPage
 */
xyz.swapee.rc.AbstractFileEditorPage = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPage.constructor&xyz.swapee.rc.FileEditorPage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPage.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPage.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.UFileEditorPage|typeof xyz.swapee.rc.UFileEditorPage)|!xyz.swapee.rc.IFileEditorPageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPage}
 */
xyz.swapee.rc.AbstractFileEditorPage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPage}
 */
xyz.swapee.rc.AbstractFileEditorPage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.UFileEditorPage|typeof xyz.swapee.rc.UFileEditorPage)|!xyz.swapee.rc.IFileEditorPageHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPage}
 */
xyz.swapee.rc.AbstractFileEditorPage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.UFileEditorPage|typeof xyz.swapee.rc.UFileEditorPage)|!xyz.swapee.rc.IFileEditorPageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPage}
 */
xyz.swapee.rc.AbstractFileEditorPage.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageJoinpointModelHyperslice
 */
xyz.swapee.rc.IFileEditorPageJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFileEditor>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditor>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFileEditorThrows=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFileEditorCancels=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFileEditor>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFileEditor>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFileEditorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFileEditorReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFilterReadFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFilterReadFile>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFile>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterReadFileThrows=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterReadFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterReadFileCancels=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFilterReadFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFilterReadFile>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterReadFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterReadFile>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterReadFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterReadFileReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterReadFileReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFilterSaveFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFilterSaveFile>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFile>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterSaveFileThrows=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterSaveFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterSaveFileCancels=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFilterSaveFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFilterSaveFile>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterSaveFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterSaveFile>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterSaveFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterSaveFileReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterSaveFileReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageJoinpointModelHyperslice

/**
 * A concrete class of _IFileEditorPageJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileEditorPageJoinpointModelHyperslice = class extends xyz.swapee.rc.IFileEditorPageJoinpointModelHyperslice { }
xyz.swapee.rc.FileEditorPageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFileEditor<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditor<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFileEditorThrows=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFileEditorCancels=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFileEditor<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditor<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditorReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterReadFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterReadFile<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFile<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterReadFileThrows=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterReadFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterReadFileCancels=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterReadFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterReadFile<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFile<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterReadFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFileReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFileReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterSaveFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterSaveFile<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFile<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterSaveFileThrows=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterSaveFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterSaveFileCancels=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterSaveFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterSaveFile<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFile<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterSaveFileReturns=/** @type {!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFileReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFileReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice

/**
 * A concrete class of _IFileEditorPageJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileEditorPageJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice { }
xyz.swapee.rc.FileEditorPageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IFileEditorPage`'s methods.
 * @interface xyz.swapee.rc.IFileEditorPageJoinpointModel
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel = class { }
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFileEditor} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.beforeFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditor} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorThrows} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFileEditorThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorReturns} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFileEditorReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorCancels} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFileEditorCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFileEditor} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.beforeEachFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFileEditor} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterEachFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFileEditorReturns} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterEachFileEditorReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFilterReadFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.beforeFilterReadFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterReadFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileThrows} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterReadFileThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileReturns} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterReadFileReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileCancels} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterReadFileCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFilterReadFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.beforeEachFilterReadFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterReadFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterEachFilterReadFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterReadFileReturns} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterEachFilterReadFileReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFilterSaveFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.beforeFilterSaveFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterSaveFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileThrows} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterSaveFileThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileReturns} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterSaveFileReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileCancels} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterFilterSaveFileCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFilterSaveFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.beforeEachFilterSaveFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterSaveFile} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterEachFilterSaveFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterSaveFileReturns} */
xyz.swapee.rc.IFileEditorPageJoinpointModel.prototype.afterEachFilterSaveFileReturns = function() {}

/**
 * A concrete class of _IFileEditorPageJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageJoinpointModel
 * @implements {xyz.swapee.rc.IFileEditorPageJoinpointModel} An interface that enumerates the joinpoints of `IFileEditorPage`'s methods.
 */
xyz.swapee.rc.FileEditorPageJoinpointModel = class extends xyz.swapee.rc.IFileEditorPageJoinpointModel { }
xyz.swapee.rc.FileEditorPageJoinpointModel.prototype.constructor = xyz.swapee.rc.FileEditorPageJoinpointModel

/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel} */
xyz.swapee.rc.RecordIFileEditorPageJoinpointModel

/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel} xyz.swapee.rc.BoundIFileEditorPageJoinpointModel */

/** @typedef {xyz.swapee.rc.FileEditorPageJoinpointModel} xyz.swapee.rc.BoundFileEditorPageJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorPageAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPageAspectsInstaller)} xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPageAspectsInstaller} xyz.swapee.rc.FileEditorPageAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileEditorPageAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller
 */
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.constructor&xyz.swapee.rc.FileEditorPageAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspectsInstaller|typeof xyz.swapee.rc.FileEditorPageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspectsInstaller|typeof xyz.swapee.rc.FileEditorPageAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspectsInstaller|typeof xyz.swapee.rc.FileEditorPageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileEditorPageAspectsInstaller.Initialese[]) => xyz.swapee.rc.IFileEditorPageAspectsInstaller} xyz.swapee.rc.FileEditorPageAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.rc.IFileEditorPageAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.rc.IFileEditorPageAspectsInstaller */
xyz.swapee.rc.IFileEditorPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.IFileEditorPageAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeFileEditor=/** @type {number} */ (void 0)
    this.afterFileEditor=/** @type {number} */ (void 0)
    this.afterFileEditorThrows=/** @type {number} */ (void 0)
    this.afterFileEditorReturns=/** @type {number} */ (void 0)
    this.afterFileEditorCancels=/** @type {number} */ (void 0)
    this.beforeEachFileEditor=/** @type {number} */ (void 0)
    this.afterEachFileEditor=/** @type {number} */ (void 0)
    this.afterEachFileEditorReturns=/** @type {number} */ (void 0)
    this.beforeFilterReadFile=/** @type {number} */ (void 0)
    this.afterFilterReadFile=/** @type {number} */ (void 0)
    this.afterFilterReadFileThrows=/** @type {number} */ (void 0)
    this.afterFilterReadFileReturns=/** @type {number} */ (void 0)
    this.afterFilterReadFileCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterReadFile=/** @type {number} */ (void 0)
    this.afterEachFilterReadFile=/** @type {number} */ (void 0)
    this.afterEachFilterReadFileReturns=/** @type {number} */ (void 0)
    this.beforeFilterSaveFile=/** @type {number} */ (void 0)
    this.afterFilterSaveFile=/** @type {number} */ (void 0)
    this.afterFilterSaveFileThrows=/** @type {number} */ (void 0)
    this.afterFilterSaveFileReturns=/** @type {number} */ (void 0)
    this.afterFilterSaveFileCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterSaveFile=/** @type {number} */ (void 0)
    this.afterEachFilterSaveFile=/** @type {number} */ (void 0)
    this.afterEachFilterSaveFileReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  fileEditor() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterReadFile() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterSaveFile() { }
}
/**
 * Create a new *IFileEditorPageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileEditorPageAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPageAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageAspectsInstaller.Initialese>)} xyz.swapee.rc.FileEditorPageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageAspectsInstaller} xyz.swapee.rc.IFileEditorPageAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileEditorPageAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageAspectsInstaller
 * @implements {xyz.swapee.rc.IFileEditorPageAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.FileEditorPageAspectsInstaller.constructor&xyz.swapee.rc.IFileEditorPageAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorPageAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileEditorPageAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorPageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPageAspectsInstaller}
 */
xyz.swapee.rc.FileEditorPageAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPage.FileEditorNArgs
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} form The form.
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Validation} validation The validation.
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx} ctx The ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageJoinpointModel.FileEditorPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileEditorPage.FileEditorNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFileEditorPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileEditorPage.FileEditorNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `fileEditor` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers>) => void} sub Cancels a call to `fileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FileEditorPointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFileEditorPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FileEditorPointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFileEditorPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `fileEditor` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FileEditorPointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFileEditorPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFileEditorPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FileEditorPointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFileEditorPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFileEditorPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FileEditorPointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFileEditorPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPage.FilterReadFileNArgs
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Form} form The action form.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterReadFilePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileEditorPage.FilterReadFileNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterReadFilePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileEditorPage.FilterReadFileNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterReadFile` method from being executed.
 * @prop {(value: xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers>) => void} sub Cancels a call to `filterReadFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterReadFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterReadFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterReadFilePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData
 * @prop {xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterReadFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterReadFilePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterReadFile` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterReadFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterReadFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterReadFilePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterReadFilePointcutData
 * @prop {xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterReadFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterReadFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterReadFilePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterReadFilePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterReadFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterReadFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterReadFilePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPage.FilterSaveFileNArgs
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form} form The action form.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterSaveFilePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileEditorPage.FilterSaveFileNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterSaveFilePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileEditorPage.FilterSaveFileNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterSaveFile` method from being executed.
 * @prop {(value: xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers>) => void} sub Cancels a call to `filterSaveFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterSaveFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterSaveFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterSaveFilePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData
 * @prop {xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterSaveFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterSaveFilePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterSaveFile` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterSaveFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterSaveFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterSaveFilePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterSaveFilePointcutData
 * @prop {xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterSaveFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterSaveFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterSaveFilePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterSaveFilePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterSaveFilePointcutData&xyz.swapee.rc.IFileEditorPageJoinpointModel.FilterSaveFilePointcutData} xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterSaveFilePointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IFileEditorPage.Initialese[]) => xyz.swapee.rc.IFileEditorPage} xyz.swapee.rc.FileEditorPageConstructor */

/** @typedef {symbol} xyz.swapee.rc.FileEditorPageMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UFileEditorPage.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IFileEditorPage} [fileEditorPage]
 */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPageUniversal)} xyz.swapee.rc.AbstractFileEditorPageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPageUniversal} xyz.swapee.rc.FileEditorPageUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UFileEditorPage` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPageUniversal
 */
xyz.swapee.rc.AbstractFileEditorPageUniversal = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPageUniversal.constructor&xyz.swapee.rc.FileEditorPageUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPageUniversal.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPageUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPageUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPageUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UFileEditorPage|typeof xyz.swapee.rc.UFileEditorPage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPageUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPageUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UFileEditorPage|typeof xyz.swapee.rc.UFileEditorPage} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UFileEditorPage|typeof xyz.swapee.rc.UFileEditorPage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.FileEditorPageMetaUniversal} xyz.swapee.rc.AbstractFileEditorPageUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UFileEditorPage.Initialese[]) => xyz.swapee.rc.UFileEditorPage} xyz.swapee.rc.UFileEditorPageConstructor */

/** @typedef {function(new: xyz.swapee.rc.UFileEditorPageFields&engineering.type.IEngineer&xyz.swapee.rc.UFileEditorPageCaster)} xyz.swapee.rc.UFileEditorPage.constructor */
/**
 * A trait that allows to access an _IFileEditorPage_ object via a `.fileEditorPage` field.
 * @interface xyz.swapee.rc.UFileEditorPage
 */
xyz.swapee.rc.UFileEditorPage = class extends /** @type {xyz.swapee.rc.UFileEditorPage.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UFileEditorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileEditorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UFileEditorPage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UFileEditorPage&engineering.type.IInitialiser<!xyz.swapee.rc.UFileEditorPage.Initialese>)} xyz.swapee.rc.FileEditorPageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UFileEditorPage} xyz.swapee.rc.UFileEditorPage.typeof */
/**
 * A concrete class of _UFileEditorPage_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageUniversal
 * @implements {xyz.swapee.rc.UFileEditorPage} A trait that allows to access an _IFileEditorPage_ object via a `.fileEditorPage` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UFileEditorPage.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPageUniversal = class extends /** @type {xyz.swapee.rc.FileEditorPageUniversal.constructor&xyz.swapee.rc.UFileEditorPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UFileEditorPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UFileEditorPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UFileEditorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileEditorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPageUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPageUniversal}
 */
xyz.swapee.rc.FileEditorPageUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UFileEditorPage.
 * @interface xyz.swapee.rc.UFileEditorPageFields
 */
xyz.swapee.rc.UFileEditorPageFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UFileEditorPageFields.prototype.fileEditorPage = /** @type {xyz.swapee.rc.IFileEditorPage} */ (void 0)

/** @typedef {xyz.swapee.rc.UFileEditorPage} */
xyz.swapee.rc.RecordUFileEditorPage

/** @typedef {xyz.swapee.rc.UFileEditorPage} xyz.swapee.rc.BoundUFileEditorPage */

/** @typedef {xyz.swapee.rc.FileEditorPageUniversal} xyz.swapee.rc.BoundFileEditorPageUniversal */

/**
 * Contains getters to cast the _UFileEditorPage_ interface.
 * @interface xyz.swapee.rc.UFileEditorPageCaster
 */
xyz.swapee.rc.UFileEditorPageCaster = class { }
/**
 * Provides direct access to _UFileEditorPage_ via the _BoundUFileEditorPage_ universal.
 * @type {!xyz.swapee.rc.BoundFileEditorPage}
 */
xyz.swapee.rc.UFileEditorPageCaster.prototype.asFileEditorPage
/**
 * Cast the _UFileEditorPage_ instance into the _BoundUFileEditorPage_ type.
 * @type {!xyz.swapee.rc.BoundUFileEditorPage}
 */
xyz.swapee.rc.UFileEditorPageCaster.prototype.asUFileEditorPage
/**
 * Access the _FileEditorPageUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundFileEditorPageUniversal}
 */
xyz.swapee.rc.UFileEditorPageCaster.prototype.superFileEditorPageUniversal

/** @typedef {function(new: xyz.swapee.rc.BFileEditorPageAspectsCaster<THIS>&xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BFileEditorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice} xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IFileEditorPage* that bind to an instance.
 * @interface xyz.swapee.rc.BFileEditorPageAspects
 * @template THIS
 */
xyz.swapee.rc.BFileEditorPageAspects = class extends /** @type {xyz.swapee.rc.BFileEditorPageAspects.constructor&xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BFileEditorPageAspects.prototype.constructor = xyz.swapee.rc.BFileEditorPageAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorPageAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPageAspects)} xyz.swapee.rc.AbstractFileEditorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPageAspects} xyz.swapee.rc.FileEditorPageAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileEditorPageAspects` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPageAspects
 */
xyz.swapee.rc.AbstractFileEditorPageAspects = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPageAspects.constructor&xyz.swapee.rc.FileEditorPageAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPageAspects.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPageAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPageAspects.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPageAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileEditorPageAspects|typeof xyz.swapee.rc.FileEditorPageAspects)|(!xyz.swapee.rc.BFileEditorPageAspects|typeof xyz.swapee.rc.BFileEditorPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPageAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPageAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileEditorPageAspects|typeof xyz.swapee.rc.FileEditorPageAspects)|(!xyz.swapee.rc.BFileEditorPageAspects|typeof xyz.swapee.rc.BFileEditorPageAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileEditorPageAspects|typeof xyz.swapee.rc.FileEditorPageAspects)|(!xyz.swapee.rc.BFileEditorPageAspects|typeof xyz.swapee.rc.BFileEditorPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileEditorPageAspects.Initialese[]) => xyz.swapee.rc.IFileEditorPageAspects} xyz.swapee.rc.FileEditorPageAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileEditorPageAspectsCaster&xyz.swapee.rc.BFileEditorPageAspects<!xyz.swapee.rc.IFileEditorPageAspects>)} xyz.swapee.rc.IFileEditorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BFileEditorPageAspects} xyz.swapee.rc.BFileEditorPageAspects.typeof */
/**
 * The aspects of the *IFileEditorPage*.
 * @interface xyz.swapee.rc.IFileEditorPageAspects
 */
xyz.swapee.rc.IFileEditorPageAspects = class extends /** @type {xyz.swapee.rc.IFileEditorPageAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BFileEditorPageAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorPageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileEditorPageAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPageAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageAspects.Initialese>)} xyz.swapee.rc.FileEditorPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageAspects} xyz.swapee.rc.IFileEditorPageAspects.typeof */
/**
 * A concrete class of _IFileEditorPageAspects_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageAspects
 * @implements {xyz.swapee.rc.IFileEditorPageAspects} The aspects of the *IFileEditorPage*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageAspects.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPageAspects = class extends /** @type {xyz.swapee.rc.FileEditorPageAspects.constructor&xyz.swapee.rc.IFileEditorPageAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorPageAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileEditorPageAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorPageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPageAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPageAspects}
 */
xyz.swapee.rc.FileEditorPageAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BFileEditorPageAspects_ interface.
 * @interface xyz.swapee.rc.BFileEditorPageAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BFileEditorPageAspectsCaster = class { }
/**
 * Cast the _BFileEditorPageAspects_ instance into the _BoundIFileEditorPage_ type.
 * @type {!xyz.swapee.rc.BoundIFileEditorPage}
 */
xyz.swapee.rc.BFileEditorPageAspectsCaster.prototype.asIFileEditorPage

/**
 * Contains getters to cast the _IFileEditorPageAspects_ interface.
 * @interface xyz.swapee.rc.IFileEditorPageAspectsCaster
 */
xyz.swapee.rc.IFileEditorPageAspectsCaster = class { }
/**
 * Cast the _IFileEditorPageAspects_ instance into the _BoundIFileEditorPage_ type.
 * @type {!xyz.swapee.rc.BoundIFileEditorPage}
 */
xyz.swapee.rc.IFileEditorPageAspectsCaster.prototype.asIFileEditorPage

/** @typedef {xyz.swapee.rc.IFileEditorPage.Initialese} xyz.swapee.rc.IHyperFileEditorPage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.HyperFileEditorPage)} xyz.swapee.rc.AbstractHyperFileEditorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperFileEditorPage} xyz.swapee.rc.HyperFileEditorPage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperFileEditorPage` interface.
 * @constructor xyz.swapee.rc.AbstractHyperFileEditorPage
 */
xyz.swapee.rc.AbstractHyperFileEditorPage = class extends /** @type {xyz.swapee.rc.AbstractHyperFileEditorPage.constructor&xyz.swapee.rc.HyperFileEditorPage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperFileEditorPage.prototype.constructor = xyz.swapee.rc.AbstractHyperFileEditorPage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperFileEditorPage.class = /** @type {typeof xyz.swapee.rc.AbstractHyperFileEditorPage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperFileEditorPage|typeof xyz.swapee.rc.HyperFileEditorPage)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.IFileEditorPageHyperslice|typeof xyz.swapee.rc.FileEditorPageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileEditorPage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileEditorPage}
 */
xyz.swapee.rc.AbstractHyperFileEditorPage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPage}
 */
xyz.swapee.rc.AbstractHyperFileEditorPage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperFileEditorPage|typeof xyz.swapee.rc.HyperFileEditorPage)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.IFileEditorPageHyperslice|typeof xyz.swapee.rc.FileEditorPageHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPage}
 */
xyz.swapee.rc.AbstractHyperFileEditorPage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperFileEditorPage|typeof xyz.swapee.rc.HyperFileEditorPage)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.IFileEditorPageHyperslice|typeof xyz.swapee.rc.FileEditorPageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPage}
 */
xyz.swapee.rc.AbstractHyperFileEditorPage.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IFileEditorPageAspects|function(new: xyz.swapee.rc.IFileEditorPageAspects)} aides The list of aides that advise the IFileEditorPage to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileEditorPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileEditorPage.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperFileEditorPage.Initialese[]) => xyz.swapee.rc.IHyperFileEditorPage} xyz.swapee.rc.HyperFileEditorPageConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperFileEditorPageCaster&xyz.swapee.rc.IFileEditorPage)} xyz.swapee.rc.IHyperFileEditorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPage} xyz.swapee.rc.IFileEditorPage.typeof */
/** @interface xyz.swapee.rc.IHyperFileEditorPage */
xyz.swapee.rc.IHyperFileEditorPage = class extends /** @type {xyz.swapee.rc.IHyperFileEditorPage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileEditorPage.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperFileEditorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileEditorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperFileEditorPage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperFileEditorPage&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileEditorPage.Initialese>)} xyz.swapee.rc.HyperFileEditorPage.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperFileEditorPage} xyz.swapee.rc.IHyperFileEditorPage.typeof */
/**
 * A concrete class of _IHyperFileEditorPage_ instances.
 * @constructor xyz.swapee.rc.HyperFileEditorPage
 * @implements {xyz.swapee.rc.IHyperFileEditorPage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileEditorPage.Initialese>} ‎
 */
xyz.swapee.rc.HyperFileEditorPage = class extends /** @type {xyz.swapee.rc.HyperFileEditorPage.constructor&xyz.swapee.rc.IHyperFileEditorPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperFileEditorPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperFileEditorPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperFileEditorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileEditorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperFileEditorPage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperFileEditorPage}
 */
xyz.swapee.rc.HyperFileEditorPage.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperFileEditorPage} */
xyz.swapee.rc.RecordIHyperFileEditorPage

/** @typedef {xyz.swapee.rc.IHyperFileEditorPage} xyz.swapee.rc.BoundIHyperFileEditorPage */

/** @typedef {xyz.swapee.rc.HyperFileEditorPage} xyz.swapee.rc.BoundHyperFileEditorPage */

/**
 * Contains getters to cast the _IHyperFileEditorPage_ interface.
 * @interface xyz.swapee.rc.IHyperFileEditorPageCaster
 */
xyz.swapee.rc.IHyperFileEditorPageCaster = class { }
/**
 * Cast the _IHyperFileEditorPage_ instance into the _BoundIHyperFileEditorPage_ type.
 * @type {!xyz.swapee.rc.BoundIHyperFileEditorPage}
 */
xyz.swapee.rc.IHyperFileEditorPageCaster.prototype.asIHyperFileEditorPage
/**
 * Access the _HyperFileEditorPage_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperFileEditorPage}
 */
xyz.swapee.rc.IHyperFileEditorPageCaster.prototype.superHyperFileEditorPage

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageHyperslice
 */
xyz.swapee.rc.IFileEditorPageHyperslice = class {
  constructor() {
    this.fileEditor=/** @type {!xyz.swapee.rc.IFileEditorPage._fileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPage._fileEditor>} */ (void 0)
    /**
     * Reads the file on the filesystem and opens it in the editor.
     */
    this.filterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPage._filterReadFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPage._filterReadFile>} */ (void 0)
    /**
     * Saves the file back to the filesystem.
     */
    this.filterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPage._filterSaveFile|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPage._filterSaveFile>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageHyperslice

/**
 * A concrete class of _IFileEditorPageHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileEditorPageHyperslice = class extends xyz.swapee.rc.IFileEditorPageHyperslice { }
xyz.swapee.rc.FileEditorPageHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileEditorPageBindingHyperslice = class {
  constructor() {
    this.fileEditor=/** @type {!xyz.swapee.rc.IFileEditorPage.__fileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPage.__fileEditor<THIS>>} */ (void 0)
    /**
     * Reads the file on the filesystem and opens it in the editor.
     */
    this.filterReadFile=/** @type {!xyz.swapee.rc.IFileEditorPage.__filterReadFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPage.__filterReadFile<THIS>>} */ (void 0)
    /**
     * Saves the file back to the filesystem.
     */
    this.filterSaveFile=/** @type {!xyz.swapee.rc.IFileEditorPage.__filterSaveFile<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPage.__filterSaveFile<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageBindingHyperslice

/**
 * A concrete class of _IFileEditorPageBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageBindingHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileEditorPageBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileEditorPageBindingHyperslice = class extends xyz.swapee.rc.IFileEditorPageBindingHyperslice { }
xyz.swapee.rc.FileEditorPageBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPageFields&engineering.type.IEngineer&xyz.swapee.rc.IFileEditorPageCaster&xyz.swapee.rc.UFileEditorPage)} xyz.swapee.rc.IFileEditorPage.constructor */
/** @interface xyz.swapee.rc.IFileEditorPage */
xyz.swapee.rc.IFileEditorPage = class extends /** @type {xyz.swapee.rc.IFileEditorPage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.UFileEditorPage.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileEditorPage.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IFileEditorPage.fileEditor} */
xyz.swapee.rc.IFileEditorPage.prototype.fileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPage.filterReadFile} */
xyz.swapee.rc.IFileEditorPage.prototype.filterReadFile = function() {}
/** @type {xyz.swapee.rc.IFileEditorPage.filterSaveFile} */
xyz.swapee.rc.IFileEditorPage.prototype.filterSaveFile = function() {}

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage&engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPage.Initialese>)} xyz.swapee.rc.FileEditorPage.constructor */
/**
 * A concrete class of _IFileEditorPage_ instances.
 * @constructor xyz.swapee.rc.FileEditorPage
 * @implements {xyz.swapee.rc.IFileEditorPage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPage.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPage = class extends /** @type {xyz.swapee.rc.FileEditorPage.constructor&xyz.swapee.rc.IFileEditorPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileEditorPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPage}
 */
xyz.swapee.rc.FileEditorPage.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorPage.
 * @interface xyz.swapee.rc.IFileEditorPageFields
 */
xyz.swapee.rc.IFileEditorPageFields = class { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPageFields.prototype.fileEditorCtx = /** @type {!xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx} */ (void 0)

/** @typedef {xyz.swapee.rc.IFileEditorPage} */
xyz.swapee.rc.RecordIFileEditorPage

/** @typedef {xyz.swapee.rc.IFileEditorPage} xyz.swapee.rc.BoundIFileEditorPage */

/** @typedef {xyz.swapee.rc.FileEditorPage} xyz.swapee.rc.BoundFileEditorPage */

/**
 * Contains getters to cast the _IFileEditorPage_ interface.
 * @interface xyz.swapee.rc.IFileEditorPageCaster
 */
xyz.swapee.rc.IFileEditorPageCaster = class { }
/**
 * Cast the _IFileEditorPage_ instance into the _BoundIFileEditorPage_ type.
 * @type {!xyz.swapee.rc.BoundIFileEditorPage}
 */
xyz.swapee.rc.IFileEditorPageCaster.prototype.asIFileEditorPage
/**
 * Access the _FileEditorPage_ prototype.
 * @type {!xyz.swapee.rc.BoundFileEditorPage}
 */
xyz.swapee.rc.IFileEditorPageCaster.prototype.superFileEditorPage

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFileEditor<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFileEditor} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPage.FileEditorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `fileEditor` method from being executed.
 * - `sub` _(value: !IFileEditorPage.fileEditor.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.fileEditor.OptAnswers&gt;) =&gt; void_ Cancels a call to `fileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditor<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditor} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `after` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.fileEditor.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorThrows<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `fileEditor` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorReturns<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `afterReturns` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.fileEditor.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileEditorPage.fileEditor.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.fileEditor.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFileEditorCancels<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFileEditorCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFileEditorCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFileEditor<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFileEditor} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPage.FileEditorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `fileEditor` method from being executed.
 * - `sub` _(value: !IFileEditorPage.fileEditor.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.fileEditor.OptAnswers&gt;) =&gt; void_ Cancels a call to `fileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditor<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFileEditor} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `after` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.fileEditor.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFileEditorReturns<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFileEditorReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFileEditorReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFileEditorPointcutData} [data] Metadata passed to the pointcuts of _fileEditor_ at the `afterEach` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.fileEditor.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `args` _IFileEditorPage.FileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFileEditorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterReadFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFilterReadFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFilterReadFile} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPage.FilterReadFileNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterReadFile` method from being executed.
 * - `sub` _(value: IFileEditorPage.filterReadFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterReadFile.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterReadFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFilterReadFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFile} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `after` joinpoint.
 * - `res` _IFileEditorPage.filterReadFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterReadFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileThrows<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterReadFile` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileReturns<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `afterReturns` joinpoint.
 * - `res` _IFileEditorPage.filterReadFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterReadFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IFileEditorPage.filterReadFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterReadFile.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterReadFileCancels<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterReadFileCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterReadFileCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterReadFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFilterReadFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFilterReadFile} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPage.FilterReadFileNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterReadFile` method from being executed.
 * - `sub` _(value: IFileEditorPage.filterReadFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterReadFile.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterReadFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFilterReadFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterReadFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterReadFile} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `after` joinpoint.
 * - `res` _IFileEditorPage.filterReadFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterReadFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterReadFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFileReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterReadFileReturns<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterReadFileReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterReadFileReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterReadFilePointcutData} [data] Metadata passed to the pointcuts of _filterReadFile_ at the `afterEach` joinpoint.
 * - `res` _IFileEditorPage.filterReadFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterReadFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `args` _IFileEditorPage.FilterReadFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterReadFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterReadFileReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeFilterSaveFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeFilterSaveFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFilterSaveFile} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPage.FilterSaveFileNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterSaveFile` method from being executed.
 * - `sub` _(value: IFileEditorPage.filterSaveFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterSaveFile.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterSaveFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeFilterSaveFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFile} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `after` joinpoint.
 * - `res` _IFileEditorPage.filterSaveFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterSaveFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileThrows<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterThrowsFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterSaveFile` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileReturns<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterReturnsFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `afterReturns` joinpoint.
 * - `res` _IFileEditorPage.filterSaveFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterSaveFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IFileEditorPage.filterSaveFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterSaveFile.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterFilterSaveFileCancels<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterFilterSaveFileCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterCancelsFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterFilterSaveFileCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__beforeEachFilterSaveFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._beforeEachFilterSaveFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFilterSaveFile} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.BeforeFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPage.FilterSaveFileNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterSaveFile` method from being executed.
 * - `sub` _(value: IFileEditorPage.filterSaveFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterSaveFile.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterSaveFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.beforeEachFilterSaveFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFile<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterSaveFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterSaveFile} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `after` joinpoint.
 * - `res` _IFileEditorPage.filterSaveFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterSaveFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterSaveFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData) => void} xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFileReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageJoinpointModel.__afterEachFilterSaveFileReturns<!xyz.swapee.rc.IFileEditorPageJoinpointModel>} xyz.swapee.rc.IFileEditorPageJoinpointModel._afterEachFilterSaveFileReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterSaveFileReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageJoinpointModel.AfterFilterSaveFilePointcutData} [data] Metadata passed to the pointcuts of _filterSaveFile_ at the `afterEach` joinpoint.
 * - `res` _IFileEditorPage.filterSaveFile.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileEditorPage.filterSaveFile.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `args` _IFileEditorPage.FilterSaveFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageJoinpointModel.FilterSaveFilePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageJoinpointModel.afterEachFilterSaveFileReturns = function(data) {}

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IFileEditorPage.fileEditor.Form, answers: !xyz.swapee.rc.IFileEditorPage.fileEditor.Answers, validation: !xyz.swapee.rc.IFileEditorPage.fileEditor.Validation, errors: !xyz.swapee.rc.IFileEditorPage.fileEditor.Errors, ctx: !xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx) => (void|!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers>)} xyz.swapee.rc.IFileEditorPage.__fileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPage.__fileEditor<!xyz.swapee.rc.IFileEditorPage>} xyz.swapee.rc.IFileEditorPage._fileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPage.fileEditor} */
/**
 *  `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} form The form.
 * - `locale` _string_
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} answers The answers.
 * - `locale` _string_
 * - `content` _&#42;_ ⤴ *IFileEditorPage.filterReadFile.Answers*
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Validation} validation The validation.
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx} ctx The ctx.
 * - `validation` _!IFileEditorPage.fileEditor.Validation_ The validation.
 * - `errors` _!IFileEditorPage.fileEditor.Errors_ The errors.
 * - `answers` _!IFileEditorPage.fileEditor.Answers_ The answers.
 * @return {void|!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers>}
 */
xyz.swapee.rc.IFileEditorPage.fileEditor = function(form, answers, validation, errors, ctx) {}

/**
 * The form.
 * @record xyz.swapee.rc.IFileEditorPage.fileEditor.Form
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Form = class { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Form.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers)} xyz.swapee.rc.IFileEditorPage.fileEditor.Answers.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers} xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IFileEditorPage.fileEditor.Answers
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Answers = class extends /** @type {xyz.swapee.rc.IFileEditorPage.fileEditor.Answers.constructor&xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Answers.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers)} xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers} xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers = class extends /** @type {xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers.constructor&xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.OptAnswers.prototype.locale = /** @type {string|undefined} */ (void 0)

/**
 * The validation.
 * @record xyz.swapee.rc.IFileEditorPage.fileEditor.Validation
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Validation = class { }
xyz.swapee.rc.IFileEditorPage.fileEditor.Validation.prototype.constructor = xyz.swapee.rc.IFileEditorPage.fileEditor.Validation

/**
 * The errors.
 * @record xyz.swapee.rc.IFileEditorPage.fileEditor.Errors
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Errors = class { }
xyz.swapee.rc.IFileEditorPage.fileEditor.Errors.prototype.constructor = xyz.swapee.rc.IFileEditorPage.fileEditor.Errors

/**
 * The ctx.
 * @record xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx.prototype.validation = /** @type {!xyz.swapee.rc.IFileEditorPage.fileEditor.Validation} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx.prototype.errors = /** @type {!xyz.swapee.rc.IFileEditorPage.fileEditor.Errors} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx.prototype.answers = /** @type {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} */ (void 0)

/**
 * The ctx.
 * @record xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx.prototype.validation = /** @type {(!xyz.swapee.rc.IFileEditorPage.fileEditor.Validation)|undefined} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx.prototype.errors = /** @type {(!xyz.swapee.rc.IFileEditorPage.fileEditor.Errors)|undefined} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx.prototype.answers = /** @type {(!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers)|undefined} */ (void 0)

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IFileEditorPage.filterReadFile.Form, answers: !xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers, validation: !xyz.swapee.rc.IFileEditorPage.filterReadFile.Validation, errors: !xyz.swapee.rc.IFileEditorPage.filterReadFile.Errors, ctx: !xyz.swapee.rc.IFileEditorPage.filterReadFile.Ctx) => (void|xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers>)} xyz.swapee.rc.IFileEditorPage.__filterReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPage.__filterReadFile<!xyz.swapee.rc.IFileEditorPage>} xyz.swapee.rc.IFileEditorPage._filterReadFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPage.filterReadFile} */
/**
 * Reads the file on the filesystem and opens it in the editor. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Form} form The action form.
 * - `path` _&#42;_
 * @param {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers} answers The action answers.
 * - `content` _&#42;_
 * @param {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers>}
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IFileEditorPage.filterReadFile.Form
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.Form = class { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.Form.prototype.path = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.Answers.prototype.content = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.OptAnswers.prototype.content = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IFileEditorPage.filterReadFile.Validation
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.Validation = class { }
xyz.swapee.rc.IFileEditorPage.filterReadFile.Validation.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterReadFile.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IFileEditorPage.filterReadFile.Errors
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.Errors = class { }
xyz.swapee.rc.IFileEditorPage.filterReadFile.Errors.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterReadFile.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IFileEditorPage.filterReadFile.Ctx
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.Ctx = class { }
xyz.swapee.rc.IFileEditorPage.filterReadFile.Ctx.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterReadFile.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IFileEditorPage.filterReadFile.OptCtx
 */
xyz.swapee.rc.IFileEditorPage.filterReadFile.OptCtx = class { }
xyz.swapee.rc.IFileEditorPage.filterReadFile.OptCtx.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterReadFile.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form, answers: !xyz.swapee.rc.IFileEditorPage.filterSaveFile.Answers, validation: !xyz.swapee.rc.IFileEditorPage.filterSaveFile.Validation, errors: !xyz.swapee.rc.IFileEditorPage.filterSaveFile.Errors, ctx: !xyz.swapee.rc.IFileEditorPage.filterSaveFile.Ctx) => (void|xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers>)} xyz.swapee.rc.IFileEditorPage.__filterSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPage.__filterSaveFile<!xyz.swapee.rc.IFileEditorPage>} xyz.swapee.rc.IFileEditorPage._filterSaveFile */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPage.filterSaveFile} */
/**
 * Saves the file back to the filesystem. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form} form The action form.
 * - `saveFile` _&#42;_
 * - `content` _&#42;_
 * - `path` _&#42;_
 * @param {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Answers} answers The action answers.
 * @param {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers>}
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form = class { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form.prototype.saveFile = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form.prototype.content = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form.prototype.path = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IFileEditorPage.filterSaveFile.Answers
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Answers = class { }
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Answers.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterSaveFile.Answers

/**
 * The action answers.
 * @record xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers = class { }
xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptAnswers

/**
 * The action validation.
 * @record xyz.swapee.rc.IFileEditorPage.filterSaveFile.Validation
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Validation = class { }
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Validation.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterSaveFile.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IFileEditorPage.filterSaveFile.Errors
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Errors = class { }
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Errors.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterSaveFile.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IFileEditorPage.filterSaveFile.Ctx
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Ctx = class { }
xyz.swapee.rc.IFileEditorPage.filterSaveFile.Ctx.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterSaveFile.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptCtx
 */
xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptCtx = class { }
xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptCtx.prototype.constructor = xyz.swapee.rc.IFileEditorPage.filterSaveFile.OptCtx

// nss:xyz.swapee.rc,xyz.swapee.rc.IFileEditorPageJoinpointModel,xyz.swapee.rc.IFileEditorPage
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-editor/file-editor.ws/file-editor/FileEditor/design/IFileEditorPageAliases.xml}  6c4af280d936fc45cca7828c14f121fe */
/** @constructor xyz.swapee.rc.FileEditor */
xyz.swapee.rc.FileEditor = class { }
xyz.swapee.rc.FileEditor.prototype.constructor = xyz.swapee.rc.FileEditor

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage.fileEditor.Answers)} xyz.swapee.rc.FileEditor.Answers.constructor */
/**
 * The answers.
 * @record xyz.swapee.rc.FileEditor.Answers
 */
xyz.swapee.rc.FileEditor.Answers = class extends /** @type {xyz.swapee.rc.FileEditor.Answers.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileEditor.Answers.prototype.props = /** @type {!xyz.swapee.rc.FileEditor.Answers.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileEditor.Answers.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage.fileEditor.Form)} xyz.swapee.rc.FileEditor.Form.constructor */
/**
 * The form.
 * @record xyz.swapee.rc.FileEditor.Form
 */
xyz.swapee.rc.FileEditor.Form = class extends /** @type {xyz.swapee.rc.FileEditor.Form.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileEditor.Form.prototype.props = /** @type {!xyz.swapee.rc.FileEditor.Form.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileEditor.Form.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage.fileEditor.Errors)} xyz.swapee.rc.FileEditor.Errors.constructor */
/**
 * The errors.
 * @record xyz.swapee.rc.FileEditor.Errors
 */
xyz.swapee.rc.FileEditor.Errors = class extends /** @type {xyz.swapee.rc.FileEditor.Errors.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileEditor.Errors.prototype.props = /** @type {!xyz.swapee.rc.FileEditor.Errors.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileEditor.Errors.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage.fileEditor.Validation)} xyz.swapee.rc.FileEditor.Validation.constructor */
/**
 * The validation.
 * @record xyz.swapee.rc.FileEditor.Validation
 */
xyz.swapee.rc.FileEditor.Validation = class extends /** @type {xyz.swapee.rc.FileEditor.Validation.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileEditor.Validation.prototype.props = /** @type {!xyz.swapee.rc.FileEditor.Validation.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileEditor.Validation.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx)} xyz.swapee.rc.FileEditor.Ctx.constructor */
/**
 * The ctx.
 * @record xyz.swapee.rc.FileEditor.Ctx
 */
xyz.swapee.rc.FileEditor.Ctx = class extends /** @type {xyz.swapee.rc.FileEditor.Ctx.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileEditor.Ctx.prototype.props = /** @type {!xyz.swapee.rc.FileEditor.Ctx.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileEditor.Ctx.Props The props for VSCode. */

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-editor/file-editor.ws/file-editor/FileEditor/design/IFileEditorPageView.xml}  4d37fc96760db18b203c5f69d50e7132 */
/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorPageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPageView)} xyz.swapee.rc.AbstractFileEditorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPageView} xyz.swapee.rc.FileEditorPageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileEditorPageView` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPageView
 */
xyz.swapee.rc.AbstractFileEditorPageView = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPageView.constructor&xyz.swapee.rc.FileEditorPageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPageView.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPageView.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileEditorPageView|typeof xyz.swapee.rc.FileEditorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UFileEditorPageView|typeof xyz.swapee.rc.UFileEditorPageView)|!xyz.swapee.rc.IFileEditorPageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPageView}
 */
xyz.swapee.rc.AbstractFileEditorPageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageView}
 */
xyz.swapee.rc.AbstractFileEditorPageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileEditorPageView|typeof xyz.swapee.rc.FileEditorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UFileEditorPageView|typeof xyz.swapee.rc.UFileEditorPageView)|!xyz.swapee.rc.IFileEditorPageViewHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageView}
 */
xyz.swapee.rc.AbstractFileEditorPageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileEditorPageView|typeof xyz.swapee.rc.FileEditorPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UFileEditorPageView|typeof xyz.swapee.rc.UFileEditorPageView)|!xyz.swapee.rc.IFileEditorPageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageView}
 */
xyz.swapee.rc.AbstractFileEditorPageView.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageViewJoinpointModelHyperslice
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeViewFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeViewFileEditor>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditor>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewFileEditorThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewFileEditorCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachViewFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachViewFileEditor>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachViewFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachViewFileEditor>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachViewFileEditorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachViewFileEditorReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeGetFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeGetFileEditor>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditor>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetFileEditorThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetFileEditorCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachGetFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachGetFileEditor>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachGetFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachGetFileEditor>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachGetFileEditorReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachGetFileEditorReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeSetFileEditorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeSetFileEditorCtx>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtx>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetFileEditorCtxThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetFileEditorCtxReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetFileEditorCtxCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachSetFileEditorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachSetFileEditorCtx>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachSetFileEditorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachSetFileEditorCtx>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetFileEditorCtxReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachSetFileEditorCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachSetFileEditorCtxReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileEditorPartial=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._before_FileEditorPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._before_FileEditorPartial>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileEditorPartial=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartial>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileEditorPartialThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileEditorPartialReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileEditorPartialCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._before_FileEditorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._before_FileEditorView>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorView>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileEditorViewThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileEditorViewReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileEditorViewCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEach_FileEditorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEach_FileEditorView>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEach_FileEditorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEach_FileEditorView>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_FileEditorViewReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEach_FileEditorViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEach_FileEditorViewReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageViewJoinpointModelHyperslice

/**
 * A concrete class of _IFileEditorPageViewJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageViewJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileEditorPageViewJoinpointModelHyperslice = class extends xyz.swapee.rc.IFileEditorPageViewJoinpointModelHyperslice { }
xyz.swapee.rc.FileEditorPageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageViewJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeViewFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeViewFileEditor<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditor<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewFileEditorThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewFileEditorCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachViewFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachViewFileEditor<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditor<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditorReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeGetFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeGetFileEditor<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditor<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetFileEditorThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetFileEditorCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachGetFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachGetFileEditor<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditor<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetFileEditorReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditorReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditorReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeSetFileEditorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeSetFileEditorCtx<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtx<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetFileEditorCtxThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetFileEditorCtxReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetFileEditorCtxCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachSetFileEditorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachSetFileEditorCtx<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtx<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetFileEditorCtxReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtxReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileEditorPartial=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorPartial<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileEditorPartial=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartial<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileEditorPartialThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileEditorPartialReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileEditorPartialCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorView<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorView<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileEditorViewThrows=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileEditorViewReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileEditorViewCancels=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEach_FileEditorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEach_FileEditorView<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorView<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_FileEditorViewReturns=/** @type {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorViewReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice

/**
 * A concrete class of _IFileEditorPageViewJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileEditorPageViewJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice { }
xyz.swapee.rc.FileEditorPageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageViewJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IFileEditorPageView`'s methods.
 * @interface xyz.swapee.rc.IFileEditorPageViewJoinpointModel
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel = class { }
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeViewFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.beforeViewFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterViewFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorThrows} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterViewFileEditorThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterViewFileEditorReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorCancels} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterViewFileEditorCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachViewFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.beforeEachViewFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachViewFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEachViewFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachViewFileEditorReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEachViewFileEditorReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeGetFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.beforeGetFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterGetFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorThrows} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterGetFileEditorThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterGetFileEditorReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorCancels} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterGetFileEditorCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachGetFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.beforeEachGetFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachGetFileEditor} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEachGetFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachGetFileEditorReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEachGetFileEditorReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeSetFileEditorCtx} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.beforeSetFileEditorCtx = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtx} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterSetFileEditorCtx = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxThrows} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterSetFileEditorCtxThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterSetFileEditorCtxReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxCancels} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterSetFileEditorCtxCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachSetFileEditorCtx} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.beforeEachSetFileEditorCtx = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachSetFileEditorCtx} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEachSetFileEditorCtx = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachSetFileEditorCtxReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEachSetFileEditorCtxReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.before_FileEditorPartial} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.before_FileEditorPartial = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartial} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorPartial = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialThrows} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorPartialThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorPartialReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialCancels} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorPartialCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.before_FileEditorView} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.before_FileEditorView = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorView} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorView = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewThrows} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorViewThrows = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorViewReturns = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewCancels} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.after_FileEditorViewCancels = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEach_FileEditorView} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.beforeEach_FileEditorView = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEach_FileEditorView} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEach_FileEditorView = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEach_FileEditorViewReturns} */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.prototype.afterEach_FileEditorViewReturns = function() {}

/**
 * A concrete class of _IFileEditorPageViewJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewJoinpointModel
 * @implements {xyz.swapee.rc.IFileEditorPageViewJoinpointModel} An interface that enumerates the joinpoints of `IFileEditorPageView`'s methods.
 */
xyz.swapee.rc.FileEditorPageViewJoinpointModel = class extends xyz.swapee.rc.IFileEditorPageViewJoinpointModel { }
xyz.swapee.rc.FileEditorPageViewJoinpointModel.prototype.constructor = xyz.swapee.rc.FileEditorPageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel} */
xyz.swapee.rc.RecordIFileEditorPageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel} xyz.swapee.rc.BoundIFileEditorPageViewJoinpointModel */

/** @typedef {xyz.swapee.rc.FileEditorPageViewJoinpointModel} xyz.swapee.rc.BoundFileEditorPageViewJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPageViewAspectsInstaller)} xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller} xyz.swapee.rc.FileEditorPageViewAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileEditorPageViewAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.constructor&xyz.swapee.rc.FileEditorPageViewAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller|typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller)|(!xyz.swapee.rc.IFileEditorPageInstaller|typeof xyz.swapee.rc.FileEditorPageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller|typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller)|(!xyz.swapee.rc.IFileEditorPageInstaller|typeof xyz.swapee.rc.FileEditorPageInstaller)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller|typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller)|(!xyz.swapee.rc.IFileEditorPageInstaller|typeof xyz.swapee.rc.FileEditorPageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.Initialese[]) => xyz.swapee.rc.IFileEditorPageViewAspectsInstaller} xyz.swapee.rc.FileEditorPageViewAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileEditorPageInstaller)} xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageInstaller} xyz.swapee.rc.IFileEditorPageInstaller.typeof */
/** @interface xyz.swapee.rc.IFileEditorPageViewAspectsInstaller */
xyz.swapee.rc.IFileEditorPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileEditorPageInstaller.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeViewFileEditor=/** @type {number} */ (void 0)
    this.afterViewFileEditor=/** @type {number} */ (void 0)
    this.afterViewFileEditorThrows=/** @type {number} */ (void 0)
    this.afterViewFileEditorReturns=/** @type {number} */ (void 0)
    this.afterViewFileEditorCancels=/** @type {number} */ (void 0)
    this.beforeEachViewFileEditor=/** @type {number} */ (void 0)
    this.afterEachViewFileEditor=/** @type {number} */ (void 0)
    this.afterEachViewFileEditorReturns=/** @type {number} */ (void 0)
    this.beforeGetFileEditor=/** @type {number} */ (void 0)
    this.afterGetFileEditor=/** @type {number} */ (void 0)
    this.afterGetFileEditorThrows=/** @type {number} */ (void 0)
    this.afterGetFileEditorReturns=/** @type {number} */ (void 0)
    this.afterGetFileEditorCancels=/** @type {number} */ (void 0)
    this.beforeEachGetFileEditor=/** @type {number} */ (void 0)
    this.afterEachGetFileEditor=/** @type {number} */ (void 0)
    this.afterEachGetFileEditorReturns=/** @type {number} */ (void 0)
    this.beforeSetFileEditorCtx=/** @type {number} */ (void 0)
    this.afterSetFileEditorCtx=/** @type {number} */ (void 0)
    this.afterSetFileEditorCtxThrows=/** @type {number} */ (void 0)
    this.afterSetFileEditorCtxReturns=/** @type {number} */ (void 0)
    this.afterSetFileEditorCtxCancels=/** @type {number} */ (void 0)
    this.beforeEachSetFileEditorCtx=/** @type {number} */ (void 0)
    this.afterEachSetFileEditorCtx=/** @type {number} */ (void 0)
    this.afterEachSetFileEditorCtxReturns=/** @type {number} */ (void 0)
    this.before_FileEditorPartial=/** @type {number} */ (void 0)
    this.after_FileEditorPartial=/** @type {number} */ (void 0)
    this.after_FileEditorPartialThrows=/** @type {number} */ (void 0)
    this.after_FileEditorPartialReturns=/** @type {number} */ (void 0)
    this.after_FileEditorPartialCancels=/** @type {number} */ (void 0)
    this.before_FileEditorView=/** @type {number} */ (void 0)
    this.after_FileEditorView=/** @type {number} */ (void 0)
    this.after_FileEditorViewThrows=/** @type {number} */ (void 0)
    this.after_FileEditorViewReturns=/** @type {number} */ (void 0)
    this.after_FileEditorViewCancels=/** @type {number} */ (void 0)
    this.beforeEach_FileEditorView=/** @type {number} */ (void 0)
    this.afterEach_FileEditorView=/** @type {number} */ (void 0)
    this.afterEach_FileEditorViewReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  viewFileEditor() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  getFileEditor() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  setFileEditorCtx() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  FileEditorPartial() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  FileEditorView() { }
}
/**
 * Create a new *IFileEditorPageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPageViewAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.Initialese>)} xyz.swapee.rc.FileEditorPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewAspectsInstaller} xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.typeof */
/**
 * A concrete class of _IFileEditorPageViewAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewAspectsInstaller
 * @implements {xyz.swapee.rc.IFileEditorPageViewAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.FileEditorPageViewAspectsInstaller.constructor&xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorPageViewAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorPageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspectsInstaller}
 */
xyz.swapee.rc.FileEditorPageViewAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageView.ViewFileEditorNArgs
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileEditorPageView.ViewFileEditorNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeViewFileEditorPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileEditorPageView.ViewFileEditorNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `viewFileEditor` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPageView.FileEditorView) => void} sub Cancels a call to `viewFileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeViewFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeViewFileEditorPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsViewFileEditorPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `viewFileEditor` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsViewFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsViewFileEditorPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsViewFileEditorPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPageView.FileEditorView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsViewFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsViewFileEditorPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsViewFileEditorPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsViewFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsViewFileEditorPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageView.GetFileEditorNArgs
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileEditorPageView.GetFileEditorNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeGetFileEditorPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileEditorPageView.GetFileEditorNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getFileEditor` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPageView.FileEditorView) => void} sub Cancels a call to `getFileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeGetFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeGetFileEditorPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsGetFileEditorPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getFileEditor` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsGetFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsGetFileEditorPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsGetFileEditorPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPageView.FileEditorView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsGetFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsGetFileEditorPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsGetFileEditorPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsGetFileEditorPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsGetFileEditorPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageView.SetFileEditorCtxNArgs
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation} translation The translation for the user's locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileEditorPageView.SetFileEditorCtxNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeSetFileEditorCtxPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileEditorPageView.SetFileEditorCtxNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `setFileEditorCtx` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx) => void} sub Cancels a call to `setFileEditorCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeSetFileEditorCtxPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeSetFileEditorCtxPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsSetFileEditorCtxPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `setFileEditorCtx` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsSetFileEditorCtxPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsSetFileEditorCtxPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsSetFileEditorCtxPointcutData
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsSetFileEditorCtxPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsSetFileEditorCtxPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsSetFileEditorCtxPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsSetFileEditorCtxPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsSetFileEditorCtxPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageView.FileEditorPartialNArgs
 * @prop {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions} actions The actions.
 * @prop {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation} translation The translation for the chosen user locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileEditorPageView.FileEditorPartialNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorPartialPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileEditorPageView.FileEditorPartialNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `FileEditorPartial` method from being executed.
 * @prop {(value: engineering.type.VNode) => void} sub Cancels a call to `FileEditorPartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorPartialPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorPartialPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorPartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorPartialPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorPartialPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorPartialPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `FileEditorPartial` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorPartialPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorPartialPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorPartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 * @prop {(value: engineering.type.VNode) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorPartialPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorPartialPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorPartialPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorPartialPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorPartialPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorViewPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `FileEditorView` method from being executed.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorViewPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorViewPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorViewPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorViewPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `FileEditorView` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorViewPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorViewPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorViewPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorViewPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorViewPointcutData&xyz.swapee.rc.IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorViewPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IFileEditorPageView.Initialese[]) => xyz.swapee.rc.IFileEditorPageView} xyz.swapee.rc.FileEditorPageViewConstructor */

/** @typedef {symbol} xyz.swapee.rc.FileEditorPageViewMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UFileEditorPageView.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IFileEditorPageView} [fileEditorPageView]
 */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPageViewUniversal)} xyz.swapee.rc.AbstractFileEditorPageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPageViewUniversal} xyz.swapee.rc.FileEditorPageViewUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UFileEditorPageView` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPageViewUniversal
 */
xyz.swapee.rc.AbstractFileEditorPageViewUniversal = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPageViewUniversal.constructor&xyz.swapee.rc.FileEditorPageViewUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPageViewUniversal.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPageViewUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPageViewUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPageViewUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UFileEditorPageView|typeof xyz.swapee.rc.UFileEditorPageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPageViewUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPageViewUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageViewUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageViewUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UFileEditorPageView|typeof xyz.swapee.rc.UFileEditorPageView} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageViewUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UFileEditorPageView|typeof xyz.swapee.rc.UFileEditorPageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewUniversal}
 */
xyz.swapee.rc.AbstractFileEditorPageViewUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.FileEditorPageViewMetaUniversal} xyz.swapee.rc.AbstractFileEditorPageViewUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UFileEditorPageView.Initialese[]) => xyz.swapee.rc.UFileEditorPageView} xyz.swapee.rc.UFileEditorPageViewConstructor */

/** @typedef {function(new: xyz.swapee.rc.UFileEditorPageViewFields&engineering.type.IEngineer&xyz.swapee.rc.UFileEditorPageViewCaster)} xyz.swapee.rc.UFileEditorPageView.constructor */
/**
 * A trait that allows to access an _IFileEditorPageView_ object via a `.fileEditorPageView` field.
 * @interface xyz.swapee.rc.UFileEditorPageView
 */
xyz.swapee.rc.UFileEditorPageView = class extends /** @type {xyz.swapee.rc.UFileEditorPageView.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UFileEditorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileEditorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UFileEditorPageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UFileEditorPageView&engineering.type.IInitialiser<!xyz.swapee.rc.UFileEditorPageView.Initialese>)} xyz.swapee.rc.FileEditorPageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UFileEditorPageView} xyz.swapee.rc.UFileEditorPageView.typeof */
/**
 * A concrete class of _UFileEditorPageView_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewUniversal
 * @implements {xyz.swapee.rc.UFileEditorPageView} A trait that allows to access an _IFileEditorPageView_ object via a `.fileEditorPageView` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UFileEditorPageView.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPageViewUniversal = class extends /** @type {xyz.swapee.rc.FileEditorPageViewUniversal.constructor&xyz.swapee.rc.UFileEditorPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UFileEditorPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UFileEditorPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UFileEditorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileEditorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPageViewUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPageViewUniversal}
 */
xyz.swapee.rc.FileEditorPageViewUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UFileEditorPageView.
 * @interface xyz.swapee.rc.UFileEditorPageViewFields
 */
xyz.swapee.rc.UFileEditorPageViewFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UFileEditorPageViewFields.prototype.fileEditorPageView = /** @type {xyz.swapee.rc.IFileEditorPageView} */ (void 0)

/** @typedef {xyz.swapee.rc.UFileEditorPageView} */
xyz.swapee.rc.RecordUFileEditorPageView

/** @typedef {xyz.swapee.rc.UFileEditorPageView} xyz.swapee.rc.BoundUFileEditorPageView */

/** @typedef {xyz.swapee.rc.FileEditorPageViewUniversal} xyz.swapee.rc.BoundFileEditorPageViewUniversal */

/**
 * Contains getters to cast the _UFileEditorPageView_ interface.
 * @interface xyz.swapee.rc.UFileEditorPageViewCaster
 */
xyz.swapee.rc.UFileEditorPageViewCaster = class { }
/**
 * Provides direct access to _UFileEditorPageView_ via the _BoundUFileEditorPageView_ universal.
 * @type {!xyz.swapee.rc.BoundFileEditorPageView}
 */
xyz.swapee.rc.UFileEditorPageViewCaster.prototype.asFileEditorPageView
/**
 * Cast the _UFileEditorPageView_ instance into the _BoundUFileEditorPageView_ type.
 * @type {!xyz.swapee.rc.BoundUFileEditorPageView}
 */
xyz.swapee.rc.UFileEditorPageViewCaster.prototype.asUFileEditorPageView
/**
 * Access the _FileEditorPageViewUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundFileEditorPageViewUniversal}
 */
xyz.swapee.rc.UFileEditorPageViewCaster.prototype.superFileEditorPageViewUniversal

/** @typedef {function(new: xyz.swapee.rc.BFileEditorPageViewAspectsCaster<THIS>&xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BFileEditorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice} xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IFileEditorPageView* that bind to an instance.
 * @interface xyz.swapee.rc.BFileEditorPageViewAspects
 * @template THIS
 */
xyz.swapee.rc.BFileEditorPageViewAspects = class extends /** @type {xyz.swapee.rc.BFileEditorPageViewAspects.constructor&xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BFileEditorPageViewAspects.prototype.constructor = xyz.swapee.rc.BFileEditorPageViewAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorPageViewAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileEditorPageViewAspects)} xyz.swapee.rc.AbstractFileEditorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorPageViewAspects} xyz.swapee.rc.FileEditorPageViewAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileEditorPageViewAspects` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorPageViewAspects
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspects = class extends /** @type {xyz.swapee.rc.AbstractFileEditorPageViewAspects.constructor&xyz.swapee.rc.FileEditorPageViewAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorPageViewAspects.prototype.constructor = xyz.swapee.rc.AbstractFileEditorPageViewAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspects.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileEditorPageViewAspects|typeof xyz.swapee.rc.FileEditorPageViewAspects)|(!xyz.swapee.rc.BFileEditorPageViewAspects|typeof xyz.swapee.rc.BFileEditorPageViewAspects)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BFileEditorPageAspects|typeof xyz.swapee.rc.BFileEditorPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileEditorPageViewAspects|typeof xyz.swapee.rc.FileEditorPageViewAspects)|(!xyz.swapee.rc.BFileEditorPageViewAspects|typeof xyz.swapee.rc.BFileEditorPageViewAspects)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BFileEditorPageAspects|typeof xyz.swapee.rc.BFileEditorPageAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileEditorPageViewAspects|typeof xyz.swapee.rc.FileEditorPageViewAspects)|(!xyz.swapee.rc.BFileEditorPageViewAspects|typeof xyz.swapee.rc.BFileEditorPageViewAspects)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BFileEditorPageAspects|typeof xyz.swapee.rc.BFileEditorPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspects}
 */
xyz.swapee.rc.AbstractFileEditorPageViewAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileEditorPageViewAspects.Initialese[]) => xyz.swapee.rc.IFileEditorPageViewAspects} xyz.swapee.rc.FileEditorPageViewAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileEditorPageViewAspectsCaster&xyz.swapee.rc.BFileEditorPageViewAspects<!xyz.swapee.rc.IFileEditorPageViewAspects>&xyz.swapee.rc.IFileEditorPage&_idio.IRedirectMod&xyz.swapee.rc.BFileEditorPageAspects)} xyz.swapee.rc.IFileEditorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BFileEditorPageViewAspects} xyz.swapee.rc.BFileEditorPageViewAspects.typeof */
/** @typedef {typeof _idio.IRedirectMod} _idio.IRedirectMod.typeof */
/**
 * The aspects of the *IFileEditorPageView*.
 * @interface xyz.swapee.rc.IFileEditorPageViewAspects
 */
xyz.swapee.rc.IFileEditorPageViewAspects = class extends /** @type {xyz.swapee.rc.IFileEditorPageViewAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BFileEditorPageViewAspects.typeof&xyz.swapee.rc.IFileEditorPage.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.BFileEditorPageAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorPageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileEditorPageViewAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPageViewAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageViewAspects.Initialese>)} xyz.swapee.rc.FileEditorPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewAspects} xyz.swapee.rc.IFileEditorPageViewAspects.typeof */
/**
 * A concrete class of _IFileEditorPageViewAspects_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewAspects
 * @implements {xyz.swapee.rc.IFileEditorPageViewAspects} The aspects of the *IFileEditorPageView*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageViewAspects.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPageViewAspects = class extends /** @type {xyz.swapee.rc.FileEditorPageViewAspects.constructor&xyz.swapee.rc.IFileEditorPageViewAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorPageViewAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileEditorPageViewAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorPageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPageViewAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPageViewAspects}
 */
xyz.swapee.rc.FileEditorPageViewAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BFileEditorPageViewAspects_ interface.
 * @interface xyz.swapee.rc.BFileEditorPageViewAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BFileEditorPageViewAspectsCaster = class { }
/**
 * Cast the _BFileEditorPageViewAspects_ instance into the _BoundIFileEditorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIFileEditorPageView}
 */
xyz.swapee.rc.BFileEditorPageViewAspectsCaster.prototype.asIFileEditorPageView

/**
 * Contains getters to cast the _IFileEditorPageViewAspects_ interface.
 * @interface xyz.swapee.rc.IFileEditorPageViewAspectsCaster
 */
xyz.swapee.rc.IFileEditorPageViewAspectsCaster = class { }
/**
 * Cast the _IFileEditorPageViewAspects_ instance into the _BoundIFileEditorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIFileEditorPageView}
 */
xyz.swapee.rc.IFileEditorPageViewAspectsCaster.prototype.asIFileEditorPageView

/** @typedef {xyz.swapee.rc.IFileEditorPageView.Initialese} xyz.swapee.rc.IHyperFileEditorPageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.HyperFileEditorPageView)} xyz.swapee.rc.AbstractHyperFileEditorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperFileEditorPageView} xyz.swapee.rc.HyperFileEditorPageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperFileEditorPageView` interface.
 * @constructor xyz.swapee.rc.AbstractHyperFileEditorPageView
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView = class extends /** @type {xyz.swapee.rc.AbstractHyperFileEditorPageView.constructor&xyz.swapee.rc.HyperFileEditorPageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperFileEditorPageView.prototype.constructor = xyz.swapee.rc.AbstractHyperFileEditorPageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView.class = /** @type {typeof xyz.swapee.rc.AbstractHyperFileEditorPageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperFileEditorPageView|typeof xyz.swapee.rc.HyperFileEditorPageView)|(!xyz.swapee.rc.IFileEditorPageView|typeof xyz.swapee.rc.FileEditorPageView)|(!xyz.swapee.rc.IFileEditorPageViewHyperslice|typeof xyz.swapee.rc.FileEditorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileEditorPageView}
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPageView}
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperFileEditorPageView|typeof xyz.swapee.rc.HyperFileEditorPageView)|(!xyz.swapee.rc.IFileEditorPageView|typeof xyz.swapee.rc.FileEditorPageView)|(!xyz.swapee.rc.IFileEditorPageViewHyperslice|typeof xyz.swapee.rc.FileEditorPageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPageView}
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperFileEditorPageView|typeof xyz.swapee.rc.HyperFileEditorPageView)|(!xyz.swapee.rc.IFileEditorPageView|typeof xyz.swapee.rc.FileEditorPageView)|(!xyz.swapee.rc.IFileEditorPageViewHyperslice|typeof xyz.swapee.rc.FileEditorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileEditorPageView}
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IFileEditorPageViewAspects|function(new: xyz.swapee.rc.IFileEditorPageViewAspects)} aides The list of aides that advise the IFileEditorPageView to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileEditorPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileEditorPageView.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperFileEditorPageView.Initialese[]) => xyz.swapee.rc.IHyperFileEditorPageView} xyz.swapee.rc.HyperFileEditorPageViewConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperFileEditorPageViewCaster&xyz.swapee.rc.IFileEditorPageView)} xyz.swapee.rc.IHyperFileEditorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageView} xyz.swapee.rc.IFileEditorPageView.typeof */
/** @interface xyz.swapee.rc.IHyperFileEditorPageView */
xyz.swapee.rc.IHyperFileEditorPageView = class extends /** @type {xyz.swapee.rc.IHyperFileEditorPageView.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileEditorPageView.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperFileEditorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileEditorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperFileEditorPageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperFileEditorPageView&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileEditorPageView.Initialese>)} xyz.swapee.rc.HyperFileEditorPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperFileEditorPageView} xyz.swapee.rc.IHyperFileEditorPageView.typeof */
/**
 * A concrete class of _IHyperFileEditorPageView_ instances.
 * @constructor xyz.swapee.rc.HyperFileEditorPageView
 * @implements {xyz.swapee.rc.IHyperFileEditorPageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileEditorPageView.Initialese>} ‎
 */
xyz.swapee.rc.HyperFileEditorPageView = class extends /** @type {xyz.swapee.rc.HyperFileEditorPageView.constructor&xyz.swapee.rc.IHyperFileEditorPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperFileEditorPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperFileEditorPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperFileEditorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileEditorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperFileEditorPageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperFileEditorPageView}
 */
xyz.swapee.rc.HyperFileEditorPageView.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperFileEditorPageView} */
xyz.swapee.rc.RecordIHyperFileEditorPageView

/** @typedef {xyz.swapee.rc.IHyperFileEditorPageView} xyz.swapee.rc.BoundIHyperFileEditorPageView */

/** @typedef {xyz.swapee.rc.HyperFileEditorPageView} xyz.swapee.rc.BoundHyperFileEditorPageView */

/**
 * Contains getters to cast the _IHyperFileEditorPageView_ interface.
 * @interface xyz.swapee.rc.IHyperFileEditorPageViewCaster
 */
xyz.swapee.rc.IHyperFileEditorPageViewCaster = class { }
/**
 * Cast the _IHyperFileEditorPageView_ instance into the _BoundIHyperFileEditorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIHyperFileEditorPageView}
 */
xyz.swapee.rc.IHyperFileEditorPageViewCaster.prototype.asIHyperFileEditorPageView
/**
 * Access the _HyperFileEditorPageView_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperFileEditorPageView}
 */
xyz.swapee.rc.IHyperFileEditorPageViewCaster.prototype.superHyperFileEditorPageView

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageViewHyperslice
 */
xyz.swapee.rc.IFileEditorPageViewHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageView._viewFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView._viewFileEditor>} */ (void 0)
    /**
     * The internal view handler for the `fileEditor` action.
     */
    this.getFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageView._getFileEditor|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView._getFileEditor>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageView._setFileEditorCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView._setFileEditorCtx>} */ (void 0)
    /**
     * The _FileEditor_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageView._FileEditorView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView._FileEditorView>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageViewHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageViewHyperslice

/**
 * A concrete class of _IFileEditorPageViewHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageViewHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileEditorPageViewHyperslice = class extends xyz.swapee.rc.IFileEditorPageViewHyperslice { }
xyz.swapee.rc.FileEditorPageViewHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageViewHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileEditorPageViewBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileEditorPageViewBindingHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageView.__viewFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView.__viewFileEditor<THIS>>} */ (void 0)
    /**
     * The internal view handler for the `fileEditor` action.
     */
    this.getFileEditor=/** @type {!xyz.swapee.rc.IFileEditorPageView.__getFileEditor<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView.__getFileEditor<THIS>>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setFileEditorCtx=/** @type {!xyz.swapee.rc.IFileEditorPageView.__setFileEditorCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView.__setFileEditorCtx<THIS>>} */ (void 0)
    /**
     * The _FileEditor_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.FileEditorView=/** @type {!xyz.swapee.rc.IFileEditorPageView.__FileEditorView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileEditorPageView.__FileEditorView<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileEditorPageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileEditorPageViewBindingHyperslice

/**
 * A concrete class of _IFileEditorPageViewBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageViewBindingHyperslice
 * @implements {xyz.swapee.rc.IFileEditorPageViewBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileEditorPageViewBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileEditorPageViewBindingHyperslice = class extends xyz.swapee.rc.IFileEditorPageViewBindingHyperslice { }
xyz.swapee.rc.FileEditorPageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileEditorPageViewBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPageViewFields&engineering.type.IEngineer&xyz.swapee.rc.IFileEditorPageViewCaster&_idio.IRedirectMod&xyz.swapee.rc.UFileEditorPageView)} xyz.swapee.rc.IFileEditorPageView.constructor */
/** @interface xyz.swapee.rc.IFileEditorPageView */
xyz.swapee.rc.IFileEditorPageView = class extends /** @type {xyz.swapee.rc.IFileEditorPageView.constructor&engineering.type.IEngineer.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.UFileEditorPageView.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileEditorPageView.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IFileEditorPageView.viewFileEditor} */
xyz.swapee.rc.IFileEditorPageView.prototype.viewFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageView.getFileEditor} */
xyz.swapee.rc.IFileEditorPageView.prototype.getFileEditor = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageView.setFileEditorCtx} */
xyz.swapee.rc.IFileEditorPageView.prototype.setFileEditorCtx = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageView.FileEditorPartial} */
xyz.swapee.rc.IFileEditorPageView.prototype.FileEditorPartial = function() {}
/** @type {xyz.swapee.rc.IFileEditorPageView.FileEditorView} */
xyz.swapee.rc.IFileEditorPageView.prototype.FileEditorView = function() {}

/** @typedef {function(new: xyz.swapee.rc.IFileEditorPageView&engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageView.Initialese>)} xyz.swapee.rc.FileEditorPageView.constructor */
/**
 * A concrete class of _IFileEditorPageView_ instances.
 * @constructor xyz.swapee.rc.FileEditorPageView
 * @implements {xyz.swapee.rc.IFileEditorPageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorPageView.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorPageView = class extends /** @type {xyz.swapee.rc.FileEditorPageView.constructor&xyz.swapee.rc.IFileEditorPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileEditorPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorPageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorPageView}
 */
xyz.swapee.rc.FileEditorPageView.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorPageView.
 * @interface xyz.swapee.rc.IFileEditorPageViewFields
 */
xyz.swapee.rc.IFileEditorPageViewFields = class { }
/**
 * The navs for _GET_ paths (e.g., to use for redirects).
 */
xyz.swapee.rc.IFileEditorPageViewFields.prototype.GET = /** @type {!xyz.swapee.rc.IFileEditorPageView.GET} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileEditorPageViewFields.prototype.fileEditorTranslations = /** @type {!Object<string, !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation>} */ (void 0)

/** @typedef {xyz.swapee.rc.IFileEditorPageView} */
xyz.swapee.rc.RecordIFileEditorPageView

/** @typedef {xyz.swapee.rc.IFileEditorPageView} xyz.swapee.rc.BoundIFileEditorPageView */

/** @typedef {xyz.swapee.rc.FileEditorPageView} xyz.swapee.rc.BoundFileEditorPageView */

/**
 * @typedef {Object} xyz.swapee.rc.IFileEditorPageView.GET The navs for _GET_ paths (e.g., to use for redirects).
 * @prop {xyz.swapee.rc.FileEditor.fileEditorNav} fileEditor
 */

/**
 * Contains getters to cast the _IFileEditorPageView_ interface.
 * @interface xyz.swapee.rc.IFileEditorPageViewCaster
 */
xyz.swapee.rc.IFileEditorPageViewCaster = class { }
/**
 * Cast the _IFileEditorPageView_ instance into the _BoundIFileEditorPageView_ type.
 * @type {!xyz.swapee.rc.BoundIFileEditorPageView}
 */
xyz.swapee.rc.IFileEditorPageViewCaster.prototype.asIFileEditorPageView
/**
 * Cast the _IFileEditorPageView_ instance into the _BoundIFileEditorPage_ type.
 * @type {!xyz.swapee.rc.BoundIFileEditorPage}
 */
xyz.swapee.rc.IFileEditorPageViewCaster.prototype.asIFileEditorPage
/**
 * Access the _FileEditorPageView_ prototype.
 * @type {!xyz.swapee.rc.BoundFileEditorPageView}
 */
xyz.swapee.rc.IFileEditorPageViewCaster.prototype.superFileEditorPageView

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeViewFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeViewFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeViewFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeViewFileEditor} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPageView.ViewFileEditorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewFileEditor` method from being executed.
 * - `sub` _(value: !IFileEditorPageView.FileEditorView) =&gt; void_ Cancels a call to `viewFileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeViewFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditor} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `after` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorThrows<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `viewFileEditor` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `afterReturns` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileEditorPageView.FileEditorView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterViewFileEditorCancels<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterViewFileEditorCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterViewFileEditorCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachViewFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachViewFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachViewFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachViewFileEditor} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPageView.ViewFileEditorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewFileEditor` method from being executed.
 * - `sub` _(value: !IFileEditorPageView.FileEditorView) =&gt; void_ Cancels a call to `viewFileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachViewFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachViewFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachViewFileEditor} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `after` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachViewFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachViewFileEditorReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachViewFileEditorReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachViewFileEditorReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterViewFileEditorPointcutData} [data] Metadata passed to the pointcuts of _viewFileEditor_ at the `afterEach` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `args` _IFileEditorPageView.ViewFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.ViewFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachViewFileEditorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeGetFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeGetFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeGetFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeGetFileEditor} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPageView.GetFileEditorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getFileEditor` method from being executed.
 * - `sub` _(value: !IFileEditorPageView.FileEditorView) =&gt; void_ Cancels a call to `getFileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeGetFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditor} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `after` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorThrows<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getFileEditor` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `afterReturns` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileEditorPageView.FileEditorView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterGetFileEditorCancels<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterGetFileEditorCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterGetFileEditorCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachGetFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachGetFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachGetFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachGetFileEditor} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPageView.GetFileEditorNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getFileEditor` method from being executed.
 * - `sub` _(value: !IFileEditorPageView.FileEditorView) =&gt; void_ Cancels a call to `getFileEditor` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachGetFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditor<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachGetFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachGetFileEditor} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `after` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachGetFileEditor = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditorReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachGetFileEditorReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachGetFileEditorReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachGetFileEditorReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterGetFileEditorPointcutData} [data] Metadata passed to the pointcuts of _getFileEditor_ at the `afterEach` joinpoint.
 * - `res` _!IFileEditorPageView.FileEditorView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `args` _IFileEditorPageView.GetFileEditorNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.GetFileEditorPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachGetFileEditorReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeSetFileEditorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeSetFileEditorCtx<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeSetFileEditorCtx */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeSetFileEditorCtx} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPageView.SetFileEditorCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setFileEditorCtx` method from being executed.
 * - `sub` _(value: !IFileEditorPage.fileEditor.OptCtx) =&gt; void_ Cancels a call to `setFileEditorCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeSetFileEditorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtx<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtx */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtx} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `after` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxThrows<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `setFileEditorCtx` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `afterReturns` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptCtx_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileEditorPage.fileEditor.OptCtx) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterSetFileEditorCtxCancels<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterSetFileEditorCtxCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterSetFileEditorCtxCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachSetFileEditorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEachSetFileEditorCtx<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEachSetFileEditorCtx */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachSetFileEditorCtx} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPageView.SetFileEditorCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setFileEditorCtx` method from being executed.
 * - `sub` _(value: !IFileEditorPage.fileEditor.OptCtx) =&gt; void_ Cancels a call to `setFileEditorCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEachSetFileEditorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtx<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachSetFileEditorCtx */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachSetFileEditorCtx} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `after` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachSetFileEditorCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEachSetFileEditorCtxReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEachSetFileEditorCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachSetFileEditorCtxReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterSetFileEditorCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileEditorCtx_ at the `afterEach` joinpoint.
 * - `res` _!IFileEditorPage.fileEditor.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `args` _IFileEditorPageView.SetFileEditorCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.SetFileEditorCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEachSetFileEditorCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorPartialPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorPartial<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._before_FileEditorPartial */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.before_FileEditorPartial} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorPartialPointcutData} [data] Metadata passed to the pointcuts of _FileEditorPartial_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorPageView.FileEditorPartialNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `FileEditorPartial` method from being executed.
 * - `sub` _(value: engineering.type.VNode) =&gt; void_ Cancels a call to `FileEditorPartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `args` _IFileEditorPageView.FileEditorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.before_FileEditorPartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorPartialPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartial<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartial */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartial} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorPartialPointcutData} [data] Metadata passed to the pointcuts of _FileEditorPartial_ at the `after` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `args` _IFileEditorPageView.FileEditorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorPartialPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialThrows<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorPartialPointcutData} [data] Metadata passed to the pointcuts of _FileEditorPartial_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `FileEditorPartial` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `args` _IFileEditorPageView.FileEditorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorPartialPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorPartialPointcutData} [data] Metadata passed to the pointcuts of _FileEditorPartial_ at the `afterReturns` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `sub` _(value: engineering.type.VNode) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `args` _IFileEditorPageView.FileEditorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorPartialPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorPartialCancels<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorPartialCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorPartialPointcutData} [data] Metadata passed to the pointcuts of _FileEditorPartial_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `args` _IFileEditorPageView.FileEditorPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorPartialCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__before_FileEditorView<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._before_FileEditorView */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.before_FileEditorView} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `FileEditorView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.before_FileEditorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorView<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorView */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorView} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewThrows<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewThrows */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterThrowsFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `FileEditorView` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterReturnsFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__after_FileEditorViewCancels<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._after_FileEditorViewCancels */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterCancelsFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.after_FileEditorViewCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEach_FileEditorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__beforeEach_FileEditorView<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._beforeEach_FileEditorView */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEach_FileEditorView} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.BeforeFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `FileEditorView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.beforeEach_FileEditorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorView<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEach_FileEditorView */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEach_FileEditorView} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEach_FileEditorView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorViewPointcutData) => void} xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageViewJoinpointModel.__afterEach_FileEditorViewReturns<!xyz.swapee.rc.IFileEditorPageViewJoinpointModel>} xyz.swapee.rc.IFileEditorPageViewJoinpointModel._afterEach_FileEditorViewReturns */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEach_FileEditorViewReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPageViewJoinpointModel.AfterFileEditorViewPointcutData} [data] Metadata passed to the pointcuts of _FileEditorView_ at the `afterEach` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorPageViewJoinpointModel.FileEditorViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileEditorPageViewJoinpointModel.afterEach_FileEditorViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx, form: !xyz.swapee.rc.IFileEditorPage.fileEditor.Form) => !xyz.swapee.rc.IFileEditorPageView.FileEditorView} xyz.swapee.rc.IFileEditorPageView.__viewFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageView.__viewFileEditor<!xyz.swapee.rc.IFileEditorPageView>} xyz.swapee.rc.IFileEditorPageView._viewFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageView.viewFileEditor} */
/**
 * Assigns required view data to the context, then redirects, or invokes another pagelet. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx} ctx The context.
 * - `validation` _!IFileEditorPage.fileEditor.Validation_ The validation.
 * - `errors` _!IFileEditorPage.fileEditor.Errors_ The errors.
 * - `answers` _!IFileEditorPage.fileEditor.Answers_ The answers.
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} form The form.
 * - `locale` _string_
 * @return {!xyz.swapee.rc.IFileEditorPageView.FileEditorView}
 */
xyz.swapee.rc.IFileEditorPageView.viewFileEditor = function(ctx, form) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx, form: !xyz.swapee.rc.IFileEditorPage.fileEditor.Form) => (void|!xyz.swapee.rc.IFileEditorPageView.FileEditorView)} xyz.swapee.rc.IFileEditorPageView.__getFileEditor
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageView.__getFileEditor<!xyz.swapee.rc.IFileEditorPageView>} xyz.swapee.rc.IFileEditorPageView._getFileEditor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageView.getFileEditor} */
/**
 * The internal view handler for the `fileEditor` action. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Ctx} ctx The context.
 * - `validation` _!IFileEditorPage.fileEditor.Validation_ The validation.
 * - `errors` _!IFileEditorPage.fileEditor.Errors_ The errors.
 * - `answers` _!IFileEditorPage.fileEditor.Answers_ The answers.
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} form The form.
 * - `locale` _string_
 * @return {void|!xyz.swapee.rc.IFileEditorPageView.FileEditorView}
 */
xyz.swapee.rc.IFileEditorPageView.getFileEditor = function(ctx, form) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IFileEditorPage.fileEditor.Answers, translation: !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation) => !xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx} xyz.swapee.rc.IFileEditorPageView.__setFileEditorCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageView.__setFileEditorCtx<!xyz.swapee.rc.IFileEditorPageView>} xyz.swapee.rc.IFileEditorPageView._setFileEditorCtx */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageView.setFileEditorCtx} */
/**
 * Assigns the context based on answers and translations. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} answers The answers.
 * - `locale` _string_
 * - `content` _&#42;_ ⤴ *IFileEditorPage.filterReadFile.Answers*
 * @param {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation} translation The translation for the user's locale.
 * @return {!xyz.swapee.rc.IFileEditorPage.fileEditor.OptCtx}
 */
xyz.swapee.rc.IFileEditorPageView.setFileEditorCtx = function(answers, translation) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IFileEditorPage.fileEditor.Answers, errors: !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Errors, actions: !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions, translation: !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IFileEditorPageView.__FileEditorPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageView.__FileEditorPartial<!xyz.swapee.rc.IFileEditorPageView>} xyz.swapee.rc.IFileEditorPageView._FileEditorPartial */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageView.FileEditorPartial} */
/**
 * The partial.
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} answers The answers.
 * - `locale` _string_
 * - `content` _&#42;_ ⤴ *IFileEditorPage.filterReadFile.Answers*
 * @param {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions} actions The actions.
 * - `fileEditor` _FileEditor.fileEditorNav_
 * - `_fileEditor` _!IFileEditorPage.fileEditor.Form_
 * - `_filterReadFile` _IFileEditorPage.filterReadFile.Form_ The form of the `filterReadFile` action.
 * - `filterReadFile` _!FileEditor.filterReadFileNav_
 * - `_filterSaveFile` _IFileEditorPage.filterSaveFile.Form_ The form of the `filterSaveFile` action.
 * - `filterSaveFile` _!FileEditor.filterSaveFileNav_
 * @param {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial = function(answers, errors, actions, translation) {}

/** @typedef {xyz.swapee.rc.IFileEditorPage.fileEditor.Errors&xyz.swapee.rc.IFileEditorPage.fileEditor.Validation} xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Errors The errors. */

/**
 * The actions.
 * @record xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions = class { }
/**
 *
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions.prototype.fileEditor = /** @type {xyz.swapee.rc.FileEditor.fileEditorNav} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions.prototype._fileEditor = /** @type {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} */ (void 0)
/**
 * The form of the `filterReadFile` action.
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions.prototype._filterReadFile = /** @type {xyz.swapee.rc.IFileEditorPage.filterReadFile.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions.prototype.filterReadFile = /** @type {!xyz.swapee.rc.FileEditor.filterReadFileNav} */ (void 0)
/**
 * The form of the `filterSaveFile` action.
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions.prototype._filterSaveFile = /** @type {xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions.prototype.filterSaveFile = /** @type {!xyz.swapee.rc.FileEditor.filterSaveFileNav} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation The translation for the chosen user locale. */

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IFileEditorPage.fileEditor.Answers, errors: !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Errors, actions: !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions, translation: !xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IFileEditorPageView.__FileEditorView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileEditorPageView.__FileEditorView<!xyz.swapee.rc.IFileEditorPageView>} xyz.swapee.rc.IFileEditorPageView._FileEditorView */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageView.FileEditorView} */
/**
 * The _FileEditor_ view with partials and translation mechanics that has
 * access to answers written by the controller. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Answers} answers The answers.
 * - `locale` _string_
 * - `content` _&#42;_ ⤴ *IFileEditorPage.filterReadFile.Answers*
 * @param {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Actions} actions The actions.
 * - `fileEditor` _FileEditor.fileEditorNav_
 * - `_fileEditor` _!IFileEditorPage.fileEditor.Form_
 * - `_filterReadFile` _IFileEditorPage.filterReadFile.Form_ The form of the `filterReadFile` action.
 * - `filterReadFile` _!FileEditor.filterReadFileNav_
 * - `_filterSaveFile` _IFileEditorPage.filterSaveFile.Form_ The form of the `filterSaveFile` action.
 * - `filterSaveFile` _!FileEditor.filterSaveFileNav_
 * @param {!xyz.swapee.rc.IFileEditorPageView.FileEditorPartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IFileEditorPageView.FileEditorView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.IFileEditorPageViewJoinpointModel,xyz.swapee.rc.IFileEditorPageView
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-editor/file-editor.ws/file-editor/FileEditor/design/api.xml}  3093938d7f97e1694cc3c48ce63e6445 */
/**
 * The `fileEditor` navigation metadata.
 * @constructor xyz.swapee.rc.FileEditor.FileEditorNav
 */
xyz.swapee.rc.FileEditor.FileEditorNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.FileEditor.FileEditorNav.prototype.fileEditor = /** @type {number} */ (void 0)

/**
 * The `filterReadFile` navigation metadata.
 * @constructor xyz.swapee.rc.FileEditor.FilterReadFileNav
 */
xyz.swapee.rc.FileEditor.FilterReadFileNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.FileEditor.FilterReadFileNav.prototype.filterReadFile = /** @type {number} */ (void 0)

/**
 * The `filterSaveFile` navigation metadata.
 * @constructor xyz.swapee.rc.FileEditor.FilterSaveFileNav
 */
xyz.swapee.rc.FileEditor.FilterSaveFileNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.FileEditor.FilterSaveFileNav.prototype.filterSaveFile = /** @type {number} */ (void 0)

/**
 * The ids of the methods.
 * @constructor xyz.swapee.rc.fileEditorMethodsIds
 */
xyz.swapee.rc.fileEditorMethodsIds = class { }
xyz.swapee.rc.fileEditorMethodsIds.prototype.constructor = xyz.swapee.rc.fileEditorMethodsIds

/**
 * The ids of the arcs.
 * @constructor xyz.swapee.rc.fileEditorArcsIds
 */
xyz.swapee.rc.fileEditorArcsIds = class { }
xyz.swapee.rc.fileEditorArcsIds.prototype.constructor = xyz.swapee.rc.fileEditorArcsIds

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileEditorImpl.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileEditorImpl)} xyz.swapee.rc.AbstractFileEditorImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.FileEditorImpl} xyz.swapee.rc.FileEditorImpl.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileEditorImpl` interface.
 * @constructor xyz.swapee.rc.AbstractFileEditorImpl
 */
xyz.swapee.rc.AbstractFileEditorImpl = class extends /** @type {xyz.swapee.rc.AbstractFileEditorImpl.constructor&xyz.swapee.rc.FileEditorImpl.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileEditorImpl.prototype.constructor = xyz.swapee.rc.AbstractFileEditorImpl
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileEditorImpl.class = /** @type {typeof xyz.swapee.rc.AbstractFileEditorImpl} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileEditorImpl|typeof xyz.swapee.rc.FileEditorImpl)|(!xyz.swapee.rc.IFileEditorPageAspects|typeof xyz.swapee.rc.FileEditorPageAspects)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.IFileEditorPageHyperslice|typeof xyz.swapee.rc.FileEditorPageHyperslice)|(!xyz.swapee.rc.IFileEditorPageViewHyperslice|typeof xyz.swapee.rc.FileEditorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileEditorImpl.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileEditorImpl}
 */
xyz.swapee.rc.AbstractFileEditorImpl.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorImpl}
 */
xyz.swapee.rc.AbstractFileEditorImpl.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileEditorImpl|typeof xyz.swapee.rc.FileEditorImpl)|(!xyz.swapee.rc.IFileEditorPageAspects|typeof xyz.swapee.rc.FileEditorPageAspects)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.IFileEditorPageHyperslice|typeof xyz.swapee.rc.FileEditorPageHyperslice)|(!xyz.swapee.rc.IFileEditorPageViewHyperslice|typeof xyz.swapee.rc.FileEditorPageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorImpl}
 */
xyz.swapee.rc.AbstractFileEditorImpl.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileEditorImpl|typeof xyz.swapee.rc.FileEditorImpl)|(!xyz.swapee.rc.IFileEditorPageAspects|typeof xyz.swapee.rc.FileEditorPageAspects)|(!xyz.swapee.rc.IFileEditorPage|typeof xyz.swapee.rc.FileEditorPage)|(!xyz.swapee.rc.IFileEditorPageHyperslice|typeof xyz.swapee.rc.FileEditorPageHyperslice)|(!xyz.swapee.rc.IFileEditorPageViewHyperslice|typeof xyz.swapee.rc.FileEditorPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileEditorImpl}
 */
xyz.swapee.rc.AbstractFileEditorImpl.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileEditorPageAspects&xyz.swapee.rc.IFileEditorPage&xyz.swapee.rc.IFileEditorPageHyperslice&xyz.swapee.rc.IFileEditorPageViewHyperslice)} xyz.swapee.rc.IFileEditorImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageHyperslice} xyz.swapee.rc.IFileEditorPageHyperslice.typeof */
/** @typedef {typeof xyz.swapee.rc.IFileEditorPageViewHyperslice} xyz.swapee.rc.IFileEditorPageViewHyperslice.typeof */
/** @interface xyz.swapee.rc.IFileEditorImpl */
xyz.swapee.rc.IFileEditorImpl = class extends /** @type {xyz.swapee.rc.IFileEditorImpl.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileEditorPageAspects.typeof&xyz.swapee.rc.IFileEditorPage.typeof&xyz.swapee.rc.IFileEditorPageHyperslice.typeof&xyz.swapee.rc.IFileEditorPageViewHyperslice.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileEditorImpl.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileEditorImpl&engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorImpl.Initialese>)} xyz.swapee.rc.FileEditorImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileEditorImpl} xyz.swapee.rc.IFileEditorImpl.typeof */
/**
 * A concrete class of _IFileEditorImpl_ instances.
 * @constructor xyz.swapee.rc.FileEditorImpl
 * @implements {xyz.swapee.rc.IFileEditorImpl} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileEditorImpl.Initialese>} ‎
 */
xyz.swapee.rc.FileEditorImpl = class extends /** @type {xyz.swapee.rc.FileEditorImpl.constructor&xyz.swapee.rc.IFileEditorImpl.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorImpl* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileEditorImpl.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileEditorImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileEditorImpl.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileEditorImpl}
 */
xyz.swapee.rc.FileEditorImpl.__extend = function(...Extensions) {}

/** @typedef {typeof xyz.swapee.rc.FileEditor.fileEditorNav} */
/**
 * Navigates to `fileEditor`.
 * @param {!xyz.swapee.rc.IFileEditorPage.fileEditor.Form} form
 * - `locale` _string_
 * @return {void}
 */
xyz.swapee.rc.FileEditor.fileEditorNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.FileEditor.filterReadFileNav} */
/**
 * Invokes the `filterReadFile` action.
 * @param {!xyz.swapee.rc.IFileEditorPage.filterReadFile.Form} form
 * - `path` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.FileEditor.filterReadFileNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.FileEditor.filterSaveFileNav} */
/**
 * Invokes the `filterSaveFile` action.
 * @param {!xyz.swapee.rc.IFileEditorPage.filterSaveFile.Form} form
 * - `saveFile` _&#42;_
 * - `content` _&#42;_
 * - `path` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.FileEditor.filterSaveFileNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.getFileEditor} */
/**
 * Allows to render the pagelet after it's been processed.
 * @return {void}
 */
xyz.swapee.rc.getFileEditor = function() {}

/** @typedef {typeof xyz.swapee.rc.fileEditor} */
/**
 * @param {!xyz.swapee.rc.IFileEditorImpl} implementation
 * @return {!xyz.swapee.rc.IFileEditorPage}
 */
xyz.swapee.rc.fileEditor = function(implementation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.FileEditor
/* @typal-end */