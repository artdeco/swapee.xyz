import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorPageViewAspects}
 */
function __AbstractFileEditorPageViewAspects() {}
__AbstractFileEditorPageViewAspects.prototype = /** @type {!_AbstractFileEditorPageViewAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPageViewAspects}
 */
class _AbstractFileEditorPageViewAspects { }
/**
 * The aspects of the *IFileEditorPageView*.
 * @extends {xyz.swapee.rc.AbstractFileEditorPageViewAspects} ‎
 */
class AbstractFileEditorPageViewAspects extends newAspects(
 _AbstractFileEditorPageViewAspects,399991590218,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspects} */
AbstractFileEditorPageViewAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspects} */
function AbstractFileEditorPageViewAspectsClass(){}

export default AbstractFileEditorPageViewAspects


AbstractFileEditorPageViewAspects[$implementations]=[
 __AbstractFileEditorPageViewAspects,
]