import FileEditorPageViewUniversal, {setFileEditorPageViewUniversalSymbols} from './anchors/FileEditorPageViewUniversal'
import { newAbstract, $implementations, prereturnFirst, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorPageView}
 */
function __AbstractFileEditorPageView() {}
__AbstractFileEditorPageView.prototype = /** @type {!_AbstractFileEditorPageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPageView}
 */
class _AbstractFileEditorPageView { }
/** @extends {xyz.swapee.rc.AbstractFileEditorPageView} ‎ */
class AbstractFileEditorPageView extends newAbstract(
 _AbstractFileEditorPageView,399991590222,null,{
  asIFileEditorPageView:1,
  superFileEditorPageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageView} */
AbstractFileEditorPageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageView} */
function AbstractFileEditorPageViewClass(){}

export default AbstractFileEditorPageView


AbstractFileEditorPageView[$implementations]=[
 __AbstractFileEditorPageView,
 FileEditorPageViewUniversal,
 AbstractFileEditorPageViewClass.prototype=/**@type {!xyz.swapee.rc.IFileEditorPageView}*/({
  viewFileEditor:prereturnFirst,
  getFileEditor:precombined,
  setFileEditorCtx:precombined,
  FileEditorView:prereturnFirst,
 }),
]