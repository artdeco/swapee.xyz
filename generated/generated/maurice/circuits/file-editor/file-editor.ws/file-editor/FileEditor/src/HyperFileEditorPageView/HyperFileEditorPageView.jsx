import '../../types'
import FileEditorPageView from '../FileEditorPageView'
import AbstractHyperFileEditorPageView from '../../gen/hyper/AbstractHyperFileEditorPageView'
import {AssignLocale} from '@websystems/parthenon'
import FileEditorPageArcsIds from '../../gen/arcs-ids'
import FileEditorPageMethodsIds from '../../gen/methods-ids'
import {RenderAide} from '@idio2/server'
import AbstractFileEditorPageViewAspects from '../../gen/aspects/AbstractFileEditorPageViewAspects'

/** @extends {xyz.swapee.rc.HyperFileEditorPageView}  */
export default class HyperFileEditorPageView extends AbstractHyperFileEditorPageView.consults(
 AbstractFileEditorPageViewAspects.class.prototype=/**@type {!xyz.swapee.rc.FileEditorPageViewAspects} */({
  beforeFileEditor:[
   AssignLocale,
  ],
  afterEachSetFileEditorCtxReturns:[
   function({res:res}) {
    const{asIFileEditorPage:{asCContext:ctx}}=this
    Object.assign(ctx,res)
   },
  ],
  afterViewFileEditorReturns:[
   RenderAide.RenderActionableJSXCtx({
    ...FileEditorPageMethodsIds,
   },{
    attrsProcess:[
    ],
    arcs:FileEditorPageArcsIds,
   },{
    fileEditorTranslations:1,
   }),
  ],
 }),
).implements(
 FileEditorPageView,
){}