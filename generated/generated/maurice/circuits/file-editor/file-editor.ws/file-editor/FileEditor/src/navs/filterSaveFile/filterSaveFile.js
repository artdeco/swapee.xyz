import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.FileEditor.FilterSaveFileNav} */
export const FilterSaveFileNav={
 toString(){return'filterSaveFile'},
 filterSaveFile:methodsIds.filterSaveFile,
}
export default FilterSaveFileNav