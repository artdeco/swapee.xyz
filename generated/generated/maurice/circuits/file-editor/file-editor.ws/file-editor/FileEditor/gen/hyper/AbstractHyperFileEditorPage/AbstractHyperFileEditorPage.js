import FileEditorPageAspectsInstaller from '../../aspects-installers/FileEditorPageAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperFileEditorPage}
 */
function __AbstractHyperFileEditorPage() {}
__AbstractHyperFileEditorPage.prototype = /** @type {!_AbstractHyperFileEditorPage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperFileEditorPage}
 */
class _AbstractHyperFileEditorPage {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperFileEditorPage
    .clone({aspectsInstaller:FileEditorPageAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.rc.AbstractHyperFileEditorPage} ‎ */
class AbstractHyperFileEditorPage extends newAbstract(
 _AbstractHyperFileEditorPage,39999159028,null,{
  asIHyperFileEditorPage:1,
  superHyperFileEditorPage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperFileEditorPage} */
AbstractHyperFileEditorPage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperFileEditorPage} */
function AbstractHyperFileEditorPageClass(){}

export default AbstractHyperFileEditorPage


AbstractHyperFileEditorPage[$implementations]=[
 __AbstractHyperFileEditorPage,
]