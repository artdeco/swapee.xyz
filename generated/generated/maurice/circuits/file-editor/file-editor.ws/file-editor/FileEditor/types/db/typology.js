/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.rc.IFileEditorPageJoinpointModelHyperslice': {
  'id': 39999159021,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageJoinpointModelBindingHyperslice': {
  'id': 39999159022,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageJoinpointModel': {
  'id': 39999159023,
  'symbols': {},
  'methods': {
   'beforeFileEditor': 1,
   'afterFileEditor': 2,
   'afterFileEditorThrows': 3,
   'afterFileEditorReturns': 4,
   'afterFileEditorCancels': 5,
   'beforeEachFileEditor': 6,
   'afterEachFileEditor': 7,
   'afterEachFileEditorReturns': 8,
   'beforeFilterReadFile': 9,
   'afterFilterReadFile': 10,
   'afterFilterReadFileThrows': 11,
   'afterFilterReadFileReturns': 12,
   'afterFilterReadFileCancels': 13,
   'beforeEachFilterReadFile': 14,
   'afterEachFilterReadFile': 15,
   'afterEachFilterReadFileReturns': 16,
   'beforeFilterSaveFile': 17,
   'afterFilterSaveFile': 18,
   'afterFilterSaveFileThrows': 19,
   'afterFilterSaveFileReturns': 20,
   'afterFilterSaveFileCancels': 21,
   'beforeEachFilterSaveFile': 22,
   'afterEachFilterSaveFile': 23,
   'afterEachFilterSaveFileReturns': 24
  }
 },
 'xyz.swapee.rc.IFileEditorPageAspectsInstaller': {
  'id': 39999159024,
  'symbols': {},
  'methods': {
   'fileEditor': 1,
   'filterReadFile': 2,
   'filterSaveFile': 3
  }
 },
 'xyz.swapee.rc.UFileEditorPage': {
  'id': 39999159025,
  'symbols': {
   'fileEditorPage': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BFileEditorPageAspects': {
  'id': 39999159026,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IFileEditorPageAspects': {
  'id': 39999159027,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperFileEditorPage': {
  'id': 39999159028,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageHyperslice': {
  'id': 39999159029,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageBindingHyperslice': {
  'id': 399991590210,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPage': {
  'id': 399991590211,
  'symbols': {},
  'methods': {
   'fileEditor': 1,
   'filterReadFile': 2,
   'filterSaveFile': 3
  }
 },
 'xyz.swapee.rc.IFileEditorPageViewJoinpointModelHyperslice': {
  'id': 399991590212,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageViewJoinpointModelBindingHyperslice': {
  'id': 399991590213,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageViewJoinpointModel': {
  'id': 399991590214,
  'symbols': {},
  'methods': {
   'beforeViewFileEditor': 1,
   'afterViewFileEditor': 2,
   'afterViewFileEditorThrows': 3,
   'afterViewFileEditorReturns': 4,
   'afterViewFileEditorCancels': 5,
   'beforeEachViewFileEditor': 6,
   'afterEachViewFileEditor': 7,
   'afterEachViewFileEditorReturns': 8,
   'beforeGetFileEditor': 9,
   'afterGetFileEditor': 10,
   'afterGetFileEditorThrows': 11,
   'afterGetFileEditorReturns': 12,
   'afterGetFileEditorCancels': 13,
   'beforeEachGetFileEditor': 14,
   'afterEachGetFileEditor': 15,
   'afterEachGetFileEditorReturns': 16,
   'beforeSetFileEditorCtx': 17,
   'afterSetFileEditorCtx': 18,
   'afterSetFileEditorCtxThrows': 19,
   'afterSetFileEditorCtxReturns': 20,
   'afterSetFileEditorCtxCancels': 21,
   'beforeEachSetFileEditorCtx': 22,
   'afterEachSetFileEditorCtx': 23,
   'afterEachSetFileEditorCtxReturns': 24,
   'before_FileEditorPartial': 25,
   'after_FileEditorPartial': 26,
   'after_FileEditorPartialThrows': 27,
   'after_FileEditorPartialReturns': 28,
   'after_FileEditorPartialCancels': 29,
   'before_FileEditorView': 30,
   'after_FileEditorView': 31,
   'after_FileEditorViewThrows': 32,
   'after_FileEditorViewReturns': 33,
   'after_FileEditorViewCancels': 34,
   'beforeEach_FileEditorView': 35,
   'afterEach_FileEditorView': 36,
   'afterEach_FileEditorViewReturns': 37
  }
 },
 'xyz.swapee.rc.IFileEditorPageViewAspectsInstaller': {
  'id': 399991590215,
  'symbols': {},
  'methods': {
   'viewFileEditor': 1,
   'getFileEditor': 2,
   'setFileEditorCtx': 3,
   'FileEditorPartial': 4,
   'FileEditorView': 5
  }
 },
 'xyz.swapee.rc.UFileEditorPageView': {
  'id': 399991590216,
  'symbols': {
   'fileEditorPageView': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BFileEditorPageViewAspects': {
  'id': 399991590217,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IFileEditorPageViewAspects': {
  'id': 399991590218,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperFileEditorPageView': {
  'id': 399991590219,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageViewHyperslice': {
  'id': 399991590220,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageViewBindingHyperslice': {
  'id': 399991590221,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileEditorPageView': {
  'id': 399991590222,
  'symbols': {},
  'methods': {
   'viewFileEditor': 1,
   'getFileEditor': 2,
   'setFileEditorCtx': 3,
   'FileEditorPartial': 4,
   'FileEditorView': 5
  }
 },
 'xyz.swapee.rc.IFileEditorImpl': {
  'id': 399991590223,
  'symbols': {},
  'methods': {}
 }
})