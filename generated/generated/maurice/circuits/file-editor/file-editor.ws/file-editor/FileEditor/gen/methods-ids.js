export const FileEditorPageMethodsIds={
 fileEditor:7546942,
 filterReadFile:'e8abe',
 filterSaveFile:'f0d42',
}
export const FileEditorPageLiteralMethodsIds={
 'fileEditor':7546942,
 'filterReadFile':'e8abe',
 'filterSaveFile':'f0d42',
}

export default FileEditorPageMethodsIds