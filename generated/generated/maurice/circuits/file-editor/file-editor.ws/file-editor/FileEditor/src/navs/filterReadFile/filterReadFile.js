import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.FileEditor.FilterReadFileNav} */
export const FilterReadFileNav={
 toString(){return'filterReadFile'},
 filterReadFile:methodsIds.filterReadFile,
}
export default FilterReadFileNav