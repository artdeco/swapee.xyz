import FileEditorPageUniversal, {setFileEditorPageUniversalSymbols} from './anchors/FileEditorPageUniversal'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorPage}
 */
function __AbstractFileEditorPage() {}
__AbstractFileEditorPage.prototype = /** @type {!_AbstractFileEditorPage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPage}
 */
class _AbstractFileEditorPage { }
/** @extends {xyz.swapee.rc.AbstractFileEditorPage} ‎ */
class AbstractFileEditorPage extends newAbstract(
 _AbstractFileEditorPage,399991590211,null,{
  asIFileEditorPage:1,
  superFileEditorPage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileEditorPage} */
AbstractFileEditorPage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileEditorPage} */
function AbstractFileEditorPageClass(){}

export default AbstractFileEditorPage


AbstractFileEditorPage[$implementations]=[
 __AbstractFileEditorPage,
 FileEditorPageUniversal,
 AbstractFileEditorPageClass.prototype=/**@type {!xyz.swapee.rc.IFileEditorPage}*/({
  fileEditor:precombined,
  filterReadFile:precombined,
  filterSaveFile:precombined,
 }),
]