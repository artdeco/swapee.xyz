const FileEditorPageArcsIds={
 locale:2016775,
 path:3345881,
 saveFile:8040969,
 content:1429749,
}
export const FileEditorPageLiteralArcsIds={
 'locale':2016775,
 'path':3345881,
 'saveFile':8040969,
 'content':1429749,
}
export const ReverseFileEditorPageArcsIds=Object.keys(FileEditorPageArcsIds)
 .reduce((a,k)=>{a[FileEditorPageArcsIds[`${k}`]]=k;return a},{})

export default FileEditorPageArcsIds