import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorPageUniversal}
 */
function __FileEditorPageUniversal() {}
__FileEditorPageUniversal.prototype = /** @type {!_FileEditorPageUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPageUniversal}
 */
class _FileEditorPageUniversal { }
/**
 * A trait that allows to access an _IFileEditorPage_ object via a `.fileEditorPage` field.
 * @extends {xyz.swapee.rc.AbstractFileEditorPageUniversal} ‎
 */
class FileEditorPageUniversal extends newAbstract(
 _FileEditorPageUniversal,39999159025,null,{
  asIFileEditorPage:1,
  asUFileEditorPage:3,
  fileEditorPage:4,
  asFileEditorPage:5,
  superFileEditorPageUniversal:2,
 },false,{
  fileEditorPage:{_fileEditorPage:1},
 }) {}

/** @type {typeof xyz.swapee.rc.UFileEditorPage} */
FileEditorPageUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UFileEditorPage} */
function FileEditorPageUniversalClass(){}

export default FileEditorPageUniversal

/** @type {xyz.swapee.rc.UFileEditorPage.Symbols} */
export const FileEditorPageUniversalSymbols=FileEditorPageUniversal.Symbols
/** @type {xyz.swapee.rc.UFileEditorPage.getSymbols} */
export const getFileEditorPageUniversalSymbols=FileEditorPageUniversal.getSymbols
/** @type {xyz.swapee.rc.UFileEditorPage.setSymbols} */
export const setFileEditorPageUniversalSymbols=FileEditorPageUniversal.setSymbols

FileEditorPageUniversal[$implementations]=[
 __FileEditorPageUniversal,
 FileEditorPageUniversalClass.prototype=/**@type {!xyz.swapee.rc.UFileEditorPage}*/({
  [FileEditorPageUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UFileEditorPage} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UFileEditorPage.Initialese}*/({
   fileEditorPage:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.FileEditorPageMetaUniversal}*/
export const FileEditorPageMetaUniversal=FileEditorPageUniversal.MetaUniversal