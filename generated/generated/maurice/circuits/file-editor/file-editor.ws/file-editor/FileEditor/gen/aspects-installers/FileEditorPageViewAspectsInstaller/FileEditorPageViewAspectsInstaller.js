import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorPageViewAspectsInstaller}
 */
function __FileEditorPageViewAspectsInstaller() {}
__FileEditorPageViewAspectsInstaller.prototype = /** @type {!_FileEditorPageViewAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller}
 */
class _FileEditorPageViewAspectsInstaller { }

_FileEditorPageViewAspectsInstaller.prototype[$advice]=__FileEditorPageViewAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller} ‎ */
class FileEditorPageViewAspectsInstaller extends newAbstract(
 _FileEditorPageViewAspectsInstaller,399991590215,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller} */
FileEditorPageViewAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageViewAspectsInstaller} */
function FileEditorPageViewAspectsInstallerClass(){}

export default FileEditorPageViewAspectsInstaller


FileEditorPageViewAspectsInstaller[$implementations]=[
 FileEditorPageViewAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IFileEditorPageViewAspectsInstaller}*/({
  viewFileEditor(){
   this.beforeViewFileEditor=1
   this.afterViewFileEditor=2
   this.aroundViewFileEditor=3
   this.afterViewFileEditorThrows=4
   this.afterViewFileEditorReturns=5
   this.afterViewFileEditorCancels=7
   this.beforeEachViewFileEditor=8
   this.afterEachViewFileEditor=9
   this.afterEachViewFileEditorReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  getFileEditor(){
   this.beforeGetFileEditor=1
   this.afterGetFileEditor=2
   this.aroundGetFileEditor=3
   this.afterGetFileEditorThrows=4
   this.afterGetFileEditorReturns=5
   this.afterGetFileEditorCancels=7
   this.beforeEachGetFileEditor=8
   this.afterEachGetFileEditor=9
   this.afterEachGetFileEditorReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  setFileEditorCtx(){
   this.beforeSetFileEditorCtx=1
   this.afterSetFileEditorCtx=2
   this.aroundSetFileEditorCtx=3
   this.afterSetFileEditorCtxThrows=4
   this.afterSetFileEditorCtxReturns=5
   this.afterSetFileEditorCtxCancels=7
   this.beforeEachSetFileEditorCtx=8
   this.afterEachSetFileEditorCtx=9
   this.afterEachSetFileEditorCtxReturns=10
   return {
    answers:1,
    translation:2,
   }
  },
  FileEditorPartial(){
   this.before_FileEditorPartial=1
   this.after_FileEditorPartial=2
   this.around_FileEditorPartial=3
   this.after_FileEditorPartialThrows=4
   this.after_FileEditorPartialReturns=5
   this.after_FileEditorPartialCancels=7
   return {
    answers:1,
    errors:2,
    actions:3,
    translation:4,
   }
  },
  FileEditorView(){
   this.before_FileEditorView=1
   this.after_FileEditorView=2
   this.around_FileEditorView=3
   this.after_FileEditorViewThrows=4
   this.after_FileEditorViewReturns=5
   this.after_FileEditorViewCancels=7
   this.beforeEach_FileEditorView=8
   this.afterEach_FileEditorView=9
   this.afterEach_FileEditorViewReturns=10
  },
 }),
 __FileEditorPageViewAspectsInstaller,
]