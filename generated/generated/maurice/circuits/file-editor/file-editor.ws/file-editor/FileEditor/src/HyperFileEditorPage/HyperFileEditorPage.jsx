import '../../types'
import filterReadFile from '../../../../../../../../../maurice/circuits/file-editor/FileEditor.mvc/src/FileEditorService/methods/filterReadFile/filter-read-file'
import {FileEditorCachePQs} from '../../../../../../../../../maurice/circuits/file-editor/FileEditor.mvc/pqs/FileEditorCachePQs'
import {FileEditorMemoryPQs} from '../../../../../../../../../maurice/circuits/file-editor/FileEditor.mvc/pqs/FileEditorMemoryPQs'
import AbstractHyperFileEditorPage from '../../gen/hyper/AbstractHyperFileEditorPage'
import HyperFileEditorPageView from '../HyperFileEditorPageView'
import {AssignAnswers} from '@websystems/parthenon'
import AbstractFileEditorPage from '../../gen/AbstractFileEditorPage'
import AbstractFileEditorPageAspects from '../../gen/aspects/AbstractFileEditorPageAspects'
import FileEditorPageMethodsIds from '../../gen/methods-ids'

/** @extends {xyz.swapee.rc.HyperFileEditorPage}  */
export default class HyperFileEditorPage extends AbstractHyperFileEditorPage.consults(
 HyperFileEditorPageView,
 AbstractFileEditorPageAspects.class.prototype=/**@type {!xyz.swapee.rc.FileEditorPageAspects} */({
  afterEachFilterReadFileReturns:[
   AssignAnswers,
  ],
  afterEachFilterSaveFileReturns:[
   AssignAnswers,
  ],
  afterEachFileEditorReturns:[
   AssignAnswers,
  ],
 }),
 // todo: add HyperView here
 AbstractFileEditorPageAspects.class.prototype=/**@type {!xyz.swapee.rc.FileEditorPageAspects} */({ 
  afterFilterReadFile:[
   function({args:{ctx:ctx,answers:answers}}){
    const body={}
    for(const key in answers){
     const q=PQs[key]
     body[q]=answers[key]
    }
    ctx.body={data:body}
   },
  ],
 }),
).implements(
 AbstractFileEditorPage,
 AbstractHyperFileEditorPage.class.prototype=/**@type {!xyz.swapee.rc.FileEditorPageHyperslice} */({
  get _methodIds(){return FileEditorPageMethodsIds},
 }),
 AbstractHyperFileEditorPage.class.prototype=/**@type {!xyz.swapee.rc.FileEditorPageHyperslice} */({
  filterReadFile:[
   filterReadFile,
  ],
 }),
){}

const PQs={...FileEditorCachePQs,...FileEditorMemoryPQs}