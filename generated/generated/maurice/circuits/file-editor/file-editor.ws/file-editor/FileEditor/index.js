require('./types')
module.exports={
 toString(){return 'fileEditor'},
 get FileEditorPage(){
  const d=Date.now();
  const R=require('./src/HyperFileEditorPage');
  console.log("✅ Loaded dev %s in %s",'FileEditor',Date.now()-d,'ms');
  return R
 },
 get fileEditorMethodsIds() {
  const methodsIds={}
  const o=require('./gen/methods-ids')
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') methodsIds[key]=val
  }
  return methodsIds
 },
 get fileEditorArcsIds() {
  const arcsIds={}
  let o={}
  try{o=require('./gen/arcs-ids')}catch(err){}
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') arcsIds[val]=key
  }
  return arcsIds
 },
 get getFileEditor(){
  const GET=require('./src/api/getFileEditor/getFileEditor')
  return GET
 },
 get FileEditorNav() {
  return require('./src/navs/fileEditor')
 },
 get FilterReadFileNav() {
  return require('./src/navs/filterReadFile')
 },
 get FilterSaveFileNav() {
  return require('./src/navs/filterSaveFile')
 },
}