import FileEditorPageViewAspectsInstaller from '../../aspects-installers/FileEditorPageViewAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperFileEditorPageView}
 */
function __AbstractHyperFileEditorPageView() {}
__AbstractHyperFileEditorPageView.prototype = /** @type {!_AbstractHyperFileEditorPageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperFileEditorPageView}
 */
class _AbstractHyperFileEditorPageView {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperFileEditorPageView
    .clone({aspectsInstaller:FileEditorPageViewAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.rc.AbstractHyperFileEditorPageView} ‎ */
class AbstractHyperFileEditorPageView extends newAbstract(
 _AbstractHyperFileEditorPageView,399991590219,null,{
  asIHyperFileEditorPageView:1,
  superHyperFileEditorPageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperFileEditorPageView} */
AbstractHyperFileEditorPageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperFileEditorPageView} */
function AbstractHyperFileEditorPageViewClass(){}

export default AbstractHyperFileEditorPageView


AbstractHyperFileEditorPageView[$implementations]=[
 __AbstractHyperFileEditorPageView,
]