import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorPageAspects}
 */
function __AbstractFileEditorPageAspects() {}
__AbstractFileEditorPageAspects.prototype = /** @type {!_AbstractFileEditorPageAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileEditorPageAspects}
 */
class _AbstractFileEditorPageAspects { }
/**
 * The aspects of the *IFileEditorPage*.
 * @extends {xyz.swapee.rc.AbstractFileEditorPageAspects} ‎
 */
class AbstractFileEditorPageAspects extends newAspects(
 _AbstractFileEditorPageAspects,39999159027,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageAspects} */
AbstractFileEditorPageAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileEditorPageAspects} */
function AbstractFileEditorPageAspectsClass(){}

export default AbstractFileEditorPageAspects


AbstractFileEditorPageAspects[$implementations]=[
 __AbstractFileEditorPageAspects,
]