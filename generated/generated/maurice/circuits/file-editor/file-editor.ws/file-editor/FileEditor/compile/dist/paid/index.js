require('@type.engineering/type-engineer/types/typedefs')
//require('./types/typology')
require('./types/typedefs')

/**
@license
@LICENSE @xyz.swapee.rc/file-editor (c) by X 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @extends {xyz.swapee.rc.FileEditorPage} */
class FileEditorPage extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the methods.
 * @extends {xyz.swapee.rc.fileEditorMethodsIds}
 */
class fileEditorMethodsIds extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the arcs.
 * @extends {xyz.swapee.rc.fileEditorArcsIds}
 */
class fileEditorArcsIds extends (class {/* lazy-loaded */}) {}
/**
 * Allows to render the pagelet after it's been processed.
 */
function getFileEditor() {
  // lazy-loaded()
}
/** @extends {xyz.swapee.rc.FileEditorPageView} */
class FileEditorPageView extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.rc.HyperFileEditorPageView} */
class HyperFileEditorPageView extends (class {/* lazy-loaded */}) {}
/**
 * The `fileEditor` navigation metadata.
 * @extends {xyz.swapee.rc.FileEditor.FileEditorNav}
 */
class FileEditorNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterReadFile` navigation metadata.
 * @extends {xyz.swapee.rc.FileEditor.FilterReadFileNav}
 */
class FilterReadFileNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterSaveFile` navigation metadata.
 * @extends {xyz.swapee.rc.FileEditor.FilterSaveFileNav}
 */
class FilterSaveFileNav extends (class {/* lazy-loaded */}) {}

module.exports.FileEditorPage = FileEditorPage
module.exports.fileEditorMethodsIds = fileEditorMethodsIds
module.exports.fileEditorArcsIds = fileEditorArcsIds
module.exports.getFileEditor = getFileEditor
module.exports.FileEditorPageView = FileEditorPageView
module.exports.HyperFileEditorPageView = HyperFileEditorPageView
module.exports.FileEditorNav = FileEditorNav
module.exports.FilterReadFileNav = FilterReadFileNav
module.exports.FilterSaveFileNav = FilterSaveFileNav

Object.defineProperties(module.exports, {
 'FileEditorPage': {get: () => require('./compile')[39999159021]},
 [39999159021]: {get: () => module.exports['FileEditorPage']},
 'fileEditorMethodsIds': {get: () => require('./compile')[39999159022]},
 [39999159022]: {get: () => module.exports['fileEditorMethodsIds']},
 'fileEditorArcsIds': {get: () => require('./compile')[39999159023]},
 [39999159023]: {get: () => module.exports['fileEditorArcsIds']},
 'getFileEditor': {get: () => require('./compile')[39999159024]},
 [39999159024]: {get: () => module.exports['getFileEditor']},
 'FileEditorPageView': {get: () => require('./compile')[399991590210]},
 [399991590210]: {get: () => module.exports['FileEditorPageView']},
 'HyperFileEditorPageView': {get: () => require('./compile')[399991590213]},
 [399991590213]: {get: () => module.exports['HyperFileEditorPageView']},
 'FileEditorNav': {get: () => require('./compile')[3999915902100]},
 [3999915902100]: {get: () => module.exports['FileEditorNav']},
 'FilterReadFileNav': {get: () => require('./compile')[3999915902101]},
 [3999915902101]: {get: () => module.exports['FilterReadFileNav']},
 'FilterSaveFileNav': {get: () => require('./compile')[3999915902102]},
 [3999915902102]: {get: () => module.exports['FilterSaveFileNav']},
})