import FileEditorPageMethodsIds from '../../../gen/methods-ids'
import '../../../types'

const methodsIds=Object.values(FileEditorPageMethodsIds)

/**@type {xyz.swapee.rc.IFileEditorPageView._getFileEditor} */
export default function _getFileEditor(ctx,form) {
 const actionId=parseFloat(form['action'])
 if(methodsIds.includes(actionId)) {
  const{asFileEditorPageView:{viewFileEditor:viewFileEditor}}=this
  const ev=viewFileEditor(ctx,form)
  return ev
 }
}