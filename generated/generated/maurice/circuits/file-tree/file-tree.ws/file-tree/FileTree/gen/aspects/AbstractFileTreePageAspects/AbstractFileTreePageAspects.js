import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreePageAspects}
 */
function __AbstractFileTreePageAspects() {}
__AbstractFileTreePageAspects.prototype = /** @type {!_AbstractFileTreePageAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePageAspects}
 */
class _AbstractFileTreePageAspects { }
/**
 * The aspects of the *IFileTreePage*.
 * @extends {xyz.swapee.rc.AbstractFileTreePageAspects} ‎
 */
class AbstractFileTreePageAspects extends newAspects(
 _AbstractFileTreePageAspects,'IFileTreePageAspects',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileTreePageAspects} */
AbstractFileTreePageAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileTreePageAspects} */
function AbstractFileTreePageAspectsClass(){}

export default AbstractFileTreePageAspects


AbstractFileTreePageAspects[$implementations]=[
 __AbstractFileTreePageAspects,
]