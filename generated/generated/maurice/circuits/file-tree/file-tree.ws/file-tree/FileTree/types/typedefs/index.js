/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
xyz.swapee.rc.IFileTreePage={}
xyz.swapee.rc.IFileTreePage.fileTree={}
xyz.swapee.rc.IFileTreePage.filterFiles={}
xyz.swapee.rc.IFileTreePageAspectsInstaller={}
xyz.swapee.rc.IFileTreePageJoinpointModel={}
xyz.swapee.rc.UFileTreePage={}
xyz.swapee.rc.AbstractFileTreePageUniversal={}
xyz.swapee.rc.IFileTreePageAspects={}
xyz.swapee.rc.IHyperFileTreePage={}
xyz.swapee.rc.FileTree={}
xyz.swapee.rc.FileTree.Answers={}
xyz.swapee.rc.FileTree.Form={}
xyz.swapee.rc.FileTree.Errors={}
xyz.swapee.rc.FileTree.Validation={}
xyz.swapee.rc.FileTree.Ctx={}
xyz.swapee.rc.IFileTreePageView={}
xyz.swapee.rc.IFileTreePageView.FileTreePartial={}
xyz.swapee.rc.IFileTreePageViewJoinpointModel={}
xyz.swapee.rc.IFileTreePageViewAspectsInstaller={}
xyz.swapee.rc.UFileTreePageView={}
xyz.swapee.rc.AbstractFileTreePageViewUniversal={}
xyz.swapee.rc.IFileTreePageViewAspects={}
xyz.swapee.rc.IHyperFileTreePageView={}
xyz.swapee.rc.IFileTreeImpl={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-tree/file-tree.ws/file-tree/FileTree/design/IFileTreePage.xml}  e3463f44e00656dfa5ee04e44f47488d */
/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreePage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileTreePage)} xyz.swapee.rc.AbstractFileTreePage.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePage} xyz.swapee.rc.FileTreePage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileTreePage` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePage
 */
xyz.swapee.rc.AbstractFileTreePage = class extends /** @type {xyz.swapee.rc.AbstractFileTreePage.constructor&xyz.swapee.rc.FileTreePage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePage.prototype.constructor = xyz.swapee.rc.AbstractFileTreePage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePage.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.UFileTreePage|typeof xyz.swapee.rc.UFileTreePage)|!xyz.swapee.rc.IFileTreePageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePage}
 */
xyz.swapee.rc.AbstractFileTreePage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePage}
 */
xyz.swapee.rc.AbstractFileTreePage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.UFileTreePage|typeof xyz.swapee.rc.UFileTreePage)|!xyz.swapee.rc.IFileTreePageHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePage}
 */
xyz.swapee.rc.AbstractFileTreePage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.UFileTreePage|typeof xyz.swapee.rc.UFileTreePage)|!xyz.swapee.rc.IFileTreePageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePage}
 */
xyz.swapee.rc.AbstractFileTreePage.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageJoinpointModelHyperslice
 */
xyz.swapee.rc.IFileTreePageJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeFileTree>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTree>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFileTreeThrows=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFileTreeCancels=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeEachFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeEachFileTree>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFileTree>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFileTreeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFileTreeReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeFilterFiles|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeFilterFiles>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFiles|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFiles>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterFilesThrows=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterFilesReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterFilesCancels=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeEachFilterFiles|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._beforeEachFilterFiles>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFilterFiles|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFilterFiles>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterFilesReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFilterFilesReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFilterFilesReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageJoinpointModelHyperslice

/**
 * A concrete class of _IFileTreePageJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileTreePageJoinpointModelHyperslice = class extends xyz.swapee.rc.IFileTreePageJoinpointModelHyperslice { }
xyz.swapee.rc.FileTreePageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFileTree<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTree<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFileTreeThrows=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFileTreeCancels=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFileTree<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFileTree=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTree<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTreeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTreeReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFilterFiles<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFilterFiles<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFiles<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFiles<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterFilesThrows=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterFilesReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterFilesCancels=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFilterFiles<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFilterFiles<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterFiles=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFiles<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFiles<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterFilesReturns=/** @type {!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFilesReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFilesReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice

/**
 * A concrete class of _IFileTreePageJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileTreePageJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice { }
xyz.swapee.rc.FileTreePageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IFileTreePage`'s methods.
 * @interface xyz.swapee.rc.IFileTreePageJoinpointModel
 */
xyz.swapee.rc.IFileTreePageJoinpointModel = class { }
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.beforeFileTree} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.beforeFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTree} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeThrows} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFileTreeThrows = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeReturns} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFileTreeReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeCancels} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFileTreeCancels = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.beforeEachFileTree} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.beforeEachFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFileTree} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterEachFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFileTreeReturns} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterEachFileTreeReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.beforeFilterFiles} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.beforeFilterFiles = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFiles} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFilterFiles = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesThrows} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFilterFilesThrows = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesReturns} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFilterFilesReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesCancels} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterFilterFilesCancels = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.beforeEachFilterFiles} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.beforeEachFilterFiles = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFilterFiles} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterEachFilterFiles = function() {}
/** @type {xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFilterFilesReturns} */
xyz.swapee.rc.IFileTreePageJoinpointModel.prototype.afterEachFilterFilesReturns = function() {}

/**
 * A concrete class of _IFileTreePageJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.FileTreePageJoinpointModel
 * @implements {xyz.swapee.rc.IFileTreePageJoinpointModel} An interface that enumerates the joinpoints of `IFileTreePage`'s methods.
 */
xyz.swapee.rc.FileTreePageJoinpointModel = class extends xyz.swapee.rc.IFileTreePageJoinpointModel { }
xyz.swapee.rc.FileTreePageJoinpointModel.prototype.constructor = xyz.swapee.rc.FileTreePageJoinpointModel

/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel} */
xyz.swapee.rc.RecordIFileTreePageJoinpointModel

/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel} xyz.swapee.rc.BoundIFileTreePageJoinpointModel */

/** @typedef {xyz.swapee.rc.FileTreePageJoinpointModel} xyz.swapee.rc.BoundFileTreePageJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreePageAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileTreePageAspectsInstaller)} xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePageAspectsInstaller} xyz.swapee.rc.FileTreePageAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileTreePageAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePageAspectsInstaller
 */
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.constructor&xyz.swapee.rc.FileTreePageAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractFileTreePageAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePageAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.IFileTreePageAspectsInstaller|typeof xyz.swapee.rc.FileTreePageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.IFileTreePageAspectsInstaller|typeof xyz.swapee.rc.FileTreePageAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.IFileTreePageAspectsInstaller|typeof xyz.swapee.rc.FileTreePageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileTreePageAspectsInstaller.Initialese[]) => xyz.swapee.rc.IFileTreePageAspectsInstaller} xyz.swapee.rc.FileTreePageAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.rc.IFileTreePageAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.rc.IFileTreePageAspectsInstaller */
xyz.swapee.rc.IFileTreePageAspectsInstaller = class extends /** @type {xyz.swapee.rc.IFileTreePageAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeFileTree=/** @type {number} */ (void 0)
    this.afterFileTree=/** @type {number} */ (void 0)
    this.afterFileTreeThrows=/** @type {number} */ (void 0)
    this.afterFileTreeReturns=/** @type {number} */ (void 0)
    this.afterFileTreeCancels=/** @type {number} */ (void 0)
    this.beforeEachFileTree=/** @type {number} */ (void 0)
    this.afterEachFileTree=/** @type {number} */ (void 0)
    this.afterEachFileTreeReturns=/** @type {number} */ (void 0)
    this.beforeFilterFiles=/** @type {number} */ (void 0)
    this.afterFilterFiles=/** @type {number} */ (void 0)
    this.afterFilterFilesThrows=/** @type {number} */ (void 0)
    this.afterFilterFilesReturns=/** @type {number} */ (void 0)
    this.afterFilterFilesCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterFiles=/** @type {number} */ (void 0)
    this.afterEachFilterFiles=/** @type {number} */ (void 0)
    this.afterEachFilterFilesReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  fileTree() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterFiles() { }
}
/**
 * Create a new *IFileTreePageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileTreePageAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileTreePageAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageAspectsInstaller.Initialese>)} xyz.swapee.rc.FileTreePageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageAspectsInstaller} xyz.swapee.rc.IFileTreePageAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileTreePageAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.FileTreePageAspectsInstaller
 * @implements {xyz.swapee.rc.IFileTreePageAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePageAspectsInstaller = class extends /** @type {xyz.swapee.rc.FileTreePageAspectsInstaller.constructor&xyz.swapee.rc.IFileTreePageAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreePageAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileTreePageAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreePageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePageAspectsInstaller}
 */
xyz.swapee.rc.FileTreePageAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePage.FileTreeNArgs
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Form} form The form.
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Validation} validation The validation.
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Ctx} ctx The ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageJoinpointModel.FileTreePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileTreePage.FileTreeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFileTreePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileTreePage.FileTreeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `fileTree` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers>) => void} sub Cancels a call to `fileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFileTreePointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FileTreePointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFileTreePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FileTreePointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFileTreePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `fileTree` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFileTreePointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FileTreePointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFileTreePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFileTreePointcutData
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFileTreePointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FileTreePointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFileTreePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFileTreePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFileTreePointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FileTreePointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFileTreePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePage.FilterFilesNArgs
 * @prop {!xyz.swapee.rc.IFileTreePage.filterFiles.Form} form The action form.
 * @prop {!xyz.swapee.rc.IFileTreePage.filterFiles.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IFileTreePage.filterFiles.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IFileTreePage.filterFiles.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IFileTreePage.filterFiles.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageJoinpointModel.FilterFilesPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileTreePage.FilterFilesNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFilterFilesPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileTreePage.FilterFilesNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterFiles` method from being executed.
 * @prop {(value: xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers>) => void} sub Cancels a call to `filterFiles` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFilterFilesPointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FilterFilesPointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFilterFilesPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData
 * @prop {xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FilterFilesPointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFilterFilesPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterFiles` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFilterFilesPointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FilterFilesPointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFilterFilesPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFilterFilesPointcutData
 * @prop {xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFilterFilesPointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FilterFilesPointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFilterFilesPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFilterFilesPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFilterFilesPointcutData&xyz.swapee.rc.IFileTreePageJoinpointModel.FilterFilesPointcutData} xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFilterFilesPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IFileTreePage.Initialese[]) => xyz.swapee.rc.IFileTreePage} xyz.swapee.rc.FileTreePageConstructor */

/** @typedef {symbol} xyz.swapee.rc.FileTreePageMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UFileTreePage.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IFileTreePage} [fileTreePage]
 */

/** @typedef {function(new: xyz.swapee.rc.FileTreePageUniversal)} xyz.swapee.rc.AbstractFileTreePageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePageUniversal} xyz.swapee.rc.FileTreePageUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UFileTreePage` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePageUniversal
 */
xyz.swapee.rc.AbstractFileTreePageUniversal = class extends /** @type {xyz.swapee.rc.AbstractFileTreePageUniversal.constructor&xyz.swapee.rc.FileTreePageUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePageUniversal.prototype.constructor = xyz.swapee.rc.AbstractFileTreePageUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePageUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePageUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UFileTreePage|typeof xyz.swapee.rc.UFileTreePage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePageUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePageUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UFileTreePage|typeof xyz.swapee.rc.UFileTreePage} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UFileTreePage|typeof xyz.swapee.rc.UFileTreePage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.FileTreePageMetaUniversal} xyz.swapee.rc.AbstractFileTreePageUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UFileTreePage.Initialese[]) => xyz.swapee.rc.UFileTreePage} xyz.swapee.rc.UFileTreePageConstructor */

/** @typedef {function(new: xyz.swapee.rc.UFileTreePageFields&engineering.type.IEngineer&xyz.swapee.rc.UFileTreePageCaster)} xyz.swapee.rc.UFileTreePage.constructor */
/**
 * A trait that allows to access an _IFileTreePage_ object via a `.fileTreePage` field.
 * @interface xyz.swapee.rc.UFileTreePage
 */
xyz.swapee.rc.UFileTreePage = class extends /** @type {xyz.swapee.rc.UFileTreePage.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UFileTreePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileTreePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UFileTreePage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UFileTreePage&engineering.type.IInitialiser<!xyz.swapee.rc.UFileTreePage.Initialese>)} xyz.swapee.rc.FileTreePageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UFileTreePage} xyz.swapee.rc.UFileTreePage.typeof */
/**
 * A concrete class of _UFileTreePage_ instances.
 * @constructor xyz.swapee.rc.FileTreePageUniversal
 * @implements {xyz.swapee.rc.UFileTreePage} A trait that allows to access an _IFileTreePage_ object via a `.fileTreePage` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UFileTreePage.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePageUniversal = class extends /** @type {xyz.swapee.rc.FileTreePageUniversal.constructor&xyz.swapee.rc.UFileTreePage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UFileTreePage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UFileTreePage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UFileTreePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileTreePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePageUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePageUniversal}
 */
xyz.swapee.rc.FileTreePageUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UFileTreePage.
 * @interface xyz.swapee.rc.UFileTreePageFields
 */
xyz.swapee.rc.UFileTreePageFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UFileTreePageFields.prototype.fileTreePage = /** @type {xyz.swapee.rc.IFileTreePage} */ (void 0)

/** @typedef {xyz.swapee.rc.UFileTreePage} */
xyz.swapee.rc.RecordUFileTreePage

/** @typedef {xyz.swapee.rc.UFileTreePage} xyz.swapee.rc.BoundUFileTreePage */

/** @typedef {xyz.swapee.rc.FileTreePageUniversal} xyz.swapee.rc.BoundFileTreePageUniversal */

/**
 * Contains getters to cast the _UFileTreePage_ interface.
 * @interface xyz.swapee.rc.UFileTreePageCaster
 */
xyz.swapee.rc.UFileTreePageCaster = class { }
/**
 * Provides direct access to _UFileTreePage_ via the _BoundUFileTreePage_ universal.
 * @type {!xyz.swapee.rc.BoundFileTreePage}
 */
xyz.swapee.rc.UFileTreePageCaster.prototype.asFileTreePage
/**
 * Cast the _UFileTreePage_ instance into the _BoundUFileTreePage_ type.
 * @type {!xyz.swapee.rc.BoundUFileTreePage}
 */
xyz.swapee.rc.UFileTreePageCaster.prototype.asUFileTreePage
/**
 * Access the _FileTreePageUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundFileTreePageUniversal}
 */
xyz.swapee.rc.UFileTreePageCaster.prototype.superFileTreePageUniversal

/** @typedef {function(new: xyz.swapee.rc.BFileTreePageAspectsCaster<THIS>&xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BFileTreePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice} xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IFileTreePage* that bind to an instance.
 * @interface xyz.swapee.rc.BFileTreePageAspects
 * @template THIS
 */
xyz.swapee.rc.BFileTreePageAspects = class extends /** @type {xyz.swapee.rc.BFileTreePageAspects.constructor&xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BFileTreePageAspects.prototype.constructor = xyz.swapee.rc.BFileTreePageAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreePageAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileTreePageAspects)} xyz.swapee.rc.AbstractFileTreePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePageAspects} xyz.swapee.rc.FileTreePageAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileTreePageAspects` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePageAspects
 */
xyz.swapee.rc.AbstractFileTreePageAspects = class extends /** @type {xyz.swapee.rc.AbstractFileTreePageAspects.constructor&xyz.swapee.rc.FileTreePageAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePageAspects.prototype.constructor = xyz.swapee.rc.AbstractFileTreePageAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePageAspects.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePageAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileTreePageAspects|typeof xyz.swapee.rc.FileTreePageAspects)|(!xyz.swapee.rc.BFileTreePageAspects|typeof xyz.swapee.rc.BFileTreePageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePageAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePageAspects}
 */
xyz.swapee.rc.AbstractFileTreePageAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspects}
 */
xyz.swapee.rc.AbstractFileTreePageAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileTreePageAspects|typeof xyz.swapee.rc.FileTreePageAspects)|(!xyz.swapee.rc.BFileTreePageAspects|typeof xyz.swapee.rc.BFileTreePageAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspects}
 */
xyz.swapee.rc.AbstractFileTreePageAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileTreePageAspects|typeof xyz.swapee.rc.FileTreePageAspects)|(!xyz.swapee.rc.BFileTreePageAspects|typeof xyz.swapee.rc.BFileTreePageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageAspects}
 */
xyz.swapee.rc.AbstractFileTreePageAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileTreePageAspects.Initialese[]) => xyz.swapee.rc.IFileTreePageAspects} xyz.swapee.rc.FileTreePageAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileTreePageAspectsCaster&xyz.swapee.rc.BFileTreePageAspects<!xyz.swapee.rc.IFileTreePageAspects>)} xyz.swapee.rc.IFileTreePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BFileTreePageAspects} xyz.swapee.rc.BFileTreePageAspects.typeof */
/**
 * The aspects of the *IFileTreePage*.
 * @interface xyz.swapee.rc.IFileTreePageAspects
 */
xyz.swapee.rc.IFileTreePageAspects = class extends /** @type {xyz.swapee.rc.IFileTreePageAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BFileTreePageAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreePageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileTreePageAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileTreePageAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageAspects.Initialese>)} xyz.swapee.rc.FileTreePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageAspects} xyz.swapee.rc.IFileTreePageAspects.typeof */
/**
 * A concrete class of _IFileTreePageAspects_ instances.
 * @constructor xyz.swapee.rc.FileTreePageAspects
 * @implements {xyz.swapee.rc.IFileTreePageAspects} The aspects of the *IFileTreePage*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageAspects.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePageAspects = class extends /** @type {xyz.swapee.rc.FileTreePageAspects.constructor&xyz.swapee.rc.IFileTreePageAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreePageAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileTreePageAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreePageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePageAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePageAspects}
 */
xyz.swapee.rc.FileTreePageAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BFileTreePageAspects_ interface.
 * @interface xyz.swapee.rc.BFileTreePageAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BFileTreePageAspectsCaster = class { }
/**
 * Cast the _BFileTreePageAspects_ instance into the _BoundIFileTreePage_ type.
 * @type {!xyz.swapee.rc.BoundIFileTreePage}
 */
xyz.swapee.rc.BFileTreePageAspectsCaster.prototype.asIFileTreePage

/**
 * Contains getters to cast the _IFileTreePageAspects_ interface.
 * @interface xyz.swapee.rc.IFileTreePageAspectsCaster
 */
xyz.swapee.rc.IFileTreePageAspectsCaster = class { }
/**
 * Cast the _IFileTreePageAspects_ instance into the _BoundIFileTreePage_ type.
 * @type {!xyz.swapee.rc.BoundIFileTreePage}
 */
xyz.swapee.rc.IFileTreePageAspectsCaster.prototype.asIFileTreePage

/** @typedef {xyz.swapee.rc.IFileTreePage.Initialese} xyz.swapee.rc.IHyperFileTreePage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.HyperFileTreePage)} xyz.swapee.rc.AbstractHyperFileTreePage.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperFileTreePage} xyz.swapee.rc.HyperFileTreePage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperFileTreePage` interface.
 * @constructor xyz.swapee.rc.AbstractHyperFileTreePage
 */
xyz.swapee.rc.AbstractHyperFileTreePage = class extends /** @type {xyz.swapee.rc.AbstractHyperFileTreePage.constructor&xyz.swapee.rc.HyperFileTreePage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperFileTreePage.prototype.constructor = xyz.swapee.rc.AbstractHyperFileTreePage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperFileTreePage.class = /** @type {typeof xyz.swapee.rc.AbstractHyperFileTreePage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperFileTreePage|typeof xyz.swapee.rc.HyperFileTreePage)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.IFileTreePageHyperslice|typeof xyz.swapee.rc.FileTreePageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileTreePage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileTreePage}
 */
xyz.swapee.rc.AbstractHyperFileTreePage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePage}
 */
xyz.swapee.rc.AbstractHyperFileTreePage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperFileTreePage|typeof xyz.swapee.rc.HyperFileTreePage)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.IFileTreePageHyperslice|typeof xyz.swapee.rc.FileTreePageHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePage}
 */
xyz.swapee.rc.AbstractHyperFileTreePage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperFileTreePage|typeof xyz.swapee.rc.HyperFileTreePage)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.IFileTreePageHyperslice|typeof xyz.swapee.rc.FileTreePageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePage}
 */
xyz.swapee.rc.AbstractHyperFileTreePage.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IFileTreePageAspects|function(new: xyz.swapee.rc.IFileTreePageAspects)} aides The list of aides that advise the IFileTreePage to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileTreePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileTreePage.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperFileTreePage.Initialese[]) => xyz.swapee.rc.IHyperFileTreePage} xyz.swapee.rc.HyperFileTreePageConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperFileTreePageCaster&xyz.swapee.rc.IFileTreePage)} xyz.swapee.rc.IHyperFileTreePage.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePage} xyz.swapee.rc.IFileTreePage.typeof */
/** @interface xyz.swapee.rc.IHyperFileTreePage */
xyz.swapee.rc.IHyperFileTreePage = class extends /** @type {xyz.swapee.rc.IHyperFileTreePage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileTreePage.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperFileTreePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileTreePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperFileTreePage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperFileTreePage&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileTreePage.Initialese>)} xyz.swapee.rc.HyperFileTreePage.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperFileTreePage} xyz.swapee.rc.IHyperFileTreePage.typeof */
/**
 * A concrete class of _IHyperFileTreePage_ instances.
 * @constructor xyz.swapee.rc.HyperFileTreePage
 * @implements {xyz.swapee.rc.IHyperFileTreePage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileTreePage.Initialese>} ‎
 */
xyz.swapee.rc.HyperFileTreePage = class extends /** @type {xyz.swapee.rc.HyperFileTreePage.constructor&xyz.swapee.rc.IHyperFileTreePage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperFileTreePage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperFileTreePage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperFileTreePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileTreePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperFileTreePage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperFileTreePage}
 */
xyz.swapee.rc.HyperFileTreePage.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperFileTreePage} */
xyz.swapee.rc.RecordIHyperFileTreePage

/** @typedef {xyz.swapee.rc.IHyperFileTreePage} xyz.swapee.rc.BoundIHyperFileTreePage */

/** @typedef {xyz.swapee.rc.HyperFileTreePage} xyz.swapee.rc.BoundHyperFileTreePage */

/**
 * Contains getters to cast the _IHyperFileTreePage_ interface.
 * @interface xyz.swapee.rc.IHyperFileTreePageCaster
 */
xyz.swapee.rc.IHyperFileTreePageCaster = class { }
/**
 * Cast the _IHyperFileTreePage_ instance into the _BoundIHyperFileTreePage_ type.
 * @type {!xyz.swapee.rc.BoundIHyperFileTreePage}
 */
xyz.swapee.rc.IHyperFileTreePageCaster.prototype.asIHyperFileTreePage
/**
 * Access the _HyperFileTreePage_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperFileTreePage}
 */
xyz.swapee.rc.IHyperFileTreePageCaster.prototype.superHyperFileTreePage

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageHyperslice
 */
xyz.swapee.rc.IFileTreePageHyperslice = class {
  constructor() {
    this.fileTree=/** @type {!xyz.swapee.rc.IFileTreePage._fileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePage._fileTree>} */ (void 0)
    /**
     * Returns files and folders in the directory.
     */
    this.filterFiles=/** @type {!xyz.swapee.rc.IFileTreePage._filterFiles|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePage._filterFiles>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageHyperslice

/**
 * A concrete class of _IFileTreePageHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileTreePageHyperslice = class extends xyz.swapee.rc.IFileTreePageHyperslice { }
xyz.swapee.rc.FileTreePageHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileTreePageBindingHyperslice = class {
  constructor() {
    this.fileTree=/** @type {!xyz.swapee.rc.IFileTreePage.__fileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePage.__fileTree<THIS>>} */ (void 0)
    /**
     * Returns files and folders in the directory.
     */
    this.filterFiles=/** @type {!xyz.swapee.rc.IFileTreePage.__filterFiles<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePage.__filterFiles<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageBindingHyperslice

/**
 * A concrete class of _IFileTreePageBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageBindingHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileTreePageBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileTreePageBindingHyperslice = class extends xyz.swapee.rc.IFileTreePageBindingHyperslice { }
xyz.swapee.rc.FileTreePageBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IFileTreePageFields&engineering.type.IEngineer&xyz.swapee.rc.IFileTreePageCaster&xyz.swapee.rc.UFileTreePage)} xyz.swapee.rc.IFileTreePage.constructor */
/** @interface xyz.swapee.rc.IFileTreePage */
xyz.swapee.rc.IFileTreePage = class extends /** @type {xyz.swapee.rc.IFileTreePage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.UFileTreePage.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileTreePage.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IFileTreePage.fileTree} */
xyz.swapee.rc.IFileTreePage.prototype.fileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePage.filterFiles} */
xyz.swapee.rc.IFileTreePage.prototype.filterFiles = function() {}

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage&engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePage.Initialese>)} xyz.swapee.rc.FileTreePage.constructor */
/**
 * A concrete class of _IFileTreePage_ instances.
 * @constructor xyz.swapee.rc.FileTreePage
 * @implements {xyz.swapee.rc.IFileTreePage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePage.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePage = class extends /** @type {xyz.swapee.rc.FileTreePage.constructor&xyz.swapee.rc.IFileTreePage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreePage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileTreePage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePage}
 */
xyz.swapee.rc.FileTreePage.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreePage.
 * @interface xyz.swapee.rc.IFileTreePageFields
 */
xyz.swapee.rc.IFileTreePageFields = class { }
/**
 *
 */
xyz.swapee.rc.IFileTreePageFields.prototype.fileTreeCtx = /** @type {!xyz.swapee.rc.IFileTreePage.fileTree.Ctx} */ (void 0)

/** @typedef {xyz.swapee.rc.IFileTreePage} */
xyz.swapee.rc.RecordIFileTreePage

/** @typedef {xyz.swapee.rc.IFileTreePage} xyz.swapee.rc.BoundIFileTreePage */

/** @typedef {xyz.swapee.rc.FileTreePage} xyz.swapee.rc.BoundFileTreePage */

/**
 * Contains getters to cast the _IFileTreePage_ interface.
 * @interface xyz.swapee.rc.IFileTreePageCaster
 */
xyz.swapee.rc.IFileTreePageCaster = class { }
/**
 * Cast the _IFileTreePage_ instance into the _BoundIFileTreePage_ type.
 * @type {!xyz.swapee.rc.BoundIFileTreePage}
 */
xyz.swapee.rc.IFileTreePageCaster.prototype.asIFileTreePage
/**
 * Access the _FileTreePage_ prototype.
 * @type {!xyz.swapee.rc.BoundFileTreePage}
 */
xyz.swapee.rc.IFileTreePageCaster.prototype.superFileTreePage

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFileTree<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._beforeFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.beforeFileTree} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePage.FileTreeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `fileTree` method from being executed.
 * - `sub` _(value: !IFileTreePage.fileTree.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.fileTree.OptAnswers&gt;) =&gt; void_ Cancels a call to `fileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.beforeFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTree<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTree} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `after` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.fileTree.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeThrows<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeThrows */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `fileTree` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeReturns<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `afterReturns` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.fileTree.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileTreePage.fileTree.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.fileTree.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFileTreeCancels<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFileTreeCancels */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFileTreeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFileTree<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._beforeEachFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.beforeEachFileTree} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePage.FileTreeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `fileTree` method from being executed.
 * - `sub` _(value: !IFileTreePage.fileTree.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.fileTree.OptAnswers&gt;) =&gt; void_ Cancels a call to `fileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.beforeEachFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTree<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFileTree} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `after` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.fileTree.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTreeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFileTreeReturns<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFileTreeReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFileTreeReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFileTreePointcutData} [data] Metadata passed to the pointcuts of _fileTree_ at the `afterEach` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.fileTree.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `args` _IFileTreePage.FileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFileTreeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFilterFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeFilterFiles<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._beforeFilterFiles */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.beforeFilterFiles} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePage.FilterFilesNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterFiles` method from being executed.
 * - `sub` _(value: IFileTreePage.filterFiles.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.filterFiles.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterFiles` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.beforeFilterFiles = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFiles<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFiles */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFiles} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `after` joinpoint.
 * - `res` _IFileTreePage.filterFiles.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.filterFiles.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFiles = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesThrows<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesThrows */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterThrowsFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterFiles` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesReturns<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterReturnsFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `afterReturns` joinpoint.
 * - `res` _IFileTreePage.filterFiles.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.filterFiles.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IFileTreePage.filterFiles.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.filterFiles.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterFilterFilesCancels<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterFilterFilesCancels */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterCancelsFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterFilterFilesCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFilterFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__beforeEachFilterFiles<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._beforeEachFilterFiles */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.beforeEachFilterFiles} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.BeforeFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePage.FilterFilesNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterFiles` method from being executed.
 * - `sub` _(value: IFileTreePage.filterFiles.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.filterFiles.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterFiles` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.beforeEachFilterFiles = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFiles<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFilterFiles */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFilterFiles} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `after` joinpoint.
 * - `res` _IFileTreePage.filterFiles.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.filterFiles.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFilterFiles = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData) => void} xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFilesReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageJoinpointModel.__afterEachFilterFilesReturns<!xyz.swapee.rc.IFileTreePageJoinpointModel>} xyz.swapee.rc.IFileTreePageJoinpointModel._afterEachFilterFilesReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFilterFilesReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageJoinpointModel.AfterFilterFilesPointcutData} [data] Metadata passed to the pointcuts of _filterFiles_ at the `afterEach` joinpoint.
 * - `res` _IFileTreePage.filterFiles.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IFileTreePage.filterFiles.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `args` _IFileTreePage.FilterFilesNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageJoinpointModel.FilterFilesPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageJoinpointModel.afterEachFilterFilesReturns = function(data) {}

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IFileTreePage.fileTree.Form, answers: !xyz.swapee.rc.IFileTreePage.fileTree.Answers, validation: !xyz.swapee.rc.IFileTreePage.fileTree.Validation, errors: !xyz.swapee.rc.IFileTreePage.fileTree.Errors, ctx: !xyz.swapee.rc.IFileTreePage.fileTree.Ctx) => (void|!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers>)} xyz.swapee.rc.IFileTreePage.__fileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePage.__fileTree<!xyz.swapee.rc.IFileTreePage>} xyz.swapee.rc.IFileTreePage._fileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePage.fileTree} */
/**
 *  `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Form} form The form.
 * - `locale` _string_
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} answers The answers.
 * - `locale` _string_
 * - `files` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * - `folders` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Validation} validation The validation.
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Ctx} ctx The ctx.
 * - `validation` _!IFileTreePage.fileTree.Validation_ The validation.
 * - `errors` _!IFileTreePage.fileTree.Errors_ The errors.
 * - `answers` _!IFileTreePage.fileTree.Answers_ The answers.
 * @return {void|!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers>}
 */
xyz.swapee.rc.IFileTreePage.fileTree = function(form, answers, validation, errors, ctx) {}

/**
 * The form.
 * @record xyz.swapee.rc.IFileTreePage.fileTree.Form
 */
xyz.swapee.rc.IFileTreePage.fileTree.Form = class { }
/**
 *
 */
xyz.swapee.rc.IFileTreePage.fileTree.Form.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage.filterFiles.Answers)} xyz.swapee.rc.IFileTreePage.fileTree.Answers.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePage.filterFiles.Answers} xyz.swapee.rc.IFileTreePage.filterFiles.Answers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IFileTreePage.fileTree.Answers
 */
xyz.swapee.rc.IFileTreePage.fileTree.Answers = class extends /** @type {xyz.swapee.rc.IFileTreePage.fileTree.Answers.constructor&xyz.swapee.rc.IFileTreePage.filterFiles.Answers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IFileTreePage.fileTree.Answers.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers)} xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers} xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers
 */
xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers = class extends /** @type {xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers.constructor&xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IFileTreePage.fileTree.OptAnswers.prototype.locale = /** @type {string|undefined} */ (void 0)

/**
 * The validation.
 * @record xyz.swapee.rc.IFileTreePage.fileTree.Validation
 */
xyz.swapee.rc.IFileTreePage.fileTree.Validation = class { }
xyz.swapee.rc.IFileTreePage.fileTree.Validation.prototype.constructor = xyz.swapee.rc.IFileTreePage.fileTree.Validation

/**
 * The errors.
 * @record xyz.swapee.rc.IFileTreePage.fileTree.Errors
 */
xyz.swapee.rc.IFileTreePage.fileTree.Errors = class { }
xyz.swapee.rc.IFileTreePage.fileTree.Errors.prototype.constructor = xyz.swapee.rc.IFileTreePage.fileTree.Errors

/**
 * The ctx.
 * @record xyz.swapee.rc.IFileTreePage.fileTree.Ctx
 */
xyz.swapee.rc.IFileTreePage.fileTree.Ctx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IFileTreePage.fileTree.Ctx.prototype.validation = /** @type {!xyz.swapee.rc.IFileTreePage.fileTree.Validation} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IFileTreePage.fileTree.Ctx.prototype.errors = /** @type {!xyz.swapee.rc.IFileTreePage.fileTree.Errors} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IFileTreePage.fileTree.Ctx.prototype.answers = /** @type {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} */ (void 0)

/**
 * The ctx.
 * @record xyz.swapee.rc.IFileTreePage.fileTree.OptCtx
 */
xyz.swapee.rc.IFileTreePage.fileTree.OptCtx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IFileTreePage.fileTree.OptCtx.prototype.validation = /** @type {(!xyz.swapee.rc.IFileTreePage.fileTree.Validation)|undefined} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IFileTreePage.fileTree.OptCtx.prototype.errors = /** @type {(!xyz.swapee.rc.IFileTreePage.fileTree.Errors)|undefined} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IFileTreePage.fileTree.OptCtx.prototype.answers = /** @type {(!xyz.swapee.rc.IFileTreePage.fileTree.Answers)|undefined} */ (void 0)

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IFileTreePage.filterFiles.Form, answers: !xyz.swapee.rc.IFileTreePage.filterFiles.Answers, validation: !xyz.swapee.rc.IFileTreePage.filterFiles.Validation, errors: !xyz.swapee.rc.IFileTreePage.filterFiles.Errors, ctx: !xyz.swapee.rc.IFileTreePage.filterFiles.Ctx) => (void|xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers>)} xyz.swapee.rc.IFileTreePage.__filterFiles
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePage.__filterFiles<!xyz.swapee.rc.IFileTreePage>} xyz.swapee.rc.IFileTreePage._filterFiles */
/** @typedef {typeof xyz.swapee.rc.IFileTreePage.filterFiles} */
/**
 * Returns files and folders in the directory. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePage.filterFiles.Form} form The action form.
 * - `root` _&#42;_
 * - `folder` _&#42;_
 * @param {!xyz.swapee.rc.IFileTreePage.filterFiles.Answers} answers The action answers.
 * - `files` _&#42;_
 * - `folders` _&#42;_
 * @param {!xyz.swapee.rc.IFileTreePage.filterFiles.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IFileTreePage.filterFiles.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IFileTreePage.filterFiles.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers>}
 */
xyz.swapee.rc.IFileTreePage.filterFiles = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IFileTreePage.filterFiles.Form
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Form = class { }
/**
 *
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Form.prototype.root = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Form.prototype.folder = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IFileTreePage.filterFiles.Answers
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Answers.prototype.files = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Answers.prototype.folders = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers
 */
xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers.prototype.files = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileTreePage.filterFiles.OptAnswers.prototype.folders = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IFileTreePage.filterFiles.Validation
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Validation = class { }
xyz.swapee.rc.IFileTreePage.filterFiles.Validation.prototype.constructor = xyz.swapee.rc.IFileTreePage.filterFiles.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IFileTreePage.filterFiles.Errors
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Errors = class { }
xyz.swapee.rc.IFileTreePage.filterFiles.Errors.prototype.constructor = xyz.swapee.rc.IFileTreePage.filterFiles.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IFileTreePage.filterFiles.Ctx
 */
xyz.swapee.rc.IFileTreePage.filterFiles.Ctx = class { }
xyz.swapee.rc.IFileTreePage.filterFiles.Ctx.prototype.constructor = xyz.swapee.rc.IFileTreePage.filterFiles.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IFileTreePage.filterFiles.OptCtx
 */
xyz.swapee.rc.IFileTreePage.filterFiles.OptCtx = class { }
xyz.swapee.rc.IFileTreePage.filterFiles.OptCtx.prototype.constructor = xyz.swapee.rc.IFileTreePage.filterFiles.OptCtx

// nss:xyz.swapee.rc,xyz.swapee.rc.IFileTreePageJoinpointModel,xyz.swapee.rc.IFileTreePage
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-tree/file-tree.ws/file-tree/FileTree/design/IFileTreePageAliases.xml}  4c3c0ab518f2d9ef96fa0411c2d5ba1b */
/** @constructor xyz.swapee.rc.FileTree */
xyz.swapee.rc.FileTree = class { }
xyz.swapee.rc.FileTree.prototype.constructor = xyz.swapee.rc.FileTree

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage.fileTree.Answers)} xyz.swapee.rc.FileTree.Answers.constructor */
/**
 * The answers.
 * @record xyz.swapee.rc.FileTree.Answers
 */
xyz.swapee.rc.FileTree.Answers = class extends /** @type {xyz.swapee.rc.FileTree.Answers.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileTree.Answers.prototype.props = /** @type {!xyz.swapee.rc.FileTree.Answers.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileTree.Answers.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage.fileTree.Form)} xyz.swapee.rc.FileTree.Form.constructor */
/**
 * The form.
 * @record xyz.swapee.rc.FileTree.Form
 */
xyz.swapee.rc.FileTree.Form = class extends /** @type {xyz.swapee.rc.FileTree.Form.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileTree.Form.prototype.props = /** @type {!xyz.swapee.rc.FileTree.Form.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileTree.Form.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage.fileTree.Errors)} xyz.swapee.rc.FileTree.Errors.constructor */
/**
 * The errors.
 * @record xyz.swapee.rc.FileTree.Errors
 */
xyz.swapee.rc.FileTree.Errors = class extends /** @type {xyz.swapee.rc.FileTree.Errors.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileTree.Errors.prototype.props = /** @type {!xyz.swapee.rc.FileTree.Errors.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileTree.Errors.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage.fileTree.Validation)} xyz.swapee.rc.FileTree.Validation.constructor */
/**
 * The validation.
 * @record xyz.swapee.rc.FileTree.Validation
 */
xyz.swapee.rc.FileTree.Validation = class extends /** @type {xyz.swapee.rc.FileTree.Validation.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileTree.Validation.prototype.props = /** @type {!xyz.swapee.rc.FileTree.Validation.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileTree.Validation.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IFileTreePage.fileTree.Ctx)} xyz.swapee.rc.FileTree.Ctx.constructor */
/**
 * The ctx.
 * @record xyz.swapee.rc.FileTree.Ctx
 */
xyz.swapee.rc.FileTree.Ctx = class extends /** @type {xyz.swapee.rc.FileTree.Ctx.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.FileTree.Ctx.prototype.props = /** @type {!xyz.swapee.rc.FileTree.Ctx.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.FileTree.Ctx.Props The props for VSCode. */

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-tree/file-tree.ws/file-tree/FileTree/design/IFileTreePageView.xml}  c2396ee85f6cfb05f1b26efa5573e775 */
/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreePageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileTreePageView)} xyz.swapee.rc.AbstractFileTreePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePageView} xyz.swapee.rc.FileTreePageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileTreePageView` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePageView
 */
xyz.swapee.rc.AbstractFileTreePageView = class extends /** @type {xyz.swapee.rc.AbstractFileTreePageView.constructor&xyz.swapee.rc.FileTreePageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePageView.prototype.constructor = xyz.swapee.rc.AbstractFileTreePageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePageView.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileTreePageView|typeof xyz.swapee.rc.FileTreePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UFileTreePageView|typeof xyz.swapee.rc.UFileTreePageView)|!xyz.swapee.rc.IFileTreePageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePageView}
 */
xyz.swapee.rc.AbstractFileTreePageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageView}
 */
xyz.swapee.rc.AbstractFileTreePageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileTreePageView|typeof xyz.swapee.rc.FileTreePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UFileTreePageView|typeof xyz.swapee.rc.UFileTreePageView)|!xyz.swapee.rc.IFileTreePageViewHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageView}
 */
xyz.swapee.rc.AbstractFileTreePageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileTreePageView|typeof xyz.swapee.rc.FileTreePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UFileTreePageView|typeof xyz.swapee.rc.UFileTreePageView)|!xyz.swapee.rc.IFileTreePageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageView}
 */
xyz.swapee.rc.AbstractFileTreePageView.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageViewJoinpointModelHyperslice
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeViewFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeViewFileTree>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTree>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewFileTreeThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewFileTreeCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachViewFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachViewFileTree>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachViewFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachViewFileTree>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachViewFileTreeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachViewFileTreeReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeGetFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeGetFileTree>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTree>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetFileTreeThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetFileTreeCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachGetFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachGetFileTree>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachGetFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachGetFileTree>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachGetFileTreeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachGetFileTreeReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeSetFileTreeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeSetFileTreeCtx>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtx>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetFileTreeCtxThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetFileTreeCtxReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetFileTreeCtxCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachSetFileTreeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachSetFileTreeCtx>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachSetFileTreeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachSetFileTreeCtx>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetFileTreeCtxReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachSetFileTreeCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachSetFileTreeCtxReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileTreePartial=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._before_FileTreePartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._before_FileTreePartial>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileTreePartial=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartial>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileTreePartialThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileTreePartialReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileTreePartialCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._before_FileTreeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._before_FileTreeView>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeView>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileTreeViewThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileTreeViewReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileTreeViewCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEach_FileTreeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEach_FileTreeView>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEach_FileTreeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEach_FileTreeView>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_FileTreeViewReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEach_FileTreeViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEach_FileTreeViewReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageViewJoinpointModelHyperslice

/**
 * A concrete class of _IFileTreePageViewJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageViewJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileTreePageViewJoinpointModelHyperslice = class extends xyz.swapee.rc.IFileTreePageViewJoinpointModelHyperslice { }
xyz.swapee.rc.FileTreePageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageViewJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeViewFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeViewFileTree<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTree<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewFileTreeThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewFileTreeCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachViewFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachViewFileTree<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTree<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTreeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTreeReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeGetFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeGetFileTree<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTree<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetFileTreeThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetFileTreeCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachGetFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachGetFileTree<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetFileTree=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTree<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetFileTreeReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTreeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTreeReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeSetFileTreeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeSetFileTreeCtx<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtx<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetFileTreeCtxThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetFileTreeCtxReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetFileTreeCtxCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachSetFileTreeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachSetFileTreeCtx<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtx<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetFileTreeCtxReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtxReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileTreePartial=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreePartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreePartial<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileTreePartial=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartial<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileTreePartialThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileTreePartialReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileTreePartialCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreeView<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeView<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_FileTreeViewThrows=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_FileTreeViewReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_FileTreeViewCancels=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEach_FileTreeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEach_FileTreeView<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeView<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_FileTreeViewReturns=/** @type {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeViewReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice

/**
 * A concrete class of _IFileTreePageViewJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileTreePageViewJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice { }
xyz.swapee.rc.FileTreePageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageViewJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IFileTreePageView`'s methods.
 * @interface xyz.swapee.rc.IFileTreePageViewJoinpointModel
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel = class { }
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeViewFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.beforeViewFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterViewFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeThrows} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterViewFileTreeThrows = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterViewFileTreeReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeCancels} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterViewFileTreeCancels = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachViewFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.beforeEachViewFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachViewFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEachViewFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachViewFileTreeReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEachViewFileTreeReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeGetFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.beforeGetFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterGetFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeThrows} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterGetFileTreeThrows = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterGetFileTreeReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeCancels} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterGetFileTreeCancels = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachGetFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.beforeEachGetFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachGetFileTree} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEachGetFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachGetFileTreeReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEachGetFileTreeReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeSetFileTreeCtx} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.beforeSetFileTreeCtx = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtx} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterSetFileTreeCtx = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxThrows} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterSetFileTreeCtxThrows = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterSetFileTreeCtxReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxCancels} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterSetFileTreeCtxCancels = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachSetFileTreeCtx} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.beforeEachSetFileTreeCtx = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachSetFileTreeCtx} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEachSetFileTreeCtx = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachSetFileTreeCtxReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEachSetFileTreeCtxReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.before_FileTreePartial} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.before_FileTreePartial = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartial} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreePartial = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialThrows} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreePartialThrows = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreePartialReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialCancels} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreePartialCancels = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.before_FileTreeView} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.before_FileTreeView = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeView} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreeView = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewThrows} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreeViewThrows = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreeViewReturns = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewCancels} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.after_FileTreeViewCancels = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEach_FileTreeView} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.beforeEach_FileTreeView = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEach_FileTreeView} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEach_FileTreeView = function() {}
/** @type {xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEach_FileTreeViewReturns} */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.prototype.afterEach_FileTreeViewReturns = function() {}

/**
 * A concrete class of _IFileTreePageViewJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewJoinpointModel
 * @implements {xyz.swapee.rc.IFileTreePageViewJoinpointModel} An interface that enumerates the joinpoints of `IFileTreePageView`'s methods.
 */
xyz.swapee.rc.FileTreePageViewJoinpointModel = class extends xyz.swapee.rc.IFileTreePageViewJoinpointModel { }
xyz.swapee.rc.FileTreePageViewJoinpointModel.prototype.constructor = xyz.swapee.rc.FileTreePageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel} */
xyz.swapee.rc.RecordIFileTreePageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel} xyz.swapee.rc.BoundIFileTreePageViewJoinpointModel */

/** @typedef {xyz.swapee.rc.FileTreePageViewJoinpointModel} xyz.swapee.rc.BoundFileTreePageViewJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreePageViewAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileTreePageViewAspectsInstaller)} xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller} xyz.swapee.rc.FileTreePageViewAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileTreePageViewAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller
 */
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.constructor&xyz.swapee.rc.FileTreePageViewAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileTreePageViewAspectsInstaller|typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller)|(!xyz.swapee.rc.IFileTreePageInstaller|typeof xyz.swapee.rc.FileTreePageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileTreePageViewAspectsInstaller|typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller)|(!xyz.swapee.rc.IFileTreePageInstaller|typeof xyz.swapee.rc.FileTreePageInstaller)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileTreePageViewAspectsInstaller|typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller)|(!xyz.swapee.rc.IFileTreePageInstaller|typeof xyz.swapee.rc.FileTreePageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileTreePageViewAspectsInstaller.Initialese[]) => xyz.swapee.rc.IFileTreePageViewAspectsInstaller} xyz.swapee.rc.FileTreePageViewAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileTreePageInstaller)} xyz.swapee.rc.IFileTreePageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageInstaller} xyz.swapee.rc.IFileTreePageInstaller.typeof */
/** @interface xyz.swapee.rc.IFileTreePageViewAspectsInstaller */
xyz.swapee.rc.IFileTreePageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.IFileTreePageViewAspectsInstaller.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileTreePageInstaller.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeViewFileTree=/** @type {number} */ (void 0)
    this.afterViewFileTree=/** @type {number} */ (void 0)
    this.afterViewFileTreeThrows=/** @type {number} */ (void 0)
    this.afterViewFileTreeReturns=/** @type {number} */ (void 0)
    this.afterViewFileTreeCancels=/** @type {number} */ (void 0)
    this.beforeEachViewFileTree=/** @type {number} */ (void 0)
    this.afterEachViewFileTree=/** @type {number} */ (void 0)
    this.afterEachViewFileTreeReturns=/** @type {number} */ (void 0)
    this.beforeGetFileTree=/** @type {number} */ (void 0)
    this.afterGetFileTree=/** @type {number} */ (void 0)
    this.afterGetFileTreeThrows=/** @type {number} */ (void 0)
    this.afterGetFileTreeReturns=/** @type {number} */ (void 0)
    this.afterGetFileTreeCancels=/** @type {number} */ (void 0)
    this.beforeEachGetFileTree=/** @type {number} */ (void 0)
    this.afterEachGetFileTree=/** @type {number} */ (void 0)
    this.afterEachGetFileTreeReturns=/** @type {number} */ (void 0)
    this.beforeSetFileTreeCtx=/** @type {number} */ (void 0)
    this.afterSetFileTreeCtx=/** @type {number} */ (void 0)
    this.afterSetFileTreeCtxThrows=/** @type {number} */ (void 0)
    this.afterSetFileTreeCtxReturns=/** @type {number} */ (void 0)
    this.afterSetFileTreeCtxCancels=/** @type {number} */ (void 0)
    this.beforeEachSetFileTreeCtx=/** @type {number} */ (void 0)
    this.afterEachSetFileTreeCtx=/** @type {number} */ (void 0)
    this.afterEachSetFileTreeCtxReturns=/** @type {number} */ (void 0)
    this.before_FileTreePartial=/** @type {number} */ (void 0)
    this.after_FileTreePartial=/** @type {number} */ (void 0)
    this.after_FileTreePartialThrows=/** @type {number} */ (void 0)
    this.after_FileTreePartialReturns=/** @type {number} */ (void 0)
    this.after_FileTreePartialCancels=/** @type {number} */ (void 0)
    this.before_FileTreeView=/** @type {number} */ (void 0)
    this.after_FileTreeView=/** @type {number} */ (void 0)
    this.after_FileTreeViewThrows=/** @type {number} */ (void 0)
    this.after_FileTreeViewReturns=/** @type {number} */ (void 0)
    this.after_FileTreeViewCancels=/** @type {number} */ (void 0)
    this.beforeEach_FileTreeView=/** @type {number} */ (void 0)
    this.afterEach_FileTreeView=/** @type {number} */ (void 0)
    this.afterEach_FileTreeViewReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  viewFileTree() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  getFileTree() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  setFileTreeCtx() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  FileTreePartial() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  FileTreeView() { }
}
/**
 * Create a new *IFileTreePageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileTreePageViewAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileTreePageViewAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageViewAspectsInstaller.Initialese>)} xyz.swapee.rc.FileTreePageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewAspectsInstaller} xyz.swapee.rc.IFileTreePageViewAspectsInstaller.typeof */
/**
 * A concrete class of _IFileTreePageViewAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewAspectsInstaller
 * @implements {xyz.swapee.rc.IFileTreePageViewAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageViewAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.FileTreePageViewAspectsInstaller.constructor&xyz.swapee.rc.IFileTreePageViewAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreePageViewAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileTreePageViewAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreePageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspectsInstaller}
 */
xyz.swapee.rc.FileTreePageViewAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageView.ViewFileTreeNArgs
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageViewJoinpointModel.ViewFileTreePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileTreePageView.ViewFileTreeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeViewFileTreePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileTreePageView.ViewFileTreeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `viewFileTree` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileTreePageView.FileTreeView) => void} sub Cancels a call to `viewFileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeViewFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.ViewFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeViewFileTreePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreeView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.ViewFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsViewFileTreePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `viewFileTree` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsViewFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.ViewFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsViewFileTreePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsViewFileTreePointcutData
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreeView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileTreePageView.FileTreeView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsViewFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.ViewFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsViewFileTreePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsViewFileTreePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsViewFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.ViewFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsViewFileTreePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageView.GetFileTreeNArgs
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageViewJoinpointModel.GetFileTreePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileTreePageView.GetFileTreeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeGetFileTreePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileTreePageView.GetFileTreeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getFileTree` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileTreePageView.FileTreeView) => void} sub Cancels a call to `getFileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeGetFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.GetFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeGetFileTreePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreeView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.GetFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsGetFileTreePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getFileTree` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsGetFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.GetFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsGetFileTreePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsGetFileTreePointcutData
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreeView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileTreePageView.FileTreeView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsGetFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.GetFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsGetFileTreePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsGetFileTreePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsGetFileTreePointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.GetFileTreePointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsGetFileTreePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageView.SetFileTreeCtxNArgs
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation} translation The translation for the user's locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileTreePageView.SetFileTreeCtxNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeSetFileTreeCtxPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileTreePageView.SetFileTreeCtxNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `setFileTreeCtx` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IFileTreePage.fileTree.OptCtx) => void} sub Cancels a call to `setFileTreeCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeSetFileTreeCtxPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeSetFileTreeCtxPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.OptCtx} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsSetFileTreeCtxPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `setFileTreeCtx` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsSetFileTreeCtxPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsSetFileTreeCtxPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsSetFileTreeCtxPointcutData
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.OptCtx} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IFileTreePage.fileTree.OptCtx) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsSetFileTreeCtxPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsSetFileTreeCtxPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsSetFileTreeCtxPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsSetFileTreeCtxPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsSetFileTreeCtxPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageView.FileTreePartialNArgs
 * @prop {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions} actions The actions.
 * @prop {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation} translation The translation for the chosen user locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreePartialPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IFileTreePageView.FileTreePartialNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreePartialPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IFileTreePageView.FileTreePartialNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `FileTreePartial` method from being executed.
 * @prop {(value: engineering.type.VNode) => void} sub Cancels a call to `FileTreePartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreePartialPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreePartialPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreePartialPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreePartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreePartialPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreePartialPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreePartialPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreePartialPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `FileTreePartial` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreePartialPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreePartialPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreePartialPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreePartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 * @prop {(value: engineering.type.VNode) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreePartialPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreePartialPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreePartialPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreePartialPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreePartialPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreePartialPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreePartialPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreeViewPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreeViewPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `FileTreeView` method from being executed.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreeViewPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreeViewPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreeViewPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreeViewPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreeViewPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreeViewPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `FileTreeView` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreeViewPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreeViewPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreeViewPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreeViewPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreeViewPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreeViewPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreeViewPointcutData&xyz.swapee.rc.IFileTreePageViewJoinpointModel.FileTreeViewPointcutData} xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreeViewPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IFileTreePageView.Initialese[]) => xyz.swapee.rc.IFileTreePageView} xyz.swapee.rc.FileTreePageViewConstructor */

/** @typedef {symbol} xyz.swapee.rc.FileTreePageViewMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UFileTreePageView.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IFileTreePageView} [fileTreePageView]
 */

/** @typedef {function(new: xyz.swapee.rc.FileTreePageViewUniversal)} xyz.swapee.rc.AbstractFileTreePageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePageViewUniversal} xyz.swapee.rc.FileTreePageViewUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UFileTreePageView` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePageViewUniversal
 */
xyz.swapee.rc.AbstractFileTreePageViewUniversal = class extends /** @type {xyz.swapee.rc.AbstractFileTreePageViewUniversal.constructor&xyz.swapee.rc.FileTreePageViewUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePageViewUniversal.prototype.constructor = xyz.swapee.rc.AbstractFileTreePageViewUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePageViewUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePageViewUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UFileTreePageView|typeof xyz.swapee.rc.UFileTreePageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePageViewUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePageViewUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageViewUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageViewUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UFileTreePageView|typeof xyz.swapee.rc.UFileTreePageView} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageViewUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UFileTreePageView|typeof xyz.swapee.rc.UFileTreePageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewUniversal}
 */
xyz.swapee.rc.AbstractFileTreePageViewUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.FileTreePageViewMetaUniversal} xyz.swapee.rc.AbstractFileTreePageViewUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UFileTreePageView.Initialese[]) => xyz.swapee.rc.UFileTreePageView} xyz.swapee.rc.UFileTreePageViewConstructor */

/** @typedef {function(new: xyz.swapee.rc.UFileTreePageViewFields&engineering.type.IEngineer&xyz.swapee.rc.UFileTreePageViewCaster)} xyz.swapee.rc.UFileTreePageView.constructor */
/**
 * A trait that allows to access an _IFileTreePageView_ object via a `.fileTreePageView` field.
 * @interface xyz.swapee.rc.UFileTreePageView
 */
xyz.swapee.rc.UFileTreePageView = class extends /** @type {xyz.swapee.rc.UFileTreePageView.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UFileTreePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileTreePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UFileTreePageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UFileTreePageView&engineering.type.IInitialiser<!xyz.swapee.rc.UFileTreePageView.Initialese>)} xyz.swapee.rc.FileTreePageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UFileTreePageView} xyz.swapee.rc.UFileTreePageView.typeof */
/**
 * A concrete class of _UFileTreePageView_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewUniversal
 * @implements {xyz.swapee.rc.UFileTreePageView} A trait that allows to access an _IFileTreePageView_ object via a `.fileTreePageView` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UFileTreePageView.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePageViewUniversal = class extends /** @type {xyz.swapee.rc.FileTreePageViewUniversal.constructor&xyz.swapee.rc.UFileTreePageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UFileTreePageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UFileTreePageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UFileTreePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UFileTreePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePageViewUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePageViewUniversal}
 */
xyz.swapee.rc.FileTreePageViewUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UFileTreePageView.
 * @interface xyz.swapee.rc.UFileTreePageViewFields
 */
xyz.swapee.rc.UFileTreePageViewFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UFileTreePageViewFields.prototype.fileTreePageView = /** @type {xyz.swapee.rc.IFileTreePageView} */ (void 0)

/** @typedef {xyz.swapee.rc.UFileTreePageView} */
xyz.swapee.rc.RecordUFileTreePageView

/** @typedef {xyz.swapee.rc.UFileTreePageView} xyz.swapee.rc.BoundUFileTreePageView */

/** @typedef {xyz.swapee.rc.FileTreePageViewUniversal} xyz.swapee.rc.BoundFileTreePageViewUniversal */

/**
 * Contains getters to cast the _UFileTreePageView_ interface.
 * @interface xyz.swapee.rc.UFileTreePageViewCaster
 */
xyz.swapee.rc.UFileTreePageViewCaster = class { }
/**
 * Provides direct access to _UFileTreePageView_ via the _BoundUFileTreePageView_ universal.
 * @type {!xyz.swapee.rc.BoundFileTreePageView}
 */
xyz.swapee.rc.UFileTreePageViewCaster.prototype.asFileTreePageView
/**
 * Cast the _UFileTreePageView_ instance into the _BoundUFileTreePageView_ type.
 * @type {!xyz.swapee.rc.BoundUFileTreePageView}
 */
xyz.swapee.rc.UFileTreePageViewCaster.prototype.asUFileTreePageView
/**
 * Access the _FileTreePageViewUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundFileTreePageViewUniversal}
 */
xyz.swapee.rc.UFileTreePageViewCaster.prototype.superFileTreePageViewUniversal

/** @typedef {function(new: xyz.swapee.rc.BFileTreePageViewAspectsCaster<THIS>&xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BFileTreePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice} xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IFileTreePageView* that bind to an instance.
 * @interface xyz.swapee.rc.BFileTreePageViewAspects
 * @template THIS
 */
xyz.swapee.rc.BFileTreePageViewAspects = class extends /** @type {xyz.swapee.rc.BFileTreePageViewAspects.constructor&xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BFileTreePageViewAspects.prototype.constructor = xyz.swapee.rc.BFileTreePageViewAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreePageViewAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileTreePageViewAspects)} xyz.swapee.rc.AbstractFileTreePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreePageViewAspects} xyz.swapee.rc.FileTreePageViewAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileTreePageViewAspects` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreePageViewAspects
 */
xyz.swapee.rc.AbstractFileTreePageViewAspects = class extends /** @type {xyz.swapee.rc.AbstractFileTreePageViewAspects.constructor&xyz.swapee.rc.FileTreePageViewAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreePageViewAspects.prototype.constructor = xyz.swapee.rc.AbstractFileTreePageViewAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreePageViewAspects.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreePageViewAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileTreePageViewAspects|typeof xyz.swapee.rc.FileTreePageViewAspects)|(!xyz.swapee.rc.BFileTreePageViewAspects|typeof xyz.swapee.rc.BFileTreePageViewAspects)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BFileTreePageAspects|typeof xyz.swapee.rc.BFileTreePageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreePageViewAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreePageViewAspects}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspects}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileTreePageViewAspects|typeof xyz.swapee.rc.FileTreePageViewAspects)|(!xyz.swapee.rc.BFileTreePageViewAspects|typeof xyz.swapee.rc.BFileTreePageViewAspects)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BFileTreePageAspects|typeof xyz.swapee.rc.BFileTreePageAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspects}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileTreePageViewAspects|typeof xyz.swapee.rc.FileTreePageViewAspects)|(!xyz.swapee.rc.BFileTreePageViewAspects|typeof xyz.swapee.rc.BFileTreePageViewAspects)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BFileTreePageAspects|typeof xyz.swapee.rc.BFileTreePageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspects}
 */
xyz.swapee.rc.AbstractFileTreePageViewAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IFileTreePageViewAspects.Initialese[]) => xyz.swapee.rc.IFileTreePageViewAspects} xyz.swapee.rc.FileTreePageViewAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileTreePageViewAspectsCaster&xyz.swapee.rc.BFileTreePageViewAspects<!xyz.swapee.rc.IFileTreePageViewAspects>&xyz.swapee.rc.IFileTreePage&_idio.IRedirectMod&xyz.swapee.rc.BFileTreePageAspects)} xyz.swapee.rc.IFileTreePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BFileTreePageViewAspects} xyz.swapee.rc.BFileTreePageViewAspects.typeof */
/** @typedef {typeof _idio.IRedirectMod} _idio.IRedirectMod.typeof */
/**
 * The aspects of the *IFileTreePageView*.
 * @interface xyz.swapee.rc.IFileTreePageViewAspects
 */
xyz.swapee.rc.IFileTreePageViewAspects = class extends /** @type {xyz.swapee.rc.IFileTreePageViewAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BFileTreePageViewAspects.typeof&xyz.swapee.rc.IFileTreePage.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.BFileTreePageAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreePageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileTreePageViewAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileTreePageViewAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageViewAspects.Initialese>)} xyz.swapee.rc.FileTreePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewAspects} xyz.swapee.rc.IFileTreePageViewAspects.typeof */
/**
 * A concrete class of _IFileTreePageViewAspects_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewAspects
 * @implements {xyz.swapee.rc.IFileTreePageViewAspects} The aspects of the *IFileTreePageView*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageViewAspects.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePageViewAspects = class extends /** @type {xyz.swapee.rc.FileTreePageViewAspects.constructor&xyz.swapee.rc.IFileTreePageViewAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreePageViewAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileTreePageViewAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreePageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePageViewAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePageViewAspects}
 */
xyz.swapee.rc.FileTreePageViewAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BFileTreePageViewAspects_ interface.
 * @interface xyz.swapee.rc.BFileTreePageViewAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BFileTreePageViewAspectsCaster = class { }
/**
 * Cast the _BFileTreePageViewAspects_ instance into the _BoundIFileTreePageView_ type.
 * @type {!xyz.swapee.rc.BoundIFileTreePageView}
 */
xyz.swapee.rc.BFileTreePageViewAspectsCaster.prototype.asIFileTreePageView

/**
 * Contains getters to cast the _IFileTreePageViewAspects_ interface.
 * @interface xyz.swapee.rc.IFileTreePageViewAspectsCaster
 */
xyz.swapee.rc.IFileTreePageViewAspectsCaster = class { }
/**
 * Cast the _IFileTreePageViewAspects_ instance into the _BoundIFileTreePageView_ type.
 * @type {!xyz.swapee.rc.BoundIFileTreePageView}
 */
xyz.swapee.rc.IFileTreePageViewAspectsCaster.prototype.asIFileTreePageView

/** @typedef {xyz.swapee.rc.IFileTreePageView.Initialese} xyz.swapee.rc.IHyperFileTreePageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.HyperFileTreePageView)} xyz.swapee.rc.AbstractHyperFileTreePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperFileTreePageView} xyz.swapee.rc.HyperFileTreePageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperFileTreePageView` interface.
 * @constructor xyz.swapee.rc.AbstractHyperFileTreePageView
 */
xyz.swapee.rc.AbstractHyperFileTreePageView = class extends /** @type {xyz.swapee.rc.AbstractHyperFileTreePageView.constructor&xyz.swapee.rc.HyperFileTreePageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperFileTreePageView.prototype.constructor = xyz.swapee.rc.AbstractHyperFileTreePageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperFileTreePageView.class = /** @type {typeof xyz.swapee.rc.AbstractHyperFileTreePageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperFileTreePageView|typeof xyz.swapee.rc.HyperFileTreePageView)|(!xyz.swapee.rc.IFileTreePageView|typeof xyz.swapee.rc.FileTreePageView)|(!xyz.swapee.rc.IFileTreePageViewHyperslice|typeof xyz.swapee.rc.FileTreePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileTreePageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileTreePageView}
 */
xyz.swapee.rc.AbstractHyperFileTreePageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePageView}
 */
xyz.swapee.rc.AbstractHyperFileTreePageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperFileTreePageView|typeof xyz.swapee.rc.HyperFileTreePageView)|(!xyz.swapee.rc.IFileTreePageView|typeof xyz.swapee.rc.FileTreePageView)|(!xyz.swapee.rc.IFileTreePageViewHyperslice|typeof xyz.swapee.rc.FileTreePageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePageView}
 */
xyz.swapee.rc.AbstractHyperFileTreePageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperFileTreePageView|typeof xyz.swapee.rc.HyperFileTreePageView)|(!xyz.swapee.rc.IFileTreePageView|typeof xyz.swapee.rc.FileTreePageView)|(!xyz.swapee.rc.IFileTreePageViewHyperslice|typeof xyz.swapee.rc.FileTreePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperFileTreePageView}
 */
xyz.swapee.rc.AbstractHyperFileTreePageView.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IFileTreePageViewAspects|function(new: xyz.swapee.rc.IFileTreePageViewAspects)} aides The list of aides that advise the IFileTreePageView to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperFileTreePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperFileTreePageView.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperFileTreePageView.Initialese[]) => xyz.swapee.rc.IHyperFileTreePageView} xyz.swapee.rc.HyperFileTreePageViewConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperFileTreePageViewCaster&xyz.swapee.rc.IFileTreePageView)} xyz.swapee.rc.IHyperFileTreePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageView} xyz.swapee.rc.IFileTreePageView.typeof */
/** @interface xyz.swapee.rc.IHyperFileTreePageView */
xyz.swapee.rc.IHyperFileTreePageView = class extends /** @type {xyz.swapee.rc.IHyperFileTreePageView.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileTreePageView.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperFileTreePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileTreePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperFileTreePageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperFileTreePageView&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileTreePageView.Initialese>)} xyz.swapee.rc.HyperFileTreePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperFileTreePageView} xyz.swapee.rc.IHyperFileTreePageView.typeof */
/**
 * A concrete class of _IHyperFileTreePageView_ instances.
 * @constructor xyz.swapee.rc.HyperFileTreePageView
 * @implements {xyz.swapee.rc.IHyperFileTreePageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperFileTreePageView.Initialese>} ‎
 */
xyz.swapee.rc.HyperFileTreePageView = class extends /** @type {xyz.swapee.rc.HyperFileTreePageView.constructor&xyz.swapee.rc.IHyperFileTreePageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperFileTreePageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperFileTreePageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperFileTreePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperFileTreePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperFileTreePageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperFileTreePageView}
 */
xyz.swapee.rc.HyperFileTreePageView.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperFileTreePageView} */
xyz.swapee.rc.RecordIHyperFileTreePageView

/** @typedef {xyz.swapee.rc.IHyperFileTreePageView} xyz.swapee.rc.BoundIHyperFileTreePageView */

/** @typedef {xyz.swapee.rc.HyperFileTreePageView} xyz.swapee.rc.BoundHyperFileTreePageView */

/**
 * Contains getters to cast the _IHyperFileTreePageView_ interface.
 * @interface xyz.swapee.rc.IHyperFileTreePageViewCaster
 */
xyz.swapee.rc.IHyperFileTreePageViewCaster = class { }
/**
 * Cast the _IHyperFileTreePageView_ instance into the _BoundIHyperFileTreePageView_ type.
 * @type {!xyz.swapee.rc.BoundIHyperFileTreePageView}
 */
xyz.swapee.rc.IHyperFileTreePageViewCaster.prototype.asIHyperFileTreePageView
/**
 * Access the _HyperFileTreePageView_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperFileTreePageView}
 */
xyz.swapee.rc.IHyperFileTreePageViewCaster.prototype.superHyperFileTreePageView

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageViewHyperslice
 */
xyz.swapee.rc.IFileTreePageViewHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageView._viewFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView._viewFileTree>} */ (void 0)
    /**
     * The internal view handler for the `fileTree` action.
     */
    this.getFileTree=/** @type {!xyz.swapee.rc.IFileTreePageView._getFileTree|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView._getFileTree>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageView._setFileTreeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView._setFileTreeCtx>} */ (void 0)
    /**
     * The _FileTree_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageView._FileTreeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView._FileTreeView>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageViewHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageViewHyperslice

/**
 * A concrete class of _IFileTreePageViewHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageViewHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.FileTreePageViewHyperslice = class extends xyz.swapee.rc.IFileTreePageViewHyperslice { }
xyz.swapee.rc.FileTreePageViewHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageViewHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IFileTreePageViewBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IFileTreePageViewBindingHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewFileTree=/** @type {!xyz.swapee.rc.IFileTreePageView.__viewFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView.__viewFileTree<THIS>>} */ (void 0)
    /**
     * The internal view handler for the `fileTree` action.
     */
    this.getFileTree=/** @type {!xyz.swapee.rc.IFileTreePageView.__getFileTree<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView.__getFileTree<THIS>>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setFileTreeCtx=/** @type {!xyz.swapee.rc.IFileTreePageView.__setFileTreeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView.__setFileTreeCtx<THIS>>} */ (void 0)
    /**
     * The _FileTree_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.FileTreeView=/** @type {!xyz.swapee.rc.IFileTreePageView.__FileTreeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IFileTreePageView.__FileTreeView<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IFileTreePageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.IFileTreePageViewBindingHyperslice

/**
 * A concrete class of _IFileTreePageViewBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.FileTreePageViewBindingHyperslice
 * @implements {xyz.swapee.rc.IFileTreePageViewBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IFileTreePageViewBindingHyperslice<THIS>}
 */
xyz.swapee.rc.FileTreePageViewBindingHyperslice = class extends xyz.swapee.rc.IFileTreePageViewBindingHyperslice { }
xyz.swapee.rc.FileTreePageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.FileTreePageViewBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IFileTreePageViewFields&engineering.type.IEngineer&xyz.swapee.rc.IFileTreePageViewCaster&_idio.IRedirectMod&xyz.swapee.rc.UFileTreePageView)} xyz.swapee.rc.IFileTreePageView.constructor */
/** @interface xyz.swapee.rc.IFileTreePageView */
xyz.swapee.rc.IFileTreePageView = class extends /** @type {xyz.swapee.rc.IFileTreePageView.constructor&engineering.type.IEngineer.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.UFileTreePageView.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileTreePageView.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IFileTreePageView.viewFileTree} */
xyz.swapee.rc.IFileTreePageView.prototype.viewFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageView.getFileTree} */
xyz.swapee.rc.IFileTreePageView.prototype.getFileTree = function() {}
/** @type {xyz.swapee.rc.IFileTreePageView.setFileTreeCtx} */
xyz.swapee.rc.IFileTreePageView.prototype.setFileTreeCtx = function() {}
/** @type {xyz.swapee.rc.IFileTreePageView.FileTreePartial} */
xyz.swapee.rc.IFileTreePageView.prototype.FileTreePartial = function() {}
/** @type {xyz.swapee.rc.IFileTreePageView.FileTreeView} */
xyz.swapee.rc.IFileTreePageView.prototype.FileTreeView = function() {}

/** @typedef {function(new: xyz.swapee.rc.IFileTreePageView&engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageView.Initialese>)} xyz.swapee.rc.FileTreePageView.constructor */
/**
 * A concrete class of _IFileTreePageView_ instances.
 * @constructor xyz.swapee.rc.FileTreePageView
 * @implements {xyz.swapee.rc.IFileTreePageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreePageView.Initialese>} ‎
 */
xyz.swapee.rc.FileTreePageView = class extends /** @type {xyz.swapee.rc.FileTreePageView.constructor&xyz.swapee.rc.IFileTreePageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreePageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileTreePageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreePageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreePageView}
 */
xyz.swapee.rc.FileTreePageView.__extend = function(...Extensions) {}

/**
 * Fields of the IFileTreePageView.
 * @interface xyz.swapee.rc.IFileTreePageViewFields
 */
xyz.swapee.rc.IFileTreePageViewFields = class { }
/**
 * The navs for _GET_ paths (e.g., to use for redirects).
 */
xyz.swapee.rc.IFileTreePageViewFields.prototype.GET = /** @type {!xyz.swapee.rc.IFileTreePageView.GET} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileTreePageViewFields.prototype.fileTreeTranslations = /** @type {!Object<string, !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation>} */ (void 0)

/** @typedef {xyz.swapee.rc.IFileTreePageView} */
xyz.swapee.rc.RecordIFileTreePageView

/** @typedef {xyz.swapee.rc.IFileTreePageView} xyz.swapee.rc.BoundIFileTreePageView */

/** @typedef {xyz.swapee.rc.FileTreePageView} xyz.swapee.rc.BoundFileTreePageView */

/**
 * @typedef {Object} xyz.swapee.rc.IFileTreePageView.GET The navs for _GET_ paths (e.g., to use for redirects).
 * @prop {xyz.swapee.rc.FileTree.fileTreeNav} fileTree
 */

/**
 * Contains getters to cast the _IFileTreePageView_ interface.
 * @interface xyz.swapee.rc.IFileTreePageViewCaster
 */
xyz.swapee.rc.IFileTreePageViewCaster = class { }
/**
 * Cast the _IFileTreePageView_ instance into the _BoundIFileTreePageView_ type.
 * @type {!xyz.swapee.rc.BoundIFileTreePageView}
 */
xyz.swapee.rc.IFileTreePageViewCaster.prototype.asIFileTreePageView
/**
 * Cast the _IFileTreePageView_ instance into the _BoundIFileTreePage_ type.
 * @type {!xyz.swapee.rc.BoundIFileTreePage}
 */
xyz.swapee.rc.IFileTreePageViewCaster.prototype.asIFileTreePage
/**
 * Access the _FileTreePageView_ prototype.
 * @type {!xyz.swapee.rc.BoundFileTreePageView}
 */
xyz.swapee.rc.IFileTreePageViewCaster.prototype.superFileTreePageView

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeViewFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeViewFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeViewFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeViewFileTree} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePageView.ViewFileTreeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewFileTree` method from being executed.
 * - `sub` _(value: !IFileTreePageView.FileTreeView) =&gt; void_ Cancels a call to `viewFileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeViewFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTree} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `after` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeThrows<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeThrows */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `viewFileTree` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `afterReturns` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileTreePageView.FileTreeView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterViewFileTreeCancels<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterViewFileTreeCancels */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterViewFileTreeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachViewFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachViewFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachViewFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachViewFileTree} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePageView.ViewFileTreeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewFileTree` method from being executed.
 * - `sub` _(value: !IFileTreePageView.FileTreeView) =&gt; void_ Cancels a call to `viewFileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachViewFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachViewFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachViewFileTree} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `after` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachViewFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTreeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachViewFileTreeReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachViewFileTreeReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachViewFileTreeReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterViewFileTreePointcutData} [data] Metadata passed to the pointcuts of _viewFileTree_ at the `afterEach` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `args` _IFileTreePageView.ViewFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.ViewFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachViewFileTreeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeGetFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeGetFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeGetFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeGetFileTree} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePageView.GetFileTreeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getFileTree` method from being executed.
 * - `sub` _(value: !IFileTreePageView.FileTreeView) =&gt; void_ Cancels a call to `getFileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeGetFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTree} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `after` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeThrows<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeThrows */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getFileTree` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `afterReturns` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileTreePageView.FileTreeView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterGetFileTreeCancels<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterGetFileTreeCancels */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterGetFileTreeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachGetFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachGetFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachGetFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachGetFileTree} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePageView.GetFileTreeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getFileTree` method from being executed.
 * - `sub` _(value: !IFileTreePageView.FileTreeView) =&gt; void_ Cancels a call to `getFileTree` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachGetFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTree<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachGetFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachGetFileTree} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `after` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachGetFileTree = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTreeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachGetFileTreeReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachGetFileTreeReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachGetFileTreeReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterGetFileTreePointcutData} [data] Metadata passed to the pointcuts of _getFileTree_ at the `afterEach` joinpoint.
 * - `res` _!IFileTreePageView.FileTreeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `args` _IFileTreePageView.GetFileTreeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.GetFileTreePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachGetFileTreeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeSetFileTreeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeSetFileTreeCtx<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeSetFileTreeCtx */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeSetFileTreeCtx} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePageView.SetFileTreeCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setFileTreeCtx` method from being executed.
 * - `sub` _(value: !IFileTreePage.fileTree.OptCtx) =&gt; void_ Cancels a call to `setFileTreeCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeSetFileTreeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtx<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtx */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtx} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `after` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxThrows<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxThrows */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `setFileTreeCtx` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `afterReturns` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptCtx_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFileTreePage.fileTree.OptCtx) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterSetFileTreeCtxCancels<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterSetFileTreeCtxCancels */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterSetFileTreeCtxCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachSetFileTreeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEachSetFileTreeCtx<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEachSetFileTreeCtx */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachSetFileTreeCtx} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePageView.SetFileTreeCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setFileTreeCtx` method from being executed.
 * - `sub` _(value: !IFileTreePage.fileTree.OptCtx) =&gt; void_ Cancels a call to `setFileTreeCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEachSetFileTreeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtx<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachSetFileTreeCtx */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachSetFileTreeCtx} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `after` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachSetFileTreeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEachSetFileTreeCtxReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEachSetFileTreeCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachSetFileTreeCtxReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterSetFileTreeCtxPointcutData} [data] Metadata passed to the pointcuts of _setFileTreeCtx_ at the `afterEach` joinpoint.
 * - `res` _!IFileTreePage.fileTree.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `args` _IFileTreePageView.SetFileTreeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.SetFileTreeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEachSetFileTreeCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreePartialPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreePartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreePartial<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._before_FileTreePartial */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.before_FileTreePartial} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreePartialPointcutData} [data] Metadata passed to the pointcuts of _FileTreePartial_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileTreePageView.FileTreePartialNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `FileTreePartial` method from being executed.
 * - `sub` _(value: engineering.type.VNode) =&gt; void_ Cancels a call to `FileTreePartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `args` _IFileTreePageView.FileTreePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.before_FileTreePartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreePartialPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartial<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartial */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartial} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreePartialPointcutData} [data] Metadata passed to the pointcuts of _FileTreePartial_ at the `after` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `args` _IFileTreePageView.FileTreePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreePartialPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialThrows<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialThrows */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreePartialPointcutData} [data] Metadata passed to the pointcuts of _FileTreePartial_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `FileTreePartial` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `args` _IFileTreePageView.FileTreePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreePartialPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreePartialPointcutData} [data] Metadata passed to the pointcuts of _FileTreePartial_ at the `afterReturns` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `sub` _(value: engineering.type.VNode) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `args` _IFileTreePageView.FileTreePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreePartialPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreePartialCancels<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreePartialCancels */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreePartialPointcutData} [data] Metadata passed to the pointcuts of _FileTreePartial_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `args` _IFileTreePageView.FileTreePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreePartialCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__before_FileTreeView<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._before_FileTreeView */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.before_FileTreeView} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `FileTreeView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.before_FileTreeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeView<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeView */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeView} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewThrows<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewThrows */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterThrowsFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `FileTreeView` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterReturnsFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__after_FileTreeViewCancels<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._after_FileTreeViewCancels */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterCancelsFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.after_FileTreeViewCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEach_FileTreeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__beforeEach_FileTreeView<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._beforeEach_FileTreeView */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEach_FileTreeView} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.BeforeFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `FileTreeView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.beforeEach_FileTreeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeView<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEach_FileTreeView */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEach_FileTreeView} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEach_FileTreeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreeViewPointcutData) => void} xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageViewJoinpointModel.__afterEach_FileTreeViewReturns<!xyz.swapee.rc.IFileTreePageViewJoinpointModel>} xyz.swapee.rc.IFileTreePageViewJoinpointModel._afterEach_FileTreeViewReturns */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEach_FileTreeViewReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePageViewJoinpointModel.AfterFileTreeViewPointcutData} [data] Metadata passed to the pointcuts of _FileTreeView_ at the `afterEach` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileTreePageViewJoinpointModel.FileTreeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IFileTreePageViewJoinpointModel.afterEach_FileTreeViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IFileTreePage.fileTree.Ctx, form: !xyz.swapee.rc.IFileTreePage.fileTree.Form) => !xyz.swapee.rc.IFileTreePageView.FileTreeView} xyz.swapee.rc.IFileTreePageView.__viewFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageView.__viewFileTree<!xyz.swapee.rc.IFileTreePageView>} xyz.swapee.rc.IFileTreePageView._viewFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageView.viewFileTree} */
/**
 * Assigns required view data to the context, then redirects, or invokes another pagelet. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Ctx} ctx The context.
 * - `validation` _!IFileTreePage.fileTree.Validation_ The validation.
 * - `errors` _!IFileTreePage.fileTree.Errors_ The errors.
 * - `answers` _!IFileTreePage.fileTree.Answers_ The answers.
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Form} form The form.
 * - `locale` _string_
 * @return {!xyz.swapee.rc.IFileTreePageView.FileTreeView}
 */
xyz.swapee.rc.IFileTreePageView.viewFileTree = function(ctx, form) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IFileTreePage.fileTree.Ctx, form: !xyz.swapee.rc.IFileTreePage.fileTree.Form) => (void|!xyz.swapee.rc.IFileTreePageView.FileTreeView)} xyz.swapee.rc.IFileTreePageView.__getFileTree
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageView.__getFileTree<!xyz.swapee.rc.IFileTreePageView>} xyz.swapee.rc.IFileTreePageView._getFileTree */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageView.getFileTree} */
/**
 * The internal view handler for the `fileTree` action. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Ctx} ctx The context.
 * - `validation` _!IFileTreePage.fileTree.Validation_ The validation.
 * - `errors` _!IFileTreePage.fileTree.Errors_ The errors.
 * - `answers` _!IFileTreePage.fileTree.Answers_ The answers.
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Form} form The form.
 * - `locale` _string_
 * @return {void|!xyz.swapee.rc.IFileTreePageView.FileTreeView}
 */
xyz.swapee.rc.IFileTreePageView.getFileTree = function(ctx, form) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IFileTreePage.fileTree.Answers, translation: !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation) => !xyz.swapee.rc.IFileTreePage.fileTree.OptCtx} xyz.swapee.rc.IFileTreePageView.__setFileTreeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageView.__setFileTreeCtx<!xyz.swapee.rc.IFileTreePageView>} xyz.swapee.rc.IFileTreePageView._setFileTreeCtx */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageView.setFileTreeCtx} */
/**
 * Assigns the context based on answers and translations. `🔗 $combine`
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} answers The answers.
 * - `locale` _string_
 * - `files` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * - `folders` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * @param {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation} translation The translation for the user's locale.
 * @return {!xyz.swapee.rc.IFileTreePage.fileTree.OptCtx}
 */
xyz.swapee.rc.IFileTreePageView.setFileTreeCtx = function(answers, translation) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IFileTreePage.fileTree.Answers, errors: !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Errors, actions: !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions, translation: !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IFileTreePageView.__FileTreePartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageView.__FileTreePartial<!xyz.swapee.rc.IFileTreePageView>} xyz.swapee.rc.IFileTreePageView._FileTreePartial */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageView.FileTreePartial} */
/**
 * The partial.
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} answers The answers.
 * - `locale` _string_
 * - `files` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * - `folders` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * @param {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions} actions The actions.
 * - `fileTree` _FileTree.fileTreeNav_
 * - `_fileTree` _!IFileTreePage.fileTree.Form_
 * - `_filterFiles` _IFileTreePage.filterFiles.Form_ The form of the `filterFiles` action.
 * - `filterFiles` _!FileTree.filterFilesNav_
 * @param {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IFileTreePageView.FileTreePartial = function(answers, errors, actions, translation) {}

/** @typedef {xyz.swapee.rc.IFileTreePage.fileTree.Errors&xyz.swapee.rc.IFileTreePage.fileTree.Validation} xyz.swapee.rc.IFileTreePageView.FileTreePartial.Errors The errors. */

/**
 * The actions.
 * @record xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions
 */
xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions = class { }
/**
 *
 */
xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions.prototype.fileTree = /** @type {xyz.swapee.rc.FileTree.fileTreeNav} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions.prototype._fileTree = /** @type {!xyz.swapee.rc.IFileTreePage.fileTree.Form} */ (void 0)
/**
 * The form of the `filterFiles` action.
 */
xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions.prototype._filterFiles = /** @type {xyz.swapee.rc.IFileTreePage.filterFiles.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions.prototype.filterFiles = /** @type {!xyz.swapee.rc.FileTree.filterFilesNav} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation The translation for the chosen user locale. */

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IFileTreePage.fileTree.Answers, errors: !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Errors, actions: !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions, translation: !xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IFileTreePageView.__FileTreeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IFileTreePageView.__FileTreeView<!xyz.swapee.rc.IFileTreePageView>} xyz.swapee.rc.IFileTreePageView._FileTreeView */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageView.FileTreeView} */
/**
 * The _FileTree_ view with partials and translation mechanics that has
 * access to answers written by the controller. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Answers} answers The answers.
 * - `locale` _string_
 * - `files` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * - `folders` _&#42;_ ⤴ *IFileTreePage.filterFiles.Answers*
 * @param {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Actions} actions The actions.
 * - `fileTree` _FileTree.fileTreeNav_
 * - `_fileTree` _!IFileTreePage.fileTree.Form_
 * - `_filterFiles` _IFileTreePage.filterFiles.Form_ The form of the `filterFiles` action.
 * - `filterFiles` _!FileTree.filterFilesNav_
 * @param {!xyz.swapee.rc.IFileTreePageView.FileTreePartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IFileTreePageView.FileTreeView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.IFileTreePageViewJoinpointModel,xyz.swapee.rc.IFileTreePageView
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/file-tree/file-tree.ws/file-tree/FileTree/design/api.xml}  9f0a7fa2a37fa1437dd839b4b6956b9f */
/**
 * The `fileTree` navigation metadata.
 * @constructor xyz.swapee.rc.FileTree.FileTreeNav
 */
xyz.swapee.rc.FileTree.FileTreeNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.FileTree.FileTreeNav.prototype.fileTree = /** @type {number} */ (void 0)

/**
 * The `filterFiles` navigation metadata.
 * @constructor xyz.swapee.rc.FileTree.FilterFilesNav
 */
xyz.swapee.rc.FileTree.FilterFilesNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.FileTree.FilterFilesNav.prototype.filterFiles = /** @type {number} */ (void 0)

/**
 * The ids of the methods.
 * @constructor xyz.swapee.rc.fileTreeMethodsIds
 */
xyz.swapee.rc.fileTreeMethodsIds = class { }
xyz.swapee.rc.fileTreeMethodsIds.prototype.constructor = xyz.swapee.rc.fileTreeMethodsIds

/**
 * The ids of the arcs.
 * @constructor xyz.swapee.rc.fileTreeArcsIds
 */
xyz.swapee.rc.fileTreeArcsIds = class { }
xyz.swapee.rc.fileTreeArcsIds.prototype.constructor = xyz.swapee.rc.fileTreeArcsIds

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IFileTreeImpl.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.FileTreeImpl)} xyz.swapee.rc.AbstractFileTreeImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.FileTreeImpl} xyz.swapee.rc.FileTreeImpl.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IFileTreeImpl` interface.
 * @constructor xyz.swapee.rc.AbstractFileTreeImpl
 */
xyz.swapee.rc.AbstractFileTreeImpl = class extends /** @type {xyz.swapee.rc.AbstractFileTreeImpl.constructor&xyz.swapee.rc.FileTreeImpl.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractFileTreeImpl.prototype.constructor = xyz.swapee.rc.AbstractFileTreeImpl
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractFileTreeImpl.class = /** @type {typeof xyz.swapee.rc.AbstractFileTreeImpl} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IFileTreeImpl|typeof xyz.swapee.rc.FileTreeImpl)|(!xyz.swapee.rc.IFileTreePageAspects|typeof xyz.swapee.rc.FileTreePageAspects)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.IFileTreePageHyperslice|typeof xyz.swapee.rc.FileTreePageHyperslice)|(!xyz.swapee.rc.IFileTreePageViewHyperslice|typeof xyz.swapee.rc.FileTreePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreeImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractFileTreeImpl.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractFileTreeImpl}
 */
xyz.swapee.rc.AbstractFileTreeImpl.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreeImpl}
 */
xyz.swapee.rc.AbstractFileTreeImpl.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IFileTreeImpl|typeof xyz.swapee.rc.FileTreeImpl)|(!xyz.swapee.rc.IFileTreePageAspects|typeof xyz.swapee.rc.FileTreePageAspects)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.IFileTreePageHyperslice|typeof xyz.swapee.rc.FileTreePageHyperslice)|(!xyz.swapee.rc.IFileTreePageViewHyperslice|typeof xyz.swapee.rc.FileTreePageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreeImpl}
 */
xyz.swapee.rc.AbstractFileTreeImpl.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IFileTreeImpl|typeof xyz.swapee.rc.FileTreeImpl)|(!xyz.swapee.rc.IFileTreePageAspects|typeof xyz.swapee.rc.FileTreePageAspects)|(!xyz.swapee.rc.IFileTreePage|typeof xyz.swapee.rc.FileTreePage)|(!xyz.swapee.rc.IFileTreePageHyperslice|typeof xyz.swapee.rc.FileTreePageHyperslice)|(!xyz.swapee.rc.IFileTreePageViewHyperslice|typeof xyz.swapee.rc.FileTreePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.FileTreeImpl}
 */
xyz.swapee.rc.AbstractFileTreeImpl.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IFileTreePageAspects&xyz.swapee.rc.IFileTreePage&xyz.swapee.rc.IFileTreePageHyperslice&xyz.swapee.rc.IFileTreePageViewHyperslice)} xyz.swapee.rc.IFileTreeImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageHyperslice} xyz.swapee.rc.IFileTreePageHyperslice.typeof */
/** @typedef {typeof xyz.swapee.rc.IFileTreePageViewHyperslice} xyz.swapee.rc.IFileTreePageViewHyperslice.typeof */
/** @interface xyz.swapee.rc.IFileTreeImpl */
xyz.swapee.rc.IFileTreeImpl = class extends /** @type {xyz.swapee.rc.IFileTreeImpl.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IFileTreePageAspects.typeof&xyz.swapee.rc.IFileTreePage.typeof&xyz.swapee.rc.IFileTreePageHyperslice.typeof&xyz.swapee.rc.IFileTreePageViewHyperslice.typeof} */ (class {}) {
}
/**
 * Create a new *IFileTreeImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreeImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IFileTreeImpl.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IFileTreeImpl&engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreeImpl.Initialese>)} xyz.swapee.rc.FileTreeImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IFileTreeImpl} xyz.swapee.rc.IFileTreeImpl.typeof */
/**
 * A concrete class of _IFileTreeImpl_ instances.
 * @constructor xyz.swapee.rc.FileTreeImpl
 * @implements {xyz.swapee.rc.IFileTreeImpl} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IFileTreeImpl.Initialese>} ‎
 */
xyz.swapee.rc.FileTreeImpl = class extends /** @type {xyz.swapee.rc.FileTreeImpl.constructor&xyz.swapee.rc.IFileTreeImpl.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileTreeImpl* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IFileTreeImpl.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileTreeImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IFileTreeImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.FileTreeImpl.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.FileTreeImpl}
 */
xyz.swapee.rc.FileTreeImpl.__extend = function(...Extensions) {}

/** @typedef {typeof xyz.swapee.rc.FileTree.fileTreeNav} */
/**
 * Navigates to `fileTree`.
 * @param {!xyz.swapee.rc.IFileTreePage.fileTree.Form} form
 * - `locale` _string_
 * @return {void}
 */
xyz.swapee.rc.FileTree.fileTreeNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.FileTree.filterFilesNav} */
/**
 * Invokes the `filterFiles` action.
 * @param {!xyz.swapee.rc.IFileTreePage.filterFiles.Form} form
 * - `root` _&#42;_
 * - `folder` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.FileTree.filterFilesNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.getFileTree} */
/**
 * Allows to render the pagelet after it's been processed.
 * @return {void}
 */
xyz.swapee.rc.getFileTree = function() {}

/** @typedef {typeof xyz.swapee.rc.fileTree} */
/**
 * @param {!xyz.swapee.rc.IFileTreeImpl} implementation
 * @return {!xyz.swapee.rc.IFileTreePage}
 */
xyz.swapee.rc.fileTree = function(implementation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.FileTree
/* @typal-end */