import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreePageViewAspectsInstaller}
 */
function __FileTreePageViewAspectsInstaller() {}
__FileTreePageViewAspectsInstaller.prototype = /** @type {!_FileTreePageViewAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller}
 */
class _FileTreePageViewAspectsInstaller { }

_FileTreePageViewAspectsInstaller.prototype[$advice]=__FileTreePageViewAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller} ‎ */
class FileTreePageViewAspectsInstaller extends newAbstract(
 _FileTreePageViewAspectsInstaller,'IFileTreePageViewAspectsInstaller',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller} */
FileTreePageViewAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileTreePageViewAspectsInstaller} */
function FileTreePageViewAspectsInstallerClass(){}

export default FileTreePageViewAspectsInstaller


FileTreePageViewAspectsInstaller[$implementations]=[
 FileTreePageViewAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IFileTreePageViewAspectsInstaller}*/({
  viewFileTree(){
   this.beforeViewFileTree=1
   this.afterViewFileTree=2
   this.aroundViewFileTree=3
   this.afterViewFileTreeThrows=4
   this.afterViewFileTreeReturns=5
   this.afterViewFileTreeCancels=7
   this.beforeEachViewFileTree=8
   this.afterEachViewFileTree=9
   this.afterEachViewFileTreeReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  getFileTree(){
   this.beforeGetFileTree=1
   this.afterGetFileTree=2
   this.aroundGetFileTree=3
   this.afterGetFileTreeThrows=4
   this.afterGetFileTreeReturns=5
   this.afterGetFileTreeCancels=7
   this.beforeEachGetFileTree=8
   this.afterEachGetFileTree=9
   this.afterEachGetFileTreeReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  setFileTreeCtx(){
   this.beforeSetFileTreeCtx=1
   this.afterSetFileTreeCtx=2
   this.aroundSetFileTreeCtx=3
   this.afterSetFileTreeCtxThrows=4
   this.afterSetFileTreeCtxReturns=5
   this.afterSetFileTreeCtxCancels=7
   this.beforeEachSetFileTreeCtx=8
   this.afterEachSetFileTreeCtx=9
   this.afterEachSetFileTreeCtxReturns=10
   return {
    answers:1,
    translation:2,
   }
  },
  FileTreePartial(){
   this.before_FileTreePartial=1
   this.after_FileTreePartial=2
   this.around_FileTreePartial=3
   this.after_FileTreePartialThrows=4
   this.after_FileTreePartialReturns=5
   this.after_FileTreePartialCancels=7
   return {
    answers:1,
    errors:2,
    actions:3,
    translation:4,
   }
  },
  FileTreeView(){
   this.before_FileTreeView=1
   this.after_FileTreeView=2
   this.around_FileTreeView=3
   this.after_FileTreeViewThrows=4
   this.after_FileTreeViewReturns=5
   this.after_FileTreeViewCancels=7
   this.beforeEach_FileTreeView=8
   this.afterEach_FileTreeView=9
   this.afterEach_FileTreeViewReturns=10
  },
 }),
 __FileTreePageViewAspectsInstaller,
]