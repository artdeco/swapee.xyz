import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreePageUniversal}
 */
function __FileTreePageUniversal() {}
__FileTreePageUniversal.prototype = /** @type {!_FileTreePageUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePageUniversal}
 */
class _FileTreePageUniversal { }
/**
 * A trait that allows to access an _IFileTreePage_ object via a `.fileTreePage` field.
 * @extends {xyz.swapee.rc.AbstractFileTreePageUniversal} ‎
 */
class FileTreePageUniversal extends newAbstract(
 _FileTreePageUniversal,'UFileTreePage',null,{
  asIFileTreePage:1,
  asUFileTreePage:3,
  fileTreePage:4,
  asFileTreePage:5,
  superFileTreePageUniversal:2,
 },false,{
  fileTreePage:{_fileTreePage:'_fileTreePage'},
 }) {}

/** @type {typeof xyz.swapee.rc.UFileTreePage} */
FileTreePageUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UFileTreePage} */
function FileTreePageUniversalClass(){}

export default FileTreePageUniversal

/** @type {xyz.swapee.rc.UFileTreePage.Symbols} */
export const FileTreePageUniversalSymbols=FileTreePageUniversal.Symbols
/** @type {xyz.swapee.rc.UFileTreePage.getSymbols} */
export const getFileTreePageUniversalSymbols=FileTreePageUniversal.getSymbols
/** @type {xyz.swapee.rc.UFileTreePage.setSymbols} */
export const setFileTreePageUniversalSymbols=FileTreePageUniversal.setSymbols

FileTreePageUniversal[$implementations]=[
 __FileTreePageUniversal,
 FileTreePageUniversalClass.prototype=/**@type {!xyz.swapee.rc.UFileTreePage}*/({
  [FileTreePageUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UFileTreePage} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UFileTreePage.Initialese}*/({
   fileTreePage:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.FileTreePageMetaUniversal}*/
export const FileTreePageMetaUniversal=FileTreePageUniversal.MetaUniversal