import FileTreePageMethodsIds from '../../../gen/methods-ids'
import '../../../types'

const methodsIds=Object.values(FileTreePageMethodsIds)

/**@type {xyz.swapee.rc.IFileTreePageView._getFileTree} */
export default function _getFileTree(ctx,form) {
 const actionId=parseFloat(form['action'])
 if(methodsIds.includes(actionId)) {
  const{asFileTreePageView:{viewFileTree:viewFileTree}}=this
  const ev=viewFileTree(ctx,form)
  return ev
 }
}