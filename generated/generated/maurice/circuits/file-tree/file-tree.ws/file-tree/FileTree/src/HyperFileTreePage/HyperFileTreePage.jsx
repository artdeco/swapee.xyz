import '../../types'
import filterFiles from '../../../../../../../../../maurice/circuits/file-tree/FileTree.mvc/src/FileTreeService/methods/filterFiles/filter-files'
import {FileTreeCachePQs} from '../../../../../../../../../maurice/circuits/file-tree/FileTree.mvc/pqs/FileTreeCachePQs'
import {FileTreeMemoryPQs} from '../../../../../../../../../maurice/circuits/file-tree/FileTree.mvc/pqs/FileTreeMemoryPQs'
import AbstractHyperFileTreePage from '../../gen/hyper/AbstractHyperFileTreePage'
import HyperFileTreePageView from '../HyperFileTreePageView'
import {AssignAnswers} from '@websystems/parthenon'
import AbstractFileTreePage from '../../gen/AbstractFileTreePage'
import AbstractFileTreePageAspects from '../../gen/aspects/AbstractFileTreePageAspects'
import FileTreePageMethodsIds from '../../gen/methods-ids'

/** @extends {xyz.swapee.rc.HyperFileTreePage}  */
export default class HyperFileTreePage extends AbstractHyperFileTreePage.consults(
 HyperFileTreePageView,
 AbstractFileTreePageAspects.class.prototype=/**@type {!xyz.swapee.rc.FileTreePageAspects} */({
  afterEachFilterFilesReturns:[
   AssignAnswers,
  ],
  afterEachFileTreeReturns:[
   AssignAnswers,
  ],
 }),
 // todo: add HyperView here
 AbstractFileTreePageAspects.class.prototype=/**@type {!xyz.swapee.rc.FileTreePageAspects} */({ 
  afterFilterFiles:[
   function({args:{ctx:ctx,answers:answers}}){
    const body={}
    for(const key in answers){
     const q=PQs[key]
     body[q]=answers[key]
    }
    ctx.body={data:body}
   },
  ],
 }),
).implements(
 AbstractFileTreePage,
 AbstractHyperFileTreePage.class.prototype=/**@type {!xyz.swapee.rc.FileTreePageHyperslice} */({
  get _methodIds(){return FileTreePageMethodsIds},
 }),
 AbstractHyperFileTreePage.class.prototype=/**@type {!xyz.swapee.rc.FileTreePageHyperslice} */({
  filterFiles:[
   filterFiles,
  ],
 }),
){}

const PQs={...FileTreeCachePQs,...FileTreeMemoryPQs}