import FileTreePageViewAspectsInstaller from '../../aspects-installers/FileTreePageViewAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperFileTreePageView}
 */
function __AbstractHyperFileTreePageView() {}
__AbstractHyperFileTreePageView.prototype = /** @type {!_AbstractHyperFileTreePageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperFileTreePageView}
 */
class _AbstractHyperFileTreePageView {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperFileTreePageView
    .clone({aspectsInstaller:FileTreePageViewAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.rc.AbstractHyperFileTreePageView} ‎ */
class AbstractHyperFileTreePageView extends newAbstract(
 _AbstractHyperFileTreePageView,'IHyperFileTreePageView',null,{
  asIHyperFileTreePageView:1,
  superHyperFileTreePageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperFileTreePageView} */
AbstractHyperFileTreePageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperFileTreePageView} */
function AbstractHyperFileTreePageViewClass(){}

export default AbstractHyperFileTreePageView


AbstractHyperFileTreePageView[$implementations]=[
 __AbstractHyperFileTreePageView,
]