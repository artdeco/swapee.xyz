import FileTreePageUniversal, {setFileTreePageUniversalSymbols} from './anchors/FileTreePageUniversal'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreePage}
 */
function __AbstractFileTreePage() {}
__AbstractFileTreePage.prototype = /** @type {!_AbstractFileTreePage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePage}
 */
class _AbstractFileTreePage { }
/** @extends {xyz.swapee.rc.AbstractFileTreePage} ‎ */
class AbstractFileTreePage extends newAbstract(
 _AbstractFileTreePage,'IFileTreePage',null,{
  asIFileTreePage:1,
  superFileTreePage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileTreePage} */
AbstractFileTreePage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileTreePage} */
function AbstractFileTreePageClass(){}

export default AbstractFileTreePage


AbstractFileTreePage[$implementations]=[
 __AbstractFileTreePage,
 FileTreePageUniversal,
 AbstractFileTreePageClass.prototype=/**@type {!xyz.swapee.rc.IFileTreePage}*/({
  fileTree:precombined,
  filterFiles:precombined,
 }),
]