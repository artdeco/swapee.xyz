/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.rc.IFileTreePageJoinpointModelHyperslice': {
  'id': 20384354731,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageJoinpointModelBindingHyperslice': {
  'id': 20384354732,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageJoinpointModel': {
  'id': 20384354733,
  'symbols': {},
  'methods': {
   'beforeFileTree': 1,
   'afterFileTree': 2,
   'afterFileTreeThrows': 3,
   'afterFileTreeReturns': 4,
   'afterFileTreeCancels': 5,
   'beforeEachFileTree': 6,
   'afterEachFileTree': 7,
   'afterEachFileTreeReturns': 8,
   'beforeFilterFiles': 9,
   'afterFilterFiles': 10,
   'afterFilterFilesThrows': 11,
   'afterFilterFilesReturns': 12,
   'afterFilterFilesCancels': 13,
   'beforeEachFilterFiles': 14,
   'afterEachFilterFiles': 15,
   'afterEachFilterFilesReturns': 16
  }
 },
 'xyz.swapee.rc.IFileTreePageAspectsInstaller': {
  'id': 20384354734,
  'symbols': {},
  'methods': {
   'fileTree': 1,
   'filterFiles': 2
  }
 },
 'xyz.swapee.rc.UFileTreePage': {
  'id': 20384354735,
  'symbols': {
   'fileTreePage': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BFileTreePageAspects': {
  'id': 20384354736,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IFileTreePageAspects': {
  'id': 20384354737,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperFileTreePage': {
  'id': 20384354738,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageHyperslice': {
  'id': 20384354739,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageBindingHyperslice': {
  'id': 203843547310,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePage': {
  'id': 203843547311,
  'symbols': {},
  'methods': {
   'fileTree': 1,
   'filterFiles': 2
  }
 },
 'xyz.swapee.rc.IFileTreePageViewJoinpointModelHyperslice': {
  'id': 203843547312,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageViewJoinpointModelBindingHyperslice': {
  'id': 203843547313,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageViewJoinpointModel': {
  'id': 203843547314,
  'symbols': {},
  'methods': {
   'beforeViewFileTree': 1,
   'afterViewFileTree': 2,
   'afterViewFileTreeThrows': 3,
   'afterViewFileTreeReturns': 4,
   'afterViewFileTreeCancels': 5,
   'beforeEachViewFileTree': 6,
   'afterEachViewFileTree': 7,
   'afterEachViewFileTreeReturns': 8,
   'beforeGetFileTree': 9,
   'afterGetFileTree': 10,
   'afterGetFileTreeThrows': 11,
   'afterGetFileTreeReturns': 12,
   'afterGetFileTreeCancels': 13,
   'beforeEachGetFileTree': 14,
   'afterEachGetFileTree': 15,
   'afterEachGetFileTreeReturns': 16,
   'beforeSetFileTreeCtx': 17,
   'afterSetFileTreeCtx': 18,
   'afterSetFileTreeCtxThrows': 19,
   'afterSetFileTreeCtxReturns': 20,
   'afterSetFileTreeCtxCancels': 21,
   'beforeEachSetFileTreeCtx': 22,
   'afterEachSetFileTreeCtx': 23,
   'afterEachSetFileTreeCtxReturns': 24,
   'before_FileTreePartial': 25,
   'after_FileTreePartial': 26,
   'after_FileTreePartialThrows': 27,
   'after_FileTreePartialReturns': 28,
   'after_FileTreePartialCancels': 29,
   'before_FileTreeView': 30,
   'after_FileTreeView': 31,
   'after_FileTreeViewThrows': 32,
   'after_FileTreeViewReturns': 33,
   'after_FileTreeViewCancels': 34,
   'beforeEach_FileTreeView': 35,
   'afterEach_FileTreeView': 36,
   'afterEach_FileTreeViewReturns': 37
  }
 },
 'xyz.swapee.rc.IFileTreePageViewAspectsInstaller': {
  'id': 203843547315,
  'symbols': {},
  'methods': {
   'viewFileTree': 1,
   'getFileTree': 2,
   'setFileTreeCtx': 3,
   'FileTreePartial': 4,
   'FileTreeView': 5
  }
 },
 'xyz.swapee.rc.UFileTreePageView': {
  'id': 203843547316,
  'symbols': {
   'fileTreePageView': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BFileTreePageViewAspects': {
  'id': 203843547317,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IFileTreePageViewAspects': {
  'id': 203843547318,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperFileTreePageView': {
  'id': 203843547319,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageViewHyperslice': {
  'id': 203843547320,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageViewBindingHyperslice': {
  'id': 203843547321,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IFileTreePageView': {
  'id': 203843547322,
  'symbols': {},
  'methods': {
   'viewFileTree': 1,
   'getFileTree': 2,
   'setFileTreeCtx': 3,
   'FileTreePartial': 4,
   'FileTreeView': 5
  }
 },
 'xyz.swapee.rc.IFileTreeImpl': {
  'id': 203843547323,
  'symbols': {},
  'methods': {}
 }
})