import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreePageViewAspects}
 */
function __AbstractFileTreePageViewAspects() {}
__AbstractFileTreePageViewAspects.prototype = /** @type {!_AbstractFileTreePageViewAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePageViewAspects}
 */
class _AbstractFileTreePageViewAspects { }
/**
 * The aspects of the *IFileTreePageView*.
 * @extends {xyz.swapee.rc.AbstractFileTreePageViewAspects} ‎
 */
class AbstractFileTreePageViewAspects extends newAspects(
 _AbstractFileTreePageViewAspects,'IFileTreePageViewAspects',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileTreePageViewAspects} */
AbstractFileTreePageViewAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileTreePageViewAspects} */
function AbstractFileTreePageViewAspectsClass(){}

export default AbstractFileTreePageViewAspects


AbstractFileTreePageViewAspects[$implementations]=[
 __AbstractFileTreePageViewAspects,
]