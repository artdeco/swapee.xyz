import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreePageAspectsInstaller}
 */
function __FileTreePageAspectsInstaller() {}
__FileTreePageAspectsInstaller.prototype = /** @type {!_FileTreePageAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePageAspectsInstaller}
 */
class _FileTreePageAspectsInstaller { }

_FileTreePageAspectsInstaller.prototype[$advice]=__FileTreePageAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractFileTreePageAspectsInstaller} ‎ */
class FileTreePageAspectsInstaller extends newAbstract(
 _FileTreePageAspectsInstaller,'IFileTreePageAspectsInstaller',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileTreePageAspectsInstaller} */
FileTreePageAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileTreePageAspectsInstaller} */
function FileTreePageAspectsInstallerClass(){}

export default FileTreePageAspectsInstaller


FileTreePageAspectsInstaller[$implementations]=[
 FileTreePageAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IFileTreePageAspectsInstaller}*/({
  fileTree(){
   this.beforeFileTree=1
   this.afterFileTree=2
   this.aroundFileTree=3
   this.afterFileTreeThrows=4
   this.afterFileTreeReturns=5
   this.afterFileTreeCancels=7
   this.beforeEachFileTree=8
   this.afterEachFileTree=9
   this.afterEachFileTreeReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterFiles(){
   this.beforeFilterFiles=1
   this.afterFilterFiles=2
   this.aroundFilterFiles=3
   this.afterFilterFilesThrows=4
   this.afterFilterFilesReturns=5
   this.afterFilterFilesCancels=7
   this.beforeEachFilterFiles=8
   this.afterEachFilterFiles=9
   this.afterEachFilterFilesReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
 }),
 __FileTreePageAspectsInstaller,
]