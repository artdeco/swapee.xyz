import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileTreePageViewUniversal}
 */
function __FileTreePageViewUniversal() {}
__FileTreePageViewUniversal.prototype = /** @type {!_FileTreePageViewUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePageViewUniversal}
 */
class _FileTreePageViewUniversal { }
/**
 * A trait that allows to access an _IFileTreePageView_ object via a `.fileTreePageView` field.
 * @extends {xyz.swapee.rc.AbstractFileTreePageViewUniversal} ‎
 */
class FileTreePageViewUniversal extends newAbstract(
 _FileTreePageViewUniversal,'UFileTreePageView',null,{
  asIFileTreePageView:1,
  asUFileTreePageView:3,
  fileTreePageView:4,
  asFileTreePageView:5,
  superFileTreePageViewUniversal:2,
 },false,{
  fileTreePageView:{_fileTreePageView:'_fileTreePageView'},
 }) {}

/** @type {typeof xyz.swapee.rc.UFileTreePageView} */
FileTreePageViewUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UFileTreePageView} */
function FileTreePageViewUniversalClass(){}

export default FileTreePageViewUniversal

/** @type {xyz.swapee.rc.UFileTreePageView.Symbols} */
export const FileTreePageViewUniversalSymbols=FileTreePageViewUniversal.Symbols
/** @type {xyz.swapee.rc.UFileTreePageView.getSymbols} */
export const getFileTreePageViewUniversalSymbols=FileTreePageViewUniversal.getSymbols
/** @type {xyz.swapee.rc.UFileTreePageView.setSymbols} */
export const setFileTreePageViewUniversalSymbols=FileTreePageViewUniversal.setSymbols

FileTreePageViewUniversal[$implementations]=[
 __FileTreePageViewUniversal,
 FileTreePageViewUniversalClass.prototype=/**@type {!xyz.swapee.rc.UFileTreePageView}*/({
  [FileTreePageViewUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UFileTreePageView} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UFileTreePageView.Initialese}*/({
   fileTreePageView:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.FileTreePageViewMetaUniversal}*/
export const FileTreePageViewMetaUniversal=FileTreePageViewUniversal.MetaUniversal