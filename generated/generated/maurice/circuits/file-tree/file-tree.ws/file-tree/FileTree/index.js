require('./types')
module.exports={
 toString(){return 'fileTree'},
 get FileTreePage(){
  const d=Date.now();
  const R=require('./src/HyperFileTreePage');
  console.log("✅ Loaded dev %s in %s",'FileTree',Date.now()-d,'ms');
  return R
 },
 get fileTreeMethodsIds() {
  const methodsIds={}
  const o=require('./gen/methods-ids')
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') methodsIds[key]=val
  }
  return methodsIds
 },
 get fileTreeArcsIds() {
  const arcsIds={}
  let o={}
  try{o=require('./gen/arcs-ids')}catch(err){}
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') arcsIds[val]=key
  }
  return arcsIds
 },
 get getFileTree(){
  const GET=require('./src/api/getFileTree/getFileTree')
  return GET
 },
 get FileTreeNav() {
  return require('./src/navs/fileTree')
 },
 get FilterFilesNav() {
  return require('./src/navs/filterFiles')
 },
}