import FileTreePageAspectsInstaller from '../../aspects-installers/FileTreePageAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperFileTreePage}
 */
function __AbstractHyperFileTreePage() {}
__AbstractHyperFileTreePage.prototype = /** @type {!_AbstractHyperFileTreePage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperFileTreePage}
 */
class _AbstractHyperFileTreePage {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperFileTreePage
    .clone({aspectsInstaller:FileTreePageAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.rc.AbstractHyperFileTreePage} ‎ */
class AbstractHyperFileTreePage extends newAbstract(
 _AbstractHyperFileTreePage,'IHyperFileTreePage',null,{
  asIHyperFileTreePage:1,
  superHyperFileTreePage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperFileTreePage} */
AbstractHyperFileTreePage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperFileTreePage} */
function AbstractHyperFileTreePageClass(){}

export default AbstractHyperFileTreePage


AbstractHyperFileTreePage[$implementations]=[
 __AbstractHyperFileTreePage,
]