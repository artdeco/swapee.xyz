import methodsIds from '../../../gen/methods-ids'
/** @type {xyz.swapee.rc.FileTree.FileTreeNav} */
export const FileTreeNav={
 toString(){return'fileTree'},
 fileTree:methodsIds.fileTree,
}
export default FileTreeNav