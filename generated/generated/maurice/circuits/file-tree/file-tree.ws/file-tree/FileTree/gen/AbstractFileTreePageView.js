import FileTreePageViewUniversal, {setFileTreePageViewUniversalSymbols} from './anchors/FileTreePageViewUniversal'
import { newAbstract, $implementations, prereturnFirst, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileTreePageView}
 */
function __AbstractFileTreePageView() {}
__AbstractFileTreePageView.prototype = /** @type {!_AbstractFileTreePageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractFileTreePageView}
 */
class _AbstractFileTreePageView { }
/** @extends {xyz.swapee.rc.AbstractFileTreePageView} ‎ */
class AbstractFileTreePageView extends newAbstract(
 _AbstractFileTreePageView,'IFileTreePageView',null,{
  asIFileTreePageView:1,
  superFileTreePageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractFileTreePageView} */
AbstractFileTreePageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractFileTreePageView} */
function AbstractFileTreePageViewClass(){}

export default AbstractFileTreePageView


AbstractFileTreePageView[$implementations]=[
 __AbstractFileTreePageView,
 FileTreePageViewUniversal,
 AbstractFileTreePageViewClass.prototype=/**@type {!xyz.swapee.rc.IFileTreePageView}*/({
  viewFileTree:prereturnFirst,
  getFileTree:precombined,
  setFileTreeCtx:precombined,
  FileTreeView:prereturnFirst,
 }),
]