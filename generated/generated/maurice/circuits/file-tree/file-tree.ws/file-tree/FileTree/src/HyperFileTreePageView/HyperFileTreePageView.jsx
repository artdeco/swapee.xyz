import '../../types'
import FileTreePageView from '../FileTreePageView'
import AbstractHyperFileTreePageView from '../../gen/hyper/AbstractHyperFileTreePageView'
import {AssignLocale} from '@websystems/parthenon'
import FileTreePageArcsIds from '../../gen/arcs-ids'
import FileTreePageMethodsIds from '../../gen/methods-ids'
import {RenderAide} from '@idio2/server'
import AbstractFileTreePageViewAspects from '../../gen/aspects/AbstractFileTreePageViewAspects'

/** @extends {xyz.swapee.rc.HyperFileTreePageView}  */
export default class HyperFileTreePageView extends AbstractHyperFileTreePageView.consults(
 AbstractFileTreePageViewAspects.class.prototype=/**@type {!xyz.swapee.rc.FileTreePageViewAspects} */({
  beforeFileTree:[
   AssignLocale,
  ],
  afterEachSetFileTreeCtxReturns:[
   function({res:res}) {
    const{asIFileTreePage:{asCContext:ctx}}=this
    Object.assign(ctx,res)
   },
  ],
  afterViewFileTreeReturns:[
   RenderAide.RenderActionableJSXCtx({
    ...FileTreePageMethodsIds,
   },{
    attrsProcess:[
    ],
    arcs:FileTreePageArcsIds,
   },{
    fileTreeTranslations:1,
   }),
  ],
 }),
).implements(
 FileTreePageView,
){}