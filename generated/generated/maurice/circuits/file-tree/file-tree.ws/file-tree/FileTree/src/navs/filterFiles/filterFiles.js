import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.FileTree.FilterFilesNav} */
export const FilterFilesNav={
 toString(){return'filterFiles'},
 filterFiles:methodsIds.filterFiles,
}
export default FilterFilesNav