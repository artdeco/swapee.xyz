require('@type.engineering/type-engineer/types/typedefs')
//require('./types/typology')
require('./types/typedefs')

/**
@license
@LICENSE @xyz.swapee.rc/file-tree (c) by X 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @extends {xyz.swapee.rc.FileTreePage} */
class FileTreePage extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the methods.
 * @extends {xyz.swapee.rc.fileTreeMethodsIds}
 */
class fileTreeMethodsIds extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the arcs.
 * @extends {xyz.swapee.rc.fileTreeArcsIds}
 */
class fileTreeArcsIds extends (class {/* lazy-loaded */}) {}
/**
 * Allows to render the pagelet after it's been processed.
 */
function getFileTree() {
  // lazy-loaded()
}
/** @extends {xyz.swapee.rc.FileTreePageView} */
class FileTreePageView extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.rc.HyperFileTreePageView} */
class HyperFileTreePageView extends (class {/* lazy-loaded */}) {}
/**
 * The `fileTree` navigation metadata.
 * @extends {xyz.swapee.rc.FileTree.FileTreeNav}
 */
class FileTreeNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterFiles` navigation metadata.
 * @extends {xyz.swapee.rc.FileTree.FilterFilesNav}
 */
class FilterFilesNav extends (class {/* lazy-loaded */}) {}

module.exports.FileTreePage = FileTreePage
module.exports.fileTreeMethodsIds = fileTreeMethodsIds
module.exports.fileTreeArcsIds = fileTreeArcsIds
module.exports.getFileTree = getFileTree
module.exports.FileTreePageView = FileTreePageView
module.exports.HyperFileTreePageView = HyperFileTreePageView
module.exports.FileTreeNav = FileTreeNav
module.exports.FilterFilesNav = FilterFilesNav

Object.defineProperties(module.exports, {
 'FileTreePage': {get: () => require('./compile')[20384354731]},
 [20384354731]: {get: () => module.exports['FileTreePage']},
 'fileTreeMethodsIds': {get: () => require('./compile')[20384354732]},
 [20384354732]: {get: () => module.exports['fileTreeMethodsIds']},
 'fileTreeArcsIds': {get: () => require('./compile')[20384354733]},
 [20384354733]: {get: () => module.exports['fileTreeArcsIds']},
 'getFileTree': {get: () => require('./compile')[20384354734]},
 [20384354734]: {get: () => module.exports['getFileTree']},
 'FileTreePageView': {get: () => require('./compile')[203843547310]},
 [203843547310]: {get: () => module.exports['FileTreePageView']},
 'HyperFileTreePageView': {get: () => require('./compile')[203843547313]},
 [203843547313]: {get: () => module.exports['HyperFileTreePageView']},
 'FileTreeNav': {get: () => require('./compile')[2038435473100]},
 [2038435473100]: {get: () => module.exports['FileTreeNav']},
 'FilterFilesNav': {get: () => require('./compile')[2038435473101]},
 [2038435473101]: {get: () => module.exports['FilterFilesNav']},
})