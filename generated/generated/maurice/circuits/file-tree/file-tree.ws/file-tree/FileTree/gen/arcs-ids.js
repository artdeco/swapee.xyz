const FileTreePageArcsIds={
 locale:8988601,
 root:3674914,
 folder:5793544,
}
export const FileTreePageLiteralArcsIds={
 'locale':8988601,
 'root':3674914,
 'folder':5793544,
}
export const ReverseFileTreePageArcsIds=Object.keys(FileTreePageArcsIds)
 .reduce((a,k)=>{a[FileTreePageArcsIds[`${k}`]]=k;return a},{})

export default FileTreePageArcsIds