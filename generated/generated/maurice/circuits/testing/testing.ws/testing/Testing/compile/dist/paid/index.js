require('@type.engineering/type-engineer/types/typedefs')
//require('./types/typology')
require('./types/typedefs')

/**
@license
@LICENSE @xyz.swapee.rc/testing (c) by X 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @extends {xyz.swapee.rc.TestingPage} */
class TestingPage extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the methods.
 * @extends {xyz.swapee.rc.testingMethodsIds}
 */
class testingMethodsIds extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the arcs.
 * @extends {xyz.swapee.rc.testingArcsIds}
 */
class testingArcsIds extends (class {/* lazy-loaded */}) {}
/**
 * Allows to render the pagelet after it's been processed.
 */
function getTesting() {
  // lazy-loaded()
}
/** @extends {xyz.swapee.rc.TestingPageView} */
class TestingPageView extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.rc.HyperTestingPageView} */
class HyperTestingPageView extends (class {/* lazy-loaded */}) {}
/**
 * The `testing` navigation metadata.
 * @extends {xyz.swapee.rc.Testing.TestingNav}
 */
class TestingNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterChangellyFloatingOffer` navigation metadata.
 * @extends {xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav}
 */
class FilterChangellyFloatingOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterChangellyFixedOffer` navigation metadata.
 * @extends {xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav}
 */
class FilterChangellyFixedOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterChangenowOffer` navigation metadata.
 * @extends {xyz.swapee.rc.Testing.FilterChangenowOfferNav}
 */
class FilterChangenowOfferNav extends (class {/* lazy-loaded */}) {}

module.exports.TestingPage = TestingPage
module.exports.testingMethodsIds = testingMethodsIds
module.exports.testingArcsIds = testingArcsIds
module.exports.getTesting = getTesting
module.exports.TestingPageView = TestingPageView
module.exports.HyperTestingPageView = HyperTestingPageView
module.exports.TestingNav = TestingNav
module.exports.FilterChangellyFloatingOfferNav = FilterChangellyFloatingOfferNav
module.exports.FilterChangellyFixedOfferNav = FilterChangellyFixedOfferNav
module.exports.FilterChangenowOfferNav = FilterChangenowOfferNav

Object.defineProperties(module.exports, {
 'TestingPage': {get: () => require('./compile')[21743047451]},
 [21743047451]: {get: () => module.exports['TestingPage']},
 'testingMethodsIds': {get: () => require('./compile')[21743047452]},
 [21743047452]: {get: () => module.exports['testingMethodsIds']},
 'testingArcsIds': {get: () => require('./compile')[21743047453]},
 [21743047453]: {get: () => module.exports['testingArcsIds']},
 'getTesting': {get: () => require('./compile')[21743047454]},
 [21743047454]: {get: () => module.exports['getTesting']},
 'TestingPageView': {get: () => require('./compile')[217430474510]},
 [217430474510]: {get: () => module.exports['TestingPageView']},
 'HyperTestingPageView': {get: () => require('./compile')[217430474513]},
 [217430474513]: {get: () => module.exports['HyperTestingPageView']},
 'TestingNav': {get: () => require('./compile')[2174304745100]},
 [2174304745100]: {get: () => module.exports['TestingNav']},
 'FilterChangellyFloatingOfferNav': {get: () => require('./compile')[2174304745101]},
 [2174304745101]: {get: () => module.exports['FilterChangellyFloatingOfferNav']},
 'FilterChangellyFixedOfferNav': {get: () => require('./compile')[2174304745102]},
 [2174304745102]: {get: () => module.exports['FilterChangellyFixedOfferNav']},
 'FilterChangenowOfferNav': {get: () => require('./compile')[2174304745103]},
 [2174304745103]: {get: () => module.exports['FilterChangenowOfferNav']},
})