import TestingPageViewAspectsInstaller from '../../aspects-installers/TestingPageViewAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperTestingPageView}
 */
function __AbstractHyperTestingPageView() {}
__AbstractHyperTestingPageView.prototype = /** @type {!_AbstractHyperTestingPageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperTestingPageView}
 */
class _AbstractHyperTestingPageView {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperTestingPageView
    .clone({aspectsInstaller:TestingPageViewAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.rc.AbstractHyperTestingPageView} ‎ */
class AbstractHyperTestingPageView extends newAbstract(
 _AbstractHyperTestingPageView,217430474519,null,{
  asIHyperTestingPageView:1,
  superHyperTestingPageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperTestingPageView} */
AbstractHyperTestingPageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperTestingPageView} */
function AbstractHyperTestingPageViewClass(){}

export default AbstractHyperTestingPageView


AbstractHyperTestingPageView[$implementations]=[
 __AbstractHyperTestingPageView,
]