require('./types')
module.exports={
 toString(){return 'testing'},
 get TestingPage(){
  const d=Date.now();
  const R=require('./src/HyperTestingPage');
  console.log("✅ Loaded dev %s in %s",'Testing',Date.now()-d,'ms');
  return R
 },
 get testingMethodsIds() {
  const methodsIds={}
  const o=require('./gen/methods-ids')
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') methodsIds[key]=val
  }
  return methodsIds
 },
 get testingArcsIds() {
  const arcsIds={}
  let o={}
  try{o=require('./gen/arcs-ids')}catch(err){}
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') arcsIds[val]=key
  }
  return arcsIds
 },
 get getTesting(){
  const GET=require('./src/api/getTesting/getTesting')
  return GET
 },
 get TestingNav() {
  return require('./src/navs/testing')
 },
 get FilterChangellyFloatingOfferNav() {
  return require('./src/navs/filterChangellyFloatingOffer')
 },
 get FilterChangellyFixedOfferNav() {
  return require('./src/navs/filterChangellyFixedOffer')
 },
 get FilterChangenowOfferNav() {
  return require('./src/navs/filterChangenowOffer')
 },
}