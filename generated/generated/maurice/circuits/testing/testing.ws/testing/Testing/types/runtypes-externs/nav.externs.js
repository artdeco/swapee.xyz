/**
 * @fileoverview
 * @externs
 */

/** @const */
var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.Form only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.testing.Form = function() {}
/** @type {string} */
xyz.swapee.rc.ITestingPage.testing.Form.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.amountIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.currencyIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.currencyOut

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.amountIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.currencyIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.currencyOut

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.changeNow
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.amountIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.currencyIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.currencyOut

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.testingNav only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Form): void} */
xyz.swapee.rc.Testing.testingNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form): void} */
xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.filterChangellyFixedOfferNav only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form): void} */
xyz.swapee.rc.Testing.filterChangellyFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.filterChangenowOfferNav only:Testing.testingNav,xyz.swapee.rc.ITestingPage.testing.Form,Testing.filterChangellyFloatingOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form,Testing.filterChangellyFixedOfferNav,xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form,Testing.filterChangenowOfferNav,xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form 9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form): void} */
xyz.swapee.rc.Testing.filterChangenowOfferNav

// nss:xyz.swapee.rc
/* @typal-end */