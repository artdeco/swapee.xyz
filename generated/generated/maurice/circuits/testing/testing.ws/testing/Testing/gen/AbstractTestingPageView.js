import TestingPageViewUniversal, {setTestingPageViewUniversalSymbols} from './anchors/TestingPageViewUniversal'
import { newAbstract, $implementations, prereturnFirst, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingPageView}
 */
function __AbstractTestingPageView() {}
__AbstractTestingPageView.prototype = /** @type {!_AbstractTestingPageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPageView}
 */
class _AbstractTestingPageView { }
/** @extends {xyz.swapee.rc.AbstractTestingPageView} ‎ */
class AbstractTestingPageView extends newAbstract(
 _AbstractTestingPageView,217430474522,null,{
  asITestingPageView:1,
  superTestingPageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractTestingPageView} */
AbstractTestingPageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractTestingPageView} */
function AbstractTestingPageViewClass(){}

export default AbstractTestingPageView


AbstractTestingPageView[$implementations]=[
 __AbstractTestingPageView,
 TestingPageViewUniversal,
 AbstractTestingPageViewClass.prototype=/**@type {!xyz.swapee.rc.ITestingPageView}*/({
  viewTesting:prereturnFirst,
  getTesting:precombined,
  setTestingCtx:precombined,
  TestingView:prereturnFirst,
 }),
]