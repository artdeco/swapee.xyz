import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav} */
export const FilterChangellyFloatingOfferNav={
 toString(){return'filterChangellyFloatingOffer'},
 filterChangellyFloatingOffer:methodsIds.filterChangellyFloatingOffer,
}
export default FilterChangellyFloatingOfferNav