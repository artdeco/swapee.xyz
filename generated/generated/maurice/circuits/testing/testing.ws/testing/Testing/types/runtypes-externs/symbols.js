/** @const {?} */ $xyz.swapee.rc.UTestingPage
/** @const {?} */ $xyz.swapee.rc.UTestingPageView
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPageUniversal.getSymbols filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @typedef {!xyz.swapee.rc.UTestingPage.getSymbols} */
xyz.swapee.rc.AbstractTestingPageUniversal.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPageUniversal.setSymbols filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @typedef {!xyz.swapee.rc.UTestingPage.setSymbols} */
xyz.swapee.rc.AbstractTestingPageUniversal.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPageUniversal.Symbols filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @typedef {!xyz.swapee.rc.UTestingPage.Symbols} */
xyz.swapee.rc.AbstractTestingPageUniversal.Symbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPage.SymbolsIn filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @record */
$xyz.swapee.rc.UTestingPage.SymbolsIn = function() {}
/** @type {xyz.swapee.rc.ITestingPage|undefined} */
$xyz.swapee.rc.UTestingPage.SymbolsIn.prototype._testingPage
/** @typedef {$xyz.swapee.rc.UTestingPage.SymbolsIn} */
xyz.swapee.rc.UTestingPage.SymbolsIn

// nss:xyz.swapee.rc,$xyz.swapee.rc.UTestingPage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPage.Symbols filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @record */
$xyz.swapee.rc.UTestingPage.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.rc.UTestingPage.Symbols.prototype._testingPage
/** @typedef {$xyz.swapee.rc.UTestingPage.Symbols} */
xyz.swapee.rc.UTestingPage.Symbols

// nss:xyz.swapee.rc,$xyz.swapee.rc.UTestingPage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPage.SymbolsOut filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @record */
$xyz.swapee.rc.UTestingPage.SymbolsOut = function() {}
/** @type {xyz.swapee.rc.ITestingPage} */
$xyz.swapee.rc.UTestingPage.SymbolsOut.prototype._testingPage
/** @typedef {$xyz.swapee.rc.UTestingPage.SymbolsOut} */
xyz.swapee.rc.UTestingPage.SymbolsOut

// nss:xyz.swapee.rc,$xyz.swapee.rc.UTestingPage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPage.setSymbols filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @typedef {function(!xyz.swapee.rc.UTestingPage, !xyz.swapee.rc.UTestingPage.SymbolsIn): void} */
xyz.swapee.rc.UTestingPage.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPage.getSymbols filter:Symbolism 633a5580f6b3c974cf3d903865e4048b */
/** @typedef {function(!xyz.swapee.rc.UTestingPage): !xyz.swapee.rc.UTestingPage.SymbolsOut} */
xyz.swapee.rc.UTestingPage.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageViewUniversal.getSymbols filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @typedef {!xyz.swapee.rc.UTestingPageView.getSymbols} */
xyz.swapee.rc.AbstractTestingPageViewUniversal.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageViewUniversal.setSymbols filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @typedef {!xyz.swapee.rc.UTestingPageView.setSymbols} */
xyz.swapee.rc.AbstractTestingPageViewUniversal.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageViewUniversal.Symbols filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @typedef {!xyz.swapee.rc.UTestingPageView.Symbols} */
xyz.swapee.rc.AbstractTestingPageViewUniversal.Symbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageView.SymbolsIn filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @record */
$xyz.swapee.rc.UTestingPageView.SymbolsIn = function() {}
/** @type {xyz.swapee.rc.ITestingPageView|undefined} */
$xyz.swapee.rc.UTestingPageView.SymbolsIn.prototype._testingPageView
/** @typedef {$xyz.swapee.rc.UTestingPageView.SymbolsIn} */
xyz.swapee.rc.UTestingPageView.SymbolsIn

// nss:xyz.swapee.rc,$xyz.swapee.rc.UTestingPageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageView.Symbols filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @record */
$xyz.swapee.rc.UTestingPageView.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.rc.UTestingPageView.Symbols.prototype._testingPageView
/** @typedef {$xyz.swapee.rc.UTestingPageView.Symbols} */
xyz.swapee.rc.UTestingPageView.Symbols

// nss:xyz.swapee.rc,$xyz.swapee.rc.UTestingPageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageView.SymbolsOut filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @record */
$xyz.swapee.rc.UTestingPageView.SymbolsOut = function() {}
/** @type {xyz.swapee.rc.ITestingPageView} */
$xyz.swapee.rc.UTestingPageView.SymbolsOut.prototype._testingPageView
/** @typedef {$xyz.swapee.rc.UTestingPageView.SymbolsOut} */
xyz.swapee.rc.UTestingPageView.SymbolsOut

// nss:xyz.swapee.rc,$xyz.swapee.rc.UTestingPageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageView.setSymbols filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @typedef {function(!xyz.swapee.rc.UTestingPageView, !xyz.swapee.rc.UTestingPageView.SymbolsIn): void} */
xyz.swapee.rc.UTestingPageView.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageView.getSymbols filter:Symbolism 9c94d7079915a315fadd7437cea05033 */
/** @typedef {function(!xyz.swapee.rc.UTestingPageView): !xyz.swapee.rc.UTestingPageView.SymbolsOut} */
xyz.swapee.rc.UTestingPageView.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */