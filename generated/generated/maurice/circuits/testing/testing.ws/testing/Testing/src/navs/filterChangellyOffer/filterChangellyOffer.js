import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.Testing.FilterChangellyOfferNav} */
export const FilterChangellyOfferNav={
 toString(){return'filterChangellyOffer'},
 filterChangellyOffer:methodsIds.filterChangellyOffer,
}
export default FilterChangellyOfferNav