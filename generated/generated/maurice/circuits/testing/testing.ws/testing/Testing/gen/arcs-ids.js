const TestingPageArcsIds={
 locale:4978097,
 amountIn:4169634,
 currencyIn:2620872,
 currencyOut:3239469,
 changeNow:2471896,
}
export const TestingPageLiteralArcsIds={
 'locale':4978097,
 'amountIn':4169634,
 'currencyIn':2620872,
 'currencyOut':3239469,
 'changeNow':2471896,
}
export const ReverseTestingPageArcsIds=Object.keys(TestingPageArcsIds)
 .reduce((a,k)=>{a[TestingPageArcsIds[`${k}`]]=k;return a},{})

export default TestingPageArcsIds