import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.Testing.FilterChangenowOfferNav} */
export const FilterChangenowOfferNav={
 toString(){return'filterChangenowOffer'},
 filterChangenowOffer:methodsIds.filterChangenowOffer,
}
export default FilterChangenowOfferNav