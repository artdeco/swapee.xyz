import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingPageViewAspectsInstaller}
 */
function __TestingPageViewAspectsInstaller() {}
__TestingPageViewAspectsInstaller.prototype = /** @type {!_TestingPageViewAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller}
 */
class _TestingPageViewAspectsInstaller { }

_TestingPageViewAspectsInstaller.prototype[$advice]=__TestingPageViewAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller} ‎ */
class TestingPageViewAspectsInstaller extends newAbstract(
 _TestingPageViewAspectsInstaller,217430474515,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller} */
TestingPageViewAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller} */
function TestingPageViewAspectsInstallerClass(){}

export default TestingPageViewAspectsInstaller


TestingPageViewAspectsInstaller[$implementations]=[
 TestingPageViewAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.ITestingPageViewAspectsInstaller}*/({
  viewTesting(){
   this.beforeViewTesting=1
   this.afterViewTesting=2
   this.aroundViewTesting=3
   this.afterViewTestingThrows=4
   this.afterViewTestingReturns=5
   this.afterViewTestingCancels=7
   this.beforeEachViewTesting=8
   this.afterEachViewTesting=9
   this.afterEachViewTestingReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  getTesting(){
   this.beforeGetTesting=1
   this.afterGetTesting=2
   this.aroundGetTesting=3
   this.afterGetTestingThrows=4
   this.afterGetTestingReturns=5
   this.afterGetTestingCancels=7
   this.beforeEachGetTesting=8
   this.afterEachGetTesting=9
   this.afterEachGetTestingReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  setTestingCtx(){
   this.beforeSetTestingCtx=1
   this.afterSetTestingCtx=2
   this.aroundSetTestingCtx=3
   this.afterSetTestingCtxThrows=4
   this.afterSetTestingCtxReturns=5
   this.afterSetTestingCtxCancels=7
   this.beforeEachSetTestingCtx=8
   this.afterEachSetTestingCtx=9
   this.afterEachSetTestingCtxReturns=10
   return {
    answers:1,
    translation:2,
   }
  },
  TestingPartial(){
   this.before_TestingPartial=1
   this.after_TestingPartial=2
   this.around_TestingPartial=3
   this.after_TestingPartialThrows=4
   this.after_TestingPartialReturns=5
   this.after_TestingPartialCancels=7
   return {
    answers:1,
    errors:2,
    actions:3,
    translation:4,
   }
  },
  TestingView(){
   this.before_TestingView=1
   this.after_TestingView=2
   this.around_TestingView=3
   this.after_TestingViewThrows=4
   this.after_TestingViewReturns=5
   this.after_TestingViewCancels=7
   this.beforeEach_TestingView=8
   this.afterEach_TestingView=9
   this.afterEach_TestingViewReturns=10
  },
 }),
 __TestingPageViewAspectsInstaller,
]