import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingPageUniversal}
 */
function __TestingPageUniversal() {}
__TestingPageUniversal.prototype = /** @type {!_TestingPageUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPageUniversal}
 */
class _TestingPageUniversal { }
/**
 * A trait that allows to access an _ITestingPage_ object via a `.testingPage` field.
 * @extends {xyz.swapee.rc.AbstractTestingPageUniversal} ‎
 */
class TestingPageUniversal extends newAbstract(
 _TestingPageUniversal,21743047455,null,{
  asITestingPage:1,
  asUTestingPage:3,
  testingPage:4,
  asTestingPage:5,
  superTestingPageUniversal:2,
 },false,{
  testingPage:{_testingPage:1},
 }) {}

/** @type {typeof xyz.swapee.rc.UTestingPage} */
TestingPageUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UTestingPage} */
function TestingPageUniversalClass(){}

export default TestingPageUniversal

/** @type {xyz.swapee.rc.UTestingPage.Symbols} */
export const TestingPageUniversalSymbols=TestingPageUniversal.Symbols
/** @type {xyz.swapee.rc.UTestingPage.getSymbols} */
export const getTestingPageUniversalSymbols=TestingPageUniversal.getSymbols
/** @type {xyz.swapee.rc.UTestingPage.setSymbols} */
export const setTestingPageUniversalSymbols=TestingPageUniversal.setSymbols

TestingPageUniversal[$implementations]=[
 __TestingPageUniversal,
 TestingPageUniversalClass.prototype=/**@type {!xyz.swapee.rc.UTestingPage}*/({
  [TestingPageUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UTestingPage} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UTestingPage.Initialese}*/({
   testingPage:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.TestingPageMetaUniversal}*/
export const TestingPageMetaUniversal=TestingPageUniversal.MetaUniversal