import TestingPageAspectsInstaller from '../../aspects-installers/TestingPageAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperTestingPage}
 */
function __AbstractHyperTestingPage() {}
__AbstractHyperTestingPage.prototype = /** @type {!_AbstractHyperTestingPage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperTestingPage}
 */
class _AbstractHyperTestingPage {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperTestingPage
    .clone({aspectsInstaller:TestingPageAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.rc.AbstractHyperTestingPage} ‎ */
class AbstractHyperTestingPage extends newAbstract(
 _AbstractHyperTestingPage,21743047458,null,{
  asIHyperTestingPage:1,
  superHyperTestingPage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperTestingPage} */
AbstractHyperTestingPage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperTestingPage} */
function AbstractHyperTestingPageClass(){}

export default AbstractHyperTestingPage


AbstractHyperTestingPage[$implementations]=[
 __AbstractHyperTestingPage,
]