import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingPageAspects}
 */
function __AbstractTestingPageAspects() {}
__AbstractTestingPageAspects.prototype = /** @type {!_AbstractTestingPageAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPageAspects}
 */
class _AbstractTestingPageAspects { }
/**
 * The aspects of the *ITestingPage*.
 * @extends {xyz.swapee.rc.AbstractTestingPageAspects} ‎
 */
class AbstractTestingPageAspects extends newAspects(
 _AbstractTestingPageAspects,21743047457,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractTestingPageAspects} */
AbstractTestingPageAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractTestingPageAspects} */
function AbstractTestingPageAspectsClass(){}

export default AbstractTestingPageAspects


AbstractTestingPageAspects[$implementations]=[
 __AbstractTestingPageAspects,
]