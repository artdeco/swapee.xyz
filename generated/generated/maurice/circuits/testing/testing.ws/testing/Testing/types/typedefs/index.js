/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
xyz.swapee.rc.ITestingPage={}
xyz.swapee.rc.ITestingPage.testing={}
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer={}
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer={}
xyz.swapee.rc.ITestingPage.filterChangenowOffer={}
xyz.swapee.rc.ITestingPageJoinpointModel={}
xyz.swapee.rc.ITestingPageAspectsInstaller={}
xyz.swapee.rc.UTestingPage={}
xyz.swapee.rc.AbstractTestingPageUniversal={}
xyz.swapee.rc.ITestingPageAspects={}
xyz.swapee.rc.IHyperTestingPage={}
xyz.swapee.rc.Testing={}
xyz.swapee.rc.Testing.Answers={}
xyz.swapee.rc.Testing.Form={}
xyz.swapee.rc.Testing.Errors={}
xyz.swapee.rc.Testing.Validation={}
xyz.swapee.rc.Testing.Ctx={}
xyz.swapee.rc.ITestingPageView={}
xyz.swapee.rc.ITestingPageView.TestingPartial={}
xyz.swapee.rc.ITestingPageViewJoinpointModel={}
xyz.swapee.rc.ITestingPageViewAspectsInstaller={}
xyz.swapee.rc.UTestingPageView={}
xyz.swapee.rc.AbstractTestingPageViewUniversal={}
xyz.swapee.rc.ITestingPageViewAspects={}
xyz.swapee.rc.IHyperTestingPageView={}
xyz.swapee.rc.ITestingImpl={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml}  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {com.changelly.UChangelly.Initialese} xyz.swapee.rc.ITestingPage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.TestingPage)} xyz.swapee.rc.AbstractTestingPage.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPage} xyz.swapee.rc.TestingPage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.ITestingPage` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPage
 */
xyz.swapee.rc.AbstractTestingPage = class extends /** @type {xyz.swapee.rc.AbstractTestingPage.constructor&xyz.swapee.rc.TestingPage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPage.prototype.constructor = xyz.swapee.rc.AbstractTestingPage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPage.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)|!xyz.swapee.rc.ITestingPageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)|!xyz.swapee.rc.ITestingPageHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)|!xyz.swapee.rc.ITestingPageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageJoinpointModelHyperslice
 */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeTesting>} */ (void 0)
    /**
     * After the method.
     */
    this.afterTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTesting>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterTestingThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterTestingCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachTesting>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTesting>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTestingReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFloatingOfferThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFloatingOfferCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFixedOfferThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFixedOfferCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangenowOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangenowOfferThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangenowOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangenowOfferCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangenowOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangenowOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOfferReturns>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageJoinpointModelHyperslice

/**
 * A concrete class of _ITestingPageJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.ITestingPageJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.TestingPageJoinpointModelHyperslice = class extends xyz.swapee.rc.ITestingPageJoinpointModelHyperslice { }
xyz.swapee.rc.TestingPageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterTestingThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterTestingCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachTesting=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFloatingOfferThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFloatingOfferCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFloatingOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangellyFixedOfferThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangellyFixedOfferCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangellyFixedOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterChangenowOfferThrows=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterChangenowOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterChangenowOfferCancels=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterChangenowOfferReturns=/** @type {!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice

/**
 * A concrete class of _ITestingPageJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.TestingPageJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice { }
xyz.swapee.rc.TestingPageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `ITestingPage`'s methods.
 * @interface xyz.swapee.rc.ITestingPageJoinpointModel
 */
xyz.swapee.rc.ITestingPageJoinpointModel = class { }
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTestingThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTestingReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTestingCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTestingReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachTestingReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOfferReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOfferThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOfferCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeFilterChangenowOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOfferThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOfferReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOfferCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachFilterChangenowOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangenowOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangenowOfferReturns = function() {}

/**
 * A concrete class of _ITestingPageJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.TestingPageJoinpointModel
 * @implements {xyz.swapee.rc.ITestingPageJoinpointModel} An interface that enumerates the joinpoints of `ITestingPage`'s methods.
 */
xyz.swapee.rc.TestingPageJoinpointModel = class extends xyz.swapee.rc.ITestingPageJoinpointModel { }
xyz.swapee.rc.TestingPageJoinpointModel.prototype.constructor = xyz.swapee.rc.TestingPageJoinpointModel

/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel} */
xyz.swapee.rc.RecordITestingPageJoinpointModel

/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel} xyz.swapee.rc.BoundITestingPageJoinpointModel */

/** @typedef {xyz.swapee.rc.TestingPageJoinpointModel} xyz.swapee.rc.BoundTestingPageJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.TestingPageAspectsInstaller)} xyz.swapee.rc.AbstractTestingPageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPageAspectsInstaller} xyz.swapee.rc.TestingPageAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.ITestingPageAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPageAspectsInstaller
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractTestingPageAspectsInstaller.constructor&xyz.swapee.rc.TestingPageAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractTestingPageAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPageAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller|typeof xyz.swapee.rc.TestingPageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller|typeof xyz.swapee.rc.TestingPageAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller|typeof xyz.swapee.rc.TestingPageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese[]) => xyz.swapee.rc.ITestingPageAspectsInstaller} xyz.swapee.rc.TestingPageAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.rc.ITestingPageAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.rc.ITestingPageAspectsInstaller */
xyz.swapee.rc.ITestingPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.ITestingPageAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeTesting=/** @type {number} */ (void 0)
    this.afterTesting=/** @type {number} */ (void 0)
    this.afterTestingThrows=/** @type {number} */ (void 0)
    this.afterTestingReturns=/** @type {number} */ (void 0)
    this.afterTestingCancels=/** @type {number} */ (void 0)
    this.beforeEachTesting=/** @type {number} */ (void 0)
    this.afterEachTesting=/** @type {number} */ (void 0)
    this.afterEachTestingReturns=/** @type {number} */ (void 0)
    this.beforeFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterChangellyFloatingOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFloatingOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFloatingOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterChangellyFixedOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangellyFixedOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterChangenowOffer=/** @type {number} */ (void 0)
    this.afterFilterChangenowOffer=/** @type {number} */ (void 0)
    this.afterFilterChangenowOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterChangenowOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterChangenowOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterChangenowOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangenowOffer=/** @type {number} */ (void 0)
    this.afterEachFilterChangenowOfferReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  testing() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterChangellyFloatingOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterChangellyFixedOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterChangenowOffer() { }
}
/**
 * Create a new *ITestingPageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.ITestingPageAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese>)} xyz.swapee.rc.TestingPageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageAspectsInstaller} xyz.swapee.rc.ITestingPageAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITestingPageAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.TestingPageAspectsInstaller
 * @implements {xyz.swapee.rc.ITestingPageAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.TestingPageAspectsInstaller = class extends /** @type {xyz.swapee.rc.TestingPageAspectsInstaller.constructor&xyz.swapee.rc.ITestingPageAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingPageAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingPageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.TestingPageAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPage.TestingNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Form} form The form.
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Validation} validation The validation.
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx The ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPage.TestingNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPage.TestingNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `testing` method from being executed.
 * @prop {(value: !xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>) => void} sub Cancels a call to `testing` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData
 * @prop {!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `testing` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData
 * @prop {!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPage.FilterChangellyFloatingOfferNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPage.FilterChangellyFloatingOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPage.FilterChangellyFloatingOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterChangellyFloatingOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>) => void} sub Cancels a call to `filterChangellyFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterChangellyFloatingOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData
 * @prop {xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPage.FilterChangellyFixedOfferNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPage.FilterChangellyFixedOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPage.FilterChangellyFixedOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterChangellyFixedOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>) => void} sub Cancels a call to `filterChangellyFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData
 * @prop {xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterChangellyFixedOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData
 * @prop {xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPage.FilterChangenowOfferNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPage.FilterChangenowOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPage.FilterChangenowOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterChangenowOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>) => void} sub Cancels a call to `filterChangenowOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData
 * @prop {xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterChangenowOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData
 * @prop {xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData&xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.ITestingPage.Initialese[]) => xyz.swapee.rc.ITestingPage} xyz.swapee.rc.TestingPageConstructor */

/** @typedef {symbol} xyz.swapee.rc.TestingPageMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UTestingPage.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.ITestingPage} [testingPage]
 */

/** @typedef {function(new: xyz.swapee.rc.TestingPageUniversal)} xyz.swapee.rc.AbstractTestingPageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPageUniversal} xyz.swapee.rc.TestingPageUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UTestingPage` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPageUniversal
 */
xyz.swapee.rc.AbstractTestingPageUniversal = class extends /** @type {xyz.swapee.rc.AbstractTestingPageUniversal.constructor&xyz.swapee.rc.TestingPageUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPageUniversal.prototype.constructor = xyz.swapee.rc.AbstractTestingPageUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPageUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPageUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.TestingPageMetaUniversal} xyz.swapee.rc.AbstractTestingPageUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UTestingPage.Initialese[]) => xyz.swapee.rc.UTestingPage} xyz.swapee.rc.UTestingPageConstructor */

/** @typedef {function(new: xyz.swapee.rc.UTestingPageFields&engineering.type.IEngineer&xyz.swapee.rc.UTestingPageCaster)} xyz.swapee.rc.UTestingPage.constructor */
/**
 * A trait that allows to access an _ITestingPage_ object via a `.testingPage` field.
 * @interface xyz.swapee.rc.UTestingPage
 */
xyz.swapee.rc.UTestingPage = class extends /** @type {xyz.swapee.rc.UTestingPage.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UTestingPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UTestingPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UTestingPage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UTestingPage&engineering.type.IInitialiser<!xyz.swapee.rc.UTestingPage.Initialese>)} xyz.swapee.rc.TestingPageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UTestingPage} xyz.swapee.rc.UTestingPage.typeof */
/**
 * A concrete class of _UTestingPage_ instances.
 * @constructor xyz.swapee.rc.TestingPageUniversal
 * @implements {xyz.swapee.rc.UTestingPage} A trait that allows to access an _ITestingPage_ object via a `.testingPage` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UTestingPage.Initialese>} ‎
 */
xyz.swapee.rc.TestingPageUniversal = class extends /** @type {xyz.swapee.rc.TestingPageUniversal.constructor&xyz.swapee.rc.UTestingPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UTestingPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UTestingPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UTestingPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UTestingPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPageUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.TestingPageUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UTestingPage.
 * @interface xyz.swapee.rc.UTestingPageFields
 */
xyz.swapee.rc.UTestingPageFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UTestingPageFields.prototype.testingPage = /** @type {xyz.swapee.rc.ITestingPage} */ (void 0)

/** @typedef {xyz.swapee.rc.UTestingPage} */
xyz.swapee.rc.RecordUTestingPage

/** @typedef {xyz.swapee.rc.UTestingPage} xyz.swapee.rc.BoundUTestingPage */

/** @typedef {xyz.swapee.rc.TestingPageUniversal} xyz.swapee.rc.BoundTestingPageUniversal */

/**
 * Contains getters to cast the _UTestingPage_ interface.
 * @interface xyz.swapee.rc.UTestingPageCaster
 */
xyz.swapee.rc.UTestingPageCaster = class { }
/**
 * Provides direct access to _UTestingPage_ via the _BoundUTestingPage_ universal.
 * @type {!xyz.swapee.rc.BoundTestingPage}
 */
xyz.swapee.rc.UTestingPageCaster.prototype.asTestingPage
/**
 * Cast the _UTestingPage_ instance into the _BoundUTestingPage_ type.
 * @type {!xyz.swapee.rc.BoundUTestingPage}
 */
xyz.swapee.rc.UTestingPageCaster.prototype.asUTestingPage
/**
 * Access the _TestingPageUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundTestingPageUniversal}
 */
xyz.swapee.rc.UTestingPageCaster.prototype.superTestingPageUniversal

/** @typedef {function(new: xyz.swapee.rc.BTestingPageAspectsCaster<THIS>&xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BTestingPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice} xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *ITestingPage* that bind to an instance.
 * @interface xyz.swapee.rc.BTestingPageAspects
 * @template THIS
 */
xyz.swapee.rc.BTestingPageAspects = class extends /** @type {xyz.swapee.rc.BTestingPageAspects.constructor&xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BTestingPageAspects.prototype.constructor = xyz.swapee.rc.BTestingPageAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ITestingPageAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.TestingPageAspects)} xyz.swapee.rc.AbstractTestingPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPageAspects} xyz.swapee.rc.TestingPageAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.ITestingPageAspects` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPageAspects
 */
xyz.swapee.rc.AbstractTestingPageAspects = class extends /** @type {xyz.swapee.rc.AbstractTestingPageAspects.constructor&xyz.swapee.rc.TestingPageAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPageAspects.prototype.constructor = xyz.swapee.rc.AbstractTestingPageAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPageAspects.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPageAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.ITestingPageAspects.Initialese[]) => xyz.swapee.rc.ITestingPageAspects} xyz.swapee.rc.TestingPageAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.ITestingPageAspectsCaster&xyz.swapee.rc.BTestingPageAspects<!xyz.swapee.rc.ITestingPageAspects>&com.changelly.UChangelly)} xyz.swapee.rc.ITestingPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BTestingPageAspects} xyz.swapee.rc.BTestingPageAspects.typeof */
/** @typedef {typeof com.changelly.UChangelly} com.changelly.UChangelly.typeof */
/**
 * The aspects of the *ITestingPage*.
 * @interface xyz.swapee.rc.ITestingPageAspects
 */
xyz.swapee.rc.ITestingPageAspects = class extends /** @type {xyz.swapee.rc.ITestingPageAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BTestingPageAspects.typeof&com.changelly.UChangelly.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingPageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ITestingPageAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.ITestingPageAspects&engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageAspects.Initialese>)} xyz.swapee.rc.TestingPageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageAspects} xyz.swapee.rc.ITestingPageAspects.typeof */
/**
 * A concrete class of _ITestingPageAspects_ instances.
 * @constructor xyz.swapee.rc.TestingPageAspects
 * @implements {xyz.swapee.rc.ITestingPageAspects} The aspects of the *ITestingPage*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageAspects.Initialese>} ‎
 */
xyz.swapee.rc.TestingPageAspects = class extends /** @type {xyz.swapee.rc.TestingPageAspects.constructor&xyz.swapee.rc.ITestingPageAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingPageAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.ITestingPageAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingPageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPageAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.TestingPageAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BTestingPageAspects_ interface.
 * @interface xyz.swapee.rc.BTestingPageAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BTestingPageAspectsCaster = class { }
/**
 * Cast the _BTestingPageAspects_ instance into the _BoundITestingPage_ type.
 * @type {!xyz.swapee.rc.BoundITestingPage}
 */
xyz.swapee.rc.BTestingPageAspectsCaster.prototype.asITestingPage

/**
 * Contains getters to cast the _ITestingPageAspects_ interface.
 * @interface xyz.swapee.rc.ITestingPageAspectsCaster
 */
xyz.swapee.rc.ITestingPageAspectsCaster = class { }
/**
 * Cast the _ITestingPageAspects_ instance into the _BoundITestingPage_ type.
 * @type {!xyz.swapee.rc.BoundITestingPage}
 */
xyz.swapee.rc.ITestingPageAspectsCaster.prototype.asITestingPage

/** @typedef {xyz.swapee.rc.ITestingPage.Initialese} xyz.swapee.rc.IHyperTestingPage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.HyperTestingPage)} xyz.swapee.rc.AbstractHyperTestingPage.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperTestingPage} xyz.swapee.rc.HyperTestingPage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperTestingPage` interface.
 * @constructor xyz.swapee.rc.AbstractHyperTestingPage
 */
xyz.swapee.rc.AbstractHyperTestingPage = class extends /** @type {xyz.swapee.rc.AbstractHyperTestingPage.constructor&xyz.swapee.rc.HyperTestingPage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperTestingPage.prototype.constructor = xyz.swapee.rc.AbstractHyperTestingPage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperTestingPage.class = /** @type {typeof xyz.swapee.rc.AbstractHyperTestingPage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperTestingPage|typeof xyz.swapee.rc.HyperTestingPage)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperTestingPage|typeof xyz.swapee.rc.HyperTestingPage)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperTestingPage|typeof xyz.swapee.rc.HyperTestingPage)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.ITestingPageAspects|function(new: xyz.swapee.rc.ITestingPageAspects)} aides The list of aides that advise the ITestingPage to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPage.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperTestingPage.Initialese[]) => xyz.swapee.rc.IHyperTestingPage} xyz.swapee.rc.HyperTestingPageConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperTestingPageCaster&xyz.swapee.rc.ITestingPage)} xyz.swapee.rc.IHyperTestingPage.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPage} xyz.swapee.rc.ITestingPage.typeof */
/** @interface xyz.swapee.rc.IHyperTestingPage */
xyz.swapee.rc.IHyperTestingPage = class extends /** @type {xyz.swapee.rc.IHyperTestingPage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.ITestingPage.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperTestingPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperTestingPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperTestingPage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperTestingPage&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperTestingPage.Initialese>)} xyz.swapee.rc.HyperTestingPage.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperTestingPage} xyz.swapee.rc.IHyperTestingPage.typeof */
/**
 * A concrete class of _IHyperTestingPage_ instances.
 * @constructor xyz.swapee.rc.HyperTestingPage
 * @implements {xyz.swapee.rc.IHyperTestingPage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperTestingPage.Initialese>} ‎
 */
xyz.swapee.rc.HyperTestingPage = class extends /** @type {xyz.swapee.rc.HyperTestingPage.constructor&xyz.swapee.rc.IHyperTestingPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperTestingPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperTestingPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperTestingPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperTestingPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperTestingPage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.HyperTestingPage.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperTestingPage} */
xyz.swapee.rc.RecordIHyperTestingPage

/** @typedef {xyz.swapee.rc.IHyperTestingPage} xyz.swapee.rc.BoundIHyperTestingPage */

/** @typedef {xyz.swapee.rc.HyperTestingPage} xyz.swapee.rc.BoundHyperTestingPage */

/**
 * Contains getters to cast the _IHyperTestingPage_ interface.
 * @interface xyz.swapee.rc.IHyperTestingPageCaster
 */
xyz.swapee.rc.IHyperTestingPageCaster = class { }
/**
 * Cast the _IHyperTestingPage_ instance into the _BoundIHyperTestingPage_ type.
 * @type {!xyz.swapee.rc.BoundIHyperTestingPage}
 */
xyz.swapee.rc.IHyperTestingPageCaster.prototype.asIHyperTestingPage
/**
 * Access the _HyperTestingPage_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperTestingPage}
 */
xyz.swapee.rc.IHyperTestingPageCaster.prototype.superHyperTestingPage

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageHyperslice
 */
xyz.swapee.rc.ITestingPageHyperslice = class {
  constructor() {
    this.testing=/** @type {!xyz.swapee.rc.ITestingPage._testing|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._testing>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPage._filterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._filterChangellyFloatingOffer>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPage._filterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._filterChangellyFixedOffer>} */ (void 0)
    /**
     * Loads the _Changenow_ offer.
     */
    this.filterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPage._filterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._filterChangenowOffer>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageHyperslice

/**
 * A concrete class of _ITestingPageHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageHyperslice
 * @implements {xyz.swapee.rc.ITestingPageHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.TestingPageHyperslice = class extends xyz.swapee.rc.ITestingPageHyperslice { }
xyz.swapee.rc.TestingPageHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.ITestingPageBindingHyperslice = class {
  constructor() {
    this.testing=/** @type {!xyz.swapee.rc.ITestingPage.__testing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__testing<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ floating offer.
     */
    this.filterChangellyFloatingOffer=/** @type {!xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer<THIS>>} */ (void 0)
    /**
     * Loads the _Changelly_ fixed offer.
     */
    this.filterChangellyFixedOffer=/** @type {!xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer<THIS>>} */ (void 0)
    /**
     * Loads the _Changenow_ offer.
     */
    this.filterChangenowOffer=/** @type {!xyz.swapee.rc.ITestingPage.__filterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__filterChangenowOffer<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageBindingHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageBindingHyperslice

/**
 * A concrete class of _ITestingPageBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageBindingHyperslice
 * @implements {xyz.swapee.rc.ITestingPageBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.ITestingPageBindingHyperslice<THIS>}
 */
xyz.swapee.rc.TestingPageBindingHyperslice = class extends xyz.swapee.rc.ITestingPageBindingHyperslice { }
xyz.swapee.rc.TestingPageBindingHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.ITestingPageFields&engineering.type.IEngineer&xyz.swapee.rc.ITestingPageCaster&com.changelly.UChangelly&xyz.swapee.rc.UTestingPage)} xyz.swapee.rc.ITestingPage.constructor */
/** @interface xyz.swapee.rc.ITestingPage */
xyz.swapee.rc.ITestingPage = class extends /** @type {xyz.swapee.rc.ITestingPage.constructor&engineering.type.IEngineer.typeof&com.changelly.UChangelly.typeof&xyz.swapee.rc.UTestingPage.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ITestingPage.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.ITestingPage.testing} */
xyz.swapee.rc.ITestingPage.prototype.testing = function() {}
/** @type {xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPage.prototype.filterChangellyFloatingOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPage.prototype.filterChangellyFixedOffer = function() {}
/** @type {xyz.swapee.rc.ITestingPage.filterChangenowOffer} */
xyz.swapee.rc.ITestingPage.prototype.filterChangenowOffer = function() {}

/** @typedef {function(new: xyz.swapee.rc.ITestingPage&engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPage.Initialese>)} xyz.swapee.rc.TestingPage.constructor */
/**
 * A concrete class of _ITestingPage_ instances.
 * @constructor xyz.swapee.rc.TestingPage
 * @implements {xyz.swapee.rc.ITestingPage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPage.Initialese>} ‎
 */
xyz.swapee.rc.TestingPage = class extends /** @type {xyz.swapee.rc.TestingPage.constructor&xyz.swapee.rc.ITestingPage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingPage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.ITestingPage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingPage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.TestingPage.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingPage.
 * @interface xyz.swapee.rc.ITestingPageFields
 */
xyz.swapee.rc.ITestingPageFields = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPageFields.prototype.testingCtx = /** @type {!xyz.swapee.rc.ITestingPage.testing.Ctx} */ (void 0)

/** @typedef {xyz.swapee.rc.ITestingPage} */
xyz.swapee.rc.RecordITestingPage

/** @typedef {xyz.swapee.rc.ITestingPage} xyz.swapee.rc.BoundITestingPage */

/** @typedef {xyz.swapee.rc.TestingPage} xyz.swapee.rc.BoundTestingPage */

/**
 * Contains getters to cast the _ITestingPage_ interface.
 * @interface xyz.swapee.rc.ITestingPageCaster
 */
xyz.swapee.rc.ITestingPageCaster = class { }
/**
 * Cast the _ITestingPage_ instance into the _BoundITestingPage_ type.
 * @type {!xyz.swapee.rc.BoundITestingPage}
 */
xyz.swapee.rc.ITestingPageCaster.prototype.asITestingPage
/**
 * Access the _TestingPage_ prototype.
 * @type {!xyz.swapee.rc.BoundTestingPage}
 */
xyz.swapee.rc.ITestingPageCaster.prototype.superTestingPage

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeTesting} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.TestingNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `testing` method from being executed.
 * - `sub` _(value: !ITestingPage.testing.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.testing.OptAnswers&gt;) =&gt; void_ Cancels a call to `testing` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterTesting} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `after` joinpoint.
 * - `res` _!ITestingPage.testing.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.testing.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `testing` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `afterReturns` joinpoint.
 * - `res` _!ITestingPage.testing.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.testing.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: !ITestingPage.testing.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.testing.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachTesting} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.TestingNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `testing` method from being executed.
 * - `sub` _(value: !ITestingPage.testing.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.testing.OptAnswers&gt;) =&gt; void_ Cancels a call to `testing` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTesting} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `after` joinpoint.
 * - `res` _!ITestingPage.testing.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.testing.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTestingReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTestingReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data] Metadata passed to the pointcuts of _testing_ at the `afterEach` joinpoint.
 * - `res` _!ITestingPage.testing.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.testing.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `args` _ITestingPage.TestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.TestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTestingReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFloatingOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.FilterChangellyFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFloatingOffer` method from being executed.
 * - `sub` _(value: ITestingPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `after` joinpoint.
 * - `res` _ITestingPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterChangellyFloatingOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterReturns` joinpoint.
 * - `res` _ITestingPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: ITestingPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFloatingOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFloatingOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.FilterChangellyFloatingOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFloatingOffer` method from being executed.
 * - `sub` _(value: ITestingPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFloatingOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFloatingOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `after` joinpoint.
 * - `res` _ITestingPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFloatingOffer_ at the `afterEach` joinpoint.
 * - `res` _ITestingPage.filterChangellyFloatingOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFloatingOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFloatingOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFixedOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.FilterChangellyFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFixedOffer` method from being executed.
 * - `sub` _(value: ITestingPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `after` joinpoint.
 * - `res` _ITestingPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterChangellyFixedOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterReturns` joinpoint.
 * - `res` _ITestingPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: ITestingPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFixedOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFixedOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.FilterChangellyFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangellyFixedOffer` method from being executed.
 * - `sub` _(value: ITestingPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangellyFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `after` joinpoint.
 * - `res` _ITestingPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangellyFixedOffer_ at the `afterEach` joinpoint.
 * - `res` _ITestingPage.filterChangellyFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangellyFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `args` _ITestingPage.FilterChangellyFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangenowOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangenowOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.FilterChangenowOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangenowOffer` method from being executed.
 * - `sub` _(value: ITestingPage.filterChangenowOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangenowOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangenowOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangenowOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `after` joinpoint.
 * - `res` _ITestingPage.filterChangenowOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangenowOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterChangenowOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `afterReturns` joinpoint.
 * - `res` _ITestingPage.filterChangenowOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangenowOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: ITestingPage.filterChangenowOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangenowOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangenowOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangenowOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPage.FilterChangenowOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterChangenowOffer` method from being executed.
 * - `sub` _(value: ITestingPage.filterChangenowOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangenowOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterChangenowOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangenowOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `after` joinpoint.
 * - `res` _ITestingPage.filterChangenowOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangenowOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData) => void} xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns<!xyz.swapee.rc.ITestingPageJoinpointModel>} xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOfferReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data] Metadata passed to the pointcuts of _filterChangenowOffer_ at the `afterEach` joinpoint.
 * - `res` _ITestingPage.filterChangenowOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!ITestingPage.filterChangenowOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `args` _ITestingPage.FilterChangenowOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageJoinpointModel.FilterChangenowOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.ITestingPage.testing.Form, answers: !xyz.swapee.rc.ITestingPage.testing.Answers, validation: !xyz.swapee.rc.ITestingPage.testing.Validation, errors: !xyz.swapee.rc.ITestingPage.testing.Errors, ctx: !xyz.swapee.rc.ITestingPage.testing.Ctx) => (void|!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)} xyz.swapee.rc.ITestingPage.__testing
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPage.__testing<!xyz.swapee.rc.ITestingPage>} xyz.swapee.rc.ITestingPage._testing */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.testing} */
/**
 *  `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form The form.
 * - `locale` _string_
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFixedOffer.Answers*
 * - `changenowOffer` _&#42;_ ⤴ *ITestingPage.filterChangenowOffer.Answers*
 * @param {!xyz.swapee.rc.ITestingPage.testing.Validation} validation The validation.
 * @param {!xyz.swapee.rc.ITestingPage.testing.Errors} errors The errors.
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx The ctx.
 * - `validation` _!ITestingPage.testing.Validation_ The validation.
 * - `errors` _!ITestingPage.testing.Errors_ The errors.
 * - `answers` _!ITestingPage.testing.Answers_ The answers.
 * @return {void|!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>}
 */
xyz.swapee.rc.ITestingPage.testing = function(form, answers, validation, errors, ctx) {}

/**
 * The form.
 * @record xyz.swapee.rc.ITestingPage.testing.Form
 */
xyz.swapee.rc.ITestingPage.testing.Form = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.testing.Form.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers&xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers&xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers)} xyz.swapee.rc.ITestingPage.testing.Answers.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers} xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.ITestingPage.testing.Answers
 */
xyz.swapee.rc.ITestingPage.testing.Answers = class extends /** @type {xyz.swapee.rc.ITestingPage.testing.Answers.constructor&xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers.typeof&xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers.typeof&xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.testing.Answers.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers&xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers&xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers)} xyz.swapee.rc.ITestingPage.testing.OptAnswers.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers} xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.ITestingPage.testing.OptAnswers
 */
xyz.swapee.rc.ITestingPage.testing.OptAnswers = class extends /** @type {xyz.swapee.rc.ITestingPage.testing.OptAnswers.constructor&xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers.typeof&xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers.typeof&xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.testing.OptAnswers.prototype.locale = /** @type {string|undefined} */ (void 0)

/**
 * The validation.
 * @record xyz.swapee.rc.ITestingPage.testing.Validation
 */
xyz.swapee.rc.ITestingPage.testing.Validation = class { }
xyz.swapee.rc.ITestingPage.testing.Validation.prototype.constructor = xyz.swapee.rc.ITestingPage.testing.Validation

/**
 * The errors.
 * @record xyz.swapee.rc.ITestingPage.testing.Errors
 */
xyz.swapee.rc.ITestingPage.testing.Errors = class { }
xyz.swapee.rc.ITestingPage.testing.Errors.prototype.constructor = xyz.swapee.rc.ITestingPage.testing.Errors

/**
 * The ctx.
 * @record xyz.swapee.rc.ITestingPage.testing.Ctx
 */
xyz.swapee.rc.ITestingPage.testing.Ctx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.ITestingPage.testing.Ctx.prototype.validation = /** @type {!xyz.swapee.rc.ITestingPage.testing.Validation} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.ITestingPage.testing.Ctx.prototype.errors = /** @type {!xyz.swapee.rc.ITestingPage.testing.Errors} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.ITestingPage.testing.Ctx.prototype.answers = /** @type {!xyz.swapee.rc.ITestingPage.testing.Answers} */ (void 0)

/**
 * The ctx.
 * @record xyz.swapee.rc.ITestingPage.testing.OptCtx
 */
xyz.swapee.rc.ITestingPage.testing.OptCtx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.ITestingPage.testing.OptCtx.prototype.validation = /** @type {(!xyz.swapee.rc.ITestingPage.testing.Validation)|undefined} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.ITestingPage.testing.OptCtx.prototype.errors = /** @type {(!xyz.swapee.rc.ITestingPage.testing.Errors)|undefined} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.ITestingPage.testing.OptCtx.prototype.answers = /** @type {(!xyz.swapee.rc.ITestingPage.testing.Answers)|undefined} */ (void 0)

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form, answers: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers, validation: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation, errors: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors, ctx: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx) => (void|xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)} xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer<!xyz.swapee.rc.ITestingPage>} xyz.swapee.rc.ITestingPage._filterChangellyFloatingOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer} */
/**
 * Loads the _Changelly_ floating offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form} form The action form.
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers} answers The action answers.
 * - `changellyFloatingOffer` _&#42;_
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>}
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.amountIn = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.currencyIn = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.currencyOut = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatingOffer = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatingOffer = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptCtx
 */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptCtx = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptCtx.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form, answers: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers, validation: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation, errors: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors, ctx: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx) => (void|xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)} xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer<!xyz.swapee.rc.ITestingPage>} xyz.swapee.rc.ITestingPage._filterChangellyFixedOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer} */
/**
 * Loads the _Changelly_ fixed offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form} form The action form.
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers} answers The action answers.
 * - `changellyFixedOffer` _&#42;_
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>}
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.amountIn = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.currencyIn = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.currencyOut = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedOffer = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedOffer = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptCtx
 */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptCtx = class { }
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptCtx.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form, answers: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers, validation: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation, errors: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors, ctx: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx) => (void|xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)} xyz.swapee.rc.ITestingPage.__filterChangenowOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPage.__filterChangenowOffer<!xyz.swapee.rc.ITestingPage>} xyz.swapee.rc.ITestingPage._filterChangenowOffer */
/** @typedef {typeof xyz.swapee.rc.ITestingPage.filterChangenowOffer} */
/**
 * Loads the _Changenow_ offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form} form The action form.
 * - `changeNow` _&#42;_
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers} answers The action answers.
 * - `changenowOffer` _&#42;_
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>}
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.changeNow = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.amountIn = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.currencyIn = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.currencyOut = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers.prototype.changenowOffer = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers.prototype.changenowOffer = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation = class { }
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors = class { }
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx = class { }
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptCtx
 */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptCtx = class { }
xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptCtx.prototype.constructor = xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptCtx

// nss:xyz.swapee.rc,xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc.ITestingPage
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml}  c12b9bb72618d701304f17934ecdaa53 */
/** @constructor xyz.swapee.rc.Testing */
xyz.swapee.rc.Testing = class { }
xyz.swapee.rc.Testing.prototype.constructor = xyz.swapee.rc.Testing

/** @typedef {function(new: xyz.swapee.rc.ITestingPage.testing.Answers)} xyz.swapee.rc.Testing.Answers.constructor */
/**
 * The answers.
 * @record xyz.swapee.rc.Testing.Answers
 */
xyz.swapee.rc.Testing.Answers = class extends /** @type {xyz.swapee.rc.Testing.Answers.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.Testing.Answers.prototype.props = /** @type {!xyz.swapee.rc.Testing.Answers.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.Testing.Answers.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.ITestingPage.testing.Form)} xyz.swapee.rc.Testing.Form.constructor */
/**
 * The form.
 * @record xyz.swapee.rc.Testing.Form
 */
xyz.swapee.rc.Testing.Form = class extends /** @type {xyz.swapee.rc.Testing.Form.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.Testing.Form.prototype.props = /** @type {!xyz.swapee.rc.Testing.Form.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.Testing.Form.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.ITestingPage.testing.Errors)} xyz.swapee.rc.Testing.Errors.constructor */
/**
 * The errors.
 * @record xyz.swapee.rc.Testing.Errors
 */
xyz.swapee.rc.Testing.Errors = class extends /** @type {xyz.swapee.rc.Testing.Errors.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.Testing.Errors.prototype.props = /** @type {!xyz.swapee.rc.Testing.Errors.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.Testing.Errors.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.ITestingPage.testing.Validation)} xyz.swapee.rc.Testing.Validation.constructor */
/**
 * The validation.
 * @record xyz.swapee.rc.Testing.Validation
 */
xyz.swapee.rc.Testing.Validation = class extends /** @type {xyz.swapee.rc.Testing.Validation.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.Testing.Validation.prototype.props = /** @type {!xyz.swapee.rc.Testing.Validation.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.Testing.Validation.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.ITestingPage.testing.Ctx)} xyz.swapee.rc.Testing.Ctx.constructor */
/**
 * The ctx.
 * @record xyz.swapee.rc.Testing.Ctx
 */
xyz.swapee.rc.Testing.Ctx = class extends /** @type {xyz.swapee.rc.Testing.Ctx.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.Testing.Ctx.prototype.props = /** @type {!xyz.swapee.rc.Testing.Ctx.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.Testing.Ctx.Props The props for VSCode. */

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml}  9c94d7079915a315fadd7437cea05033 */
/** @typedef {typeof __$te_plain} xyz.swapee.rc.ITestingPageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.TestingPageView)} xyz.swapee.rc.AbstractTestingPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPageView} xyz.swapee.rc.TestingPageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.ITestingPageView` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPageView
 */
xyz.swapee.rc.AbstractTestingPageView = class extends /** @type {xyz.swapee.rc.AbstractTestingPageView.constructor&xyz.swapee.rc.TestingPageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPageView.prototype.constructor = xyz.swapee.rc.AbstractTestingPageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPageView.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)|!xyz.swapee.rc.ITestingPageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)|!xyz.swapee.rc.ITestingPageViewHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)|!xyz.swapee.rc.ITestingPageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice
 */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeViewTesting>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTesting>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewTestingThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewTestingCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachViewTesting>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTesting>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTestingReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeGetTesting>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTesting>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetTestingThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetTestingCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachGetTesting>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTesting>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTestingReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeSetTestingCtx>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtx>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetTestingCtxThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetTestingCtxReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetTestingCtxCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachSetTestingCtx>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtx>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetTestingCtxReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtxReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_TestingPartial=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingPartial>} */ (void 0)
    /**
     * After the method.
     */
    this.after_TestingPartial=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartial>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_TestingPartialThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_TestingPartialReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_TestingPartialCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingView>} */ (void 0)
    /**
     * After the method.
     */
    this.after_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingView>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_TestingViewThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_TestingViewReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_TestingViewCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEach_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEach_TestingView>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingView>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_TestingViewReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingViewReturns>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice

/**
 * A concrete class of _ITestingPageViewJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.TestingPageViewJoinpointModelHyperslice = class extends xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice { }
xyz.swapee.rc.TestingPageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageViewJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewTestingThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewTestingCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetTestingThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetTestingCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetTesting=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetTestingReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetTestingCtxThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetTestingCtxReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetTestingCtxCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetTestingCtxReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_TestingPartial=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_TestingPartial=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_TestingPartialThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_TestingPartialReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_TestingPartialCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_TestingViewThrows=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_TestingViewReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_TestingViewCancels=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_TestingView=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_TestingViewReturns=/** @type {!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice

/**
 * A concrete class of _ITestingPageViewJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.TestingPageViewJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice { }
xyz.swapee.rc.TestingPageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageViewJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `ITestingPageView`'s methods.
 * @interface xyz.swapee.rc.ITestingPageViewJoinpointModel
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel = class { }
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeViewTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTestingThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTestingReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTestingCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEachViewTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachViewTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachViewTestingReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeGetTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTestingThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTestingReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTestingCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEachGetTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachGetTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachGetTestingReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeSetTestingCtx = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtx = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtxThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtxReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtxCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEachSetTestingCtx = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachSetTestingCtx = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtxReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachSetTestingCtxReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingPartial} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.before_TestingPartial = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartial} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartial = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartialThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartialReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartialCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.before_TestingView = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingView = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingViewThrows = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingViewReturns = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingViewCancels = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEach_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEach_TestingView = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEach_TestingView = function() {}
/** @type {xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingViewReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEach_TestingViewReturns = function() {}

/**
 * A concrete class of _ITestingPageViewJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewJoinpointModel
 * @implements {xyz.swapee.rc.ITestingPageViewJoinpointModel} An interface that enumerates the joinpoints of `ITestingPageView`'s methods.
 */
xyz.swapee.rc.TestingPageViewJoinpointModel = class extends xyz.swapee.rc.ITestingPageViewJoinpointModel { }
xyz.swapee.rc.TestingPageViewJoinpointModel.prototype.constructor = xyz.swapee.rc.TestingPageViewJoinpointModel

/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel} */
xyz.swapee.rc.RecordITestingPageViewJoinpointModel

/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel} xyz.swapee.rc.BoundITestingPageViewJoinpointModel */

/** @typedef {xyz.swapee.rc.TestingPageViewJoinpointModel} xyz.swapee.rc.BoundTestingPageViewJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.TestingPageViewAspectsInstaller)} xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller} xyz.swapee.rc.TestingPageViewAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.ITestingPageViewAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.constructor&xyz.swapee.rc.TestingPageViewAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.ITestingPageViewAspectsInstaller|typeof xyz.swapee.rc.TestingPageViewAspectsInstaller)|(!xyz.swapee.rc.ITestingPageInstaller|typeof xyz.swapee.rc.TestingPageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.ITestingPageViewAspectsInstaller|typeof xyz.swapee.rc.TestingPageViewAspectsInstaller)|(!xyz.swapee.rc.ITestingPageInstaller|typeof xyz.swapee.rc.TestingPageInstaller)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.ITestingPageViewAspectsInstaller|typeof xyz.swapee.rc.TestingPageViewAspectsInstaller)|(!xyz.swapee.rc.ITestingPageInstaller|typeof xyz.swapee.rc.TestingPageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese[]) => xyz.swapee.rc.ITestingPageViewAspectsInstaller} xyz.swapee.rc.TestingPageViewAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.ITestingPageInstaller)} xyz.swapee.rc.ITestingPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageInstaller} xyz.swapee.rc.ITestingPageInstaller.typeof */
/** @interface xyz.swapee.rc.ITestingPageViewAspectsInstaller */
xyz.swapee.rc.ITestingPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.ITestingPageViewAspectsInstaller.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.ITestingPageInstaller.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeViewTesting=/** @type {number} */ (void 0)
    this.afterViewTesting=/** @type {number} */ (void 0)
    this.afterViewTestingThrows=/** @type {number} */ (void 0)
    this.afterViewTestingReturns=/** @type {number} */ (void 0)
    this.afterViewTestingCancels=/** @type {number} */ (void 0)
    this.beforeEachViewTesting=/** @type {number} */ (void 0)
    this.afterEachViewTesting=/** @type {number} */ (void 0)
    this.afterEachViewTestingReturns=/** @type {number} */ (void 0)
    this.beforeGetTesting=/** @type {number} */ (void 0)
    this.afterGetTesting=/** @type {number} */ (void 0)
    this.afterGetTestingThrows=/** @type {number} */ (void 0)
    this.afterGetTestingReturns=/** @type {number} */ (void 0)
    this.afterGetTestingCancels=/** @type {number} */ (void 0)
    this.beforeEachGetTesting=/** @type {number} */ (void 0)
    this.afterEachGetTesting=/** @type {number} */ (void 0)
    this.afterEachGetTestingReturns=/** @type {number} */ (void 0)
    this.beforeSetTestingCtx=/** @type {number} */ (void 0)
    this.afterSetTestingCtx=/** @type {number} */ (void 0)
    this.afterSetTestingCtxThrows=/** @type {number} */ (void 0)
    this.afterSetTestingCtxReturns=/** @type {number} */ (void 0)
    this.afterSetTestingCtxCancels=/** @type {number} */ (void 0)
    this.beforeEachSetTestingCtx=/** @type {number} */ (void 0)
    this.afterEachSetTestingCtx=/** @type {number} */ (void 0)
    this.afterEachSetTestingCtxReturns=/** @type {number} */ (void 0)
    this.before_TestingPartial=/** @type {number} */ (void 0)
    this.after_TestingPartial=/** @type {number} */ (void 0)
    this.after_TestingPartialThrows=/** @type {number} */ (void 0)
    this.after_TestingPartialReturns=/** @type {number} */ (void 0)
    this.after_TestingPartialCancels=/** @type {number} */ (void 0)
    this.before_TestingView=/** @type {number} */ (void 0)
    this.after_TestingView=/** @type {number} */ (void 0)
    this.after_TestingViewThrows=/** @type {number} */ (void 0)
    this.after_TestingViewReturns=/** @type {number} */ (void 0)
    this.after_TestingViewCancels=/** @type {number} */ (void 0)
    this.beforeEach_TestingView=/** @type {number} */ (void 0)
    this.afterEach_TestingView=/** @type {number} */ (void 0)
    this.afterEach_TestingViewReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  viewTesting() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  getTesting() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  setTestingCtx() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  TestingPartial() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  TestingView() { }
}
/**
 * Create a new *ITestingPageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.ITestingPageViewAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese>)} xyz.swapee.rc.TestingPageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewAspectsInstaller} xyz.swapee.rc.ITestingPageViewAspectsInstaller.typeof */
/**
 * A concrete class of _ITestingPageViewAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewAspectsInstaller
 * @implements {xyz.swapee.rc.ITestingPageViewAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.TestingPageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.TestingPageViewAspectsInstaller.constructor&xyz.swapee.rc.ITestingPageViewAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingPageViewAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingPageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.TestingPageViewAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageView.ViewTestingNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPageView.ViewTestingNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPageView.ViewTestingNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `viewTesting` method from being executed.
 * @prop {(value: !xyz.swapee.rc.ITestingPageView.TestingView) => void} sub Cancels a call to `viewTesting` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `viewTesting` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.ITestingPageView.TestingView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageView.GetTestingNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPageView.GetTestingNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPageView.GetTestingNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getTesting` method from being executed.
 * @prop {(value: !xyz.swapee.rc.ITestingPageView.TestingView) => void} sub Cancels a call to `getTesting` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getTesting` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.ITestingPageView.TestingView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageView.SetTestingCtxNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation The translation for the user's locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPageView.SetTestingCtxNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPageView.SetTestingCtxNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `setTestingCtx` method from being executed.
 * @prop {(value: !xyz.swapee.rc.ITestingPage.testing.OptCtx) => void} sub Cancels a call to `setTestingCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData
 * @prop {!xyz.swapee.rc.ITestingPage.testing.OptCtx} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `setTestingCtx` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData
 * @prop {!xyz.swapee.rc.ITestingPage.testing.OptCtx} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.ITestingPage.testing.OptCtx) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageView.TestingPartialNArgs
 * @prop {!xyz.swapee.rc.ITestingPage.testing.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingPartial.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingPartial.Actions} actions The actions.
 * @prop {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation The translation for the chosen user locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.ITestingPageView.TestingPartialNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.ITestingPageView.TestingPartialNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `TestingPartial` method from being executed.
 * @prop {(value: engineering.type.VNode) => void} sub Cancels a call to `TestingPartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `TestingPartial` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 * @prop {(value: engineering.type.VNode) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `TestingView` method from being executed.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `TestingView` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData&xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.ITestingPageView.Initialese[]) => xyz.swapee.rc.ITestingPageView} xyz.swapee.rc.TestingPageViewConstructor */

/** @typedef {symbol} xyz.swapee.rc.TestingPageViewMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UTestingPageView.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.ITestingPageView} [testingPageView]
 */

/** @typedef {function(new: xyz.swapee.rc.TestingPageViewUniversal)} xyz.swapee.rc.AbstractTestingPageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPageViewUniversal} xyz.swapee.rc.TestingPageViewUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UTestingPageView` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPageViewUniversal
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal = class extends /** @type {xyz.swapee.rc.AbstractTestingPageViewUniversal.constructor&xyz.swapee.rc.TestingPageViewUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPageViewUniversal.prototype.constructor = xyz.swapee.rc.AbstractTestingPageViewUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPageViewUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.TestingPageViewMetaUniversal} xyz.swapee.rc.AbstractTestingPageViewUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UTestingPageView.Initialese[]) => xyz.swapee.rc.UTestingPageView} xyz.swapee.rc.UTestingPageViewConstructor */

/** @typedef {function(new: xyz.swapee.rc.UTestingPageViewFields&engineering.type.IEngineer&xyz.swapee.rc.UTestingPageViewCaster)} xyz.swapee.rc.UTestingPageView.constructor */
/**
 * A trait that allows to access an _ITestingPageView_ object via a `.testingPageView` field.
 * @interface xyz.swapee.rc.UTestingPageView
 */
xyz.swapee.rc.UTestingPageView = class extends /** @type {xyz.swapee.rc.UTestingPageView.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UTestingPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UTestingPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UTestingPageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UTestingPageView&engineering.type.IInitialiser<!xyz.swapee.rc.UTestingPageView.Initialese>)} xyz.swapee.rc.TestingPageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UTestingPageView} xyz.swapee.rc.UTestingPageView.typeof */
/**
 * A concrete class of _UTestingPageView_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewUniversal
 * @implements {xyz.swapee.rc.UTestingPageView} A trait that allows to access an _ITestingPageView_ object via a `.testingPageView` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UTestingPageView.Initialese>} ‎
 */
xyz.swapee.rc.TestingPageViewUniversal = class extends /** @type {xyz.swapee.rc.TestingPageViewUniversal.constructor&xyz.swapee.rc.UTestingPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UTestingPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UTestingPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UTestingPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UTestingPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPageViewUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.TestingPageViewUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UTestingPageView.
 * @interface xyz.swapee.rc.UTestingPageViewFields
 */
xyz.swapee.rc.UTestingPageViewFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UTestingPageViewFields.prototype.testingPageView = /** @type {xyz.swapee.rc.ITestingPageView} */ (void 0)

/** @typedef {xyz.swapee.rc.UTestingPageView} */
xyz.swapee.rc.RecordUTestingPageView

/** @typedef {xyz.swapee.rc.UTestingPageView} xyz.swapee.rc.BoundUTestingPageView */

/** @typedef {xyz.swapee.rc.TestingPageViewUniversal} xyz.swapee.rc.BoundTestingPageViewUniversal */

/**
 * Contains getters to cast the _UTestingPageView_ interface.
 * @interface xyz.swapee.rc.UTestingPageViewCaster
 */
xyz.swapee.rc.UTestingPageViewCaster = class { }
/**
 * Provides direct access to _UTestingPageView_ via the _BoundUTestingPageView_ universal.
 * @type {!xyz.swapee.rc.BoundTestingPageView}
 */
xyz.swapee.rc.UTestingPageViewCaster.prototype.asTestingPageView
/**
 * Cast the _UTestingPageView_ instance into the _BoundUTestingPageView_ type.
 * @type {!xyz.swapee.rc.BoundUTestingPageView}
 */
xyz.swapee.rc.UTestingPageViewCaster.prototype.asUTestingPageView
/**
 * Access the _TestingPageViewUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundTestingPageViewUniversal}
 */
xyz.swapee.rc.UTestingPageViewCaster.prototype.superTestingPageViewUniversal

/** @typedef {function(new: xyz.swapee.rc.BTestingPageViewAspectsCaster<THIS>&xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BTestingPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice} xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *ITestingPageView* that bind to an instance.
 * @interface xyz.swapee.rc.BTestingPageViewAspects
 * @template THIS
 */
xyz.swapee.rc.BTestingPageViewAspects = class extends /** @type {xyz.swapee.rc.BTestingPageViewAspects.constructor&xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BTestingPageViewAspects.prototype.constructor = xyz.swapee.rc.BTestingPageViewAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ITestingPageViewAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.TestingPageViewAspects)} xyz.swapee.rc.AbstractTestingPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingPageViewAspects} xyz.swapee.rc.TestingPageViewAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.ITestingPageViewAspects` interface.
 * @constructor xyz.swapee.rc.AbstractTestingPageViewAspects
 */
xyz.swapee.rc.AbstractTestingPageViewAspects = class extends /** @type {xyz.swapee.rc.AbstractTestingPageViewAspects.constructor&xyz.swapee.rc.TestingPageViewAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingPageViewAspects.prototype.constructor = xyz.swapee.rc.AbstractTestingPageViewAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.class = /** @type {typeof xyz.swapee.rc.AbstractTestingPageViewAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.ITestingPageViewAspects|typeof xyz.swapee.rc.TestingPageViewAspects)|(!xyz.swapee.rc.BTestingPageViewAspects|typeof xyz.swapee.rc.BTestingPageViewAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.ITestingPageViewAspects|typeof xyz.swapee.rc.TestingPageViewAspects)|(!xyz.swapee.rc.BTestingPageViewAspects|typeof xyz.swapee.rc.BTestingPageViewAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.ITestingPageViewAspects|typeof xyz.swapee.rc.TestingPageViewAspects)|(!xyz.swapee.rc.BTestingPageViewAspects|typeof xyz.swapee.rc.BTestingPageViewAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.ITestingPageViewAspects.Initialese[]) => xyz.swapee.rc.ITestingPageViewAspects} xyz.swapee.rc.TestingPageViewAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.ITestingPageViewAspectsCaster&xyz.swapee.rc.BTestingPageViewAspects<!xyz.swapee.rc.ITestingPageViewAspects>&xyz.swapee.rc.ITestingPage&_idio.IRedirectMod&xyz.swapee.rc.BTestingPageAspects)} xyz.swapee.rc.ITestingPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BTestingPageViewAspects} xyz.swapee.rc.BTestingPageViewAspects.typeof */
/** @typedef {typeof _idio.IRedirectMod} _idio.IRedirectMod.typeof */
/**
 * The aspects of the *ITestingPageView*.
 * @interface xyz.swapee.rc.ITestingPageViewAspects
 */
xyz.swapee.rc.ITestingPageViewAspects = class extends /** @type {xyz.swapee.rc.ITestingPageViewAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BTestingPageViewAspects.typeof&xyz.swapee.rc.ITestingPage.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.BTestingPageAspects.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingPageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ITestingPageViewAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.ITestingPageViewAspects&engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageViewAspects.Initialese>)} xyz.swapee.rc.TestingPageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewAspects} xyz.swapee.rc.ITestingPageViewAspects.typeof */
/**
 * A concrete class of _ITestingPageViewAspects_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewAspects
 * @implements {xyz.swapee.rc.ITestingPageViewAspects} The aspects of the *ITestingPageView*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageViewAspects.Initialese>} ‎
 */
xyz.swapee.rc.TestingPageViewAspects = class extends /** @type {xyz.swapee.rc.TestingPageViewAspects.constructor&xyz.swapee.rc.ITestingPageViewAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingPageViewAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.ITestingPageViewAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingPageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPageViewAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.TestingPageViewAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BTestingPageViewAspects_ interface.
 * @interface xyz.swapee.rc.BTestingPageViewAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BTestingPageViewAspectsCaster = class { }
/**
 * Cast the _BTestingPageViewAspects_ instance into the _BoundITestingPageView_ type.
 * @type {!xyz.swapee.rc.BoundITestingPageView}
 */
xyz.swapee.rc.BTestingPageViewAspectsCaster.prototype.asITestingPageView

/**
 * Contains getters to cast the _ITestingPageViewAspects_ interface.
 * @interface xyz.swapee.rc.ITestingPageViewAspectsCaster
 */
xyz.swapee.rc.ITestingPageViewAspectsCaster = class { }
/**
 * Cast the _ITestingPageViewAspects_ instance into the _BoundITestingPageView_ type.
 * @type {!xyz.swapee.rc.BoundITestingPageView}
 */
xyz.swapee.rc.ITestingPageViewAspectsCaster.prototype.asITestingPageView

/** @typedef {xyz.swapee.rc.ITestingPageView.Initialese} xyz.swapee.rc.IHyperTestingPageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.HyperTestingPageView)} xyz.swapee.rc.AbstractHyperTestingPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperTestingPageView} xyz.swapee.rc.HyperTestingPageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperTestingPageView` interface.
 * @constructor xyz.swapee.rc.AbstractHyperTestingPageView
 */
xyz.swapee.rc.AbstractHyperTestingPageView = class extends /** @type {xyz.swapee.rc.AbstractHyperTestingPageView.constructor&xyz.swapee.rc.HyperTestingPageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperTestingPageView.prototype.constructor = xyz.swapee.rc.AbstractHyperTestingPageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperTestingPageView.class = /** @type {typeof xyz.swapee.rc.AbstractHyperTestingPageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperTestingPageView|typeof xyz.swapee.rc.HyperTestingPageView)|(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperTestingPageView|typeof xyz.swapee.rc.HyperTestingPageView)|(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperTestingPageView|typeof xyz.swapee.rc.HyperTestingPageView)|(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.ITestingPageViewAspects|function(new: xyz.swapee.rc.ITestingPageViewAspects)} aides The list of aides that advise the ITestingPageView to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPageView.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperTestingPageView.Initialese[]) => xyz.swapee.rc.IHyperTestingPageView} xyz.swapee.rc.HyperTestingPageViewConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperTestingPageViewCaster&xyz.swapee.rc.ITestingPageView)} xyz.swapee.rc.IHyperTestingPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageView} xyz.swapee.rc.ITestingPageView.typeof */
/** @interface xyz.swapee.rc.IHyperTestingPageView */
xyz.swapee.rc.IHyperTestingPageView = class extends /** @type {xyz.swapee.rc.IHyperTestingPageView.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.ITestingPageView.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperTestingPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperTestingPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperTestingPageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperTestingPageView&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperTestingPageView.Initialese>)} xyz.swapee.rc.HyperTestingPageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperTestingPageView} xyz.swapee.rc.IHyperTestingPageView.typeof */
/**
 * A concrete class of _IHyperTestingPageView_ instances.
 * @constructor xyz.swapee.rc.HyperTestingPageView
 * @implements {xyz.swapee.rc.IHyperTestingPageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperTestingPageView.Initialese>} ‎
 */
xyz.swapee.rc.HyperTestingPageView = class extends /** @type {xyz.swapee.rc.HyperTestingPageView.constructor&xyz.swapee.rc.IHyperTestingPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperTestingPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperTestingPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperTestingPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperTestingPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperTestingPageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.HyperTestingPageView.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperTestingPageView} */
xyz.swapee.rc.RecordIHyperTestingPageView

/** @typedef {xyz.swapee.rc.IHyperTestingPageView} xyz.swapee.rc.BoundIHyperTestingPageView */

/** @typedef {xyz.swapee.rc.HyperTestingPageView} xyz.swapee.rc.BoundHyperTestingPageView */

/**
 * Contains getters to cast the _IHyperTestingPageView_ interface.
 * @interface xyz.swapee.rc.IHyperTestingPageViewCaster
 */
xyz.swapee.rc.IHyperTestingPageViewCaster = class { }
/**
 * Cast the _IHyperTestingPageView_ instance into the _BoundIHyperTestingPageView_ type.
 * @type {!xyz.swapee.rc.BoundIHyperTestingPageView}
 */
xyz.swapee.rc.IHyperTestingPageViewCaster.prototype.asIHyperTestingPageView
/**
 * Access the _HyperTestingPageView_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperTestingPageView}
 */
xyz.swapee.rc.IHyperTestingPageViewCaster.prototype.superHyperTestingPageView

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageViewHyperslice
 */
xyz.swapee.rc.ITestingPageViewHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewTesting=/** @type {!xyz.swapee.rc.ITestingPageView._viewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._viewTesting>} */ (void 0)
    /**
     * The internal view handler for the `testing` action.
     */
    this.getTesting=/** @type {!xyz.swapee.rc.ITestingPageView._getTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._getTesting>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageView._setTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._setTestingCtx>} */ (void 0)
    /**
     * The _Testing_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.TestingView=/** @type {!xyz.swapee.rc.ITestingPageView._TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._TestingView>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageViewHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageViewHyperslice

/**
 * A concrete class of _ITestingPageViewHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewHyperslice
 * @implements {xyz.swapee.rc.ITestingPageViewHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.TestingPageViewHyperslice = class extends xyz.swapee.rc.ITestingPageViewHyperslice { }
xyz.swapee.rc.TestingPageViewHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageViewHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.ITestingPageViewBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.ITestingPageViewBindingHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewTesting=/** @type {!xyz.swapee.rc.ITestingPageView.__viewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__viewTesting<THIS>>} */ (void 0)
    /**
     * The internal view handler for the `testing` action.
     */
    this.getTesting=/** @type {!xyz.swapee.rc.ITestingPageView.__getTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__getTesting<THIS>>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setTestingCtx=/** @type {!xyz.swapee.rc.ITestingPageView.__setTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__setTestingCtx<THIS>>} */ (void 0)
    /**
     * The _Testing_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.TestingView=/** @type {!xyz.swapee.rc.ITestingPageView.__TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__TestingView<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.ITestingPageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.ITestingPageViewBindingHyperslice

/**
 * A concrete class of _ITestingPageViewBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.TestingPageViewBindingHyperslice
 * @implements {xyz.swapee.rc.ITestingPageViewBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.ITestingPageViewBindingHyperslice<THIS>}
 */
xyz.swapee.rc.TestingPageViewBindingHyperslice = class extends xyz.swapee.rc.ITestingPageViewBindingHyperslice { }
xyz.swapee.rc.TestingPageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.TestingPageViewBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.ITestingPageViewFields&engineering.type.IEngineer&xyz.swapee.rc.ITestingPageViewCaster&_idio.IRedirectMod&xyz.swapee.rc.UTestingPageView)} xyz.swapee.rc.ITestingPageView.constructor */
/** @interface xyz.swapee.rc.ITestingPageView */
xyz.swapee.rc.ITestingPageView = class extends /** @type {xyz.swapee.rc.ITestingPageView.constructor&engineering.type.IEngineer.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.UTestingPageView.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ITestingPageView.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.ITestingPageView.viewTesting} */
xyz.swapee.rc.ITestingPageView.prototype.viewTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageView.getTesting} */
xyz.swapee.rc.ITestingPageView.prototype.getTesting = function() {}
/** @type {xyz.swapee.rc.ITestingPageView.setTestingCtx} */
xyz.swapee.rc.ITestingPageView.prototype.setTestingCtx = function() {}
/** @type {xyz.swapee.rc.ITestingPageView.TestingPartial} */
xyz.swapee.rc.ITestingPageView.prototype.TestingPartial = function() {}
/** @type {xyz.swapee.rc.ITestingPageView.TestingView} */
xyz.swapee.rc.ITestingPageView.prototype.TestingView = function() {}

/** @typedef {function(new: xyz.swapee.rc.ITestingPageView&engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageView.Initialese>)} xyz.swapee.rc.TestingPageView.constructor */
/**
 * A concrete class of _ITestingPageView_ instances.
 * @constructor xyz.swapee.rc.TestingPageView
 * @implements {xyz.swapee.rc.ITestingPageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageView.Initialese>} ‎
 */
xyz.swapee.rc.TestingPageView = class extends /** @type {xyz.swapee.rc.TestingPageView.constructor&xyz.swapee.rc.ITestingPageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingPageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.ITestingPageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingPageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingPageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingPageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.TestingPageView.__extend = function(...Extensions) {}

/**
 * Fields of the ITestingPageView.
 * @interface xyz.swapee.rc.ITestingPageViewFields
 */
xyz.swapee.rc.ITestingPageViewFields = class { }
/**
 * The navs for _GET_ paths (e.g., to use for redirects).
 */
xyz.swapee.rc.ITestingPageViewFields.prototype.GET = /** @type {!xyz.swapee.rc.ITestingPageView.GET} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPageViewFields.prototype.testingTranslations = /** @type {!Object<string, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation>} */ (void 0)

/** @typedef {xyz.swapee.rc.ITestingPageView} */
xyz.swapee.rc.RecordITestingPageView

/** @typedef {xyz.swapee.rc.ITestingPageView} xyz.swapee.rc.BoundITestingPageView */

/** @typedef {xyz.swapee.rc.TestingPageView} xyz.swapee.rc.BoundTestingPageView */

/**
 * @typedef {Object} xyz.swapee.rc.ITestingPageView.GET The navs for _GET_ paths (e.g., to use for redirects).
 * @prop {xyz.swapee.rc.Testing.testingNav} testing
 */

/**
 * Contains getters to cast the _ITestingPageView_ interface.
 * @interface xyz.swapee.rc.ITestingPageViewCaster
 */
xyz.swapee.rc.ITestingPageViewCaster = class { }
/**
 * Cast the _ITestingPageView_ instance into the _BoundITestingPageView_ type.
 * @type {!xyz.swapee.rc.BoundITestingPageView}
 */
xyz.swapee.rc.ITestingPageViewCaster.prototype.asITestingPageView
/**
 * Cast the _ITestingPageView_ instance into the _BoundITestingPage_ type.
 * @type {!xyz.swapee.rc.BoundITestingPage}
 */
xyz.swapee.rc.ITestingPageViewCaster.prototype.asITestingPage
/**
 * Access the _TestingPageView_ prototype.
 * @type {!xyz.swapee.rc.BoundTestingPageView}
 */
xyz.swapee.rc.ITestingPageViewCaster.prototype.superTestingPageView

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeViewTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeViewTesting} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPageView.ViewTestingNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewTesting` method from being executed.
 * - `sub` _(value: !ITestingPageView.TestingView) =&gt; void_ Cancels a call to `viewTesting` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeViewTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTesting} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `after` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `viewTesting` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `afterReturns` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `sub` _(value: !ITestingPageView.TestingView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachViewTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachViewTesting} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPageView.ViewTestingNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewTesting` method from being executed.
 * - `sub` _(value: !ITestingPageView.TestingView) =&gt; void_ Cancels a call to `viewTesting` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachViewTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTesting} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `after` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTestingReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTestingReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data] Metadata passed to the pointcuts of _viewTesting_ at the `afterEach` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `args` _ITestingPageView.ViewTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.ViewTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTestingReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeGetTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeGetTesting} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPageView.GetTestingNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getTesting` method from being executed.
 * - `sub` _(value: !ITestingPageView.TestingView) =&gt; void_ Cancels a call to `getTesting` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeGetTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTesting} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `after` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getTesting` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `afterReturns` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `sub` _(value: !ITestingPageView.TestingView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachGetTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachGetTesting} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPageView.GetTestingNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getTesting` method from being executed.
 * - `sub` _(value: !ITestingPageView.TestingView) =&gt; void_ Cancels a call to `getTesting` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachGetTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTesting} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `after` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTesting = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTestingReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTestingReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data] Metadata passed to the pointcuts of _getTesting_ at the `afterEach` joinpoint.
 * - `res` _!ITestingPageView.TestingView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `args` _ITestingPageView.GetTestingNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.GetTestingPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTestingReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeSetTestingCtx */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeSetTestingCtx} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPageView.SetTestingCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setTestingCtx` method from being executed.
 * - `sub` _(value: !ITestingPage.testing.OptCtx) =&gt; void_ Cancels a call to `setTestingCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeSetTestingCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtx */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtx} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `after` joinpoint.
 * - `res` _!ITestingPage.testing.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `setTestingCtx` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `afterReturns` joinpoint.
 * - `res` _!ITestingPage.testing.OptCtx_ The return of the method after it's successfully run.
 * - `sub` _(value: !ITestingPage.testing.OptCtx) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachSetTestingCtx */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachSetTestingCtx} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPageView.SetTestingCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setTestingCtx` method from being executed.
 * - `sub` _(value: !ITestingPage.testing.OptCtx) =&gt; void_ Cancels a call to `setTestingCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachSetTestingCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtx */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtx} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `after` joinpoint.
 * - `res` _!ITestingPage.testing.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtxReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtxReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data] Metadata passed to the pointcuts of _setTestingCtx_ at the `afterEach` joinpoint.
 * - `res` _!ITestingPage.testing.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `args` _ITestingPageView.SetTestingCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.SetTestingCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingPartial */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingPartial} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData} [data] Metadata passed to the pointcuts of _TestingPartial_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ITestingPageView.TestingPartialNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `TestingPartial` method from being executed.
 * - `sub` _(value: engineering.type.VNode) =&gt; void_ Cancels a call to `TestingPartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `args` _ITestingPageView.TestingPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingPartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartial */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartial} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData} [data] Metadata passed to the pointcuts of _TestingPartial_ at the `after` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `args` _ITestingPageView.TestingPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData} [data] Metadata passed to the pointcuts of _TestingPartial_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `TestingPartial` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `args` _ITestingPageView.TestingPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData} [data] Metadata passed to the pointcuts of _TestingPartial_ at the `afterReturns` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `sub` _(value: engineering.type.VNode) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `args` _ITestingPageView.TestingPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData} [data] Metadata passed to the pointcuts of _TestingPartial_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `args` _ITestingPageView.TestingPartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingPartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingView */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingView} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `TestingView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingView */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingView} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewThrows */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `TestingView` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewCancels */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEach_TestingView */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEach_TestingView} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `TestingView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEach_TestingView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingView */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingView} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData) => void} xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns<!xyz.swapee.rc.ITestingPageViewJoinpointModel>} xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingViewReturns */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingViewReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data] Metadata passed to the pointcuts of _TestingView_ at the `afterEach` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ITestingPageViewJoinpointModel.TestingViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.ITestingPage.testing.Ctx, form: !xyz.swapee.rc.ITestingPage.testing.Form) => !xyz.swapee.rc.ITestingPageView.TestingView} xyz.swapee.rc.ITestingPageView.__viewTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageView.__viewTesting<!xyz.swapee.rc.ITestingPageView>} xyz.swapee.rc.ITestingPageView._viewTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageView.viewTesting} */
/**
 * Assigns required view data to the context, then redirects, or invokes another pagelet. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx The context.
 * - `validation` _!ITestingPage.testing.Validation_ The validation.
 * - `errors` _!ITestingPage.testing.Errors_ The errors.
 * - `answers` _!ITestingPage.testing.Answers_ The answers.
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form The form.
 * - `locale` _string_
 * @return {!xyz.swapee.rc.ITestingPageView.TestingView}
 */
xyz.swapee.rc.ITestingPageView.viewTesting = function(ctx, form) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.ITestingPage.testing.Ctx, form: !xyz.swapee.rc.ITestingPage.testing.Form) => (void|!xyz.swapee.rc.ITestingPageView.TestingView)} xyz.swapee.rc.ITestingPageView.__getTesting
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageView.__getTesting<!xyz.swapee.rc.ITestingPageView>} xyz.swapee.rc.ITestingPageView._getTesting */
/** @typedef {typeof xyz.swapee.rc.ITestingPageView.getTesting} */
/**
 * The internal view handler for the `testing` action. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx The context.
 * - `validation` _!ITestingPage.testing.Validation_ The validation.
 * - `errors` _!ITestingPage.testing.Errors_ The errors.
 * - `answers` _!ITestingPage.testing.Answers_ The answers.
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form The form.
 * - `locale` _string_
 * @return {void|!xyz.swapee.rc.ITestingPageView.TestingView}
 */
xyz.swapee.rc.ITestingPageView.getTesting = function(ctx, form) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.ITestingPage.testing.Answers, translation: !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation) => !xyz.swapee.rc.ITestingPage.testing.OptCtx} xyz.swapee.rc.ITestingPageView.__setTestingCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageView.__setTestingCtx<!xyz.swapee.rc.ITestingPageView>} xyz.swapee.rc.ITestingPageView._setTestingCtx */
/** @typedef {typeof xyz.swapee.rc.ITestingPageView.setTestingCtx} */
/**
 * Assigns the context based on answers and translations. `🔗 $combine`
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFixedOffer.Answers*
 * - `changenowOffer` _&#42;_ ⤴ *ITestingPage.filterChangenowOffer.Answers*
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation The translation for the user's locale.
 * @return {!xyz.swapee.rc.ITestingPage.testing.OptCtx}
 */
xyz.swapee.rc.ITestingPageView.setTestingCtx = function(answers, translation) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.ITestingPage.testing.Answers, errors: !xyz.swapee.rc.ITestingPageView.TestingPartial.Errors, actions: !xyz.swapee.rc.ITestingPageView.TestingPartial.Actions, translation: !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation) => engineering.type.VNode} xyz.swapee.rc.ITestingPageView.__TestingPartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageView.__TestingPartial<!xyz.swapee.rc.ITestingPageView>} xyz.swapee.rc.ITestingPageView._TestingPartial */
/** @typedef {typeof xyz.swapee.rc.ITestingPageView.TestingPartial} */
/**
 * The partial.
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFixedOffer.Answers*
 * - `changenowOffer` _&#42;_ ⤴ *ITestingPage.filterChangenowOffer.Answers*
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Actions} actions The actions.
 * - `testing` _Testing.testingNav_
 * - `_testing` _!ITestingPage.testing.Form_
 * - `_filterChangellyFloatingOffer` _ITestingPage.filterChangellyFloatingOffer.Form_ The form of the `filterChangellyFloatingOffer` action.
 * - `filterChangellyFloatingOffer` _!Testing.filterChangellyFloatingOfferNav_
 * - `_filterChangellyFixedOffer` _ITestingPage.filterChangellyFixedOffer.Form_ The form of the `filterChangellyFixedOffer` action.
 * - `filterChangellyFixedOffer` _!Testing.filterChangellyFixedOfferNav_
 * - `_filterChangenowOffer` _ITestingPage.filterChangenowOffer.Form_ The form of the `filterChangenowOffer` action.
 * - `filterChangenowOffer` _!Testing.filterChangenowOfferNav_
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.ITestingPageView.TestingPartial = function(answers, errors, actions, translation) {}

/** @typedef {xyz.swapee.rc.ITestingPage.testing.Errors&xyz.swapee.rc.ITestingPage.testing.Validation} xyz.swapee.rc.ITestingPageView.TestingPartial.Errors The errors. */

/**
 * The actions.
 * @record xyz.swapee.rc.ITestingPageView.TestingPartial.Actions
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions = class { }
/**
 *
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.testing = /** @type {xyz.swapee.rc.Testing.testingNav} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._testing = /** @type {!xyz.swapee.rc.ITestingPage.testing.Form} */ (void 0)
/**
 * The form of the `filterChangellyFloatingOffer` action.
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._filterChangellyFloatingOffer = /** @type {xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.filterChangellyFloatingOffer = /** @type {!xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav} */ (void 0)
/**
 * The form of the `filterChangellyFixedOffer` action.
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._filterChangellyFixedOffer = /** @type {xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.filterChangellyFixedOffer = /** @type {!xyz.swapee.rc.Testing.filterChangellyFixedOfferNav} */ (void 0)
/**
 * The form of the `filterChangenowOffer` action.
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._filterChangenowOffer = /** @type {xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.filterChangenowOffer = /** @type {!xyz.swapee.rc.Testing.filterChangenowOfferNav} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ITestingPageView.TestingPartial.Translation The translation for the chosen user locale. */

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.ITestingPage.testing.Answers, errors: !xyz.swapee.rc.ITestingPageView.TestingPartial.Errors, actions: !xyz.swapee.rc.ITestingPageView.TestingPartial.Actions, translation: !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation) => engineering.type.VNode} xyz.swapee.rc.ITestingPageView.__TestingView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.ITestingPageView.__TestingView<!xyz.swapee.rc.ITestingPageView>} xyz.swapee.rc.ITestingPageView._TestingView */
/** @typedef {typeof xyz.swapee.rc.ITestingPageView.TestingView} */
/**
 * The _Testing_ view with partials and translation mechanics that has
 * access to answers written by the controller. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers The answers.
 * - `locale` _string_
 * - `changellyFloatingOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFloatingOffer.Answers*
 * - `changellyFixedOffer` _&#42;_ ⤴ *ITestingPage.filterChangellyFixedOffer.Answers*
 * - `changenowOffer` _&#42;_ ⤴ *ITestingPage.filterChangenowOffer.Answers*
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Actions} actions The actions.
 * - `testing` _Testing.testingNav_
 * - `_testing` _!ITestingPage.testing.Form_
 * - `_filterChangellyFloatingOffer` _ITestingPage.filterChangellyFloatingOffer.Form_ The form of the `filterChangellyFloatingOffer` action.
 * - `filterChangellyFloatingOffer` _!Testing.filterChangellyFloatingOfferNav_
 * - `_filterChangellyFixedOffer` _ITestingPage.filterChangellyFixedOffer.Form_ The form of the `filterChangellyFixedOffer` action.
 * - `filterChangellyFixedOffer` _!Testing.filterChangellyFixedOfferNav_
 * - `_filterChangenowOffer` _ITestingPage.filterChangenowOffer.Form_ The form of the `filterChangenowOffer` action.
 * - `filterChangenowOffer` _!Testing.filterChangenowOfferNav_
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.ITestingPageView.TestingView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc.ITestingPageView
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml}  9f341e7dc675a505975b1d1a6d53023a */
/**
 * The `testing` navigation metadata.
 * @constructor xyz.swapee.rc.Testing.TestingNav
 */
xyz.swapee.rc.Testing.TestingNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.Testing.TestingNav.prototype.testing = /** @type {number} */ (void 0)

/**
 * The `filterChangellyFloatingOffer` navigation metadata.
 * @constructor xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav
 */
xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav.prototype.filterChangellyFloatingOffer = /** @type {number} */ (void 0)

/**
 * The `filterChangellyFixedOffer` navigation metadata.
 * @constructor xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav
 */
xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav.prototype.filterChangellyFixedOffer = /** @type {number} */ (void 0)

/**
 * The `filterChangenowOffer` navigation metadata.
 * @constructor xyz.swapee.rc.Testing.FilterChangenowOfferNav
 */
xyz.swapee.rc.Testing.FilterChangenowOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.Testing.FilterChangenowOfferNav.prototype.filterChangenowOffer = /** @type {number} */ (void 0)

/**
 * The ids of the methods.
 * @constructor xyz.swapee.rc.testingMethodsIds
 */
xyz.swapee.rc.testingMethodsIds = class { }
xyz.swapee.rc.testingMethodsIds.prototype.constructor = xyz.swapee.rc.testingMethodsIds

/**
 * The ids of the arcs.
 * @constructor xyz.swapee.rc.testingArcsIds
 */
xyz.swapee.rc.testingArcsIds = class { }
xyz.swapee.rc.testingArcsIds.prototype.constructor = xyz.swapee.rc.testingArcsIds

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ITestingImpl.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.TestingImpl)} xyz.swapee.rc.AbstractTestingImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.TestingImpl} xyz.swapee.rc.TestingImpl.typeof */
/**
 * An abstract class of `xyz.swapee.rc.ITestingImpl` interface.
 * @constructor xyz.swapee.rc.AbstractTestingImpl
 */
xyz.swapee.rc.AbstractTestingImpl = class extends /** @type {xyz.swapee.rc.AbstractTestingImpl.constructor&xyz.swapee.rc.TestingImpl.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractTestingImpl.prototype.constructor = xyz.swapee.rc.AbstractTestingImpl
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractTestingImpl.class = /** @type {typeof xyz.swapee.rc.AbstractTestingImpl} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.ITestingImpl|typeof xyz.swapee.rc.TestingImpl)|(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingImpl.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractTestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.ITestingImpl|typeof xyz.swapee.rc.TestingImpl)|(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.ITestingImpl|typeof xyz.swapee.rc.TestingImpl)|(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.ITestingPageAspects&xyz.swapee.rc.ITestingPage&xyz.swapee.rc.ITestingPageHyperslice&xyz.swapee.rc.ITestingPageViewHyperslice)} xyz.swapee.rc.ITestingImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingPageHyperslice} xyz.swapee.rc.ITestingPageHyperslice.typeof */
/** @typedef {typeof xyz.swapee.rc.ITestingPageViewHyperslice} xyz.swapee.rc.ITestingPageViewHyperslice.typeof */
/** @interface xyz.swapee.rc.ITestingImpl */
xyz.swapee.rc.ITestingImpl = class extends /** @type {xyz.swapee.rc.ITestingImpl.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.ITestingPageAspects.typeof&xyz.swapee.rc.ITestingPage.typeof&xyz.swapee.rc.ITestingPageHyperslice.typeof&xyz.swapee.rc.ITestingPageViewHyperslice.typeof} */ (class {}) {
}
/**
 * Create a new *ITestingImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ITestingImpl.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.ITestingImpl&engineering.type.IInitialiser<!xyz.swapee.rc.ITestingImpl.Initialese>)} xyz.swapee.rc.TestingImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.ITestingImpl} xyz.swapee.rc.ITestingImpl.typeof */
/**
 * A concrete class of _ITestingImpl_ instances.
 * @constructor xyz.swapee.rc.TestingImpl
 * @implements {xyz.swapee.rc.ITestingImpl} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingImpl.Initialese>} ‎
 */
xyz.swapee.rc.TestingImpl = class extends /** @type {xyz.swapee.rc.TestingImpl.constructor&xyz.swapee.rc.ITestingImpl.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITestingImpl* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.ITestingImpl.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITestingImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.ITestingImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.TestingImpl.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.TestingImpl.__extend = function(...Extensions) {}

/** @typedef {typeof xyz.swapee.rc.Testing.testingNav} */
/**
 * Navigates to `testing`.
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form
 * - `locale` _string_
 * @return {void}
 */
xyz.swapee.rc.Testing.testingNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav} */
/**
 * Invokes the `filterChangellyFloatingOffer` action.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.Testing.filterChangellyFixedOfferNav} */
/**
 * Invokes the `filterChangellyFixedOffer` action.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form} form
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.Testing.filterChangellyFixedOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.Testing.filterChangenowOfferNav} */
/**
 * Invokes the `filterChangenowOffer` action.
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form} form
 * - `changeNow` _&#42;_
 * - `amountIn` _&#42;_
 * - `currencyIn` _&#42;_
 * - `currencyOut` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.Testing.filterChangenowOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.getTesting} */
/**
 * Allows to render the pagelet after it's been processed.
 * @return {void}
 */
xyz.swapee.rc.getTesting = function() {}

/** @typedef {typeof xyz.swapee.rc.testing} */
/**
 * @param {!xyz.swapee.rc.ITestingImpl} implementation
 * @return {!xyz.swapee.rc.ITestingPage}
 */
xyz.swapee.rc.testing = function(implementation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.Testing
/* @typal-end */