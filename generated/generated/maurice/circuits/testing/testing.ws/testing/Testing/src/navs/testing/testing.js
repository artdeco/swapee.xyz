import methodsIds from '../../../gen/methods-ids'
/** @type {xyz.swapee.rc.Testing.TestingNav} */
export const TestingNav={
 toString(){return'testing'},
 testing:methodsIds.testing,
}
export default TestingNav