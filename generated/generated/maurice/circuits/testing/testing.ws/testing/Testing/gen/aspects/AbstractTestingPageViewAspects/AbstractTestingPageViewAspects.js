import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingPageViewAspects}
 */
function __AbstractTestingPageViewAspects() {}
__AbstractTestingPageViewAspects.prototype = /** @type {!_AbstractTestingPageViewAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPageViewAspects}
 */
class _AbstractTestingPageViewAspects { }
/**
 * The aspects of the *ITestingPageView*.
 * @extends {xyz.swapee.rc.AbstractTestingPageViewAspects} ‎
 */
class AbstractTestingPageViewAspects extends newAspects(
 _AbstractTestingPageViewAspects,217430474518,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractTestingPageViewAspects} */
AbstractTestingPageViewAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractTestingPageViewAspects} */
function AbstractTestingPageViewAspectsClass(){}

export default AbstractTestingPageViewAspects


AbstractTestingPageViewAspects[$implementations]=[
 __AbstractTestingPageViewAspects,
]