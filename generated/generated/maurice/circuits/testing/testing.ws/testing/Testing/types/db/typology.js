/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.rc.ITestingPageJoinpointModelHyperslice': {
  'id': 21743047451,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice': {
  'id': 21743047452,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageJoinpointModel': {
  'id': 21743047453,
  'symbols': {},
  'methods': {
   'beforeTesting': 1,
   'afterTesting': 2,
   'afterTestingThrows': 3,
   'afterTestingReturns': 4,
   'afterTestingCancels': 5,
   'beforeEachTesting': 6,
   'afterEachTesting': 7,
   'afterEachTestingReturns': 8,
   'beforeFilterChangenowOffer': 17,
   'afterFilterChangenowOffer': 18,
   'afterFilterChangenowOfferThrows': 19,
   'afterFilterChangenowOfferReturns': 20,
   'afterFilterChangenowOfferCancels': 21,
   'beforeEachFilterChangenowOffer': 22,
   'afterEachFilterChangenowOffer': 23,
   'afterEachFilterChangenowOfferReturns': 24,
   'beforeFilterChangellyFloatingOffer': 25,
   'afterFilterChangellyFloatingOffer': 26,
   'afterFilterChangellyFloatingOfferThrows': 27,
   'afterFilterChangellyFloatingOfferReturns': 28,
   'afterFilterChangellyFloatingOfferCancels': 29,
   'beforeEachFilterChangellyFloatingOffer': 30,
   'afterEachFilterChangellyFloatingOffer': 31,
   'afterEachFilterChangellyFloatingOfferReturns': 32,
   'beforeFilterChangellyFixedOffer': 33,
   'afterFilterChangellyFixedOffer': 34,
   'afterFilterChangellyFixedOfferThrows': 35,
   'afterFilterChangellyFixedOfferReturns': 36,
   'afterFilterChangellyFixedOfferCancels': 37,
   'beforeEachFilterChangellyFixedOffer': 38,
   'afterEachFilterChangellyFixedOffer': 39,
   'afterEachFilterChangellyFixedOfferReturns': 40
  }
 },
 'xyz.swapee.rc.ITestingPageAspectsInstaller': {
  'id': 21743047454,
  'symbols': {},
  'methods': {
   'testing': 1,
   'filterChangenowOffer': 3,
   'filterChangellyFloatingOffer': 4,
   'filterChangellyFixedOffer': 5
  }
 },
 'xyz.swapee.rc.UTestingPage': {
  'id': 21743047455,
  'symbols': {
   'testingPage': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BTestingPageAspects': {
  'id': 21743047456,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.ITestingPageAspects': {
  'id': 21743047457,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperTestingPage': {
  'id': 21743047458,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageHyperslice': {
  'id': 21743047459,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageBindingHyperslice': {
  'id': 217430474510,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPage': {
  'id': 217430474511,
  'symbols': {},
  'methods': {
   'testing': 1,
   'filterChangenowOffer': 3,
   'filterChangellyFloatingOffer': 4,
   'filterChangellyFixedOffer': 5
  }
 },
 'xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice': {
  'id': 217430474512,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice': {
  'id': 217430474513,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageViewJoinpointModel': {
  'id': 217430474514,
  'symbols': {},
  'methods': {
   'beforeViewTesting': 1,
   'afterViewTesting': 2,
   'afterViewTestingThrows': 3,
   'afterViewTestingReturns': 4,
   'afterViewTestingCancels': 5,
   'beforeEachViewTesting': 6,
   'afterEachViewTesting': 7,
   'afterEachViewTestingReturns': 8,
   'beforeGetTesting': 9,
   'afterGetTesting': 10,
   'afterGetTestingThrows': 11,
   'afterGetTestingReturns': 12,
   'afterGetTestingCancels': 13,
   'beforeEachGetTesting': 14,
   'afterEachGetTesting': 15,
   'afterEachGetTestingReturns': 16,
   'beforeSetTestingCtx': 17,
   'afterSetTestingCtx': 18,
   'afterSetTestingCtxThrows': 19,
   'afterSetTestingCtxReturns': 20,
   'afterSetTestingCtxCancels': 21,
   'beforeEachSetTestingCtx': 22,
   'afterEachSetTestingCtx': 23,
   'afterEachSetTestingCtxReturns': 24,
   'before_TestingPartial': 25,
   'after_TestingPartial': 26,
   'after_TestingPartialThrows': 27,
   'after_TestingPartialReturns': 28,
   'after_TestingPartialCancels': 29,
   'before_TestingView': 30,
   'after_TestingView': 31,
   'after_TestingViewThrows': 32,
   'after_TestingViewReturns': 33,
   'after_TestingViewCancels': 34,
   'beforeEach_TestingView': 35,
   'afterEach_TestingView': 36,
   'afterEach_TestingViewReturns': 37
  }
 },
 'xyz.swapee.rc.ITestingPageViewAspectsInstaller': {
  'id': 217430474515,
  'symbols': {},
  'methods': {
   'viewTesting': 1,
   'getTesting': 2,
   'setTestingCtx': 3,
   'TestingPartial': 4,
   'TestingView': 5
  }
 },
 'xyz.swapee.rc.UTestingPageView': {
  'id': 217430474516,
  'symbols': {
   'testingPageView': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BTestingPageViewAspects': {
  'id': 217430474517,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.ITestingPageViewAspects': {
  'id': 217430474518,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperTestingPageView': {
  'id': 217430474519,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageViewHyperslice': {
  'id': 217430474520,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageViewBindingHyperslice': {
  'id': 217430474521,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.ITestingPageView': {
  'id': 217430474522,
  'symbols': {},
  'methods': {
   'viewTesting': 1,
   'getTesting': 2,
   'setTestingCtx': 3,
   'TestingPartial': 4,
   'TestingView': 5
  }
 },
 'xyz.swapee.rc.ITestingImpl': {
  'id': 217430474523,
  'symbols': {},
  'methods': {}
 }
})