import '../../types'
import {ChangellyUniversal} from '@type.community/changelly.com'
import filterChangellyFloatingOffer from '../../../../../../../../../maurice/circuits/testing/Testing.mvc/src/TestingService/methods/filter-changelly-floating-offer'
import filterChangellyFixedOffer from '../../../../../../../../../maurice/circuits/testing/Testing.mvc/src/TestingService/methods/filter-changelly-fixed-offer'
import filterChangenowOffer from '../../../../../../../../../maurice/circuits/testing/Testing.mvc/src/TestingService/methods/filter-changenow-offer'
import {TestingCachePQs} from '../../../../../../../../../maurice/circuits/testing/Testing.mvc/pqs/TestingCachePQs'
import {TestingMemoryPQs} from '../../../../../../../../../maurice/circuits/testing/Testing.mvc/pqs/TestingMemoryPQs'
import AbstractHyperTestingPage from '../../gen/hyper/AbstractHyperTestingPage'
import HyperTestingPageView from '../HyperTestingPageView'
import {AssignAnswers} from '@websystems/parthenon'
import AbstractTestingPage from '../../gen/AbstractTestingPage'
import AbstractTestingPageAspects from '../../gen/aspects/AbstractTestingPageAspects'
import TestingPageMethodsIds from '../../gen/methods-ids'

/** @extends {xyz.swapee.rc.HyperTestingPage}  */
export default class HyperTestingPage extends AbstractHyperTestingPage.consults(
 HyperTestingPageView,
 AbstractTestingPageAspects.class.prototype=/**@type {!xyz.swapee.rc.TestingPageAspects} */({
  afterEachFilterChangellyFloatingOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterChangellyFixedOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterChangenowOfferReturns:[
   AssignAnswers,
  ],
  afterEachTestingReturns:[
   AssignAnswers,
  ],
 }),
 // todo: add HyperView here
 AbstractTestingPageAspects.class.prototype=/**@type {!xyz.swapee.rc.TestingPageAspects} */({ 
  afterFilterChangellyFloatingOffer:[
   function({args:{ctx:ctx,answers:answers}}){
    const body={}
    for(const key in answers){
     const q=PQs[key]
     body[q]=answers[key]
    }
    ctx.body={data:body}
   },
  ],
  afterFilterChangellyFixedOffer:[
   function({args:{ctx:ctx,answers:answers}}){
    const body={}
    for(const key in answers){
     const q=PQs[key]
     body[q]=answers[key]
    }
    ctx.body={data:body}
   },
  ],
  afterFilterChangenowOffer:[
   function({args:{ctx:ctx,answers:answers}}){
    const body={}
    for(const key in answers){
     const q=PQs[key]
     body[q]=answers[key]
    }
    ctx.body={data:body}
   },
  ],
 }),
).implements(
 AbstractTestingPage,
 ChangellyUniversal,
 AbstractHyperTestingPage.class.prototype=/**@type {!xyz.swapee.rc.TestingPageHyperslice} */({
  get _methodIds(){return TestingPageMethodsIds},
 }),
 AbstractHyperTestingPage.class.prototype=/**@type {!xyz.swapee.rc.TestingPageHyperslice} */({
  filterChangellyFloatingOffer:[
   filterChangellyFloatingOffer,
  ],
  filterChangellyFixedOffer:[
   filterChangellyFixedOffer,
  ],
  filterChangenowOffer:[
   filterChangenowOffer,
  ],
 }),
){}

const PQs={...TestingCachePQs,...TestingMemoryPQs}