import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingPageAspectsInstaller}
 */
function __TestingPageAspectsInstaller() {}
__TestingPageAspectsInstaller.prototype = /** @type {!_TestingPageAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPageAspectsInstaller}
 */
class _TestingPageAspectsInstaller { }

_TestingPageAspectsInstaller.prototype[$advice]=__TestingPageAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractTestingPageAspectsInstaller} ‎ */
class TestingPageAspectsInstaller extends newAbstract(
 _TestingPageAspectsInstaller,21743047454,null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractTestingPageAspectsInstaller} */
TestingPageAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractTestingPageAspectsInstaller} */
function TestingPageAspectsInstallerClass(){}

export default TestingPageAspectsInstaller


TestingPageAspectsInstaller[$implementations]=[
 TestingPageAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.ITestingPageAspectsInstaller}*/({
  testing(){
   this.beforeTesting=1
   this.afterTesting=2
   this.aroundTesting=3
   this.afterTestingThrows=4
   this.afterTestingReturns=5
   this.afterTestingCancels=7
   this.beforeEachTesting=8
   this.afterEachTesting=9
   this.afterEachTestingReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterChangellyFloatingOffer(){
   this.beforeFilterChangellyFloatingOffer=1
   this.afterFilterChangellyFloatingOffer=2
   this.aroundFilterChangellyFloatingOffer=3
   this.afterFilterChangellyFloatingOfferThrows=4
   this.afterFilterChangellyFloatingOfferReturns=5
   this.afterFilterChangellyFloatingOfferCancels=7
   this.beforeEachFilterChangellyFloatingOffer=8
   this.afterEachFilterChangellyFloatingOffer=9
   this.afterEachFilterChangellyFloatingOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterChangellyFixedOffer(){
   this.beforeFilterChangellyFixedOffer=1
   this.afterFilterChangellyFixedOffer=2
   this.aroundFilterChangellyFixedOffer=3
   this.afterFilterChangellyFixedOfferThrows=4
   this.afterFilterChangellyFixedOfferReturns=5
   this.afterFilterChangellyFixedOfferCancels=7
   this.beforeEachFilterChangellyFixedOffer=8
   this.afterEachFilterChangellyFixedOffer=9
   this.afterEachFilterChangellyFixedOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterChangenowOffer(){
   this.beforeFilterChangenowOffer=1
   this.afterFilterChangenowOffer=2
   this.aroundFilterChangenowOffer=3
   this.afterFilterChangenowOfferThrows=4
   this.afterFilterChangenowOfferReturns=5
   this.afterFilterChangenowOfferCancels=7
   this.beforeEachFilterChangenowOffer=8
   this.afterEachFilterChangenowOffer=9
   this.afterEachFilterChangenowOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
 }),
 __TestingPageAspectsInstaller,
]