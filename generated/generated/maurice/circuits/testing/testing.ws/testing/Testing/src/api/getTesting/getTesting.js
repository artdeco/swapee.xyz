import TestingPageMethodsIds from '../../../gen/methods-ids'
import '../../../types'

const methodsIds=Object.values(TestingPageMethodsIds)

/**@type {xyz.swapee.rc.ITestingPageView._getTesting} */
export default function _getTesting(ctx,form) {
 const actionId=parseFloat(form['action'])
 if(methodsIds.includes(actionId)) {
  const{asTestingPageView:{viewTesting:viewTesting}}=this
  const ev=viewTesting(ctx,form)
  return ev
 }
}