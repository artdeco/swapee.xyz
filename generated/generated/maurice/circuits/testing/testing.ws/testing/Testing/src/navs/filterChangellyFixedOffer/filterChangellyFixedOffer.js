import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav} */
export const FilterChangellyFixedOfferNav={
 toString(){return'filterChangellyFixedOffer'},
 filterChangellyFixedOffer:methodsIds.filterChangellyFixedOffer,
}
export default FilterChangellyFixedOfferNav