import '../../types'
import TestingPageView from '../TestingPageView'
import AbstractHyperTestingPageView from '../../gen/hyper/AbstractHyperTestingPageView'
import {AssignLocale} from '@websystems/parthenon'
import TestingPageArcsIds from '../../gen/arcs-ids'
import TestingPageMethodsIds from '../../gen/methods-ids'
import {RenderAide} from '@idio2/server'
import AbstractTestingPageViewAspects from '../../gen/aspects/AbstractTestingPageViewAspects'

/** @extends {xyz.swapee.rc.HyperTestingPageView}  */
export default class HyperTestingPageView extends AbstractHyperTestingPageView.consults(
 AbstractTestingPageViewAspects.class.prototype=/**@type {!xyz.swapee.rc.TestingPageViewAspects} */({
  beforeTesting:[
   AssignLocale,
  ],
  afterEachSetTestingCtxReturns:[
   function({res:res}) {
    const{asITestingPage:{asCContext:ctx}}=this
    Object.assign(ctx,res)
   },
  ],
  afterViewTestingReturns:[
   RenderAide.RenderActionableJSXCtx({
    ...TestingPageMethodsIds,
   },{
    attrsProcess:[
    ],
    arcs:TestingPageArcsIds,
   },{
    testingTranslations:1,
   }),
  ],
 }),
).implements(
 TestingPageView,
){}