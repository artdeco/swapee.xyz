import TestingPageUniversal, {setTestingPageUniversalSymbols} from './anchors/TestingPageUniversal'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTestingPage}
 */
function __AbstractTestingPage() {}
__AbstractTestingPage.prototype = /** @type {!_AbstractTestingPage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPage}
 */
class _AbstractTestingPage { }
/** @extends {xyz.swapee.rc.AbstractTestingPage} ‎ */
class AbstractTestingPage extends newAbstract(
 _AbstractTestingPage,217430474511,null,{
  asITestingPage:1,
  superTestingPage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractTestingPage} */
AbstractTestingPage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractTestingPage} */
function AbstractTestingPageClass(){}

export default AbstractTestingPage


AbstractTestingPage[$implementations]=[
 __AbstractTestingPage,
 TestingPageUniversal,
 AbstractTestingPageClass.prototype=/**@type {!xyz.swapee.rc.ITestingPage}*/({
  testing:precombined,
  filterChangellyFloatingOffer:precombined,
  filterChangellyFixedOffer:precombined,
  filterChangenowOffer:precombined,
 }),
]