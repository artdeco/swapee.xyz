/**
 * @fileoverview
 * @externs
 */

xyz.swapee.rc.ITestingPageJoinpointModel={}
xyz.swapee.rc.ITestingPage={}
xyz.swapee.rc.ITestingPageViewJoinpointModel={}
xyz.swapee.rc.ITestingPageView={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.rc={}
$$xyz.swapee.rc.ITestingPageJoinpointModel={}
$$xyz.swapee.rc.ITestingPage={}
$$xyz.swapee.rc.ITestingPageViewJoinpointModel={}
$$xyz.swapee.rc.ITestingPageView={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.Initialese  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 */
xyz.swapee.rc.ITestingPage.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageFields  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.ITestingPageFields
/** @type {!xyz.swapee.rc.ITestingPage.testing.Ctx} */
xyz.swapee.rc.ITestingPageFields.prototype.testingCtx

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageCaster  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.ITestingPageCaster
/** @type {!xyz.swapee.rc.BoundITestingPage} */
xyz.swapee.rc.ITestingPageCaster.prototype.asITestingPage
/** @type {!xyz.swapee.rc.BoundTestingPage} */
xyz.swapee.rc.ITestingPageCaster.prototype.superTestingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPageFields  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.UTestingPageFields
/** @type {xyz.swapee.rc.ITestingPage} */
xyz.swapee.rc.UTestingPageFields.prototype.testingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPageCaster  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.UTestingPageCaster
/** @type {!xyz.swapee.rc.BoundTestingPage} */
xyz.swapee.rc.UTestingPageCaster.prototype.asTestingPage
/** @type {!xyz.swapee.rc.BoundUTestingPage} */
xyz.swapee.rc.UTestingPageCaster.prototype.asUTestingPage
/** @type {!xyz.swapee.rc.BoundTestingPageUniversal} */
xyz.swapee.rc.UTestingPageCaster.prototype.superTestingPageUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @extends {xyz.swapee.rc.UTestingPageFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.UTestingPageCaster}
 */
xyz.swapee.rc.UTestingPage = function() {}
/** @param {...!xyz.swapee.rc.UTestingPage.Initialese} init */
xyz.swapee.rc.UTestingPage.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @extends {xyz.swapee.rc.ITestingPageFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.ITestingPageCaster}
 * @extends {com.changelly.UChangelly}
 * @extends {xyz.swapee.rc.UTestingPage}
 */
xyz.swapee.rc.ITestingPage = function() {}
/** @param {...!xyz.swapee.rc.ITestingPage.Initialese} init */
xyz.swapee.rc.ITestingPage.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.testing.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.testing.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx
 * @return {(undefined|!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)}
 */
xyz.swapee.rc.ITestingPage.prototype.testing = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)}
 */
xyz.swapee.rc.ITestingPage.prototype.filterChangellyFloatingOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)}
 */
xyz.swapee.rc.ITestingPage.prototype.filterChangellyFixedOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)}
 */
xyz.swapee.rc.ITestingPage.prototype.filterChangenowOffer = function(form, answers, validation, errors, ctx) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.ITestingPage.Initialese} init
 * @implements {xyz.swapee.rc.ITestingPage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPage.Initialese>}
 */
xyz.swapee.rc.TestingPage = function(...init) {}
/** @param {...!xyz.swapee.rc.ITestingPage.Initialese} init */
xyz.swapee.rc.TestingPage.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.TestingPage.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.AbstractTestingPage = function() {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)|!xyz.swapee.rc.ITestingPageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPage.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)|!xyz.swapee.rc.ITestingPageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)|!xyz.swapee.rc.ITestingPageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPage}
 */
xyz.swapee.rc.AbstractTestingPage.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModelHyperslice  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeTesting>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTesting>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingThrows>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterTestingThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingCancels>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterTestingCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachTesting>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeEachTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTesting>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTestingReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferThrows>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOfferThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferCancels>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFloatingOfferCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFixedOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferThrows>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOfferThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferCancels>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangellyFixedOfferCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFixedOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangenowOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferThrows>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangenowOfferThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangenowOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferCancels>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterFilterChangenowOfferCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangenowOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.beforeEachFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOffer>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOfferReturns>)} */
xyz.swapee.rc.ITestingPageJoinpointModelHyperslice.prototype.afterEachFilterChangenowOfferReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageJoinpointModelHyperslice  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageJoinpointModelHyperslice}
 */
xyz.swapee.rc.TestingPageJoinpointModelHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterTestingThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterTestingCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeEachTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachTesting
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOfferThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFloatingOfferCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFloatingOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOfferThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangellyFixedOfferCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangellyFixedOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangenowOfferThrows
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangenowOfferReturns
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterFilterChangenowOfferCancels
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.beforeEachFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangenowOffer
/** @type {(!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice.prototype.afterEachFilterChangenowOfferReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageJoinpointModelBindingHyperslice  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.TestingPageJoinpointModelBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.ITestingPageJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTestingThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTestingReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterTestingCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachTestingReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFloatingOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFloatingOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangellyFixedOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangellyFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeFilterChangenowOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterFilterChangenowOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.beforeEachFilterChangenowOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangenowOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.prototype.afterEachFilterChangenowOfferReturns = function(data) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageJoinpointModel  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageJoinpointModel}
 */
xyz.swapee.rc.TestingPageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.RecordITestingPageJoinpointModel  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ beforeTesting: xyz.swapee.rc.ITestingPageJoinpointModel.beforeTesting, afterTesting: xyz.swapee.rc.ITestingPageJoinpointModel.afterTesting, afterTestingThrows: xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingThrows, afterTestingReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingReturns, afterTestingCancels: xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingCancels, beforeEachTesting: xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachTesting, afterEachTesting: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTesting, afterEachTestingReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTestingReturns, beforeFilterChangellyFloatingOffer: xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFloatingOffer, afterFilterChangellyFloatingOffer: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOffer, afterFilterChangellyFloatingOfferThrows: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferThrows, afterFilterChangellyFloatingOfferReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferReturns, afterFilterChangellyFloatingOfferCancels: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferCancels, beforeEachFilterChangellyFloatingOffer: xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFloatingOffer, afterEachFilterChangellyFloatingOffer: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOffer, afterEachFilterChangellyFloatingOfferReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns, beforeFilterChangellyFixedOffer: xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFixedOffer, afterFilterChangellyFixedOffer: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOffer, afterFilterChangellyFixedOfferThrows: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferThrows, afterFilterChangellyFixedOfferReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferReturns, afterFilterChangellyFixedOfferCancels: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferCancels, beforeEachFilterChangellyFixedOffer: xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFixedOffer, afterEachFilterChangellyFixedOffer: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOffer, afterEachFilterChangellyFixedOfferReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns, beforeFilterChangenowOffer: xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangenowOffer, afterFilterChangenowOffer: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOffer, afterFilterChangenowOfferThrows: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferThrows, afterFilterChangenowOfferReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferReturns, afterFilterChangenowOfferCancels: xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferCancels, beforeEachFilterChangenowOffer: xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangenowOffer, afterEachFilterChangenowOffer: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOffer, afterEachFilterChangenowOfferReturns: xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOfferReturns }} */
xyz.swapee.rc.RecordITestingPageJoinpointModel

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundITestingPageJoinpointModel  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordITestingPageJoinpointModel}
 */
xyz.swapee.rc.BoundITestingPageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundTestingPageJoinpointModel  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundITestingPageJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundTestingPageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeTesting  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeTesting

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterTesting  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterTesting

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingThrows  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingThrows

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingCancels  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterTestingCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterTestingCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterTestingCancels

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachTesting  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachTesting

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTesting  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTesting

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTestingReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachTestingReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachTestingReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachTestingReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFloatingOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferThrows  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferThrows

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferCancels  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFloatingOfferCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFloatingOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFloatingOfferCancels

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFloatingOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFloatingOfferReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFloatingOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFloatingOfferReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFixedOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangellyFixedOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferThrows  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferThrows

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferCancels  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangellyFixedOfferCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangellyFixedOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangellyFixedOfferCancels

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFixedOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangellyFixedOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangellyFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangellyFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangellyFixedOfferReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangenowOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeFilterChangenowOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeFilterChangenowOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeFilterChangenowOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferThrows  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferThrows

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferCancels  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterFilterChangenowOfferCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterFilterChangenowOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterFilterChangenowOfferCancels

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangenowOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.beforeEachFilterChangenowOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._beforeEachFilterChangenowOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__beforeEachFilterChangenowOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOffer

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOfferReturns  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.afterEachFilterChangenowOfferReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageJoinpointModel, !xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData=): void} */
xyz.swapee.rc.ITestingPageJoinpointModel._afterEachFilterChangenowOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns} */
xyz.swapee.rc.ITestingPageJoinpointModel.__afterEachFilterChangenowOfferReturns

// nss:xyz.swapee.rc.ITestingPageJoinpointModel,$$xyz.swapee.rc.ITestingPageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageAspectsInstaller  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
xyz.swapee.rc.ITestingPageAspectsInstaller = function() {}
/** @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese} init */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterTestingThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterTestingReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterTestingCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeEachTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachTestingReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFloatingOfferThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFloatingOfferCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeEachFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachFilterChangellyFloatingOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachFilterChangellyFloatingOfferReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFixedOfferThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangellyFixedOfferCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeEachFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachFilterChangellyFixedOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachFilterChangellyFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeFilterChangenowOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangenowOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangenowOfferThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangenowOfferReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterFilterChangenowOfferCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.beforeEachFilterChangenowOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachFilterChangenowOffer
/** @type {number} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.afterEachFilterChangenowOfferReturns
/** @return {?} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.testing = function() {}
/** @return {?} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.filterChangellyFloatingOffer = function() {}
/** @return {?} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.filterChangellyFixedOffer = function() {}
/** @return {?} */
xyz.swapee.rc.ITestingPageAspectsInstaller.prototype.filterChangenowOffer = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageAspectsInstaller  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.rc.ITestingPageAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese>}
 */
xyz.swapee.rc.TestingPageAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese} init */
xyz.swapee.rc.TestingPageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.TestingPageAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPageAspectsInstaller  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller = function() {}
/**
 * @param {...(!xyz.swapee.rc.ITestingPageAspectsInstaller|typeof xyz.swapee.rc.TestingPageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.ITestingPageAspectsInstaller|typeof xyz.swapee.rc.TestingPageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.ITestingPageAspectsInstaller|typeof xyz.swapee.rc.TestingPageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageAspectsInstallerConstructor  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {function(new: xyz.swapee.rc.ITestingPageAspectsInstaller, ...!xyz.swapee.rc.ITestingPageAspectsInstaller.Initialese)} */
xyz.swapee.rc.TestingPageAspectsInstallerConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.TestingNArgs  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ form: !xyz.swapee.rc.ITestingPage.testing.Form, answers: !xyz.swapee.rc.ITestingPage.testing.Answers, validation: !xyz.swapee.rc.ITestingPage.testing.Validation, errors: !xyz.swapee.rc.ITestingPage.testing.Errors, ctx: !xyz.swapee.rc.ITestingPage.testing.Ctx }} */
xyz.swapee.rc.ITestingPage.TestingNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPage.TestingNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPage.TestingNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeTestingPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData = function() {}
/** @type {(!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterTestingPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsTestingPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData = function() {}
/** @type {(!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData.prototype.res
/**
 * @param {(!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsTestingPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.TestingPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsTestingPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.FilterChangellyFloatingOfferNArgs  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ form: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form, answers: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers, validation: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation, errors: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors, ctx: !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx }} */
xyz.swapee.rc.ITestingPage.FilterChangellyFloatingOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPage.FilterChangellyFloatingOfferNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPage.FilterChangellyFloatingOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFloatingOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFloatingOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFloatingOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFloatingOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFloatingOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.FilterChangellyFixedOfferNArgs  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ form: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form, answers: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers, validation: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation, errors: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors, ctx: !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx }} */
xyz.swapee.rc.ITestingPage.FilterChangellyFixedOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPage.FilterChangellyFixedOfferNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPage.FilterChangellyFixedOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangellyFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangellyFixedOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangellyFixedOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangellyFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangellyFixedOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangellyFixedOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.FilterChangenowOfferNArgs  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ form: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form, answers: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers, validation: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation, errors: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors, ctx: !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx }} */
xyz.swapee.rc.ITestingPage.FilterChangenowOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPage.FilterChangenowOfferNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPage.FilterChangenowOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.BeforeFilterChangenowOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterFilterChangenowOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterThrowsFilterChangenowOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterReturnsFilterChangenowOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModel.FilterChangenowOfferPointcutData}
 */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageJoinpointModel.AfterCancelsFilterChangenowOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageConstructor  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {function(new: xyz.swapee.rc.ITestingPage, ...!xyz.swapee.rc.ITestingPage.Initialese)} */
xyz.swapee.rc.TestingPageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageMetaUniversal  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {symbol} */
xyz.swapee.rc.TestingPageMetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPage.Initialese  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.UTestingPage.Initialese = function() {}
/** @type {xyz.swapee.rc.ITestingPage|undefined} */
xyz.swapee.rc.UTestingPage.Initialese.prototype.testingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageUniversal  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.UTestingPage.Initialese} init
 * @implements {xyz.swapee.rc.UTestingPage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UTestingPage.Initialese>}
 */
xyz.swapee.rc.TestingPageUniversal = function(...init) {}
/** @param {...!xyz.swapee.rc.UTestingPage.Initialese} init */
xyz.swapee.rc.TestingPageUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.TestingPageUniversal.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPageUniversal  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal = function() {}
/**
 * @param {...(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.UTestingPage|typeof xyz.swapee.rc.UTestingPage)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageUniversal}
 */
xyz.swapee.rc.AbstractTestingPageUniversal.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPageUniversal.MetaUniversal  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {xyz.swapee.rc.TestingPageMetaUniversal} */
xyz.swapee.rc.AbstractTestingPageUniversal.MetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.UTestingPageConstructor  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {function(new: xyz.swapee.rc.UTestingPage, ...!xyz.swapee.rc.UTestingPage.Initialese)} */
xyz.swapee.rc.UTestingPageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.RecordUTestingPage  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordUTestingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundUTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.UTestingPageFields}
 * @extends {xyz.swapee.rc.RecordUTestingPage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.UTestingPageCaster}
 */
xyz.swapee.rc.BoundUTestingPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundTestingPageUniversal  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundUTestingPage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundTestingPageUniversal = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BTestingPageAspectsCaster  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.BTestingPageAspectsCaster
/** @type {!xyz.swapee.rc.BoundITestingPage} */
xyz.swapee.rc.BTestingPageAspectsCaster.prototype.asITestingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BTestingPageAspects  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @extends {xyz.swapee.rc.BTestingPageAspectsCaster<THIS>}
 * @extends {xyz.swapee.rc.ITestingPageJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.BTestingPageAspects = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageAspects.Initialese  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPageAspects.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageAspectsCaster  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.ITestingPageAspectsCaster
/** @type {!xyz.swapee.rc.BoundITestingPage} */
xyz.swapee.rc.ITestingPageAspectsCaster.prototype.asITestingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageAspects  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.ITestingPageAspectsCaster}
 * @extends {xyz.swapee.rc.BTestingPageAspects<!xyz.swapee.rc.ITestingPageAspects>}
 * @extends {com.changelly.UChangelly}
 */
xyz.swapee.rc.ITestingPageAspects = function() {}
/** @param {...!xyz.swapee.rc.ITestingPageAspects.Initialese} init */
xyz.swapee.rc.ITestingPageAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageAspects  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.ITestingPageAspects.Initialese} init
 * @implements {xyz.swapee.rc.ITestingPageAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageAspects.Initialese>}
 */
xyz.swapee.rc.TestingPageAspects = function(...init) {}
/** @param {...!xyz.swapee.rc.ITestingPageAspects.Initialese} init */
xyz.swapee.rc.TestingPageAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.TestingPageAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractTestingPageAspects  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects = function() {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageAspects}
 */
xyz.swapee.rc.AbstractTestingPageAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageAspectsConstructor  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {function(new: xyz.swapee.rc.ITestingPageAspects, ...!xyz.swapee.rc.ITestingPageAspects.Initialese)} */
xyz.swapee.rc.TestingPageAspectsConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.IHyperTestingPage.Initialese  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.Initialese}
 */
xyz.swapee.rc.IHyperTestingPage.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.IHyperTestingPageCaster  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.IHyperTestingPageCaster
/** @type {!xyz.swapee.rc.BoundIHyperTestingPage} */
xyz.swapee.rc.IHyperTestingPageCaster.prototype.asIHyperTestingPage
/** @type {!xyz.swapee.rc.BoundHyperTestingPage} */
xyz.swapee.rc.IHyperTestingPageCaster.prototype.superHyperTestingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.IHyperTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IHyperTestingPageCaster}
 * @extends {xyz.swapee.rc.ITestingPage}
 */
xyz.swapee.rc.IHyperTestingPage = function() {}
/** @param {...!xyz.swapee.rc.IHyperTestingPage.Initialese} init */
xyz.swapee.rc.IHyperTestingPage.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.HyperTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IHyperTestingPage.Initialese} init
 * @implements {xyz.swapee.rc.IHyperTestingPage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperTestingPage.Initialese>}
 */
xyz.swapee.rc.HyperTestingPage = function(...init) {}
/** @param {...!xyz.swapee.rc.IHyperTestingPage.Initialese} init */
xyz.swapee.rc.HyperTestingPage.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.HyperTestingPage.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.AbstractHyperTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage = function() {}
/**
 * @param {...((!xyz.swapee.rc.IHyperTestingPage|typeof xyz.swapee.rc.HyperTestingPage)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPage.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperTestingPage|typeof xyz.swapee.rc.HyperTestingPage)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperTestingPage|typeof xyz.swapee.rc.HyperTestingPage)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperTestingPage}
 */
xyz.swapee.rc.AbstractHyperTestingPage.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.ITestingPageAspects|function(new: xyz.swapee.rc.ITestingPageAspects))} aides
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPage.consults = function(...aides) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.HyperTestingPageConstructor  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {function(new: xyz.swapee.rc.IHyperTestingPage, ...!xyz.swapee.rc.IHyperTestingPage.Initialese)} */
xyz.swapee.rc.HyperTestingPageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.RecordIHyperTestingPage  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordIHyperTestingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundIHyperTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIHyperTestingPage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IHyperTestingPageCaster}
 */
xyz.swapee.rc.BoundIHyperTestingPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundHyperTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIHyperTestingPage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundHyperTestingPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageHyperslice  633a5580f6b3c974cf3d903865e4048b */
/** @interface */
xyz.swapee.rc.ITestingPageHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPage._testing|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._testing>)} */
xyz.swapee.rc.ITestingPageHyperslice.prototype.testing
/** @type {(!xyz.swapee.rc.ITestingPage._filterChangellyFloatingOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._filterChangellyFloatingOffer>)} */
xyz.swapee.rc.ITestingPageHyperslice.prototype.filterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPage._filterChangellyFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._filterChangellyFixedOffer>)} */
xyz.swapee.rc.ITestingPageHyperslice.prototype.filterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPage._filterChangenowOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage._filterChangenowOffer>)} */
xyz.swapee.rc.ITestingPageHyperslice.prototype.filterChangenowOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageHyperslice  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageHyperslice}
 */
xyz.swapee.rc.TestingPageHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPageBindingHyperslice  633a5580f6b3c974cf3d903865e4048b */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.ITestingPageBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPage.__testing<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__testing<THIS>>)} */
xyz.swapee.rc.ITestingPageBindingHyperslice.prototype.testing
/** @type {(!xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageBindingHyperslice.prototype.filterChangellyFloatingOffer
/** @type {(!xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageBindingHyperslice.prototype.filterChangellyFixedOffer
/** @type {(!xyz.swapee.rc.ITestingPage.__filterChangenowOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPage.__filterChangenowOffer<THIS>>)} */
xyz.swapee.rc.ITestingPageBindingHyperslice.prototype.filterChangenowOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.TestingPageBindingHyperslice  633a5580f6b3c974cf3d903865e4048b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.TestingPageBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.RecordITestingPage  633a5580f6b3c974cf3d903865e4048b */
/** @typedef {{ testing: xyz.swapee.rc.ITestingPage.testing, filterChangellyFloatingOffer: xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer, filterChangellyFixedOffer: xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer, filterChangenowOffer: xyz.swapee.rc.ITestingPage.filterChangenowOffer }} */
xyz.swapee.rc.RecordITestingPage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundITestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageFields}
 * @extends {xyz.swapee.rc.RecordITestingPage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.ITestingPageCaster}
 * @extends {com.changelly.BoundUChangelly}
 */
xyz.swapee.rc.BoundITestingPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.BoundTestingPage  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundITestingPage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundTestingPage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.testing.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.testing.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx
 * @return {(undefined|!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)}
 */
$$xyz.swapee.rc.ITestingPage.__testing = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Form, !xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPage.testing.Validation, !xyz.swapee.rc.ITestingPage.testing.Errors, !xyz.swapee.rc.ITestingPage.testing.Ctx): (undefined|!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)} */
xyz.swapee.rc.ITestingPage.testing
/** @typedef {function(this: xyz.swapee.rc.ITestingPage, !xyz.swapee.rc.ITestingPage.testing.Form, !xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPage.testing.Validation, !xyz.swapee.rc.ITestingPage.testing.Errors, !xyz.swapee.rc.ITestingPage.testing.Ctx): (undefined|!xyz.swapee.rc.ITestingPage.testing.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.testing.OptAnswers>)} */
xyz.swapee.rc.ITestingPage._testing
/** @typedef {typeof $$xyz.swapee.rc.ITestingPage.__testing} */
xyz.swapee.rc.ITestingPage.__testing

// nss:xyz.swapee.rc.ITestingPage,$$xyz.swapee.rc.ITestingPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx): (undefined|xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPage, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors, !xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx): (undefined|xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPage._filterChangellyFloatingOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer} */
xyz.swapee.rc.ITestingPage.__filterChangellyFloatingOffer

// nss:xyz.swapee.rc.ITestingPage,$$xyz.swapee.rc.ITestingPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx): (undefined|xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPage, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors, !xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx): (undefined|xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPage._filterChangellyFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer} */
xyz.swapee.rc.ITestingPage.__filterChangellyFixedOffer

// nss:xyz.swapee.rc.ITestingPage,$$xyz.swapee.rc.ITestingPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer  633a5580f6b3c974cf3d903865e4048b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form} form
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation} validation
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.ITestingPage.__filterChangenowOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx): (undefined|xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer
/** @typedef {function(this: xyz.swapee.rc.ITestingPage, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors, !xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx): (undefined|xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers>)} */
xyz.swapee.rc.ITestingPage._filterChangenowOffer
/** @typedef {typeof $$xyz.swapee.rc.ITestingPage.__filterChangenowOffer} */
xyz.swapee.rc.ITestingPage.__filterChangenowOffer

// nss:xyz.swapee.rc.ITestingPage,$$xyz.swapee.rc.ITestingPage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.Form  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.testing.Form = function() {}
/** @type {string} */
xyz.swapee.rc.ITestingPage.testing.Form.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers.prototype.changellyFloatingOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers.prototype.changellyFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers.prototype.changenowOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.Answers  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Answers}
 * @extends {xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Answers}
 * @extends {xyz.swapee.rc.ITestingPage.filterChangenowOffer.Answers}
 */
xyz.swapee.rc.ITestingPage.testing.Answers = function() {}
/** @type {string} */
xyz.swapee.rc.ITestingPage.testing.Answers.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers.prototype.changellyFloatingOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers.prototype.changellyFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers.prototype.changenowOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.OptAnswers  633a5580f6b3c974cf3d903865e4048b */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptAnswers}
 * @extends {xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptAnswers}
 * @extends {xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptAnswers}
 */
xyz.swapee.rc.ITestingPage.testing.OptAnswers = function() {}
/** @type {string|undefined} */
xyz.swapee.rc.ITestingPage.testing.OptAnswers.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.Validation  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.testing.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.Errors  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.testing.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.Ctx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.testing.Ctx = function() {}
/** @type {!xyz.swapee.rc.ITestingPage.testing.Validation} */
xyz.swapee.rc.ITestingPage.testing.Ctx.prototype.validation
/** @type {!xyz.swapee.rc.ITestingPage.testing.Errors} */
xyz.swapee.rc.ITestingPage.testing.Ctx.prototype.errors
/** @type {!xyz.swapee.rc.ITestingPage.testing.Answers} */
xyz.swapee.rc.ITestingPage.testing.Ctx.prototype.answers

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.testing.OptCtx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.testing.OptCtx = function() {}
/** @type {(!xyz.swapee.rc.ITestingPage.testing.Validation)|undefined} */
xyz.swapee.rc.ITestingPage.testing.OptCtx.prototype.validation
/** @type {(!xyz.swapee.rc.ITestingPage.testing.Errors)|undefined} */
xyz.swapee.rc.ITestingPage.testing.OptCtx.prototype.errors
/** @type {(!xyz.swapee.rc.ITestingPage.testing.Answers)|undefined} */
xyz.swapee.rc.ITestingPage.testing.OptCtx.prototype.answers

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.amountIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.currencyIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form.prototype.currencyOut

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptCtx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.amountIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.currencyIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form.prototype.currencyOut

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptCtx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.changeNow
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.amountIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.currencyIn
/** @type {*} */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form.prototype.currencyOut

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPage.xml} xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptCtx  633a5580f6b3c974cf3d903865e4048b */
/** @record */
xyz.swapee.rc.ITestingPage.filterChangenowOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing  c12b9bb72618d701304f17934ecdaa53 */
/** @constructor */
xyz.swapee.rc.Testing = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Answers  c12b9bb72618d701304f17934ecdaa53 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.testing.Answers}
 */
xyz.swapee.rc.Testing.Answers = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Answers.Props  c12b9bb72618d701304f17934ecdaa53 */
/** @record */
xyz.swapee.rc.Testing.Answers.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Form  c12b9bb72618d701304f17934ecdaa53 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.testing.Form}
 */
xyz.swapee.rc.Testing.Form = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Form.Props  c12b9bb72618d701304f17934ecdaa53 */
/** @record */
xyz.swapee.rc.Testing.Form.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Errors  c12b9bb72618d701304f17934ecdaa53 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.testing.Errors}
 */
xyz.swapee.rc.Testing.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Errors.Props  c12b9bb72618d701304f17934ecdaa53 */
/** @record */
xyz.swapee.rc.Testing.Errors.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Validation  c12b9bb72618d701304f17934ecdaa53 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.testing.Validation}
 */
xyz.swapee.rc.Testing.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Validation.Props  c12b9bb72618d701304f17934ecdaa53 */
/** @record */
xyz.swapee.rc.Testing.Validation.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Ctx  c12b9bb72618d701304f17934ecdaa53 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.testing.Ctx}
 */
xyz.swapee.rc.Testing.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageAliases.xml} xyz.swapee.rc.Testing.Ctx.Props  c12b9bb72618d701304f17934ecdaa53 */
/** @record */
xyz.swapee.rc.Testing.Ctx.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.Initialese  9c94d7079915a315fadd7437cea05033 */
/** @record */
xyz.swapee.rc.ITestingPageView.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewFields  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.ITestingPageViewFields
/** @type {!xyz.swapee.rc.ITestingPageView.GET} */
xyz.swapee.rc.ITestingPageViewFields.prototype.GET
/** @type {!Object<string, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation>} */
xyz.swapee.rc.ITestingPageViewFields.prototype.testingTranslations

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewCaster  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.ITestingPageViewCaster
/** @type {!xyz.swapee.rc.BoundITestingPageView} */
xyz.swapee.rc.ITestingPageViewCaster.prototype.asITestingPageView
/** @type {!xyz.swapee.rc.BoundITestingPage} */
xyz.swapee.rc.ITestingPageViewCaster.prototype.asITestingPage
/** @type {!xyz.swapee.rc.BoundTestingPageView} */
xyz.swapee.rc.ITestingPageViewCaster.prototype.superTestingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageViewFields  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.UTestingPageViewFields
/** @type {xyz.swapee.rc.ITestingPageView} */
xyz.swapee.rc.UTestingPageViewFields.prototype.testingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageViewCaster  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.UTestingPageViewCaster
/** @type {!xyz.swapee.rc.BoundTestingPageView} */
xyz.swapee.rc.UTestingPageViewCaster.prototype.asTestingPageView
/** @type {!xyz.swapee.rc.BoundUTestingPageView} */
xyz.swapee.rc.UTestingPageViewCaster.prototype.asUTestingPageView
/** @type {!xyz.swapee.rc.BoundTestingPageViewUniversal} */
xyz.swapee.rc.UTestingPageViewCaster.prototype.superTestingPageViewUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @extends {xyz.swapee.rc.UTestingPageViewFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.UTestingPageViewCaster}
 */
xyz.swapee.rc.UTestingPageView = function() {}
/** @param {...!xyz.swapee.rc.UTestingPageView.Initialese} init */
xyz.swapee.rc.UTestingPageView.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @extends {xyz.swapee.rc.ITestingPageViewFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.ITestingPageViewCaster}
 * @extends {_idio.IRedirectMod}
 * @extends {xyz.swapee.rc.UTestingPageView}
 */
xyz.swapee.rc.ITestingPageView = function() {}
/** @param {...!xyz.swapee.rc.ITestingPageView.Initialese} init */
xyz.swapee.rc.ITestingPageView.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form
 * @return {!xyz.swapee.rc.ITestingPageView.TestingView}
 */
xyz.swapee.rc.ITestingPageView.prototype.viewTesting = function(ctx, form) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form
 * @return {(undefined|!xyz.swapee.rc.ITestingPageView.TestingView)}
 */
xyz.swapee.rc.ITestingPageView.prototype.getTesting = function(ctx, form) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation
 * @return {!xyz.swapee.rc.ITestingPage.testing.OptCtx}
 */
xyz.swapee.rc.ITestingPageView.prototype.setTestingCtx = function(answers, translation) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Actions} actions
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.ITestingPageView.prototype.TestingPartial = function(answers, errors, actions, translation) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Actions} actions
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.ITestingPageView.prototype.TestingView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.ITestingPageView.Initialese} init
 * @implements {xyz.swapee.rc.ITestingPageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageView.Initialese>}
 */
xyz.swapee.rc.TestingPageView = function(...init) {}
/** @param {...!xyz.swapee.rc.ITestingPageView.Initialese} init */
xyz.swapee.rc.TestingPageView.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.TestingPageView.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView = function() {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)|!xyz.swapee.rc.ITestingPageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageView.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)|!xyz.swapee.rc.ITestingPageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)|!xyz.swapee.rc.ITestingPageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageView}
 */
xyz.swapee.rc.AbstractTestingPageView.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeViewTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.beforeViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingThrows>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterViewTestingThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterViewTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingCancels>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterViewTestingCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachViewTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.beforeEachViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEachViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTestingReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEachViewTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeGetTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.beforeGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingThrows>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterGetTestingThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterGetTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingCancels>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterGetTestingCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachGetTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.beforeEachGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTesting>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEachGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTestingReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTestingReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEachGetTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeSetTestingCtx>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.beforeSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtx>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxThrows>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterSetTestingCtxThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterSetTestingCtxReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxCancels>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterSetTestingCtxCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachSetTestingCtx>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.beforeEachSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtx>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEachSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtxReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEachSetTestingCtxReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingPartial>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.before_TestingPartial
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartial>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingPartial
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialThrows>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingPartialThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingPartialReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialCancels>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingPartialCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingView>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.before_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingView>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewThrows>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingViewThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingViewReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewCancels>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.after_TestingViewCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEach_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEach_TestingView>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.beforeEach_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingView>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEach_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingViewReturns>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice.prototype.afterEach_TestingViewReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewJoinpointModelHyperslice  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageViewJoinpointModelHyperslice}
 */
xyz.swapee.rc.TestingPageViewJoinpointModelHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.beforeViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterViewTestingThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterViewTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterViewTestingCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.beforeEachViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEachViewTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEachViewTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.beforeGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterGetTestingThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterGetTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterGetTestingCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.beforeEachGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEachGetTesting
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEachGetTestingReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.beforeSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterSetTestingCtxThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterSetTestingCtxReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterSetTestingCtxCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.beforeEachSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEachSetTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEachSetTestingCtxReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.before_TestingPartial
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingPartial
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingPartialThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingPartialReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingPartialCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.before_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingViewThrows
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingViewReturns
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.after_TestingViewCancels
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.beforeEach_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEach_TestingView
/** @type {(!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns<THIS>>)} */
xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice.prototype.afterEach_TestingViewReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewJoinpointModelBindingHyperslice  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.TestingPageViewJoinpointModelBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.ITestingPageViewJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeViewTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTestingThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTestingReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterViewTestingCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEachViewTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachViewTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachViewTestingReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeGetTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTestingThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTestingReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterGetTestingCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEachGetTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachGetTesting = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachGetTestingReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeSetTestingCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtxThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtxReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterSetTestingCtxCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEachSetTestingCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachSetTestingCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEachSetTestingCtxReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.before_TestingPartial = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartial = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartialThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartialReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingPartialCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.before_TestingView = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingView = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingViewThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingViewReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.after_TestingViewCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.beforeEach_TestingView = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEach_TestingView = function(data) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.prototype.afterEach_TestingViewReturns = function(data) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewJoinpointModel  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageViewJoinpointModel}
 */
xyz.swapee.rc.TestingPageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.RecordITestingPageViewJoinpointModel  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ beforeViewTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeViewTesting, afterViewTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTesting, afterViewTestingThrows: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingThrows, afterViewTestingReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingReturns, afterViewTestingCancels: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingCancels, beforeEachViewTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachViewTesting, afterEachViewTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTesting, afterEachViewTestingReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTestingReturns, beforeGetTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeGetTesting, afterGetTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTesting, afterGetTestingThrows: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingThrows, afterGetTestingReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingReturns, afterGetTestingCancels: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingCancels, beforeEachGetTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachGetTesting, afterEachGetTesting: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTesting, afterEachGetTestingReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTestingReturns, beforeSetTestingCtx: xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeSetTestingCtx, afterSetTestingCtx: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtx, afterSetTestingCtxThrows: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxThrows, afterSetTestingCtxReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxReturns, afterSetTestingCtxCancels: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxCancels, beforeEachSetTestingCtx: xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachSetTestingCtx, afterEachSetTestingCtx: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtx, afterEachSetTestingCtxReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtxReturns, before_TestingPartial: xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingPartial, after_TestingPartial: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartial, after_TestingPartialThrows: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialThrows, after_TestingPartialReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialReturns, after_TestingPartialCancels: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialCancels, before_TestingView: xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingView, after_TestingView: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingView, after_TestingViewThrows: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewThrows, after_TestingViewReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewReturns, after_TestingViewCancels: xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewCancels, beforeEach_TestingView: xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEach_TestingView, afterEach_TestingView: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingView, afterEach_TestingViewReturns: xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingViewReturns }} */
xyz.swapee.rc.RecordITestingPageViewJoinpointModel

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundITestingPageViewJoinpointModel  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordITestingPageViewJoinpointModel}
 */
xyz.swapee.rc.BoundITestingPageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundTestingPageViewJoinpointModel  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundITestingPageViewJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundTestingPageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeViewTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeViewTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeViewTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeViewTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingThrows  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingThrows

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingCancels  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterViewTestingCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterViewTestingCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterViewTestingCancels

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachViewTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachViewTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachViewTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachViewTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTestingReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachViewTestingReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachViewTestingReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachViewTestingReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeGetTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeGetTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeGetTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeGetTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingThrows  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingThrows

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingCancels  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterGetTestingCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterGetTestingCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterGetTestingCancels

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachGetTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachGetTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachGetTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachGetTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTesting

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTestingReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachGetTestingReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachGetTestingReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachGetTestingReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeSetTestingCtx  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeSetTestingCtx
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeSetTestingCtx
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeSetTestingCtx

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtx  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtx
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtx
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtx

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxThrows  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxThrows

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxCancels  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterSetTestingCtxCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterSetTestingCtxCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterSetTestingCtxCancels

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachSetTestingCtx  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEachSetTestingCtx
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEachSetTestingCtx
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEachSetTestingCtx

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtx  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtx
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtx
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtx

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtxReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEachSetTestingCtxReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEachSetTestingCtxReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEachSetTestingCtxReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingPartial  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingPartial
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingPartial
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingPartial

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartial  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartial
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartial
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartial

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialThrows  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialThrows

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialCancels  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingPartialCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingPartialCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingPartialCancels

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingView  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.before_TestingView
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._before_TestingView
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__before_TestingView

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingView  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingView
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingView
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingView

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewThrows  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewThrows
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewThrows
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewThrows

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewCancels  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.after_TestingViewCancels
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._after_TestingViewCancels
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__after_TestingViewCancels

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEach_TestingView  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.beforeEach_TestingView
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._beforeEach_TestingView
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__beforeEach_TestingView

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingView  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingView
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingView
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingView

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingViewReturns  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.afterEach_TestingViewReturns
/** @typedef {function(this: xyz.swapee.rc.ITestingPageViewJoinpointModel, !xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData=): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel._afterEach_TestingViewReturns
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.__afterEach_TestingViewReturns

// nss:xyz.swapee.rc.ITestingPageViewJoinpointModel,$$xyz.swapee.rc.ITestingPageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese  9c94d7079915a315fadd7437cea05033 */
/** @record */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewAspectsInstaller  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.ITestingPageInstaller}
 */
xyz.swapee.rc.ITestingPageViewAspectsInstaller = function() {}
/** @param {...!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese} init */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.beforeViewTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterViewTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterViewTestingThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterViewTestingReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterViewTestingCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.beforeEachViewTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEachViewTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEachViewTestingReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.beforeGetTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterGetTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterGetTestingThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterGetTestingReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterGetTestingCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.beforeEachGetTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEachGetTesting
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEachGetTestingReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.beforeSetTestingCtx
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterSetTestingCtx
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterSetTestingCtxThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterSetTestingCtxReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterSetTestingCtxCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.beforeEachSetTestingCtx
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEachSetTestingCtx
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEachSetTestingCtxReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.before_TestingPartial
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingPartial
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingPartialThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingPartialReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingPartialCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.before_TestingView
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingView
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingViewThrows
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingViewReturns
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.after_TestingViewCancels
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.beforeEach_TestingView
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEach_TestingView
/** @type {number} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.afterEach_TestingViewReturns
/** @return {?} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.viewTesting = function() {}
/** @return {?} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.getTesting = function() {}
/** @return {?} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.setTestingCtx = function() {}
/** @return {?} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.TestingPartial = function() {}
/** @return {void} */
xyz.swapee.rc.ITestingPageViewAspectsInstaller.prototype.TestingView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewAspectsInstaller  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.rc.ITestingPageViewAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese>}
 */
xyz.swapee.rc.TestingPageViewAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese} init */
xyz.swapee.rc.TestingPageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.TestingPageViewAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller = function() {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageViewAspectsInstaller|typeof xyz.swapee.rc.TestingPageViewAspectsInstaller)|(!xyz.swapee.rc.ITestingPageInstaller|typeof xyz.swapee.rc.TestingPageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageViewAspectsInstaller|typeof xyz.swapee.rc.TestingPageViewAspectsInstaller)|(!xyz.swapee.rc.ITestingPageInstaller|typeof xyz.swapee.rc.TestingPageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageViewAspectsInstaller|typeof xyz.swapee.rc.TestingPageViewAspectsInstaller)|(!xyz.swapee.rc.ITestingPageInstaller|typeof xyz.swapee.rc.TestingPageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractTestingPageViewAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewAspectsInstallerConstructor  9c94d7079915a315fadd7437cea05033 */
/** @typedef {function(new: xyz.swapee.rc.ITestingPageViewAspectsInstaller, ...!xyz.swapee.rc.ITestingPageViewAspectsInstaller.Initialese)} */
xyz.swapee.rc.TestingPageViewAspectsInstallerConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.ViewTestingNArgs  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ ctx: !xyz.swapee.rc.ITestingPage.testing.Ctx, form: !xyz.swapee.rc.ITestingPage.testing.Form }} */
xyz.swapee.rc.ITestingPageView.ViewTestingNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPageView.ViewTestingNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPageView.ViewTestingNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageView.TestingView} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeViewTestingPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData = function() {}
/** @type {!xyz.swapee.rc.ITestingPageView.TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterViewTestingPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsViewTestingPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData = function() {}
/** @type {!xyz.swapee.rc.ITestingPageView.TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.ITestingPageView.TestingView} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsViewTestingPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.ViewTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsViewTestingPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.GetTestingNArgs  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ ctx: !xyz.swapee.rc.ITestingPage.testing.Ctx, form: !xyz.swapee.rc.ITestingPage.testing.Form }} */
xyz.swapee.rc.ITestingPageView.GetTestingNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPageView.GetTestingNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPageView.GetTestingNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.ITestingPageView.TestingView} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeGetTestingPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData = function() {}
/** @type {!xyz.swapee.rc.ITestingPageView.TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterGetTestingPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsGetTestingPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData = function() {}
/** @type {!xyz.swapee.rc.ITestingPageView.TestingView} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.ITestingPageView.TestingView} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsGetTestingPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.GetTestingPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsGetTestingPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.SetTestingCtxNArgs  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ answers: !xyz.swapee.rc.ITestingPage.testing.Answers, translation: !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation }} */
xyz.swapee.rc.ITestingPageView.SetTestingCtxNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPageView.SetTestingCtxNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPageView.SetTestingCtxNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.OptCtx} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeSetTestingCtxPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData = function() {}
/** @type {!xyz.swapee.rc.ITestingPage.testing.OptCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterSetTestingCtxPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsSetTestingCtxPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData = function() {}
/** @type {!xyz.swapee.rc.ITestingPage.testing.OptCtx} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.ITestingPage.testing.OptCtx} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsSetTestingCtxPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.SetTestingCtxPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsSetTestingCtxPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.TestingPartialNArgs  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ answers: !xyz.swapee.rc.ITestingPage.testing.Answers, errors: !xyz.swapee.rc.ITestingPageView.TestingPartial.Errors, actions: !xyz.swapee.rc.ITestingPageView.TestingPartial.Actions, translation: !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation }} */
xyz.swapee.rc.ITestingPageView.TestingPartialNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.ITestingPageView.TestingPartialNArgs, proc: !Function }} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.ITestingPageView.TestingPartialNArgs} args
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {engineering.type.VNode} value
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingPartialPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData = function() {}
/** @type {engineering.type.VNode} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingPartialPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingPartialPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData = function() {}
/** @type {engineering.type.VNode} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData.prototype.res
/**
 * @param {engineering.type.VNode} value
 * @return {?}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingPartialPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingPartialPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingPartialPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.BeforeTestingViewPointcutData.prototype.cancel = function(reason) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterTestingViewPointcutData = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterThrowsTestingViewPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterReturnsTestingViewPointcutData = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModel.TestingViewPointcutData}
 */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.ITestingPageViewJoinpointModel.AfterCancelsTestingViewPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewConstructor  9c94d7079915a315fadd7437cea05033 */
/** @typedef {function(new: xyz.swapee.rc.ITestingPageView, ...!xyz.swapee.rc.ITestingPageView.Initialese)} */
xyz.swapee.rc.TestingPageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewMetaUniversal  9c94d7079915a315fadd7437cea05033 */
/** @typedef {symbol} */
xyz.swapee.rc.TestingPageViewMetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageView.Initialese  9c94d7079915a315fadd7437cea05033 */
/** @record */
xyz.swapee.rc.UTestingPageView.Initialese = function() {}
/** @type {xyz.swapee.rc.ITestingPageView|undefined} */
xyz.swapee.rc.UTestingPageView.Initialese.prototype.testingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewUniversal  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.UTestingPageView.Initialese} init
 * @implements {xyz.swapee.rc.UTestingPageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UTestingPageView.Initialese>}
 */
xyz.swapee.rc.TestingPageViewUniversal = function(...init) {}
/** @param {...!xyz.swapee.rc.UTestingPageView.Initialese} init */
xyz.swapee.rc.TestingPageViewUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.TestingPageViewUniversal.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageViewUniversal  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal = function() {}
/**
 * @param {...(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.UTestingPageView|typeof xyz.swapee.rc.UTestingPageView)} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewUniversal}
 */
xyz.swapee.rc.AbstractTestingPageViewUniversal.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageViewUniversal.MetaUniversal  9c94d7079915a315fadd7437cea05033 */
/** @typedef {xyz.swapee.rc.TestingPageViewMetaUniversal} */
xyz.swapee.rc.AbstractTestingPageViewUniversal.MetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.UTestingPageViewConstructor  9c94d7079915a315fadd7437cea05033 */
/** @typedef {function(new: xyz.swapee.rc.UTestingPageView, ...!xyz.swapee.rc.UTestingPageView.Initialese)} */
xyz.swapee.rc.UTestingPageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.RecordUTestingPageView  9c94d7079915a315fadd7437cea05033 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordUTestingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundUTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.UTestingPageViewFields}
 * @extends {xyz.swapee.rc.RecordUTestingPageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.UTestingPageViewCaster}
 */
xyz.swapee.rc.BoundUTestingPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundTestingPageViewUniversal  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundUTestingPageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundTestingPageViewUniversal = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BTestingPageViewAspectsCaster  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.BTestingPageViewAspectsCaster
/** @type {!xyz.swapee.rc.BoundITestingPageView} */
xyz.swapee.rc.BTestingPageViewAspectsCaster.prototype.asITestingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BTestingPageViewAspects  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @extends {xyz.swapee.rc.BTestingPageViewAspectsCaster<THIS>}
 * @extends {xyz.swapee.rc.ITestingPageViewJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.BTestingPageViewAspects = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewAspects.Initialese  9c94d7079915a315fadd7437cea05033 */
/** @record */
xyz.swapee.rc.ITestingPageViewAspects.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewAspectsCaster  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.ITestingPageViewAspectsCaster
/** @type {!xyz.swapee.rc.BoundITestingPageView} */
xyz.swapee.rc.ITestingPageViewAspectsCaster.prototype.asITestingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewAspects  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.ITestingPageViewAspectsCaster}
 * @extends {xyz.swapee.rc.BTestingPageViewAspects<!xyz.swapee.rc.ITestingPageViewAspects>}
 * @extends {xyz.swapee.rc.ITestingPage}
 * @extends {_idio.IRedirectMod}
 * @extends {xyz.swapee.rc.BTestingPageAspects}
 */
xyz.swapee.rc.ITestingPageViewAspects = function() {}
/** @param {...!xyz.swapee.rc.ITestingPageViewAspects.Initialese} init */
xyz.swapee.rc.ITestingPageViewAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewAspects  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.ITestingPageViewAspects.Initialese} init
 * @implements {xyz.swapee.rc.ITestingPageViewAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingPageViewAspects.Initialese>}
 */
xyz.swapee.rc.TestingPageViewAspects = function(...init) {}
/** @param {...!xyz.swapee.rc.ITestingPageViewAspects.Initialese} init */
xyz.swapee.rc.TestingPageViewAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.TestingPageViewAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractTestingPageViewAspects  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects = function() {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageViewAspects|typeof xyz.swapee.rc.TestingPageViewAspects)|(!xyz.swapee.rc.BTestingPageViewAspects|typeof xyz.swapee.rc.BTestingPageViewAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageViewAspects|typeof xyz.swapee.rc.TestingPageViewAspects)|(!xyz.swapee.rc.BTestingPageViewAspects|typeof xyz.swapee.rc.BTestingPageViewAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingPageViewAspects|typeof xyz.swapee.rc.TestingPageViewAspects)|(!xyz.swapee.rc.BTestingPageViewAspects|typeof xyz.swapee.rc.BTestingPageViewAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BTestingPageAspects|typeof xyz.swapee.rc.BTestingPageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.TestingPageViewAspects}
 */
xyz.swapee.rc.AbstractTestingPageViewAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewAspectsConstructor  9c94d7079915a315fadd7437cea05033 */
/** @typedef {function(new: xyz.swapee.rc.ITestingPageViewAspects, ...!xyz.swapee.rc.ITestingPageViewAspects.Initialese)} */
xyz.swapee.rc.TestingPageViewAspectsConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.IHyperTestingPageView.Initialese  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageView.Initialese}
 */
xyz.swapee.rc.IHyperTestingPageView.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.IHyperTestingPageViewCaster  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.IHyperTestingPageViewCaster
/** @type {!xyz.swapee.rc.BoundIHyperTestingPageView} */
xyz.swapee.rc.IHyperTestingPageViewCaster.prototype.asIHyperTestingPageView
/** @type {!xyz.swapee.rc.BoundHyperTestingPageView} */
xyz.swapee.rc.IHyperTestingPageViewCaster.prototype.superHyperTestingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.IHyperTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IHyperTestingPageViewCaster}
 * @extends {xyz.swapee.rc.ITestingPageView}
 */
xyz.swapee.rc.IHyperTestingPageView = function() {}
/** @param {...!xyz.swapee.rc.IHyperTestingPageView.Initialese} init */
xyz.swapee.rc.IHyperTestingPageView.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.HyperTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IHyperTestingPageView.Initialese} init
 * @implements {xyz.swapee.rc.IHyperTestingPageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperTestingPageView.Initialese>}
 */
xyz.swapee.rc.HyperTestingPageView = function(...init) {}
/** @param {...!xyz.swapee.rc.IHyperTestingPageView.Initialese} init */
xyz.swapee.rc.HyperTestingPageView.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.HyperTestingPageView.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.AbstractHyperTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView = function() {}
/**
 * @param {...((!xyz.swapee.rc.IHyperTestingPageView|typeof xyz.swapee.rc.HyperTestingPageView)|(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPageView.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperTestingPageView|typeof xyz.swapee.rc.HyperTestingPageView)|(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperTestingPageView|typeof xyz.swapee.rc.HyperTestingPageView)|(!xyz.swapee.rc.ITestingPageView|typeof xyz.swapee.rc.TestingPageView)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperTestingPageView}
 */
xyz.swapee.rc.AbstractHyperTestingPageView.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.ITestingPageViewAspects|function(new: xyz.swapee.rc.ITestingPageViewAspects))} aides
 * @return {typeof xyz.swapee.rc.AbstractHyperTestingPageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperTestingPageView.consults = function(...aides) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.HyperTestingPageViewConstructor  9c94d7079915a315fadd7437cea05033 */
/** @typedef {function(new: xyz.swapee.rc.IHyperTestingPageView, ...!xyz.swapee.rc.IHyperTestingPageView.Initialese)} */
xyz.swapee.rc.HyperTestingPageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.RecordIHyperTestingPageView  9c94d7079915a315fadd7437cea05033 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordIHyperTestingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundIHyperTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIHyperTestingPageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IHyperTestingPageViewCaster}
 */
xyz.swapee.rc.BoundIHyperTestingPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundHyperTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIHyperTestingPageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundHyperTestingPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewHyperslice  9c94d7079915a315fadd7437cea05033 */
/** @interface */
xyz.swapee.rc.ITestingPageViewHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPageView._viewTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._viewTesting>)} */
xyz.swapee.rc.ITestingPageViewHyperslice.prototype.viewTesting
/** @type {(!xyz.swapee.rc.ITestingPageView._getTesting|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._getTesting>)} */
xyz.swapee.rc.ITestingPageViewHyperslice.prototype.getTesting
/** @type {(!xyz.swapee.rc.ITestingPageView._setTestingCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._setTestingCtx>)} */
xyz.swapee.rc.ITestingPageViewHyperslice.prototype.setTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageView._TestingView|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView._TestingView>)} */
xyz.swapee.rc.ITestingPageViewHyperslice.prototype.TestingView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewHyperslice  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageViewHyperslice}
 */
xyz.swapee.rc.TestingPageViewHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageViewBindingHyperslice  9c94d7079915a315fadd7437cea05033 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.ITestingPageViewBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.ITestingPageView.__viewTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__viewTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewBindingHyperslice.prototype.viewTesting
/** @type {(!xyz.swapee.rc.ITestingPageView.__getTesting<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__getTesting<THIS>>)} */
xyz.swapee.rc.ITestingPageViewBindingHyperslice.prototype.getTesting
/** @type {(!xyz.swapee.rc.ITestingPageView.__setTestingCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__setTestingCtx<THIS>>)} */
xyz.swapee.rc.ITestingPageViewBindingHyperslice.prototype.setTestingCtx
/** @type {(!xyz.swapee.rc.ITestingPageView.__TestingView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.ITestingPageView.__TestingView<THIS>>)} */
xyz.swapee.rc.ITestingPageViewBindingHyperslice.prototype.TestingView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.TestingPageViewBindingHyperslice  9c94d7079915a315fadd7437cea05033 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.ITestingPageViewBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.TestingPageViewBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.RecordITestingPageView  9c94d7079915a315fadd7437cea05033 */
/** @typedef {{ viewTesting: xyz.swapee.rc.ITestingPageView.viewTesting, getTesting: xyz.swapee.rc.ITestingPageView.getTesting, setTestingCtx: xyz.swapee.rc.ITestingPageView.setTestingCtx, TestingPartial: xyz.swapee.rc.ITestingPageView.TestingPartial, TestingView: xyz.swapee.rc.ITestingPageView.TestingView }} */
xyz.swapee.rc.RecordITestingPageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundITestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPageViewFields}
 * @extends {xyz.swapee.rc.RecordITestingPageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.ITestingPageViewCaster}
 * @extends {_idio.BoundIRedirectMod}
 */
xyz.swapee.rc.BoundITestingPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.BoundTestingPageView  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundITestingPageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundTestingPageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.viewTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form
 * @return {!xyz.swapee.rc.ITestingPageView.TestingView}
 */
$$xyz.swapee.rc.ITestingPageView.__viewTesting = function(ctx, form) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Ctx, !xyz.swapee.rc.ITestingPage.testing.Form): !xyz.swapee.rc.ITestingPageView.TestingView} */
xyz.swapee.rc.ITestingPageView.viewTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageView, !xyz.swapee.rc.ITestingPage.testing.Ctx, !xyz.swapee.rc.ITestingPage.testing.Form): !xyz.swapee.rc.ITestingPageView.TestingView} */
xyz.swapee.rc.ITestingPageView._viewTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageView.__viewTesting} */
xyz.swapee.rc.ITestingPageView.__viewTesting

// nss:xyz.swapee.rc.ITestingPageView,$$xyz.swapee.rc.ITestingPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.getTesting  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.testing.Ctx} ctx
 * @param {!xyz.swapee.rc.ITestingPage.testing.Form} form
 * @return {(undefined|!xyz.swapee.rc.ITestingPageView.TestingView)}
 */
$$xyz.swapee.rc.ITestingPageView.__getTesting = function(ctx, form) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Ctx, !xyz.swapee.rc.ITestingPage.testing.Form): (undefined|!xyz.swapee.rc.ITestingPageView.TestingView)} */
xyz.swapee.rc.ITestingPageView.getTesting
/** @typedef {function(this: xyz.swapee.rc.ITestingPageView, !xyz.swapee.rc.ITestingPage.testing.Ctx, !xyz.swapee.rc.ITestingPage.testing.Form): (undefined|!xyz.swapee.rc.ITestingPageView.TestingView)} */
xyz.swapee.rc.ITestingPageView._getTesting
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageView.__getTesting} */
xyz.swapee.rc.ITestingPageView.__getTesting

// nss:xyz.swapee.rc.ITestingPageView,$$xyz.swapee.rc.ITestingPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.setTestingCtx  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation
 * @return {!xyz.swapee.rc.ITestingPage.testing.OptCtx}
 */
$$xyz.swapee.rc.ITestingPageView.__setTestingCtx = function(answers, translation) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation): !xyz.swapee.rc.ITestingPage.testing.OptCtx} */
xyz.swapee.rc.ITestingPageView.setTestingCtx
/** @typedef {function(this: xyz.swapee.rc.ITestingPageView, !xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation): !xyz.swapee.rc.ITestingPage.testing.OptCtx} */
xyz.swapee.rc.ITestingPageView._setTestingCtx
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageView.__setTestingCtx} */
xyz.swapee.rc.ITestingPageView.__setTestingCtx

// nss:xyz.swapee.rc.ITestingPageView,$$xyz.swapee.rc.ITestingPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.TestingPartial  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Actions} actions
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
$$xyz.swapee.rc.ITestingPageView.__TestingPartial = function(answers, errors, actions, translation) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPageView.TestingPartial.Errors, !xyz.swapee.rc.ITestingPageView.TestingPartial.Actions, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.ITestingPageView.TestingPartial
/** @typedef {function(this: xyz.swapee.rc.ITestingPageView, !xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPageView.TestingPartial.Errors, !xyz.swapee.rc.ITestingPageView.TestingPartial.Actions, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.ITestingPageView._TestingPartial
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageView.__TestingPartial} */
xyz.swapee.rc.ITestingPageView.__TestingPartial

// nss:xyz.swapee.rc.ITestingPageView,$$xyz.swapee.rc.ITestingPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.TestingView  9c94d7079915a315fadd7437cea05033 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.ITestingPage.testing.Answers} answers
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Errors} errors
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Actions} actions
 * @param {!xyz.swapee.rc.ITestingPageView.TestingPartial.Translation} translation
 * @return {engineering.type.VNode}
 */
$$xyz.swapee.rc.ITestingPageView.__TestingView = function(answers, errors, actions, translation) {}
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPageView.TestingPartial.Errors, !xyz.swapee.rc.ITestingPageView.TestingPartial.Actions, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.ITestingPageView.TestingView
/** @typedef {function(this: xyz.swapee.rc.ITestingPageView, !xyz.swapee.rc.ITestingPage.testing.Answers, !xyz.swapee.rc.ITestingPageView.TestingPartial.Errors, !xyz.swapee.rc.ITestingPageView.TestingPartial.Actions, !xyz.swapee.rc.ITestingPageView.TestingPartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.ITestingPageView._TestingView
/** @typedef {typeof $$xyz.swapee.rc.ITestingPageView.__TestingView} */
xyz.swapee.rc.ITestingPageView.__TestingView

// nss:xyz.swapee.rc.ITestingPageView,$$xyz.swapee.rc.ITestingPageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.GET  9c94d7079915a315fadd7437cea05033 */
/** @record */
xyz.swapee.rc.ITestingPageView.GET = function() {}
/** @type {xyz.swapee.rc.Testing.testingNav} */
xyz.swapee.rc.ITestingPageView.GET.prototype.testing

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.TestingPartial.Errors  9c94d7079915a315fadd7437cea05033 */
/**
 * @record
 * @extends {xyz.swapee.rc.ITestingPage.testing.Errors}
 * @extends {xyz.swapee.rc.ITestingPage.testing.Validation}
 */
xyz.swapee.rc.ITestingPageView.TestingPartial.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.TestingPartial.Actions  9c94d7079915a315fadd7437cea05033 */
/** @record */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions = function() {}
/** @type {xyz.swapee.rc.Testing.testingNav} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.testing
/** @type {!xyz.swapee.rc.ITestingPage.testing.Form} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._testing
/** @type {xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._filterChangellyFloatingOffer
/** @type {!xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.filterChangellyFloatingOffer
/** @type {xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._filterChangellyFixedOffer
/** @type {!xyz.swapee.rc.Testing.filterChangellyFixedOfferNav} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.filterChangellyFixedOffer
/** @type {xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype._filterChangenowOffer
/** @type {!xyz.swapee.rc.Testing.filterChangenowOfferNav} */
xyz.swapee.rc.ITestingPageView.TestingPartial.Actions.prototype.filterChangenowOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/ITestingPageView.xml} xyz.swapee.rc.ITestingPageView.TestingPartial.Translation  9c94d7079915a315fadd7437cea05033 */
/** @record */
xyz.swapee.rc.ITestingPageView.TestingPartial.Translation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.testingNav  9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.testing.Form): void} */
xyz.swapee.rc.Testing.testingNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.TestingNav  9f341e7dc675a505975b1d1a6d53023a */
/** @constructor */
xyz.swapee.rc.Testing.TestingNav = function() {}
/** @type {number} */
xyz.swapee.rc.Testing.TestingNav.prototype.testing

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav  9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangellyFloatingOffer.Form): void} */
xyz.swapee.rc.Testing.filterChangellyFloatingOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav  9f341e7dc675a505975b1d1a6d53023a */
/** @constructor */
xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.Testing.FilterChangellyFloatingOfferNav.prototype.filterChangellyFloatingOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.filterChangellyFixedOfferNav  9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangellyFixedOffer.Form): void} */
xyz.swapee.rc.Testing.filterChangellyFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav  9f341e7dc675a505975b1d1a6d53023a */
/** @constructor */
xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.Testing.FilterChangellyFixedOfferNav.prototype.filterChangellyFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.filterChangenowOfferNav  9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingPage.filterChangenowOffer.Form): void} */
xyz.swapee.rc.Testing.filterChangenowOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.Testing.FilterChangenowOfferNav  9f341e7dc675a505975b1d1a6d53023a */
/** @constructor */
xyz.swapee.rc.Testing.FilterChangenowOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.Testing.FilterChangenowOfferNav.prototype.filterChangenowOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.testingMethodsIds  9f341e7dc675a505975b1d1a6d53023a */
/** @constructor */
xyz.swapee.rc.testingMethodsIds = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.testingArcsIds  9f341e7dc675a505975b1d1a6d53023a */
/** @constructor */
xyz.swapee.rc.testingArcsIds = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.getTesting  9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(): void} */
xyz.swapee.rc.getTesting

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.testing  9f341e7dc675a505975b1d1a6d53023a */
/** @typedef {function(!xyz.swapee.rc.ITestingImpl): !xyz.swapee.rc.ITestingPage} */
xyz.swapee.rc.testing

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.ITestingImpl.Initialese  9f341e7dc675a505975b1d1a6d53023a */
/** @record */
xyz.swapee.rc.ITestingImpl.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.ITestingImpl  9f341e7dc675a505975b1d1a6d53023a */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.ITestingPageAspects}
 * @extends {xyz.swapee.rc.ITestingPage}
 * @extends {xyz.swapee.rc.ITestingPageHyperslice}
 * @extends {xyz.swapee.rc.ITestingPageViewHyperslice}
 */
xyz.swapee.rc.ITestingImpl = function() {}
/** @param {...!xyz.swapee.rc.ITestingImpl.Initialese} init */
xyz.swapee.rc.ITestingImpl.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.TestingImpl  9f341e7dc675a505975b1d1a6d53023a */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.ITestingImpl.Initialese} init
 * @implements {xyz.swapee.rc.ITestingImpl}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.ITestingImpl.Initialese>}
 */
xyz.swapee.rc.TestingImpl = function(...init) {}
/** @param {...!xyz.swapee.rc.ITestingImpl.Initialese} init */
xyz.swapee.rc.TestingImpl.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.TestingImpl.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/testing/testing.ws/testing/Testing/design/api.xml} xyz.swapee.rc.AbstractTestingImpl  9f341e7dc675a505975b1d1a6d53023a */
/**
 * @constructor
 * @extends {xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl = function() {}
/**
 * @param {...((!xyz.swapee.rc.ITestingImpl|typeof xyz.swapee.rc.TestingImpl)|(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.TestingImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractTestingImpl.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractTestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingImpl|typeof xyz.swapee.rc.TestingImpl)|(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.ITestingImpl|typeof xyz.swapee.rc.TestingImpl)|(!xyz.swapee.rc.ITestingPageAspects|typeof xyz.swapee.rc.TestingPageAspects)|(!xyz.swapee.rc.ITestingPage|typeof xyz.swapee.rc.TestingPage)|(!xyz.swapee.rc.ITestingPageHyperslice|typeof xyz.swapee.rc.TestingPageHyperslice)|(!xyz.swapee.rc.ITestingPageViewHyperslice|typeof xyz.swapee.rc.TestingPageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.TestingImpl}
 */
xyz.swapee.rc.AbstractTestingImpl.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */