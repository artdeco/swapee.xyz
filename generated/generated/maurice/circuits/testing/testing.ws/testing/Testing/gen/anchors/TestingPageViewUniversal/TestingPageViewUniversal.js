import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TestingPageViewUniversal}
 */
function __TestingPageViewUniversal() {}
__TestingPageViewUniversal.prototype = /** @type {!_TestingPageViewUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractTestingPageViewUniversal}
 */
class _TestingPageViewUniversal { }
/**
 * A trait that allows to access an _ITestingPageView_ object via a `.testingPageView` field.
 * @extends {xyz.swapee.rc.AbstractTestingPageViewUniversal} ‎
 */
class TestingPageViewUniversal extends newAbstract(
 _TestingPageViewUniversal,217430474516,null,{
  asITestingPageView:1,
  asUTestingPageView:3,
  testingPageView:4,
  asTestingPageView:5,
  superTestingPageViewUniversal:2,
 },false,{
  testingPageView:{_testingPageView:1},
 }) {}

/** @type {typeof xyz.swapee.rc.UTestingPageView} */
TestingPageViewUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UTestingPageView} */
function TestingPageViewUniversalClass(){}

export default TestingPageViewUniversal

/** @type {xyz.swapee.rc.UTestingPageView.Symbols} */
export const TestingPageViewUniversalSymbols=TestingPageViewUniversal.Symbols
/** @type {xyz.swapee.rc.UTestingPageView.getSymbols} */
export const getTestingPageViewUniversalSymbols=TestingPageViewUniversal.getSymbols
/** @type {xyz.swapee.rc.UTestingPageView.setSymbols} */
export const setTestingPageViewUniversalSymbols=TestingPageViewUniversal.setSymbols

TestingPageViewUniversal[$implementations]=[
 __TestingPageViewUniversal,
 TestingPageViewUniversalClass.prototype=/**@type {!xyz.swapee.rc.UTestingPageView}*/({
  [TestingPageViewUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UTestingPageView} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UTestingPageView.Initialese}*/({
   testingPageView:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.TestingPageViewMetaUniversal}*/
export const TestingPageViewMetaUniversal=TestingPageViewUniversal.MetaUniversal