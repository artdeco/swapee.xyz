/**
 * @fileoverview
 * @externs
 */

/** @const */
var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
xyz.swapee.rc.IChangellyExchangePage={}
xyz.swapee.rc.IChangellyExchangePage.changellyExchange={}
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer={}
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer={}
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction={}
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction={}
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction={}
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form = function() {}
/** @type {string} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.ready
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.getOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.ready
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.getOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.address
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.refundAddress
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.createTransaction

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form.prototype.tid

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.address
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.refundAddress
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.createTransaction
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.fixedId

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form.prototype.id
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form.prototype.checkPayment

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.changellyExchangeNav only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form): void} */
xyz.swapee.rc.ChangellyExchange.changellyExchangeNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterGetOfferNav only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterGetOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav only:ChangellyExchange.changellyExchangeNav,xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form,ChangellyExchange.filterGetFixedOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form,ChangellyExchange.filterGetOfferNav,xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form,ChangellyExchange.filterCreateTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form,ChangellyExchange.filterGetTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form,ChangellyExchange.filterCreateFixedTransactionNav,xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form,ChangellyExchange.filterCheckPaymentNav,xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form 2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav

// nss:xyz.swapee.rc
/* @typal-end */