import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangePageViewAspectsInstaller}
 */
function __ChangellyExchangePageViewAspectsInstaller() {}
__ChangellyExchangePageViewAspectsInstaller.prototype = /** @type {!_ChangellyExchangePageViewAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller}
 */
class _ChangellyExchangePageViewAspectsInstaller { }

_ChangellyExchangePageViewAspectsInstaller.prototype[$advice]=__ChangellyExchangePageViewAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller} ‎ */
class ChangellyExchangePageViewAspectsInstaller extends newAbstract(
 _ChangellyExchangePageViewAspectsInstaller,'IChangellyExchangePageViewAspectsInstaller',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller} */
ChangellyExchangePageViewAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller} */
function ChangellyExchangePageViewAspectsInstallerClass(){}

export default ChangellyExchangePageViewAspectsInstaller


ChangellyExchangePageViewAspectsInstaller[$implementations]=[
 ChangellyExchangePageViewAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller}*/({
  viewChangellyExchange(){
   this.beforeViewChangellyExchange=1
   this.afterViewChangellyExchange=2
   this.aroundViewChangellyExchange=3
   this.afterViewChangellyExchangeThrows=4
   this.afterViewChangellyExchangeReturns=5
   this.afterViewChangellyExchangeCancels=7
   this.beforeEachViewChangellyExchange=8
   this.afterEachViewChangellyExchange=9
   this.afterEachViewChangellyExchangeReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  getChangellyExchange(){
   this.beforeGetChangellyExchange=1
   this.afterGetChangellyExchange=2
   this.aroundGetChangellyExchange=3
   this.afterGetChangellyExchangeThrows=4
   this.afterGetChangellyExchangeReturns=5
   this.afterGetChangellyExchangeCancels=7
   this.beforeEachGetChangellyExchange=8
   this.afterEachGetChangellyExchange=9
   this.afterEachGetChangellyExchangeReturns=10
   return {
    ctx:1,
    form:2,
   }
  },
  setChangellyExchangeCtx(){
   this.beforeSetChangellyExchangeCtx=1
   this.afterSetChangellyExchangeCtx=2
   this.aroundSetChangellyExchangeCtx=3
   this.afterSetChangellyExchangeCtxThrows=4
   this.afterSetChangellyExchangeCtxReturns=5
   this.afterSetChangellyExchangeCtxCancels=7
   this.beforeEachSetChangellyExchangeCtx=8
   this.afterEachSetChangellyExchangeCtx=9
   this.afterEachSetChangellyExchangeCtxReturns=10
   return {
    answers:1,
    translation:2,
   }
  },
  ChangellyExchangePartial(){
   this.before_ChangellyExchangePartial=1
   this.after_ChangellyExchangePartial=2
   this.around_ChangellyExchangePartial=3
   this.after_ChangellyExchangePartialThrows=4
   this.after_ChangellyExchangePartialReturns=5
   this.after_ChangellyExchangePartialCancels=7
   return {
    answers:1,
    errors:2,
    actions:3,
    translation:4,
   }
  },
  ChangellyExchangeView(){
   this.before_ChangellyExchangeView=1
   this.after_ChangellyExchangeView=2
   this.around_ChangellyExchangeView=3
   this.after_ChangellyExchangeViewThrows=4
   this.after_ChangellyExchangeViewReturns=5
   this.after_ChangellyExchangeViewCancels=7
   this.beforeEach_ChangellyExchangeView=8
   this.afterEach_ChangellyExchangeView=9
   this.afterEach_ChangellyExchangeViewReturns=10
  },
 }),
 __ChangellyExchangePageViewAspectsInstaller,
]