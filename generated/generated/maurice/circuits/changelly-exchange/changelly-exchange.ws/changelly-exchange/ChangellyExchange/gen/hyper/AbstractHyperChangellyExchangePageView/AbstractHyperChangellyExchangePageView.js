import ChangellyExchangePageViewAspectsInstaller from '../../aspects-installers/ChangellyExchangePageViewAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperChangellyExchangePageView}
 */
function __AbstractHyperChangellyExchangePageView() {}
__AbstractHyperChangellyExchangePageView.prototype = /** @type {!_AbstractHyperChangellyExchangePageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 */
class _AbstractHyperChangellyExchangePageView { }
/** @extends {xyz.swapee.rc.AbstractHyperChangellyExchangePageView} ‎ */
class AbstractHyperChangellyExchangePageView extends newAbstract(
 _AbstractHyperChangellyExchangePageView,'IHyperChangellyExchangePageView',null,{
  asIHyperChangellyExchangePageView:ChangellyExchangePageViewAspectsInstaller,
  superHyperChangellyExchangePageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView} */
AbstractHyperChangellyExchangePageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView} */
function AbstractHyperChangellyExchangePageViewClass(){}

export default AbstractHyperChangellyExchangePageView


AbstractHyperChangellyExchangePageView[$implementations]=[
 __AbstractHyperChangellyExchangePageView,
]