import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangePageViewUniversal}
 */
function __ChangellyExchangePageViewUniversal() {}
__ChangellyExchangePageViewUniversal.prototype = /** @type {!_ChangellyExchangePageViewUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal}
 */
class _ChangellyExchangePageViewUniversal { }
/**
 * A trait that allows to access an _IChangellyExchangePageView_ object via a `.changellyExchangePageView` field.
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal} ‎
 */
class ChangellyExchangePageViewUniversal extends newAbstract(
 _ChangellyExchangePageViewUniversal,'UChangellyExchangePageView',null,{
  asIChangellyExchangePageView:1,
  asUChangellyExchangePageView:3,
  changellyExchangePageView:4,
  asChangellyExchangePageView:5,
  superChangellyExchangePageViewUniversal:2,
 },false,{
  changellyExchangePageView:{_changellyExchangePageView:'_changellyExchangePageView'},
 }) {}

/** @type {typeof xyz.swapee.rc.UChangellyExchangePageView} */
ChangellyExchangePageViewUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UChangellyExchangePageView} */
function ChangellyExchangePageViewUniversalClass(){}

export default ChangellyExchangePageViewUniversal

/** @type {xyz.swapee.rc.UChangellyExchangePageView.Symbols} */
export const ChangellyExchangePageViewUniversalSymbols=ChangellyExchangePageViewUniversal.Symbols
/** @type {xyz.swapee.rc.UChangellyExchangePageView.getSymbols} */
export const getChangellyExchangePageViewUniversalSymbols=ChangellyExchangePageViewUniversal.getSymbols
/** @type {xyz.swapee.rc.UChangellyExchangePageView.setSymbols} */
export const setChangellyExchangePageViewUniversalSymbols=ChangellyExchangePageViewUniversal.setSymbols

ChangellyExchangePageViewUniversal[$implementations]=[
 __ChangellyExchangePageViewUniversal,
 ChangellyExchangePageViewUniversalClass.prototype=/**@type {!xyz.swapee.rc.UChangellyExchangePageView}*/({
  [ChangellyExchangePageViewUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UChangellyExchangePageView} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UChangellyExchangePageView.Initialese}*/({
   changellyExchangePageView:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.ChangellyExchangePageViewMetaUniversal}*/
export const ChangellyExchangePageViewMetaUniversal=ChangellyExchangePageViewUniversal.MetaUniversal