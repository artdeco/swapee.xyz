require('@type.engineering/type-engineer/types/typedefs')
//require('./types/typology')
require('./types/typedefs')

/**
@license
@LICENSE @xyz.swapee.rc/changelly-exchange (c) by X 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @extends {xyz.swapee.rc.ChangellyExchangePage} */
class ChangellyExchangePage extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the methods.
 * @extends {xyz.swapee.rc.changellyExchangeMethodsIds}
 */
class changellyExchangeMethodsIds extends (class {/* lazy-loaded */}) {}
/**
 * The ids of the arcs.
 * @extends {xyz.swapee.rc.changellyExchangeArcsIds}
 */
class changellyExchangeArcsIds extends (class {/* lazy-loaded */}) {}
/**
 * Allows to render the pagelet after it's been processed.
 */
function getChangellyExchange() {
  // lazy-loaded()
}
/** @extends {xyz.swapee.rc.ChangellyExchangePageView} */
class ChangellyExchangePageView extends (class {/* lazy-loaded */}) {}
/** @extends {xyz.swapee.rc.HyperChangellyExchangePageView} */
class HyperChangellyExchangePageView extends (class {/* lazy-loaded */}) {}
/**
 * The `changellyExchange` navigation metadata.
 * @extends {xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav}
 */
class ChangellyExchangeNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterGetFixedOffer` navigation metadata.
 * @extends {xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav}
 */
class FilterGetFixedOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterGetOffer` navigation metadata.
 * @extends {xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav}
 */
class FilterGetOfferNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterCreateTransaction` navigation metadata.
 * @extends {xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav}
 */
class FilterCreateTransactionNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterGetTransaction` navigation metadata.
 * @extends {xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav}
 */
class FilterGetTransactionNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterCreateFixedTransaction` navigation metadata.
 * @extends {xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav}
 */
class FilterCreateFixedTransactionNav extends (class {/* lazy-loaded */}) {}
/**
 * The `filterCheckPayment` navigation metadata.
 * @extends {xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav}
 */
class FilterCheckPaymentNav extends (class {/* lazy-loaded */}) {}

module.exports.ChangellyExchangePage = ChangellyExchangePage
module.exports.changellyExchangeMethodsIds = changellyExchangeMethodsIds
module.exports.changellyExchangeArcsIds = changellyExchangeArcsIds
module.exports.getChangellyExchange = getChangellyExchange
module.exports.ChangellyExchangePageView = ChangellyExchangePageView
module.exports.HyperChangellyExchangePageView = HyperChangellyExchangePageView
module.exports.ChangellyExchangeNav = ChangellyExchangeNav
module.exports.FilterGetFixedOfferNav = FilterGetFixedOfferNav
module.exports.FilterGetOfferNav = FilterGetOfferNav
module.exports.FilterCreateTransactionNav = FilterCreateTransactionNav
module.exports.FilterGetTransactionNav = FilterGetTransactionNav
module.exports.FilterCreateFixedTransactionNav = FilterCreateFixedTransactionNav
module.exports.FilterCheckPaymentNav = FilterCheckPaymentNav

Object.defineProperties(module.exports, {
 'ChangellyExchangePage': {get: () => require('./compile')[14121551481]},
 [14121551481]: {get: () => module.exports['ChangellyExchangePage']},
 'changellyExchangeMethodsIds': {get: () => require('./compile')[14121551482]},
 [14121551482]: {get: () => module.exports['changellyExchangeMethodsIds']},
 'changellyExchangeArcsIds': {get: () => require('./compile')[14121551483]},
 [14121551483]: {get: () => module.exports['changellyExchangeArcsIds']},
 'getChangellyExchange': {get: () => require('./compile')[14121551484]},
 [14121551484]: {get: () => module.exports['getChangellyExchange']},
 'ChangellyExchangePageView': {get: () => require('./compile')[141215514810]},
 [141215514810]: {get: () => module.exports['ChangellyExchangePageView']},
 'HyperChangellyExchangePageView': {get: () => require('./compile')[141215514813]},
 [141215514813]: {get: () => module.exports['HyperChangellyExchangePageView']},
 'ChangellyExchangeNav': {get: () => require('./compile')[1412155148100]},
 [1412155148100]: {get: () => module.exports['ChangellyExchangeNav']},
 'FilterGetFixedOfferNav': {get: () => require('./compile')[1412155148101]},
 [1412155148101]: {get: () => module.exports['FilterGetFixedOfferNav']},
 'FilterGetOfferNav': {get: () => require('./compile')[1412155148102]},
 [1412155148102]: {get: () => module.exports['FilterGetOfferNav']},
 'FilterCreateTransactionNav': {get: () => require('./compile')[1412155148103]},
 [1412155148103]: {get: () => module.exports['FilterCreateTransactionNav']},
 'FilterGetTransactionNav': {get: () => require('./compile')[1412155148104]},
 [1412155148104]: {get: () => module.exports['FilterGetTransactionNav']},
 'FilterCreateFixedTransactionNav': {get: () => require('./compile')[1412155148105]},
 [1412155148105]: {get: () => module.exports['FilterCreateFixedTransactionNav']},
 'FilterCheckPaymentNav': {get: () => require('./compile')[1412155148106]},
 [1412155148106]: {get: () => module.exports['FilterCheckPaymentNav']},
})