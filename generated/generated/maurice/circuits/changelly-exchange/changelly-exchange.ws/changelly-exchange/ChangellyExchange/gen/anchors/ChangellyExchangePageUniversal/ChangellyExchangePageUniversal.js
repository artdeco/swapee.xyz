import { newAbstract, $implementations, precombinedGet, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangePageUniversal}
 */
function __ChangellyExchangePageUniversal() {}
__ChangellyExchangePageUniversal.prototype = /** @type {!_ChangellyExchangePageUniversal} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageUniversal}
 */
class _ChangellyExchangePageUniversal { }
/**
 * A trait that allows to access an _IChangellyExchangePage_ object via a `.changellyExchangePage` field.
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageUniversal} ‎
 */
class ChangellyExchangePageUniversal extends newAbstract(
 _ChangellyExchangePageUniversal,'UChangellyExchangePage',null,{
  asIChangellyExchangePage:1,
  asUChangellyExchangePage:3,
  changellyExchangePage:4,
  asChangellyExchangePage:5,
  superChangellyExchangePageUniversal:2,
 },false,{
  changellyExchangePage:{_changellyExchangePage:'_changellyExchangePage'},
 }) {}

/** @type {typeof xyz.swapee.rc.UChangellyExchangePage} */
ChangellyExchangePageUniversal.class=function(){}
/** @type {typeof xyz.swapee.rc.UChangellyExchangePage} */
function ChangellyExchangePageUniversalClass(){}

export default ChangellyExchangePageUniversal

/** @type {xyz.swapee.rc.UChangellyExchangePage.Symbols} */
export const ChangellyExchangePageUniversalSymbols=ChangellyExchangePageUniversal.Symbols
/** @type {xyz.swapee.rc.UChangellyExchangePage.getSymbols} */
export const getChangellyExchangePageUniversalSymbols=ChangellyExchangePageUniversal.getSymbols
/** @type {xyz.swapee.rc.UChangellyExchangePage.setSymbols} */
export const setChangellyExchangePageUniversalSymbols=ChangellyExchangePageUniversal.setSymbols

ChangellyExchangePageUniversal[$implementations]=[
 __ChangellyExchangePageUniversal,
 ChangellyExchangePageUniversalClass.prototype=/**@type {!xyz.swapee.rc.UChangellyExchangePage}*/({
  [ChangellyExchangePageUniversal.MetaUniversal]:precombinedGet,
 }),
 /** @type {!xyz.swapee.rc.UChangellyExchangePage} */ ({
  [$initialese]:/**@type {!xyz.swapee.rc.UChangellyExchangePage.Initialese}*/({
   changellyExchangePage:1,
  }),
 }),
]
/**@type {xyz.swapee.rc.ChangellyExchangePageMetaUniversal}*/
export const ChangellyExchangePageMetaUniversal=ChangellyExchangePageUniversal.MetaUniversal