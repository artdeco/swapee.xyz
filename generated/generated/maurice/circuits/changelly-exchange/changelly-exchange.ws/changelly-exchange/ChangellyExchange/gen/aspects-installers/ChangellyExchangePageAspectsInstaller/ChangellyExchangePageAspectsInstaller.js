import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangePageAspectsInstaller}
 */
function __ChangellyExchangePageAspectsInstaller() {}
__ChangellyExchangePageAspectsInstaller.prototype = /** @type {!_ChangellyExchangePageAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller}
 */
class _ChangellyExchangePageAspectsInstaller { }

_ChangellyExchangePageAspectsInstaller.prototype[$advice]=__ChangellyExchangePageAspectsInstaller

/** @extends {xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller} ‎ */
class ChangellyExchangePageAspectsInstaller extends newAbstract(
 _ChangellyExchangePageAspectsInstaller,'IChangellyExchangePageAspectsInstaller',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller} */
ChangellyExchangePageAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller} */
function ChangellyExchangePageAspectsInstallerClass(){}

export default ChangellyExchangePageAspectsInstaller


ChangellyExchangePageAspectsInstaller[$implementations]=[
 ChangellyExchangePageAspectsInstallerClass.prototype=/**@type {!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller}*/({
  changellyExchange(){
   this.beforeChangellyExchange=1
   this.afterChangellyExchange=2
   this.aroundChangellyExchange=3
   this.afterChangellyExchangeThrows=4
   this.afterChangellyExchangeReturns=5
   this.afterChangellyExchangeCancels=7
   this.beforeEachChangellyExchange=8
   this.afterEachChangellyExchange=9
   this.afterEachChangellyExchangeReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterGetFixedOffer(){
   this.beforeFilterGetFixedOffer=1
   this.afterFilterGetFixedOffer=2
   this.aroundFilterGetFixedOffer=3
   this.afterFilterGetFixedOfferThrows=4
   this.afterFilterGetFixedOfferReturns=5
   this.afterFilterGetFixedOfferCancels=7
   this.beforeEachFilterGetFixedOffer=8
   this.afterEachFilterGetFixedOffer=9
   this.afterEachFilterGetFixedOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterGetOffer(){
   this.beforeFilterGetOffer=1
   this.afterFilterGetOffer=2
   this.aroundFilterGetOffer=3
   this.afterFilterGetOfferThrows=4
   this.afterFilterGetOfferReturns=5
   this.afterFilterGetOfferCancels=7
   this.beforeEachFilterGetOffer=8
   this.afterEachFilterGetOffer=9
   this.afterEachFilterGetOfferReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterCreateTransaction(){
   this.beforeFilterCreateTransaction=1
   this.afterFilterCreateTransaction=2
   this.aroundFilterCreateTransaction=3
   this.afterFilterCreateTransactionThrows=4
   this.afterFilterCreateTransactionReturns=5
   this.afterFilterCreateTransactionCancels=7
   this.beforeEachFilterCreateTransaction=8
   this.afterEachFilterCreateTransaction=9
   this.afterEachFilterCreateTransactionReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterGetTransaction(){
   this.beforeFilterGetTransaction=1
   this.afterFilterGetTransaction=2
   this.aroundFilterGetTransaction=3
   this.afterFilterGetTransactionThrows=4
   this.afterFilterGetTransactionReturns=5
   this.afterFilterGetTransactionCancels=7
   this.beforeEachFilterGetTransaction=8
   this.afterEachFilterGetTransaction=9
   this.afterEachFilterGetTransactionReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterCreateFixedTransaction(){
   this.beforeFilterCreateFixedTransaction=1
   this.afterFilterCreateFixedTransaction=2
   this.aroundFilterCreateFixedTransaction=3
   this.afterFilterCreateFixedTransactionThrows=4
   this.afterFilterCreateFixedTransactionReturns=5
   this.afterFilterCreateFixedTransactionCancels=7
   this.beforeEachFilterCreateFixedTransaction=8
   this.afterEachFilterCreateFixedTransaction=9
   this.afterEachFilterCreateFixedTransactionReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
  filterCheckPayment(){
   this.beforeFilterCheckPayment=1
   this.afterFilterCheckPayment=2
   this.aroundFilterCheckPayment=3
   this.afterFilterCheckPaymentThrows=4
   this.afterFilterCheckPaymentReturns=5
   this.afterFilterCheckPaymentCancels=7
   this.beforeEachFilterCheckPayment=8
   this.afterEachFilterCheckPayment=9
   this.afterEachFilterCheckPaymentReturns=10
   return {
    form:1,
    answers:2,
    validation:3,
    errors:4,
    ctx:5,
   }
  },
 }),
 __ChangellyExchangePageAspectsInstaller,
]