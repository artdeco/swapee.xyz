import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav} */
export const FilterCreateFixedTransactionNav={
 toString(){return'filterCreateFixedTransaction'},
 filterCreateFixedTransaction:methodsIds.filterCreateFixedTransaction,
}
export default FilterCreateFixedTransactionNav