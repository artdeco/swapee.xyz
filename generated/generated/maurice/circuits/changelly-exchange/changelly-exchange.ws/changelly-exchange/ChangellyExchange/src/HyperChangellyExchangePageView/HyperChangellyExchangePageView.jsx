import '../../types'
import ChangellyExchangePageView from '../ChangellyExchangePageView'
import AbstractHyperChangellyExchangePageView from '../../gen/hyper/AbstractHyperChangellyExchangePageView'
import {AssignLocale} from '@websystems/parthenon'
import ChangellyExchangePageArcsIds from '../../gen/arcs-ids'
import ChangellyExchangePageMethodsIds from '../../gen/methods-ids'
import {RenderAide} from '@idio2/server'
import AbstractChangellyExchangePageViewAspects from '../../gen/aspects/AbstractChangellyExchangePageViewAspects'

/** @extends {xyz.swapee.rc.HyperChangellyExchangePageView}  */
export default class HyperChangellyExchangePageView extends AbstractHyperChangellyExchangePageView.consults(
 AbstractChangellyExchangePageViewAspects.class.prototype=/**@type {!xyz.swapee.rc.ChangellyExchangePageViewAspects} */({
  beforeChangellyExchange:[
   AssignLocale,
  ],
  afterEachSetChangellyExchangeCtxReturns:[
   function({res:res}) {
    const{asIChangellyExchangePage:{asCContext:ctx}}=this
    Object.assign(ctx,res)
   },
  ],
  afterViewChangellyExchangeReturns:[
   RenderAide.RenderActionableJSXCtx({
    ...ChangellyExchangePageMethodsIds,
   },{
    attrsProcess:[
    ],
    arcs:ChangellyExchangePageArcsIds,
   },{
    changellyExchangeTranslations:1,
   }),
  ],
 }),
).implements(
 ChangellyExchangePageView,
){}