/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.rc={}
xyz.swapee.rc.IChangellyExchangePage={}
xyz.swapee.rc.IChangellyExchangePage.changellyExchange={}
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer={}
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer={}
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction={}
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction={}
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction={}
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment={}
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller={}
xyz.swapee.rc.IChangellyExchangePageJoinpointModel={}
xyz.swapee.rc.UChangellyExchangePage={}
xyz.swapee.rc.AbstractChangellyExchangePageUniversal={}
xyz.swapee.rc.IChangellyExchangePageAspects={}
xyz.swapee.rc.IHyperChangellyExchangePage={}
xyz.swapee.rc.ChangellyExchange={}
xyz.swapee.rc.ChangellyExchange.Answers={}
xyz.swapee.rc.ChangellyExchange.Form={}
xyz.swapee.rc.ChangellyExchange.Errors={}
xyz.swapee.rc.ChangellyExchange.Validation={}
xyz.swapee.rc.ChangellyExchange.Ctx={}
xyz.swapee.rc.IChangellyExchangePageView={}
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial={}
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel={}
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller={}
xyz.swapee.rc.UChangellyExchangePageView={}
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal={}
xyz.swapee.rc.IChangellyExchangePageViewAspects={}
xyz.swapee.rc.IHyperChangellyExchangePageView={}
xyz.swapee.rc.IChangellyExchangeImpl={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml}  a4d070646b41656ce8dbc3351e381489 */
/** @typedef {com.changelly.UChangelly.Initialese&io.changenow.UChangeNow.Initialese&io.letsexchange.ULetsExchange.Initialese} xyz.swapee.rc.IChangellyExchangePage.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePage)} xyz.swapee.rc.AbstractChangellyExchangePage.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePage} xyz.swapee.rc.ChangellyExchangePage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IChangellyExchangePage` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePage
 */
xyz.swapee.rc.AbstractChangellyExchangePage = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePage.constructor&xyz.swapee.rc.ChangellyExchangePage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePage.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePage.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)|!xyz.swapee.rc.IChangellyExchangePageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)|!xyz.swapee.rc.IChangellyExchangePageHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)|!xyz.swapee.rc.IChangellyExchangePageHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeChangellyExchange>} */ (void 0)
    /**
     * After the method.
     */
    this.afterChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchange>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterChangellyExchangeThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterChangellyExchangeCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachChangellyExchange>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchange>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchangeReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetFixedOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterGetFixedOfferThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterGetFixedOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterGetFixedOfferCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetFixedOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterGetFixedOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetOffer>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOffer>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterGetOfferThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterGetOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterGetOfferCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetOffer>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOffer>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterGetOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOfferReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateTransaction>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransaction>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterCreateTransactionThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterCreateTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterCreateTransactionCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateTransaction>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransaction>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterCreateTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransactionReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetTransaction>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransaction>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterGetTransactionThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterGetTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterGetTransactionCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetTransaction>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransaction>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterGetTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransactionReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateFixedTransaction>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransaction>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterCreateFixedTransactionThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterCreateFixedTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterCreateFixedTransactionCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateFixedTransaction>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransaction>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterCreateFixedTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransactionReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCheckPayment>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPayment>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterCheckPaymentThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterCheckPaymentReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterCheckPaymentCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCheckPayment>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPayment>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterCheckPaymentReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPaymentReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPaymentReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice

/**
 * A concrete class of _IChangellyExchangePageJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.ChangellyExchangePageJoinpointModelHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice { }
xyz.swapee.rc.ChangellyExchangePageJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterChangellyExchangeThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterChangellyExchangeCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterGetFixedOfferThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterGetFixedOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterGetFixedOfferCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterGetFixedOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterGetOfferThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterGetOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterGetOfferCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterGetOfferReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterCreateTransactionThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterCreateTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterCreateTransactionCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterCreateTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterGetTransactionThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterGetTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterGetTransactionCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterGetTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterCreateFixedTransactionThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterCreateFixedTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterCreateFixedTransactionCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterCreateFixedTransactionReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFilterCheckPaymentThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFilterCheckPaymentReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFilterCheckPaymentCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachFilterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachFilterCheckPaymentReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice

/**
 * A concrete class of _IChangellyExchangePageJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.ChangellyExchangePageJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice { }
xyz.swapee.rc.ChangellyExchangePageJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IChangellyExchangePage`'s methods.
 * @interface xyz.swapee.rc.IChangellyExchangePageJoinpointModel
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel = class { }
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchangeThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchangeReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchangeCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachChangellyExchangeReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterGetFixedOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOfferThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOfferCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterGetFixedOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetFixedOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetFixedOfferReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterGetOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOfferThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOfferReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOfferCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterGetOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetOfferReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterCreateTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransactionThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransactionReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransactionCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterCreateTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateTransactionReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterGetTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransactionThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransactionReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransactionCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterGetTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetTransactionReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterCreateFixedTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransactionThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransactionReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransactionCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterCreateFixedTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateFixedTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateFixedTransactionReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterCheckPayment = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPayment = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPaymentThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPaymentReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPaymentCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterCheckPayment = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCheckPayment = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPaymentReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCheckPaymentReturns = function() {}

/**
 * A concrete class of _IChangellyExchangePageJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageJoinpointModel
 * @implements {xyz.swapee.rc.IChangellyExchangePageJoinpointModel} An interface that enumerates the joinpoints of `IChangellyExchangePage`'s methods.
 */
xyz.swapee.rc.ChangellyExchangePageJoinpointModel = class extends xyz.swapee.rc.IChangellyExchangePageJoinpointModel { }
xyz.swapee.rc.ChangellyExchangePageJoinpointModel.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageJoinpointModel

/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel} */
xyz.swapee.rc.RecordIChangellyExchangePageJoinpointModel

/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel} xyz.swapee.rc.BoundIChangellyExchangePageJoinpointModel */

/** @typedef {xyz.swapee.rc.ChangellyExchangePageJoinpointModel} xyz.swapee.rc.BoundChangellyExchangePageJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePageAspectsInstaller)} xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller} xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IChangellyExchangePageAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.constructor&xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese[]) => xyz.swapee.rc.IChangellyExchangePageAspectsInstaller} xyz.swapee.rc.ChangellyExchangePageAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.rc.IChangellyExchangePageAspectsInstaller */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller = class extends /** @type {xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeChangellyExchange=/** @type {number} */ (void 0)
    this.afterChangellyExchange=/** @type {number} */ (void 0)
    this.afterChangellyExchangeThrows=/** @type {number} */ (void 0)
    this.afterChangellyExchangeReturns=/** @type {number} */ (void 0)
    this.afterChangellyExchangeCancels=/** @type {number} */ (void 0)
    this.beforeEachChangellyExchange=/** @type {number} */ (void 0)
    this.afterEachChangellyExchange=/** @type {number} */ (void 0)
    this.afterEachChangellyExchangeReturns=/** @type {number} */ (void 0)
    this.beforeFilterGetFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterGetFixedOffer=/** @type {number} */ (void 0)
    this.afterFilterGetFixedOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterGetFixedOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterGetFixedOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterGetFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterGetFixedOffer=/** @type {number} */ (void 0)
    this.afterEachFilterGetFixedOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterGetOffer=/** @type {number} */ (void 0)
    this.afterFilterGetOffer=/** @type {number} */ (void 0)
    this.afterFilterGetOfferThrows=/** @type {number} */ (void 0)
    this.afterFilterGetOfferReturns=/** @type {number} */ (void 0)
    this.afterFilterGetOfferCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterGetOffer=/** @type {number} */ (void 0)
    this.afterEachFilterGetOffer=/** @type {number} */ (void 0)
    this.afterEachFilterGetOfferReturns=/** @type {number} */ (void 0)
    this.beforeFilterCreateTransaction=/** @type {number} */ (void 0)
    this.afterFilterCreateTransaction=/** @type {number} */ (void 0)
    this.afterFilterCreateTransactionThrows=/** @type {number} */ (void 0)
    this.afterFilterCreateTransactionReturns=/** @type {number} */ (void 0)
    this.afterFilterCreateTransactionCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterCreateTransaction=/** @type {number} */ (void 0)
    this.afterEachFilterCreateTransaction=/** @type {number} */ (void 0)
    this.afterEachFilterCreateTransactionReturns=/** @type {number} */ (void 0)
    this.beforeFilterGetTransaction=/** @type {number} */ (void 0)
    this.afterFilterGetTransaction=/** @type {number} */ (void 0)
    this.afterFilterGetTransactionThrows=/** @type {number} */ (void 0)
    this.afterFilterGetTransactionReturns=/** @type {number} */ (void 0)
    this.afterFilterGetTransactionCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterGetTransaction=/** @type {number} */ (void 0)
    this.afterEachFilterGetTransaction=/** @type {number} */ (void 0)
    this.afterEachFilterGetTransactionReturns=/** @type {number} */ (void 0)
    this.beforeFilterCreateFixedTransaction=/** @type {number} */ (void 0)
    this.afterFilterCreateFixedTransaction=/** @type {number} */ (void 0)
    this.afterFilterCreateFixedTransactionThrows=/** @type {number} */ (void 0)
    this.afterFilterCreateFixedTransactionReturns=/** @type {number} */ (void 0)
    this.afterFilterCreateFixedTransactionCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterCreateFixedTransaction=/** @type {number} */ (void 0)
    this.afterEachFilterCreateFixedTransaction=/** @type {number} */ (void 0)
    this.afterEachFilterCreateFixedTransactionReturns=/** @type {number} */ (void 0)
    this.beforeFilterCheckPayment=/** @type {number} */ (void 0)
    this.afterFilterCheckPayment=/** @type {number} */ (void 0)
    this.afterFilterCheckPaymentThrows=/** @type {number} */ (void 0)
    this.afterFilterCheckPaymentReturns=/** @type {number} */ (void 0)
    this.afterFilterCheckPaymentCancels=/** @type {number} */ (void 0)
    this.beforeEachFilterCheckPayment=/** @type {number} */ (void 0)
    this.afterEachFilterCheckPayment=/** @type {number} */ (void 0)
    this.afterEachFilterCheckPaymentReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  changellyExchange() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterGetFixedOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterGetOffer() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterCreateTransaction() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterGetTransaction() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterCreateFixedTransaction() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  filterCheckPayment() { }
}
/**
 * Create a new *IChangellyExchangePageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese>)} xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageAspectsInstaller} xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IChangellyExchangePageAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageAspectsInstaller
 * @implements {xyz.swapee.rc.IChangellyExchangePageAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePageAspectsInstaller = class extends /** @type {xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.constructor&xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangePageAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangePageAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePage.ChangellyExchangeNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form The form.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation} validation The validation.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors} errors ,,
 *
 *         The errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx The ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePage.ChangellyExchangeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePage.ChangellyExchangeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `changellyExchange` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>) => void} sub Cancels a call to `changellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `changellyExchange` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePage.FilterGetFixedOfferNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePage.FilterGetFixedOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePage.FilterGetFixedOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterGetFixedOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>) => void} sub Cancels a call to `filterGetFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterGetFixedOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePage.FilterGetOfferNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form} form The action form.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePage.FilterGetOfferNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePage.FilterGetOfferNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterGetOffer` method from being executed.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>) => void} sub Cancels a call to `filterGetOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterGetOffer` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePage.FilterCreateTransactionNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form} form The action form.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePage.FilterCreateTransactionNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePage.FilterCreateTransactionNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterCreateTransaction` method from being executed.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>) => void} sub Cancels a call to `filterCreateTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterCreateTransaction` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePage.FilterGetTransactionNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form} form The action form.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePage.FilterGetTransactionNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePage.FilterGetTransactionNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterGetTransaction` method from being executed.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>) => void} sub Cancels a call to `filterGetTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterGetTransaction` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePage.FilterCreateFixedTransactionNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form} form The action form.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePage.FilterCreateFixedTransactionNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePage.FilterCreateFixedTransactionNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterCreateFixedTransaction` method from being executed.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>) => void} sub Cancels a call to `filterCreateFixedTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterCreateFixedTransaction` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePage.FilterCheckPaymentNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form} form The action form.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers} answers The action answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation} validation The action validation.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors} errors The action errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx} ctx The action ctx.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePage.FilterCheckPaymentNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePage.FilterCheckPaymentNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `filterCheckPayment` method from being executed.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>) => void} sub Cancels a call to `filterCheckPayment` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `filterCheckPayment` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData
 * @prop {xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>} res The return of the method after it's successfully run.
 * @prop {(value: xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData&xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IChangellyExchangePage.Initialese[]) => xyz.swapee.rc.IChangellyExchangePage} xyz.swapee.rc.ChangellyExchangePageConstructor */

/** @typedef {symbol} xyz.swapee.rc.ChangellyExchangePageMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UChangellyExchangePage.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IChangellyExchangePage} [changellyExchangePage]
 */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePageUniversal)} xyz.swapee.rc.AbstractChangellyExchangePageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePageUniversal} xyz.swapee.rc.ChangellyExchangePageUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UChangellyExchangePage` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePageUniversal
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePageUniversal.constructor&xyz.swapee.rc.ChangellyExchangePageUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePageUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.ChangellyExchangePageMetaUniversal} xyz.swapee.rc.AbstractChangellyExchangePageUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UChangellyExchangePage.Initialese[]) => xyz.swapee.rc.UChangellyExchangePage} xyz.swapee.rc.UChangellyExchangePageConstructor */

/** @typedef {function(new: xyz.swapee.rc.UChangellyExchangePageFields&engineering.type.IEngineer&xyz.swapee.rc.UChangellyExchangePageCaster)} xyz.swapee.rc.UChangellyExchangePage.constructor */
/**
 * A trait that allows to access an _IChangellyExchangePage_ object via a `.changellyExchangePage` field.
 * @interface xyz.swapee.rc.UChangellyExchangePage
 */
xyz.swapee.rc.UChangellyExchangePage = class extends /** @type {xyz.swapee.rc.UChangellyExchangePage.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UChangellyExchangePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UChangellyExchangePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UChangellyExchangePage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UChangellyExchangePage&engineering.type.IInitialiser<!xyz.swapee.rc.UChangellyExchangePage.Initialese>)} xyz.swapee.rc.ChangellyExchangePageUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UChangellyExchangePage} xyz.swapee.rc.UChangellyExchangePage.typeof */
/**
 * A concrete class of _UChangellyExchangePage_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageUniversal
 * @implements {xyz.swapee.rc.UChangellyExchangePage} A trait that allows to access an _IChangellyExchangePage_ object via a `.changellyExchangePage` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UChangellyExchangePage.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePageUniversal = class extends /** @type {xyz.swapee.rc.ChangellyExchangePageUniversal.constructor&xyz.swapee.rc.UChangellyExchangePage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UChangellyExchangePage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UChangellyExchangePage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UChangellyExchangePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UChangellyExchangePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePageUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.ChangellyExchangePageUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UChangellyExchangePage.
 * @interface xyz.swapee.rc.UChangellyExchangePageFields
 */
xyz.swapee.rc.UChangellyExchangePageFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UChangellyExchangePageFields.prototype.changellyExchangePage = /** @type {xyz.swapee.rc.IChangellyExchangePage} */ (void 0)

/** @typedef {xyz.swapee.rc.UChangellyExchangePage} */
xyz.swapee.rc.RecordUChangellyExchangePage

/** @typedef {xyz.swapee.rc.UChangellyExchangePage} xyz.swapee.rc.BoundUChangellyExchangePage */

/** @typedef {xyz.swapee.rc.ChangellyExchangePageUniversal} xyz.swapee.rc.BoundChangellyExchangePageUniversal */

/**
 * Contains getters to cast the _UChangellyExchangePage_ interface.
 * @interface xyz.swapee.rc.UChangellyExchangePageCaster
 */
xyz.swapee.rc.UChangellyExchangePageCaster = class { }
/**
 * Provides direct access to _UChangellyExchangePage_ via the _BoundUChangellyExchangePage_ universal.
 * @type {!xyz.swapee.rc.BoundChangellyExchangePage}
 */
xyz.swapee.rc.UChangellyExchangePageCaster.prototype.asChangellyExchangePage
/**
 * Cast the _UChangellyExchangePage_ instance into the _BoundUChangellyExchangePage_ type.
 * @type {!xyz.swapee.rc.BoundUChangellyExchangePage}
 */
xyz.swapee.rc.UChangellyExchangePageCaster.prototype.asUChangellyExchangePage
/**
 * Access the _ChangellyExchangePageUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundChangellyExchangePageUniversal}
 */
xyz.swapee.rc.UChangellyExchangePageCaster.prototype.superChangellyExchangePageUniversal

/** @typedef {function(new: xyz.swapee.rc.BChangellyExchangePageAspectsCaster<THIS>&xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BChangellyExchangePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice} xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IChangellyExchangePage* that bind to an instance.
 * @interface xyz.swapee.rc.BChangellyExchangePageAspects
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageAspects = class extends /** @type {xyz.swapee.rc.BChangellyExchangePageAspects.constructor&xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BChangellyExchangePageAspects.prototype.constructor = xyz.swapee.rc.BChangellyExchangePageAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IChangellyExchangePageAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePageAspects)} xyz.swapee.rc.AbstractChangellyExchangePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePageAspects} xyz.swapee.rc.ChangellyExchangePageAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IChangellyExchangePageAspects` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePageAspects
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePageAspects.constructor&xyz.swapee.rc.ChangellyExchangePageAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePageAspects.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePageAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IChangellyExchangePageAspects.Initialese[]) => xyz.swapee.rc.IChangellyExchangePageAspects} xyz.swapee.rc.ChangellyExchangePageAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IChangellyExchangePageAspectsCaster&xyz.swapee.rc.BChangellyExchangePageAspects<!xyz.swapee.rc.IChangellyExchangePageAspects>&com.changelly.UChangelly&io.changenow.UChangeNow&io.letsexchange.ULetsExchange)} xyz.swapee.rc.IChangellyExchangePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BChangellyExchangePageAspects} xyz.swapee.rc.BChangellyExchangePageAspects.typeof */
/** @typedef {typeof com.changelly.UChangelly} com.changelly.UChangelly.typeof */
/** @typedef {typeof io.changenow.UChangeNow} io.changenow.UChangeNow.typeof */
/** @typedef {typeof io.letsexchange.ULetsExchange} io.letsexchange.ULetsExchange.typeof */
/**
 * The aspects of the *IChangellyExchangePage*.
 * @interface xyz.swapee.rc.IChangellyExchangePageAspects
 */
xyz.swapee.rc.IChangellyExchangePageAspects = class extends /** @type {xyz.swapee.rc.IChangellyExchangePageAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BChangellyExchangePageAspects.typeof&com.changelly.UChangelly.typeof&io.changenow.UChangeNow.typeof&io.letsexchange.ULetsExchange.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.props=/** @type {xyz.swapee.rc.IChangellyExchangePageAspects} */ (void 0)
  }
}
/**
 * Create a new *IChangellyExchangePageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IChangellyExchangePageAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese>)} xyz.swapee.rc.ChangellyExchangePageAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageAspects} xyz.swapee.rc.IChangellyExchangePageAspects.typeof */
/**
 * A concrete class of _IChangellyExchangePageAspects_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageAspects
 * @implements {xyz.swapee.rc.IChangellyExchangePageAspects} The aspects of the *IChangellyExchangePage*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePageAspects = class extends /** @type {xyz.swapee.rc.ChangellyExchangePageAspects.constructor&xyz.swapee.rc.IChangellyExchangePageAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangePageAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangePageAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePageAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.ChangellyExchangePageAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BChangellyExchangePageAspects_ interface.
 * @interface xyz.swapee.rc.BChangellyExchangePageAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageAspectsCaster = class { }
/**
 * Cast the _BChangellyExchangePageAspects_ instance into the _BoundIChangellyExchangePage_ type.
 * @type {!xyz.swapee.rc.BoundIChangellyExchangePage}
 */
xyz.swapee.rc.BChangellyExchangePageAspectsCaster.prototype.asIChangellyExchangePage

/**
 * Contains getters to cast the _IChangellyExchangePageAspects_ interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageAspectsCaster
 */
xyz.swapee.rc.IChangellyExchangePageAspectsCaster = class { }
/**
 * Cast the _IChangellyExchangePageAspects_ instance into the _BoundIChangellyExchangePage_ type.
 * @type {!xyz.swapee.rc.BoundIChangellyExchangePage}
 */
xyz.swapee.rc.IChangellyExchangePageAspectsCaster.prototype.asIChangellyExchangePage

/** @typedef {xyz.swapee.rc.IChangellyExchangePage.Initialese} xyz.swapee.rc.IHyperChangellyExchangePage.Initialese */

/** @typedef {function(new: xyz.swapee.rc.HyperChangellyExchangePage)} xyz.swapee.rc.AbstractHyperChangellyExchangePage.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperChangellyExchangePage} xyz.swapee.rc.HyperChangellyExchangePage.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperChangellyExchangePage` interface.
 * @constructor xyz.swapee.rc.AbstractHyperChangellyExchangePage
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage = class extends /** @type {xyz.swapee.rc.AbstractHyperChangellyExchangePage.constructor&xyz.swapee.rc.HyperChangellyExchangePage.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperChangellyExchangePage.prototype.constructor = xyz.swapee.rc.AbstractHyperChangellyExchangePage
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.class = /** @type {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperChangellyExchangePage|typeof xyz.swapee.rc.HyperChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperChangellyExchangePage|typeof xyz.swapee.rc.HyperChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperChangellyExchangePage|typeof xyz.swapee.rc.HyperChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspects|!Array<!xyz.swapee.rc.IChangellyExchangePageAspects>|function(new: xyz.swapee.rc.IChangellyExchangePageAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the IChangellyExchangePage to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperChangellyExchangePage.Initialese[]) => xyz.swapee.rc.IHyperChangellyExchangePage} xyz.swapee.rc.HyperChangellyExchangePageConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperChangellyExchangePageCaster&xyz.swapee.rc.IChangellyExchangePage)} xyz.swapee.rc.IHyperChangellyExchangePage.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage} xyz.swapee.rc.IChangellyExchangePage.typeof */
/** @interface xyz.swapee.rc.IHyperChangellyExchangePage */
xyz.swapee.rc.IHyperChangellyExchangePage = class extends /** @type {xyz.swapee.rc.IHyperChangellyExchangePage.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IChangellyExchangePage.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperChangellyExchangePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperChangellyExchangePage.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperChangellyExchangePage&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese>)} xyz.swapee.rc.HyperChangellyExchangePage.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperChangellyExchangePage} xyz.swapee.rc.IHyperChangellyExchangePage.typeof */
/**
 * A concrete class of _IHyperChangellyExchangePage_ instances.
 * @constructor xyz.swapee.rc.HyperChangellyExchangePage
 * @implements {xyz.swapee.rc.IHyperChangellyExchangePage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese>} ‎
 */
xyz.swapee.rc.HyperChangellyExchangePage = class extends /** @type {xyz.swapee.rc.HyperChangellyExchangePage.constructor&xyz.swapee.rc.IHyperChangellyExchangePage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperChangellyExchangePage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperChangellyExchangePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperChangellyExchangePage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.HyperChangellyExchangePage.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperChangellyExchangePage} */
xyz.swapee.rc.RecordIHyperChangellyExchangePage

/** @typedef {xyz.swapee.rc.IHyperChangellyExchangePage} xyz.swapee.rc.BoundIHyperChangellyExchangePage */

/** @typedef {xyz.swapee.rc.HyperChangellyExchangePage} xyz.swapee.rc.BoundHyperChangellyExchangePage */

/**
 * Contains getters to cast the _IHyperChangellyExchangePage_ interface.
 * @interface xyz.swapee.rc.IHyperChangellyExchangePageCaster
 */
xyz.swapee.rc.IHyperChangellyExchangePageCaster = class { }
/**
 * Cast the _IHyperChangellyExchangePage_ instance into the _BoundIHyperChangellyExchangePage_ type.
 * @type {!xyz.swapee.rc.BoundIHyperChangellyExchangePage}
 */
xyz.swapee.rc.IHyperChangellyExchangePageCaster.prototype.asIHyperChangellyExchangePage
/**
 * Access the _HyperChangellyExchangePage_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperChangellyExchangePage}
 */
xyz.swapee.rc.IHyperChangellyExchangePageCaster.prototype.superHyperChangellyExchangePage

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageHyperslice
 */
xyz.swapee.rc.IChangellyExchangePageHyperslice = class {
  constructor() {
    this.changellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePage._changellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._changellyExchange>} */ (void 0)
    /**
     * Generates a fixed offer.
     */
    this.filterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePage._filterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterGetFixedOffer>} */ (void 0)
    /**
     * Generates a floating-rate offer. Some exchanges like _Changelly_ return data
     * about fees immediately without creating a transaction.
     */
    this.filterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePage._filterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterGetOffer>} */ (void 0)
    /**
     * Creates a pair of deposit and payout addresses.
     */
    this.filterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePage._filterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterCreateTransaction>} */ (void 0)
    /**
     * Looks up the transaction info.
     */
    this.filterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePage._filterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterGetTransaction>} */ (void 0)
    /**
     * Creates a pair of deposit and payout addresses for fixed-rate offers.
     */
    this.filterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePage._filterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterCreateFixedTransaction>} */ (void 0)
    /**
     * Updates the status of the transaction.
     */
    this.filterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePage._filterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterCheckPayment>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageHyperslice

/**
 * A concrete class of _IChangellyExchangePageHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.ChangellyExchangePageHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageHyperslice { }
xyz.swapee.rc.ChangellyExchangePageHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice = class {
  constructor() {
    this.changellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePage.__changellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__changellyExchange<THIS>>} */ (void 0)
    /**
     * Generates a fixed offer.
     */
    this.filterGetFixedOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer<THIS>>} */ (void 0)
    /**
     * Generates a floating-rate offer. Some exchanges like _Changelly_ return data
     * about fees immediately without creating a transaction.
     */
    this.filterGetOffer=/** @type {!xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer<THIS>>} */ (void 0)
    /**
     * Creates a pair of deposit and payout addresses.
     */
    this.filterCreateTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction<THIS>>} */ (void 0)
    /**
     * Looks up the transaction info.
     */
    this.filterGetTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction<THIS>>} */ (void 0)
    /**
     * Creates a pair of deposit and payout addresses for fixed-rate offers.
     */
    this.filterCreateFixedTransaction=/** @type {!xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction<THIS>>} */ (void 0)
    /**
     * Updates the status of the transaction.
     */
    this.filterCheckPayment=/** @type {!xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageBindingHyperslice

/**
 * A concrete class of _IChangellyExchangePageBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageBindingHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IChangellyExchangePageBindingHyperslice<THIS>}
 */
xyz.swapee.rc.ChangellyExchangePageBindingHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageBindingHyperslice { }
xyz.swapee.rc.ChangellyExchangePageBindingHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageFields&engineering.type.IEngineer&xyz.swapee.rc.IChangellyExchangePageCaster&com.changelly.UChangelly&io.changenow.UChangeNow&io.letsexchange.ULetsExchange&xyz.swapee.rc.UChangellyExchangePage)} xyz.swapee.rc.IChangellyExchangePage.constructor */
/** @interface xyz.swapee.rc.IChangellyExchangePage */
xyz.swapee.rc.IChangellyExchangePage = class extends /** @type {xyz.swapee.rc.IChangellyExchangePage.constructor&engineering.type.IEngineer.typeof&com.changelly.UChangelly.typeof&io.changenow.UChangeNow.typeof&io.letsexchange.ULetsExchange.typeof&xyz.swapee.rc.UChangellyExchangePage.typeof} */ (class {}) {
}
/**
 * Create a new *IChangellyExchangePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IChangellyExchangePage.changellyExchange} */
xyz.swapee.rc.IChangellyExchangePage.prototype.changellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterGetFixedOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetOffer} */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterGetOffer = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterCreateTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterGetTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterCreateFixedTransaction = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterCheckPayment = function() {}

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage&engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePage.Initialese>)} xyz.swapee.rc.ChangellyExchangePage.constructor */
/**
 * A concrete class of _IChangellyExchangePage_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePage
 * @implements {xyz.swapee.rc.IChangellyExchangePage} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePage.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePage = class extends /** @type {xyz.swapee.rc.ChangellyExchangePage.constructor&xyz.swapee.rc.IChangellyExchangePage.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangePage* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IChangellyExchangePage.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangePage* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePage.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePage.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.ChangellyExchangePage.__extend = function(...Extensions) {}

/**
 * Fields of the IChangellyExchangePage.
 * @interface xyz.swapee.rc.IChangellyExchangePageFields
 */
xyz.swapee.rc.IChangellyExchangePageFields = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageFields.prototype.changellyExchangeCtx = /** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} */ (void 0)

/** @typedef {xyz.swapee.rc.IChangellyExchangePage} */
xyz.swapee.rc.RecordIChangellyExchangePage

/** @typedef {xyz.swapee.rc.IChangellyExchangePage} xyz.swapee.rc.BoundIChangellyExchangePage */

/** @typedef {xyz.swapee.rc.ChangellyExchangePage} xyz.swapee.rc.BoundChangellyExchangePage */

/**
 * Contains getters to cast the _IChangellyExchangePage_ interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageCaster
 */
xyz.swapee.rc.IChangellyExchangePageCaster = class { }
/**
 * Cast the _IChangellyExchangePage_ instance into the _BoundIChangellyExchangePage_ type.
 * @type {!xyz.swapee.rc.BoundIChangellyExchangePage}
 */
xyz.swapee.rc.IChangellyExchangePageCaster.prototype.asIChangellyExchangePage
/**
 * Access the _ChangellyExchangePage_ prototype.
 * @type {!xyz.swapee.rc.BoundChangellyExchangePage}
 */
xyz.swapee.rc.IChangellyExchangePageCaster.prototype.superChangellyExchangePage

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeChangellyExchange} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.ChangellyExchangeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `changellyExchange` method from being executed.
 * - `sub` _(value: !IChangellyExchangePage.changellyExchange.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.changellyExchange.OptAnswers&gt;) =&gt; void_ Cancels a call to `changellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchange} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.changellyExchange.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `changellyExchange` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `afterReturns` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.changellyExchange.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: !IChangellyExchangePage.changellyExchange.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.changellyExchange.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachChangellyExchange} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.ChangellyExchangeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `changellyExchange` method from being executed.
 * - `sub` _(value: !IChangellyExchangePage.changellyExchange.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.changellyExchange.OptAnswers&gt;) =&gt; void_ Cancels a call to `changellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchange} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.changellyExchange.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchangeReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchangeReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _changellyExchange_ at the `afterEach` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.changellyExchange.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePage.ChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchangeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetFixedOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterGetFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterGetFixedOffer` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterGetFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterGetFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterGetFixedOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `afterReturns` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IChangellyExchangePage.filterGetFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetFixedOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetFixedOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterGetFixedOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterGetFixedOffer` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterGetFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetFixedOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterGetFixedOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetFixedOffer_ at the `afterEach` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetFixedOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetFixedOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetFixedOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetOffer} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterGetOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterGetOffer` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterGetOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterGetOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOffer} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterGetOffer` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `afterReturns` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IChangellyExchangePage.filterGetOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetOffer.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetOffer} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterGetOfferNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterGetOffer` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterGetOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetOffer.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterGetOffer` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOffer} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOffer = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOfferReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOfferReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data] Metadata passed to the pointcuts of _filterGetOffer_ at the `afterEach` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetOffer.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetOffer.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetOfferNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOfferReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateTransaction} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterCreateTransactionNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterCreateTransaction` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterCreateTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateTransaction.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterCreateTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransaction} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterCreateTransaction` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `afterReturns` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IChangellyExchangePage.filterCreateTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateTransaction.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateTransaction} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterCreateTransactionNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterCreateTransaction` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterCreateTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateTransaction.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterCreateTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransaction} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransactionReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransactionReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateTransaction_ at the `afterEach` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransactionReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetTransaction} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterGetTransactionNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterGetTransaction` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterGetTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetTransaction.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterGetTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransaction} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterGetTransaction` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `afterReturns` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IChangellyExchangePage.filterGetTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetTransaction.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetTransaction} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterGetTransactionNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterGetTransaction` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterGetTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetTransaction.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterGetTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransaction} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransactionReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransactionReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterGetTransaction_ at the `afterEach` joinpoint.
 * - `res` _IChangellyExchangePage.filterGetTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterGetTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterGetTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransactionReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateFixedTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateFixedTransaction} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterCreateFixedTransactionNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterCreateFixedTransaction` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterCreateFixedTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateFixedTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransaction} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterCreateFixedTransaction` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `afterReturns` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateFixedTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateFixedTransaction} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterCreateFixedTransactionNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterCreateFixedTransaction` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterCreateFixedTransaction` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateFixedTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransaction} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransaction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransactionReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransactionReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data] Metadata passed to the pointcuts of _filterCreateFixedTransaction_ at the `afterEach` joinpoint.
 * - `res` _IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `args` _IChangellyExchangePage.FilterCreateFixedTransactionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransactionReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCheckPayment */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCheckPayment} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterCheckPaymentNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterCheckPayment` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterCheckPayment.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCheckPayment.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterCheckPayment` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCheckPayment = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPayment */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPayment} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterCheckPayment.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCheckPayment.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPayment = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `filterCheckPayment` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `afterReturns` joinpoint.
 * - `res` _IChangellyExchangePage.filterCheckPayment.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCheckPayment.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `sub` _(value: IChangellyExchangePage.filterCheckPayment.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCheckPayment.OptAnswers&gt;) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCheckPayment */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCheckPayment} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePage.FilterCheckPaymentNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `filterCheckPayment` method from being executed.
 * - `sub` _(value: IChangellyExchangePage.filterCheckPayment.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCheckPayment.OptAnswers&gt;) =&gt; void_ Cancels a call to `filterCheckPayment` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCheckPayment = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPayment */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPayment} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `after` joinpoint.
 * - `res` _IChangellyExchangePage.filterCheckPayment.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCheckPayment.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPayment = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPaymentReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPaymentReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data] Metadata passed to the pointcuts of _filterCheckPayment_ at the `afterEach` joinpoint.
 * - `res` _IChangellyExchangePage.filterCheckPayment.OptAnswers&vert;!Promise&lt;void&gt;&vert;!Promise&lt;!IChangellyExchangePage.filterCheckPayment.OptAnswers&gt;_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `args` _IChangellyExchangePage.FilterCheckPaymentNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPaymentReturns = function(data) {}

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx) => (void|!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)} xyz.swapee.rc.IChangellyExchangePage.__changellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePage.__changellyExchange<!xyz.swapee.rc.IChangellyExchangePage>} xyz.swapee.rc.IChangellyExchangePage._changellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.changellyExchange} */
/**
 *  `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form The form.
 * - `locale` _string_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers The answers.
 * - `locale` _string_
 * - `rate` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `fixedId` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `estimatedFixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `minAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `maxAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `estimatedFloatAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `networkFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `partnerFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `visibleAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `id` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createTransactionError` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createdAt` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinAddress` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinExtraId` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `status` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers* ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * - `kycRequired` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `confirmedAmountFrom` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixed` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `currencyFrom` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `currencyTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `amountFrom` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `amountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `notFound` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `getOffer` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `checkPaymentError` _&#42;_ ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation} validation The validation.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors} errors ,,
 *
 *         The errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx The ctx.
 * - `validation` _!IChangellyExchangePage.changellyExchange.Validation_ The validation.
 * - `errors` _!IChangellyExchangePage.changellyExchange.Errors_ The errors.
 * - `answers` _!IChangellyExchangePage.changellyExchange.Answers_ The answers.
 * @return {void|!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>}
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange = function(form, answers, validation, errors, ctx) {}

/**
 * The form.
 * @record xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers&xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers&xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers&xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers&xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers&xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers)} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers = class extends /** @type {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers.constructor&xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers.prototype.locale = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers&xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers&xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers&xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers&xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers&xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers)} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers.typeof */
/**
 * The answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers = class extends /** @type {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers.constructor&xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.typeof&xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers.typeof} */ (class {}) { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers.prototype.locale = /** @type {string|undefined} */ (void 0)

/**
 * The validation.
 * @record xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation = class { }
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation

/**
 * ,,
 *
 *         The errors.
 * @record xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors = class { }
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors

/**
 * The ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx.prototype.validation = /** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx.prototype.errors = /** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx.prototype.answers = /** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} */ (void 0)

/**
 * The ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx = class { }
/**
 * The validation.
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx.prototype.validation = /** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation)|undefined} */ (void 0)
/**
 * The errors.
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx.prototype.errors = /** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors)|undefined} */ (void 0)
/**
 * The answers.
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx.prototype.answers = /** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers)|undefined} */ (void 0)

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx) => (void|xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)} xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer<!xyz.swapee.rc.IChangellyExchangePage>} xyz.swapee.rc.IChangellyExchangePage._filterGetFixedOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer} */
/**
 * Generates a fixed offer. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `ready` _&#42;_
 * - `getOffer` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers} answers The action answers.
 * - `rate` _&#42;_
 * - `fixedId` _&#42;_
 * - `estimatedFixedAmountTo` _&#42;_
 * - `minAmount` _&#42;_
 * - `maxAmount` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>}
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.fixed = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.ready = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.getOffer = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.rate = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.fixedId = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.estimatedFixedAmountTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.minAmount = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.maxAmount = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.rate = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.fixedId = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.estimatedFixedAmountTo = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.minAmount = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.maxAmount = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptCtx
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptCtx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx) => (void|xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)} xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer<!xyz.swapee.rc.IChangellyExchangePage>} xyz.swapee.rc.IChangellyExchangePage._filterGetOffer */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetOffer} */
/**
 * Generates a floating-rate offer. Some exchanges like _Changelly_ return data
 * about fees immediately without creating a transaction. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `ready` _&#42;_
 * - `getOffer` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers} answers The action answers.
 * - `rate` _&#42;_
 * - `estimatedFloatAmountTo` _&#42;_
 * - `networkFee` _&#42;_
 * - `partnerFee` _&#42;_
 * - `visibleAmount` _&#42;_
 * - `minAmount` _&#42;_
 * - `maxAmount` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>}
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.currencyTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.fixed = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.ready = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.getOffer = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.rate = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.estimatedFloatAmountTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.networkFee = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.partnerFee = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.visibleAmount = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.minAmount = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.maxAmount = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.rate = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.estimatedFloatAmountTo = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.networkFee = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.partnerFee = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.visibleAmount = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.minAmount = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.maxAmount = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptCtx
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptCtx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptCtx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx) => (void|xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)} xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction<!xyz.swapee.rc.IChangellyExchangePage>} xyz.swapee.rc.IChangellyExchangePage._filterCreateTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction} */
/**
 * Creates a pair of deposit and payout addresses. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `address` _&#42;_
 * - `refundAddress` _&#42;_
 * - `createTransaction` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers} answers The action answers.
 * - `id` _&#42;_
 * - `createTransactionError` _&#42;_
 * - `createdAt` _&#42;_
 * - `payinAddress` _&#42;_
 * - `payinExtraId` _&#42;_
 * - `status` _&#42;_
 * - `kycRequired` _&#42;_
 * - `confirmedAmountFrom` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>}
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.currencyTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.fixed = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.address = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.refundAddress = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.createTransaction = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.id = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.createTransactionError = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.createdAt = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.payinAddress = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.payinExtraId = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.status = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.kycRequired = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.confirmedAmountFrom = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.id = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.createTransactionError = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.createdAt = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.payinAddress = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.payinExtraId = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.status = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.kycRequired = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.confirmedAmountFrom = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptCtx
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptCtx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptCtx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx) => (void|xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)} xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction<!xyz.swapee.rc.IChangellyExchangePage>} xyz.swapee.rc.IChangellyExchangePage._filterGetTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction} */
/**
 * Looks up the transaction info. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form} form The action form.
 * - `tid` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers} answers The action answers.
 * - `fixed` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `amountFrom` _&#42;_
 * - `amountTo` _&#42;_
 * - `networkFee` _&#42;_
 * - `partnerFee` _&#42;_
 * - `visibleAmount` _&#42;_
 * - `notFound` _&#42;_
 * - `rate` _&#42;_
 * - `id` _&#42;_
 * - `createdAt` _&#42;_
 * - `payinAddress` _&#42;_
 * - `payinExtraId` _&#42;_
 * - `status` _&#42;_
 * - `kycRequired` _&#42;_
 * - `confirmedAmountFrom` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>}
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form.prototype.tid = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.fixed = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.currencyTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.amountTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.networkFee = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.partnerFee = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.visibleAmount = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.notFound = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.rate = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.id = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.createdAt = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.payinAddress = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.payinExtraId = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.status = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.kycRequired = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.confirmedAmountFrom = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.fixed = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.currencyFrom = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.currencyTo = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.amountFrom = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.amountTo = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.networkFee = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.partnerFee = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.visibleAmount = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.notFound = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.rate = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.id = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.createdAt = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.payinAddress = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.payinExtraId = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.status = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.kycRequired = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.confirmedAmountFrom = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptCtx
 */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptCtx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptCtx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx) => (void|xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)} xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction<!xyz.swapee.rc.IChangellyExchangePage>} xyz.swapee.rc.IChangellyExchangePage._filterCreateFixedTransaction */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction} */
/**
 * Creates a pair of deposit and payout addresses for fixed-rate offers. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form} form The action form.
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `address` _&#42;_
 * - `refundAddress` _&#42;_
 * - `createTransaction` _&#42;_
 * - `fixedId` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers} answers The action answers.
 * - `getOffer` _&#42;_
 * - `id` _&#42;_
 * - `createTransactionError` _&#42;_
 * - `createdAt` _&#42;_
 * - `payinAddress` _&#42;_
 * - `payinExtraId` _&#42;_
 * - `status` _&#42;_
 * - `kycRequired` _&#42;_
 * - `fixedAmountTo` _&#42;_
 * - `confirmedAmountFrom` _&#42;_
 * - `networkFee` _&#42;_
 * - `partnerFee` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>}
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.amountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.currencyFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.currencyTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.fixed = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.address = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.refundAddress = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.createTransaction = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.fixedId = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.getOffer = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.id = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.createTransactionError = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.createdAt = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.payinAddress = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.payinExtraId = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.status = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.kycRequired = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.fixedAmountTo = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.confirmedAmountFrom = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.networkFee = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.partnerFee = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.getOffer = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.id = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.createTransactionError = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.createdAt = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.payinAddress = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.payinExtraId = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.status = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.kycRequired = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.fixedAmountTo = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.confirmedAmountFrom = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.networkFee = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.partnerFee = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptCtx
 */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptCtx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptCtx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptCtx

/**
 * @typedef {(this: THIS, form: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx) => (void|xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)} xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment<!xyz.swapee.rc.IChangellyExchangePage>} xyz.swapee.rc.IChangellyExchangePage._filterCheckPayment */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment} */
/**
 * Updates the status of the transaction. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form} form The action form.
 * - `id` _&#42;_
 * - `checkPayment` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers} answers The action answers.
 * - `status` _&#42;_
 * - `checkPaymentError` _&#42;_
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation} validation The action validation.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors} errors The action errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx} ctx The action ctx.
 * @return {void|xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<void>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>}
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment = function(form, answers, validation, errors, ctx) {}

/**
 * The action form.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form.prototype.id = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form.prototype.checkPayment = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers.prototype.status = /** @type {*} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers.prototype.checkPaymentError = /** @type {*} */ (void 0)

/**
 * The action answers.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers.prototype.status = /** @type {(*)|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers.prototype.checkPaymentError = /** @type {(*)|undefined} */ (void 0)

/**
 * The action validation.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation

/**
 * The action errors.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx

/**
 * The action ctx.
 * @record xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptCtx
 */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptCtx = class { }
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptCtx.prototype.constructor = xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptCtx

// nss:xyz.swapee.rc,xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc.IChangellyExchangePage
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml}  c658f76c826180a39db89d8927a6e60e */
/** @constructor xyz.swapee.rc.ChangellyExchange */
xyz.swapee.rc.ChangellyExchange = class { }
xyz.swapee.rc.ChangellyExchange.prototype.constructor = xyz.swapee.rc.ChangellyExchange

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers)} xyz.swapee.rc.ChangellyExchange.Answers.constructor */
/**
 * The answers.
 * @record xyz.swapee.rc.ChangellyExchange.Answers
 */
xyz.swapee.rc.ChangellyExchange.Answers = class extends /** @type {xyz.swapee.rc.ChangellyExchange.Answers.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.ChangellyExchange.Answers.prototype.props = /** @type {!xyz.swapee.rc.ChangellyExchange.Answers.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ChangellyExchange.Answers.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form)} xyz.swapee.rc.ChangellyExchange.Form.constructor */
/**
 * The form.
 * @record xyz.swapee.rc.ChangellyExchange.Form
 */
xyz.swapee.rc.ChangellyExchange.Form = class extends /** @type {xyz.swapee.rc.ChangellyExchange.Form.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.ChangellyExchange.Form.prototype.props = /** @type {!xyz.swapee.rc.ChangellyExchange.Form.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ChangellyExchange.Form.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors)} xyz.swapee.rc.ChangellyExchange.Errors.constructor */
/**
 * The errors.
 * @record xyz.swapee.rc.ChangellyExchange.Errors
 */
xyz.swapee.rc.ChangellyExchange.Errors = class extends /** @type {xyz.swapee.rc.ChangellyExchange.Errors.constructor} */ (class {}) { }
/**
 * ,,
 *       The props for VSCode.
 */
xyz.swapee.rc.ChangellyExchange.Errors.prototype.props = /** @type {!xyz.swapee.rc.ChangellyExchange.Errors.Props} */ (void 0)

/**
 * @typedef {typeof __$te_plain} xyz.swapee.rc.ChangellyExchange.Errors.Props ,,
 *       The props for VSCode.
 */

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation)} xyz.swapee.rc.ChangellyExchange.Validation.constructor */
/**
 * The validation.
 * @record xyz.swapee.rc.ChangellyExchange.Validation
 */
xyz.swapee.rc.ChangellyExchange.Validation = class extends /** @type {xyz.swapee.rc.ChangellyExchange.Validation.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.ChangellyExchange.Validation.prototype.props = /** @type {!xyz.swapee.rc.ChangellyExchange.Validation.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ChangellyExchange.Validation.Props The props for VSCode. */

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx)} xyz.swapee.rc.ChangellyExchange.Ctx.constructor */
/**
 * The ctx.
 * @record xyz.swapee.rc.ChangellyExchange.Ctx
 */
xyz.swapee.rc.ChangellyExchange.Ctx = class extends /** @type {xyz.swapee.rc.ChangellyExchange.Ctx.constructor} */ (class {}) { }
/**
 * The props for VSCode.
 */
xyz.swapee.rc.ChangellyExchange.Ctx.prototype.props = /** @type {!xyz.swapee.rc.ChangellyExchange.Ctx.Props} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.ChangellyExchange.Ctx.Props The props for VSCode. */

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml}  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {typeof __$te_plain} xyz.swapee.rc.IChangellyExchangePageView.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePageView)} xyz.swapee.rc.AbstractChangellyExchangePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePageView} xyz.swapee.rc.ChangellyExchangePageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IChangellyExchangePageView` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePageView
 */
xyz.swapee.rc.AbstractChangellyExchangePageView = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePageView.constructor&xyz.swapee.rc.ChangellyExchangePageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePageView.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)|!xyz.swapee.rc.IChangellyExchangePageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)|!xyz.swapee.rc.IChangellyExchangePageViewHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)|!xyz.swapee.rc.IChangellyExchangePageViewHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeViewChangellyExchange>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchange>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewChangellyExchangeThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewChangellyExchangeCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachViewChangellyExchange>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchange>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchangeReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeGetChangellyExchange>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchange>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetChangellyExchangeThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetChangellyExchangeCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachGetChangellyExchange>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchange>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchangeReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeSetChangellyExchangeCtx>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtx>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetChangellyExchangeCtxThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetChangellyExchangeCtxReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetChangellyExchangeCtxCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachSetChangellyExchangeCtx>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtx>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetChangellyExchangeCtxReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtxReturns>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_ChangellyExchangePartial=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangePartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangePartial>} */ (void 0)
    /**
     * After the method.
     */
    this.after_ChangellyExchangePartial=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartial>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_ChangellyExchangePartialThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_ChangellyExchangePartialReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_ChangellyExchangePartialCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangeView>} */ (void 0)
    /**
     * After the method.
     */
    this.after_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeView>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_ChangellyExchangeViewThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_ChangellyExchangeViewReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_ChangellyExchangeViewCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewCancels>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEach_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEach_ChangellyExchangeView>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeView>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_ChangellyExchangeViewReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeViewReturns>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice

/**
 * A concrete class of _IChangellyExchangePageViewJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice { }
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterViewChangellyExchangeThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterViewChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterViewChangellyExchangeCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachViewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachViewChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetChangellyExchangeThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetChangellyExchangeCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachGetChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachGetChangellyExchangeReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSetChangellyExchangeCtxThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSetChangellyExchangeCtxReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSetChangellyExchangeCtxCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEachSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEachSetChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEachSetChangellyExchangeCtxReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_ChangellyExchangePartial=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_ChangellyExchangePartial=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_ChangellyExchangePartialThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_ChangellyExchangePartialReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_ChangellyExchangePartialCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.before_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.after_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.after_ChangellyExchangeViewThrows=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.after_ChangellyExchangeViewReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.after_ChangellyExchangeViewCancels=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels<THIS>>} */ (void 0)
    /**
     * Before each part of the combined method executes.
     */
    this.beforeEach_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView<THIS>>} */ (void 0)
    /**
     * After each of the parts of the combined method.
     */
    this.afterEach_ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView<THIS>>} */ (void 0)
    /**
     * After each of the returning parts of the combined method.
     */
    this.afterEach_ChangellyExchangeViewReturns=/** @type {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice

/**
 * A concrete class of _IChangellyExchangePageViewJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelBindingHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice { }
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IChangellyExchangePageView`'s methods.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel = class { }
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeViewChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchangeThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchangeReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchangeCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEachViewChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachViewChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachViewChangellyExchangeReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeGetChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchangeThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchangeReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchangeCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEachGetChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachGetChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachGetChangellyExchangeReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeSetChangellyExchangeCtx = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtx = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtxThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtxReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtxCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEachSetChangellyExchangeCtx = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachSetChangellyExchangeCtx = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtxReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachSetChangellyExchangeCtxReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangePartial} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.before_ChangellyExchangePartial = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartial} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartial = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartialThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartialReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartialCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.before_ChangellyExchangeView = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeView = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeViewThrows = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeViewReturns = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeViewCancels = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEach_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEach_ChangellyExchangeView = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEach_ChangellyExchangeView = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeViewReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEach_ChangellyExchangeViewReturns = function() {}

/**
 * A concrete class of _IChangellyExchangePageViewJoinpointModel_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewJoinpointModel
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel} An interface that enumerates the joinpoints of `IChangellyExchangePageView`'s methods.
 */
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModel = class extends xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel { }
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModel.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel} */
xyz.swapee.rc.RecordIChangellyExchangePageViewJoinpointModel

/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel} xyz.swapee.rc.BoundIChangellyExchangePageViewJoinpointModel */

/** @typedef {xyz.swapee.rc.ChangellyExchangePageViewJoinpointModel} xyz.swapee.rc.BoundChangellyExchangePageViewJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller)} xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller} xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.constructor&xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller)|(!xyz.swapee.rc.IChangellyExchangePageInstaller|typeof xyz.swapee.rc.ChangellyExchangePageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller)|(!xyz.swapee.rc.IChangellyExchangePageInstaller|typeof xyz.swapee.rc.ChangellyExchangePageInstaller)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller)|(!xyz.swapee.rc.IChangellyExchangePageInstaller|typeof xyz.swapee.rc.ChangellyExchangePageInstaller)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese[]) => xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller} xyz.swapee.rc.ChangellyExchangePageViewAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IChangellyExchangePageInstaller)} xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageInstaller} xyz.swapee.rc.IChangellyExchangePageInstaller.typeof */
/** @interface xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IChangellyExchangePageInstaller.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeViewChangellyExchange=/** @type {number} */ (void 0)
    this.afterViewChangellyExchange=/** @type {number} */ (void 0)
    this.afterViewChangellyExchangeThrows=/** @type {number} */ (void 0)
    this.afterViewChangellyExchangeReturns=/** @type {number} */ (void 0)
    this.afterViewChangellyExchangeCancels=/** @type {number} */ (void 0)
    this.beforeEachViewChangellyExchange=/** @type {number} */ (void 0)
    this.afterEachViewChangellyExchange=/** @type {number} */ (void 0)
    this.afterEachViewChangellyExchangeReturns=/** @type {number} */ (void 0)
    this.beforeGetChangellyExchange=/** @type {number} */ (void 0)
    this.afterGetChangellyExchange=/** @type {number} */ (void 0)
    this.afterGetChangellyExchangeThrows=/** @type {number} */ (void 0)
    this.afterGetChangellyExchangeReturns=/** @type {number} */ (void 0)
    this.afterGetChangellyExchangeCancels=/** @type {number} */ (void 0)
    this.beforeEachGetChangellyExchange=/** @type {number} */ (void 0)
    this.afterEachGetChangellyExchange=/** @type {number} */ (void 0)
    this.afterEachGetChangellyExchangeReturns=/** @type {number} */ (void 0)
    this.beforeSetChangellyExchangeCtx=/** @type {number} */ (void 0)
    this.afterSetChangellyExchangeCtx=/** @type {number} */ (void 0)
    this.afterSetChangellyExchangeCtxThrows=/** @type {number} */ (void 0)
    this.afterSetChangellyExchangeCtxReturns=/** @type {number} */ (void 0)
    this.afterSetChangellyExchangeCtxCancels=/** @type {number} */ (void 0)
    this.beforeEachSetChangellyExchangeCtx=/** @type {number} */ (void 0)
    this.afterEachSetChangellyExchangeCtx=/** @type {number} */ (void 0)
    this.afterEachSetChangellyExchangeCtxReturns=/** @type {number} */ (void 0)
    this.before_ChangellyExchangePartial=/** @type {number} */ (void 0)
    this.after_ChangellyExchangePartial=/** @type {number} */ (void 0)
    this.after_ChangellyExchangePartialThrows=/** @type {number} */ (void 0)
    this.after_ChangellyExchangePartialReturns=/** @type {number} */ (void 0)
    this.after_ChangellyExchangePartialCancels=/** @type {number} */ (void 0)
    this.before_ChangellyExchangeView=/** @type {number} */ (void 0)
    this.after_ChangellyExchangeView=/** @type {number} */ (void 0)
    this.after_ChangellyExchangeViewThrows=/** @type {number} */ (void 0)
    this.after_ChangellyExchangeViewReturns=/** @type {number} */ (void 0)
    this.after_ChangellyExchangeViewCancels=/** @type {number} */ (void 0)
    this.beforeEach_ChangellyExchangeView=/** @type {number} */ (void 0)
    this.afterEach_ChangellyExchangeView=/** @type {number} */ (void 0)
    this.afterEach_ChangellyExchangeViewReturns=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  viewChangellyExchange() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  getChangellyExchange() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  setChangellyExchangeCtx() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  ChangellyExchangePartial() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  ChangellyExchangeView() { }
}
/**
 * Create a new *IChangellyExchangePageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese>)} xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller} xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.typeof */
/**
 * A concrete class of _IChangellyExchangePageViewAspectsInstaller_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller = class extends /** @type {xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.constructor&xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangePageViewAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangePageViewAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageView.ViewChangellyExchangeNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePageView.ViewChangellyExchangeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePageView.ViewChangellyExchangeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `viewChangellyExchange` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView) => void} sub Cancels a call to `viewChangellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `viewChangellyExchange` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageView.GetChangellyExchangeNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx The context.
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form The form.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePageView.GetChangellyExchangeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePageView.GetChangellyExchangeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getChangellyExchange` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView) => void} sub Cancels a call to `getChangellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getChangellyExchange` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageView.SetChangellyExchangeCtxNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation The translation for the user's locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePageView.SetChangellyExchangeCtxNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePageView.SetChangellyExchangeCtxNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `setChangellyExchangeCtx` method from being executed.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx) => void} sub Cancels a call to `setChangellyExchangeCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `setChangellyExchangeCtx` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartialNArgs
 * @prop {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers The answers.
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors} errors The errors.
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions} actions The actions.
 * @prop {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation The translation for the chosen user locale.
 */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartialNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartialNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `ChangellyExchangePartial` method from being executed.
 * @prop {(value: engineering.type.VNode) => void} sub Cancels a call to `ChangellyExchangePartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `ChangellyExchangePartial` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData
 * @prop {engineering.type.VNode} res The return of the method after it's successfully run.
 * @prop {(value: engineering.type.VNode) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `ChangellyExchangeView` method from being executed.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `ChangellyExchangeView` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.rc.IChangellyExchangePageView.Initialese[]) => xyz.swapee.rc.IChangellyExchangePageView} xyz.swapee.rc.ChangellyExchangePageViewConstructor */

/** @typedef {symbol} xyz.swapee.rc.ChangellyExchangePageViewMetaUniversal The symbol used to inform the meta-universal of mesa-universals. */

/**
 * @typedef {Object} xyz.swapee.rc.UChangellyExchangePageView.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {xyz.swapee.rc.IChangellyExchangePageView} [changellyExchangePageView]
 */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePageViewUniversal)} xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal} xyz.swapee.rc.ChangellyExchangePageViewUniversal.typeof */
/**
 * An abstract class of `xyz.swapee.rc.UChangellyExchangePageView` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.constructor&xyz.swapee.rc.ChangellyExchangePageViewUniversal.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.__trait = function(...Implementations) {}

/** @typedef {xyz.swapee.rc.ChangellyExchangePageViewMetaUniversal} xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.MetaUniversal The meta-universal. */

/** @typedef {new (...args: !xyz.swapee.rc.UChangellyExchangePageView.Initialese[]) => xyz.swapee.rc.UChangellyExchangePageView} xyz.swapee.rc.UChangellyExchangePageViewConstructor */

/** @typedef {function(new: xyz.swapee.rc.UChangellyExchangePageViewFields&engineering.type.IEngineer&xyz.swapee.rc.UChangellyExchangePageViewCaster)} xyz.swapee.rc.UChangellyExchangePageView.constructor */
/**
 * A trait that allows to access an _IChangellyExchangePageView_ object via a `.changellyExchangePageView` field.
 * @interface xyz.swapee.rc.UChangellyExchangePageView
 */
xyz.swapee.rc.UChangellyExchangePageView = class extends /** @type {xyz.swapee.rc.UChangellyExchangePageView.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *UChangellyExchangePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UChangellyExchangePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.UChangellyExchangePageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.UChangellyExchangePageView&engineering.type.IInitialiser<!xyz.swapee.rc.UChangellyExchangePageView.Initialese>)} xyz.swapee.rc.ChangellyExchangePageViewUniversal.constructor */
/** @typedef {typeof xyz.swapee.rc.UChangellyExchangePageView} xyz.swapee.rc.UChangellyExchangePageView.typeof */
/**
 * A concrete class of _UChangellyExchangePageView_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewUniversal
 * @implements {xyz.swapee.rc.UChangellyExchangePageView} A trait that allows to access an _IChangellyExchangePageView_ object via a `.changellyExchangePageView` field.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UChangellyExchangePageView.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePageViewUniversal = class extends /** @type {xyz.swapee.rc.ChangellyExchangePageViewUniversal.constructor&xyz.swapee.rc.UChangellyExchangePageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *UChangellyExchangePageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.UChangellyExchangePageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *UChangellyExchangePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.UChangellyExchangePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePageViewUniversal.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.ChangellyExchangePageViewUniversal.__extend = function(...Extensions) {}

/**
 * Fields of the UChangellyExchangePageView.
 * @interface xyz.swapee.rc.UChangellyExchangePageViewFields
 */
xyz.swapee.rc.UChangellyExchangePageViewFields = class { }
/**
 * Default `null`.
 */
xyz.swapee.rc.UChangellyExchangePageViewFields.prototype.changellyExchangePageView = /** @type {xyz.swapee.rc.IChangellyExchangePageView} */ (void 0)

/** @typedef {xyz.swapee.rc.UChangellyExchangePageView} */
xyz.swapee.rc.RecordUChangellyExchangePageView

/** @typedef {xyz.swapee.rc.UChangellyExchangePageView} xyz.swapee.rc.BoundUChangellyExchangePageView */

/** @typedef {xyz.swapee.rc.ChangellyExchangePageViewUniversal} xyz.swapee.rc.BoundChangellyExchangePageViewUniversal */

/**
 * Contains getters to cast the _UChangellyExchangePageView_ interface.
 * @interface xyz.swapee.rc.UChangellyExchangePageViewCaster
 */
xyz.swapee.rc.UChangellyExchangePageViewCaster = class { }
/**
 * Provides direct access to _UChangellyExchangePageView_ via the _BoundUChangellyExchangePageView_ universal.
 * @type {!xyz.swapee.rc.BoundChangellyExchangePageView}
 */
xyz.swapee.rc.UChangellyExchangePageViewCaster.prototype.asChangellyExchangePageView
/**
 * Cast the _UChangellyExchangePageView_ instance into the _BoundUChangellyExchangePageView_ type.
 * @type {!xyz.swapee.rc.BoundUChangellyExchangePageView}
 */
xyz.swapee.rc.UChangellyExchangePageViewCaster.prototype.asUChangellyExchangePageView
/**
 * Access the _ChangellyExchangePageViewUniversal_ prototype.
 * @type {!xyz.swapee.rc.BoundChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.UChangellyExchangePageViewCaster.prototype.superChangellyExchangePageViewUniversal

/** @typedef {function(new: xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster<THIS>&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.rc.BChangellyExchangePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IChangellyExchangePageView* that bind to an instance.
 * @interface xyz.swapee.rc.BChangellyExchangePageViewAspects
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageViewAspects = class extends /** @type {xyz.swapee.rc.BChangellyExchangePageViewAspects.constructor&xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.rc.BChangellyExchangePageViewAspects.prototype.constructor = xyz.swapee.rc.BChangellyExchangePageViewAspects

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangePageViewAspects)} xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects} xyz.swapee.rc.ChangellyExchangePageViewAspects.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IChangellyExchangePageViewAspects` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangePageViewAspects
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.constructor&xyz.swapee.rc.ChangellyExchangePageViewAspects.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangePageViewAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageViewAspects|typeof xyz.swapee.rc.ChangellyExchangePageViewAspects)|(!xyz.swapee.rc.BChangellyExchangePageViewAspects|typeof xyz.swapee.rc.BChangellyExchangePageViewAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageViewAspects|typeof xyz.swapee.rc.ChangellyExchangePageViewAspects)|(!xyz.swapee.rc.BChangellyExchangePageViewAspects|typeof xyz.swapee.rc.BChangellyExchangePageViewAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageViewAspects|typeof xyz.swapee.rc.ChangellyExchangePageViewAspects)|(!xyz.swapee.rc.BChangellyExchangePageViewAspects|typeof xyz.swapee.rc.BChangellyExchangePageViewAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese[]) => xyz.swapee.rc.IChangellyExchangePageViewAspects} xyz.swapee.rc.ChangellyExchangePageViewAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster&xyz.swapee.rc.BChangellyExchangePageViewAspects<!xyz.swapee.rc.IChangellyExchangePageViewAspects>&xyz.swapee.rc.IChangellyExchangePage&_idio.IRedirectMod&xyz.swapee.rc.BChangellyExchangePageAspects)} xyz.swapee.rc.IChangellyExchangePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.BChangellyExchangePageViewAspects} xyz.swapee.rc.BChangellyExchangePageViewAspects.typeof */
/** @typedef {typeof _idio.IRedirectMod} _idio.IRedirectMod.typeof */
/**
 * The aspects of the *IChangellyExchangePageView*.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewAspects
 */
xyz.swapee.rc.IChangellyExchangePageViewAspects = class extends /** @type {xyz.swapee.rc.IChangellyExchangePageViewAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.BChangellyExchangePageViewAspects.typeof&xyz.swapee.rc.IChangellyExchangePage.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.BChangellyExchangePageAspects.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.props=/** @type {xyz.swapee.rc.IChangellyExchangePageViewAspects} */ (void 0)
  }
}
/**
 * Create a new *IChangellyExchangePageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IChangellyExchangePageViewAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageViewAspects&engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese>)} xyz.swapee.rc.ChangellyExchangePageViewAspects.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewAspects} xyz.swapee.rc.IChangellyExchangePageViewAspects.typeof */
/**
 * A concrete class of _IChangellyExchangePageViewAspects_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewAspects
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewAspects} The aspects of the *IChangellyExchangePageView*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePageViewAspects = class extends /** @type {xyz.swapee.rc.ChangellyExchangePageViewAspects.constructor&xyz.swapee.rc.IChangellyExchangePageViewAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangePageViewAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangePageViewAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePageViewAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.ChangellyExchangePageViewAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BChangellyExchangePageViewAspects_ interface.
 * @interface xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster = class { }
/**
 * Cast the _BChangellyExchangePageViewAspects_ instance into the _BoundIChangellyExchangePageView_ type.
 * @type {!xyz.swapee.rc.BoundIChangellyExchangePageView}
 */
xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster.prototype.asIChangellyExchangePageView

/**
 * Contains getters to cast the _IChangellyExchangePageViewAspects_ interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster
 */
xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster = class { }
/**
 * Cast the _IChangellyExchangePageViewAspects_ instance into the _BoundIChangellyExchangePageView_ type.
 * @type {!xyz.swapee.rc.BoundIChangellyExchangePageView}
 */
xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster.prototype.asIChangellyExchangePageView

/** @typedef {xyz.swapee.rc.IChangellyExchangePageView.Initialese} xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese */

/** @typedef {function(new: xyz.swapee.rc.HyperChangellyExchangePageView)} xyz.swapee.rc.AbstractHyperChangellyExchangePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.HyperChangellyExchangePageView} xyz.swapee.rc.HyperChangellyExchangePageView.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IHyperChangellyExchangePageView` interface.
 * @constructor xyz.swapee.rc.AbstractHyperChangellyExchangePageView
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView = class extends /** @type {xyz.swapee.rc.AbstractHyperChangellyExchangePageView.constructor&xyz.swapee.rc.HyperChangellyExchangePageView.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.prototype.constructor = xyz.swapee.rc.AbstractHyperChangellyExchangePageView
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.class = /** @type {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IHyperChangellyExchangePageView|typeof xyz.swapee.rc.HyperChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IHyperChangellyExchangePageView|typeof xyz.swapee.rc.HyperChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IHyperChangellyExchangePageView|typeof xyz.swapee.rc.HyperChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspects|!Array<!xyz.swapee.rc.IChangellyExchangePageViewAspects>|function(new: xyz.swapee.rc.IChangellyExchangePageViewAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the IChangellyExchangePageView to implement aspects.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese[]) => xyz.swapee.rc.IHyperChangellyExchangePageView} xyz.swapee.rc.HyperChangellyExchangePageViewConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IHyperChangellyExchangePageViewCaster&xyz.swapee.rc.IChangellyExchangePageView)} xyz.swapee.rc.IHyperChangellyExchangePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageView} xyz.swapee.rc.IChangellyExchangePageView.typeof */
/** @interface xyz.swapee.rc.IHyperChangellyExchangePageView */
xyz.swapee.rc.IHyperChangellyExchangePageView = class extends /** @type {xyz.swapee.rc.IHyperChangellyExchangePageView.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IChangellyExchangePageView.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperChangellyExchangePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IHyperChangellyExchangePageView.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IHyperChangellyExchangePageView&engineering.type.IInitialiser<!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese>)} xyz.swapee.rc.HyperChangellyExchangePageView.constructor */
/** @typedef {typeof xyz.swapee.rc.IHyperChangellyExchangePageView} xyz.swapee.rc.IHyperChangellyExchangePageView.typeof */
/**
 * A concrete class of _IHyperChangellyExchangePageView_ instances.
 * @constructor xyz.swapee.rc.HyperChangellyExchangePageView
 * @implements {xyz.swapee.rc.IHyperChangellyExchangePageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese>} ‎
 */
xyz.swapee.rc.HyperChangellyExchangePageView = class extends /** @type {xyz.swapee.rc.HyperChangellyExchangePageView.constructor&xyz.swapee.rc.IHyperChangellyExchangePageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperChangellyExchangePageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperChangellyExchangePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.HyperChangellyExchangePageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.HyperChangellyExchangePageView.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.rc.IHyperChangellyExchangePageView} */
xyz.swapee.rc.RecordIHyperChangellyExchangePageView

/** @typedef {xyz.swapee.rc.IHyperChangellyExchangePageView} xyz.swapee.rc.BoundIHyperChangellyExchangePageView */

/** @typedef {xyz.swapee.rc.HyperChangellyExchangePageView} xyz.swapee.rc.BoundHyperChangellyExchangePageView */

/**
 * Contains getters to cast the _IHyperChangellyExchangePageView_ interface.
 * @interface xyz.swapee.rc.IHyperChangellyExchangePageViewCaster
 */
xyz.swapee.rc.IHyperChangellyExchangePageViewCaster = class { }
/**
 * Cast the _IHyperChangellyExchangePageView_ instance into the _BoundIHyperChangellyExchangePageView_ type.
 * @type {!xyz.swapee.rc.BoundIHyperChangellyExchangePageView}
 */
xyz.swapee.rc.IHyperChangellyExchangePageViewCaster.prototype.asIHyperChangellyExchangePageView
/**
 * Access the _HyperChangellyExchangePageView_ prototype.
 * @type {!xyz.swapee.rc.BoundHyperChangellyExchangePageView}
 */
xyz.swapee.rc.IHyperChangellyExchangePageViewCaster.prototype.superHyperChangellyExchangePageView

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewHyperslice
 */
xyz.swapee.rc.IChangellyExchangePageViewHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageView._viewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._viewChangellyExchange>} */ (void 0)
    /**
     * The internal view handler for the `changellyExchange` action.
     */
    this.getChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageView._getChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._getChangellyExchange>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageView._setChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._setChangellyExchangeCtx>} */ (void 0)
    /**
     * The _ChangellyExchange_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangeView>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageViewHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageViewHyperslice

/**
 * A concrete class of _IChangellyExchangePageViewHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.rc.ChangellyExchangePageViewHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageViewHyperslice { }
xyz.swapee.rc.ChangellyExchangePageViewHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageViewHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice = class {
  constructor() {
    /**
     * Assigns required view data to the context, then redirects, or invokes another pagelet.
     */
    this.viewChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange<THIS>>} */ (void 0)
    /**
     * The internal view handler for the `changellyExchange` action.
     */
    this.getChangellyExchange=/** @type {!xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange<THIS>>} */ (void 0)
    /**
     * Assigns the context based on answers and translations.
     */
    this.setChangellyExchangeCtx=/** @type {!xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx<THIS>>} */ (void 0)
    /**
     * The _ChangellyExchange_ view with partials and translation mechanics that has
     * access to answers written by the controller.
     */
    this.ChangellyExchangeView=/** @type {!xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView<THIS>>} */ (void 0)
  }
}
xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice

/**
 * A concrete class of _IChangellyExchangePageViewBindingHyperslice_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageViewBindingHyperslice
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice<THIS>}
 */
xyz.swapee.rc.ChangellyExchangePageViewBindingHyperslice = class extends xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice { }
xyz.swapee.rc.ChangellyExchangePageViewBindingHyperslice.prototype.constructor = xyz.swapee.rc.ChangellyExchangePageViewBindingHyperslice

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageViewFields&engineering.type.IEngineer&xyz.swapee.rc.IChangellyExchangePageViewCaster&_idio.IRedirectMod&xyz.swapee.rc.UChangellyExchangePageView)} xyz.swapee.rc.IChangellyExchangePageView.constructor */
/** @interface xyz.swapee.rc.IChangellyExchangePageView */
xyz.swapee.rc.IChangellyExchangePageView = class extends /** @type {xyz.swapee.rc.IChangellyExchangePageView.constructor&engineering.type.IEngineer.typeof&_idio.IRedirectMod.typeof&xyz.swapee.rc.UChangellyExchangePageView.typeof} */ (class {}) {
}
/**
 * Create a new *IChangellyExchangePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IChangellyExchangePageView.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView.viewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageView.prototype.viewChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView.getChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageView.prototype.getChangellyExchange = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView.setChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageView.prototype.setChangellyExchangeCtx = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial} */
xyz.swapee.rc.IChangellyExchangePageView.prototype.ChangellyExchangePartial = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageView.prototype.ChangellyExchangeView = function() {}

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageView&engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageView.Initialese>)} xyz.swapee.rc.ChangellyExchangePageView.constructor */
/**
 * A concrete class of _IChangellyExchangePageView_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangePageView
 * @implements {xyz.swapee.rc.IChangellyExchangePageView} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageView.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangePageView = class extends /** @type {xyz.swapee.rc.ChangellyExchangePageView.constructor&xyz.swapee.rc.IChangellyExchangePageView.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangePageView* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IChangellyExchangePageView.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangePageView* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangePageView.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangePageView.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.ChangellyExchangePageView.__extend = function(...Extensions) {}

/**
 * Fields of the IChangellyExchangePageView.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewFields
 */
xyz.swapee.rc.IChangellyExchangePageViewFields = class { }
/**
 * The navs for _GET_ paths (e.g., to use for redirects).
 */
xyz.swapee.rc.IChangellyExchangePageViewFields.prototype.GET = /** @type {!xyz.swapee.rc.IChangellyExchangePageView.GET} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageViewFields.prototype.changellyExchangeTranslations = /** @type {!Object<string, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation>} */ (void 0)

/** @typedef {xyz.swapee.rc.IChangellyExchangePageView} */
xyz.swapee.rc.RecordIChangellyExchangePageView

/** @typedef {xyz.swapee.rc.IChangellyExchangePageView} xyz.swapee.rc.BoundIChangellyExchangePageView */

/** @typedef {xyz.swapee.rc.ChangellyExchangePageView} xyz.swapee.rc.BoundChangellyExchangePageView */

/**
 * @typedef {Object} xyz.swapee.rc.IChangellyExchangePageView.GET The navs for _GET_ paths (e.g., to use for redirects).
 * @prop {xyz.swapee.rc.ChangellyExchange.changellyExchangeNav} changellyExchange
 */

/**
 * Contains getters to cast the _IChangellyExchangePageView_ interface.
 * @interface xyz.swapee.rc.IChangellyExchangePageViewCaster
 */
xyz.swapee.rc.IChangellyExchangePageViewCaster = class { }
/**
 * Cast the _IChangellyExchangePageView_ instance into the _BoundIChangellyExchangePageView_ type.
 * @type {!xyz.swapee.rc.BoundIChangellyExchangePageView}
 */
xyz.swapee.rc.IChangellyExchangePageViewCaster.prototype.asIChangellyExchangePageView
/**
 * Cast the _IChangellyExchangePageView_ instance into the _BoundIChangellyExchangePage_ type.
 * @type {!xyz.swapee.rc.BoundIChangellyExchangePage}
 */
xyz.swapee.rc.IChangellyExchangePageViewCaster.prototype.asIChangellyExchangePage
/**
 * Access the _ChangellyExchangePageView_ prototype.
 * @type {!xyz.swapee.rc.BoundChangellyExchangePageView}
 */
xyz.swapee.rc.IChangellyExchangePageViewCaster.prototype.superChangellyExchangePageView

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeViewChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeViewChangellyExchange} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePageView.ViewChangellyExchangeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewChangellyExchange` method from being executed.
 * - `sub` _(value: !IChangellyExchangePageView.ChangellyExchangeView) =&gt; void_ Cancels a call to `viewChangellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeViewChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchange} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `viewChangellyExchange` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `afterReturns` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IChangellyExchangePageView.ChangellyExchangeView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachViewChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachViewChangellyExchange} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePageView.ViewChangellyExchangeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `viewChangellyExchange` method from being executed.
 * - `sub` _(value: !IChangellyExchangePageView.ChangellyExchangeView) =&gt; void_ Cancels a call to `viewChangellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachViewChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchange} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchangeReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchangeReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _viewChangellyExchange_ at the `afterEach` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.ViewChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchangeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeGetChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeGetChangellyExchange} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePageView.GetChangellyExchangeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getChangellyExchange` method from being executed.
 * - `sub` _(value: !IChangellyExchangePageView.ChangellyExchangeView) =&gt; void_ Cancels a call to `getChangellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeGetChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchange} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getChangellyExchange` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `afterReturns` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `sub` _(value: !IChangellyExchangePageView.ChangellyExchangeView) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachGetChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachGetChangellyExchange} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePageView.GetChangellyExchangeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getChangellyExchange` method from being executed.
 * - `sub` _(value: !IChangellyExchangePageView.ChangellyExchangeView) =&gt; void_ Cancels a call to `getChangellyExchange` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachGetChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchange} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchange = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchangeReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchangeReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data] Metadata passed to the pointcuts of _getChangellyExchange_ at the `afterEach` joinpoint.
 * - `res` _!IChangellyExchangePageView.ChangellyExchangeView_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `args` _IChangellyExchangePageView.GetChangellyExchangeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchangeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeSetChangellyExchangeCtx */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeSetChangellyExchangeCtx} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePageView.SetChangellyExchangeCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setChangellyExchangeCtx` method from being executed.
 * - `sub` _(value: !IChangellyExchangePage.changellyExchange.OptCtx) =&gt; void_ Cancels a call to `setChangellyExchangeCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeSetChangellyExchangeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtx */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtx} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `setChangellyExchangeCtx` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `afterReturns` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptCtx_ The return of the method after it's successfully run.
 * - `sub` _(value: !IChangellyExchangePage.changellyExchange.OptCtx) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachSetChangellyExchangeCtx */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachSetChangellyExchangeCtx} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePageView.SetChangellyExchangeCtxNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `setChangellyExchangeCtx` method from being executed.
 * - `sub` _(value: !IChangellyExchangePage.changellyExchange.OptCtx) =&gt; void_ Cancels a call to `setChangellyExchangeCtx` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachSetChangellyExchangeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtx */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtx} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `after` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtx = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtxReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtxReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data] Metadata passed to the pointcuts of _setChangellyExchangeCtx_ at the `afterEach` joinpoint.
 * - `res` _!IChangellyExchangePage.changellyExchange.OptCtx_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `args` _IChangellyExchangePageView.SetChangellyExchangeCtxNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtxReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangePartial */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangePartial} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangePartial_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IChangellyExchangePageView.ChangellyExchangePartialNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `ChangellyExchangePartial` method from being executed.
 * - `sub` _(value: engineering.type.VNode) =&gt; void_ Cancels a call to `ChangellyExchangePartial` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `args` _IChangellyExchangePageView.ChangellyExchangePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangePartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartial */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartial} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangePartial_ at the `after` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `args` _IChangellyExchangePageView.ChangellyExchangePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartial = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangePartial_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `ChangellyExchangePartial` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `args` _IChangellyExchangePageView.ChangellyExchangePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangePartial_ at the `afterReturns` joinpoint.
 * - `res` _engineering.type.VNode_ The return of the method after it's successfully run.
 * - `sub` _(value: engineering.type.VNode) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `args` _IChangellyExchangePageView.ChangellyExchangePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangePartial_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `args` _IChangellyExchangePageView.ChangellyExchangePartialNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangeView */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangeView} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `ChangellyExchangeView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeView */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeView} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewThrows */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `ChangellyExchangeView` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewCancels */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEach_ChangellyExchangeView */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEach_ChangellyExchangeView} */
/**
 * Before each part of the combined method executes. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `ChangellyExchangeView` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEach_ChangellyExchangeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeView */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeView} */
/**
 * After each of the parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeView = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData) => void} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel>} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeViewReturns */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeViewReturns} */
/**
 * After each of the returning parts of the combined method. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data] Metadata passed to the pointcuts of _ChangellyExchangeView_ at the `afterEach` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData*
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeViewReturns = function(data) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, form: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form) => !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageView>} xyz.swapee.rc.IChangellyExchangePageView._viewChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageView.viewChangellyExchange} */
/**
 * Assigns required view data to the context, then redirects, or invokes another pagelet. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx The context.
 * - `validation` _!IChangellyExchangePage.changellyExchange.Validation_ The validation.
 * - `errors` _!IChangellyExchangePage.changellyExchange.Errors_ The errors.
 * - `answers` _!IChangellyExchangePage.changellyExchange.Answers_ The answers.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form The form.
 * - `locale` _string_
 * @return {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView}
 */
xyz.swapee.rc.IChangellyExchangePageView.viewChangellyExchange = function(ctx, form) {}

/**
 * @typedef {(this: THIS, ctx: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, form: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form) => (void|!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView)} xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange<!xyz.swapee.rc.IChangellyExchangePageView>} xyz.swapee.rc.IChangellyExchangePageView._getChangellyExchange */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageView.getChangellyExchange} */
/**
 * The internal view handler for the `changellyExchange` action. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx The context.
 * - `validation` _!IChangellyExchangePage.changellyExchange.Validation_ The validation.
 * - `errors` _!IChangellyExchangePage.changellyExchange.Errors_ The errors.
 * - `answers` _!IChangellyExchangePage.changellyExchange.Answers_ The answers.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form The form.
 * - `locale` _string_
 * @return {void|!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView}
 */
xyz.swapee.rc.IChangellyExchangePageView.getChangellyExchange = function(ctx, form) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, translation: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation) => !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx<!xyz.swapee.rc.IChangellyExchangePageView>} xyz.swapee.rc.IChangellyExchangePageView._setChangellyExchangeCtx */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageView.setChangellyExchangeCtx} */
/**
 * Assigns the context based on answers and translations. `🔗 $combine`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers The answers.
 * - `locale` _string_
 * - `fixedId` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `estimatedFixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `minAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `maxAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `estimatedFloatAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `networkFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `partnerFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `visibleAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `id` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createTransactionError` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createdAt` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinAddress` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinExtraId` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `status` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers* ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * - `kycRequired` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `expectedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `confirmedAmountFrom` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixed` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `getOffer` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `checkPaymentError` _&#42;_ ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation The translation for the user's locale.
 * @return {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx}
 */
xyz.swapee.rc.IChangellyExchangePageView.setChangellyExchangeCtx = function(answers, translation) {}

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, errors: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors, actions: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions, translation: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangePartial
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangePartial<!xyz.swapee.rc.IChangellyExchangePageView>} xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangePartial */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial} */
/**
 * The partial.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers The answers.
 * - `locale` _string_
 * - `fixedId` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `estimatedFixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `minAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `maxAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `estimatedFloatAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `networkFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `partnerFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `visibleAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `id` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createTransactionError` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createdAt` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinAddress` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinExtraId` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `status` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers* ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * - `kycRequired` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `expectedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `confirmedAmountFrom` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixed` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `getOffer` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `checkPaymentError` _&#42;_ ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions} actions The actions.
 * - `changellyExchange` _ChangellyExchange.changellyExchangeNav_
 * - `_changellyExchange` _!IChangellyExchangePage.changellyExchange.Form_
 * - `_filterGetFixedOffer` _IChangellyExchangePage.filterGetFixedOffer.Form_ The form of the `filterGetFixedOffer` action.
 * - `filterGetFixedOffer` _!ChangellyExchange.filterGetFixedOfferNav_
 * - `_filterGetOffer` _IChangellyExchangePage.filterGetOffer.Form_ The form of the `filterGetOffer` action.
 * - `filterGetOffer` _!ChangellyExchange.filterGetOfferNav_
 * - `_filterCreateTransaction` _IChangellyExchangePage.filterCreateTransaction.Form_ The form of the `filterCreateTransaction` action.
 * - `filterCreateTransaction` _!ChangellyExchange.filterCreateTransactionNav_
 * - `_filterGetTransaction` _IChangellyExchangePage.filterGetTransaction.Form_ The form of the `filterGetTransaction` action.
 * - `filterGetTransaction` _!ChangellyExchange.filterGetTransactionNav_
 * - `_filterCreateFixedTransaction` _IChangellyExchangePage.filterCreateFixedTransaction.Form_ The form of the `filterCreateFixedTransaction` action.
 * - `filterCreateFixedTransaction` _!ChangellyExchange.filterCreateFixedTransactionNav_
 * - `_filterCheckPayment` _IChangellyExchangePage.filterCheckPayment.Form_ The form of the `filterCheckPayment` action.
 * - `filterCheckPayment` _!ChangellyExchange.filterCheckPaymentNav_
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial = function(answers, errors, actions, translation) {}

/** @typedef {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors&xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors The errors. */

/**
 * The actions.
 * @record xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions = class { }
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.changellyExchange = /** @type {xyz.swapee.rc.ChangellyExchange.changellyExchangeNav} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._changellyExchange = /** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} */ (void 0)
/**
 * The form of the `filterGetFixedOffer` action.
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterGetFixedOffer = /** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterGetFixedOffer = /** @type {!xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav} */ (void 0)
/**
 * The form of the `filterGetOffer` action.
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterGetOffer = /** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterGetOffer = /** @type {!xyz.swapee.rc.ChangellyExchange.filterGetOfferNav} */ (void 0)
/**
 * The form of the `filterCreateTransaction` action.
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterCreateTransaction = /** @type {xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterCreateTransaction = /** @type {!xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav} */ (void 0)
/**
 * The form of the `filterGetTransaction` action.
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterGetTransaction = /** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterGetTransaction = /** @type {!xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav} */ (void 0)
/**
 * The form of the `filterCreateFixedTransaction` action.
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterCreateFixedTransaction = /** @type {xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterCreateFixedTransaction = /** @type {!xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav} */ (void 0)
/**
 * The form of the `filterCheckPayment` action.
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterCheckPayment = /** @type {xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form} */ (void 0)
/**
 *
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterCheckPayment = /** @type {!xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav} */ (void 0)

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation The translation for the chosen user locale. */

/**
 * @typedef {(this: THIS, answers: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, errors: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors, actions: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions, translation: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation) => engineering.type.VNode} xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView
 * @template THIS
 */
/** @typedef {xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView<!xyz.swapee.rc.IChangellyExchangePageView>} xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangeView */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
/**
 * The _ChangellyExchange_ view with partials and translation mechanics that has
 * access to answers written by the controller. `🔗 $combine` `📲 $returnFirst`
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers The answers.
 * - `locale` _string_
 * - `fixedId` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `estimatedFixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers*
 * - `minAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `maxAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetFixedOffer.Answers* ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `estimatedFloatAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `networkFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `partnerFee` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers* ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `visibleAmount` _&#42;_ ⤴ *IChangellyExchangePage.filterGetOffer.Answers*
 * - `id` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createTransactionError` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `createdAt` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinAddress` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `payinExtraId` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `status` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers* ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * - `kycRequired` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `expectedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `confirmedAmountFrom` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateTransaction.Answers* ⤴ *IChangellyExchangePage.filterGetTransaction.Answers* ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixed` _&#42;_ ⤴ *IChangellyExchangePage.filterGetTransaction.Answers*
 * - `getOffer` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `fixedAmountTo` _&#42;_ ⤴ *IChangellyExchangePage.filterCreateFixedTransaction.Answers*
 * - `checkPaymentError` _&#42;_ ⤴ *IChangellyExchangePage.filterCheckPayment.Answers*
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors} errors The errors.
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions} actions The actions.
 * - `changellyExchange` _ChangellyExchange.changellyExchangeNav_
 * - `_changellyExchange` _!IChangellyExchangePage.changellyExchange.Form_
 * - `_filterGetFixedOffer` _IChangellyExchangePage.filterGetFixedOffer.Form_ The form of the `filterGetFixedOffer` action.
 * - `filterGetFixedOffer` _!ChangellyExchange.filterGetFixedOfferNav_
 * - `_filterGetOffer` _IChangellyExchangePage.filterGetOffer.Form_ The form of the `filterGetOffer` action.
 * - `filterGetOffer` _!ChangellyExchange.filterGetOfferNav_
 * - `_filterCreateTransaction` _IChangellyExchangePage.filterCreateTransaction.Form_ The form of the `filterCreateTransaction` action.
 * - `filterCreateTransaction` _!ChangellyExchange.filterCreateTransactionNav_
 * - `_filterGetTransaction` _IChangellyExchangePage.filterGetTransaction.Form_ The form of the `filterGetTransaction` action.
 * - `filterGetTransaction` _!ChangellyExchange.filterGetTransactionNav_
 * - `_filterCreateFixedTransaction` _IChangellyExchangePage.filterCreateFixedTransaction.Form_ The form of the `filterCreateFixedTransaction` action.
 * - `filterCreateFixedTransaction` _!ChangellyExchange.filterCreateFixedTransactionNav_
 * - `_filterCheckPayment` _IChangellyExchangePage.filterCheckPayment.Form_ The form of the `filterCheckPayment` action.
 * - `filterCheckPayment` _!ChangellyExchange.filterCheckPaymentNav_
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation The translation for the chosen user locale.
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc.IChangellyExchangePageView
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml}  2b436b21c8628099c0dbc3690c5442fd */
/**
 * The `changellyExchange` navigation metadata.
 * @constructor xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav
 */
xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav.prototype.changellyExchange = /** @type {number} */ (void 0)

/**
 * The `filterGetFixedOffer` navigation metadata.
 * @constructor xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav
 */
xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav.prototype.filterGetFixedOffer = /** @type {number} */ (void 0)

/**
 * The `filterGetOffer` navigation metadata.
 * @constructor xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav
 */
xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav.prototype.filterGetOffer = /** @type {number} */ (void 0)

/**
 * The `filterCreateTransaction` navigation metadata.
 * @constructor xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav
 */
xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav.prototype.filterCreateTransaction = /** @type {number} */ (void 0)

/**
 * The `filterGetTransaction` navigation metadata.
 * @constructor xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav
 */
xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav.prototype.filterGetTransaction = /** @type {number} */ (void 0)

/**
 * The `filterCreateFixedTransaction` navigation metadata.
 * @constructor xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav
 */
xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav.prototype.filterCreateFixedTransaction = /** @type {number} */ (void 0)

/**
 * The `filterCheckPayment` navigation metadata.
 * @constructor xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav
 */
xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav = class { }
/**
 * The id of the method.
 */
xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav.prototype.filterCheckPayment = /** @type {number} */ (void 0)

/**
 * The ids of the methods.
 * @constructor xyz.swapee.rc.changellyExchangeMethodsIds
 */
xyz.swapee.rc.changellyExchangeMethodsIds = class { }
xyz.swapee.rc.changellyExchangeMethodsIds.prototype.constructor = xyz.swapee.rc.changellyExchangeMethodsIds

/**
 * The ids of the arcs.
 * @constructor xyz.swapee.rc.changellyExchangeArcsIds
 */
xyz.swapee.rc.changellyExchangeArcsIds = class { }
xyz.swapee.rc.changellyExchangeArcsIds.prototype.constructor = xyz.swapee.rc.changellyExchangeArcsIds

/** @typedef {typeof __$te_plain} xyz.swapee.rc.IChangellyExchangeImpl.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.rc.ChangellyExchangeImpl)} xyz.swapee.rc.AbstractChangellyExchangeImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.ChangellyExchangeImpl} xyz.swapee.rc.ChangellyExchangeImpl.typeof */
/**
 * An abstract class of `xyz.swapee.rc.IChangellyExchangeImpl` interface.
 * @constructor xyz.swapee.rc.AbstractChangellyExchangeImpl
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl = class extends /** @type {xyz.swapee.rc.AbstractChangellyExchangeImpl.constructor&xyz.swapee.rc.ChangellyExchangeImpl.typeof} */ (class {}) { }
xyz.swapee.rc.AbstractChangellyExchangeImpl.prototype.constructor = xyz.swapee.rc.AbstractChangellyExchangeImpl
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.class = /** @type {typeof xyz.swapee.rc.AbstractChangellyExchangeImpl} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.rc.IChangellyExchangeImpl|typeof xyz.swapee.rc.ChangellyExchangeImpl)|(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.rc.IChangellyExchangeImpl|typeof xyz.swapee.rc.ChangellyExchangeImpl)|(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.rc.IChangellyExchangeImpl|typeof xyz.swapee.rc.ChangellyExchangeImpl)|(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.rc.IChangellyExchangePageAspects&xyz.swapee.rc.IChangellyExchangePage&xyz.swapee.rc.IChangellyExchangePageHyperslice&xyz.swapee.rc.IChangellyExchangePageViewHyperslice)} xyz.swapee.rc.IChangellyExchangeImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageHyperslice} xyz.swapee.rc.IChangellyExchangePageHyperslice.typeof */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangePageViewHyperslice} xyz.swapee.rc.IChangellyExchangePageViewHyperslice.typeof */
/** @interface xyz.swapee.rc.IChangellyExchangeImpl */
xyz.swapee.rc.IChangellyExchangeImpl = class extends /** @type {xyz.swapee.rc.IChangellyExchangeImpl.constructor&engineering.type.IEngineer.typeof&xyz.swapee.rc.IChangellyExchangePageAspects.typeof&xyz.swapee.rc.IChangellyExchangePage.typeof&xyz.swapee.rc.IChangellyExchangePageHyperslice.typeof&xyz.swapee.rc.IChangellyExchangePageViewHyperslice.typeof} */ (class {}) {
}
/**
 * Create a new *IChangellyExchangeImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangeImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.IChangellyExchangeImpl.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangeImpl&engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangeImpl.Initialese>)} xyz.swapee.rc.ChangellyExchangeImpl.constructor */
/** @typedef {typeof xyz.swapee.rc.IChangellyExchangeImpl} xyz.swapee.rc.IChangellyExchangeImpl.typeof */
/**
 * A concrete class of _IChangellyExchangeImpl_ instances.
 * @constructor xyz.swapee.rc.ChangellyExchangeImpl
 * @implements {xyz.swapee.rc.IChangellyExchangeImpl} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangeImpl.Initialese>} ‎
 */
xyz.swapee.rc.ChangellyExchangeImpl = class extends /** @type {xyz.swapee.rc.ChangellyExchangeImpl.constructor&xyz.swapee.rc.IChangellyExchangeImpl.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangeImpl* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.rc.IChangellyExchangeImpl.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangeImpl* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.rc.IChangellyExchangeImpl.Initialese} init The initialisation options.
 */
xyz.swapee.rc.ChangellyExchangeImpl.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.ChangellyExchangeImpl.__extend = function(...Extensions) {}

/** @typedef {typeof xyz.swapee.rc.ChangellyExchange.changellyExchangeNav} */
/**
 * Navigates to `changellyExchange`.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form
 * - `locale` _string_
 * @return {void}
 */
xyz.swapee.rc.ChangellyExchange.changellyExchangeNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav} */
/**
 * Invokes the `filterGetFixedOffer` action.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form} form
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `ready` _&#42;_
 * - `getOffer` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.ChangellyExchange.filterGetOfferNav} */
/**
 * Invokes the `filterGetOffer` action.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form} form
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `ready` _&#42;_
 * - `getOffer` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.ChangellyExchange.filterGetOfferNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav} */
/**
 * Invokes the `filterCreateTransaction` action.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form} form
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `address` _&#42;_
 * - `refundAddress` _&#42;_
 * - `createTransaction` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav} */
/**
 * Invokes the `filterGetTransaction` action.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form} form
 * - `id` _&#42;_
 * - `getTransaction` _&#42;_
 * - `createdAt` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav} */
/**
 * Invokes the `filterCreateFixedTransaction` action.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form} form
 * - `amountFrom` _&#42;_
 * - `currencyFrom` _&#42;_
 * - `currencyTo` _&#42;_
 * - `fixed` _&#42;_
 * - `address` _&#42;_
 * - `refundAddress` _&#42;_
 * - `createTransaction` _&#42;_
 * - `fixedId` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav} */
/**
 * Invokes the `filterCheckPayment` action.
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form} form
 * - `id` _&#42;_
 * - `checkPayment` _&#42;_
 * @return {void}
 */
xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav = function(form) {}

/** @typedef {typeof xyz.swapee.rc.getChangellyExchange} */
/**
 * Allows to render the pagelet after it's been processed.
 * @return {void}
 */
xyz.swapee.rc.getChangellyExchange = function() {}

/** @typedef {typeof xyz.swapee.rc.changellyExchange} */
/**
 * @param {!xyz.swapee.rc.IChangellyExchangeImpl} implementation
 * @return {!xyz.swapee.rc.IChangellyExchangePage}
 */
xyz.swapee.rc.changellyExchange = function(implementation) {}

// nss:xyz.swapee.rc,xyz.swapee.rc.ChangellyExchange
/* @typal-end */