import ChangellyExchangePageAspectsInstaller from '../../aspects-installers/ChangellyExchangePageAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperChangellyExchangePage}
 */
function __AbstractHyperChangellyExchangePage() {}
__AbstractHyperChangellyExchangePage.prototype = /** @type {!_AbstractHyperChangellyExchangePage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 */
class _AbstractHyperChangellyExchangePage { }
/** @extends {xyz.swapee.rc.AbstractHyperChangellyExchangePage} ‎ */
class AbstractHyperChangellyExchangePage extends newAbstract(
 _AbstractHyperChangellyExchangePage,'IHyperChangellyExchangePage',null,{
  asIHyperChangellyExchangePage:ChangellyExchangePageAspectsInstaller,
  superHyperChangellyExchangePage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage} */
AbstractHyperChangellyExchangePage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage} */
function AbstractHyperChangellyExchangePageClass(){}

export default AbstractHyperChangellyExchangePage


AbstractHyperChangellyExchangePage[$implementations]=[
 __AbstractHyperChangellyExchangePage,
]