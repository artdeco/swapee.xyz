import ChangellyExchangePageMethodsIds from '../../../gen/methods-ids'
import '../../../types'

const methodsIds=Object.values(ChangellyExchangePageMethodsIds)

/**@type {xyz.swapee.rc.IChangellyExchangePageView._getChangellyExchange} */
export default function _getChangellyExchange(ctx,form) {
 const actionId=parseFloat(form['action'])
 if(methodsIds.includes(actionId)) {
  const{asChangellyExchangePageView:{viewChangellyExchange:viewChangellyExchange}}=this
  const ev=viewChangellyExchange(ctx,form)
  return ev
 }
}