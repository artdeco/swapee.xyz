import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav} */
export const FilterCreateTransactionNav={
 toString(){return'filterCreateTransaction'},
 filterCreateTransaction:methodsIds.filterCreateTransaction,
}
export default FilterCreateTransactionNav