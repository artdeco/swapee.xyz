import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.ChangellyExchange.FilterCreateChangeNowTransactionNav} */
export const FilterCreateChangeNowTransactionNav={
 toString(){return'filterCreateChangeNowTransaction'},
 filterCreateChangeNowTransaction:methodsIds.filterCreateChangeNowTransaction,
}
export default FilterCreateChangeNowTransactionNav