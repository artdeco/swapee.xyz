import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav} */
export const FilterGetTransactionNav={
 toString(){return'filterGetTransaction'},
 filterGetTransaction:methodsIds.filterGetTransaction,
}
export default FilterGetTransactionNav