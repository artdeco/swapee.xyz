import ChangellyExchangePageUniversal, {setChangellyExchangePageUniversalSymbols} from './anchors/ChangellyExchangePageUniversal'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangePage}
 */
function __AbstractChangellyExchangePage() {}
__AbstractChangellyExchangePage.prototype = /** @type {!_AbstractChangellyExchangePage} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePage}
 */
class _AbstractChangellyExchangePage { }
/** @extends {xyz.swapee.rc.AbstractChangellyExchangePage} ‎ */
class AbstractChangellyExchangePage extends newAbstract(
 _AbstractChangellyExchangePage,'IChangellyExchangePage',null,{
  asIChangellyExchangePage:1,
  superChangellyExchangePage:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePage} */
AbstractChangellyExchangePage.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePage} */
function AbstractChangellyExchangePageClass(){}

export default AbstractChangellyExchangePage


AbstractChangellyExchangePage[$implementations]=[
 __AbstractChangellyExchangePage,
 ChangellyExchangePageUniversal,
 AbstractChangellyExchangePageClass.prototype=/**@type {!xyz.swapee.rc.IChangellyExchangePage}*/({
  changellyExchange:precombined,
  filterGetFixedOffer:precombined,
  filterGetOffer:precombined,
  filterCreateTransaction:precombined,
  filterGetTransaction:precombined,
  filterCreateFixedTransaction:precombined,
  filterCheckPayment:precombined,
 }),
]