import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav} */
export const FilterGetOfferNav={
 toString(){return'filterGetOffer'},
 filterGetOffer:methodsIds.filterGetOffer,
}
export default FilterGetOfferNav