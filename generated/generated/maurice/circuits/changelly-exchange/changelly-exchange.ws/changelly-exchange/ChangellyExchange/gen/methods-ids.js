export const ChangellyExchangePageMethodsIds={
 changellyExchange:2452156,
 filterGetFixedOffer:'22dec',
 filterGetOffer:'d8a4f',
 filterCreateTransaction:'321e2',
 filterGetTransaction:'6a2c5',
 filterCreateFixedTransaction:'1344b',
 filterCheckPayment:'c3eae',
}
export const ChangellyExchangePageLiteralMethodsIds={
 'changellyExchange':2452156,
 'filterGetFixedOffer':'22dec',
 'filterGetOffer':'d8a4f',
 'filterCreateTransaction':'321e2',
 'filterGetTransaction':'6a2c5',
 'filterCreateFixedTransaction':'1344b',
 'filterCheckPayment':'c3eae',
}

export default ChangellyExchangePageMethodsIds