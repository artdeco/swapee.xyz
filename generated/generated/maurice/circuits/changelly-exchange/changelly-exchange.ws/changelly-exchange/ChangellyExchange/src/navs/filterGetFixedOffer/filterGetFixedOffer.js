import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav} */
export const FilterGetFixedOfferNav={
 toString(){return'filterGetFixedOffer'},
 filterGetFixedOffer:methodsIds.filterGetFixedOffer,
}
export default FilterGetFixedOfferNav