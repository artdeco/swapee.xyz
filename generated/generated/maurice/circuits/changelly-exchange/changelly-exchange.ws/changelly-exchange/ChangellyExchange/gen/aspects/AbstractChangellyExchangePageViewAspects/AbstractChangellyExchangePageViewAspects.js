import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangePageViewAspects}
 */
function __AbstractChangellyExchangePageViewAspects() {}
__AbstractChangellyExchangePageViewAspects.prototype = /** @type {!_AbstractChangellyExchangePageViewAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageViewAspects}
 */
class _AbstractChangellyExchangePageViewAspects { }
/**
 * The aspects of the *IChangellyExchangePageView*.
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageViewAspects} ‎
 */
class AbstractChangellyExchangePageViewAspects extends newAspects(
 _AbstractChangellyExchangePageViewAspects,'IChangellyExchangePageViewAspects',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspects} */
AbstractChangellyExchangePageViewAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspects} */
function AbstractChangellyExchangePageViewAspectsClass(){}

export default AbstractChangellyExchangePageViewAspects


AbstractChangellyExchangePageViewAspects[$implementations]=[
 __AbstractChangellyExchangePageViewAspects,
]