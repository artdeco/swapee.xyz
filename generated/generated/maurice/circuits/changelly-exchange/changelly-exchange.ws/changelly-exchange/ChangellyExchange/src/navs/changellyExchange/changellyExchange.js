import methodsIds from '../../../gen/methods-ids'
/** @type {xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav} */
export const ChangellyExchangeNav={
 toString(){return'changellyExchange'},
 changellyExchange:methodsIds.changellyExchange,
}
export default ChangellyExchangeNav