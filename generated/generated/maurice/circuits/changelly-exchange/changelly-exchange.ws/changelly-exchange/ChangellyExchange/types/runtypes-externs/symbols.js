/** @const {?} */ xyz.swapee.rc.AbstractChangellyExchangePageUniversal
/** @const {?} */ xyz.swapee.rc.UChangellyExchangePage
/** @const {?} */ $xyz.swapee.rc.UChangellyExchangePage
/** @const {?} */ $xyz.swapee.rc.UChangellyExchangePageView
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePageUniversal.getSymbols filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {!xyz.swapee.rc.UChangellyExchangePage.getSymbols} */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePageUniversal.setSymbols filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {!xyz.swapee.rc.UChangellyExchangePage.setSymbols} */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePageUniversal.Symbols filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {!xyz.swapee.rc.UChangellyExchangePage.Symbols} */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.Symbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePage.SymbolsIn filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
$xyz.swapee.rc.UChangellyExchangePage.SymbolsIn = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage|undefined} */
$xyz.swapee.rc.UChangellyExchangePage.SymbolsIn.prototype._changellyExchangePage
/** @typedef {$xyz.swapee.rc.UChangellyExchangePage.SymbolsIn} */
xyz.swapee.rc.UChangellyExchangePage.SymbolsIn

// nss:xyz.swapee.rc,$xyz.swapee.rc.UChangellyExchangePage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePage.Symbols filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
$xyz.swapee.rc.UChangellyExchangePage.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.rc.UChangellyExchangePage.Symbols.prototype._changellyExchangePage
/** @typedef {$xyz.swapee.rc.UChangellyExchangePage.Symbols} */
xyz.swapee.rc.UChangellyExchangePage.Symbols

// nss:xyz.swapee.rc,$xyz.swapee.rc.UChangellyExchangePage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePage.SymbolsOut filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
$xyz.swapee.rc.UChangellyExchangePage.SymbolsOut = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage} */
$xyz.swapee.rc.UChangellyExchangePage.SymbolsOut.prototype._changellyExchangePage
/** @typedef {$xyz.swapee.rc.UChangellyExchangePage.SymbolsOut} */
xyz.swapee.rc.UChangellyExchangePage.SymbolsOut

// nss:xyz.swapee.rc,$xyz.swapee.rc.UChangellyExchangePage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePage.setSymbols filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {function(!xyz.swapee.rc.UChangellyExchangePage, !xyz.swapee.rc.UChangellyExchangePage.SymbolsIn): void} */
xyz.swapee.rc.UChangellyExchangePage.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePage.getSymbols filter:Symbolism 3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {function(!xyz.swapee.rc.UChangellyExchangePage): !xyz.swapee.rc.UChangellyExchangePage.SymbolsOut} */
xyz.swapee.rc.UChangellyExchangePage.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.getSymbols filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {!xyz.swapee.rc.UChangellyExchangePageView.getSymbols} */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.setSymbols filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {!xyz.swapee.rc.UChangellyExchangePageView.setSymbols} */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.Symbols filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {!xyz.swapee.rc.UChangellyExchangePageView.Symbols} */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.Symbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageView.SymbolsIn filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @record */
$xyz.swapee.rc.UChangellyExchangePageView.SymbolsIn = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView|undefined} */
$xyz.swapee.rc.UChangellyExchangePageView.SymbolsIn.prototype._changellyExchangePageView
/** @typedef {$xyz.swapee.rc.UChangellyExchangePageView.SymbolsIn} */
xyz.swapee.rc.UChangellyExchangePageView.SymbolsIn

// nss:xyz.swapee.rc,$xyz.swapee.rc.UChangellyExchangePageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageView.Symbols filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @record */
$xyz.swapee.rc.UChangellyExchangePageView.Symbols = function() {}
/** @type {symbol} */
$xyz.swapee.rc.UChangellyExchangePageView.Symbols.prototype._changellyExchangePageView
/** @typedef {$xyz.swapee.rc.UChangellyExchangePageView.Symbols} */
xyz.swapee.rc.UChangellyExchangePageView.Symbols

// nss:xyz.swapee.rc,$xyz.swapee.rc.UChangellyExchangePageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageView.SymbolsOut filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @record */
$xyz.swapee.rc.UChangellyExchangePageView.SymbolsOut = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView} */
$xyz.swapee.rc.UChangellyExchangePageView.SymbolsOut.prototype._changellyExchangePageView
/** @typedef {$xyz.swapee.rc.UChangellyExchangePageView.SymbolsOut} */
xyz.swapee.rc.UChangellyExchangePageView.SymbolsOut

// nss:xyz.swapee.rc,$xyz.swapee.rc.UChangellyExchangePageView
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageView.setSymbols filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {function(!xyz.swapee.rc.UChangellyExchangePageView, !xyz.swapee.rc.UChangellyExchangePageView.SymbolsIn): void} */
xyz.swapee.rc.UChangellyExchangePageView.setSymbols

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageView.getSymbols filter:Symbolism 99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {function(!xyz.swapee.rc.UChangellyExchangePageView): !xyz.swapee.rc.UChangellyExchangePageView.SymbolsOut} */
xyz.swapee.rc.UChangellyExchangePageView.getSymbols

// nss:xyz.swapee.rc
/* @typal-end */