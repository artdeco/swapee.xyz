import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangePageAspects}
 */
function __AbstractChangellyExchangePageAspects() {}
__AbstractChangellyExchangePageAspects.prototype = /** @type {!_AbstractChangellyExchangePageAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageAspects}
 */
class _AbstractChangellyExchangePageAspects { }
/**
 * The aspects of the *IChangellyExchangePage*.
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageAspects} ‎
 */
class AbstractChangellyExchangePageAspects extends newAspects(
 _AbstractChangellyExchangePageAspects,'IChangellyExchangePageAspects',null,{},false) {}

/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspects} */
AbstractChangellyExchangePageAspects.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspects} */
function AbstractChangellyExchangePageAspectsClass(){}

export default AbstractChangellyExchangePageAspects


AbstractChangellyExchangePageAspects[$implementations]=[
 __AbstractChangellyExchangePageAspects,
]