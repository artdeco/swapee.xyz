require('./types')
module.exports={
 toString(){return 'changellyExchange'},
 get ChangellyExchangePage(){
  const d=Date.now();
  const R=require('./src/HyperChangellyExchangePage');
  console.log("✅ Loaded dev %s in %s",'ChangellyExchange',Date.now()-d,'ms');
  return R
 },
 get changellyExchangeMethodsIds() {
  const methodsIds={}
  const o=require('./gen/methods-ids')
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') methodsIds[key]=val
  }
  return methodsIds
 },
 get changellyExchangeArcsIds() {
  const arcsIds={}
  let o={}
  try{o=require('./gen/arcs-ids')}catch(err){}
  for(const key in o){
   const val=o[key]
   if(typeof val=='number') arcsIds[val]=key
  }
  return arcsIds
 },
 get getChangellyExchange(){
  const GET=require('./src/api/getChangellyExchange/getChangellyExchange')
  return GET
 },
 get ChangellyExchangeNav() {
  return require('./src/navs/changellyExchange')
 },
 get FilterGetFixedOfferNav() {
  return require('./src/navs/filterGetFixedOffer')
 },
 get FilterGetOfferNav() {
  return require('./src/navs/filterGetOffer')
 },
 get FilterCreateTransactionNav() {
  return require('./src/navs/filterCreateTransaction')
 },
 get FilterGetTransactionNav() {
  return require('./src/navs/filterGetTransaction')
 },
 get FilterCreateFixedTransactionNav() {
  return require('./src/navs/filterCreateFixedTransaction')
 },
 get FilterCheckPaymentNav() {
  return require('./src/navs/filterCheckPayment')
 },
}