import '../../types'
import {ChangellyUniversal} from '@type.community/changelly.com'
import {ChangeNowUniversal} from '@swapee.xyz/swapee.xyz'
import {LetsExchangeUniversal} from '@swapee.xyz/swapee.xyz'
import filterCheckPayment from '../../../../../../../../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterCheckPayment/filter-check-payment'
import filterGetFixedOffer from '../../../../../../../../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterGetFixedOffer/filter-get-fixed-offer'
import filterGetOffer from '../../../../../../../../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterGetOffer/filter-get-offer'
import filterCreateTransaction from '../../../../../../../../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterCreateTransaction/filter-create-transaction'
import filterGetTransaction from '../../../../../../../../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterGetTransaction/filter-get-transaction'
import filterCreateFixedTransaction from '../../../../../../../../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterCreateFixedTransaction/filter-create-fixed-transaction'
import {ser} from '@type.engineering/type-engineer'
import AbstractHyperChangellyExchangePage from '../../gen/hyper/AbstractHyperChangellyExchangePage'
import HyperChangellyExchangePageView from '../HyperChangellyExchangePageView'
import {AssignAnswers} from '@websystems/parthenon'
import AbstractChangellyExchangePage from '../../gen/AbstractChangellyExchangePage'
import AbstractChangellyExchangePageAspects from '../../gen/aspects/AbstractChangellyExchangePageAspects'
import ChangellyExchangePageMethodsIds from '../../gen/methods-ids'

/** @extends {xyz.swapee.rc.HyperChangellyExchangePage}  */
export default class HyperChangellyExchangePage extends AbstractHyperChangellyExchangePage.consults(
 HyperChangellyExchangePageView,
 AbstractChangellyExchangePageAspects.class.prototype=/**@type {!xyz.swapee.rc.ChangellyExchangePageAspects} */({
  afterEachFilterGetFixedOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterGetOfferReturns:[
   AssignAnswers,
  ],
  afterEachFilterCreateTransactionReturns:[
   AssignAnswers,
  ],
  afterEachFilterGetTransactionReturns:[
   AssignAnswers,
  ],
  afterEachFilterCreateFixedTransactionReturns:[
   AssignAnswers,
  ],
  afterEachFilterCheckPaymentReturns:[
   AssignAnswers,
  ],
  afterEachChangellyExchangeReturns:[
   AssignAnswers,
  ],
 }),
 // todo: add HyperView here
 AbstractChangellyExchangePageAspects.class.prototype=/**@type {!xyz.swapee.rc.ChangellyExchangePageAspects} */({ 
  afterFilterCheckPayment:[
   ServeData,
  ],
  afterFilterCheckPaymentThrows:[
   ServeError,
  ],
  afterFilterGetFixedOffer:[
   ServeData,
  ],
  afterFilterGetFixedOfferThrows:[
   ServeError,
  ],
  afterFilterGetOffer:[
   ServeData,
  ],
  afterFilterGetOfferThrows:[
   ServeError,
  ],
  afterFilterCreateTransaction:[
   ServeData,
  ],
  afterFilterCreateTransactionThrows:[
   ServeError,
  ],
  afterFilterGetTransaction:[
   ServeData,
  ],
  afterFilterGetTransactionThrows:[
   ServeError,
  ],
  afterFilterCreateFixedTransaction:[
   ServeData,
  ],
  afterFilterCreateFixedTransactionThrows:[
   ServeError,
  ],
 }),
).implements(
 AbstractChangellyExchangePage,
 ChangellyUniversal,
 ChangeNowUniversal,
 LetsExchangeUniversal,
 AbstractHyperChangellyExchangePage.class.prototype=/**@type {!xyz.swapee.rc.ChangellyExchangePageHyperslice} */({
  get _methodIds(){return ChangellyExchangePageMethodsIds},
 }),
 AbstractHyperChangellyExchangePage.class.prototype=/**@type {!xyz.swapee.rc.ChangellyExchangePageHyperslice} */({
  filterCheckPayment:[
   filterCheckPayment,
  ],
  filterGetFixedOffer:[
   filterGetFixedOffer,
  ],
  filterGetOffer:[
   filterGetOffer,
  ],
  filterCreateTransaction:[
   filterCreateTransaction,
  ],
  filterGetTransaction:[
   filterGetTransaction,
  ],
  filterCreateFixedTransaction:[
   filterCreateFixedTransaction,
  ],
 }),
){}

function ServeError({hide:hide,err:err,args:{ctx:ctx}}){
 if(err.message.startsWith('!')) {
  hide(err)
  ctx.body={error:err.message.replace('!','')}
 }
}
function ServeData({args:{ctx:ctx,answers:answers}}){
 const data=ser(answers,PQs)
 ctx.body={data:data}
}

const PQs={
 rate:'67942',
 fixedId:'2568a',
 estimatedFixedAmountTo:'ea0a0',
 minAmount:'ae0d3',
 maxAmount:'d0b0c',
 estimatedFloatAmountTo:'8ca05',
 networkFee:'5fd9c',
 partnerFee:'96f44',
 visibleAmount:'4685c',
 id:'b80bb',
 createTransactionError:'601f8',
 createdAt:'97def',
 payinAddress:'59af8',
 payinExtraId:'7a19b',
 status:'9acb4',
 kycRequired:'2b803',
 confirmedAmountFrom:'6103f',
 fixed:'cec31',
 currencyFrom:'96c88',
 currencyTo:'c23cd',
 amountFrom:'748e6',
 amountTo:'9dc9f',
 notFound:'5ccf4',
 getOffer:'b370a',
 checkPaymentError:'afbad',
}