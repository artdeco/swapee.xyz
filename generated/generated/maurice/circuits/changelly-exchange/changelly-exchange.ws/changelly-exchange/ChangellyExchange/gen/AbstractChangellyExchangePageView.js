import ChangellyExchangePageViewUniversal, {setChangellyExchangePageViewUniversalSymbols} from './anchors/ChangellyExchangePageViewUniversal'
import { newAbstract, $implementations, prereturnFirst, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangePageView}
 */
function __AbstractChangellyExchangePageView() {}
__AbstractChangellyExchangePageView.prototype = /** @type {!_AbstractChangellyExchangePageView} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.rc.AbstractChangellyExchangePageView}
 */
class _AbstractChangellyExchangePageView { }
/** @extends {xyz.swapee.rc.AbstractChangellyExchangePageView} ‎ */
class AbstractChangellyExchangePageView extends newAbstract(
 _AbstractChangellyExchangePageView,'IChangellyExchangePageView',null,{
  asIChangellyExchangePageView:1,
  superChangellyExchangePageView:2,
 },false) {}

/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageView} */
AbstractChangellyExchangePageView.class=function(){}
/** @type {typeof xyz.swapee.rc.AbstractChangellyExchangePageView} */
function AbstractChangellyExchangePageViewClass(){}

export default AbstractChangellyExchangePageView


AbstractChangellyExchangePageView[$implementations]=[
 __AbstractChangellyExchangePageView,
 ChangellyExchangePageViewUniversal,
 AbstractChangellyExchangePageViewClass.prototype=/**@type {!xyz.swapee.rc.IChangellyExchangePageView}*/({
  viewChangellyExchange:prereturnFirst,
  getChangellyExchange:precombined,
  setChangellyExchangeCtx:precombined,
  ChangellyExchangeView:prereturnFirst,
 }),
]