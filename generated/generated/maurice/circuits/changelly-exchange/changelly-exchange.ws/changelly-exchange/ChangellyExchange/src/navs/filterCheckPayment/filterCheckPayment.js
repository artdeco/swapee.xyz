import methodsIds from '../../../gen/methods-ids'
/**@type {xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav} */
export const FilterCheckPaymentNav={
 toString(){return'filterCheckPayment'},
 filterCheckPayment:methodsIds.filterCheckPayment,
}
export default FilterCheckPaymentNav