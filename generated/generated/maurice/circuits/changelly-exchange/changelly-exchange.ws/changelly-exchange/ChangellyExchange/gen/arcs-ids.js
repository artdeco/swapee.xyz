const ChangellyExchangePageArcsIds={
 locale:5751973,
 amountFrom:8907040,
 currencyFrom:9972150,
 currencyTo:3489396,
 fixed:3246624,
 ready:2025881,
 getOffer:7596502,
 address:8576095,
 refundAddress:1501073,
 createTransaction:7883748,
 tid:9954861,
 fixedId:2718515,
 id:2363075,
 checkPayment:2213040,
}
export const ChangellyExchangePageLiteralArcsIds={
 'locale':5751973,
 'amountFrom':8907040,
 'currencyFrom':9972150,
 'currencyTo':3489396,
 'fixed':3246624,
 'ready':2025881,
 'getOffer':7596502,
 'address':8576095,
 'refundAddress':1501073,
 'createTransaction':7883748,
 'tid':9954861,
 'fixedId':2718515,
 'id':2363075,
 'checkPayment':2213040,
}
export const ReverseChangellyExchangePageArcsIds=Object.keys(ChangellyExchangePageArcsIds)
 .reduce((a,k)=>{a[ChangellyExchangePageArcsIds[`${k}`]]=k;return a},{})

export default ChangellyExchangePageArcsIds