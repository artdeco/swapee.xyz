/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice': {
  'id': 14121551481,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice': {
  'id': 14121551482,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageJoinpointModel': {
  'id': 14121551483,
  'symbols': {},
  'methods': {
   'beforeChangellyExchange': 1,
   'afterChangellyExchange': 2,
   'afterChangellyExchangeThrows': 3,
   'afterChangellyExchangeReturns': 4,
   'afterChangellyExchangeCancels': 5,
   'beforeEachChangellyExchange': 6,
   'afterEachChangellyExchange': 7,
   'afterEachChangellyExchangeReturns': 8,
   'beforeFilterCreateTransaction': 9,
   'afterFilterCreateTransaction': 10,
   'afterFilterCreateTransactionThrows': 11,
   'afterFilterCreateTransactionReturns': 12,
   'afterFilterCreateTransactionCancels': 13,
   'beforeEachFilterCreateTransaction': 14,
   'afterEachFilterCreateTransaction': 15,
   'afterEachFilterCreateTransactionReturns': 16,
   'beforeFilterCheckPayment': 25,
   'afterFilterCheckPayment': 26,
   'afterFilterCheckPaymentThrows': 27,
   'afterFilterCheckPaymentReturns': 28,
   'afterFilterCheckPaymentCancels': 29,
   'beforeEachFilterCheckPayment': 30,
   'afterEachFilterCheckPayment': 31,
   'afterEachFilterCheckPaymentReturns': 32,
   'beforeFilterGetFixedOffer': 33,
   'afterFilterGetFixedOffer': 34,
   'afterFilterGetFixedOfferThrows': 35,
   'afterFilterGetFixedOfferReturns': 36,
   'afterFilterGetFixedOfferCancels': 37,
   'beforeEachFilterGetFixedOffer': 38,
   'afterEachFilterGetFixedOffer': 39,
   'afterEachFilterGetFixedOfferReturns': 40,
   'beforeFilterGetOffer': 41,
   'afterFilterGetOffer': 42,
   'afterFilterGetOfferThrows': 43,
   'afterFilterGetOfferReturns': 44,
   'afterFilterGetOfferCancels': 45,
   'beforeEachFilterGetOffer': 46,
   'afterEachFilterGetOffer': 47,
   'afterEachFilterGetOfferReturns': 48,
   'beforeFilterCreateFixedTransaction': 49,
   'afterFilterCreateFixedTransaction': 50,
   'afterFilterCreateFixedTransactionThrows': 51,
   'afterFilterCreateFixedTransactionReturns': 52,
   'afterFilterCreateFixedTransactionCancels': 53,
   'beforeEachFilterCreateFixedTransaction': 54,
   'afterEachFilterCreateFixedTransaction': 55,
   'afterEachFilterCreateFixedTransactionReturns': 56,
   'beforeFilterGetTransaction': 57,
   'afterFilterGetTransaction': 58,
   'afterFilterGetTransactionThrows': 59,
   'afterFilterGetTransactionReturns': 60,
   'afterFilterGetTransactionCancels': 61,
   'beforeEachFilterGetTransaction': 62,
   'afterEachFilterGetTransaction': 63,
   'afterEachFilterGetTransactionReturns': 64
  }
 },
 'xyz.swapee.rc.IChangellyExchangePageAspectsInstaller': {
  'id': 14121551484,
  'symbols': {},
  'methods': {
   'changellyExchange': 1,
   'filterCreateTransaction': 2,
   'filterCheckPayment': 4,
   'filterGetFixedOffer': 5,
   'filterGetOffer': 6,
   'filterCreateFixedTransaction': 7,
   'filterGetTransaction': 8
  }
 },
 'xyz.swapee.rc.UChangellyExchangePage': {
  'id': 14121551485,
  'symbols': {
   'changellyExchangePage': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BChangellyExchangePageAspects': {
  'id': 14121551486,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IChangellyExchangePageAspects': {
  'id': 14121551487,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperChangellyExchangePage': {
  'id': 14121551488,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageHyperslice': {
  'id': 14121551489,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageBindingHyperslice': {
  'id': 141215514810,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePage': {
  'id': 141215514811,
  'symbols': {},
  'methods': {
   'changellyExchange': 1,
   'filterCreateTransaction': 2,
   'filterCheckPayment': 4,
   'filterGetFixedOffer': 5,
   'filterGetOffer': 6,
   'filterCreateFixedTransaction': 7,
   'filterGetTransaction': 8
  }
 },
 'xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice': {
  'id': 141215514812,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice': {
  'id': 141215514813,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel': {
  'id': 141215514814,
  'symbols': {},
  'methods': {
   'beforeViewChangellyExchange': 1,
   'afterViewChangellyExchange': 2,
   'afterViewChangellyExchangeThrows': 3,
   'afterViewChangellyExchangeReturns': 4,
   'afterViewChangellyExchangeCancels': 5,
   'beforeEachViewChangellyExchange': 6,
   'afterEachViewChangellyExchange': 7,
   'afterEachViewChangellyExchangeReturns': 8,
   'beforeGetChangellyExchange': 9,
   'afterGetChangellyExchange': 10,
   'afterGetChangellyExchangeThrows': 11,
   'afterGetChangellyExchangeReturns': 12,
   'afterGetChangellyExchangeCancels': 13,
   'beforeEachGetChangellyExchange': 14,
   'afterEachGetChangellyExchange': 15,
   'afterEachGetChangellyExchangeReturns': 16,
   'beforeSetChangellyExchangeCtx': 17,
   'afterSetChangellyExchangeCtx': 18,
   'afterSetChangellyExchangeCtxThrows': 19,
   'afterSetChangellyExchangeCtxReturns': 20,
   'afterSetChangellyExchangeCtxCancels': 21,
   'beforeEachSetChangellyExchangeCtx': 22,
   'afterEachSetChangellyExchangeCtx': 23,
   'afterEachSetChangellyExchangeCtxReturns': 24,
   'before_ChangellyExchangePartial': 25,
   'after_ChangellyExchangePartial': 26,
   'after_ChangellyExchangePartialThrows': 27,
   'after_ChangellyExchangePartialReturns': 28,
   'after_ChangellyExchangePartialCancels': 29,
   'before_ChangellyExchangeView': 30,
   'after_ChangellyExchangeView': 31,
   'after_ChangellyExchangeViewThrows': 32,
   'after_ChangellyExchangeViewReturns': 33,
   'after_ChangellyExchangeViewCancels': 34,
   'beforeEach_ChangellyExchangeView': 35,
   'afterEach_ChangellyExchangeView': 36,
   'afterEach_ChangellyExchangeViewReturns': 37
  }
 },
 'xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller': {
  'id': 141215514815,
  'symbols': {},
  'methods': {
   'viewChangellyExchange': 1,
   'getChangellyExchange': 2,
   'setChangellyExchangeCtx': 3,
   'ChangellyExchangePartial': 4,
   'ChangellyExchangeView': 5
  }
 },
 'xyz.swapee.rc.UChangellyExchangePageView': {
  'id': 141215514816,
  'symbols': {
   'changellyExchangePageView': 1
  },
  'methods': {}
 },
 'xyz.swapee.rc.BChangellyExchangePageViewAspects': {
  'id': 141215514817,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.rc.IChangellyExchangePageViewAspects': {
  'id': 141215514818,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IHyperChangellyExchangePageView': {
  'id': 141215514819,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageViewHyperslice': {
  'id': 141215514820,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice': {
  'id': 141215514821,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.rc.IChangellyExchangePageView': {
  'id': 141215514822,
  'symbols': {},
  'methods': {
   'viewChangellyExchange': 1,
   'getChangellyExchange': 2,
   'setChangellyExchangeCtx': 3,
   'ChangellyExchangePartial': 4,
   'ChangellyExchangeView': 5
  }
 },
 'xyz.swapee.rc.IChangellyExchangeImpl': {
  'id': 141215514823,
  'symbols': {},
  'methods': {}
 }
})