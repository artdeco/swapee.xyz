/**
 * @fileoverview
 * @externs
 */

xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel={}
xyz.swapee.rc.IChangellyExchangePageView={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.rc={}
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel={}
$$xyz.swapee.rc.IChangellyExchangePageView={}
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel={}
$$xyz.swapee.rc.IChangellyExchangePage={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.Initialese  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 * @extends {io.changenow.UChangeNow.Initialese}
 * @extends {io.letsexchange.ULetsExchange.Initialese}
 */
xyz.swapee.rc.IChangellyExchangePage.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageFields  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageFields
/** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} */
xyz.swapee.rc.IChangellyExchangePageFields.prototype.changellyExchangeCtx

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageCaster  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageCaster
/** @type {!xyz.swapee.rc.BoundIChangellyExchangePage} */
xyz.swapee.rc.IChangellyExchangePageCaster.prototype.asIChangellyExchangePage
/** @type {!xyz.swapee.rc.BoundChangellyExchangePage} */
xyz.swapee.rc.IChangellyExchangePageCaster.prototype.superChangellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePageFields  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.UChangellyExchangePageFields
/** @type {xyz.swapee.rc.IChangellyExchangePage} */
xyz.swapee.rc.UChangellyExchangePageFields.prototype.changellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePageCaster  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.UChangellyExchangePageCaster
/** @type {!xyz.swapee.rc.BoundChangellyExchangePage} */
xyz.swapee.rc.UChangellyExchangePageCaster.prototype.asChangellyExchangePage
/** @type {!xyz.swapee.rc.BoundUChangellyExchangePage} */
xyz.swapee.rc.UChangellyExchangePageCaster.prototype.asUChangellyExchangePage
/** @type {!xyz.swapee.rc.BoundChangellyExchangePageUniversal} */
xyz.swapee.rc.UChangellyExchangePageCaster.prototype.superChangellyExchangePageUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @extends {xyz.swapee.rc.UChangellyExchangePageFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.UChangellyExchangePageCaster}
 */
xyz.swapee.rc.UChangellyExchangePage = function() {}
/** @param {...!xyz.swapee.rc.UChangellyExchangePage.Initialese} init */
xyz.swapee.rc.UChangellyExchangePage.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @extends {xyz.swapee.rc.IChangellyExchangePageFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageCaster}
 * @extends {com.changelly.UChangelly}
 * @extends {io.changenow.UChangeNow}
 * @extends {io.letsexchange.ULetsExchange}
 * @extends {xyz.swapee.rc.UChangellyExchangePage}
 */
xyz.swapee.rc.IChangellyExchangePage = function() {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePage.Initialese} init */
xyz.swapee.rc.IChangellyExchangePage.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx
 * @return {(undefined|!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)}
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.changellyExchange = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)}
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterGetFixedOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)}
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterGetOffer = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)}
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterCreateTransaction = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)}
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterGetTransaction = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)}
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterCreateFixedTransaction = function(form, answers, validation, errors, ctx) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)}
 */
xyz.swapee.rc.IChangellyExchangePage.prototype.filterCheckPayment = function(form, answers, validation, errors, ctx) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IChangellyExchangePage.Initialese} init
 * @implements {xyz.swapee.rc.IChangellyExchangePage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePage.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePage = function(...init) {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePage.Initialese} init */
xyz.swapee.rc.ChangellyExchangePage.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.ChangellyExchangePage.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage = function() {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)|!xyz.swapee.rc.IChangellyExchangePageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePage.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)|!xyz.swapee.rc.IChangellyExchangePageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange)|(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)|!xyz.swapee.rc.IChangellyExchangePageHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePage}
 */
xyz.swapee.rc.AbstractChangellyExchangePage.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeThrows>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterChangellyExchangeThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeCancels>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterChangellyExchangeCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeEachChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchangeReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetFixedOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferThrows>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetFixedOfferThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetFixedOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferCancels>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetFixedOfferCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetFixedOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeEachFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOfferReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterGetFixedOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferThrows>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetOfferThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferCancels>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetOfferCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeEachFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOffer>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOfferReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOfferReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterGetOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionThrows>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateTransactionThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionCancels>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateTransactionCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeEachFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransactionReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterCreateTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionThrows>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetTransactionThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionCancels>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterGetTransactionCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeEachFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransactionReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterGetTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateFixedTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionThrows>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateFixedTransactionThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateFixedTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionCancels>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCreateFixedTransactionCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateFixedTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeEachFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransactionReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransactionReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterCreateFixedTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCheckPayment>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPayment>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentThrows>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCheckPaymentThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCheckPaymentReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentCancels>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterFilterCheckPaymentCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCheckPayment>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.beforeEachFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPayment>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPaymentReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPaymentReturns>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice.prototype.afterEachFilterCheckPaymentReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageJoinpointModelHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageJoinpointModelHyperslice}
 */
xyz.swapee.rc.ChangellyExchangePageJoinpointModelHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterChangellyExchangeThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterChangellyExchangeCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeEachChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetFixedOfferThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetFixedOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetFixedOfferCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeEachFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterGetFixedOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetOfferThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetOfferCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeEachFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterGetOfferReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateTransactionThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateTransactionCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeEachFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterCreateTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetTransactionThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterGetTransactionCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeEachFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterGetTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateFixedTransactionThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateFixedTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCreateFixedTransactionCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeEachFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterCreateFixedTransactionReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCheckPaymentThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCheckPaymentReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterFilterCheckPaymentCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.beforeEachFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterCheckPayment
/** @type {(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice.prototype.afterEachFilterCheckPaymentReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageJoinpointModelBindingHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.ChangellyExchangePageJoinpointModelBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchangeThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchangeReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterChangellyExchangeCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachChangellyExchangeReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterGetFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetFixedOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterGetFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetFixedOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetFixedOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterGetOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOfferThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetOfferCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterGetOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetOffer = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetOfferReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterCreateTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransactionThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransactionReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateTransactionCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterCreateTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateTransactionReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterGetTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransactionThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransactionReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterGetTransactionCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterGetTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterGetTransactionReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterCreateFixedTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransactionThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransactionReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCreateFixedTransactionCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterCreateFixedTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateFixedTransaction = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCreateFixedTransactionReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeFilterCheckPayment = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPayment = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPaymentThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPaymentReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterFilterCheckPaymentCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.beforeEachFilterCheckPayment = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCheckPayment = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.prototype.afterEachFilterCheckPaymentReturns = function(data) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageJoinpointModel  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageJoinpointModel}
 */
xyz.swapee.rc.ChangellyExchangePageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.RecordIChangellyExchangePageJoinpointModel  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ beforeChangellyExchange: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeChangellyExchange, afterChangellyExchange: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchange, afterChangellyExchangeThrows: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeThrows, afterChangellyExchangeReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeReturns, afterChangellyExchangeCancels: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeCancels, beforeEachChangellyExchange: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachChangellyExchange, afterEachChangellyExchange: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchange, afterEachChangellyExchangeReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchangeReturns, beforeFilterGetFixedOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetFixedOffer, afterFilterGetFixedOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOffer, afterFilterGetFixedOfferThrows: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferThrows, afterFilterGetFixedOfferReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferReturns, afterFilterGetFixedOfferCancels: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferCancels, beforeEachFilterGetFixedOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetFixedOffer, afterEachFilterGetFixedOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOffer, afterEachFilterGetFixedOfferReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOfferReturns, beforeFilterGetOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetOffer, afterFilterGetOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOffer, afterFilterGetOfferThrows: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferThrows, afterFilterGetOfferReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferReturns, afterFilterGetOfferCancels: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferCancels, beforeEachFilterGetOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetOffer, afterEachFilterGetOffer: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOffer, afterEachFilterGetOfferReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOfferReturns, beforeFilterCreateTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateTransaction, afterFilterCreateTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransaction, afterFilterCreateTransactionThrows: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionThrows, afterFilterCreateTransactionReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionReturns, afterFilterCreateTransactionCancels: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionCancels, beforeEachFilterCreateTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateTransaction, afterEachFilterCreateTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransaction, afterEachFilterCreateTransactionReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransactionReturns, beforeFilterGetTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetTransaction, afterFilterGetTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransaction, afterFilterGetTransactionThrows: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionThrows, afterFilterGetTransactionReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionReturns, afterFilterGetTransactionCancels: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionCancels, beforeEachFilterGetTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetTransaction, afterEachFilterGetTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransaction, afterEachFilterGetTransactionReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransactionReturns, beforeFilterCreateFixedTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateFixedTransaction, afterFilterCreateFixedTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransaction, afterFilterCreateFixedTransactionThrows: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionThrows, afterFilterCreateFixedTransactionReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionReturns, afterFilterCreateFixedTransactionCancels: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionCancels, beforeEachFilterCreateFixedTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateFixedTransaction, afterEachFilterCreateFixedTransaction: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransaction, afterEachFilterCreateFixedTransactionReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransactionReturns, beforeFilterCheckPayment: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCheckPayment, afterFilterCheckPayment: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPayment, afterFilterCheckPaymentThrows: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentThrows, afterFilterCheckPaymentReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentReturns, afterFilterCheckPaymentCancels: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentCancels, beforeEachFilterCheckPayment: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCheckPayment, afterEachFilterCheckPayment: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPayment, afterEachFilterCheckPaymentReturns: xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPaymentReturns }} */
xyz.swapee.rc.RecordIChangellyExchangePageJoinpointModel

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundIChangellyExchangePageJoinpointModel  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIChangellyExchangePageJoinpointModel}
 */
xyz.swapee.rc.BoundIChangellyExchangePageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundChangellyExchangePageJoinpointModel  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIChangellyExchangePageJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundChangellyExchangePageJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeChangellyExchange  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchange  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeThrows  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeThrows

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeCancels  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterChangellyExchangeCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterChangellyExchangeCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterChangellyExchangeCancels

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachChangellyExchange  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchange  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchangeReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachChangellyExchangeReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachChangellyExchangeReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachChangellyExchangeReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetFixedOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetFixedOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferThrows  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferThrows

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferCancels  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetFixedOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetFixedOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetFixedOfferCancels

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetFixedOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetFixedOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOfferReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetFixedOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetFixedOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetFixedOfferReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferThrows  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferThrows

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferCancels  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetOfferCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetOfferCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetOfferCancels

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOffer

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOfferReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetOfferReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetOfferReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetOfferReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionThrows  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionThrows

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionCancels  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateTransactionCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateTransactionCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateTransactionCancels

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransactionReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateTransactionReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateTransactionReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateTransactionReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterGetTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterGetTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterGetTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionThrows  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionThrows

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionCancels  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterGetTransactionCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterGetTransactionCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterGetTransactionCancels

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterGetTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterGetTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterGetTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransactionReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterGetTransactionReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterGetTransactionReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterGetTransactionReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateFixedTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCreateFixedTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionThrows  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionThrows

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionCancels  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCreateFixedTransactionCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCreateFixedTransactionCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCreateFixedTransactionCancels

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateFixedTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCreateFixedTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransaction

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransactionReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCreateFixedTransactionReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCreateFixedTransactionReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCreateFixedTransactionReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCheckPayment  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeFilterCheckPayment
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeFilterCheckPayment
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeFilterCheckPayment

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPayment  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPayment
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPayment
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPayment

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentThrows  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentThrows

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentCancels  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterFilterCheckPaymentCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterFilterCheckPaymentCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterFilterCheckPaymentCancels

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCheckPayment  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.beforeEachFilterCheckPayment
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._beforeEachFilterCheckPayment
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__beforeEachFilterCheckPayment

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPayment  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPayment
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPayment
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPayment

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPaymentReturns  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.afterEachFilterCheckPaymentReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel._afterEachFilterCheckPaymentReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.__afterEachFilterCheckPaymentReturns

// nss:xyz.swapee.rc.IChangellyExchangePageJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageAspectsInstaller  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller = function() {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese} init */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterChangellyExchangeThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterChangellyExchangeReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterChangellyExchangeCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeEachChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachChangellyExchangeReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeFilterGetFixedOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetFixedOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetFixedOfferThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetFixedOfferCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeEachFilterGetFixedOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterGetFixedOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterGetFixedOfferReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeFilterGetOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetOfferThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetOfferReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetOfferCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeEachFilterGetOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterGetOffer
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterGetOfferReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeFilterCreateTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateTransactionThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateTransactionReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateTransactionCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeEachFilterCreateTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterCreateTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterCreateTransactionReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeFilterGetTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetTransactionThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetTransactionReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterGetTransactionCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeEachFilterGetTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterGetTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterGetTransactionReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeFilterCreateFixedTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateFixedTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateFixedTransactionThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateFixedTransactionReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCreateFixedTransactionCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeEachFilterCreateFixedTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterCreateFixedTransaction
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterCreateFixedTransactionReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeFilterCheckPayment
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCheckPayment
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCheckPaymentThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCheckPaymentReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterFilterCheckPaymentCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.beforeEachFilterCheckPayment
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterCheckPayment
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.afterEachFilterCheckPaymentReturns
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.changellyExchange = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.filterGetFixedOffer = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.filterGetOffer = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.filterCreateTransaction = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.filterGetTransaction = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.filterCreateFixedTransaction = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.prototype.filterCheckPayment = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageAspectsInstaller  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.rc.IChangellyExchangePageAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePageAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese} init */
xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.ChangellyExchangePageAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller = function() {}
/**
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageAspectsInstallerConstructor  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageAspectsInstaller, ...!xyz.swapee.rc.IChangellyExchangePageAspectsInstaller.Initialese)} */
xyz.swapee.rc.ChangellyExchangePageAspectsInstallerConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.ChangellyExchangeNArgs  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ form: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx }} */
xyz.swapee.rc.IChangellyExchangePage.ChangellyExchangeNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePage.ChangellyExchangeNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePage.ChangellyExchangeNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeChangellyExchangePointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterChangellyExchangePointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsChangellyExchangePointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData.prototype.res
/**
 * @param {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsChangellyExchangePointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.ChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsChangellyExchangePointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.FilterGetFixedOfferNArgs  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ form: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx }} */
xyz.swapee.rc.IChangellyExchangePage.FilterGetFixedOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePage.FilterGetFixedOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePage.FilterGetFixedOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetFixedOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetFixedOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetFixedOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetFixedOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetFixedOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.FilterGetOfferNArgs  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ form: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx }} */
xyz.swapee.rc.IChangellyExchangePage.FilterGetOfferNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePage.FilterGetOfferNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePage.FilterGetOfferNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetOfferPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetOfferPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetOfferPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetOfferPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetOfferPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.FilterCreateTransactionNArgs  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ form: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx }} */
xyz.swapee.rc.IChangellyExchangePage.FilterCreateTransactionNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePage.FilterCreateTransactionNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePage.FilterCreateTransactionNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateTransactionPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateTransactionPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateTransactionPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateTransactionPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateTransactionPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.FilterGetTransactionNArgs  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ form: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx }} */
xyz.swapee.rc.IChangellyExchangePage.FilterGetTransactionNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePage.FilterGetTransactionNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePage.FilterGetTransactionNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterGetTransactionPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterGetTransactionPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterGetTransactionPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterGetTransactionPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterGetTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterGetTransactionPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.FilterCreateFixedTransactionNArgs  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ form: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx }} */
xyz.swapee.rc.IChangellyExchangePage.FilterCreateFixedTransactionNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePage.FilterCreateFixedTransactionNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePage.FilterCreateFixedTransactionNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCreateFixedTransactionPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCreateFixedTransactionPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCreateFixedTransactionPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCreateFixedTransactionPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCreateFixedTransactionPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCreateFixedTransactionPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.FilterCheckPaymentNArgs  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ form: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form, answers: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers, validation: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation, errors: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors, ctx: !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx }} */
xyz.swapee.rc.IChangellyExchangePage.FilterCheckPaymentNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePage.FilterCheckPaymentNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePage.FilterCheckPaymentNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.BeforeFilterCheckPaymentPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterFilterCheckPaymentPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterThrowsFilterCheckPaymentPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData = function() {}
/** @type {(xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData.prototype.res
/**
 * @param {(xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterReturnsFilterCheckPaymentPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModel.FilterCheckPaymentPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageJoinpointModel.AfterCancelsFilterCheckPaymentPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageConstructor  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePage, ...!xyz.swapee.rc.IChangellyExchangePage.Initialese)} */
xyz.swapee.rc.ChangellyExchangePageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageMetaUniversal  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {symbol} */
xyz.swapee.rc.ChangellyExchangePageMetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePage.Initialese  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.UChangellyExchangePage.Initialese = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePage|undefined} */
xyz.swapee.rc.UChangellyExchangePage.Initialese.prototype.changellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageUniversal  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.UChangellyExchangePage.Initialese} init
 * @implements {xyz.swapee.rc.UChangellyExchangePage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UChangellyExchangePage.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePageUniversal = function(...init) {}
/** @param {...!xyz.swapee.rc.UChangellyExchangePage.Initialese} init */
xyz.swapee.rc.ChangellyExchangePageUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.ChangellyExchangePageUniversal.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePageUniversal  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal = function() {}
/**
 * @param {...(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.UChangellyExchangePage|typeof xyz.swapee.rc.UChangellyExchangePage)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePageUniversal.MetaUniversal  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {xyz.swapee.rc.ChangellyExchangePageMetaUniversal} */
xyz.swapee.rc.AbstractChangellyExchangePageUniversal.MetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.UChangellyExchangePageConstructor  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {function(new: xyz.swapee.rc.UChangellyExchangePage, ...!xyz.swapee.rc.UChangellyExchangePage.Initialese)} */
xyz.swapee.rc.UChangellyExchangePageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.RecordUChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordUChangellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundUChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.UChangellyExchangePageFields}
 * @extends {xyz.swapee.rc.RecordUChangellyExchangePage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.UChangellyExchangePageCaster}
 */
xyz.swapee.rc.BoundUChangellyExchangePage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundChangellyExchangePageUniversal  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundUChangellyExchangePage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundChangellyExchangePageUniversal = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BChangellyExchangePageAspectsCaster  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageAspectsCaster
/** @type {!xyz.swapee.rc.BoundIChangellyExchangePage} */
xyz.swapee.rc.BChangellyExchangePageAspectsCaster.prototype.asIChangellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BChangellyExchangePageAspects  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @extends {xyz.swapee.rc.BChangellyExchangePageAspectsCaster<THIS>}
 * @extends {xyz.swapee.rc.IChangellyExchangePageJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageAspects = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageAspects.Initialese  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePageAspects.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageAspectsCaster  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageAspectsCaster
/** @type {!xyz.swapee.rc.BoundIChangellyExchangePage} */
xyz.swapee.rc.IChangellyExchangePageAspectsCaster.prototype.asIChangellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageAspects  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageAspectsCaster}
 * @extends {xyz.swapee.rc.BChangellyExchangePageAspects<!xyz.swapee.rc.IChangellyExchangePageAspects>}
 * @extends {com.changelly.UChangelly}
 * @extends {io.changenow.UChangeNow}
 * @extends {io.letsexchange.ULetsExchange}
 */
xyz.swapee.rc.IChangellyExchangePageAspects = function() {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese} init */
xyz.swapee.rc.IChangellyExchangePageAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageAspects  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese} init
 * @implements {xyz.swapee.rc.IChangellyExchangePageAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePageAspects = function(...init) {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese} init */
xyz.swapee.rc.ChangellyExchangePageAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.ChangellyExchangePageAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractChangellyExchangePageAspects  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects = function() {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageAspectsConstructor  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageAspects, ...!xyz.swapee.rc.IChangellyExchangePageAspects.Initialese)} */
xyz.swapee.rc.ChangellyExchangePageAspectsConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IHyperChangellyExchangePage.Initialese  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.Initialese}
 */
xyz.swapee.rc.IHyperChangellyExchangePage.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IHyperChangellyExchangePageCaster  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.IHyperChangellyExchangePageCaster
/** @type {!xyz.swapee.rc.BoundIHyperChangellyExchangePage} */
xyz.swapee.rc.IHyperChangellyExchangePageCaster.prototype.asIHyperChangellyExchangePage
/** @type {!xyz.swapee.rc.BoundHyperChangellyExchangePage} */
xyz.swapee.rc.IHyperChangellyExchangePageCaster.prototype.superHyperChangellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IHyperChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IHyperChangellyExchangePageCaster}
 * @extends {xyz.swapee.rc.IChangellyExchangePage}
 */
xyz.swapee.rc.IHyperChangellyExchangePage = function() {}
/** @param {...!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese} init */
xyz.swapee.rc.IHyperChangellyExchangePage.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.HyperChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese} init
 * @implements {xyz.swapee.rc.IHyperChangellyExchangePage}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese>}
 */
xyz.swapee.rc.HyperChangellyExchangePage = function(...init) {}
/** @param {...!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese} init */
xyz.swapee.rc.HyperChangellyExchangePage.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.HyperChangellyExchangePage.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.AbstractHyperChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @extends {xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage = function() {}
/**
 * @param {...((!xyz.swapee.rc.IHyperChangellyExchangePage|typeof xyz.swapee.rc.HyperChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperChangellyExchangePage|typeof xyz.swapee.rc.HyperChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperChangellyExchangePage|typeof xyz.swapee.rc.HyperChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePage}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageAspects|!Array<!xyz.swapee.rc.IChangellyExchangePageAspects>|function(new: xyz.swapee.rc.IChangellyExchangePageAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePage}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePage.installs = function(...aspectsInstallers) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.HyperChangellyExchangePageConstructor  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {function(new: xyz.swapee.rc.IHyperChangellyExchangePage, ...!xyz.swapee.rc.IHyperChangellyExchangePage.Initialese)} */
xyz.swapee.rc.HyperChangellyExchangePageConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.RecordIHyperChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordIHyperChangellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundIHyperChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIHyperChangellyExchangePage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IHyperChangellyExchangePageCaster}
 */
xyz.swapee.rc.BoundIHyperChangellyExchangePage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundHyperChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIHyperChangellyExchangePage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundHyperChangellyExchangePage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePage._changellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._changellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.changellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePage._filterGetFixedOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterGetFixedOffer>)} */
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.filterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePage._filterGetOffer|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterGetOffer>)} */
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.filterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePage._filterCreateTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterCreateTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.filterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePage._filterGetTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterGetTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.filterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePage._filterCreateFixedTransaction|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterCreateFixedTransaction>)} */
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.filterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePage._filterCheckPayment|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage._filterCheckPayment>)} */
xyz.swapee.rc.IChangellyExchangePageHyperslice.prototype.filterCheckPayment

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageHyperslice}
 */
xyz.swapee.rc.ChangellyExchangePageHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePageBindingHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.__changellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__changellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.changellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.filterGetFixedOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.filterGetOffer
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.filterCreateTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.filterGetTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.filterCreateFixedTransaction
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageBindingHyperslice.prototype.filterCheckPayment

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.ChangellyExchangePageBindingHyperslice  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.ChangellyExchangePageBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.RecordIChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/** @typedef {{ changellyExchange: xyz.swapee.rc.IChangellyExchangePage.changellyExchange, filterGetFixedOffer: xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer, filterGetOffer: xyz.swapee.rc.IChangellyExchangePage.filterGetOffer, filterCreateTransaction: xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction, filterGetTransaction: xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction, filterCreateFixedTransaction: xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction, filterCheckPayment: xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment }} */
xyz.swapee.rc.RecordIChangellyExchangePage

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundIChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageFields}
 * @extends {xyz.swapee.rc.RecordIChangellyExchangePage}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageCaster}
 * @extends {com.changelly.BoundUChangelly}
 * @extends {io.changenow.BoundUChangeNow}
 * @extends {io.letsexchange.BoundULetsExchange}
 */
xyz.swapee.rc.BoundIChangellyExchangePage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.BoundChangellyExchangePage  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIChangellyExchangePage}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundChangellyExchangePage = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx
 * @return {(undefined|!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)}
 */
$$xyz.swapee.rc.IChangellyExchangePage.__changellyExchange = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx): (undefined|!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePage, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx): (undefined|!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage._changellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePage.__changellyExchange} */
xyz.swapee.rc.IChangellyExchangePage.__changellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePage,$$xyz.swapee.rc.IChangellyExchangePage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePage, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage._filterGetFixedOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer} */
xyz.swapee.rc.IChangellyExchangePage.__filterGetFixedOffer

// nss:xyz.swapee.rc.IChangellyExchangePage,$$xyz.swapee.rc.IChangellyExchangePage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)}
 */
$$xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePage, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage._filterGetOffer
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer} */
xyz.swapee.rc.IChangellyExchangePage.__filterGetOffer

// nss:xyz.swapee.rc.IChangellyExchangePage,$$xyz.swapee.rc.IChangellyExchangePage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)}
 */
$$xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePage, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage._filterCreateTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction} */
xyz.swapee.rc.IChangellyExchangePage.__filterCreateTransaction

// nss:xyz.swapee.rc.IChangellyExchangePage,$$xyz.swapee.rc.IChangellyExchangePage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)}
 */
$$xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePage, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage._filterGetTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction} */
xyz.swapee.rc.IChangellyExchangePage.__filterGetTransaction

// nss:xyz.swapee.rc.IChangellyExchangePage,$$xyz.swapee.rc.IChangellyExchangePage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)}
 */
$$xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePage, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage._filterCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction} */
xyz.swapee.rc.IChangellyExchangePage.__filterCreateFixedTransaction

// nss:xyz.swapee.rc.IChangellyExchangePage,$$xyz.swapee.rc.IChangellyExchangePage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form} form
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation} validation
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx} ctx
 * @return {(undefined|xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)}
 */
$$xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment = function(form, answers, validation, errors, ctx) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePage, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors, !xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx): (undefined|xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers|!Promise<undefined>|!Promise<!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers>)} */
xyz.swapee.rc.IChangellyExchangePage._filterCheckPayment
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment} */
xyz.swapee.rc.IChangellyExchangePage.__filterCheckPayment

// nss:xyz.swapee.rc.IChangellyExchangePage,$$xyz.swapee.rc.IChangellyExchangePage,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form = function() {}
/** @type {string} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.rate
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.fixedId
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.estimatedFixedAmountTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.minAmount
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers.prototype.maxAmount

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.rate
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.estimatedFloatAmountTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.networkFee
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.partnerFee
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.visibleAmount
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.minAmount
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers.prototype.maxAmount

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.id
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.createTransactionError
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.createdAt
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.payinAddress
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.payinExtraId
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.status
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.kycRequired
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers.prototype.confirmedAmountFrom

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.amountTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.networkFee
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.partnerFee
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.visibleAmount
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.notFound
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.rate
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.id
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.createdAt
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.payinAddress
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.payinExtraId
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.status
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers.prototype.confirmedAmountFrom

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.getOffer
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.id
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.createTransactionError
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.createdAt
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.payinAddress
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.payinExtraId
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.status
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.kycRequired
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers.prototype.confirmedAmountFrom

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers.prototype.status
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers.prototype.checkPaymentError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Answers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Answers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Answers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Answers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Answers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Answers}
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers = function() {}
/** @type {string} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.rate
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.fixedId
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.estimatedFixedAmountTo
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.minAmount
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers.prototype.maxAmount

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.rate
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.estimatedFloatAmountTo
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.networkFee
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.partnerFee
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.visibleAmount
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.minAmount
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers.prototype.maxAmount

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.id
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.createTransactionError
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.createdAt
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.payinAddress
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.payinExtraId
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.status
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.kycRequired
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers.prototype.confirmedAmountFrom

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.fixed
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.currencyFrom
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.currencyTo
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.amountFrom
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.amountTo
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.networkFee
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.partnerFee
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.visibleAmount
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.notFound
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.rate
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.id
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.createdAt
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.payinAddress
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.payinExtraId
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.status
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers.prototype.confirmedAmountFrom

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.getOffer
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.id
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.createTransactionError
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.createdAt
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.payinAddress
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.payinExtraId
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.status
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.kycRequired
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers.prototype.confirmedAmountFrom

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers = function() {}
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers.prototype.status
/** @type {(*)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers.prototype.checkPaymentError

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers  3a80ec1fd0d85c03796882f86b16c99b */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptAnswers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptAnswers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptAnswers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptAnswers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptAnswers}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptAnswers}
 */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers = function() {}
/** @type {string|undefined} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptAnswers.prototype.locale

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx = function() {}
/** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx.prototype.validation
/** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx.prototype.errors
/** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx.prototype.answers

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx.prototype.validation
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx.prototype.errors
/** @type {(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers)|undefined} */
xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx.prototype.answers

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.ready
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form.prototype.getOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptCtx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.ready
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form.prototype.getOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptCtx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.address
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.refundAddress
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form.prototype.createTransaction

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptCtx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form.prototype.tid

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptCtx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.amountFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.currencyFrom
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.currencyTo
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.fixed
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.address
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.refundAddress
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.createTransaction
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form.prototype.fixedId

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptCtx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form = function() {}
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form.prototype.id
/** @type {*} */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form.prototype.checkPayment

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePage.xml} xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptCtx  3a80ec1fd0d85c03796882f86b16c99b */
/** @record */
xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.OptCtx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange  c658f76c826180a39db89d8927a6e60e */
/** @constructor */
xyz.swapee.rc.ChangellyExchange = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Answers  c658f76c826180a39db89d8927a6e60e */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers}
 */
xyz.swapee.rc.ChangellyExchange.Answers = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Answers.Props  c658f76c826180a39db89d8927a6e60e */
/** @record */
xyz.swapee.rc.ChangellyExchange.Answers.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Form  c658f76c826180a39db89d8927a6e60e */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form}
 */
xyz.swapee.rc.ChangellyExchange.Form = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Form.Props  c658f76c826180a39db89d8927a6e60e */
/** @record */
xyz.swapee.rc.ChangellyExchange.Form.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Errors  c658f76c826180a39db89d8927a6e60e */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors}
 */
xyz.swapee.rc.ChangellyExchange.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Errors.Props  c658f76c826180a39db89d8927a6e60e */
/** @record */
xyz.swapee.rc.ChangellyExchange.Errors.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Validation  c658f76c826180a39db89d8927a6e60e */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation}
 */
xyz.swapee.rc.ChangellyExchange.Validation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Validation.Props  c658f76c826180a39db89d8927a6e60e */
/** @record */
xyz.swapee.rc.ChangellyExchange.Validation.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Ctx  c658f76c826180a39db89d8927a6e60e */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx}
 */
xyz.swapee.rc.ChangellyExchange.Ctx = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageAliases.xml} xyz.swapee.rc.ChangellyExchange.Ctx.Props  c658f76c826180a39db89d8927a6e60e */
/** @record */
xyz.swapee.rc.ChangellyExchange.Ctx.Props = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.Initialese  99f69c6e23bc807aae3610aad583bb36 */
/** @record */
xyz.swapee.rc.IChangellyExchangePageView.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewFields  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageViewFields
/** @type {!xyz.swapee.rc.IChangellyExchangePageView.GET} */
xyz.swapee.rc.IChangellyExchangePageViewFields.prototype.GET
/** @type {!Object<string, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation>} */
xyz.swapee.rc.IChangellyExchangePageViewFields.prototype.changellyExchangeTranslations

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewCaster  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageViewCaster
/** @type {!xyz.swapee.rc.BoundIChangellyExchangePageView} */
xyz.swapee.rc.IChangellyExchangePageViewCaster.prototype.asIChangellyExchangePageView
/** @type {!xyz.swapee.rc.BoundIChangellyExchangePage} */
xyz.swapee.rc.IChangellyExchangePageViewCaster.prototype.asIChangellyExchangePage
/** @type {!xyz.swapee.rc.BoundChangellyExchangePageView} */
xyz.swapee.rc.IChangellyExchangePageViewCaster.prototype.superChangellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageViewFields  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.UChangellyExchangePageViewFields
/** @type {xyz.swapee.rc.IChangellyExchangePageView} */
xyz.swapee.rc.UChangellyExchangePageViewFields.prototype.changellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageViewCaster  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.UChangellyExchangePageViewCaster
/** @type {!xyz.swapee.rc.BoundChangellyExchangePageView} */
xyz.swapee.rc.UChangellyExchangePageViewCaster.prototype.asChangellyExchangePageView
/** @type {!xyz.swapee.rc.BoundUChangellyExchangePageView} */
xyz.swapee.rc.UChangellyExchangePageViewCaster.prototype.asUChangellyExchangePageView
/** @type {!xyz.swapee.rc.BoundChangellyExchangePageViewUniversal} */
xyz.swapee.rc.UChangellyExchangePageViewCaster.prototype.superChangellyExchangePageViewUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @extends {xyz.swapee.rc.UChangellyExchangePageViewFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.UChangellyExchangePageViewCaster}
 */
xyz.swapee.rc.UChangellyExchangePageView = function() {}
/** @param {...!xyz.swapee.rc.UChangellyExchangePageView.Initialese} init */
xyz.swapee.rc.UChangellyExchangePageView.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewCaster}
 * @extends {_idio.IRedirectMod}
 * @extends {xyz.swapee.rc.UChangellyExchangePageView}
 */
xyz.swapee.rc.IChangellyExchangePageView = function() {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageView.Initialese} init */
xyz.swapee.rc.IChangellyExchangePageView.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form
 * @return {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView}
 */
xyz.swapee.rc.IChangellyExchangePageView.prototype.viewChangellyExchange = function(ctx, form) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form
 * @return {(undefined|!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView)}
 */
xyz.swapee.rc.IChangellyExchangePageView.prototype.getChangellyExchange = function(ctx, form) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation
 * @return {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx}
 */
xyz.swapee.rc.IChangellyExchangePageView.prototype.setChangellyExchangeCtx = function(answers, translation) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions} actions
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IChangellyExchangePageView.prototype.ChangellyExchangePartial = function(answers, errors, actions, translation) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions} actions
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation
 * @return {engineering.type.VNode}
 */
xyz.swapee.rc.IChangellyExchangePageView.prototype.ChangellyExchangeView = function(answers, errors, actions, translation) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IChangellyExchangePageView.Initialese} init
 * @implements {xyz.swapee.rc.IChangellyExchangePageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageView.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePageView = function(...init) {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageView.Initialese} init */
xyz.swapee.rc.ChangellyExchangePageView.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.ChangellyExchangePageView.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView = function() {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)|!xyz.swapee.rc.IChangellyExchangePageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)|!xyz.swapee.rc.IChangellyExchangePageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)|!xyz.swapee.rc.IChangellyExchangePageViewHyperslice)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractChangellyExchangePageView.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeViewChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.beforeViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeThrows>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterViewChangellyExchangeThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterViewChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeCancels>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterViewChangellyExchangeCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachViewChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.beforeEachViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEachViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchangeReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEachViewChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeGetChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.beforeGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeThrows>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterGetChangellyExchangeThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterGetChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeCancels>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterGetChangellyExchangeCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachGetChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.beforeEachGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEachGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchangeReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchangeReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEachGetChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeSetChangellyExchangeCtx>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.beforeSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtx>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxThrows>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterSetChangellyExchangeCtxThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterSetChangellyExchangeCtxReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxCancels>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterSetChangellyExchangeCtxCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachSetChangellyExchangeCtx>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.beforeEachSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtx>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEachSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtxReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtxReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEachSetChangellyExchangeCtxReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangePartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangePartial>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.before_ChangellyExchangePartial
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartial|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartial>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangePartial
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialThrows>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangePartialThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangePartialReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialCancels>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangePartialCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangeView>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.before_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeView>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewThrows|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewThrows>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangeViewThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangeViewReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewCancels|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewCancels>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.after_ChangellyExchangeViewCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEach_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEach_ChangellyExchangeView>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.beforeEach_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeView>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEach_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeViewReturns|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeViewReturns>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice.prototype.afterEach_ChangellyExchangeViewReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelHyperslice}
 */
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.beforeViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterViewChangellyExchangeThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterViewChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterViewChangellyExchangeCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.beforeEachViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEachViewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEachViewChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.beforeGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterGetChangellyExchangeThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterGetChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterGetChangellyExchangeCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.beforeEachGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEachGetChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEachGetChangellyExchangeReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.beforeSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterSetChangellyExchangeCtxThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterSetChangellyExchangeCtxReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterSetChangellyExchangeCtxCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.beforeEachSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEachSetChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEachSetChangellyExchangeCtxReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.before_ChangellyExchangePartial
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangePartial
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangePartialThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangePartialReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangePartialCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.before_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangeViewThrows
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangeViewReturns
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.after_ChangellyExchangeViewCancels
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.beforeEach_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEach_ChangellyExchangeView
/** @type {(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice.prototype.afterEach_ChangellyExchangeViewReturns

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelBindingHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModelBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeViewChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchangeThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchangeReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterViewChangellyExchangeCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEachViewChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachViewChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachViewChangellyExchangeReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeGetChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchangeThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchangeReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterGetChangellyExchangeCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEachGetChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachGetChangellyExchange = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachGetChangellyExchangeReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeSetChangellyExchangeCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtxThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtxReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterSetChangellyExchangeCtxCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEachSetChangellyExchangeCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachSetChangellyExchangeCtx = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEachSetChangellyExchangeCtxReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.before_ChangellyExchangePartial = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartial = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartialThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartialReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangePartialCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.before_ChangellyExchangeView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeViewThrows = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeViewReturns = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.after_ChangellyExchangeViewCancels = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.beforeEach_ChangellyExchangeView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEach_ChangellyExchangeView = function(data) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.prototype.afterEach_ChangellyExchangeViewReturns = function(data) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewJoinpointModel  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel}
 */
xyz.swapee.rc.ChangellyExchangePageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.RecordIChangellyExchangePageViewJoinpointModel  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ beforeViewChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeViewChangellyExchange, afterViewChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchange, afterViewChangellyExchangeThrows: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeThrows, afterViewChangellyExchangeReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeReturns, afterViewChangellyExchangeCancels: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeCancels, beforeEachViewChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachViewChangellyExchange, afterEachViewChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchange, afterEachViewChangellyExchangeReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchangeReturns, beforeGetChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeGetChangellyExchange, afterGetChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchange, afterGetChangellyExchangeThrows: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeThrows, afterGetChangellyExchangeReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeReturns, afterGetChangellyExchangeCancels: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeCancels, beforeEachGetChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachGetChangellyExchange, afterEachGetChangellyExchange: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchange, afterEachGetChangellyExchangeReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchangeReturns, beforeSetChangellyExchangeCtx: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeSetChangellyExchangeCtx, afterSetChangellyExchangeCtx: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtx, afterSetChangellyExchangeCtxThrows: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxThrows, afterSetChangellyExchangeCtxReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxReturns, afterSetChangellyExchangeCtxCancels: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxCancels, beforeEachSetChangellyExchangeCtx: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachSetChangellyExchangeCtx, afterEachSetChangellyExchangeCtx: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtx, afterEachSetChangellyExchangeCtxReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtxReturns, before_ChangellyExchangePartial: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangePartial, after_ChangellyExchangePartial: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartial, after_ChangellyExchangePartialThrows: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialThrows, after_ChangellyExchangePartialReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialReturns, after_ChangellyExchangePartialCancels: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialCancels, before_ChangellyExchangeView: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangeView, after_ChangellyExchangeView: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeView, after_ChangellyExchangeViewThrows: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewThrows, after_ChangellyExchangeViewReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewReturns, after_ChangellyExchangeViewCancels: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewCancels, beforeEach_ChangellyExchangeView: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEach_ChangellyExchangeView, afterEach_ChangellyExchangeView: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeView, afterEach_ChangellyExchangeViewReturns: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeViewReturns }} */
xyz.swapee.rc.RecordIChangellyExchangePageViewJoinpointModel

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundIChangellyExchangePageViewJoinpointModel  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIChangellyExchangePageViewJoinpointModel}
 */
xyz.swapee.rc.BoundIChangellyExchangePageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundChangellyExchangePageViewJoinpointModel  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIChangellyExchangePageViewJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundChangellyExchangePageViewJoinpointModel = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeViewChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeViewChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeViewChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeViewChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeThrows  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeThrows

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeCancels  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterViewChangellyExchangeCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterViewChangellyExchangeCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterViewChangellyExchangeCancels

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachViewChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachViewChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachViewChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachViewChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchangeReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachViewChangellyExchangeReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachViewChangellyExchangeReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachViewChangellyExchangeReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeGetChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeGetChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeGetChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeGetChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeThrows  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeThrows

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeCancels  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterGetChangellyExchangeCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterGetChangellyExchangeCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterGetChangellyExchangeCancels

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachGetChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachGetChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachGetChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachGetChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchangeReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachGetChangellyExchangeReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachGetChangellyExchangeReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachGetChangellyExchangeReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeSetChangellyExchangeCtx  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeSetChangellyExchangeCtx
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeSetChangellyExchangeCtx
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeSetChangellyExchangeCtx

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtx  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtx
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtx
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtx

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxThrows  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxThrows

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxCancels  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterSetChangellyExchangeCtxCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterSetChangellyExchangeCtxCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterSetChangellyExchangeCtxCancels

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachSetChangellyExchangeCtx  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEachSetChangellyExchangeCtx
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEachSetChangellyExchangeCtx
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEachSetChangellyExchangeCtx

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtx  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtx
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtx
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtx

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtxReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEachSetChangellyExchangeCtxReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEachSetChangellyExchangeCtxReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEachSetChangellyExchangeCtxReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangePartial  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangePartial
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangePartial
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangePartial

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartial  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartial
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartial
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartial

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialThrows  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialThrows

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialCancels  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangePartialCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangePartialCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangePartialCancels

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangeView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.before_ChangellyExchangeView
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._before_ChangellyExchangeView
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__before_ChangellyExchangeView

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeView
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeView
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeView

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewThrows  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewThrows
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewThrows
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewThrows

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewCancels  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.after_ChangellyExchangeViewCancels
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._after_ChangellyExchangeViewCancels
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__after_ChangellyExchangeViewCancels

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEach_ChangellyExchangeView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.beforeEach_ChangellyExchangeView
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._beforeEach_ChangellyExchangeView
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__beforeEach_ChangellyExchangeView

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeView
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeView
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeView

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeViewReturns  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData} [data]
 * @return {void}
 */
$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns = function(data) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.afterEach_ChangellyExchangeViewReturns
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel, !xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData=): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel._afterEach_ChangellyExchangeViewReturns
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.__afterEach_ChangellyExchangeViewReturns

// nss:xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,$$xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese  99f69c6e23bc807aae3610aad583bb36 */
/** @record */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageInstaller}
 */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller = function() {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese} init */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.beforeViewChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterViewChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterViewChangellyExchangeThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterViewChangellyExchangeReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterViewChangellyExchangeCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.beforeEachViewChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEachViewChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEachViewChangellyExchangeReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.beforeGetChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterGetChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterGetChangellyExchangeThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterGetChangellyExchangeReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterGetChangellyExchangeCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.beforeEachGetChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEachGetChangellyExchange
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEachGetChangellyExchangeReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.beforeSetChangellyExchangeCtx
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterSetChangellyExchangeCtx
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterSetChangellyExchangeCtxThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterSetChangellyExchangeCtxReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterSetChangellyExchangeCtxCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.beforeEachSetChangellyExchangeCtx
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEachSetChangellyExchangeCtx
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEachSetChangellyExchangeCtxReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.before_ChangellyExchangePartial
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangePartial
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangePartialThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangePartialReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangePartialCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.before_ChangellyExchangeView
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangeView
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangeViewThrows
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangeViewReturns
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.after_ChangellyExchangeViewCancels
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.beforeEach_ChangellyExchangeView
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEach_ChangellyExchangeView
/** @type {number} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.afterEach_ChangellyExchangeViewReturns
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.viewChangellyExchange = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.getChangellyExchange = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.setChangellyExchangeCtx = function() {}
/** @return {?} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.ChangellyExchangePartial = function() {}
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.prototype.ChangellyExchangeView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller = function(...init) {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese} init */
xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller = function() {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller)|(!xyz.swapee.rc.IChangellyExchangePageInstaller|typeof xyz.swapee.rc.ChangellyExchangePageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller)|(!xyz.swapee.rc.IChangellyExchangePageInstaller|typeof xyz.swapee.rc.ChangellyExchangePageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller|typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller)|(!xyz.swapee.rc.IChangellyExchangePageInstaller|typeof xyz.swapee.rc.ChangellyExchangePageInstaller))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspectsInstaller}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspectsInstaller.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewAspectsInstallerConstructor  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller, ...!xyz.swapee.rc.IChangellyExchangePageViewAspectsInstaller.Initialese)} */
xyz.swapee.rc.ChangellyExchangePageViewAspectsInstallerConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.ViewChangellyExchangeNArgs  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ ctx: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, form: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form }} */
xyz.swapee.rc.IChangellyExchangePageView.ViewChangellyExchangeNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePageView.ViewChangellyExchangeNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePageView.ViewChangellyExchangeNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeViewChangellyExchangePointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData = function() {}
/** @type {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterViewChangellyExchangePointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsViewChangellyExchangePointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData = function() {}
/** @type {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsViewChangellyExchangePointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ViewChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsViewChangellyExchangePointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.GetChangellyExchangeNArgs  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ ctx: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, form: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form }} */
xyz.swapee.rc.IChangellyExchangePageView.GetChangellyExchangeNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePageView.GetChangellyExchangeNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePageView.GetChangellyExchangeNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeGetChangellyExchangePointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData = function() {}
/** @type {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterGetChangellyExchangePointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsGetChangellyExchangePointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData = function() {}
/** @type {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsGetChangellyExchangePointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.GetChangellyExchangePointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsGetChangellyExchangePointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.SetChangellyExchangeCtxNArgs  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ answers: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, translation: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation }} */
xyz.swapee.rc.IChangellyExchangePageView.SetChangellyExchangeCtxNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePageView.SetChangellyExchangeCtxNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePageView.SetChangellyExchangeCtxNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeSetChangellyExchangeCtxPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData = function() {}
/** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterSetChangellyExchangeCtxPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsSetChangellyExchangeCtxPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData = function() {}
/** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData.prototype.res
/**
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsSetChangellyExchangeCtxPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.SetChangellyExchangeCtxPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsSetChangellyExchangeCtxPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartialNArgs  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ answers: !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, errors: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors, actions: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions, translation: !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation }} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartialNArgs

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartialNArgs, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData.prototype.cond
/**
 * @param {xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartialNArgs} args
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {engineering.type.VNode} value
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangePartialPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData = function() {}
/** @type {engineering.type.VNode} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangePartialPointcutData.prototype.res

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangePartialPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData = function() {}
/** @type {engineering.type.VNode} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData.prototype.res
/**
 * @param {engineering.type.VNode} value
 * @return {?}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangePartialPointcutData.prototype.sub = function(value) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangePartialPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangePartialPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.BeforeChangellyExchangeViewPointcutData.prototype.cancel = function(reason) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterChangellyExchangeViewPointcutData = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData = function() {}
/** @type {!Error} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData.prototype.err
/** @return {void} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterThrowsChangellyExchangeViewPointcutData.prototype.hide = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterReturnsChangellyExchangeViewPointcutData = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.ChangellyExchangeViewPointcutData}
 */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData = function() {}
/** @type {!Set<string>} */
xyz.swapee.rc.IChangellyExchangePageViewJoinpointModel.AfterCancelsChangellyExchangeViewPointcutData.prototype.reasons

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewConstructor  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageView, ...!xyz.swapee.rc.IChangellyExchangePageView.Initialese)} */
xyz.swapee.rc.ChangellyExchangePageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewMetaUniversal  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {symbol} */
xyz.swapee.rc.ChangellyExchangePageViewMetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageView.Initialese  99f69c6e23bc807aae3610aad583bb36 */
/** @record */
xyz.swapee.rc.UChangellyExchangePageView.Initialese = function() {}
/** @type {xyz.swapee.rc.IChangellyExchangePageView|undefined} */
xyz.swapee.rc.UChangellyExchangePageView.Initialese.prototype.changellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewUniversal  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.UChangellyExchangePageView.Initialese} init
 * @implements {xyz.swapee.rc.UChangellyExchangePageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.UChangellyExchangePageView.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePageViewUniversal = function(...init) {}
/** @param {...!xyz.swapee.rc.UChangellyExchangePageView.Initialese} init */
xyz.swapee.rc.ChangellyExchangePageViewUniversal.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.ChangellyExchangePageViewUniversal.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal = function() {}
/**
 * @param {...(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.UChangellyExchangePageView|typeof xyz.swapee.rc.UChangellyExchangePageView)} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewUniversal}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.MetaUniversal  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {xyz.swapee.rc.ChangellyExchangePageViewMetaUniversal} */
xyz.swapee.rc.AbstractChangellyExchangePageViewUniversal.MetaUniversal

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.UChangellyExchangePageViewConstructor  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {function(new: xyz.swapee.rc.UChangellyExchangePageView, ...!xyz.swapee.rc.UChangellyExchangePageView.Initialese)} */
xyz.swapee.rc.UChangellyExchangePageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.RecordUChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordUChangellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundUChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.UChangellyExchangePageViewFields}
 * @extends {xyz.swapee.rc.RecordUChangellyExchangePageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.UChangellyExchangePageViewCaster}
 */
xyz.swapee.rc.BoundUChangellyExchangePageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundChangellyExchangePageViewUniversal  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundUChangellyExchangePageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundChangellyExchangePageViewUniversal = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster
/** @type {!xyz.swapee.rc.BoundIChangellyExchangePageView} */
xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster.prototype.asIChangellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BChangellyExchangePageViewAspects  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @extends {xyz.swapee.rc.BChangellyExchangePageViewAspectsCaster<THIS>}
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.BChangellyExchangePageViewAspects = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese  99f69c6e23bc807aae3610aad583bb36 */
/** @record */
xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster
/** @type {!xyz.swapee.rc.BoundIChangellyExchangePageView} */
xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster.prototype.asIChangellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewAspects  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewAspectsCaster}
 * @extends {xyz.swapee.rc.BChangellyExchangePageViewAspects<!xyz.swapee.rc.IChangellyExchangePageViewAspects>}
 * @extends {xyz.swapee.rc.IChangellyExchangePage}
 * @extends {_idio.IRedirectMod}
 * @extends {xyz.swapee.rc.BChangellyExchangePageAspects}
 */
xyz.swapee.rc.IChangellyExchangePageViewAspects = function() {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese} init */
xyz.swapee.rc.IChangellyExchangePageViewAspects.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewAspects  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese} init
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangePageViewAspects = function(...init) {}
/** @param {...!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese} init */
xyz.swapee.rc.ChangellyExchangePageViewAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.ChangellyExchangePageViewAspects.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractChangellyExchangePageViewAspects  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects = function() {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageViewAspects|typeof xyz.swapee.rc.ChangellyExchangePageViewAspects)|(!xyz.swapee.rc.BChangellyExchangePageViewAspects|typeof xyz.swapee.rc.BChangellyExchangePageViewAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageViewAspects|typeof xyz.swapee.rc.ChangellyExchangePageViewAspects)|(!xyz.swapee.rc.BChangellyExchangePageViewAspects|typeof xyz.swapee.rc.BChangellyExchangePageViewAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangePageViewAspects|typeof xyz.swapee.rc.ChangellyExchangePageViewAspects)|(!xyz.swapee.rc.BChangellyExchangePageViewAspects|typeof xyz.swapee.rc.BChangellyExchangePageViewAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!_idio.IRedirectMod|typeof _idio.RedirectMod)|(!xyz.swapee.rc.BChangellyExchangePageAspects|typeof xyz.swapee.rc.BChangellyExchangePageAspects))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangePageViewAspects}
 */
xyz.swapee.rc.AbstractChangellyExchangePageViewAspects.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewAspectsConstructor  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {function(new: xyz.swapee.rc.IChangellyExchangePageViewAspects, ...!xyz.swapee.rc.IChangellyExchangePageViewAspects.Initialese)} */
xyz.swapee.rc.ChangellyExchangePageViewAspectsConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageView.Initialese}
 */
xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IHyperChangellyExchangePageViewCaster  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.IHyperChangellyExchangePageViewCaster
/** @type {!xyz.swapee.rc.BoundIHyperChangellyExchangePageView} */
xyz.swapee.rc.IHyperChangellyExchangePageViewCaster.prototype.asIHyperChangellyExchangePageView
/** @type {!xyz.swapee.rc.BoundHyperChangellyExchangePageView} */
xyz.swapee.rc.IHyperChangellyExchangePageViewCaster.prototype.superHyperChangellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IHyperChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IHyperChangellyExchangePageViewCaster}
 * @extends {xyz.swapee.rc.IChangellyExchangePageView}
 */
xyz.swapee.rc.IHyperChangellyExchangePageView = function() {}
/** @param {...!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese} init */
xyz.swapee.rc.IHyperChangellyExchangePageView.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.HyperChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese} init
 * @implements {xyz.swapee.rc.IHyperChangellyExchangePageView}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese>}
 */
xyz.swapee.rc.HyperChangellyExchangePageView = function(...init) {}
/** @param {...!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese} init */
xyz.swapee.rc.HyperChangellyExchangePageView.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.HyperChangellyExchangePageView.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.AbstractHyperChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @extends {xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView = function() {}
/**
 * @param {...((!xyz.swapee.rc.IHyperChangellyExchangePageView|typeof xyz.swapee.rc.HyperChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperChangellyExchangePageView|typeof xyz.swapee.rc.HyperChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IHyperChangellyExchangePageView|typeof xyz.swapee.rc.HyperChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageView|typeof xyz.swapee.rc.ChangellyExchangePageView)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.HyperChangellyExchangePageView}
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.__trait = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.rc.IChangellyExchangePageViewAspects|!Array<!xyz.swapee.rc.IChangellyExchangePageViewAspects>|function(new: xyz.swapee.rc.IChangellyExchangePageViewAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof xyz.swapee.rc.AbstractHyperChangellyExchangePageView}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractHyperChangellyExchangePageView.installs = function(...aspectsInstallers) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.HyperChangellyExchangePageViewConstructor  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {function(new: xyz.swapee.rc.IHyperChangellyExchangePageView, ...!xyz.swapee.rc.IHyperChangellyExchangePageView.Initialese)} */
xyz.swapee.rc.HyperChangellyExchangePageViewConstructor

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.RecordIHyperChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.rc.RecordIHyperChangellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundIHyperChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.RecordIHyperChangellyExchangePageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IHyperChangellyExchangePageViewCaster}
 */
xyz.swapee.rc.BoundIHyperChangellyExchangePageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundHyperChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIHyperChangellyExchangePageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundHyperChangellyExchangePageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/** @interface */
xyz.swapee.rc.IChangellyExchangePageViewHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView._viewChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._viewChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewHyperslice.prototype.viewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView._getChangellyExchange|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._getChangellyExchange>)} */
xyz.swapee.rc.IChangellyExchangePageViewHyperslice.prototype.getChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView._setChangellyExchangeCtx|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._setChangellyExchangeCtx>)} */
xyz.swapee.rc.IChangellyExchangePageViewHyperslice.prototype.setChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangeView|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangeView>)} */
xyz.swapee.rc.IChangellyExchangePageViewHyperslice.prototype.ChangellyExchangeView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewHyperslice}
 */
xyz.swapee.rc.ChangellyExchangePageViewHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @interface
 * @template THIS
 */
xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice = function() {}
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice.prototype.viewChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice.prototype.getChangellyExchange
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice.prototype.setChangellyExchangeCtx
/** @type {(!xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView<THIS>>)} */
xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice.prototype.ChangellyExchangeView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.ChangellyExchangePageViewBindingHyperslice  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @constructor
 * @implements {xyz.swapee.rc.IChangellyExchangePageViewBindingHyperslice<THIS>}
 * @template THIS
 */
xyz.swapee.rc.ChangellyExchangePageViewBindingHyperslice = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.RecordIChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/** @typedef {{ viewChangellyExchange: xyz.swapee.rc.IChangellyExchangePageView.viewChangellyExchange, getChangellyExchange: xyz.swapee.rc.IChangellyExchangePageView.getChangellyExchange, setChangellyExchangeCtx: xyz.swapee.rc.IChangellyExchangePageView.setChangellyExchangeCtx, ChangellyExchangePartial: xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial, ChangellyExchangeView: xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView }} */
xyz.swapee.rc.RecordIChangellyExchangePageView

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundIChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewFields}
 * @extends {xyz.swapee.rc.RecordIChangellyExchangePageView}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewCaster}
 * @extends {_idio.BoundIRedirectMod}
 */
xyz.swapee.rc.BoundIChangellyExchangePageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.BoundChangellyExchangePageView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.BoundIChangellyExchangePageView}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.rc.BoundChangellyExchangePageView = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.viewChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form
 * @return {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView}
 */
$$xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange = function(ctx, form) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form): !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageView.viewChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageView, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form): !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageView._viewChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageView.__viewChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageView,$$xyz.swapee.rc.IChangellyExchangePageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.getChangellyExchange  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx} ctx
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} form
 * @return {(undefined|!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView)}
 */
$$xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange = function(ctx, form) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form): (undefined|!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView)} */
xyz.swapee.rc.IChangellyExchangePageView.getChangellyExchange
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageView, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Ctx, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form): (undefined|!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView)} */
xyz.swapee.rc.IChangellyExchangePageView._getChangellyExchange
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange} */
xyz.swapee.rc.IChangellyExchangePageView.__getChangellyExchange

// nss:xyz.swapee.rc.IChangellyExchangePageView,$$xyz.swapee.rc.IChangellyExchangePageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.setChangellyExchangeCtx  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation
 * @return {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx}
 */
$$xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx = function(answers, translation) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation): !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} */
xyz.swapee.rc.IChangellyExchangePageView.setChangellyExchangeCtx
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageView, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation): !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.OptCtx} */
xyz.swapee.rc.IChangellyExchangePageView._setChangellyExchangeCtx
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx} */
xyz.swapee.rc.IChangellyExchangePageView.__setChangellyExchangeCtx

// nss:xyz.swapee.rc.IChangellyExchangePageView,$$xyz.swapee.rc.IChangellyExchangePageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions} actions
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation
 * @return {engineering.type.VNode}
 */
$$xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangePartial = function(answers, errors, actions, translation) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageView, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangePartial
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangePartial} */
xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangePartial

// nss:xyz.swapee.rc.IChangellyExchangePageView,$$xyz.swapee.rc.IChangellyExchangePageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers} answers
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors} errors
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions} actions
 * @param {!xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation} translation
 * @return {engineering.type.VNode}
 */
$$xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView = function(answers, errors, actions, translation) {}
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangeView
/** @typedef {function(this: xyz.swapee.rc.IChangellyExchangePageView, !xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Answers, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions, !xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation): engineering.type.VNode} */
xyz.swapee.rc.IChangellyExchangePageView._ChangellyExchangeView
/** @typedef {typeof $$xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView} */
xyz.swapee.rc.IChangellyExchangePageView.__ChangellyExchangeView

// nss:xyz.swapee.rc.IChangellyExchangePageView,$$xyz.swapee.rc.IChangellyExchangePageView,xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.GET  99f69c6e23bc807aae3610aad583bb36 */
/** @record */
xyz.swapee.rc.IChangellyExchangePageView.GET = function() {}
/** @type {xyz.swapee.rc.ChangellyExchange.changellyExchangeNav} */
xyz.swapee.rc.IChangellyExchangePageView.GET.prototype.changellyExchange

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors  99f69c6e23bc807aae3610aad583bb36 */
/**
 * @record
 * @extends {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Errors}
 * @extends {xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Validation}
 */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Errors = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions  99f69c6e23bc807aae3610aad583bb36 */
/** @record */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions = function() {}
/** @type {xyz.swapee.rc.ChangellyExchange.changellyExchangeNav} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.changellyExchange
/** @type {!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._changellyExchange
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterGetFixedOffer
/** @type {!xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterGetFixedOffer
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterGetOffer
/** @type {!xyz.swapee.rc.ChangellyExchange.filterGetOfferNav} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterGetOffer
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterCreateTransaction
/** @type {!xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterCreateTransaction
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterGetTransaction
/** @type {!xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterGetTransaction
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterCreateFixedTransaction
/** @type {!xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterCreateFixedTransaction
/** @type {xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype._filterCheckPayment
/** @type {!xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav} */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Actions.prototype.filterCheckPayment

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/IChangellyExchangePageView.xml} xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation  99f69c6e23bc807aae3610aad583bb36 */
/** @record */
xyz.swapee.rc.IChangellyExchangePageView.ChangellyExchangePartial.Translation = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.changellyExchangeNav  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.changellyExchange.Form): void} */
xyz.swapee.rc.ChangellyExchange.changellyExchangeNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav = function() {}
/** @type {number} */
xyz.swapee.rc.ChangellyExchange.ChangellyExchangeNav.prototype.changellyExchange

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetFixedOffer.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterGetFixedOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.ChangellyExchange.FilterGetFixedOfferNav.prototype.filterGetFixedOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterGetOfferNav  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetOffer.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterGetOfferNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav = function() {}
/** @type {number} */
xyz.swapee.rc.ChangellyExchange.FilterGetOfferNav.prototype.filterGetOffer

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCreateTransaction.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterCreateTransactionNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav = function() {}
/** @type {number} */
xyz.swapee.rc.ChangellyExchange.FilterCreateTransactionNav.prototype.filterCreateTransaction

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterGetTransaction.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterGetTransactionNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav = function() {}
/** @type {number} */
xyz.swapee.rc.ChangellyExchange.FilterGetTransactionNav.prototype.filterGetTransaction

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCreateFixedTransaction.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterCreateFixedTransactionNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav = function() {}
/** @type {number} */
xyz.swapee.rc.ChangellyExchange.FilterCreateFixedTransactionNav.prototype.filterCreateFixedTransaction

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangePage.filterCheckPayment.Form): void} */
xyz.swapee.rc.ChangellyExchange.filterCheckPaymentNav

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav = function() {}
/** @type {number} */
xyz.swapee.rc.ChangellyExchange.FilterCheckPaymentNav.prototype.filterCheckPayment

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.changellyExchangeMethodsIds  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.changellyExchangeMethodsIds = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.changellyExchangeArcsIds  2b436b21c8628099c0dbc3690c5442fd */
/** @constructor */
xyz.swapee.rc.changellyExchangeArcsIds = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.getChangellyExchange  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(): void} */
xyz.swapee.rc.getChangellyExchange

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.changellyExchange  2b436b21c8628099c0dbc3690c5442fd */
/** @typedef {function(!xyz.swapee.rc.IChangellyExchangeImpl): !xyz.swapee.rc.IChangellyExchangePage} */
xyz.swapee.rc.changellyExchange

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.IChangellyExchangeImpl.Initialese  2b436b21c8628099c0dbc3690c5442fd */
/** @record */
xyz.swapee.rc.IChangellyExchangeImpl.Initialese = function() {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.IChangellyExchangeImpl  2b436b21c8628099c0dbc3690c5442fd */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.rc.IChangellyExchangePageAspects}
 * @extends {xyz.swapee.rc.IChangellyExchangePage}
 * @extends {xyz.swapee.rc.IChangellyExchangePageHyperslice}
 * @extends {xyz.swapee.rc.IChangellyExchangePageViewHyperslice}
 */
xyz.swapee.rc.IChangellyExchangeImpl = function() {}
/** @param {...!xyz.swapee.rc.IChangellyExchangeImpl.Initialese} init */
xyz.swapee.rc.IChangellyExchangeImpl.prototype.constructor = function(...init) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.ChangellyExchangeImpl  2b436b21c8628099c0dbc3690c5442fd */
/**
 * @constructor
 * @param {...!xyz.swapee.rc.IChangellyExchangeImpl.Initialese} init
 * @implements {xyz.swapee.rc.IChangellyExchangeImpl}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.rc.IChangellyExchangeImpl.Initialese>}
 */
xyz.swapee.rc.ChangellyExchangeImpl = function(...init) {}
/** @param {...!xyz.swapee.rc.IChangellyExchangeImpl.Initialese} init */
xyz.swapee.rc.ChangellyExchangeImpl.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.ChangellyExchangeImpl.__extend = function(...Extensions) {}

// nss:xyz.swapee.rc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange/design/api.xml} xyz.swapee.rc.AbstractChangellyExchangeImpl  2b436b21c8628099c0dbc3690c5442fd */
/**
 * @constructor
 * @extends {xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl = function() {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangeImpl|typeof xyz.swapee.rc.ChangellyExchangeImpl)|(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 * @nosideeffects
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.rc.AbstractChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangeImpl|typeof xyz.swapee.rc.ChangellyExchangeImpl)|(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.rc.IChangellyExchangeImpl|typeof xyz.swapee.rc.ChangellyExchangeImpl)|(!xyz.swapee.rc.IChangellyExchangePageAspects|typeof xyz.swapee.rc.ChangellyExchangePageAspects)|(!xyz.swapee.rc.IChangellyExchangePage|typeof xyz.swapee.rc.ChangellyExchangePage)|(!xyz.swapee.rc.IChangellyExchangePageHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageHyperslice)|(!xyz.swapee.rc.IChangellyExchangePageViewHyperslice|typeof xyz.swapee.rc.ChangellyExchangePageViewHyperslice))} Implementations
 * @return {typeof xyz.swapee.rc.ChangellyExchangeImpl}
 */
xyz.swapee.rc.AbstractChangellyExchangeImpl.__trait = function(...Implementations) {}

// nss:xyz.swapee.rc
/* @typal-end */