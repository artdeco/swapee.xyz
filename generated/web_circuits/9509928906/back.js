import '../../maurice/circuits/region-selector/RegionSelector.mvc/types/db/typology'
export * from '../../maurice/circuits/region-selector/RegionSelector.mvc/compile/module/back/back-lib'
import {
 AbstractRegionSelector,
 RegionSelectorPort,
 AbstractRegionSelectorController,
 RegionSelectorHtmlComponent,
 RegionSelectorBuffer,
 AbstractRegionSelectorComputer,
 RegionSelectorComputer,
 RegionSelectorController,
} from '../../maurice/circuits/region-selector/RegionSelector.mvc/compile/module/back/back-lib'

export default {
 95099289061:AbstractRegionSelector,
 95099289063:RegionSelectorPort,
 95099289064:AbstractRegionSelectorController,
 950992890610:RegionSelectorHtmlComponent,
 950992890611:RegionSelectorBuffer,
 950992890630:AbstractRegionSelectorComputer,
 950992890631:RegionSelectorComputer,
 950992890661:RegionSelectorController,
}