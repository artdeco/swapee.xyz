import '../../maurice/circuits/region-selector/RegionSelector.mvc/types/db/typology'
export * from '../../maurice/circuits/region-selector/RegionSelector.mvc/compile/module/element/element-lib'
import {
 AbstractRegionSelector,
 RegionSelectorPort,
 AbstractRegionSelectorController,
 RegionSelectorElement,
 RegionSelectorBuffer,
 AbstractRegionSelectorComputer,
 RegionSelectorController,
} from '../../maurice/circuits/region-selector/RegionSelector.mvc/compile/module/element/element-lib'

export default {
 95099289061:AbstractRegionSelector,
 95099289063:RegionSelectorPort,
 95099289064:AbstractRegionSelectorController,
 95099289068:RegionSelectorElement,
 950992890611:RegionSelectorBuffer,
 950992890630:AbstractRegionSelectorComputer,
 950992890661:RegionSelectorController,
}