import '../../maurice/circuits/region-selector/RegionSelector.mvc/types/db/typology'
export * from '../../maurice/circuits/region-selector/RegionSelector.mvc/compile/module/front/front-lib'
import {
 RegionSelectorDisplay,
 RegionSelectorScreen,
} from '../../maurice/circuits/region-selector/RegionSelector.mvc/compile/module/front/front-lib'

export default {
 950992890641:RegionSelectorDisplay,
 950992890671:RegionSelectorScreen,
}