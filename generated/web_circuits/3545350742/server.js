import '../../maurice/circuits/crypto-select/CryptoSelect.mvc/types/db/typology'
export * from '../../maurice/circuits/crypto-select/CryptoSelect.mvc/compile/module/element/element-lib'
import {
 AbstractCryptoSelect,
 CryptoSelectPort,
 AbstractCryptoSelectController,
 CryptoSelectElement,
 CryptoSelectBuffer,
 AbstractCryptoSelectComputer,
 CryptoSelectComputer,
 CryptoSelectController,
} from '../../maurice/circuits/crypto-select/CryptoSelect.mvc/compile/module/element/element-lib'

export default {
 35453507421:AbstractCryptoSelect,
 35453507423:CryptoSelectPort,
 35453507424:AbstractCryptoSelectController,
 35453507428:CryptoSelectElement,
 354535074211:CryptoSelectBuffer,
 354535074230:AbstractCryptoSelectComputer,
 354535074231:CryptoSelectComputer,
 354535074261:CryptoSelectController,
}