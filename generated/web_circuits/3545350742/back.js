import '../../maurice/circuits/crypto-select/CryptoSelect.mvc/types/db/typology'
export * from '../../maurice/circuits/crypto-select/CryptoSelect.mvc/compile/module/back/back-lib'
import {
 AbstractCryptoSelect,
 CryptoSelectPort,
 AbstractCryptoSelectController,
 CryptoSelectHtmlComponent,
 CryptoSelectBuffer,
 AbstractCryptoSelectComputer,
 CryptoSelectComputer,
 CryptoSelectProcessor,
 CryptoSelectController,
} from '../../maurice/circuits/crypto-select/CryptoSelect.mvc/compile/module/back/back-lib'

export default {
 35453507421:AbstractCryptoSelect,
 35453507423:CryptoSelectPort,
 35453507424:AbstractCryptoSelectController,
 354535074210:CryptoSelectHtmlComponent,
 354535074211:CryptoSelectBuffer,
 354535074230:AbstractCryptoSelectComputer,
 354535074231:CryptoSelectComputer,
 354535074251:CryptoSelectProcessor,
 354535074261:CryptoSelectController,
}