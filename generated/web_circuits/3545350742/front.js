import '../../maurice/circuits/crypto-select/CryptoSelect.mvc/types/db/typology'
export * from '../../maurice/circuits/crypto-select/CryptoSelect.mvc/compile/module/front/front-lib'
import {
 CryptoSelectDisplay,
 CryptoSelectTouchscreen,
} from '../../maurice/circuits/crypto-select/CryptoSelect.mvc/compile/module/front/front-lib'

export default {
 354535074241:CryptoSelectDisplay,
 354535074271:CryptoSelectTouchscreen,
}