import '../../maurice/circuits/motion-path/MotionPath.mvc/types/db/typology'
export * from '../../maurice/circuits/motion-path/MotionPath.mvc/compile/module/element/element-lib'
import {
 AbstractMotionPath,
 MotionPathPort,
 AbstractMotionPathController,
 MotionPathElement,
 MotionPathBuffer,
 AbstractMotionPathComputer,
 MotionPathController,
} from '../../maurice/circuits/motion-path/MotionPath.mvc/compile/module/element/element-lib'

export default {
 32099806271:AbstractMotionPath,
 32099806273:MotionPathPort,
 32099806274:AbstractMotionPathController,
 32099806278:MotionPathElement,
 320998062711:MotionPathBuffer,
 320998062730:AbstractMotionPathComputer,
 320998062761:MotionPathController,
}