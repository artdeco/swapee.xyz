import '../../maurice/circuits/motion-path/MotionPath.mvc/types/db/typology'
export * from '../../maurice/circuits/motion-path/MotionPath.mvc/compile/module/front/front-lib'
import {
 MotionPathDisplay,
 MotionPathScreen,
} from '../../maurice/circuits/motion-path/MotionPath.mvc/compile/module/front/front-lib'

export default {
 320998062741:MotionPathDisplay,
 320998062771:MotionPathScreen,
}