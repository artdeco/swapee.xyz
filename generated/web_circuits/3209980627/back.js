import '../../maurice/circuits/motion-path/MotionPath.mvc/types/db/typology'
export * from '../../maurice/circuits/motion-path/MotionPath.mvc/compile/module/back/back-lib'
import {
 AbstractMotionPath,
 MotionPathPort,
 AbstractMotionPathController,
 MotionPathHtmlComponent,
 MotionPathBuffer,
 AbstractMotionPathComputer,
 MotionPathController,
} from '../../maurice/circuits/motion-path/MotionPath.mvc/compile/module/back/back-lib'

export default {
 32099806271:AbstractMotionPath,
 32099806273:MotionPathPort,
 32099806274:AbstractMotionPathController,
 320998062710:MotionPathHtmlComponent,
 320998062711:MotionPathBuffer,
 320998062730:AbstractMotionPathComputer,
 320998062761:MotionPathController,
}