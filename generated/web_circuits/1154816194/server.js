import '../../maurice/circuits/offers-table/OffersTable.mvc/types/db/typology'
export * from '../../maurice/circuits/offers-table/OffersTable.mvc/compile/module/element/element-lib'
import {
 AbstractOffersTable,
 OffersTablePort,
 AbstractOffersTableController,
 OffersTableElement,
 OffersTableBuffer,
 AbstractOffersTableComputer,
 OffersTableController,
} from '../../maurice/circuits/offers-table/OffersTable.mvc/compile/module/element/element-lib'

export default {
 11548161941:AbstractOffersTable,
 11548161943:OffersTablePort,
 11548161944:AbstractOffersTableController,
 11548161948:OffersTableElement,
 115481619411:OffersTableBuffer,
 115481619430:AbstractOffersTableComputer,
 115481619461:OffersTableController,
}