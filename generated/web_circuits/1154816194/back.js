import '../../maurice/circuits/offers-table/OffersTable.mvc/types/db/typology'
export * from '../../maurice/circuits/offers-table/OffersTable.mvc/compile/module/back/back-lib'
import {
 AbstractOffersTable,
 OffersTablePort,
 AbstractOffersTableController,
 OffersTableHtmlComponent,
 OffersTableBuffer,
 AbstractOffersTableComputer,
 OffersTableComputer,
 OffersTableProcessor,
 OffersTableController,
} from '../../maurice/circuits/offers-table/OffersTable.mvc/compile/module/back/back-lib'

export default {
 11548161941:AbstractOffersTable,
 11548161943:OffersTablePort,
 11548161944:AbstractOffersTableController,
 115481619410:OffersTableHtmlComponent,
 115481619411:OffersTableBuffer,
 115481619430:AbstractOffersTableComputer,
 115481619431:OffersTableComputer,
 115481619451:OffersTableProcessor,
 115481619461:OffersTableController,
}