import '../../maurice/circuits/offers-table/OffersTable.mvc/types/db/typology'
export * from '../../maurice/circuits/offers-table/OffersTable.mvc/compile/module/front/front-lib'
import {
 OffersTableDisplay,
 OffersTableScreen,
} from '../../maurice/circuits/offers-table/OffersTable.mvc/compile/module/front/front-lib'

export default {
 115481619441:OffersTableDisplay,
 115481619471:OffersTableScreen,
}