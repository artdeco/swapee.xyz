import '../../maurice/circuits/offer-exchange/OfferExchange.mvc/types/db/typology'
export * from '../../maurice/circuits/offer-exchange/OfferExchange.mvc/compile/module/element/element-lib'
import {
 AbstractOfferExchange,
 OfferExchangePort,
 AbstractOfferExchangeController,
 OfferExchangeElement,
 OfferExchangeBuffer,
 AbstractOfferExchangeComputer,
 OfferExchangeController,
} from '../../maurice/circuits/offer-exchange/OfferExchange.mvc/compile/module/element/element-lib'

export default {
 65293396511:AbstractOfferExchange,
 65293396513:OfferExchangePort,
 65293396514:AbstractOfferExchangeController,
 65293396518:OfferExchangeElement,
 652933965111:OfferExchangeBuffer,
 652933965130:AbstractOfferExchangeComputer,
 652933965161:OfferExchangeController,
}