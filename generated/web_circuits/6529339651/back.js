import '../../maurice/circuits/offer-exchange/OfferExchange.mvc/types/db/typology'
export * from '../../maurice/circuits/offer-exchange/OfferExchange.mvc/compile/module/back/back-lib'
import {
 AbstractOfferExchange,
 OfferExchangePort,
 AbstractOfferExchangeController,
 OfferExchangeHtmlComponent,
 OfferExchangeBuffer,
 AbstractOfferExchangeComputer,
 OfferExchangeComputer,
 OfferExchangeController,
} from '../../maurice/circuits/offer-exchange/OfferExchange.mvc/compile/module/back/back-lib'

export default {
 65293396511:AbstractOfferExchange,
 65293396513:OfferExchangePort,
 65293396514:AbstractOfferExchangeController,
 652933965110:OfferExchangeHtmlComponent,
 652933965111:OfferExchangeBuffer,
 652933965130:AbstractOfferExchangeComputer,
 652933965131:OfferExchangeComputer,
 652933965161:OfferExchangeController,
}