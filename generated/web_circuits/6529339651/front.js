import '../../maurice/circuits/offer-exchange/OfferExchange.mvc/types/db/typology'
export * from '../../maurice/circuits/offer-exchange/OfferExchange.mvc/compile/module/front/front-lib'
import {
 OfferExchangeDisplay,
 OfferExchangeTouchscreen,
} from '../../maurice/circuits/offer-exchange/OfferExchange.mvc/compile/module/front/front-lib'

export default {
 652933965141:OfferExchangeDisplay,
 652933965171:OfferExchangeTouchscreen,
}