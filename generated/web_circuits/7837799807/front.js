import '../../maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/types/db/typology'
export * from '../../maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/compile/module/front/front-lib'
import {
 ExchangeStatusTitleDisplay,
 ExchangeStatusTitleScreen,
} from '../../maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/compile/module/front/front-lib'

export default {
 783779980741:ExchangeStatusTitleDisplay,
 783779980771:ExchangeStatusTitleScreen,
}