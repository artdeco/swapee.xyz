import '../../maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/types/db/typology'
export * from '../../maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/compile/module/element/element-lib'
import {
 AbstractSwapeeSocialButtons,
 SwapeeSocialButtonsPort,
 AbstractSwapeeSocialButtonsController,
 SwapeeSocialButtonsElement,
 SwapeeSocialButtonsBuffer,
 AbstractSwapeeSocialButtonsComputer,
 SwapeeSocialButtonsController,
} from '../../maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/compile/module/element/element-lib'

export default {
 94225766191:AbstractSwapeeSocialButtons,
 94225766193:SwapeeSocialButtonsPort,
 94225766194:AbstractSwapeeSocialButtonsController,
 94225766198:SwapeeSocialButtonsElement,
 942257661911:SwapeeSocialButtonsBuffer,
 942257661930:AbstractSwapeeSocialButtonsComputer,
 942257661961:SwapeeSocialButtonsController,
}