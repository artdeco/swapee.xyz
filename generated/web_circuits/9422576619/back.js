import '../../maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/types/db/typology'
export * from '../../maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/compile/module/back/back-lib'
import {
 AbstractSwapeeSocialButtons,
 SwapeeSocialButtonsPort,
 AbstractSwapeeSocialButtonsController,
 SwapeeSocialButtonsHtmlComponent,
 SwapeeSocialButtonsBuffer,
 AbstractSwapeeSocialButtonsComputer,
 SwapeeSocialButtonsController,
} from '../../maurice/circuits/swapee-social-buttons/SwapeeSocialButtons.mvc/compile/module/back/back-lib'

export default {
 94225766191:AbstractSwapeeSocialButtons,
 94225766193:SwapeeSocialButtonsPort,
 94225766194:AbstractSwapeeSocialButtonsController,
 942257661910:SwapeeSocialButtonsHtmlComponent,
 942257661911:SwapeeSocialButtonsBuffer,
 942257661930:AbstractSwapeeSocialButtonsComputer,
 942257661961:SwapeeSocialButtonsController,
}