import '../../maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/types/db/typology'
export * from '../../maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/compile/module/front/front-lib'
import {
 OfferExchangeInfoRowDisplay,
 OfferExchangeInfoRowScreen,
} from '../../maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/compile/module/front/front-lib'

export default {
 871188356541:OfferExchangeInfoRowDisplay,
 871188356571:OfferExchangeInfoRowScreen,
}