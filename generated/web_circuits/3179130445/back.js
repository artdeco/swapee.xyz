import '../../maurice/circuits/swapee-button/SwapeeButton.mvc/types/db/typology'
export * from '../../maurice/circuits/swapee-button/SwapeeButton.mvc/compile/module/back/back-lib'
import {
 AbstractSwapeeButton,
 SwapeeButtonPort,
 AbstractSwapeeButtonController,
 SwapeeButtonHtmlComponent,
 SwapeeButtonBuffer,
 AbstractSwapeeButtonComputer,
 SwapeeButtonController,
} from '../../maurice/circuits/swapee-button/SwapeeButton.mvc/compile/module/back/back-lib'

export default {
 31791304451:AbstractSwapeeButton,
 31791304453:SwapeeButtonPort,
 31791304454:AbstractSwapeeButtonController,
 317913044510:SwapeeButtonHtmlComponent,
 317913044511:SwapeeButtonBuffer,
 317913044530:AbstractSwapeeButtonComputer,
 317913044561:SwapeeButtonController,
}