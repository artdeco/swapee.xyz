import '../../maurice/circuits/testing/Testing.mvc/types/db/typology'
export * from '../../maurice/circuits/testing/Testing.mvc/compile/module/element/element-lib'
import {
 AbstractTesting,
 TestingPort,
 AbstractTestingController,
 TestingElement,
 TestingBuffer,
 AbstractTestingComputer,
 TestingController,
} from '../../maurice/circuits/testing/Testing.mvc/compile/module/element/element-lib'

export default {
 62696530441:AbstractTesting,
 62696530443:TestingPort,
 62696530444:AbstractTestingController,
 62696530448:TestingElement,
 626965304411:TestingBuffer,
 626965304430:AbstractTestingComputer,
 626965304461:TestingController,
}