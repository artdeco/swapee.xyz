import '../../maurice/circuits/transaction-info/TransactionInfo.mvc/types/db/typology'
export * from '../../maurice/circuits/transaction-info/TransactionInfo.mvc/compile/module/element/element-lib'
import {
 AbstractTransactionInfo,
 TransactionInfoPort,
 AbstractTransactionInfoController,
 TransactionInfoElement,
 TransactionInfoBuffer,
 AbstractTransactionInfoComputer,
 TransactionInfoController,
} from '../../maurice/circuits/transaction-info/TransactionInfo.mvc/compile/module/element/element-lib'

export default {
 19880518191:AbstractTransactionInfo,
 19880518193:TransactionInfoPort,
 19880518194:AbstractTransactionInfoController,
 19880518198:TransactionInfoElement,
 198805181911:TransactionInfoBuffer,
 198805181930:AbstractTransactionInfoComputer,
 198805181961:TransactionInfoController,
}