import '../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/types/db/typology'
export * from '../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/compile/module/front/front-lib'
import {
 ChangellyExchangeDisplay,
 ChangellyExchangeScreen,
} from '../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/compile/module/front/front-lib'

export default {
 201339238041:ChangellyExchangeDisplay,
 201339238071:ChangellyExchangeScreen,
}