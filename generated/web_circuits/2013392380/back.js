import '../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/types/db/typology'
export * from '../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/compile/module/back/back-lib'
import {
 AbstractChangellyExchange,
 ChangellyExchangePort,
 AbstractChangellyExchangeController,
 ChangellyExchangeHtmlComponent,
 ChangellyExchangeBuffer,
 AbstractChangellyExchangeComputer,
 ChangellyExchangeComputer,
 ChangellyExchangeController,
} from '../../maurice/circuits/changelly-exchange/ChangellyExchange.mvc/compile/module/back/back-lib'

export default {
 20133923801:AbstractChangellyExchange,
 20133923803:ChangellyExchangePort,
 20133923804:AbstractChangellyExchangeController,
 201339238010:ChangellyExchangeHtmlComponent,
 201339238011:ChangellyExchangeBuffer,
 201339238030:AbstractChangellyExchangeComputer,
 201339238031:ChangellyExchangeComputer,
 201339238061:ChangellyExchangeController,
}