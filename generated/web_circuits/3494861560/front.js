import '../../maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/types/db/typology'
export * from '../../maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/compile/module/front/front-lib'
import {
 AggregatorPageCircuitDisplay,
 AggregatorPageCircuitScreen,
} from '../../maurice/circuits/aggregator-page-circuit/AggregatorPageCircuit.mvc/compile/module/front/front-lib'

export default {
 349486156041:AggregatorPageCircuitDisplay,
 349486156071:AggregatorPageCircuitScreen,
}