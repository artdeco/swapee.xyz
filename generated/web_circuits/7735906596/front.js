import '../../maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/types/db/typology'
export * from '../../maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/compile/module/front/front-lib'
import {
 ExchangePageCircuitDisplay,
 ExchangePageCircuitScreen,
} from '../../maurice/circuits/exchange-page-circuit/ExchangePageCircuit.mvc/compile/module/front/front-lib'

export default {
 773590659641:ExchangePageCircuitDisplay,
 773590659671:ExchangePageCircuitScreen,
}