import '../../maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/types/db/typology'
export * from '../../maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/compile/module/front/front-lib'
import {
 ExchangeIdRowDisplay,
 ExchangeIdRowScreen,
} from '../../maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/compile/module/front/front-lib'

export default {
 109947522741:ExchangeIdRowDisplay,
 109947522771:ExchangeIdRowScreen,
}