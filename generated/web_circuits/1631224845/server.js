import '../../maurice/circuits/region-ping/RegionPing.mvc/types/db/typology'
export * from '../../maurice/circuits/region-ping/RegionPing.mvc/compile/module/element/element-lib'
import {
 AbstractRegionPing,
 RegionPingPort,
 AbstractRegionPingController,
 RegionPingElement,
 RegionPingBuffer,
 AbstractRegionPingComputer,
 RegionPingController,
} from '../../maurice/circuits/region-ping/RegionPing.mvc/compile/module/element/element-lib'

export default {
 16312248451:AbstractRegionPing,
 16312248453:RegionPingPort,
 16312248454:AbstractRegionPingController,
 16312248458:RegionPingElement,
 163122484511:RegionPingBuffer,
 163122484530:AbstractRegionPingComputer,
 163122484561:RegionPingController,
}