import '../../maurice/circuits/region-ping/RegionPing.mvc/types/db/typology'
export * from '../../maurice/circuits/region-ping/RegionPing.mvc/compile/module/front/front-lib'
import {
 RegionPingDisplay,
 RegionPingScreen,
} from '../../maurice/circuits/region-ping/RegionPing.mvc/compile/module/front/front-lib'

export default {
 163122484541:RegionPingDisplay,
 163122484571:RegionPingScreen,
}