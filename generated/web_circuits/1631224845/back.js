import '../../maurice/circuits/region-ping/RegionPing.mvc/types/db/typology'
export * from '../../maurice/circuits/region-ping/RegionPing.mvc/compile/module/back/back-lib'
import {
 AbstractRegionPing,
 RegionPingPort,
 AbstractRegionPingController,
 RegionPingHtmlComponent,
 RegionPingBuffer,
 AbstractRegionPingComputer,
 RegionPingComputer,
 RegionPingController,
} from '../../maurice/circuits/region-ping/RegionPing.mvc/compile/module/back/back-lib'

export default {
 16312248451:AbstractRegionPing,
 16312248453:RegionPingPort,
 16312248454:AbstractRegionPingController,
 163122484510:RegionPingHtmlComponent,
 163122484511:RegionPingBuffer,
 163122484530:AbstractRegionPingComputer,
 163122484531:RegionPingComputer,
 163122484561:RegionPingController,
}