import '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/types/db/typology'
export * from '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/compile/module/front/front-lib'
import {
 OffersAggregatorDisplay,
 OffersAggregatorScreen,
} from '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/compile/module/front/front-lib'

export default {
 489008875741:OffersAggregatorDisplay,
 489008875771:OffersAggregatorScreen,
}