import '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/types/db/typology'
export * from '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/compile/module/element/element-lib'
import {
 AbstractOffersAggregator,
 OffersAggregatorPort,
 AbstractOffersAggregatorController,
 OffersAggregatorElement,
 OffersAggregatorBuffer,
 AbstractOffersAggregatorComputer,
 OffersAggregatorController,
} from '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/compile/module/element/element-lib'

export default {
 48900887571:AbstractOffersAggregator,
 48900887573:OffersAggregatorPort,
 48900887574:AbstractOffersAggregatorController,
 48900887578:OffersAggregatorElement,
 489008875711:OffersAggregatorBuffer,
 489008875730:AbstractOffersAggregatorComputer,
 489008875761:OffersAggregatorController,
}