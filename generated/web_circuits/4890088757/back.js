import '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/types/db/typology'
export * from '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/compile/module/back/back-lib'
import {
 AbstractOffersAggregator,
 OffersAggregatorPort,
 AbstractOffersAggregatorController,
 OffersAggregatorHtmlComponent,
 OffersAggregatorBuffer,
 AbstractOffersAggregatorComputer,
 OffersAggregatorComputer,
 OffersAggregatorController,
} from '../../maurice/circuits/offers-aggregator/OffersAggregator.mvc/compile/module/back/back-lib'

export default {
 48900887571:AbstractOffersAggregator,
 48900887573:OffersAggregatorPort,
 48900887574:AbstractOffersAggregatorController,
 489008875710:OffersAggregatorHtmlComponent,
 489008875711:OffersAggregatorBuffer,
 489008875730:AbstractOffersAggregatorComputer,
 489008875731:OffersAggregatorComputer,
 489008875761:OffersAggregatorController,
}