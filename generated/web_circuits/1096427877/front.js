import '../../maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/types/db/typology'
export * from '../../maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/compile/module/front/front-lib'
import {
 DealBrokerAggregatorAdapterDisplay,
 DealBrokerAggregatorAdapterScreen,
} from '../../maurice/circuits/deal-broker-aggregator-adapter/DealBrokerAggregatorAdapter.mvc/compile/module/front/front-lib'

export default {
 109642787741:DealBrokerAggregatorAdapterDisplay,
 109642787771:DealBrokerAggregatorAdapterScreen,
}