import '../../maurice/circuits/offer-row/OfferRow.mvc/types/db/typology'
export * from '../../maurice/circuits/offer-row/OfferRow.mvc/compile/module/element/element-lib'
import {
 AbstractOfferRow,
 OfferRowPort,
 AbstractOfferRowController,
 OfferRowElement,
 OfferRowBuffer,
 AbstractOfferRowComputer,
 OfferRowController,
} from '../../maurice/circuits/offer-row/OfferRow.mvc/compile/module/element/element-lib'

export default {
 80854338691:AbstractOfferRow,
 80854338693:OfferRowPort,
 80854338694:AbstractOfferRowController,
 80854338698:OfferRowElement,
 808543386911:OfferRowBuffer,
 808543386930:AbstractOfferRowComputer,
 808543386961:OfferRowController,
}