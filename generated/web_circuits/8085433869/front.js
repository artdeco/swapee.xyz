import '../../maurice/circuits/offer-row/OfferRow.mvc/types/db/typology'
export * from '../../maurice/circuits/offer-row/OfferRow.mvc/compile/module/front/front-lib'
import {
 OfferRowDisplay,
 OfferRowScreen,
} from '../../maurice/circuits/offer-row/OfferRow.mvc/compile/module/front/front-lib'

export default {
 808543386941:OfferRowDisplay,
 808543386971:OfferRowScreen,
}