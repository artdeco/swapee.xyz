import '../../maurice/circuits/offer-row/OfferRow.mvc/types/db/typology'
export * from '../../maurice/circuits/offer-row/OfferRow.mvc/compile/module/back/back-lib'
import {
 AbstractOfferRow,
 OfferRowPort,
 AbstractOfferRowController,
 OfferRowHtmlComponent,
 OfferRowBuffer,
 AbstractOfferRowComputer,
 OfferRowController,
} from '../../maurice/circuits/offer-row/OfferRow.mvc/compile/module/back/back-lib'

export default {
 80854338691:AbstractOfferRow,
 80854338693:OfferRowPort,
 80854338694:AbstractOfferRowController,
 808543386910:OfferRowHtmlComponent,
 808543386911:OfferRowBuffer,
 808543386930:AbstractOfferRowComputer,
 808543386961:OfferRowController,
}