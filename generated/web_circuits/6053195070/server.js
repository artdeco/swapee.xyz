import '../../maurice/circuits/type-writer/TypeWriter.mvc/types/db/typology'
export * from '../../maurice/circuits/type-writer/TypeWriter.mvc/compile/module/element/element-lib'
import {
 TypeWriterController,
 TypeWriterElement,
 TypeWriterPort,
 AbstractTypeWriterController,
 AbstractTypeWriterComputer,
 AbstractTypeWriter,
} from '../../maurice/circuits/type-writer/TypeWriter.mvc/compile/module/element/element-lib'

export default {
 605319507061:TypeWriterController,
 60531950708:TypeWriterElement,
 60531950703:TypeWriterPort,
 60531950704:AbstractTypeWriterController,
 605319507030:AbstractTypeWriterComputer,
 60531950701:AbstractTypeWriter,
}