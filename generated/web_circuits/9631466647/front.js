import '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/types/db/typology'
export * from '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/compile/module/front/front-lib'
import {
 BorderMotionPathDisplay,
 BorderMotionPathScreen,
} from '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/compile/module/front/front-lib'

export default {
 963146664741:BorderMotionPathDisplay,
 963146664771:BorderMotionPathScreen,
}