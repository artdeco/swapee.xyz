import '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/types/db/typology'
export * from '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/compile/module/element/element-lib'
import {
 AbstractBorderMotionPath,
 BorderMotionPathPort,
 AbstractBorderMotionPathController,
 BorderMotionPathElement,
 BorderMotionPathBuffer,
 AbstractBorderMotionPathComputer,
 BorderMotionPathController,
} from '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/compile/module/element/element-lib'

export default {
 96314666471:AbstractBorderMotionPath,
 96314666473:BorderMotionPathPort,
 96314666474:AbstractBorderMotionPathController,
 96314666478:BorderMotionPathElement,
 963146664711:BorderMotionPathBuffer,
 963146664730:AbstractBorderMotionPathComputer,
 963146664761:BorderMotionPathController,
}