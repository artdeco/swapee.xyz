import '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/types/db/typology'
export * from '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/compile/module/back/back-lib'
import {
 AbstractBorderMotionPath,
 BorderMotionPathPort,
 AbstractBorderMotionPathController,
 BorderMotionPathHtmlComponent,
 BorderMotionPathBuffer,
 AbstractBorderMotionPathComputer,
 BorderMotionPathComputer,
 BorderMotionPathController,
} from '../../maurice/circuits/border-motion-path/BorderMotionPath.mvc/compile/module/back/back-lib'

export default {
 96314666471:AbstractBorderMotionPath,
 96314666473:BorderMotionPathPort,
 96314666474:AbstractBorderMotionPathController,
 963146664710:BorderMotionPathHtmlComponent,
 963146664711:BorderMotionPathBuffer,
 963146664730:AbstractBorderMotionPathComputer,
 963146664731:BorderMotionPathComputer,
 963146664761:BorderMotionPathController,
}