import '../../maurice/circuits/exchange-intent/ExchangeIntent.mvc/types/db/typology'
export * from '../../maurice/circuits/exchange-intent/ExchangeIntent.mvc/compile/module/front/front-lib'
import {
 ExchangeIntentDisplay,
 ExchangeIntentScreen,
} from '../../maurice/circuits/exchange-intent/ExchangeIntent.mvc/compile/module/front/front-lib'

export default {
 783386804841:ExchangeIntentDisplay,
 783386804871:ExchangeIntentScreen,
}