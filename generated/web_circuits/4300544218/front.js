import '../../maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/types/db/typology'
export * from '../../maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/compile/module/front/front-lib'
import {
 TransactionInfoHeadDisplay,
 TransactionInfoHeadScreen,
} from '../../maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/compile/module/front/front-lib'

export default {
 430054421841:TransactionInfoHeadDisplay,
 430054421871:TransactionInfoHeadScreen,
}