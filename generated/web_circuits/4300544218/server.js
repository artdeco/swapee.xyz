import '../../maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/types/db/typology'
export * from '../../maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/compile/module/element/element-lib'
import {
 AbstractTransactionInfoHead,
 TransactionInfoHeadPort,
 AbstractTransactionInfoHeadController,
 TransactionInfoHeadElement,
 TransactionInfoHeadBuffer,
 AbstractTransactionInfoHeadComputer,
 TransactionInfoHeadController,
} from '../../maurice/circuits/transaction-info-head/TransactionInfoHead.mvc/compile/module/element/element-lib'

export default {
 43005442181:AbstractTransactionInfoHead,
 43005442183:TransactionInfoHeadPort,
 43005442184:AbstractTransactionInfoHeadController,
 43005442188:TransactionInfoHeadElement,
 430054421811:TransactionInfoHeadBuffer,
 430054421830:AbstractTransactionInfoHeadComputer,
 430054421861:TransactionInfoHeadController,
}