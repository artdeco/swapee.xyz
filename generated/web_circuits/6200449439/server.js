import '../../maurice/circuits/swapee-me/SwapeeMe.mvc/types/db/typology'
export * from '../../maurice/circuits/swapee-me/SwapeeMe.mvc/compile/module/element/element-lib'
import {
 AbstractSwapeeMe,
 SwapeeMePort,
 AbstractSwapeeMeController,
 SwapeeMeElement,
 SwapeeMeBuffer,
 AbstractSwapeeMeComputer,
 SwapeeMeController,
} from '../../maurice/circuits/swapee-me/SwapeeMe.mvc/compile/module/element/element-lib'

export default {
 62004494391:AbstractSwapeeMe,
 62004494393:SwapeeMePort,
 62004494394:AbstractSwapeeMeController,
 62004494398:SwapeeMeElement,
 620044943911:SwapeeMeBuffer,
 620044943930:AbstractSwapeeMeComputer,
 620044943961:SwapeeMeController,
}