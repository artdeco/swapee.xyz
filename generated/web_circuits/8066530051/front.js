import '../../maurice/circuits/offers-filter/OffersFilter.mvc/types/db/typology'
export * from '../../maurice/circuits/offers-filter/OffersFilter.mvc/compile/module/front/front-lib'
import {
 OffersFilterDisplay,
 OffersFilterScreen,
} from '../../maurice/circuits/offers-filter/OffersFilter.mvc/compile/module/front/front-lib'

export default {
 806653005141:OffersFilterDisplay,
 806653005171:OffersFilterScreen,
}