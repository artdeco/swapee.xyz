import '../../maurice/circuits/irq-test/IrqTest.mvc/types/db/typology'
export * from '../../maurice/circuits/irq-test/IrqTest.mvc/compile/module/element/element-lib'
import {
 AbstractIrqTest,
 IrqTestPort,
 AbstractIrqTestController,
 IrqTestElement,
 IrqTestBuffer,
 AbstractIrqTestComputer,
 IrqTestController,
} from '../../maurice/circuits/irq-test/IrqTest.mvc/compile/module/element/element-lib'

export default {
 29406750421:AbstractIrqTest,
 29406750423:IrqTestPort,
 29406750424:AbstractIrqTestController,
 29406750428:IrqTestElement,
 294067504211:IrqTestBuffer,
 294067504230:AbstractIrqTestComputer,
 294067504261:IrqTestController,
}