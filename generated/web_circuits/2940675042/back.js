import '../../maurice/circuits/irq-test/IrqTest.mvc/types/db/typology'
export * from '../../maurice/circuits/irq-test/IrqTest.mvc/compile/module/back/back-lib'
import {
 AbstractIrqTest,
 IrqTestPort,
 AbstractIrqTestController,
 IrqTestHtmlComponent,
 IrqTestBuffer,
 AbstractIrqTestComputer,
 IrqTestController,
} from '../../maurice/circuits/irq-test/IrqTest.mvc/compile/module/back/back-lib'

export default {
 29406750421:AbstractIrqTest,
 29406750423:IrqTestPort,
 29406750424:AbstractIrqTestController,
 294067504210:IrqTestHtmlComponent,
 294067504211:IrqTestBuffer,
 294067504230:AbstractIrqTestComputer,
 294067504261:IrqTestController,
}