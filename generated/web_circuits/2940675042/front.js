import '../../maurice/circuits/irq-test/IrqTest.mvc/types/db/typology'
export * from '../../maurice/circuits/irq-test/IrqTest.mvc/compile/module/front/front-lib'
import {
 IrqTestDisplay,
 IrqTestTouchscreen,
} from '../../maurice/circuits/irq-test/IrqTest.mvc/compile/module/front/front-lib'

export default {
 294067504241:IrqTestDisplay,
 294067504271:IrqTestTouchscreen,
}