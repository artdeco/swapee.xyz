import {writeFileSync,mkdirSync} from 'fs'
import {dirname, join, relative} from 'path'
import {kebabCase} from '@artdeco/crlf'

const db={
 // 'xyz.swapee.rc.testing':[
 //  'generated/maurice/circuits/testing/testing.ws',
 //  'generated/generated/maurice/circuits/testing/testing.ws/testing/Testing',
 // ],
 'xyz.swapee.rc.offers-aggregator':[
  'generated/maurice/circuits/offers-aggregator/offers-aggregator.ws',
  'generated/generated/maurice/circuits/offers-aggregator/offers-aggregator.ws/offers-aggregator/OffersAggregator',
 ],
 'xyz.swapee.rc.changelly-exchange':[
  'generated/maurice/circuits/changelly-exchange/changelly-exchange.ws',
  'generated/generated/maurice/circuits/changelly-exchange/changelly-exchange.ws/changelly-exchange/ChangellyExchange',
 ],
 'xyz.swapee.rc.file-tree':[
  'generated/maurice/circuits/file-tree/file-tree.ws',
  'generated/generated/maurice/circuits/file-tree/file-tree.ws/file-tree/FileTree',
 ],
 'xyz.swapee.rc.file-editor':[
  'generated/maurice/circuits/file-editor/file-editor.ws',
  'generated/generated/maurice/circuits/file-editor/file-editor.ws/file-editor/FileEditor',
 ],
}

for(const key in db) {
 const kName=kebabCase(key)
 const p=kName.split('.')
 const[N]=[...p].reverse()
 const[dev,prod]=db[key]

 const mainPath=join('node_modules',...p,'index.js')
 const modulePath=join('node_modules',...p,'module.js')
 const prodPath=join('node_modules',...p,'prod.js')
 const devPath=join('node_modules',...p,'dev.js')
 const packageJsonPath=join('node_modules',...p,'package.json')

 const rel=relative(dirname(mainPath),prod+'/')
 const rel2=relative(dirname(mainPath),prod+'/node-module.mjs')
 const relProd=relative(dirname(mainPath),prod+'/compile/dist/paid')
 const relDev=relative(dirname(mainPath),dev)

 try{mkdirSync(dirname(mainPath),{recursive:true})}catch(err){}
 writeFileSync(mainPath,`module.exports=require`+`('${rel}')`)
 writeFileSync(modulePath,`export * from '${rel2}'`)
 writeFileSync(prodPath,`module.exports=require('${relProd}')`)
 writeFileSync(devPath,`module.exports=require('${relDev}')`)
 writeFileSync(packageJsonPath,JSON.stringify({
  name:kName,main:'index.js',module:'module.js',
  // write method ids and arc ids in separate externs file with :only
  externs:relative(dirname(packageJsonPath),join(prod,'types/runtypes-externs/nav.externs.js')),
 },null,1))
 console.log('[+] %s -> %s',key.replace(/\./g,'/'),mainPath)
}