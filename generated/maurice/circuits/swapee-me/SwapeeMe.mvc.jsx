/** @extends {xyz.swapee.wc.AbstractSwapeeMe} */
export default class AbstractSwapeeMe extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractSwapeeMeComputer} */
export class AbstractSwapeeMeComputer extends (<computer>
   <adapter name="adaptUserpic">
    <xyz.swapee.wc.ISwapeeMeCore profile />
    <outputs>
     <xyz.swapee.wc.ISwapeeMeCore userpic />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeMeController} */
export class AbstractSwapeeMeController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeMePort} */
export class SwapeeMePort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeMeView} */
export class AbstractSwapeeMeView extends (<view>
  <classes>
   <string opt name="UserpicIm">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeMeElement} */
export class AbstractSwapeeMeElement extends (<element v3 html mv>
 <block src="./SwapeeMe.mvc/src/SwapeeMeElement/methods/render.jsx" />
 <inducer src="./SwapeeMe.mvc/src/SwapeeMeElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeMeHtmlComponent} */
export class AbstractSwapeeMeHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>