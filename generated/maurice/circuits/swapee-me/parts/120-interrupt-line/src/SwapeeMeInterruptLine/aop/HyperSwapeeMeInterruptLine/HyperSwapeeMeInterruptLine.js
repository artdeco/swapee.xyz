import AbstractHyperSwapeeMeInterruptLine from '../../../../gen/AbstractSwapeeMeInterruptLine/hyper/AbstractHyperSwapeeMeInterruptLine'
import SwapeeMeInterruptLine from '../../SwapeeMeInterruptLine'
import SwapeeMeInterruptLineGeneralAspects from '../SwapeeMeInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperSwapeeMeInterruptLine} */
export default class extends AbstractHyperSwapeeMeInterruptLine
 .consults(
  SwapeeMeInterruptLineGeneralAspects,
 )
 .implements(
  SwapeeMeInterruptLine,
 )
{}