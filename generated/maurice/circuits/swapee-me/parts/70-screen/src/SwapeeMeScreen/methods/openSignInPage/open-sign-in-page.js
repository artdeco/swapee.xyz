import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ISwapeeMeScreen._openSignInPage} */
export default function openSignInPage() {
 const page=`/swapee-me.html`
 const popupCenter=(url,{title,w,h}={}) => {
  // Fixes dual-screen position                             Most browsers      Firefox
  const dualScreenLeft=window.screenLeft !==  undefined?window.screenLeft : window.screenX
  const dualScreenTop = window.screenTop !==  undefined?window.screenTop  : window.screenY

  const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth?document.documentElement.clientWidth:screen.width
  const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight:screen.height

  // const systemZoom = width / window.screen.availWidth
  const left = (width - w) / 2 + dualScreenLeft
  const top = (height - h) / 2 + dualScreenTop

  window['_swapee_me_cb']=()=>{
   // now update the profile based on data in local storage
   const newData=this.deduceInputs(this.element)
   this.setInputs(Object.assign({},...newData))
  }

  const newWindow=window.open(url,title,`
    scrollbars=yes,
    width=${w},
    height=${h},
    top=${top},
    left=${left}`,
  )

  if(!newWindow) return
  if (window.focus) newWindow.focus()
  return newWindow
 }
 const popup=popupCenter(page,{w:450,h:605})
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvc3dhcGVlLW1lL3N3YXBlZS1tZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBcUZHLFNBQVMsY0FBYyxDQUFDO0NBQ3ZCLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQztDQUN2QixNQUFNLFdBQVcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtFQUNwQyxHQUFHLE1BQU0sWUFBWSxxQ0FBcUMsS0FBSyxjQUFjO0VBQzdFLE1BQU0sY0FBYyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO0VBQ2pGLE1BQU0sYUFBYSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDOztFQUVqRixNQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUM7RUFDdkksTUFBTSxNQUFNLEdBQUcsTUFBTSxDQUFDLFlBQVksRUFBRSxNQUFNLENBQUMsWUFBWSxFQUFFLFFBQVEsQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUM7O0VBRTlJLEdBQUcsTUFBTSxVQUFVLEdBQUcsTUFBTSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUM7RUFDNUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRTtFQUMvQixNQUFNLEdBQUcsR0FBRyxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFOztFQUUvQix1QkFBdUIsQ0FBQyxDQUFDO0dBQ3hCLEdBQUcsSUFBSSxPQUFPLElBQUksUUFBUSxNQUFNLEdBQUcsS0FBSyxHQUFHLE1BQU07R0FDakQsTUFBTSxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7R0FDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU07QUFDdEM7O0VBRUUsTUFBTSxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO0lBQ3BDLFVBQVUsQ0FBQyxHQUFHO0lBQ2QsS0FBSyxDQUFDLEVBQUUsQ0FBQztJQUNULE1BQU0sQ0FBQyxFQUFFLENBQUM7SUFDVixHQUFHLENBQUMsRUFBRSxHQUFHO0lBQ1QsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7RUFDZjs7RUFFQSxFQUFFLENBQUMsWUFBWTtFQUNmLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxTQUFTLENBQUMsS0FBSyxDQUFDO0VBQ2xDLE9BQU87QUFDVDtDQUNDLE1BQU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQztBQUMzQyxDQUFGIn0=