/** @type {xyz.swapee.wc.ISwapeeMeScreen._stashUseridInLocalStorage} */
export default function paint_stashUseridInLocalStorage({userid:userid},_,{'ea8f5':prev_userid}) {
 if(prev_userid===undefined||userid===null) return
 const{
  asISwapeeMeScreen:{stashUseridInLocalStorage:stashUseridInLocalStorage},
 }=this
 stashUseridInLocalStorage({
  userid:userid,
 })
}
paint_stashUseridInLocalStorage['_id']='079b11c'