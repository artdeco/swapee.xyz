/** @type {xyz.swapee.wc.ISwapeeMeScreen._stashProfileInLocalStorage} */
export default function paint_stashProfileInLocalStorage({profile:profile},_,{'7d974':prev_profile}) {
 if(prev_profile===undefined||profile===null) return
 const{
  asISwapeeMeScreen:{stashProfileInLocalStorage:stashProfileInLocalStorage},
 }=this
 stashProfileInLocalStorage({
  profile:profile,
 })
}
paint_stashProfileInLocalStorage['_id']='46a4bcf'