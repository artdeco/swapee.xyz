/** @type {xyz.swapee.wc.ISwapeeMeScreen._stashUsernameInLocalStorage} */
export default function paint_stashUsernameInLocalStorage({username:username},_,{'14c4b':prev_username}) {
 if(prev_username===undefined||username===null) return
 const{
  asISwapeeMeScreen:{stashUsernameInLocalStorage:stashUsernameInLocalStorage},
 }=this
 stashUsernameInLocalStorage({
  username:username,
 })
}
paint_stashUsernameInLocalStorage['_id']='92c20ae'