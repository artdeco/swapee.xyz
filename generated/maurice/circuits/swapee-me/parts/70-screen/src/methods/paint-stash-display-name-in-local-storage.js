/** @type {xyz.swapee.wc.ISwapeeMeScreen._stashDisplayNameInLocalStorage} */
export default function paint_stashDisplayNameInLocalStorage({displayName:displayName},_,{'4498e':prev_displayName}) {
 if(prev_displayName===undefined||displayName===null) return
 const{
  asISwapeeMeScreen:{stashDisplayNameInLocalStorage:stashDisplayNameInLocalStorage},
 }=this
 stashDisplayNameInLocalStorage({
  displayName:displayName,
 })
}
paint_stashDisplayNameInLocalStorage['_id']='d62fd30'