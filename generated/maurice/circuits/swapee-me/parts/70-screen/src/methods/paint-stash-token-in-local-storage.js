/** @type {xyz.swapee.wc.ISwapeeMeScreen._stashTokenInLocalStorage} */
export default function paint_stashTokenInLocalStorage({token:token},_,{'94a08':prev_token}) {
 if(prev_token===undefined||token===null) return
 const{
  asISwapeeMeScreen:{stashTokenInLocalStorage:stashTokenInLocalStorage},
 }=this
 stashTokenInLocalStorage({
  token:token,
 })
}
paint_stashTokenInLocalStorage['_id']='5b747bb'