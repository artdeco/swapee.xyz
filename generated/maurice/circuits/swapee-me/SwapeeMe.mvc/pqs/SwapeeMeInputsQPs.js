import {SwapeeMeInputsPQs} from './SwapeeMeInputsPQs'
export const SwapeeMeInputsQPs=/**@type {!xyz.swapee.wc.SwapeeMeInputsQPs}*/(Object.keys(SwapeeMeInputsPQs)
 .reduce((a,k)=>{a[SwapeeMeInputsPQs[k]]=k;return a},{}))