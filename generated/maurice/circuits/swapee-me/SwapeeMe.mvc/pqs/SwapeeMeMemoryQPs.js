import {SwapeeMeMemoryPQs} from './SwapeeMeMemoryPQs'
export const SwapeeMeMemoryQPs=/**@type {!xyz.swapee.wc.SwapeeMeMemoryQPs}*/(Object.keys(SwapeeMeMemoryPQs)
 .reduce((a,k)=>{a[SwapeeMeMemoryPQs[k]]=k;return a},{}))