import {SwapeeMeCachePQs} from './SwapeeMeCachePQs'
export const SwapeeMeCacheQPs=/**@type {!xyz.swapee.wc.SwapeeMeCacheQPs}*/(Object.keys(SwapeeMeCachePQs)
 .reduce((a,k)=>{a[SwapeeMeCachePQs[k]]=k;return a},{}))