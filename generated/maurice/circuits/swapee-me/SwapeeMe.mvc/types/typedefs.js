/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ISwapeeMeComputer={}
xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic={}
xyz.swapee.wc.ISwapeeMeOuterCore={}
xyz.swapee.wc.ISwapeeMeOuterCore.Model={}
xyz.swapee.wc.ISwapeeMeOuterCore.Model.SwapeeHost={}
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Token={}
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Userid={}
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Username={}
xyz.swapee.wc.ISwapeeMeOuterCore.Model.DisplayName={}
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Profile={}
xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel={}
xyz.swapee.wc.ISwapeeMePort={}
xyz.swapee.wc.ISwapeeMePort.Inputs={}
xyz.swapee.wc.ISwapeeMePort.WeakInputs={}
xyz.swapee.wc.ISwapeeMeCore={}
xyz.swapee.wc.ISwapeeMeCore.Model={}
xyz.swapee.wc.ISwapeeMeCore.Model.Userpic={}
xyz.swapee.wc.ISwapeeMePortInterface={}
xyz.swapee.wc.ISwapeeMeProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ISwapeeMeController={}
xyz.swapee.wc.front.ISwapeeMeControllerAT={}
xyz.swapee.wc.front.ISwapeeMeScreenAR={}
xyz.swapee.wc.ISwapeeMe={}
xyz.swapee.wc.ISwapeeMeHtmlComponent={}
xyz.swapee.wc.ISwapeeMeElement={}
xyz.swapee.wc.ISwapeeMeElementPort={}
xyz.swapee.wc.ISwapeeMeElementPort.Inputs={}
xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs={}
xyz.swapee.wc.ISwapeeMeDesigner={}
xyz.swapee.wc.ISwapeeMeDesigner.communicator={}
xyz.swapee.wc.ISwapeeMeDesigner.relay={}
xyz.swapee.wc.ISwapeeMeDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ISwapeeMeDisplay={}
xyz.swapee.wc.back.ISwapeeMeController={}
xyz.swapee.wc.back.ISwapeeMeControllerAR={}
xyz.swapee.wc.back.ISwapeeMeScreen={}
xyz.swapee.wc.back.ISwapeeMeScreenAT={}
xyz.swapee.wc.ISwapeeMeController={}
xyz.swapee.wc.ISwapeeMeScreen={}
xyz.swapee.wc.ISwapeeMeScreen.stashTokenInLocalStorage={}
xyz.swapee.wc.ISwapeeMeScreen.stashUseridInLocalStorage={}
xyz.swapee.wc.ISwapeeMeScreen.stashUsernameInLocalStorage={}
xyz.swapee.wc.ISwapeeMeScreen.stashDisplayNameInLocalStorage={}
xyz.swapee.wc.ISwapeeMeScreen.stashProfileInLocalStorage={}
xyz.swapee.wc.ISwapeeMeGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/02-ISwapeeMeComputer.xml}  8b61382f5d4b20ddbae6eefa3bee22e9 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ISwapeeMeComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeComputer)} xyz.swapee.wc.AbstractSwapeeMeComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeComputer} xyz.swapee.wc.SwapeeMeComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeComputer` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeComputer
 */
xyz.swapee.wc.AbstractSwapeeMeComputer = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeComputer.constructor&xyz.swapee.wc.SwapeeMeComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeComputer.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeComputer.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeComputer.Initialese[]) => xyz.swapee.wc.ISwapeeMeComputer} xyz.swapee.wc.SwapeeMeComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.SwapeeMeMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.ISwapeeMeComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ISwapeeMeComputer
 */
xyz.swapee.wc.ISwapeeMeComputer = class extends /** @type {xyz.swapee.wc.ISwapeeMeComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic} */
xyz.swapee.wc.ISwapeeMeComputer.prototype.adaptUserpic = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeComputer.compute} */
xyz.swapee.wc.ISwapeeMeComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeComputer.Initialese>)} xyz.swapee.wc.SwapeeMeComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeComputer} xyz.swapee.wc.ISwapeeMeComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeMeComputer_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeComputer
 * @implements {xyz.swapee.wc.ISwapeeMeComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeComputer.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeComputer = class extends /** @type {xyz.swapee.wc.SwapeeMeComputer.constructor&xyz.swapee.wc.ISwapeeMeComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeComputer}
 */
xyz.swapee.wc.SwapeeMeComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeComputer} */
xyz.swapee.wc.RecordISwapeeMeComputer

/** @typedef {xyz.swapee.wc.ISwapeeMeComputer} xyz.swapee.wc.BoundISwapeeMeComputer */

/** @typedef {xyz.swapee.wc.SwapeeMeComputer} xyz.swapee.wc.BoundSwapeeMeComputer */

/**
 * Contains getters to cast the _ISwapeeMeComputer_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeComputerCaster
 */
xyz.swapee.wc.ISwapeeMeComputerCaster = class { }
/**
 * Cast the _ISwapeeMeComputer_ instance into the _BoundISwapeeMeComputer_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeComputer}
 */
xyz.swapee.wc.ISwapeeMeComputerCaster.prototype.asISwapeeMeComputer
/**
 * Access the _SwapeeMeComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeComputer}
 */
xyz.swapee.wc.ISwapeeMeComputerCaster.prototype.superSwapeeMeComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Form, changes: xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Return|void)>)} xyz.swapee.wc.ISwapeeMeComputer.__adaptLoadBitcoinAddress
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeComputer.__adaptLoadBitcoinAddress<!xyz.swapee.wc.ISwapeeMeComputer>} xyz.swapee.wc.ISwapeeMeComputer._adaptLoadBitcoinAddress */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress} */
/**
 * @param {!xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Form} form The form with inputs.
 * - `swapeeHost` _string_ The Swapee.me host. ⤴ *ISwapeeMeOuterCore.Model.SwapeeHost_Safe*
 * - `token` _string_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Token_Safe*
 * @param {xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Form} changes The previous values of the form.
 * - `swapeeHost` _string_ The Swapee.me host. ⤴ *ISwapeeMeOuterCore.Model.SwapeeHost_Safe*
 * - `token` _string_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Token_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.SwapeeHost_Safe&xyz.swapee.wc.ISwapeeMeCore.Model.Token_Safe} xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ISwapeeISwapeeMeMePort.Inputs} xyz.swapee.wc.ISwapeeMeComputer.adaptLoadBitcoinAddress.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Form, changes: xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Form) => (void|xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Return)} xyz.swapee.wc.ISwapeeMeComputer.__adaptUserpic
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeComputer.__adaptUserpic<!xyz.swapee.wc.ISwapeeMeComputer>} xyz.swapee.wc.ISwapeeMeComputer._adaptUserpic */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic} */
/**
 * @param {!xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Form} form The form with inputs.
 * - `profile` _string_ The profile id. ⤴ *ISwapeeMeOuterCore.Model.Profile_Safe*
 * @param {xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Form} changes The previous values of the form.
 * - `profile` _string_ The profile id. ⤴ *ISwapeeMeOuterCore.Model.Profile_Safe*
 * @return {void|xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Return} The form with outputs.
 */
xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.Profile_Safe} xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.Userpic} xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.SwapeeMeMemory) => void} xyz.swapee.wc.ISwapeeMeComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeComputer.__compute<!xyz.swapee.wc.ISwapeeMeComputer>} xyz.swapee.wc.ISwapeeMeComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.SwapeeMeMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/03-ISwapeeMeOuterCore.xml}  61a94bf0f5750655dc97312da5b0fdfe */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeMeOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeOuterCore)} xyz.swapee.wc.AbstractSwapeeMeOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeOuterCore} xyz.swapee.wc.SwapeeMeOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeOuterCore
 */
xyz.swapee.wc.AbstractSwapeeMeOuterCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeOuterCore.constructor&xyz.swapee.wc.SwapeeMeOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeOuterCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ISwapeeMeOuterCore|typeof xyz.swapee.wc.SwapeeMeOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ISwapeeMeOuterCore|typeof xyz.swapee.wc.SwapeeMeOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ISwapeeMeOuterCore|typeof xyz.swapee.wc.SwapeeMeOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeOuterCoreCaster)} xyz.swapee.wc.ISwapeeMeOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _ISwapeeMe_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ISwapeeMeOuterCore
 */
xyz.swapee.wc.ISwapeeMeOuterCore = class extends /** @type {xyz.swapee.wc.ISwapeeMeOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeOuterCore.prototype.constructor = xyz.swapee.wc.ISwapeeMeOuterCore

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeOuterCore.Initialese>)} xyz.swapee.wc.SwapeeMeOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeOuterCore} xyz.swapee.wc.ISwapeeMeOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeMeOuterCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeOuterCore
 * @implements {xyz.swapee.wc.ISwapeeMeOuterCore} The _ISwapeeMe_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeOuterCore = class extends /** @type {xyz.swapee.wc.SwapeeMeOuterCore.constructor&xyz.swapee.wc.ISwapeeMeOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeOuterCore.prototype.constructor = xyz.swapee.wc.SwapeeMeOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeOuterCore}
 */
xyz.swapee.wc.SwapeeMeOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeOuterCore.
 * @interface xyz.swapee.wc.ISwapeeMeOuterCoreFields
 */
xyz.swapee.wc.ISwapeeMeOuterCoreFields = class { }
/**
 * The _ISwapeeMe_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ISwapeeMeOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeMeOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore} */
xyz.swapee.wc.RecordISwapeeMeOuterCore

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore} xyz.swapee.wc.BoundISwapeeMeOuterCore */

/** @typedef {xyz.swapee.wc.SwapeeMeOuterCore} xyz.swapee.wc.BoundSwapeeMeOuterCore */

/**
 * The Swapee.me host.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeOuterCore.Model.SwapeeHost.swapeeHost

/**
 * The token obtained from Swapee.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Token.token

/**
 * The id of the user.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Userid.userid

/**
 * The username obtained from Swapee.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Username.username

/**
 * The display name obtained from Swapee.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeOuterCore.Model.DisplayName.displayName

/**
 * The profile id.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeOuterCore.Model.Profile.profile

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.SwapeeHost&xyz.swapee.wc.ISwapeeMeOuterCore.Model.Token&xyz.swapee.wc.ISwapeeMeOuterCore.Model.Userid&xyz.swapee.wc.ISwapeeMeOuterCore.Model.Username&xyz.swapee.wc.ISwapeeMeOuterCore.Model.DisplayName&xyz.swapee.wc.ISwapeeMeOuterCore.Model.Profile} xyz.swapee.wc.ISwapeeMeOuterCore.Model The _ISwapeeMe_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.SwapeeHost&xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Token&xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Userid&xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Username&xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.DisplayName&xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Profile} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel The _ISwapeeMe_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ISwapeeMeOuterCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeOuterCoreCaster
 */
xyz.swapee.wc.ISwapeeMeOuterCoreCaster = class { }
/**
 * Cast the _ISwapeeMeOuterCore_ instance into the _BoundISwapeeMeOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeOuterCore}
 */
xyz.swapee.wc.ISwapeeMeOuterCoreCaster.prototype.asISwapeeMeOuterCore
/**
 * Access the _SwapeeMeOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeOuterCore}
 */
xyz.swapee.wc.ISwapeeMeOuterCoreCaster.prototype.superSwapeeMeOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.SwapeeHost The Swapee.me host (optional overlay).
 * @prop {string} [swapeeHost="https://swapee.me/api"] The Swapee.me host. Default `https://swapee.me/api`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.SwapeeHost_Safe The Swapee.me host (required overlay).
 * @prop {string} swapeeHost The Swapee.me host.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Token The token obtained from Swapee (optional overlay).
 * @prop {string} [token="void 0"] The token obtained from Swapee. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Token_Safe The token obtained from Swapee (required overlay).
 * @prop {string} token The token obtained from Swapee.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Userid The id of the user (optional overlay).
 * @prop {string} [userid=""] The id of the user. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Userid_Safe The id of the user (required overlay).
 * @prop {string} userid The id of the user.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Username The username obtained from Swapee (optional overlay).
 * @prop {string} [username=""] The username obtained from Swapee. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Username_Safe The username obtained from Swapee (required overlay).
 * @prop {string} username The username obtained from Swapee.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.DisplayName The display name obtained from Swapee (optional overlay).
 * @prop {string} [displayName=""] The display name obtained from Swapee. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.DisplayName_Safe The display name obtained from Swapee (required overlay).
 * @prop {string} displayName The display name obtained from Swapee.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Profile The profile id (optional overlay).
 * @prop {string} [profile=""] The profile id. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.Model.Profile_Safe The profile id (required overlay).
 * @prop {string} profile The profile id.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.SwapeeHost The Swapee.me host (optional overlay).
 * @prop {*} [swapeeHost="https://swapee.me/api"] The Swapee.me host. Default `https://swapee.me/api`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.SwapeeHost_Safe The Swapee.me host (required overlay).
 * @prop {*} swapeeHost The Swapee.me host.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Token The token obtained from Swapee (optional overlay).
 * @prop {*} [token="void 0"] The token obtained from Swapee. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Token_Safe The token obtained from Swapee (required overlay).
 * @prop {*} token The token obtained from Swapee.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Userid The id of the user (optional overlay).
 * @prop {*} [userid=null] The id of the user. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Userid_Safe The id of the user (required overlay).
 * @prop {*} userid The id of the user.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Username The username obtained from Swapee (optional overlay).
 * @prop {*} [username=null] The username obtained from Swapee. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Username_Safe The username obtained from Swapee (required overlay).
 * @prop {*} username The username obtained from Swapee.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.DisplayName The display name obtained from Swapee (optional overlay).
 * @prop {*} [displayName=null] The display name obtained from Swapee. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.DisplayName_Safe The display name obtained from Swapee (required overlay).
 * @prop {*} displayName The display name obtained from Swapee.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Profile The profile id (optional overlay).
 * @prop {*} [profile=null] The profile id. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Profile_Safe The profile id (required overlay).
 * @prop {*} profile The profile id.
 */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.SwapeeHost} xyz.swapee.wc.ISwapeeMePort.Inputs.SwapeeHost The Swapee.me host (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.SwapeeHost_Safe} xyz.swapee.wc.ISwapeeMePort.Inputs.SwapeeHost_Safe The Swapee.me host (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Token} xyz.swapee.wc.ISwapeeMePort.Inputs.Token The token obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Token_Safe} xyz.swapee.wc.ISwapeeMePort.Inputs.Token_Safe The token obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Userid} xyz.swapee.wc.ISwapeeMePort.Inputs.Userid The id of the user (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Userid_Safe} xyz.swapee.wc.ISwapeeMePort.Inputs.Userid_Safe The id of the user (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Username} xyz.swapee.wc.ISwapeeMePort.Inputs.Username The username obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Username_Safe} xyz.swapee.wc.ISwapeeMePort.Inputs.Username_Safe The username obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.DisplayName} xyz.swapee.wc.ISwapeeMePort.Inputs.DisplayName The display name obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.DisplayName_Safe} xyz.swapee.wc.ISwapeeMePort.Inputs.DisplayName_Safe The display name obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Profile} xyz.swapee.wc.ISwapeeMePort.Inputs.Profile The profile id (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Profile_Safe} xyz.swapee.wc.ISwapeeMePort.Inputs.Profile_Safe The profile id (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.SwapeeHost} xyz.swapee.wc.ISwapeeMePort.WeakInputs.SwapeeHost The Swapee.me host (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.SwapeeHost_Safe} xyz.swapee.wc.ISwapeeMePort.WeakInputs.SwapeeHost_Safe The Swapee.me host (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Token} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Token The token obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Token_Safe} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Token_Safe The token obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Userid} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Userid The id of the user (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Userid_Safe} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Userid_Safe The id of the user (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Username} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Username The username obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Username_Safe} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Username_Safe The username obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.DisplayName} xyz.swapee.wc.ISwapeeMePort.WeakInputs.DisplayName The display name obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.DisplayName_Safe} xyz.swapee.wc.ISwapeeMePort.WeakInputs.DisplayName_Safe The display name obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Profile} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Profile The profile id (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.Profile_Safe} xyz.swapee.wc.ISwapeeMePort.WeakInputs.Profile_Safe The profile id (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.SwapeeHost} xyz.swapee.wc.ISwapeeMeCore.Model.SwapeeHost The Swapee.me host (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.SwapeeHost_Safe} xyz.swapee.wc.ISwapeeMeCore.Model.SwapeeHost_Safe The Swapee.me host (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Token} xyz.swapee.wc.ISwapeeMeCore.Model.Token The token obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Token_Safe} xyz.swapee.wc.ISwapeeMeCore.Model.Token_Safe The token obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Userid} xyz.swapee.wc.ISwapeeMeCore.Model.Userid The id of the user (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Userid_Safe} xyz.swapee.wc.ISwapeeMeCore.Model.Userid_Safe The id of the user (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Username} xyz.swapee.wc.ISwapeeMeCore.Model.Username The username obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Username_Safe} xyz.swapee.wc.ISwapeeMeCore.Model.Username_Safe The username obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.DisplayName} xyz.swapee.wc.ISwapeeMeCore.Model.DisplayName The display name obtained from Swapee (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.DisplayName_Safe} xyz.swapee.wc.ISwapeeMeCore.Model.DisplayName_Safe The display name obtained from Swapee (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Profile} xyz.swapee.wc.ISwapeeMeCore.Model.Profile The profile id (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model.Profile_Safe} xyz.swapee.wc.ISwapeeMeCore.Model.Profile_Safe The profile id (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/04-ISwapeeMePort.xml}  8557d748cf2aaa64bfb0e80f2cab1b4a */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeMePort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMePort)} xyz.swapee.wc.AbstractSwapeeMePort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMePort} xyz.swapee.wc.SwapeeMePort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMePort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMePort
 */
xyz.swapee.wc.AbstractSwapeeMePort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMePort.constructor&xyz.swapee.wc.SwapeeMePort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMePort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMePort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMePort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMePort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMePort|typeof xyz.swapee.wc.SwapeeMePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMePort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMePort}
 */
xyz.swapee.wc.AbstractSwapeeMePort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMePort}
 */
xyz.swapee.wc.AbstractSwapeeMePort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMePort|typeof xyz.swapee.wc.SwapeeMePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMePort}
 */
xyz.swapee.wc.AbstractSwapeeMePort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMePort|typeof xyz.swapee.wc.SwapeeMePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMePort}
 */
xyz.swapee.wc.AbstractSwapeeMePort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMePort.Initialese[]) => xyz.swapee.wc.ISwapeeMePort} xyz.swapee.wc.SwapeeMePortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMePortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMePortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeMePort.Inputs>)} xyz.swapee.wc.ISwapeeMePort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ISwapeeMe_, providing input
 * pins.
 * @interface xyz.swapee.wc.ISwapeeMePort
 */
xyz.swapee.wc.ISwapeeMePort = class extends /** @type {xyz.swapee.wc.ISwapeeMePort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMePort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMePort.resetPort} */
xyz.swapee.wc.ISwapeeMePort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeMePort.resetSwapeeMePort} */
xyz.swapee.wc.ISwapeeMePort.prototype.resetSwapeeMePort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMePort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMePort.Initialese>)} xyz.swapee.wc.SwapeeMePort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMePort} xyz.swapee.wc.ISwapeeMePort.typeof */
/**
 * A concrete class of _ISwapeeMePort_ instances.
 * @constructor xyz.swapee.wc.SwapeeMePort
 * @implements {xyz.swapee.wc.ISwapeeMePort} The port that serves as an interface to the _ISwapeeMe_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMePort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMePort = class extends /** @type {xyz.swapee.wc.SwapeeMePort.constructor&xyz.swapee.wc.ISwapeeMePort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMePort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMePort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMePort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMePort}
 */
xyz.swapee.wc.SwapeeMePort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMePort.
 * @interface xyz.swapee.wc.ISwapeeMePortFields
 */
xyz.swapee.wc.ISwapeeMePortFields = class { }
/**
 * The inputs to the _ISwapeeMe_'s controller via its port.
 */
xyz.swapee.wc.ISwapeeMePortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMePort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeMePortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeMePort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMePort} */
xyz.swapee.wc.RecordISwapeeMePort

/** @typedef {xyz.swapee.wc.ISwapeeMePort} xyz.swapee.wc.BoundISwapeeMePort */

/** @typedef {xyz.swapee.wc.SwapeeMePort} xyz.swapee.wc.BoundSwapeeMePort */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeMePort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel} xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ISwapeeMe_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeMePort.Inputs
 */
xyz.swapee.wc.ISwapeeMePort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeMePort.Inputs.constructor&xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMePort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeMePort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeMePort.WeakInputs.constructor */
/**
 * The inputs to the _ISwapeeMe_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeMePort.WeakInputs
 */
xyz.swapee.wc.ISwapeeMePort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeMePort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMePort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeMePort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ISwapeeMePortInterface
 */
xyz.swapee.wc.ISwapeeMePortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ISwapeeMePortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeMePortInterface.prototype.constructor = xyz.swapee.wc.ISwapeeMePortInterface

/**
 * A concrete class of _ISwapeeMePortInterface_ instances.
 * @constructor xyz.swapee.wc.SwapeeMePortInterface
 * @implements {xyz.swapee.wc.ISwapeeMePortInterface} The port interface.
 */
xyz.swapee.wc.SwapeeMePortInterface = class extends xyz.swapee.wc.ISwapeeMePortInterface { }
xyz.swapee.wc.SwapeeMePortInterface.prototype.constructor = xyz.swapee.wc.SwapeeMePortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMePortInterface.Props
 * @prop {string} userid The id of the user.
 * @prop {string} username The username obtained from Swapee.
 * @prop {string} displayName The display name obtained from Swapee.
 * @prop {string} profile The profile id.
 * @prop {string} [swapeeHost="https://swapee.me/api"] The Swapee.me host. Default `https://swapee.me/api`.
 * @prop {string} [token="void 0"] The token obtained from Swapee. Default `void 0`.
 */

/**
 * Contains getters to cast the _ISwapeeMePort_ interface.
 * @interface xyz.swapee.wc.ISwapeeMePortCaster
 */
xyz.swapee.wc.ISwapeeMePortCaster = class { }
/**
 * Cast the _ISwapeeMePort_ instance into the _BoundISwapeeMePort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMePort}
 */
xyz.swapee.wc.ISwapeeMePortCaster.prototype.asISwapeeMePort
/**
 * Access the _SwapeeMePort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMePort}
 */
xyz.swapee.wc.ISwapeeMePortCaster.prototype.superSwapeeMePort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMePort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMePort.__resetPort<!xyz.swapee.wc.ISwapeeMePort>} xyz.swapee.wc.ISwapeeMePort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMePort.resetPort} */
/**
 * Resets the _ISwapeeMe_ port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMePort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMePort.__resetSwapeeMePort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMePort.__resetSwapeeMePort<!xyz.swapee.wc.ISwapeeMePort>} xyz.swapee.wc.ISwapeeMePort._resetSwapeeMePort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMePort.resetSwapeeMePort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMePort.resetSwapeeMePort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMePort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/09-ISwapeeMeCore.xml}  9f52f66c823e9689530bcc06af6b3b42 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeMeCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeCore)} xyz.swapee.wc.AbstractSwapeeMeCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeCore} xyz.swapee.wc.SwapeeMeCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeCore
 */
xyz.swapee.wc.AbstractSwapeeMeCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeCore.constructor&xyz.swapee.wc.SwapeeMeCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeCore|typeof xyz.swapee.wc.SwapeeMeCore)|(!xyz.swapee.wc.ISwapeeMeOuterCore|typeof xyz.swapee.wc.SwapeeMeOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeCore}
 */
xyz.swapee.wc.AbstractSwapeeMeCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeCore}
 */
xyz.swapee.wc.AbstractSwapeeMeCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeCore|typeof xyz.swapee.wc.SwapeeMeCore)|(!xyz.swapee.wc.ISwapeeMeOuterCore|typeof xyz.swapee.wc.SwapeeMeOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeCore}
 */
xyz.swapee.wc.AbstractSwapeeMeCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeCore|typeof xyz.swapee.wc.SwapeeMeCore)|(!xyz.swapee.wc.ISwapeeMeOuterCore|typeof xyz.swapee.wc.SwapeeMeOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeCore}
 */
xyz.swapee.wc.AbstractSwapeeMeCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeCoreCaster&xyz.swapee.wc.ISwapeeMeOuterCore)} xyz.swapee.wc.ISwapeeMeCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ISwapeeMeCore
 */
xyz.swapee.wc.ISwapeeMeCore = class extends /** @type {xyz.swapee.wc.ISwapeeMeCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeMeCore.resetCore} */
xyz.swapee.wc.ISwapeeMeCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeCore.resetSwapeeMeCore} */
xyz.swapee.wc.ISwapeeMeCore.prototype.resetSwapeeMeCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeCore.Initialese>)} xyz.swapee.wc.SwapeeMeCore.constructor */
/**
 * A concrete class of _ISwapeeMeCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeCore
 * @implements {xyz.swapee.wc.ISwapeeMeCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeCore = class extends /** @type {xyz.swapee.wc.SwapeeMeCore.constructor&xyz.swapee.wc.ISwapeeMeCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeCore.prototype.constructor = xyz.swapee.wc.SwapeeMeCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeCore}
 */
xyz.swapee.wc.SwapeeMeCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeCore.
 * @interface xyz.swapee.wc.ISwapeeMeCoreFields
 */
xyz.swapee.wc.ISwapeeMeCoreFields = class { }
/**
 * The _ISwapeeMe_'s memory.
 */
xyz.swapee.wc.ISwapeeMeCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeMeCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ISwapeeMeCoreFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeMeCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeCore} */
xyz.swapee.wc.RecordISwapeeMeCore

/** @typedef {xyz.swapee.wc.ISwapeeMeCore} xyz.swapee.wc.BoundISwapeeMeCore */

/** @typedef {xyz.swapee.wc.SwapeeMeCore} xyz.swapee.wc.BoundSwapeeMeCore */

/**
 * The picture of the user.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeCore.Model.Userpic.userpic

/** @typedef {xyz.swapee.wc.ISwapeeMeOuterCore.Model&xyz.swapee.wc.ISwapeeMeCore.Model.Userpic} xyz.swapee.wc.ISwapeeMeCore.Model The _ISwapeeMe_'s memory. */

/**
 * Contains getters to cast the _ISwapeeMeCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeCoreCaster
 */
xyz.swapee.wc.ISwapeeMeCoreCaster = class { }
/**
 * Cast the _ISwapeeMeCore_ instance into the _BoundISwapeeMeCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeCore}
 */
xyz.swapee.wc.ISwapeeMeCoreCaster.prototype.asISwapeeMeCore
/**
 * Access the _SwapeeMeCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeCore}
 */
xyz.swapee.wc.ISwapeeMeCoreCaster.prototype.superSwapeeMeCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeCore.Model.Userpic The picture of the user (optional overlay).
 * @prop {?string} [userpic=null] The picture of the user. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeCore.Model.Userpic_Safe The picture of the user (required overlay).
 * @prop {?string} userpic The picture of the user.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeCore.__resetCore<!xyz.swapee.wc.ISwapeeMeCore>} xyz.swapee.wc.ISwapeeMeCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeCore.resetCore} */
/**
 * Resets the _ISwapeeMe_ core.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeCore.__resetSwapeeMeCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeCore.__resetSwapeeMeCore<!xyz.swapee.wc.ISwapeeMeCore>} xyz.swapee.wc.ISwapeeMeCore._resetSwapeeMeCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeCore.resetSwapeeMeCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeCore.resetSwapeeMeCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/10-ISwapeeMeProcessor.xml}  2c6015355df490b585546101515fa9aa */
/** @typedef {xyz.swapee.wc.ISwapeeMeComputer.Initialese&xyz.swapee.wc.ISwapeeMeController.Initialese} xyz.swapee.wc.ISwapeeMeProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeProcessor)} xyz.swapee.wc.AbstractSwapeeMeProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeProcessor} xyz.swapee.wc.SwapeeMeProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeProcessor
 */
xyz.swapee.wc.AbstractSwapeeMeProcessor = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeProcessor.constructor&xyz.swapee.wc.SwapeeMeProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeProcessor.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!xyz.swapee.wc.ISwapeeMeCore|typeof xyz.swapee.wc.SwapeeMeCore)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!xyz.swapee.wc.ISwapeeMeCore|typeof xyz.swapee.wc.SwapeeMeCore)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!xyz.swapee.wc.ISwapeeMeCore|typeof xyz.swapee.wc.SwapeeMeCore)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeProcessor.Initialese[]) => xyz.swapee.wc.ISwapeeMeProcessor} xyz.swapee.wc.SwapeeMeProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeProcessorCaster&xyz.swapee.wc.ISwapeeMeComputer&xyz.swapee.wc.ISwapeeMeCore&xyz.swapee.wc.ISwapeeMeController)} xyz.swapee.wc.ISwapeeMeProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController} xyz.swapee.wc.ISwapeeMeController.typeof */
/**
 * The processor to compute changes to the memory for the _ISwapeeMe_.
 * @interface xyz.swapee.wc.ISwapeeMeProcessor
 */
xyz.swapee.wc.ISwapeeMeProcessor = class extends /** @type {xyz.swapee.wc.ISwapeeMeProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeComputer.typeof&xyz.swapee.wc.ISwapeeMeCore.typeof&xyz.swapee.wc.ISwapeeMeController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeProcessor.Initialese>)} xyz.swapee.wc.SwapeeMeProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeProcessor} xyz.swapee.wc.ISwapeeMeProcessor.typeof */
/**
 * A concrete class of _ISwapeeMeProcessor_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeProcessor
 * @implements {xyz.swapee.wc.ISwapeeMeProcessor} The processor to compute changes to the memory for the _ISwapeeMe_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeProcessor.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeProcessor = class extends /** @type {xyz.swapee.wc.SwapeeMeProcessor.constructor&xyz.swapee.wc.ISwapeeMeProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeProcessor}
 */
xyz.swapee.wc.SwapeeMeProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeProcessor} */
xyz.swapee.wc.RecordISwapeeMeProcessor

/** @typedef {xyz.swapee.wc.ISwapeeMeProcessor} xyz.swapee.wc.BoundISwapeeMeProcessor */

/** @typedef {xyz.swapee.wc.SwapeeMeProcessor} xyz.swapee.wc.BoundSwapeeMeProcessor */

/**
 * Contains getters to cast the _ISwapeeMeProcessor_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeProcessorCaster
 */
xyz.swapee.wc.ISwapeeMeProcessorCaster = class { }
/**
 * Cast the _ISwapeeMeProcessor_ instance into the _BoundISwapeeMeProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeProcessor}
 */
xyz.swapee.wc.ISwapeeMeProcessorCaster.prototype.asISwapeeMeProcessor
/**
 * Access the _SwapeeMeProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeProcessor}
 */
xyz.swapee.wc.ISwapeeMeProcessorCaster.prototype.superSwapeeMeProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/100-SwapeeMeMemory.xml}  7f00beb39fa5e764137152c15965d550 */
/**
 * The memory of the _ISwapeeMe_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.SwapeeMeMemory
 */
xyz.swapee.wc.SwapeeMeMemory = class { }
/**
 * The Swapee.me host. Default empty string.
 */
xyz.swapee.wc.SwapeeMeMemory.prototype.swapeeHost = /** @type {string} */ (void 0)
/**
 * The token obtained from Swapee. Default empty string.
 */
xyz.swapee.wc.SwapeeMeMemory.prototype.token = /** @type {string} */ (void 0)
/**
 * The id of the user. Default empty string.
 */
xyz.swapee.wc.SwapeeMeMemory.prototype.userid = /** @type {string} */ (void 0)
/**
 * The username obtained from Swapee. Default empty string.
 */
xyz.swapee.wc.SwapeeMeMemory.prototype.username = /** @type {string} */ (void 0)
/**
 * The display name obtained from Swapee. Default empty string.
 */
xyz.swapee.wc.SwapeeMeMemory.prototype.displayName = /** @type {string} */ (void 0)
/**
 * The profile id. Default empty string.
 */
xyz.swapee.wc.SwapeeMeMemory.prototype.profile = /** @type {string} */ (void 0)
/**
 * The picture of the user. Default `null`.
 */
xyz.swapee.wc.SwapeeMeMemory.prototype.userpic = /** @type {?string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/102-SwapeeMeInputs.xml}  e36e027e63eaac5301deca1818ae7c2a */
/**
 * The inputs of the _ISwapeeMe_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.SwapeeMeInputs
 */
xyz.swapee.wc.front.SwapeeMeInputs = class { }
/**
 * The Swapee.me host. Default `https://swapee.me/api`.
 */
xyz.swapee.wc.front.SwapeeMeInputs.prototype.swapeeHost = /** @type {string|undefined} */ (void 0)
/**
 * The token obtained from Swapee. Default `void 0`.
 */
xyz.swapee.wc.front.SwapeeMeInputs.prototype.token = /** @type {string|undefined} */ (void 0)
/**
 * The id of the user. Default empty string.
 */
xyz.swapee.wc.front.SwapeeMeInputs.prototype.userid = /** @type {string|undefined} */ (void 0)
/**
 * The username obtained from Swapee. Default empty string.
 */
xyz.swapee.wc.front.SwapeeMeInputs.prototype.username = /** @type {string|undefined} */ (void 0)
/**
 * The display name obtained from Swapee. Default empty string.
 */
xyz.swapee.wc.front.SwapeeMeInputs.prototype.displayName = /** @type {string|undefined} */ (void 0)
/**
 * The profile id. Default empty string.
 */
xyz.swapee.wc.front.SwapeeMeInputs.prototype.profile = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/11-ISwapeeMe.xml}  7b6bc3b60f1d425d652c376826c3ba79 */
/**
 * An atomic wrapper for the _ISwapeeMe_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.SwapeeMeEnv
 */
xyz.swapee.wc.SwapeeMeEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.SwapeeMeEnv.prototype.swapeeMe = /** @type {xyz.swapee.wc.ISwapeeMe} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.ISwapeeMeController.Inputs>&xyz.swapee.wc.ISwapeeMeProcessor.Initialese&xyz.swapee.wc.ISwapeeMeComputer.Initialese&xyz.swapee.wc.ISwapeeMeController.Initialese} xyz.swapee.wc.ISwapeeMe.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMe)} xyz.swapee.wc.AbstractSwapeeMe.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMe} xyz.swapee.wc.SwapeeMe.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMe` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMe
 */
xyz.swapee.wc.AbstractSwapeeMe = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMe.constructor&xyz.swapee.wc.SwapeeMe.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMe.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMe
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMe.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMe} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMe|typeof xyz.swapee.wc.SwapeeMe)|(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMe}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMe.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMe}
 */
xyz.swapee.wc.AbstractSwapeeMe.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMe}
 */
xyz.swapee.wc.AbstractSwapeeMe.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMe|typeof xyz.swapee.wc.SwapeeMe)|(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMe}
 */
xyz.swapee.wc.AbstractSwapeeMe.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMe|typeof xyz.swapee.wc.SwapeeMe)|(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMe}
 */
xyz.swapee.wc.AbstractSwapeeMe.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMe.Initialese[]) => xyz.swapee.wc.ISwapeeMe} xyz.swapee.wc.SwapeeMeConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMe.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ISwapeeMe.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ISwapeeMe.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ISwapeeMe.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.SwapeeMeClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeCaster&xyz.swapee.wc.ISwapeeMeProcessor&xyz.swapee.wc.ISwapeeMeComputer&xyz.swapee.wc.ISwapeeMeController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.ISwapeeMeController.Inputs, null>)} xyz.swapee.wc.ISwapeeMe.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ISwapeeMe
 */
xyz.swapee.wc.ISwapeeMe = class extends /** @type {xyz.swapee.wc.ISwapeeMe.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeProcessor.typeof&xyz.swapee.wc.ISwapeeMeComputer.typeof&xyz.swapee.wc.ISwapeeMeController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMe* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMe.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMe.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMe&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMe.Initialese>)} xyz.swapee.wc.SwapeeMe.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMe} xyz.swapee.wc.ISwapeeMe.typeof */
/**
 * A concrete class of _ISwapeeMe_ instances.
 * @constructor xyz.swapee.wc.SwapeeMe
 * @implements {xyz.swapee.wc.ISwapeeMe} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMe.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMe = class extends /** @type {xyz.swapee.wc.SwapeeMe.constructor&xyz.swapee.wc.ISwapeeMe.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMe* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMe.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMe* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMe.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMe.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMe}
 */
xyz.swapee.wc.SwapeeMe.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMe.
 * @interface xyz.swapee.wc.ISwapeeMeFields
 */
xyz.swapee.wc.ISwapeeMeFields = class { }
/**
 * The input pins of the _ISwapeeMe_ port.
 */
xyz.swapee.wc.ISwapeeMeFields.prototype.pinout = /** @type {!xyz.swapee.wc.ISwapeeMe.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMe} */
xyz.swapee.wc.RecordISwapeeMe

/** @typedef {xyz.swapee.wc.ISwapeeMe} xyz.swapee.wc.BoundISwapeeMe */

/** @typedef {xyz.swapee.wc.SwapeeMe} xyz.swapee.wc.BoundSwapeeMe */

/** @typedef {xyz.swapee.wc.ISwapeeMeController.Inputs} xyz.swapee.wc.ISwapeeMe.Pinout The input pins of the _ISwapeeMe_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeMeController.Inputs>)} xyz.swapee.wc.ISwapeeMeBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ISwapeeMeBuffer
 */
xyz.swapee.wc.ISwapeeMeBuffer = class extends /** @type {xyz.swapee.wc.ISwapeeMeBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeBuffer.prototype.constructor = xyz.swapee.wc.ISwapeeMeBuffer

/**
 * A concrete class of _ISwapeeMeBuffer_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeBuffer
 * @implements {xyz.swapee.wc.ISwapeeMeBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.SwapeeMeBuffer = class extends xyz.swapee.wc.ISwapeeMeBuffer { }
xyz.swapee.wc.SwapeeMeBuffer.prototype.constructor = xyz.swapee.wc.SwapeeMeBuffer

/**
 * Contains getters to cast the _ISwapeeMe_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeCaster
 */
xyz.swapee.wc.ISwapeeMeCaster = class { }
/**
 * Cast the _ISwapeeMe_ instance into the _BoundISwapeeMe_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMe}
 */
xyz.swapee.wc.ISwapeeMeCaster.prototype.asISwapeeMe
/**
 * Access the _SwapeeMe_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMe}
 */
xyz.swapee.wc.ISwapeeMeCaster.prototype.superSwapeeMe

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/110-SwapeeMeSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeMemoryPQs
 */
xyz.swapee.wc.SwapeeMeMemoryPQs = class {
  constructor() {
    /**
     * `ab70c`
     */
    this.swapeeHost=/** @type {string} */ (void 0)
    /**
     * `j4a08`
     */
    this.token=/** @type {string} */ (void 0)
    /**
     * `ea8f5`
     */
    this.userid=/** @type {string} */ (void 0)
    /**
     * `b4c4b`
     */
    this.username=/** @type {string} */ (void 0)
    /**
     * `e498e`
     */
    this.displayName=/** @type {string} */ (void 0)
    /**
     * `hd974`
     */
    this.profile=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeMeMemoryPQs.prototype.constructor = xyz.swapee.wc.SwapeeMeMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeMemoryQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeMemoryQPs = class { }
/**
 * `swapeeHost`
 */
xyz.swapee.wc.SwapeeMeMemoryQPs.prototype.ab70c = /** @type {string} */ (void 0)
/**
 * `token`
 */
xyz.swapee.wc.SwapeeMeMemoryQPs.prototype.j4a08 = /** @type {string} */ (void 0)
/**
 * `userid`
 */
xyz.swapee.wc.SwapeeMeMemoryQPs.prototype.ea8f5 = /** @type {string} */ (void 0)
/**
 * `username`
 */
xyz.swapee.wc.SwapeeMeMemoryQPs.prototype.b4c4b = /** @type {string} */ (void 0)
/**
 * `displayName`
 */
xyz.swapee.wc.SwapeeMeMemoryQPs.prototype.e498e = /** @type {string} */ (void 0)
/**
 * `profile`
 */
xyz.swapee.wc.SwapeeMeMemoryQPs.prototype.hd974 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeMemoryPQs)} xyz.swapee.wc.SwapeeMeInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeMemoryPQs} xyz.swapee.wc.SwapeeMeMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeInputsPQs
 */
xyz.swapee.wc.SwapeeMeInputsPQs = class extends /** @type {xyz.swapee.wc.SwapeeMeInputsPQs.constructor&xyz.swapee.wc.SwapeeMeMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeInputsPQs.prototype.constructor = xyz.swapee.wc.SwapeeMeInputsPQs

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeMemoryPQs)} xyz.swapee.wc.SwapeeMeInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeInputsQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeInputsQPs = class extends /** @type {xyz.swapee.wc.SwapeeMeInputsQPs.constructor&xyz.swapee.wc.SwapeeMeMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeInputsQPs.prototype.constructor = xyz.swapee.wc.SwapeeMeInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeCachePQs
 */
xyz.swapee.wc.SwapeeMeCachePQs = class {
  constructor() {
    /**
     * `a8906`
     */
    this.userpic=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeMeCachePQs.prototype.constructor = xyz.swapee.wc.SwapeeMeCachePQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeCacheQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeCacheQPs = class { }
/**
 * `userpic`
 */
xyz.swapee.wc.SwapeeMeCacheQPs.prototype.a8906 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeVdusPQs
 */
xyz.swapee.wc.SwapeeMeVdusPQs = class {
  constructor() {
    /**
     * `e1e91`
     */
    this.TokenLa=/** @type {string} */ (void 0)
    /**
     * `e1e92`
     */
    this.UsernameLa=/** @type {string} */ (void 0)
    /**
     * `e1e93`
     */
    this.UserBlock=/** @type {string} */ (void 0)
    /**
     * `e1e94`
     */
    this.UserpicIm=/** @type {string} */ (void 0)
    /**
     * `e1e95`
     */
    this.SignOutBu=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeMeVdusPQs.prototype.constructor = xyz.swapee.wc.SwapeeMeVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeVdusQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeVdusQPs = class { }
/**
 * `TokenLa`
 */
xyz.swapee.wc.SwapeeMeVdusQPs.prototype.e1e91 = /** @type {string} */ (void 0)
/**
 * `UsernameLa`
 */
xyz.swapee.wc.SwapeeMeVdusQPs.prototype.e1e92 = /** @type {string} */ (void 0)
/**
 * `UserBlock`
 */
xyz.swapee.wc.SwapeeMeVdusQPs.prototype.e1e93 = /** @type {string} */ (void 0)
/**
 * `UserpicIm`
 */
xyz.swapee.wc.SwapeeMeVdusQPs.prototype.e1e94 = /** @type {string} */ (void 0)
/**
 * `SignOutBu`
 */
xyz.swapee.wc.SwapeeMeVdusQPs.prototype.e1e95 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeClassesPQs
 */
xyz.swapee.wc.SwapeeMeClassesPQs = class {
  constructor() {
    /**
     * `a95c3`
     */
    this.UserpicIm=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeMeClassesPQs.prototype.constructor = xyz.swapee.wc.SwapeeMeClassesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeClassesQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeClassesQPs = class { }
/**
 * `UserpicIm`
 */
xyz.swapee.wc.SwapeeMeClassesQPs.prototype.a95c3 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/12-ISwapeeMeHtmlComponent.xml}  afa06b91282db514f61eece42e675476 */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeController.Initialese&xyz.swapee.wc.back.ISwapeeMeScreen.Initialese&xyz.swapee.wc.ISwapeeMe.Initialese&xyz.swapee.wc.ISwapeeMeGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ISwapeeMeProcessor.Initialese&xyz.swapee.wc.ISwapeeMeComputer.Initialese} xyz.swapee.wc.ISwapeeMeHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeHtmlComponent)} xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeHtmlComponent} xyz.swapee.wc.SwapeeMeHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeHtmlComponent
 */
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.constructor&xyz.swapee.wc.SwapeeMeHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeHtmlComponent|typeof xyz.swapee.wc.SwapeeMeHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeMeController|typeof xyz.swapee.wc.back.SwapeeMeController)|(!xyz.swapee.wc.back.ISwapeeMeScreen|typeof xyz.swapee.wc.back.SwapeeMeScreen)|(!xyz.swapee.wc.ISwapeeMe|typeof xyz.swapee.wc.SwapeeMe)|(!xyz.swapee.wc.ISwapeeMeGPU|typeof xyz.swapee.wc.SwapeeMeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeHtmlComponent|typeof xyz.swapee.wc.SwapeeMeHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeMeController|typeof xyz.swapee.wc.back.SwapeeMeController)|(!xyz.swapee.wc.back.ISwapeeMeScreen|typeof xyz.swapee.wc.back.SwapeeMeScreen)|(!xyz.swapee.wc.ISwapeeMe|typeof xyz.swapee.wc.SwapeeMe)|(!xyz.swapee.wc.ISwapeeMeGPU|typeof xyz.swapee.wc.SwapeeMeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeHtmlComponent|typeof xyz.swapee.wc.SwapeeMeHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeMeController|typeof xyz.swapee.wc.back.SwapeeMeController)|(!xyz.swapee.wc.back.ISwapeeMeScreen|typeof xyz.swapee.wc.back.SwapeeMeScreen)|(!xyz.swapee.wc.ISwapeeMe|typeof xyz.swapee.wc.SwapeeMe)|(!xyz.swapee.wc.ISwapeeMeGPU|typeof xyz.swapee.wc.SwapeeMeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeMeProcessor|typeof xyz.swapee.wc.SwapeeMeProcessor)|(!xyz.swapee.wc.ISwapeeMeComputer|typeof xyz.swapee.wc.SwapeeMeComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeHtmlComponent.Initialese[]) => xyz.swapee.wc.ISwapeeMeHtmlComponent} xyz.swapee.wc.SwapeeMeHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeHtmlComponentCaster&xyz.swapee.wc.back.ISwapeeMeController&xyz.swapee.wc.back.ISwapeeMeScreen&xyz.swapee.wc.ISwapeeMe&xyz.swapee.wc.ISwapeeMeGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.ISwapeeMeController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.ISwapeeMeProcessor&xyz.swapee.wc.ISwapeeMeComputer)} xyz.swapee.wc.ISwapeeMeHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeController} xyz.swapee.wc.back.ISwapeeMeController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeScreen} xyz.swapee.wc.back.ISwapeeMeScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeGPU} xyz.swapee.wc.ISwapeeMeGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _ISwapeeMe_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ISwapeeMeHtmlComponent
 */
xyz.swapee.wc.ISwapeeMeHtmlComponent = class extends /** @type {xyz.swapee.wc.ISwapeeMeHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeMeController.typeof&xyz.swapee.wc.back.ISwapeeMeScreen.typeof&xyz.swapee.wc.ISwapeeMe.typeof&xyz.swapee.wc.ISwapeeMeGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ISwapeeMeProcessor.typeof&xyz.swapee.wc.ISwapeeMeComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeHtmlComponent.Initialese>)} xyz.swapee.wc.SwapeeMeHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeHtmlComponent} xyz.swapee.wc.ISwapeeMeHtmlComponent.typeof */
/**
 * A concrete class of _ISwapeeMeHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeHtmlComponent
 * @implements {xyz.swapee.wc.ISwapeeMeHtmlComponent} The _ISwapeeMe_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeHtmlComponent = class extends /** @type {xyz.swapee.wc.SwapeeMeHtmlComponent.constructor&xyz.swapee.wc.ISwapeeMeHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeHtmlComponent}
 */
xyz.swapee.wc.SwapeeMeHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeHtmlComponent} */
xyz.swapee.wc.RecordISwapeeMeHtmlComponent

/** @typedef {xyz.swapee.wc.ISwapeeMeHtmlComponent} xyz.swapee.wc.BoundISwapeeMeHtmlComponent */

/** @typedef {xyz.swapee.wc.SwapeeMeHtmlComponent} xyz.swapee.wc.BoundSwapeeMeHtmlComponent */

/**
 * Contains getters to cast the _ISwapeeMeHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeHtmlComponentCaster
 */
xyz.swapee.wc.ISwapeeMeHtmlComponentCaster = class { }
/**
 * Cast the _ISwapeeMeHtmlComponent_ instance into the _BoundISwapeeMeHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeHtmlComponent}
 */
xyz.swapee.wc.ISwapeeMeHtmlComponentCaster.prototype.asISwapeeMeHtmlComponent
/**
 * Access the _SwapeeMeHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeHtmlComponent}
 */
xyz.swapee.wc.ISwapeeMeHtmlComponentCaster.prototype.superSwapeeMeHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/130-ISwapeeMeElement.xml}  1da17f4aac94bd0e359bef4dd1552354 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.ISwapeeMeElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ISwapeeMeElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeElement)} xyz.swapee.wc.AbstractSwapeeMeElement.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeElement} xyz.swapee.wc.SwapeeMeElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeElement` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeElement
 */
xyz.swapee.wc.AbstractSwapeeMeElement = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeElement.constructor&xyz.swapee.wc.SwapeeMeElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeElement.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeElement.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeElement|typeof xyz.swapee.wc.SwapeeMeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeElement}
 */
xyz.swapee.wc.AbstractSwapeeMeElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElement}
 */
xyz.swapee.wc.AbstractSwapeeMeElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeElement|typeof xyz.swapee.wc.SwapeeMeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElement}
 */
xyz.swapee.wc.AbstractSwapeeMeElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeElement|typeof xyz.swapee.wc.SwapeeMeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElement}
 */
xyz.swapee.wc.AbstractSwapeeMeElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeElement.Initialese[]) => xyz.swapee.wc.ISwapeeMeElement} xyz.swapee.wc.SwapeeMeElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeElementFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.ISwapeeMeElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.ISwapeeMeElement.Inputs, null>)} xyz.swapee.wc.ISwapeeMeElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _ISwapeeMe_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ISwapeeMeElement
 */
xyz.swapee.wc.ISwapeeMeElement = class extends /** @type {xyz.swapee.wc.ISwapeeMeElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeElement.solder} */
xyz.swapee.wc.ISwapeeMeElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeElement.render} */
xyz.swapee.wc.ISwapeeMeElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeElement.server} */
xyz.swapee.wc.ISwapeeMeElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeElement.inducer} */
xyz.swapee.wc.ISwapeeMeElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeElement&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeElement.Initialese>)} xyz.swapee.wc.SwapeeMeElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElement} xyz.swapee.wc.ISwapeeMeElement.typeof */
/**
 * A concrete class of _ISwapeeMeElement_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeElement
 * @implements {xyz.swapee.wc.ISwapeeMeElement} A component description.
 *
 * The _ISwapeeMe_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeElement.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeElement = class extends /** @type {xyz.swapee.wc.SwapeeMeElement.constructor&xyz.swapee.wc.ISwapeeMeElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeElement}
 */
xyz.swapee.wc.SwapeeMeElement.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeElement.
 * @interface xyz.swapee.wc.ISwapeeMeElementFields
 */
xyz.swapee.wc.ISwapeeMeElementFields = class { }
/**
 * The element-specific inputs to the _ISwapeeMe_ component.
 */
xyz.swapee.wc.ISwapeeMeElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMeElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeElement} */
xyz.swapee.wc.RecordISwapeeMeElement

/** @typedef {xyz.swapee.wc.ISwapeeMeElement} xyz.swapee.wc.BoundISwapeeMeElement */

/** @typedef {xyz.swapee.wc.SwapeeMeElement} xyz.swapee.wc.BoundSwapeeMeElement */

/** @typedef {xyz.swapee.wc.ISwapeeMePort.Inputs&xyz.swapee.wc.ISwapeeMeDisplay.Queries&xyz.swapee.wc.ISwapeeMeController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ISwapeeMeElementPort.Inputs} xyz.swapee.wc.ISwapeeMeElement.Inputs The element-specific inputs to the _ISwapeeMe_ component. */

/**
 * Contains getters to cast the _ISwapeeMeElement_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeElementCaster
 */
xyz.swapee.wc.ISwapeeMeElementCaster = class { }
/**
 * Cast the _ISwapeeMeElement_ instance into the _BoundISwapeeMeElement_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeElement}
 */
xyz.swapee.wc.ISwapeeMeElementCaster.prototype.asISwapeeMeElement
/**
 * Access the _SwapeeMeElement_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeElement}
 */
xyz.swapee.wc.ISwapeeMeElementCaster.prototype.superSwapeeMeElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeMeMemory, props: !xyz.swapee.wc.ISwapeeMeElement.Inputs) => Object<string, *>} xyz.swapee.wc.ISwapeeMeElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeElement.__solder<!xyz.swapee.wc.ISwapeeMeElement>} xyz.swapee.wc.ISwapeeMeElement._solder */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.SwapeeMeMemory} model The model.
 * - `swapeeHost` _string_ The Swapee.me host. Default empty string.
 * - `token` _string_ The token obtained from Swapee. Default empty string.
 * - `userid` _string_ The id of the user. Default empty string.
 * - `username` _string_ The username obtained from Swapee. Default empty string.
 * - `displayName` _string_ The display name obtained from Swapee. Default empty string.
 * - `profile` _string_ The profile id. Default empty string.
 * - `userpic` _?string_ The picture of the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeMeElement.Inputs} props The element props.
 * - `[swapeeHost="https://swapee.me/api"]` _&#42;?_ The Swapee.me host. ⤴ *ISwapeeMeOuterCore.WeakModel.SwapeeHost* ⤴ *ISwapeeMeOuterCore.WeakModel.SwapeeHost* Default `https://swapee.me/api`.
 * - `[token=null]` _&#42;?_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.Token* ⤴ *ISwapeeMeOuterCore.WeakModel.Token* Default `null`.
 * - `[userid=null]` _&#42;?_ The id of the user. ⤴ *ISwapeeMeOuterCore.WeakModel.Userid* ⤴ *ISwapeeMeOuterCore.WeakModel.Userid* Default `null`.
 * - `[username=null]` _&#42;?_ The username obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.Username* ⤴ *ISwapeeMeOuterCore.WeakModel.Username* Default `null`.
 * - `[displayName=null]` _&#42;?_ The display name obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.DisplayName* ⤴ *ISwapeeMeOuterCore.WeakModel.DisplayName* Default `null`.
 * - `[profile=null]` _&#42;?_ The profile id. ⤴ *ISwapeeMeOuterCore.WeakModel.Profile* ⤴ *ISwapeeMeOuterCore.WeakModel.Profile* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeMeElementPort.Inputs.NoSolder* Default `false`.
 * - `[tokenLaOpts]` _!Object?_ The options to pass to the _TokenLa_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.TokenLaOpts* Default `{}`.
 * - `[userpicImOpts]` _!Object?_ The options to pass to the _UserpicIm_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UserpicImOpts* Default `{}`.
 * - `[usernameLaOpts]` _!Object?_ The options to pass to the _UsernameLa_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UsernameLaOpts* Default `{}`.
 * - `[userBlockOpts]` _!Object?_ The options to pass to the _UserBlock_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UserBlockOpts* Default `{}`.
 * - `[signOutBuOpts]` _!Object?_ The options to pass to the _SignOutBu_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.SignOutBuOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ISwapeeMeElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeMeMemory, instance?: !xyz.swapee.wc.ISwapeeMeScreen&xyz.swapee.wc.ISwapeeMeController) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeMeElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeElement.__render<!xyz.swapee.wc.ISwapeeMeElement>} xyz.swapee.wc.ISwapeeMeElement._render */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.SwapeeMeMemory} [model] The model for the view.
 * - `swapeeHost` _string_ The Swapee.me host. Default empty string.
 * - `token` _string_ The token obtained from Swapee. Default empty string.
 * - `userid` _string_ The id of the user. Default empty string.
 * - `username` _string_ The username obtained from Swapee. Default empty string.
 * - `displayName` _string_ The display name obtained from Swapee. Default empty string.
 * - `profile` _string_ The profile id. Default empty string.
 * - `userpic` _?string_ The picture of the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeMeScreen&xyz.swapee.wc.ISwapeeMeController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeMeElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeMeMemory, inputs: !xyz.swapee.wc.ISwapeeMeElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeMeElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeElement.__server<!xyz.swapee.wc.ISwapeeMeElement>} xyz.swapee.wc.ISwapeeMeElement._server */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.SwapeeMeMemory} memory The memory registers.
 * - `swapeeHost` _string_ The Swapee.me host. Default empty string.
 * - `token` _string_ The token obtained from Swapee. Default empty string.
 * - `userid` _string_ The id of the user. Default empty string.
 * - `username` _string_ The username obtained from Swapee. Default empty string.
 * - `displayName` _string_ The display name obtained from Swapee. Default empty string.
 * - `profile` _string_ The profile id. Default empty string.
 * - `userpic` _?string_ The picture of the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeMeElement.Inputs} inputs The inputs to the port.
 * - `[swapeeHost="https://swapee.me/api"]` _&#42;?_ The Swapee.me host. ⤴ *ISwapeeMeOuterCore.WeakModel.SwapeeHost* ⤴ *ISwapeeMeOuterCore.WeakModel.SwapeeHost* Default `https://swapee.me/api`.
 * - `[token=null]` _&#42;?_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.Token* ⤴ *ISwapeeMeOuterCore.WeakModel.Token* Default `null`.
 * - `[userid=null]` _&#42;?_ The id of the user. ⤴ *ISwapeeMeOuterCore.WeakModel.Userid* ⤴ *ISwapeeMeOuterCore.WeakModel.Userid* Default `null`.
 * - `[username=null]` _&#42;?_ The username obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.Username* ⤴ *ISwapeeMeOuterCore.WeakModel.Username* Default `null`.
 * - `[displayName=null]` _&#42;?_ The display name obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.DisplayName* ⤴ *ISwapeeMeOuterCore.WeakModel.DisplayName* Default `null`.
 * - `[profile=null]` _&#42;?_ The profile id. ⤴ *ISwapeeMeOuterCore.WeakModel.Profile* ⤴ *ISwapeeMeOuterCore.WeakModel.Profile* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeMeElementPort.Inputs.NoSolder* Default `false`.
 * - `[tokenLaOpts]` _!Object?_ The options to pass to the _TokenLa_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.TokenLaOpts* Default `{}`.
 * - `[userpicImOpts]` _!Object?_ The options to pass to the _UserpicIm_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UserpicImOpts* Default `{}`.
 * - `[usernameLaOpts]` _!Object?_ The options to pass to the _UsernameLa_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UsernameLaOpts* Default `{}`.
 * - `[userBlockOpts]` _!Object?_ The options to pass to the _UserBlock_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UserBlockOpts* Default `{}`.
 * - `[signOutBuOpts]` _!Object?_ The options to pass to the _SignOutBu_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.SignOutBuOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeMeElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeMeMemory, port?: !xyz.swapee.wc.ISwapeeMeElement.Inputs) => ?} xyz.swapee.wc.ISwapeeMeElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeElement.__inducer<!xyz.swapee.wc.ISwapeeMeElement>} xyz.swapee.wc.ISwapeeMeElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.SwapeeMeMemory} [model] The model of the component into which to induce the state.
 * - `swapeeHost` _string_ The Swapee.me host. Default empty string.
 * - `token` _string_ The token obtained from Swapee. Default empty string.
 * - `userid` _string_ The id of the user. Default empty string.
 * - `username` _string_ The username obtained from Swapee. Default empty string.
 * - `displayName` _string_ The display name obtained from Swapee. Default empty string.
 * - `profile` _string_ The profile id. Default empty string.
 * - `userpic` _?string_ The picture of the user. Default `null`.
 * @param {!xyz.swapee.wc.ISwapeeMeElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[swapeeHost="https://swapee.me/api"]` _&#42;?_ The Swapee.me host. ⤴ *ISwapeeMeOuterCore.WeakModel.SwapeeHost* ⤴ *ISwapeeMeOuterCore.WeakModel.SwapeeHost* Default `https://swapee.me/api`.
 * - `[token=null]` _&#42;?_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.Token* ⤴ *ISwapeeMeOuterCore.WeakModel.Token* Default `null`.
 * - `[userid=null]` _&#42;?_ The id of the user. ⤴ *ISwapeeMeOuterCore.WeakModel.Userid* ⤴ *ISwapeeMeOuterCore.WeakModel.Userid* Default `null`.
 * - `[username=null]` _&#42;?_ The username obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.Username* ⤴ *ISwapeeMeOuterCore.WeakModel.Username* Default `null`.
 * - `[displayName=null]` _&#42;?_ The display name obtained from Swapee. ⤴ *ISwapeeMeOuterCore.WeakModel.DisplayName* ⤴ *ISwapeeMeOuterCore.WeakModel.DisplayName* Default `null`.
 * - `[profile=null]` _&#42;?_ The profile id. ⤴ *ISwapeeMeOuterCore.WeakModel.Profile* ⤴ *ISwapeeMeOuterCore.WeakModel.Profile* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeMeElementPort.Inputs.NoSolder* Default `false`.
 * - `[tokenLaOpts]` _!Object?_ The options to pass to the _TokenLa_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.TokenLaOpts* Default `{}`.
 * - `[userpicImOpts]` _!Object?_ The options to pass to the _UserpicIm_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UserpicImOpts* Default `{}`.
 * - `[usernameLaOpts]` _!Object?_ The options to pass to the _UsernameLa_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UsernameLaOpts* Default `{}`.
 * - `[userBlockOpts]` _!Object?_ The options to pass to the _UserBlock_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.UserBlockOpts* Default `{}`.
 * - `[signOutBuOpts]` _!Object?_ The options to pass to the _SignOutBu_ vdu. ⤴ *ISwapeeMeElementPort.Inputs.SignOutBuOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeMeElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/140-ISwapeeMeElementPort.xml}  e97a4f56a035d7b41a3caeef8fbab99f */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeMeElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeElementPort)} xyz.swapee.wc.AbstractSwapeeMeElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeElementPort} xyz.swapee.wc.SwapeeMeElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeElementPort
 */
xyz.swapee.wc.AbstractSwapeeMeElementPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeElementPort.constructor&xyz.swapee.wc.SwapeeMeElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeElementPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeElementPort|typeof xyz.swapee.wc.SwapeeMeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeElementPort|typeof xyz.swapee.wc.SwapeeMeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeElementPort|typeof xyz.swapee.wc.SwapeeMeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeElementPort.Initialese[]) => xyz.swapee.wc.ISwapeeMeElementPort} xyz.swapee.wc.SwapeeMeElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeMeElementPort.Inputs>)} xyz.swapee.wc.ISwapeeMeElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ISwapeeMeElementPort
 */
xyz.swapee.wc.ISwapeeMeElementPort = class extends /** @type {xyz.swapee.wc.ISwapeeMeElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeElementPort.Initialese>)} xyz.swapee.wc.SwapeeMeElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElementPort} xyz.swapee.wc.ISwapeeMeElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeMeElementPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeElementPort
 * @implements {xyz.swapee.wc.ISwapeeMeElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeElementPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeElementPort = class extends /** @type {xyz.swapee.wc.SwapeeMeElementPort.constructor&xyz.swapee.wc.ISwapeeMeElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeElementPort}
 */
xyz.swapee.wc.SwapeeMeElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeElementPort.
 * @interface xyz.swapee.wc.ISwapeeMeElementPortFields
 */
xyz.swapee.wc.ISwapeeMeElementPortFields = class { }
/**
 * The inputs to the _ISwapeeMeElement_'s controller via its element port.
 */
xyz.swapee.wc.ISwapeeMeElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMeElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeMeElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeMeElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeElementPort} */
xyz.swapee.wc.RecordISwapeeMeElementPort

/** @typedef {xyz.swapee.wc.ISwapeeMeElementPort} xyz.swapee.wc.BoundISwapeeMeElementPort */

/** @typedef {xyz.swapee.wc.SwapeeMeElementPort} xyz.swapee.wc.BoundSwapeeMeElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder)} xyz.swapee.wc.ISwapeeMeElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder} xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _ISwapeeMeElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeMeElementPort.Inputs
 */
xyz.swapee.wc.ISwapeeMeElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeMeElementPort.Inputs.constructor&xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeMeElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _ISwapeeMeElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs

/**
 * Contains getters to cast the _ISwapeeMeElementPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeElementPortCaster
 */
xyz.swapee.wc.ISwapeeMeElementPortCaster = class { }
/**
 * Cast the _ISwapeeMeElementPort_ instance into the _BoundISwapeeMeElementPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeElementPort}
 */
xyz.swapee.wc.ISwapeeMeElementPortCaster.prototype.asISwapeeMeElementPort
/**
 * Access the _SwapeeMeElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeElementPort}
 */
xyz.swapee.wc.ISwapeeMeElementPortCaster.prototype.superSwapeeMeElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/170-ISwapeeMeDesigner.xml}  51e85ddef72ddb891e7f8fe5150c196b */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ISwapeeMeDesigner
 */
xyz.swapee.wc.ISwapeeMeDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.SwapeeMeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeMe />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeMeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeMe />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeMeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeMeDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof ISwapeeMeController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeMeDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof ISwapeeMeController_
   * - `This` _typeof ISwapeeMeController_
   * @param {!xyz.swapee.wc.ISwapeeMeDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `SwapeeMe` _!SwapeeMeMemory_
   * - `This` _!SwapeeMeMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeMeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeMeClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ISwapeeMeDesigner.prototype.constructor = xyz.swapee.wc.ISwapeeMeDesigner

/**
 * A concrete class of _ISwapeeMeDesigner_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeDesigner
 * @implements {xyz.swapee.wc.ISwapeeMeDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.SwapeeMeDesigner = class extends xyz.swapee.wc.ISwapeeMeDesigner { }
xyz.swapee.wc.SwapeeMeDesigner.prototype.constructor = xyz.swapee.wc.SwapeeMeDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/40-ISwapeeMeDisplay.xml}  e6f8f734d59d28b44d95633eb1206dd8 */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ISwapeeMeDisplay.Settings>} xyz.swapee.wc.ISwapeeMeDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeDisplay)} xyz.swapee.wc.AbstractSwapeeMeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeDisplay} xyz.swapee.wc.SwapeeMeDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeDisplay
 */
xyz.swapee.wc.AbstractSwapeeMeDisplay = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeDisplay.constructor&xyz.swapee.wc.SwapeeMeDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeDisplay.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeDisplay|typeof xyz.swapee.wc.SwapeeMeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeDisplay|typeof xyz.swapee.wc.SwapeeMeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeDisplay|typeof xyz.swapee.wc.SwapeeMeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeDisplay.Initialese[]) => xyz.swapee.wc.ISwapeeMeDisplay} xyz.swapee.wc.SwapeeMeDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.SwapeeMeMemory, !HTMLDivElement, !xyz.swapee.wc.ISwapeeMeDisplay.Settings, xyz.swapee.wc.ISwapeeMeDisplay.Queries, null>)} xyz.swapee.wc.ISwapeeMeDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ISwapeeMe_.
 * @interface xyz.swapee.wc.ISwapeeMeDisplay
 */
xyz.swapee.wc.ISwapeeMeDisplay = class extends /** @type {xyz.swapee.wc.ISwapeeMeDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeDisplay.paint} */
xyz.swapee.wc.ISwapeeMeDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeDisplay.Initialese>)} xyz.swapee.wc.SwapeeMeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeDisplay} xyz.swapee.wc.ISwapeeMeDisplay.typeof */
/**
 * A concrete class of _ISwapeeMeDisplay_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeDisplay
 * @implements {xyz.swapee.wc.ISwapeeMeDisplay} Display for presenting information from the _ISwapeeMe_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeDisplay.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeDisplay = class extends /** @type {xyz.swapee.wc.SwapeeMeDisplay.constructor&xyz.swapee.wc.ISwapeeMeDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeDisplay}
 */
xyz.swapee.wc.SwapeeMeDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeDisplay.
 * @interface xyz.swapee.wc.ISwapeeMeDisplayFields
 */
xyz.swapee.wc.ISwapeeMeDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ISwapeeMeDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ISwapeeMeDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ISwapeeMeDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ISwapeeMeDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeDisplay} */
xyz.swapee.wc.RecordISwapeeMeDisplay

/** @typedef {xyz.swapee.wc.ISwapeeMeDisplay} xyz.swapee.wc.BoundISwapeeMeDisplay */

/** @typedef {xyz.swapee.wc.SwapeeMeDisplay} xyz.swapee.wc.BoundSwapeeMeDisplay */

/** @typedef {xyz.swapee.wc.ISwapeeMeDisplay.Queries} xyz.swapee.wc.ISwapeeMeDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeMeDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _ISwapeeMeDisplay_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeDisplayCaster
 */
xyz.swapee.wc.ISwapeeMeDisplayCaster = class { }
/**
 * Cast the _ISwapeeMeDisplay_ instance into the _BoundISwapeeMeDisplay_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeDisplay}
 */
xyz.swapee.wc.ISwapeeMeDisplayCaster.prototype.asISwapeeMeDisplay
/**
 * Cast the _ISwapeeMeDisplay_ instance into the _BoundISwapeeMeScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeScreen}
 */
xyz.swapee.wc.ISwapeeMeDisplayCaster.prototype.asISwapeeMeScreen
/**
 * Access the _SwapeeMeDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeDisplay}
 */
xyz.swapee.wc.ISwapeeMeDisplayCaster.prototype.superSwapeeMeDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeMeMemory, land: null) => void} xyz.swapee.wc.ISwapeeMeDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeDisplay.__paint<!xyz.swapee.wc.ISwapeeMeDisplay>} xyz.swapee.wc.ISwapeeMeDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeMeMemory} memory The display data.
 * - `swapeeHost` _string_ The Swapee.me host. Default empty string.
 * - `token` _string_ The token obtained from Swapee. Default empty string.
 * - `userid` _string_ The id of the user. Default empty string.
 * - `username` _string_ The username obtained from Swapee. Default empty string.
 * - `displayName` _string_ The display name obtained from Swapee. Default empty string.
 * - `profile` _string_ The profile id. Default empty string.
 * - `userpic` _?string_ The picture of the user. Default `null`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/40-ISwapeeMeDisplayBack.xml}  bc43763645837f2f5841ee250bc00372 */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.SwapeeMeClasses>} xyz.swapee.wc.back.ISwapeeMeDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeDisplay)} xyz.swapee.wc.back.AbstractSwapeeMeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeDisplay} xyz.swapee.wc.back.SwapeeMeDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeDisplay
 */
xyz.swapee.wc.back.AbstractSwapeeMeDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeDisplay.constructor&xyz.swapee.wc.back.SwapeeMeDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeDisplay|typeof xyz.swapee.wc.back.SwapeeMeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeDisplay|typeof xyz.swapee.wc.back.SwapeeMeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeDisplay|typeof xyz.swapee.wc.back.SwapeeMeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.SwapeeMeClasses, null>)} xyz.swapee.wc.back.ISwapeeMeDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ISwapeeMeDisplay
 */
xyz.swapee.wc.back.ISwapeeMeDisplay = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ISwapeeMeDisplay.paint} */
xyz.swapee.wc.back.ISwapeeMeDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeDisplay.Initialese>)} xyz.swapee.wc.back.SwapeeMeDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeDisplay} xyz.swapee.wc.back.ISwapeeMeDisplay.typeof */
/**
 * A concrete class of _ISwapeeMeDisplay_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeDisplay
 * @implements {xyz.swapee.wc.back.ISwapeeMeDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeDisplay = class extends /** @type {xyz.swapee.wc.back.SwapeeMeDisplay.constructor&xyz.swapee.wc.back.ISwapeeMeDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.SwapeeMeDisplay.prototype.constructor = xyz.swapee.wc.back.SwapeeMeDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeDisplay}
 */
xyz.swapee.wc.back.SwapeeMeDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeDisplay} */
xyz.swapee.wc.back.RecordISwapeeMeDisplay

/** @typedef {xyz.swapee.wc.back.ISwapeeMeDisplay} xyz.swapee.wc.back.BoundISwapeeMeDisplay */

/** @typedef {xyz.swapee.wc.back.SwapeeMeDisplay} xyz.swapee.wc.back.BoundSwapeeMeDisplay */

/**
 * Contains getters to cast the _ISwapeeMeDisplay_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeDisplayCaster
 */
xyz.swapee.wc.back.ISwapeeMeDisplayCaster = class { }
/**
 * Cast the _ISwapeeMeDisplay_ instance into the _BoundISwapeeMeDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeDisplay}
 */
xyz.swapee.wc.back.ISwapeeMeDisplayCaster.prototype.asISwapeeMeDisplay
/**
 * Access the _SwapeeMeDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeDisplay}
 */
xyz.swapee.wc.back.ISwapeeMeDisplayCaster.prototype.superSwapeeMeDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.SwapeeMeMemory, land?: null) => void} xyz.swapee.wc.back.ISwapeeMeDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeDisplay.__paint<!xyz.swapee.wc.back.ISwapeeMeDisplay>} xyz.swapee.wc.back.ISwapeeMeDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeMeMemory} [memory] The display data.
 * - `swapeeHost` _string_ The Swapee.me host. Default empty string.
 * - `token` _string_ The token obtained from Swapee. Default empty string.
 * - `userid` _string_ The id of the user. Default empty string.
 * - `username` _string_ The username obtained from Swapee. Default empty string.
 * - `displayName` _string_ The display name obtained from Swapee. Default empty string.
 * - `profile` _string_ The profile id. Default empty string.
 * - `userpic` _?string_ The picture of the user. Default `null`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeMeDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeMeDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/41-SwapeeMeClasses.xml}  77355188360551735c2592a4ef76c933 */
/**
 * The classes of the _ISwapeeMeDisplay_.
 * @record xyz.swapee.wc.SwapeeMeClasses
 */
xyz.swapee.wc.SwapeeMeClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.SwapeeMeClasses.prototype.UserpicIm = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.SwapeeMeClasses.prototype.props = /** @type {xyz.swapee.wc.SwapeeMeClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/50-ISwapeeMeController.xml}  d8fda7c8f596c3479c8e8f5e143ba528 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ISwapeeMeController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ISwapeeMeController.Inputs, !xyz.swapee.wc.ISwapeeMeOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel>} xyz.swapee.wc.ISwapeeMeController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeController)} xyz.swapee.wc.AbstractSwapeeMeController.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeController} xyz.swapee.wc.SwapeeMeController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeController` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeController
 */
xyz.swapee.wc.AbstractSwapeeMeController = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeController.constructor&xyz.swapee.wc.SwapeeMeController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeController.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeController.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.ISwapeeMeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeController}
 */
xyz.swapee.wc.AbstractSwapeeMeController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeController}
 */
xyz.swapee.wc.AbstractSwapeeMeController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.ISwapeeMeControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeController}
 */
xyz.swapee.wc.AbstractSwapeeMeController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.ISwapeeMeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeController}
 */
xyz.swapee.wc.AbstractSwapeeMeController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeController.Initialese[]) => xyz.swapee.wc.ISwapeeMeController} xyz.swapee.wc.SwapeeMeControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeMeControllerHyperslice
 */
xyz.swapee.wc.ISwapeeMeControllerHyperslice = class {
  constructor() {
    /**
     * Called when the user has signed out.
     */
    this.signedOut=/** @type {!xyz.swapee.wc.ISwapeeMeController._signedOut|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeMeController._signedOut>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeMeControllerHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeMeControllerHyperslice

/**
 * A concrete class of _ISwapeeMeControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeControllerHyperslice
 * @implements {xyz.swapee.wc.ISwapeeMeControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.SwapeeMeControllerHyperslice = class extends xyz.swapee.wc.ISwapeeMeControllerHyperslice { }
xyz.swapee.wc.SwapeeMeControllerHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeMeControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice = class {
  constructor() {
    /**
     * Called when the user has signed out.
     */
    this.signedOut=/** @type {!xyz.swapee.wc.ISwapeeMeController.__signedOut<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ISwapeeMeController.__signedOut<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice

/**
 * A concrete class of _ISwapeeMeControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeControllerBindingHyperslice
 * @implements {xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.SwapeeMeControllerBindingHyperslice = class extends xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice { }
xyz.swapee.wc.SwapeeMeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.SwapeeMeControllerBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ISwapeeMeController.Inputs, !xyz.swapee.wc.ISwapeeMeOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ISwapeeMeOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ISwapeeMeController.Inputs, !xyz.swapee.wc.ISwapeeMeController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ISwapeeMeController.Inputs, !xyz.swapee.wc.SwapeeMeMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeMeController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ISwapeeMeController.Inputs>)} xyz.swapee.wc.ISwapeeMeController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ISwapeeMeController
 */
xyz.swapee.wc.ISwapeeMeController = class extends /** @type {xyz.swapee.wc.ISwapeeMeController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeController.resetPort} */
xyz.swapee.wc.ISwapeeMeController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.signIn} */
xyz.swapee.wc.ISwapeeMeController.prototype.signIn = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.signOut} */
xyz.swapee.wc.ISwapeeMeController.prototype.signOut = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.signedOut} */
xyz.swapee.wc.ISwapeeMeController.prototype.signedOut = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.onSignedOut} */
xyz.swapee.wc.ISwapeeMeController.prototype.onSignedOut = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.setToken} */
xyz.swapee.wc.ISwapeeMeController.prototype.setToken = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.unsetToken} */
xyz.swapee.wc.ISwapeeMeController.prototype.unsetToken = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.setUserid} */
xyz.swapee.wc.ISwapeeMeController.prototype.setUserid = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.unsetUserid} */
xyz.swapee.wc.ISwapeeMeController.prototype.unsetUserid = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.setUsername} */
xyz.swapee.wc.ISwapeeMeController.prototype.setUsername = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.unsetUsername} */
xyz.swapee.wc.ISwapeeMeController.prototype.unsetUsername = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.setDisplayName} */
xyz.swapee.wc.ISwapeeMeController.prototype.setDisplayName = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.unsetDisplayName} */
xyz.swapee.wc.ISwapeeMeController.prototype.unsetDisplayName = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.setProfile} */
xyz.swapee.wc.ISwapeeMeController.prototype.setProfile = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeController.unsetProfile} */
xyz.swapee.wc.ISwapeeMeController.prototype.unsetProfile = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeController&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeController.Initialese>)} xyz.swapee.wc.SwapeeMeController.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController} xyz.swapee.wc.ISwapeeMeController.typeof */
/**
 * A concrete class of _ISwapeeMeController_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeController
 * @implements {xyz.swapee.wc.ISwapeeMeController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeController.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeController = class extends /** @type {xyz.swapee.wc.SwapeeMeController.constructor&xyz.swapee.wc.ISwapeeMeController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeController}
 */
xyz.swapee.wc.SwapeeMeController.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeController.
 * @interface xyz.swapee.wc.ISwapeeMeControllerFields
 */
xyz.swapee.wc.ISwapeeMeControllerFields = class { }
/**
 * The inputs to the _ISwapeeMe_'s controller.
 */
xyz.swapee.wc.ISwapeeMeControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMeController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeMeControllerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeMeController} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeController} */
xyz.swapee.wc.RecordISwapeeMeController

/** @typedef {xyz.swapee.wc.ISwapeeMeController} xyz.swapee.wc.BoundISwapeeMeController */

/** @typedef {xyz.swapee.wc.SwapeeMeController} xyz.swapee.wc.BoundSwapeeMeController */

/** @typedef {xyz.swapee.wc.ISwapeeMePort.Inputs} xyz.swapee.wc.ISwapeeMeController.Inputs The inputs to the _ISwapeeMe_'s controller. */

/** @typedef {xyz.swapee.wc.ISwapeeMePort.WeakInputs} xyz.swapee.wc.ISwapeeMeController.WeakInputs The inputs to the _ISwapeeMe_'s controller. */

/**
 * Contains getters to cast the _ISwapeeMeController_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeControllerCaster
 */
xyz.swapee.wc.ISwapeeMeControllerCaster = class { }
/**
 * Cast the _ISwapeeMeController_ instance into the _BoundISwapeeMeController_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeController}
 */
xyz.swapee.wc.ISwapeeMeControllerCaster.prototype.asISwapeeMeController
/**
 * Cast the _ISwapeeMeController_ instance into the _BoundISwapeeMeProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeProcessor}
 */
xyz.swapee.wc.ISwapeeMeControllerCaster.prototype.asISwapeeMeProcessor
/**
 * Access the _SwapeeMeController_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeController}
 */
xyz.swapee.wc.ISwapeeMeControllerCaster.prototype.superSwapeeMeController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__resetPort<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__signIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__signIn<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._signIn */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.signIn} */
/**
 * Opens the popup to sign in.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.signIn = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__signOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__signOut<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._signOut */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.signOut} */
/**
 * Clears the data stored localy.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.signOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__signedOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__signedOut<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._signedOut */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.signedOut} */
/**
 * Called when the user has signed out. `🔗 $combine`
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.signedOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__onSignedOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__onSignedOut<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._onSignedOut */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.onSignedOut} */
/**
 * Called when the user has signed out.
 *
 * Executed when `signedOut` is called. Can be used in relays.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.onSignedOut = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ISwapeeMeController.__setToken
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__setToken<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._setToken */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.setToken} */
/**
 * Sets the `token` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.setToken = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__unsetToken
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__unsetToken<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._unsetToken */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.unsetToken} */
/**
 * Clears the `token` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.unsetToken = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ISwapeeMeController.__setUserid
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__setUserid<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._setUserid */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.setUserid} */
/**
 * Sets the `userid` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.setUserid = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__unsetUserid
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__unsetUserid<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._unsetUserid */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.unsetUserid} */
/**
 * Clears the `userid` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.unsetUserid = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ISwapeeMeController.__setUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__setUsername<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._setUsername */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.setUsername} */
/**
 * Sets the `username` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.setUsername = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__unsetUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__unsetUsername<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._unsetUsername */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.unsetUsername} */
/**
 * Clears the `username` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.unsetUsername = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ISwapeeMeController.__setDisplayName
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__setDisplayName<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._setDisplayName */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.setDisplayName} */
/**
 * Sets the `displayName` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.setDisplayName = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__unsetDisplayName
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__unsetDisplayName<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._unsetDisplayName */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.unsetDisplayName} */
/**
 * Clears the `displayName` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.unsetDisplayName = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.ISwapeeMeController.__setProfile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__setProfile<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._setProfile */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.setProfile} */
/**
 * Sets the `profile` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.setProfile = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeController.__unsetProfile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeController.__unsetProfile<!xyz.swapee.wc.ISwapeeMeController>} xyz.swapee.wc.ISwapeeMeController._unsetProfile */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeController.unsetProfile} */
/**
 * Clears the `profile` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeController.unsetProfile = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/51-ISwapeeMeControllerFront.xml}  a30e1927617f0708e0ea16cec819e741 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ISwapeeMeController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeMeController)} xyz.swapee.wc.front.AbstractSwapeeMeController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeMeController} xyz.swapee.wc.front.SwapeeMeController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeMeController` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeMeController
 */
xyz.swapee.wc.front.AbstractSwapeeMeController = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeMeController.constructor&xyz.swapee.wc.front.SwapeeMeController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeMeController.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeMeController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeMeController.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeController|typeof xyz.swapee.wc.front.SwapeeMeController)|(!xyz.swapee.wc.front.ISwapeeMeControllerAT|typeof xyz.swapee.wc.front.SwapeeMeControllerAT)|!xyz.swapee.wc.front.ISwapeeMeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeMeController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeMeController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeController|typeof xyz.swapee.wc.front.SwapeeMeController)|(!xyz.swapee.wc.front.ISwapeeMeControllerAT|typeof xyz.swapee.wc.front.SwapeeMeControllerAT)|!xyz.swapee.wc.front.ISwapeeMeControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeController|typeof xyz.swapee.wc.front.SwapeeMeController)|(!xyz.swapee.wc.front.ISwapeeMeControllerAT|typeof xyz.swapee.wc.front.SwapeeMeControllerAT)|!xyz.swapee.wc.front.ISwapeeMeControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeMeController.Initialese[]) => xyz.swapee.wc.front.ISwapeeMeController} xyz.swapee.wc.front.SwapeeMeControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeControllerHyperslice
 */
xyz.swapee.wc.front.ISwapeeMeControllerHyperslice = class {
  constructor() {
    /**
     * Called when the user has signed out.
     */
    this.signedOut=/** @type {!xyz.swapee.wc.front.ISwapeeMeController._signedOut|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.ISwapeeMeController._signedOut>} */ (void 0)
  }
}
xyz.swapee.wc.front.ISwapeeMeControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.ISwapeeMeControllerHyperslice

/**
 * A concrete class of _ISwapeeMeControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeControllerHyperslice
 * @implements {xyz.swapee.wc.front.ISwapeeMeControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.front.SwapeeMeControllerHyperslice = class extends xyz.swapee.wc.front.ISwapeeMeControllerHyperslice { }
xyz.swapee.wc.front.SwapeeMeControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.SwapeeMeControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice = class {
  constructor() {
    /**
     * Called when the user has signed out.
     */
    this.signedOut=/** @type {!xyz.swapee.wc.front.ISwapeeMeController.__signedOut<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.ISwapeeMeController.__signedOut<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice

/**
 * A concrete class of _ISwapeeMeControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeControllerBindingHyperslice
 * @implements {xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.front.SwapeeMeControllerBindingHyperslice = class extends xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice { }
xyz.swapee.wc.front.SwapeeMeControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.SwapeeMeControllerBindingHyperslice

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeMeControllerCaster&xyz.swapee.wc.front.ISwapeeMeControllerAT)} xyz.swapee.wc.front.ISwapeeMeController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeControllerAT} xyz.swapee.wc.front.ISwapeeMeControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ISwapeeMeController
 */
xyz.swapee.wc.front.ISwapeeMeController = class extends /** @type {xyz.swapee.wc.front.ISwapeeMeController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ISwapeeMeControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeMeController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.signIn} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.signIn = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.signOut} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.signOut = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.signedOut} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.signedOut = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.onSignedOut} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.onSignedOut = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.setToken} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.setToken = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.unsetToken} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.unsetToken = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.setUserid} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.setUserid = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.unsetUserid} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.unsetUserid = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.setUsername} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.setUsername = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.unsetUsername} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.unsetUsername = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.setDisplayName} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.setDisplayName = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.unsetDisplayName} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.unsetDisplayName = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.setProfile} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.setProfile = function() {}
/** @type {xyz.swapee.wc.front.ISwapeeMeController.unsetProfile} */
xyz.swapee.wc.front.ISwapeeMeController.prototype.unsetProfile = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeMeController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeController.Initialese>)} xyz.swapee.wc.front.SwapeeMeController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController} xyz.swapee.wc.front.ISwapeeMeController.typeof */
/**
 * A concrete class of _ISwapeeMeController_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeController
 * @implements {xyz.swapee.wc.front.ISwapeeMeController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeController.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeMeController = class extends /** @type {xyz.swapee.wc.front.SwapeeMeController.constructor&xyz.swapee.wc.front.ISwapeeMeController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeMeController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeMeController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeMeController}
 */
xyz.swapee.wc.front.SwapeeMeController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeMeController} */
xyz.swapee.wc.front.RecordISwapeeMeController

/** @typedef {xyz.swapee.wc.front.ISwapeeMeController} xyz.swapee.wc.front.BoundISwapeeMeController */

/** @typedef {xyz.swapee.wc.front.SwapeeMeController} xyz.swapee.wc.front.BoundSwapeeMeController */

/**
 * Contains getters to cast the _ISwapeeMeController_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeControllerCaster
 */
xyz.swapee.wc.front.ISwapeeMeControllerCaster = class { }
/**
 * Cast the _ISwapeeMeController_ instance into the _BoundISwapeeMeController_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeMeController}
 */
xyz.swapee.wc.front.ISwapeeMeControllerCaster.prototype.asISwapeeMeController
/**
 * Access the _SwapeeMeController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeMeController}
 */
xyz.swapee.wc.front.ISwapeeMeControllerCaster.prototype.superSwapeeMeController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__signIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__signIn<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._signIn */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.signIn} */
/**
 * Opens the popup to sign in.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.signIn = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__signOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__signOut<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._signOut */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.signOut} */
/**
 * Clears the data stored localy.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.signOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__signedOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__signedOut<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._signedOut */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.signedOut} */
/**
 * Called when the user has signed out. `🔗 $combine` `🔗 $combine`
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.signedOut = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__onSignedOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__onSignedOut<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._onSignedOut */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.onSignedOut} */
/**
 * Called when the user has signed out.
 *
 * Executed when `signedOut` is called. Can be used in relays.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.onSignedOut = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ISwapeeMeController.__setToken
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__setToken<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._setToken */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.setToken} */
/**
 * Sets the `token` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.setToken = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__unsetToken
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__unsetToken<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._unsetToken */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.unsetToken} */
/**
 * Clears the `token` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.unsetToken = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ISwapeeMeController.__setUserid
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__setUserid<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._setUserid */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.setUserid} */
/**
 * Sets the `userid` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.setUserid = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__unsetUserid
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__unsetUserid<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._unsetUserid */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.unsetUserid} */
/**
 * Clears the `userid` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.unsetUserid = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ISwapeeMeController.__setUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__setUsername<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._setUsername */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.setUsername} */
/**
 * Sets the `username` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.setUsername = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__unsetUsername
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__unsetUsername<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._unsetUsername */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.unsetUsername} */
/**
 * Clears the `username` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.unsetUsername = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ISwapeeMeController.__setDisplayName
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__setDisplayName<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._setDisplayName */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.setDisplayName} */
/**
 * Sets the `displayName` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.setDisplayName = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__unsetDisplayName
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__unsetDisplayName<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._unsetDisplayName */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.unsetDisplayName} */
/**
 * Clears the `displayName` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.unsetDisplayName = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.ISwapeeMeController.__setProfile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__setProfile<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._setProfile */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.setProfile} */
/**
 * Sets the `profile` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.setProfile = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.ISwapeeMeController.__unsetProfile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ISwapeeMeController.__unsetProfile<!xyz.swapee.wc.front.ISwapeeMeController>} xyz.swapee.wc.front.ISwapeeMeController._unsetProfile */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeController.unsetProfile} */
/**
 * Clears the `profile` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.ISwapeeMeController.unsetProfile = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.ISwapeeMeController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/52-ISwapeeMeControllerBack.xml}  58342ddecf08ccbdd71c20e67bf85671 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ISwapeeMeController.Inputs>&xyz.swapee.wc.ISwapeeMeController.Initialese} xyz.swapee.wc.back.ISwapeeMeController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeController)} xyz.swapee.wc.back.AbstractSwapeeMeController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeController} xyz.swapee.wc.back.SwapeeMeController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeController` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeController
 */
xyz.swapee.wc.back.AbstractSwapeeMeController = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeController.constructor&xyz.swapee.wc.back.SwapeeMeController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeController.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeController.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeController|typeof xyz.swapee.wc.back.SwapeeMeController)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeController|typeof xyz.swapee.wc.back.SwapeeMeController)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeController|typeof xyz.swapee.wc.back.SwapeeMeController)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeController.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeController} xyz.swapee.wc.back.SwapeeMeControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeControllerCaster&xyz.swapee.wc.ISwapeeMeController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ISwapeeMeController.Inputs>)} xyz.swapee.wc.back.ISwapeeMeController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ISwapeeMeController
 */
xyz.swapee.wc.back.ISwapeeMeController = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeController.Initialese>)} xyz.swapee.wc.back.SwapeeMeController.constructor */
/**
 * A concrete class of _ISwapeeMeController_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeController
 * @implements {xyz.swapee.wc.back.ISwapeeMeController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeController.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeController = class extends /** @type {xyz.swapee.wc.back.SwapeeMeController.constructor&xyz.swapee.wc.back.ISwapeeMeController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeController}
 */
xyz.swapee.wc.back.SwapeeMeController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeController} */
xyz.swapee.wc.back.RecordISwapeeMeController

/** @typedef {xyz.swapee.wc.back.ISwapeeMeController} xyz.swapee.wc.back.BoundISwapeeMeController */

/** @typedef {xyz.swapee.wc.back.SwapeeMeController} xyz.swapee.wc.back.BoundSwapeeMeController */

/**
 * Contains getters to cast the _ISwapeeMeController_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeControllerCaster
 */
xyz.swapee.wc.back.ISwapeeMeControllerCaster = class { }
/**
 * Cast the _ISwapeeMeController_ instance into the _BoundISwapeeMeController_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeController}
 */
xyz.swapee.wc.back.ISwapeeMeControllerCaster.prototype.asISwapeeMeController
/**
 * Access the _SwapeeMeController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeController}
 */
xyz.swapee.wc.back.ISwapeeMeControllerCaster.prototype.superSwapeeMeController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/53-ISwapeeMeControllerAR.xml}  e817682fc9fd3a7cdb31dcf824dcf512 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeMeController.Initialese} xyz.swapee.wc.back.ISwapeeMeControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeControllerAR)} xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeControllerAR} xyz.swapee.wc.back.SwapeeMeControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeControllerAR
 */
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.constructor&xyz.swapee.wc.back.SwapeeMeControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeControllerAR|typeof xyz.swapee.wc.back.SwapeeMeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeControllerAR|typeof xyz.swapee.wc.back.SwapeeMeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeControllerAR|typeof xyz.swapee.wc.back.SwapeeMeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeController|typeof xyz.swapee.wc.SwapeeMeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeControllerAR.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeControllerAR} xyz.swapee.wc.back.SwapeeMeControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeMeController)} xyz.swapee.wc.back.ISwapeeMeControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeMeControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ISwapeeMeControllerAR
 */
xyz.swapee.wc.back.ISwapeeMeControllerAR = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeMeController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeControllerAR.Initialese>)} xyz.swapee.wc.back.SwapeeMeControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeControllerAR} xyz.swapee.wc.back.ISwapeeMeControllerAR.typeof */
/**
 * A concrete class of _ISwapeeMeControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeControllerAR
 * @implements {xyz.swapee.wc.back.ISwapeeMeControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeMeControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeControllerAR = class extends /** @type {xyz.swapee.wc.back.SwapeeMeControllerAR.constructor&xyz.swapee.wc.back.ISwapeeMeControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeControllerAR}
 */
xyz.swapee.wc.back.SwapeeMeControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeControllerAR} */
xyz.swapee.wc.back.RecordISwapeeMeControllerAR

/** @typedef {xyz.swapee.wc.back.ISwapeeMeControllerAR} xyz.swapee.wc.back.BoundISwapeeMeControllerAR */

/** @typedef {xyz.swapee.wc.back.SwapeeMeControllerAR} xyz.swapee.wc.back.BoundSwapeeMeControllerAR */

/**
 * Contains getters to cast the _ISwapeeMeControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeControllerARCaster
 */
xyz.swapee.wc.back.ISwapeeMeControllerARCaster = class { }
/**
 * Cast the _ISwapeeMeControllerAR_ instance into the _BoundISwapeeMeControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeControllerAR}
 */
xyz.swapee.wc.back.ISwapeeMeControllerARCaster.prototype.asISwapeeMeControllerAR
/**
 * Access the _SwapeeMeControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeControllerAR}
 */
xyz.swapee.wc.back.ISwapeeMeControllerARCaster.prototype.superSwapeeMeControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/54-ISwapeeMeControllerAT.xml}  d28141ad55f980100eca3d262df80fac */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ISwapeeMeControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeMeControllerAT)} xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeMeControllerAT} xyz.swapee.wc.front.SwapeeMeControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeMeControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeMeControllerAT
 */
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.constructor&xyz.swapee.wc.front.SwapeeMeControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeMeControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeControllerAT|typeof xyz.swapee.wc.front.SwapeeMeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeMeControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeControllerAT|typeof xyz.swapee.wc.front.SwapeeMeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeControllerAT|typeof xyz.swapee.wc.front.SwapeeMeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeMeControllerAT.Initialese[]) => xyz.swapee.wc.front.ISwapeeMeControllerAT} xyz.swapee.wc.front.SwapeeMeControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeMeControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ISwapeeMeControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeMeControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ISwapeeMeControllerAT
 */
xyz.swapee.wc.front.ISwapeeMeControllerAT = class extends /** @type {xyz.swapee.wc.front.ISwapeeMeControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeMeControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeMeControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeControllerAT.Initialese>)} xyz.swapee.wc.front.SwapeeMeControllerAT.constructor */
/**
 * A concrete class of _ISwapeeMeControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeControllerAT
 * @implements {xyz.swapee.wc.front.ISwapeeMeControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeMeControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeMeControllerAT = class extends /** @type {xyz.swapee.wc.front.SwapeeMeControllerAT.constructor&xyz.swapee.wc.front.ISwapeeMeControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeMeControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeMeControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeMeControllerAT}
 */
xyz.swapee.wc.front.SwapeeMeControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeMeControllerAT} */
xyz.swapee.wc.front.RecordISwapeeMeControllerAT

/** @typedef {xyz.swapee.wc.front.ISwapeeMeControllerAT} xyz.swapee.wc.front.BoundISwapeeMeControllerAT */

/** @typedef {xyz.swapee.wc.front.SwapeeMeControllerAT} xyz.swapee.wc.front.BoundSwapeeMeControllerAT */

/**
 * Contains getters to cast the _ISwapeeMeControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeControllerATCaster
 */
xyz.swapee.wc.front.ISwapeeMeControllerATCaster = class { }
/**
 * Cast the _ISwapeeMeControllerAT_ instance into the _BoundISwapeeMeControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeMeControllerAT}
 */
xyz.swapee.wc.front.ISwapeeMeControllerATCaster.prototype.asISwapeeMeControllerAT
/**
 * Access the _SwapeeMeControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeMeControllerAT}
 */
xyz.swapee.wc.front.ISwapeeMeControllerATCaster.prototype.superSwapeeMeControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/70-ISwapeeMeScreen.xml}  2bc331dbfffc201ff3ab141b144f59f1 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.front.SwapeeMeInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeMeDisplay.Settings, !xyz.swapee.wc.ISwapeeMeDisplay.Queries, null>&xyz.swapee.wc.ISwapeeMeDisplay.Initialese} xyz.swapee.wc.ISwapeeMeScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeScreen)} xyz.swapee.wc.AbstractSwapeeMeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeScreen} xyz.swapee.wc.SwapeeMeScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeScreen` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeScreen
 */
xyz.swapee.wc.AbstractSwapeeMeScreen = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeScreen.constructor&xyz.swapee.wc.SwapeeMeScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeScreen.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeScreen.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeScreen|typeof xyz.swapee.wc.SwapeeMeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeMeController|typeof xyz.swapee.wc.front.SwapeeMeController)|(!xyz.swapee.wc.ISwapeeMeDisplay|typeof xyz.swapee.wc.SwapeeMeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeScreen|typeof xyz.swapee.wc.SwapeeMeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeMeController|typeof xyz.swapee.wc.front.SwapeeMeController)|(!xyz.swapee.wc.ISwapeeMeDisplay|typeof xyz.swapee.wc.SwapeeMeDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeScreen|typeof xyz.swapee.wc.SwapeeMeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeMeController|typeof xyz.swapee.wc.front.SwapeeMeController)|(!xyz.swapee.wc.ISwapeeMeDisplay|typeof xyz.swapee.wc.SwapeeMeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeScreen.Initialese[]) => xyz.swapee.wc.ISwapeeMeScreen} xyz.swapee.wc.SwapeeMeScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.SwapeeMeMemory, !xyz.swapee.wc.front.SwapeeMeInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeMeDisplay.Settings, !xyz.swapee.wc.ISwapeeMeDisplay.Queries, null, null>&xyz.swapee.wc.front.ISwapeeMeController&xyz.swapee.wc.ISwapeeMeDisplay)} xyz.swapee.wc.ISwapeeMeScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ISwapeeMeScreen
 */
xyz.swapee.wc.ISwapeeMeScreen = class extends /** @type {xyz.swapee.wc.ISwapeeMeScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ISwapeeMeController.typeof&xyz.swapee.wc.ISwapeeMeDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeScreen.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeScreen.stashTokenInLocalStorage} */
xyz.swapee.wc.ISwapeeMeScreen.prototype.stashTokenInLocalStorage = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeScreen.stashUseridInLocalStorage} */
xyz.swapee.wc.ISwapeeMeScreen.prototype.stashUseridInLocalStorage = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeScreen.stashUsernameInLocalStorage} */
xyz.swapee.wc.ISwapeeMeScreen.prototype.stashUsernameInLocalStorage = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeScreen.stashDisplayNameInLocalStorage} */
xyz.swapee.wc.ISwapeeMeScreen.prototype.stashDisplayNameInLocalStorage = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeScreen.stashProfileInLocalStorage} */
xyz.swapee.wc.ISwapeeMeScreen.prototype.stashProfileInLocalStorage = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeScreen.openSignInPage} */
xyz.swapee.wc.ISwapeeMeScreen.prototype.openSignInPage = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeScreen.setLocal} */
xyz.swapee.wc.ISwapeeMeScreen.prototype.setLocal = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeScreen.Initialese>)} xyz.swapee.wc.SwapeeMeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen} xyz.swapee.wc.ISwapeeMeScreen.typeof */
/**
 * A concrete class of _ISwapeeMeScreen_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeScreen
 * @implements {xyz.swapee.wc.ISwapeeMeScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeScreen.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeScreen = class extends /** @type {xyz.swapee.wc.SwapeeMeScreen.constructor&xyz.swapee.wc.ISwapeeMeScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeScreen}
 */
xyz.swapee.wc.SwapeeMeScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeScreen} */
xyz.swapee.wc.RecordISwapeeMeScreen

/** @typedef {xyz.swapee.wc.ISwapeeMeScreen} xyz.swapee.wc.BoundISwapeeMeScreen */

/** @typedef {xyz.swapee.wc.SwapeeMeScreen} xyz.swapee.wc.BoundSwapeeMeScreen */

/**
 * Contains getters to cast the _ISwapeeMeScreen_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeScreenCaster
 */
xyz.swapee.wc.ISwapeeMeScreenCaster = class { }
/**
 * Cast the _ISwapeeMeScreen_ instance into the _BoundISwapeeMeScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeScreen}
 */
xyz.swapee.wc.ISwapeeMeScreenCaster.prototype.asISwapeeMeScreen
/**
 * Access the _SwapeeMeScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeScreen}
 */
xyz.swapee.wc.ISwapeeMeScreenCaster.prototype.superSwapeeMeScreen

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ISwapeeMeScreen.stashTokenInLocalStorage.Memory) => void} xyz.swapee.wc.ISwapeeMeScreen.__stashTokenInLocalStorage
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeScreen.__stashTokenInLocalStorage<!xyz.swapee.wc.ISwapeeMeScreen>} xyz.swapee.wc.ISwapeeMeScreen._stashTokenInLocalStorage */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen.stashTokenInLocalStorage} */
/**
 * The token obtained from Swapee.
 * @param {!xyz.swapee.wc.ISwapeeMeScreen.stashTokenInLocalStorage.Memory} memory The memory items.
 * - `token` _string_ The token obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Token_Safe*
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeScreen.stashTokenInLocalStorage = function(memory) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.Token_Safe} xyz.swapee.wc.ISwapeeMeScreen.stashTokenInLocalStorage.Memory The memory items. */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ISwapeeMeScreen.stashUseridInLocalStorage.Memory) => void} xyz.swapee.wc.ISwapeeMeScreen.__stashUseridInLocalStorage
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeScreen.__stashUseridInLocalStorage<!xyz.swapee.wc.ISwapeeMeScreen>} xyz.swapee.wc.ISwapeeMeScreen._stashUseridInLocalStorage */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen.stashUseridInLocalStorage} */
/**
 * The id of the user.
 * @param {!xyz.swapee.wc.ISwapeeMeScreen.stashUseridInLocalStorage.Memory} memory The memory items.
 * - `userid` _string_ The id of the user. ⤴ *ISwapeeMeOuterCore.Model.Userid_Safe*
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeScreen.stashUseridInLocalStorage = function(memory) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.Userid_Safe} xyz.swapee.wc.ISwapeeMeScreen.stashUseridInLocalStorage.Memory The memory items. */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ISwapeeMeScreen.stashUsernameInLocalStorage.Memory) => void} xyz.swapee.wc.ISwapeeMeScreen.__stashUsernameInLocalStorage
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeScreen.__stashUsernameInLocalStorage<!xyz.swapee.wc.ISwapeeMeScreen>} xyz.swapee.wc.ISwapeeMeScreen._stashUsernameInLocalStorage */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen.stashUsernameInLocalStorage} */
/**
 * The username obtained from Swapee.
 * @param {!xyz.swapee.wc.ISwapeeMeScreen.stashUsernameInLocalStorage.Memory} memory The memory items.
 * - `username` _string_ The username obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.Username_Safe*
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeScreen.stashUsernameInLocalStorage = function(memory) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.Username_Safe} xyz.swapee.wc.ISwapeeMeScreen.stashUsernameInLocalStorage.Memory The memory items. */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ISwapeeMeScreen.stashDisplayNameInLocalStorage.Memory) => void} xyz.swapee.wc.ISwapeeMeScreen.__stashDisplayNameInLocalStorage
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeScreen.__stashDisplayNameInLocalStorage<!xyz.swapee.wc.ISwapeeMeScreen>} xyz.swapee.wc.ISwapeeMeScreen._stashDisplayNameInLocalStorage */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen.stashDisplayNameInLocalStorage} */
/**
 * The display name obtained from Swapee.
 * @param {!xyz.swapee.wc.ISwapeeMeScreen.stashDisplayNameInLocalStorage.Memory} memory The memory items.
 * - `displayName` _string_ The display name obtained from Swapee. ⤴ *ISwapeeMeOuterCore.Model.DisplayName_Safe*
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeScreen.stashDisplayNameInLocalStorage = function(memory) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.DisplayName_Safe} xyz.swapee.wc.ISwapeeMeScreen.stashDisplayNameInLocalStorage.Memory The memory items. */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ISwapeeMeScreen.stashProfileInLocalStorage.Memory) => void} xyz.swapee.wc.ISwapeeMeScreen.__stashProfileInLocalStorage
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeScreen.__stashProfileInLocalStorage<!xyz.swapee.wc.ISwapeeMeScreen>} xyz.swapee.wc.ISwapeeMeScreen._stashProfileInLocalStorage */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen.stashProfileInLocalStorage} */
/**
 * The profile id.
 * @param {!xyz.swapee.wc.ISwapeeMeScreen.stashProfileInLocalStorage.Memory} memory The memory items.
 * - `profile` _string_ The profile id. ⤴ *ISwapeeMeOuterCore.Model.Profile_Safe*
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeScreen.stashProfileInLocalStorage = function(memory) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeCore.Model.Profile_Safe} xyz.swapee.wc.ISwapeeMeScreen.stashProfileInLocalStorage.Memory The memory items. */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeScreen.__openSignInPage
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeScreen.__openSignInPage<!xyz.swapee.wc.ISwapeeMeScreen>} xyz.swapee.wc.ISwapeeMeScreen._openSignInPage */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen.openSignInPage} */
/**
 * Opens the popup to sign in.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeScreen.openSignInPage = function() {}

/**
 * @typedef {(this: THIS, key: string, val: *, k: !Object) => void} xyz.swapee.wc.ISwapeeMeScreen.__setLocal
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeScreen.__setLocal<!xyz.swapee.wc.ISwapeeMeScreen>} xyz.swapee.wc.ISwapeeMeScreen._setLocal */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeScreen.setLocal} */
/**
 * Sets the local variable.
 * @param {string} key
 * @param {*} val The value.
 * @param {!Object} k The key object for debugging.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeScreen.setLocal = function(key, val, k) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeScreen
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/70-ISwapeeMeScreenBack.xml}  cf09c97e81595eac9b68c26c2d8f9909 */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese} xyz.swapee.wc.back.ISwapeeMeScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeScreen)} xyz.swapee.wc.back.AbstractSwapeeMeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeScreen} xyz.swapee.wc.back.SwapeeMeScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeScreen
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreen = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeScreen.constructor&xyz.swapee.wc.back.SwapeeMeScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeScreen.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeScreen|typeof xyz.swapee.wc.back.SwapeeMeScreen)|(!xyz.swapee.wc.back.ISwapeeMeScreenAT|typeof xyz.swapee.wc.back.SwapeeMeScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeScreen|typeof xyz.swapee.wc.back.SwapeeMeScreen)|(!xyz.swapee.wc.back.ISwapeeMeScreenAT|typeof xyz.swapee.wc.back.SwapeeMeScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeScreen|typeof xyz.swapee.wc.back.SwapeeMeScreen)|(!xyz.swapee.wc.back.ISwapeeMeScreenAT|typeof xyz.swapee.wc.back.SwapeeMeScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeScreen.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeScreen} xyz.swapee.wc.back.SwapeeMeScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeScreenCaster&xyz.swapee.wc.back.ISwapeeMeScreenAT)} xyz.swapee.wc.back.ISwapeeMeScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeScreenAT} xyz.swapee.wc.back.ISwapeeMeScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ISwapeeMeScreen
 */
xyz.swapee.wc.back.ISwapeeMeScreen = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeMeScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeScreen.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.back.ISwapeeMeScreen.openSignInPage} */
xyz.swapee.wc.back.ISwapeeMeScreen.prototype.openSignInPage = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeScreen.Initialese>)} xyz.swapee.wc.back.SwapeeMeScreen.constructor */
/**
 * A concrete class of _ISwapeeMeScreen_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeScreen
 * @implements {xyz.swapee.wc.back.ISwapeeMeScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeScreen = class extends /** @type {xyz.swapee.wc.back.SwapeeMeScreen.constructor&xyz.swapee.wc.back.ISwapeeMeScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreen}
 */
xyz.swapee.wc.back.SwapeeMeScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeScreen} */
xyz.swapee.wc.back.RecordISwapeeMeScreen

/** @typedef {xyz.swapee.wc.back.ISwapeeMeScreen} xyz.swapee.wc.back.BoundISwapeeMeScreen */

/** @typedef {xyz.swapee.wc.back.SwapeeMeScreen} xyz.swapee.wc.back.BoundSwapeeMeScreen */

/**
 * Contains getters to cast the _ISwapeeMeScreen_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeScreenCaster
 */
xyz.swapee.wc.back.ISwapeeMeScreenCaster = class { }
/**
 * Cast the _ISwapeeMeScreen_ instance into the _BoundISwapeeMeScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeScreen}
 */
xyz.swapee.wc.back.ISwapeeMeScreenCaster.prototype.asISwapeeMeScreen
/**
 * Access the _SwapeeMeScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeScreen}
 */
xyz.swapee.wc.back.ISwapeeMeScreenCaster.prototype.superSwapeeMeScreen

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.back.ISwapeeMeScreen.__openSignInPage
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeScreen.__openSignInPage<!xyz.swapee.wc.back.ISwapeeMeScreen>} xyz.swapee.wc.back.ISwapeeMeScreen._openSignInPage */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeScreen.openSignInPage} */
/**
 * Opens the popup to sign in.
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeMeScreen.openSignInPage = function() {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeMeScreen
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/73-ISwapeeMeScreenAR.xml}  98f2e256c858c752575b45718a458456 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeMeScreen.Initialese} xyz.swapee.wc.front.ISwapeeMeScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeMeScreenAR)} xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeMeScreenAR} xyz.swapee.wc.front.SwapeeMeScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeMeScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeMeScreenAR
 */
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.constructor&xyz.swapee.wc.front.SwapeeMeScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeMeScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeScreenAR|typeof xyz.swapee.wc.front.SwapeeMeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeScreen|typeof xyz.swapee.wc.SwapeeMeScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeMeScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeScreenAR|typeof xyz.swapee.wc.front.SwapeeMeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeScreen|typeof xyz.swapee.wc.SwapeeMeScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeScreenAR|typeof xyz.swapee.wc.front.SwapeeMeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeScreen|typeof xyz.swapee.wc.SwapeeMeScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeMeScreenAR.Initialese[]) => xyz.swapee.wc.front.ISwapeeMeScreenAR} xyz.swapee.wc.front.SwapeeMeScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeMeScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeMeScreen)} xyz.swapee.wc.front.ISwapeeMeScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeMeScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ISwapeeMeScreenAR
 */
xyz.swapee.wc.front.ISwapeeMeScreenAR = class extends /** @type {xyz.swapee.wc.front.ISwapeeMeScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeMeScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeMeScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeMeScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeScreenAR.Initialese>)} xyz.swapee.wc.front.SwapeeMeScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeScreenAR} xyz.swapee.wc.front.ISwapeeMeScreenAR.typeof */
/**
 * A concrete class of _ISwapeeMeScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeScreenAR
 * @implements {xyz.swapee.wc.front.ISwapeeMeScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeMeScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeMeScreenAR = class extends /** @type {xyz.swapee.wc.front.SwapeeMeScreenAR.constructor&xyz.swapee.wc.front.ISwapeeMeScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeMeScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeMeScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeMeScreenAR}
 */
xyz.swapee.wc.front.SwapeeMeScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeMeScreenAR} */
xyz.swapee.wc.front.RecordISwapeeMeScreenAR

/** @typedef {xyz.swapee.wc.front.ISwapeeMeScreenAR} xyz.swapee.wc.front.BoundISwapeeMeScreenAR */

/** @typedef {xyz.swapee.wc.front.SwapeeMeScreenAR} xyz.swapee.wc.front.BoundSwapeeMeScreenAR */

/**
 * Contains getters to cast the _ISwapeeMeScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeScreenARCaster
 */
xyz.swapee.wc.front.ISwapeeMeScreenARCaster = class { }
/**
 * Cast the _ISwapeeMeScreenAR_ instance into the _BoundISwapeeMeScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeMeScreenAR}
 */
xyz.swapee.wc.front.ISwapeeMeScreenARCaster.prototype.asISwapeeMeScreenAR
/**
 * Access the _SwapeeMeScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeMeScreenAR}
 */
xyz.swapee.wc.front.ISwapeeMeScreenARCaster.prototype.superSwapeeMeScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/74-ISwapeeMeScreenAT.xml}  d2e2a0fffc738de16f7106f136f70626 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeScreenAT)} xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeScreenAT} xyz.swapee.wc.back.SwapeeMeScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeScreenAT
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.constructor&xyz.swapee.wc.back.SwapeeMeScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeScreenAT|typeof xyz.swapee.wc.back.SwapeeMeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeScreenAT|typeof xyz.swapee.wc.back.SwapeeMeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeScreenAT|typeof xyz.swapee.wc.back.SwapeeMeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeScreenAT} xyz.swapee.wc.back.SwapeeMeScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ISwapeeMeScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeMeScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ISwapeeMeScreenAT
 */
xyz.swapee.wc.back.ISwapeeMeScreenAT = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese>)} xyz.swapee.wc.back.SwapeeMeScreenAT.constructor */
/**
 * A concrete class of _ISwapeeMeScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeScreenAT
 * @implements {xyz.swapee.wc.back.ISwapeeMeScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeMeScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeScreenAT = class extends /** @type {xyz.swapee.wc.back.SwapeeMeScreenAT.constructor&xyz.swapee.wc.back.ISwapeeMeScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeScreenAT}
 */
xyz.swapee.wc.back.SwapeeMeScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeScreenAT} */
xyz.swapee.wc.back.RecordISwapeeMeScreenAT

/** @typedef {xyz.swapee.wc.back.ISwapeeMeScreenAT} xyz.swapee.wc.back.BoundISwapeeMeScreenAT */

/** @typedef {xyz.swapee.wc.back.SwapeeMeScreenAT} xyz.swapee.wc.back.BoundSwapeeMeScreenAT */

/**
 * Contains getters to cast the _ISwapeeMeScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeScreenATCaster
 */
xyz.swapee.wc.back.ISwapeeMeScreenATCaster = class { }
/**
 * Cast the _ISwapeeMeScreenAT_ instance into the _BoundISwapeeMeScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeScreenAT}
 */
xyz.swapee.wc.back.ISwapeeMeScreenATCaster.prototype.asISwapeeMeScreenAT
/**
 * Access the _SwapeeMeScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeScreenAT}
 */
xyz.swapee.wc.back.ISwapeeMeScreenATCaster.prototype.superSwapeeMeScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me/SwapeeMe.mvc/design/80-ISwapeeMeGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeDisplay.Initialese} xyz.swapee.wc.ISwapeeMeGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeGPU)} xyz.swapee.wc.AbstractSwapeeMeGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeGPU} xyz.swapee.wc.SwapeeMeGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeGPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeGPU
 */
xyz.swapee.wc.AbstractSwapeeMeGPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeGPU.constructor&xyz.swapee.wc.SwapeeMeGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeGPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeGPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeGPU|typeof xyz.swapee.wc.SwapeeMeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeMeDisplay|typeof xyz.swapee.wc.back.SwapeeMeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeGPU|typeof xyz.swapee.wc.SwapeeMeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeMeDisplay|typeof xyz.swapee.wc.back.SwapeeMeDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeGPU|typeof xyz.swapee.wc.SwapeeMeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeMeDisplay|typeof xyz.swapee.wc.back.SwapeeMeDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeGPU.Initialese[]) => xyz.swapee.wc.ISwapeeMeGPU} xyz.swapee.wc.SwapeeMeGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeGPUCaster&com.webcircuits.IBrowserView<.!SwapeeMeMemory,>&xyz.swapee.wc.back.ISwapeeMeDisplay)} xyz.swapee.wc.ISwapeeMeGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!SwapeeMeMemory,>} com.webcircuits.IBrowserView<.!SwapeeMeMemory,>.typeof */
/**
 * Handles the periphery of the _ISwapeeMeDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ISwapeeMeGPU
 */
xyz.swapee.wc.ISwapeeMeGPU = class extends /** @type {xyz.swapee.wc.ISwapeeMeGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!SwapeeMeMemory,>.typeof&xyz.swapee.wc.back.ISwapeeMeDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeGPU.Initialese>)} xyz.swapee.wc.SwapeeMeGPU.constructor */
/**
 * A concrete class of _ISwapeeMeGPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeGPU
 * @implements {xyz.swapee.wc.ISwapeeMeGPU} Handles the periphery of the _ISwapeeMeDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeGPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeGPU = class extends /** @type {xyz.swapee.wc.SwapeeMeGPU.constructor&xyz.swapee.wc.ISwapeeMeGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeGPU}
 */
xyz.swapee.wc.SwapeeMeGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeGPU.
 * @interface xyz.swapee.wc.ISwapeeMeGPUFields
 */
xyz.swapee.wc.ISwapeeMeGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ISwapeeMeGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeGPU} */
xyz.swapee.wc.RecordISwapeeMeGPU

/** @typedef {xyz.swapee.wc.ISwapeeMeGPU} xyz.swapee.wc.BoundISwapeeMeGPU */

/** @typedef {xyz.swapee.wc.SwapeeMeGPU} xyz.swapee.wc.BoundSwapeeMeGPU */

/**
 * Contains getters to cast the _ISwapeeMeGPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeGPUCaster
 */
xyz.swapee.wc.ISwapeeMeGPUCaster = class { }
/**
 * Cast the _ISwapeeMeGPU_ instance into the _BoundISwapeeMeGPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeGPU}
 */
xyz.swapee.wc.ISwapeeMeGPUCaster.prototype.asISwapeeMeGPU
/**
 * Access the _SwapeeMeGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeGPU}
 */
xyz.swapee.wc.ISwapeeMeGPUCaster.prototype.superSwapeeMeGPU

// nss:xyz.swapee.wc
/* @typal-end */