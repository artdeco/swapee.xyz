/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ISwapeeMeComputer': {
  'id': 62004494391,
  'symbols': {},
  'methods': {
   'adaptUserpic': 1,
   'compute': 2
  }
 },
 'xyz.swapee.wc.SwapeeMeMemoryPQs': {
  'id': 62004494392,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeOuterCore': {
  'id': 62004494393,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeInputsPQs': {
  'id': 62004494394,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMePort': {
  'id': 62004494395,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeMePort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMePortInterface': {
  'id': 62004494396,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeCachePQs': {
  'id': 62004494397,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeCore': {
  'id': 62004494398,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeMeCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMeProcessor': {
  'id': 62004494399,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMe': {
  'id': 620044943910,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeBuffer': {
  'id': 620044943911,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeHtmlComponent': {
  'id': 620044943912,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeElement': {
  'id': 620044943913,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.ISwapeeMeElementPort': {
  'id': 620044943914,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeDesigner': {
  'id': 620044943915,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeMeGPU': {
  'id': 620044943916,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeDisplay': {
  'id': 620044943917,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeVdusPQs': {
  'id': 620044943918,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeDisplay': {
  'id': 620044943919,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeController': {
  'id': 620044943920,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'signOut': 2,
   'setToken': 3,
   'unsetToken': 4,
   'setUserid': 5,
   'unsetUserid': 6,
   'setUsername': 7,
   'unsetUsername': 8,
   'setDisplayName': 9,
   'unsetDisplayName': 10,
   'setProfile': 11,
   'unsetProfile': 12,
   'signedOut': 13,
   'onSignedOut': 14,
   'signIn': 15
  }
 },
 'xyz.swapee.wc.front.ISwapeeMeController': {
  'id': 620044943921,
  'symbols': {},
  'methods': {
   'signOut': 1,
   'setToken': 2,
   'unsetToken': 3,
   'setUserid': 4,
   'unsetUserid': 5,
   'setUsername': 6,
   'unsetUsername': 7,
   'setDisplayName': 8,
   'unsetDisplayName': 9,
   'setProfile': 10,
   'unsetProfile': 11,
   'signedOut': 12,
   'onSignedOut': 13,
   'signIn': 14
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeController': {
  'id': 620044943922,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeControllerAR': {
  'id': 620044943923,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeControllerAT': {
  'id': 620044943924,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeScreen': {
  'id': 620044943925,
  'symbols': {},
  'methods': {
   'stashTokenInLocalStorage': 1,
   'stashUseridInLocalStorage': 2,
   'stashUsernameInLocalStorage': 3,
   'setLocal': 4,
   'stashDisplayNameInLocalStorage': 5,
   'stashProfileInLocalStorage': 6,
   'openSignInPage': 8
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeScreen': {
  'id': 620044943926,
  'symbols': {},
  'methods': {
   'openSignInPage': 2
  }
 },
 'xyz.swapee.wc.front.ISwapeeMeScreenAR': {
  'id': 620044943927,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeScreenAT': {
  'id': 620044943928,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeClassesPQs': {
  'id': 620044943929,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeControllerHyperslice': {
  'id': 620044943930,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeControllerBindingHyperslice': {
  'id': 620044943931,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeControllerHyperslice': {
  'id': 620044943932,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeControllerBindingHyperslice': {
  'id': 620044943933,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeCPUHyperslice': {
  'id': 620044943934,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeCPUBindingHyperslice': {
  'id': 620044943935,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeCPU': {
  'id': 620044943936,
  'symbols': {},
  'methods': {
   'resetCore': 1
  }
 }
})