import SwapeeMeSharedController from '../SwapeeMeSharedController'
import AbstractSwapeeMeController from '../../gen/AbstractSwapeeMeController'

/** @extends {xyz.swapee.wc.SwapeeMeController} */
export default class extends AbstractSwapeeMeController.implements(
 SwapeeMeSharedController,
){}