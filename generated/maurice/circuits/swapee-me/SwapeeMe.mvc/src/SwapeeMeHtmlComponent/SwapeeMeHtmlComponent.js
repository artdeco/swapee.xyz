import SwapeeMeHtmlController from '../SwapeeMeHtmlController'
import SwapeeMeHtmlComputer from '../SwapeeMeHtmlComputer'
import SwapeeMeVirtualScreen from '../SwapeeMeVirtualScreen'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractSwapeeMeHtmlComponent} from '../../gen/AbstractSwapeeMeHtmlComponent'

/** @extends {xyz.swapee.wc.SwapeeMeHtmlComponent} */
export default class extends AbstractSwapeeMeHtmlComponent.implements(
 SwapeeMeHtmlController,
 SwapeeMeHtmlComputer,
 SwapeeMeVirtualScreen,
 IntegratedComponentInitialiser,
){}