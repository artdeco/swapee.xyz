import adaptUserpic from './methods/adapt-userpic'
import {preadaptUserpic} from '../../gen/AbstractSwapeeMeComputer/preadapters'
import AbstractSwapeeMeComputer from '../../gen/AbstractSwapeeMeComputer'

/** @extends {xyz.swapee.wc.SwapeeMeComputer} */
export default class SwapeeMeHtmlComputer extends AbstractSwapeeMeComputer.implements(
 /** @type {!xyz.swapee.wc.ISwapeeMeComputer} */ ({
  adaptUserpic:adaptUserpic,
  adapt:[preadaptUserpic],
 }),
){}