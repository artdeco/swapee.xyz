/** @type {xyz.swapee.wc.ISwapeeMeComputer._adaptUserpic} */
export default function adaptUserpic({profile:profile}) {
 if(!profile) return{userpic:''}
 return{
  userpic:`https://swapee.me/api/public/${profile}.webp`,
 }
}