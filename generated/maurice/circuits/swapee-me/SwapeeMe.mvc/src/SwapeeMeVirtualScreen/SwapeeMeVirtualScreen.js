import {Painter} from '@webcircuits/webcircuits'

const SwapeeMeVirtualScreen=Painter.__trait(
 /**@type {!xyz.swapee.wc.ISwapeeMeVirtualScreen}*/({
  paint:[
   function paint_stashTokenInLocalStorage({token:token},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({token:token})
    t_pa({
     pid:'5b747bb',
     mem:_mem,
     pre:_pre,
    })
   },
   function paint_stashUseridInLocalStorage({userid:userid},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({userid:userid})
    t_pa({
     pid:'079b11c',
     mem:_mem,
     pre:_pre,
    })
   },
   function paint_stashUsernameInLocalStorage({username:username},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({username:username})
    t_pa({
     pid:'92c20ae',
     mem:_mem,
     pre:_pre,
    })
   },
   function paint_stashDisplayNameInLocalStorage({displayName:displayName},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({displayName:displayName})
    t_pa({
     pid:'d62fd30',
     mem:_mem,
     pre:_pre,
    })
   },
   function paint_stashProfileInLocalStorage({profile:profile},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({profile:profile})
    t_pa({
     pid:'46a4bcf',
     mem:_mem,
     pre:_pre,
    })
   },
  ],
 }),
)
export default SwapeeMeVirtualScreen