import openSignInPage from './methods/open-sign-in-page'
import deduceInputs from './methods/deduce-inputs'
import AbstractSwapeeMeControllerAT from '../../gen/AbstractSwapeeMeControllerAT'
import paint_stashTokenInLocalStorage from '../../../parts/70-screen/src/methods/paint-stash-token-in-local-storage'
import paint_stashUseridInLocalStorage from '../../../parts/70-screen/src/methods/paint-stash-userid-in-local-storage'
import paint_stashUsernameInLocalStorage from '../../../parts/70-screen/src/methods/paint-stash-username-in-local-storage'
import paint_stashDisplayNameInLocalStorage from '../../../parts/70-screen/src/methods/paint-stash-display-name-in-local-storage'
import paint_stashProfileInLocalStorage from '../../../parts/70-screen/src/methods/paint-stash-profile-in-local-storage'
import SwapeeMeDisplay from '../SwapeeMeDisplay'
import AbstractSwapeeMeScreen from '../../gen/AbstractSwapeeMeScreen'

/** @extends {xyz.swapee.wc.SwapeeMeScreen} */
export default class extends AbstractSwapeeMeScreen.implements(
 AbstractSwapeeMeControllerAT,
 /**@type {!xyz.swapee.wc.ISwapeeMeScreen}*/({
  paint:[
   paint_stashTokenInLocalStorage,
   paint_stashUseridInLocalStorage,
   paint_stashUsernameInLocalStorage,
   paint_stashDisplayNameInLocalStorage,
   paint_stashProfileInLocalStorage,
  ],
 }),
 SwapeeMeDisplay,
 /**@type {!xyz.swapee.wc.ISwapeeMeScreen}*/({get queries(){return this.settings}}),
 /**@type {!xyz.swapee.wc.ISwapeeMeScreen}*/({
  deduceInputs(){
   const items={
    token:window.localStorage.getItem('6200449439#94a08'),
    userid:window.localStorage.getItem('6200449439#ea8f5'),
    username:window.localStorage.getItem('6200449439#14c4b'),
    displayName:window.localStorage.getItem('6200449439#4498e'),
    profile:window.localStorage.getItem('6200449439#7d974'),
   }
   return items
  },
  setLocal(key,val){
   const current=window.localStorage.getItem(key)
   if(current!=val){
    window.localStorage.setItem(key,val)
    return true
   }
  },
  stashTokenInLocalStorage({token:token}){
   const{asISwapeeMeScreen:{setLocal:setLocal}}=this
   setLocal('6200449439#94a08',token,{token:1})
  },
  stashUseridInLocalStorage({userid:userid}){
   const{asISwapeeMeScreen:{setLocal:setLocal}}=this
   setLocal('6200449439#ea8f5',userid,{userid:1})
  },
  stashUsernameInLocalStorage({username:username}){
   const{asISwapeeMeScreen:{setLocal:setLocal}}=this
   setLocal('6200449439#14c4b',username,{username:1})
  },
  stashDisplayNameInLocalStorage({displayName:displayName}){
   const{asISwapeeMeScreen:{setLocal:setLocal}}=this
   setLocal('6200449439#4498e',displayName,{displayName:1})
  },
  stashProfileInLocalStorage({profile:profile}){
   const{asISwapeeMeScreen:{setLocal:setLocal}}=this
   setLocal('6200449439#7d974',profile,{profile:1})
  },
 }),
 /** @type {!xyz.swapee.wc.ISwapeeMeScreen} */ ({
  openSignInPage:openSignInPage,
  deduceInputs:deduceInputs,
  __$id:6200449439,
 }),
){}