/** @type {xyz.swapee.wc.ISwapeeMeScreen._openSignInPage} */
export default function openSignInPage() {
 const page=`/swapee-me.html`
 const popupCenter=(url,{title,w,h}={}) => {
  // Fixes dual-screen position                             Most browsers      Firefox
  const dualScreenLeft=window.screenLeft !==  undefined?window.screenLeft : window.screenX
  const dualScreenTop = window.screenTop !==  undefined?window.screenTop  : window.screenY

  const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth?document.documentElement.clientWidth:screen.width
  const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight:screen.height

  // const systemZoom = width / window.screen.availWidth
  const left = (width - w) / 2 + dualScreenLeft
  const top = (height - h) / 2 + dualScreenTop

  window['_swapee_me_cb']=()=>{
   // now update the profile based on data in local storage
   const newData=this.deduceInputs(this.element)
   this.setInputs(Object.assign({},...newData))
  }

  const newWindow=window.open(url,title,`
    scrollbars=yes,
    width=${w},
    height=${h},
    top=${top},
    left=${left}`,
  )

  if(!newWindow) return
  if (window.focus) newWindow.focus()
  return newWindow
 }
 const popup=popupCenter(page,{w:450,h:605})
}