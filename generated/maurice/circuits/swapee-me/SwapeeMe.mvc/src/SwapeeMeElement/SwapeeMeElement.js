import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import SwapeeMeServerController from '../SwapeeMeServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractSwapeeMeElement from '../../gen/AbstractSwapeeMeElement'

/** @extends {xyz.swapee.wc.SwapeeMeElement} */
export default class SwapeeMeElement extends AbstractSwapeeMeElement.implements(
 SwapeeMeServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ISwapeeMeElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ISwapeeMeElement}*/({
   classesMap:       true,
   rootSelector:     `.SwapeeMe`,
   stylesheet:       'html/styles/SwapeeMe.css',
   blockName:        'html/SwapeeMeBlock.html',
  }),
){}

// thank you for using web circuits
