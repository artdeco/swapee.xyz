import SwapeeMeSharedController from '../SwapeeMeSharedController'
import AbstractSwapeeMeControllerBack from '../../gen/AbstractSwapeeMeControllerBack'

/** @extends {xyz.swapee.wc.back.SwapeeMeController} */
export default class extends AbstractSwapeeMeControllerBack.implements(
 SwapeeMeSharedController,
){}