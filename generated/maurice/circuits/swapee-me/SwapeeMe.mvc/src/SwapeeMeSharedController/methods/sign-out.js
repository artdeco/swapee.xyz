/** @type {xyz.swapee.wc.ISwapeeMeController._signOut} */
export default function signOut() {
 const{asISwapeeMeController:{
  unsetDisplayName,unsetProfile,unsetToken,unsetUserid,unsetUsername,//todo:groups
  signedOut:signedOut,
 }}=/**@type {!xyz.swapee.wc.ISwapeeMeController}*/(this)
 unsetDisplayName()
 unsetProfile()
 unsetToken()
 unsetUserid()
 unsetUsername()
 signedOut()
}