import signIn from './methods/sign-in'
import signOut from './methods/sign-out'
import AbstractSwapeeMeController from '../../gen/AbstractSwapeeMeController'

const SwapeeMeSharedController=AbstractSwapeeMeController.__trait(
 /** @type {!xyz.swapee.wc.ISwapeeMeController} */ ({
  signIn:signIn,
  signOut:signOut,
 }),
)
export default SwapeeMeSharedController