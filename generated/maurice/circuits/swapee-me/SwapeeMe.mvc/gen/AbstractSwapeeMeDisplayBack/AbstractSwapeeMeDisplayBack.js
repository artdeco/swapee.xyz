import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeDisplay}
 */
function __AbstractSwapeeMeDisplay() {}
__AbstractSwapeeMeDisplay.prototype = /** @type {!_AbstractSwapeeMeDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeDisplay}
 */
class _AbstractSwapeeMeDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeDisplay} ‎
 */
class AbstractSwapeeMeDisplay extends newAbstract(
 _AbstractSwapeeMeDisplay,620044943919,null,{
  asISwapeeMeDisplay:1,
  superSwapeeMeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeDisplay} */
AbstractSwapeeMeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeDisplay} */
function AbstractSwapeeMeDisplayClass(){}

export default AbstractSwapeeMeDisplay


AbstractSwapeeMeDisplay[$implementations]=[
 __AbstractSwapeeMeDisplay,
 GraphicsDriverBack,
]