import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeControllerAR}
 */
function __AbstractSwapeeMeControllerAR() {}
__AbstractSwapeeMeControllerAR.prototype = /** @type {!_AbstractSwapeeMeControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeControllerAR}
 */
class _AbstractSwapeeMeControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeMeControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeControllerAR} ‎
 */
class AbstractSwapeeMeControllerAR extends newAbstract(
 _AbstractSwapeeMeControllerAR,620044943923,null,{
  asISwapeeMeControllerAR:1,
  superSwapeeMeControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeControllerAR} */
AbstractSwapeeMeControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeControllerAR} */
function AbstractSwapeeMeControllerARClass(){}

export default AbstractSwapeeMeControllerAR


AbstractSwapeeMeControllerAR[$implementations]=[
 __AbstractSwapeeMeControllerAR,
 AR,
 AbstractSwapeeMeControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeMeControllerAR}*/({
  allocator(){
   this.methods={
    signIn:'2cef1',
    signOut:'14272',
    signedOut:'96f5e',
    onSignedOut:'2f420',
    setToken:'de478',
    unsetToken:'4c1cd',
    setUserid:'ffd73',
    unsetUserid:'6be39',
    setUsername:'6b2fc',
    unsetUsername:'c4747',
    setDisplayName:'3cab3',
    unsetDisplayName:'4403f',
    setProfile:'2c1df',
    unsetProfile:'84c8a',
   }
  },
 }),
]