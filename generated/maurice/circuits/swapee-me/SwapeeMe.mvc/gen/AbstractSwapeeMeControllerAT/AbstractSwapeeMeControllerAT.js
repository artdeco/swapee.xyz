import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeControllerAT}
 */
function __AbstractSwapeeMeControllerAT() {}
__AbstractSwapeeMeControllerAT.prototype = /** @type {!_AbstractSwapeeMeControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeControllerAT}
 */
class _AbstractSwapeeMeControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeMeControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeControllerAT} ‎
 */
class AbstractSwapeeMeControllerAT extends newAbstract(
 _AbstractSwapeeMeControllerAT,620044943924,null,{
  asISwapeeMeControllerAT:1,
  superSwapeeMeControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeControllerAT} */
AbstractSwapeeMeControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeControllerAT} */
function AbstractSwapeeMeControllerATClass(){}

export default AbstractSwapeeMeControllerAT


AbstractSwapeeMeControllerAT[$implementations]=[
 __AbstractSwapeeMeControllerAT,
 UartUniversal,
 AbstractSwapeeMeControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeMeControllerAT}*/({
  get asISwapeeMeController(){
   return this
  },
  signIn(){
   this.uart.t("inv",{mid:'2cef1'})
  },
  signOut(){
   this.uart.t("inv",{mid:'14272'})
  },
  signedOut(){
   this.uart.t("inv",{mid:'96f5e'})
  },
  onSignedOut(){
   this.uart.t("inv",{mid:'2f420'})
  },
  setToken(){
   this.uart.t("inv",{mid:'de478',args:[...arguments]})
  },
  unsetToken(){
   this.uart.t("inv",{mid:'4c1cd'})
  },
  setUserid(){
   this.uart.t("inv",{mid:'ffd73',args:[...arguments]})
  },
  unsetUserid(){
   this.uart.t("inv",{mid:'6be39'})
  },
  setUsername(){
   this.uart.t("inv",{mid:'6b2fc',args:[...arguments]})
  },
  unsetUsername(){
   this.uart.t("inv",{mid:'c4747'})
  },
  setDisplayName(){
   this.uart.t("inv",{mid:'3cab3',args:[...arguments]})
  },
  unsetDisplayName(){
   this.uart.t("inv",{mid:'4403f'})
  },
  setProfile(){
   this.uart.t("inv",{mid:'2c1df',args:[...arguments]})
  },
  unsetProfile(){
   this.uart.t("inv",{mid:'84c8a'})
  },
 }),
]