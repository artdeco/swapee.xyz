import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeScreenAR}
 */
function __AbstractSwapeeMeScreenAR() {}
__AbstractSwapeeMeScreenAR.prototype = /** @type {!_AbstractSwapeeMeScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeScreenAR}
 */
class _AbstractSwapeeMeScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeMeScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeScreenAR} ‎
 */
class AbstractSwapeeMeScreenAR extends newAbstract(
 _AbstractSwapeeMeScreenAR,620044943927,null,{
  asISwapeeMeScreenAR:1,
  superSwapeeMeScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeScreenAR} */
AbstractSwapeeMeScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeScreenAR} */
function AbstractSwapeeMeScreenARClass(){}

export default AbstractSwapeeMeScreenAR


AbstractSwapeeMeScreenAR[$implementations]=[
 __AbstractSwapeeMeScreenAR,
 AR,
 AbstractSwapeeMeScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeMeScreenAR}*/({
  allocator(){
   this.methods={
    openSignInPage:'32efa',
    setLocal:'c2d0f',
   }
  },
 }),
]
export {AbstractSwapeeMeScreenAR}