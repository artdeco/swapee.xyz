import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeDisplay}
 */
function __AbstractSwapeeMeDisplay() {}
__AbstractSwapeeMeDisplay.prototype = /** @type {!_AbstractSwapeeMeDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeDisplay}
 */
class _AbstractSwapeeMeDisplay { }
/**
 * Display for presenting information from the _ISwapeeMe_.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeDisplay} ‎
 */
class AbstractSwapeeMeDisplay extends newAbstract(
 _AbstractSwapeeMeDisplay,620044943917,null,{
  asISwapeeMeDisplay:1,
  superSwapeeMeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeDisplay} */
AbstractSwapeeMeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeDisplay} */
function AbstractSwapeeMeDisplayClass(){}

export default AbstractSwapeeMeDisplay


AbstractSwapeeMeDisplay[$implementations]=[
 __AbstractSwapeeMeDisplay,
 Display,
]