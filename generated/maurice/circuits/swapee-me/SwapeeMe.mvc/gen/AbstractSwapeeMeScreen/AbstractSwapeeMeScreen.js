import SwapeeMeClassesPQs from '../../pqs/SwapeeMeClassesPQs'
import AbstractSwapeeMeScreenAR from '../AbstractSwapeeMeScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {SwapeeMeInputsPQs} from '../../pqs/SwapeeMeInputsPQs'
import {SwapeeMeMemoryQPs} from '../../pqs/SwapeeMeMemoryQPs'
import {SwapeeMeCacheQPs} from '../../pqs/SwapeeMeCacheQPs'
import {SwapeeMeVdusPQs} from '../../pqs/SwapeeMeVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeScreen}
 */
function __AbstractSwapeeMeScreen() {}
__AbstractSwapeeMeScreen.prototype = /** @type {!_AbstractSwapeeMeScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeScreen}
 */
class _AbstractSwapeeMeScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeScreen} ‎
 */
class AbstractSwapeeMeScreen extends newAbstract(
 _AbstractSwapeeMeScreen,620044943925,null,{
  asISwapeeMeScreen:1,
  superSwapeeMeScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeScreen} */
AbstractSwapeeMeScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeScreen} */
function AbstractSwapeeMeScreenClass(){}

export default AbstractSwapeeMeScreen


AbstractSwapeeMeScreen[$implementations]=[
 __AbstractSwapeeMeScreen,
 AbstractSwapeeMeScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeScreen}*/({
  inputsPQs:SwapeeMeInputsPQs,
  classesPQs:SwapeeMeClassesPQs,
  memoryQPs:SwapeeMeMemoryQPs,
  cacheQPs:SwapeeMeCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractSwapeeMeScreenAR,
 AbstractSwapeeMeScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeScreen}*/({
  vdusPQs:SwapeeMeVdusPQs,
 }),
]