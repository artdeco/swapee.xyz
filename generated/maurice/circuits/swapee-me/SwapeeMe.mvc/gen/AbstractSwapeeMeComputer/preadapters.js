
/**@this {xyz.swapee.wc.ISwapeeMeComputer}*/
export function preadaptUserpic(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ISwapeeMeComputer.adaptUserpic.Form}*/
 const _inputs={
  profile:inputs.profile,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptUserpic(__inputs,__changes)
 return RET
}