import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeComputer}
 */
function __AbstractSwapeeMeComputer() {}
__AbstractSwapeeMeComputer.prototype = /** @type {!_AbstractSwapeeMeComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeComputer}
 */
class _AbstractSwapeeMeComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeComputer} ‎
 */
export class AbstractSwapeeMeComputer extends newAbstract(
 _AbstractSwapeeMeComputer,62004494391,null,{
  asISwapeeMeComputer:1,
  superSwapeeMeComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeComputer} */
AbstractSwapeeMeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeComputer} */
function AbstractSwapeeMeComputerClass(){}


AbstractSwapeeMeComputer[$implementations]=[
 __AbstractSwapeeMeComputer,
 Adapter,
]


export default AbstractSwapeeMeComputer