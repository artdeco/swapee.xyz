import AbstractSwapeeMeGPU from '../AbstractSwapeeMeGPU'
import AbstractSwapeeMeScreenBack from '../AbstractSwapeeMeScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {SwapeeMeInputsQPs} from '../../pqs/SwapeeMeInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeMe from '../AbstractSwapeeMe'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeHtmlComponent}
 */
function __AbstractSwapeeMeHtmlComponent() {}
__AbstractSwapeeMeHtmlComponent.prototype = /** @type {!_AbstractSwapeeMeHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeHtmlComponent}
 */
class _AbstractSwapeeMeHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.SwapeeMeHtmlComponent} */ (res)
  }
}
/**
 * The _ISwapeeMe_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeHtmlComponent} ‎
 */
export class AbstractSwapeeMeHtmlComponent extends newAbstract(
 _AbstractSwapeeMeHtmlComponent,620044943912,null,{
  asISwapeeMeHtmlComponent:1,
  superSwapeeMeHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeHtmlComponent} */
AbstractSwapeeMeHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeHtmlComponent} */
function AbstractSwapeeMeHtmlComponentClass(){}


AbstractSwapeeMeHtmlComponent[$implementations]=[
 __AbstractSwapeeMeHtmlComponent,
 HtmlComponent,
 AbstractSwapeeMe,
 AbstractSwapeeMeGPU,
 AbstractSwapeeMeScreenBack,
 AbstractSwapeeMeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeHtmlComponent}*/({
  inputsQPs:SwapeeMeInputsQPs,
 }),
]