import {makeBuffers} from '@webcircuits/webcircuits'

export const SwapeeMeBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  swapeeHost:String,
  token:String,
  userid:String,
  username:String,
  displayName:String,
  profile:String,
 }),
})

export default SwapeeMeBuffer