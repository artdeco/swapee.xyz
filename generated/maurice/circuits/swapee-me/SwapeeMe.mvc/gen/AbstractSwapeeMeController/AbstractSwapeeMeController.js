import SwapeeMeBuffer from '../SwapeeMeBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {SwapeeMePortConnector} from '../SwapeeMePort'
import {defineEmitters} from '@mauriceguest/guest2'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeController}
 */
function __AbstractSwapeeMeController() {}
__AbstractSwapeeMeController.prototype = /** @type {!_AbstractSwapeeMeController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeController}
 */
class _AbstractSwapeeMeController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeController} ‎
 */
export class AbstractSwapeeMeController extends newAbstract(
 _AbstractSwapeeMeController,620044943920,null,{
  asISwapeeMeController:1,
  superSwapeeMeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeController} */
AbstractSwapeeMeController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeController} */
function AbstractSwapeeMeControllerClass(){}


AbstractSwapeeMeController[$implementations]=[
 AbstractSwapeeMeControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.ISwapeeMePort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractSwapeeMeController,
 AbstractSwapeeMeControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeController}*/({
  signedOut:precombined,
 }),
 SwapeeMeBuffer,
 IntegratedController,
 /**@type {!AbstractSwapeeMeController}*/(SwapeeMePortConnector),
 AbstractSwapeeMeControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeController}*/({
  constructor(){
   defineEmitters(this,{
    onSignedOut:true,
   })
  },
 }),
 AbstractSwapeeMeControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeController}*/({
  setToken(val){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({token:val})
  },
  setUserid(val){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({userid:val})
  },
  setUsername(val){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({username:val})
  },
  setDisplayName(val){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({displayName:val})
  },
  setProfile(val){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({profile:val})
  },
  unsetToken(){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({token:''})
  },
  unsetUserid(){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({userid:''})
  },
  unsetUsername(){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({username:''})
  },
  unsetDisplayName(){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({displayName:''})
  },
  unsetProfile(){
   const{asISwapeeMeController:{setInputs:setInputs}}=this
   setInputs({profile:''})
  },
 }),
 AbstractSwapeeMeControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeController}*/({
  signedOut(){
   const{asISwapeeMeController:{onSignedOut:onSignedOut}}=this
   onSignedOut()
  },
 }),
]


export default AbstractSwapeeMeController