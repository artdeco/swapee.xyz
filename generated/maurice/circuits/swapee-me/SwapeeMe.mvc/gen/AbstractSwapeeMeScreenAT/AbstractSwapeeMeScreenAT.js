import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeScreenAT}
 */
function __AbstractSwapeeMeScreenAT() {}
__AbstractSwapeeMeScreenAT.prototype = /** @type {!_AbstractSwapeeMeScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeScreenAT}
 */
class _AbstractSwapeeMeScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeMeScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeScreenAT} ‎
 */
class AbstractSwapeeMeScreenAT extends newAbstract(
 _AbstractSwapeeMeScreenAT,620044943928,null,{
  asISwapeeMeScreenAT:1,
  superSwapeeMeScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreenAT} */
AbstractSwapeeMeScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreenAT} */
function AbstractSwapeeMeScreenATClass(){}

export default AbstractSwapeeMeScreenAT


AbstractSwapeeMeScreenAT[$implementations]=[
 __AbstractSwapeeMeScreenAT,
 UartUniversal,
 AbstractSwapeeMeScreenATClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeMeScreenAT}*/({
  openSignInPage(){
   const{asISwapeeMeScreenAT:{uart:uart}}=this
   uart.t('inv',{mid:'32efa'})
  },
  setLocal(){
   const{asISwapeeMeScreenAT:{uart:uart}}=this
   uart.t('inv',{mid:'c2d0f',args:[...arguments]})
  },
 }),
]