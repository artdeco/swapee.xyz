import AbstractSwapeeMeScreenAT from '../AbstractSwapeeMeScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeScreenBack}
 */
function __AbstractSwapeeMeScreenBack() {}
__AbstractSwapeeMeScreenBack.prototype = /** @type {!_AbstractSwapeeMeScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeScreen}
 */
class _AbstractSwapeeMeScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeScreen} ‎
 */
class AbstractSwapeeMeScreenBack extends newAbstract(
 _AbstractSwapeeMeScreenBack,620044943926,null,{
  asISwapeeMeScreen:1,
  superSwapeeMeScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreen} */
AbstractSwapeeMeScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeScreen} */
function AbstractSwapeeMeScreenBackClass(){}

export default AbstractSwapeeMeScreenBack


AbstractSwapeeMeScreenBack[$implementations]=[
 __AbstractSwapeeMeScreenBack,
 AbstractSwapeeMeScreenAT,
]