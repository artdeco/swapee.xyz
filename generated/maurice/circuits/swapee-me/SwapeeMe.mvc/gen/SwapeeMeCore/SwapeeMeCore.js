import {mountPins} from '@type.engineering/seers'
import {SwapeeMeMemoryPQs} from '../../pqs/SwapeeMeMemoryPQs'
import {SwapeeMeCachePQs} from '../../pqs/SwapeeMeCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMeCore}
 */
function __SwapeeMeCore() {}
__SwapeeMeCore.prototype = /** @type {!_SwapeeMeCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMeCore} */ function SwapeeMeCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeMeCore.Model}*/
  this.model={
    userpic: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeCore}
 */
class _SwapeeMeCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeCore} ‎
 */
class SwapeeMeCore extends newAbstract(
 _SwapeeMeCore,62004494398,SwapeeMeCoreConstructor,{
  asISwapeeMeCore:1,
  superSwapeeMeCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeCore} */
SwapeeMeCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeCore} */
function SwapeeMeCoreClass(){}

export default SwapeeMeCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMeOuterCore}
 */
function __SwapeeMeOuterCore() {}
__SwapeeMeOuterCore.prototype = /** @type {!_SwapeeMeOuterCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMeOuterCore} */
export function SwapeeMeOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeMeOuterCore.Model}*/
  this.model={
    swapeeHost: 'https://swapee.me/api',
    token: undefined,
    userid: '',
    username: '',
    displayName: '',
    profile: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeOuterCore}
 */
class _SwapeeMeOuterCore { }
/**
 * The _ISwapeeMe_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeOuterCore} ‎
 */
export class SwapeeMeOuterCore extends newAbstract(
 _SwapeeMeOuterCore,62004494393,SwapeeMeOuterCoreConstructor,{
  asISwapeeMeOuterCore:1,
  superSwapeeMeOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeOuterCore} */
SwapeeMeOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeOuterCore} */
function SwapeeMeOuterCoreClass(){}


SwapeeMeOuterCore[$implementations]=[
 __SwapeeMeOuterCore,
 SwapeeMeOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeOuterCore}*/({
  constructor(){
   mountPins(this.model,SwapeeMeMemoryPQs)
   mountPins(this.model,SwapeeMeCachePQs)
  },
 }),
]

SwapeeMeCore[$implementations]=[
 SwapeeMeCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeCore}*/({
  resetCore(){
   this.resetSwapeeMeCore()
  },
  resetSwapeeMeCore(){
   SwapeeMeCoreConstructor.call(
    /**@type {xyz.swapee.wc.SwapeeMeCore}*/(this),
   )
   SwapeeMeOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.SwapeeMeOuterCore}*/(
     /**@type {!xyz.swapee.wc.ISwapeeMeOuterCore}*/(this)),
   )
  },
 }),
 __SwapeeMeCore,
 SwapeeMeOuterCore,
]

export {SwapeeMeCore}