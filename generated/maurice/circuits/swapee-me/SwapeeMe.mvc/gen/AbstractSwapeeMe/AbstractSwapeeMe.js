import AbstractSwapeeMeProcessor from '../AbstractSwapeeMeProcessor'
import {SwapeeMeCore} from '../SwapeeMeCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractSwapeeMeComputer} from '../AbstractSwapeeMeComputer'
import {AbstractSwapeeMeController} from '../AbstractSwapeeMeController'
import {regulateSwapeeMeCache} from './methods/regulateSwapeeMeCache'
import {SwapeeMeCacheQPs} from '../../pqs/SwapeeMeCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMe}
 */
function __AbstractSwapeeMe() {}
__AbstractSwapeeMe.prototype = /** @type {!_AbstractSwapeeMe} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMe}
 */
class _AbstractSwapeeMe { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractSwapeeMe} ‎
 */
class AbstractSwapeeMe extends newAbstract(
 _AbstractSwapeeMe,620044943910,null,{
  asISwapeeMe:1,
  superSwapeeMe:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMe} */
AbstractSwapeeMe.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMe} */
function AbstractSwapeeMeClass(){}

export default AbstractSwapeeMe


AbstractSwapeeMe[$implementations]=[
 __AbstractSwapeeMe,
 SwapeeMeCore,
 AbstractSwapeeMeProcessor,
 IntegratedComponent,
 AbstractSwapeeMeComputer,
 AbstractSwapeeMeController,
 AbstractSwapeeMeClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMe}*/({
  regulateState:regulateSwapeeMeCache,
  stateQPs:SwapeeMeCacheQPs,
 }),
]


export {AbstractSwapeeMe}