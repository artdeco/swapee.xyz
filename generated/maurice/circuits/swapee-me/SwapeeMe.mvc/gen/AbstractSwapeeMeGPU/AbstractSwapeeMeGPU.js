import AbstractSwapeeMeDisplay from '../AbstractSwapeeMeDisplayBack'
import SwapeeMeClassesPQs from '../../pqs/SwapeeMeClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {SwapeeMeClassesQPs} from '../../pqs/SwapeeMeClassesQPs'
import {SwapeeMeVdusPQs} from '../../pqs/SwapeeMeVdusPQs'
import {SwapeeMeVdusQPs} from '../../pqs/SwapeeMeVdusQPs'
import {SwapeeMeMemoryPQs} from '../../pqs/SwapeeMeMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeGPU}
 */
function __AbstractSwapeeMeGPU() {}
__AbstractSwapeeMeGPU.prototype = /** @type {!_AbstractSwapeeMeGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeGPU}
 */
class _AbstractSwapeeMeGPU { }
/**
 * Handles the periphery of the _ISwapeeMeDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeGPU} ‎
 */
class AbstractSwapeeMeGPU extends newAbstract(
 _AbstractSwapeeMeGPU,620044943916,null,{
  asISwapeeMeGPU:1,
  superSwapeeMeGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeGPU} */
AbstractSwapeeMeGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeGPU} */
function AbstractSwapeeMeGPUClass(){}

export default AbstractSwapeeMeGPU


AbstractSwapeeMeGPU[$implementations]=[
 __AbstractSwapeeMeGPU,
 AbstractSwapeeMeGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeGPU}*/({
  classesQPs:SwapeeMeClassesQPs,
  vdusPQs:SwapeeMeVdusPQs,
  vdusQPs:SwapeeMeVdusQPs,
  memoryPQs:SwapeeMeMemoryPQs,
 }),
 AbstractSwapeeMeDisplay,
 BrowserView,
 AbstractSwapeeMeGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeGPU}*/({
  allocator(){
   pressFit(this.classes,'',SwapeeMeClassesPQs)
  },
 }),
]