import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {SwapeeMeInputsPQs} from '../../pqs/SwapeeMeInputsPQs'
import {SwapeeMeOuterCoreConstructor} from '../SwapeeMeCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMePort}
 */
function __SwapeeMePort() {}
__SwapeeMePort.prototype = /** @type {!_SwapeeMePort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMePort} */ function SwapeeMePortConstructor() {
  const self=/** @type {!xyz.swapee.wc.SwapeeMeOuterCore} */ ({model:null})
  SwapeeMeOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMePort}
 */
class _SwapeeMePort { }
/**
 * The port that serves as an interface to the _ISwapeeMe_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractSwapeeMePort} ‎
 */
export class SwapeeMePort extends newAbstract(
 _SwapeeMePort,62004494395,SwapeeMePortConstructor,{
  asISwapeeMePort:1,
  superSwapeeMePort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMePort} */
SwapeeMePort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMePort} */
function SwapeeMePortClass(){}

export const SwapeeMePortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ISwapeeMe.Pinout>}*/({
 get Port() { return SwapeeMePort },
})

SwapeeMePort[$implementations]=[
 SwapeeMePortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMePort}*/({
  resetPort(){
   this.resetSwapeeMePort()
  },
  resetSwapeeMePort(){
   SwapeeMePortConstructor.call(this)
  },
 }),
 __SwapeeMePort,
 Parametric,
 SwapeeMePortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMePort}*/({
  constructor(){
   mountPins(this.inputs,SwapeeMeInputsPQs)
  },
 }),
]


export default SwapeeMePort