import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMeElementPort}
 */
function __SwapeeMeElementPort() {}
__SwapeeMeElementPort.prototype = /** @type {!_SwapeeMeElementPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMeElementPort} */ function SwapeeMeElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeMeElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeElementPort}
 */
class _SwapeeMeElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeElementPort} ‎
 */
class SwapeeMeElementPort extends newAbstract(
 _SwapeeMeElementPort,620044943914,SwapeeMeElementPortConstructor,{
  asISwapeeMeElementPort:1,
  superSwapeeMeElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeElementPort} */
SwapeeMeElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeElementPort} */
function SwapeeMeElementPortClass(){}

export default SwapeeMeElementPort


SwapeeMeElementPort[$implementations]=[
 __SwapeeMeElementPort,
 SwapeeMeElementPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'swapee-host':undefined,
    'display-name':undefined,
   })
  },
 }),
]