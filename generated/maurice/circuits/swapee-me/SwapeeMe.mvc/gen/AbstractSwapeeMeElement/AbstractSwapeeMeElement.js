import SwapeeMeElementPort from '../SwapeeMeElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {SwapeeMeInputsPQs} from '../../pqs/SwapeeMeInputsPQs'
import {SwapeeMeCachePQs} from '../../pqs/SwapeeMeCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeMe from '../AbstractSwapeeMe'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeElement}
 */
function __AbstractSwapeeMeElement() {}
__AbstractSwapeeMeElement.prototype = /** @type {!_AbstractSwapeeMeElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeElement}
 */
class _AbstractSwapeeMeElement { }
/**
 * A component description.
 *
 * The _ISwapeeMe_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeElement} ‎
 */
class AbstractSwapeeMeElement extends newAbstract(
 _AbstractSwapeeMeElement,620044943913,null,{
  asISwapeeMeElement:1,
  superSwapeeMeElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeElement} */
AbstractSwapeeMeElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeElement} */
function AbstractSwapeeMeElementClass(){}

export default AbstractSwapeeMeElement


AbstractSwapeeMeElement[$implementations]=[
 __AbstractSwapeeMeElement,
 ElementBase,
 AbstractSwapeeMeElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':swapee-host':swapeeHostColAttr,
   ':token':tokenColAttr,
   ':userid':useridColAttr,
   ':username':usernameColAttr,
   ':display-name':displayNameColAttr,
   ':profile':profileColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'swapee-host':swapeeHostAttr,
    'token':tokenAttr,
    'userid':useridAttr,
    'username':usernameAttr,
    'display-name':displayNameAttr,
    'profile':profileAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(swapeeHostAttr===undefined?{'swapee-host':swapeeHostColAttr}:{}),
    ...(tokenAttr===undefined?{'token':tokenColAttr}:{}),
    ...(useridAttr===undefined?{'userid':useridColAttr}:{}),
    ...(usernameAttr===undefined?{'username':usernameColAttr}:{}),
    ...(displayNameAttr===undefined?{'display-name':displayNameColAttr}:{}),
    ...(profileAttr===undefined?{'profile':profileColAttr}:{}),
   }
  },
 }),
 AbstractSwapeeMeElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'swapee-host':swapeeHostAttr,
   'token':tokenAttr,
   'userid':useridAttr,
   'username':usernameAttr,
   'display-name':displayNameAttr,
   'profile':profileAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    swapeeHost:swapeeHostAttr,
    token:tokenAttr,
    userid:useridAttr,
    username:usernameAttr,
    displayName:displayNameAttr,
    profile:profileAttr,
   }
  },
 }),
 AbstractSwapeeMeElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractSwapeeMeElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeElement}*/({
  classes:{
   'UserpicIm': 'a95c3',
  },
  inputsPQs:SwapeeMeInputsPQs,
  cachePQs:SwapeeMeCachePQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractSwapeeMeElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','swapeeHost','token','userid','username','displayName','profile','no-solder',':no-solder','swapee-host',':swapee-host',':token',':userid',':username','display-name',':display-name',':profile','fe646','ab70c','94a08','ea8f5','14c4b','4498e','7d974','children']),
   })
  },
  get Port(){
   return SwapeeMeElementPort
  },
 }),
]



AbstractSwapeeMeElement[$implementations]=[AbstractSwapeeMe,
 /** @type {!AbstractSwapeeMeElement} */ ({
  rootId:'SwapeeMe',
  __$id:6200449439,
  fqn:'xyz.swapee.wc.ISwapeeMe',
  maurice_element_v3:true,
 }),
]