
import AbstractSwapeeMe from '../AbstractSwapeeMe'

/** @abstract {xyz.swapee.wc.ISwapeeMeElement} */
export default class AbstractSwapeeMeElement { }



AbstractSwapeeMeElement[$implementations]=[AbstractSwapeeMe,
 /** @type {!AbstractSwapeeMeElement} */ ({
  rootId:'SwapeeMe',
  __$id:6200449439,
  fqn:'xyz.swapee.wc.ISwapeeMe',
  maurice_element_v3:true,
 }),
]