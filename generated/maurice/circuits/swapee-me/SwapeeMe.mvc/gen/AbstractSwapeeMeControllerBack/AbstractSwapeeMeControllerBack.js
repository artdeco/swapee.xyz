import AbstractSwapeeMeControllerAR from '../AbstractSwapeeMeControllerAR'
import {AbstractSwapeeMeController} from '../AbstractSwapeeMeController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeControllerBack}
 */
function __AbstractSwapeeMeControllerBack() {}
__AbstractSwapeeMeControllerBack.prototype = /** @type {!_AbstractSwapeeMeControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeController}
 */
class _AbstractSwapeeMeControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeController} ‎
 */
class AbstractSwapeeMeControllerBack extends newAbstract(
 _AbstractSwapeeMeControllerBack,620044943922,null,{
  asISwapeeMeController:1,
  superSwapeeMeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeController} */
AbstractSwapeeMeControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeController} */
function AbstractSwapeeMeControllerBackClass(){}

export default AbstractSwapeeMeControllerBack


AbstractSwapeeMeControllerBack[$implementations]=[
 __AbstractSwapeeMeControllerBack,
 AbstractSwapeeMeController,
 AbstractSwapeeMeControllerAR,
 DriverBack,
]