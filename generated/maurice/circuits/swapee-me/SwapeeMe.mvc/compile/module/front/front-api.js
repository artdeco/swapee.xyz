import { SwapeeMeDisplay, SwapeeMeScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.SwapeeMeDisplay} */
export { SwapeeMeDisplay }
/** @lazy @api {xyz.swapee.wc.SwapeeMeScreen} */
export { SwapeeMeScreen }