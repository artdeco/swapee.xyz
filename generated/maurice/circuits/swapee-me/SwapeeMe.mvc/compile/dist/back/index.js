/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMe` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMe}
 */
class AbstractSwapeeMe extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeMe_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeMePort}
 */
class SwapeeMePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeController}
 */
class AbstractSwapeeMeController extends (class {/* lazy-loaded */}) {}
/**
 * The _ISwapeeMe_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.SwapeeMeHtmlComponent}
 */
class SwapeeMeHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeMeBuffer}
 */
class SwapeeMeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeComputer}
 */
class AbstractSwapeeMeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.SwapeeMeComputer}
 */
class SwapeeMeComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.SwapeeMeController}
 */
class SwapeeMeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeMe = AbstractSwapeeMe
module.exports.SwapeeMePort = SwapeeMePort
module.exports.AbstractSwapeeMeController = AbstractSwapeeMeController
module.exports.SwapeeMeHtmlComponent = SwapeeMeHtmlComponent
module.exports.SwapeeMeBuffer = SwapeeMeBuffer
module.exports.AbstractSwapeeMeComputer = AbstractSwapeeMeComputer
module.exports.SwapeeMeComputer = SwapeeMeComputer
module.exports.SwapeeMeController = SwapeeMeController