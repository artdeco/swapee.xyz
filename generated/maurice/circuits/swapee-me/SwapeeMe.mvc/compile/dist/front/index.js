/**
 * Display for presenting information from the _ISwapeeMe_.
 * @extends {xyz.swapee.wc.SwapeeMeDisplay}
 */
class SwapeeMeDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.SwapeeMeScreen}
 */
class SwapeeMeScreen extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeMeDisplay = SwapeeMeDisplay
module.exports.SwapeeMeScreen = SwapeeMeScreen