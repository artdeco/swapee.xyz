var d="depack-remove-start",e=d;try{if(d)throw Error();Object.setPrototypeOf(e,e);e.M=new WeakMap;e.map=new Map;e.set=new Set;Object.getOwnPropertySymbols({});Object.getOwnPropertyDescriptors({});d.includes("");[].keys();Object.values({});Object.assign({},{})}catch(a){}d="depack-remove-end";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const f=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const g=f["372700389811"];function h(a,b,c){return f["372700389812"](a,b,null,c,!1,void 0)};/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();/*

@LICENSE @webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const l=k["61893096584"],m=k["61893096586"],n=k["618930965811"],p=k["618930965812"],r=k["618930965819"];function t(){}t.prototype={};class u{}class v extends h(u,620044943917,{D:1,H:2}){}v[g]=[t,l];var w=class extends v.implements(){};function x(){((a,{title:b,w:c,l:q}={})=>{const M=((window.innerWidth?window.innerWidth:document.documentElement.clientWidth?document.documentElement.clientWidth:screen.width)-c)/2+(void 0!==window.screenLeft?window.screenLeft:window.screenX),N=((window.innerHeight?window.innerHeight:document.documentElement.clientHeight?document.documentElement.clientHeight:screen.height)-q)/2+(void 0!==window.screenTop?window.screenTop:window.screenY);window._swapee_me_cb=()=>{const O=this.deduceInputs(this.element);
this.setInputs(Object.assign({},...O))};if(a=window.open(a,b,`
    scrollbars=yes,
    width=${c},
    height=${q},
    top=${N},
    left=${M}`))return window.focus&&a.focus(),a})("/swapee-me.html",{w:450,l:605})};function y(){};function z(){}z.prototype={};class A{}class B extends h(A,620044943924,{C:1,G:2}){}B[g]=[z,n,function(){}.prototype={}];function C({i:a},b,{"94a08":c}){void 0!==c&&null!==a&&({g:{u:b}}=this,b({i:a}))}C._id="5b747bb";function D({j:a},b,{ea8f5:c}){void 0!==c&&null!==a&&({g:{v:b}}=this,b({j:a}))}D._id="079b11c";function E({username:a},b,{"14c4b":c}){void 0!==c&&null!==a&&({g:{A:b}}=this,b({username:a}))}E._id="92c20ae";function F({displayName:a},b,{"4498e":c}){void 0!==c&&null!==a&&({g:{o:b}}=this,b({displayName:a}))}F._id="d62fd30";function G({profile:a},b,{"7d974":c}){void 0!==c&&null!==a&&({g:{s:b}}=this,b({profile:a}))}G._id="46a4bcf";function H(){}H.prototype={};class I{}class J extends h(I,620044943927,{F:1,J:2}){}J[g]=[H,p,function(){}.prototype={allocator(){this.methods={m:"32efa",h:"c2d0f"}}}];const K={K:"ab70c",i:"94a08",j:"ea8f5",username:"14c4b",displayName:"4498e",profile:"7d974"};const L={...K};const P=Object.keys(K).reduce((a,b)=>{a[K[b]]=b;return a},{});const Q={L:"08906"};const R=Object.keys(Q).reduce((a,b)=>{a[Q[b]]=b;return a},{});function S(){}S.prototype={};class T{}class U extends h(T,620044943925,{g:1,I:2}){}function V(){}U[g]=[S,V.prototype={inputsPQs:L,memoryQPs:P,cacheQPs:R},m,r,J,V.prototype={vdusPQs:{}}];var W=class extends U.implements(B,{paint:[C,D,E,F,G]},w,{get queries(){return this.settings}},{deduceInputs(){return{i:window.localStorage.getItem("6200449439#94a08"),j:window.localStorage.getItem("6200449439#ea8f5"),username:window.localStorage.getItem("6200449439#14c4b"),displayName:window.localStorage.getItem("6200449439#4498e"),profile:window.localStorage.getItem("6200449439#7d974")}},h(a,b){if(window.localStorage.getItem(a)!=b)return window.localStorage.setItem(a,b),!0},u({i:a}){const {g:{h:b}}=
this;b("6200449439#94a08",a,{i:1})},v({j:a}){const {g:{h:b}}=this;b("6200449439#ea8f5",a,{j:1})},A({username:a}){const {g:{h:b}}=this;b("6200449439#14c4b",a,{username:1})},o({displayName:a}){const {g:{h:b}}=this;b("6200449439#4498e",a,{displayName:1})},s({profile:a}){const {g:{h:b}}=this;b("6200449439#7d974",a,{profile:1})}},{m:x,deduceInputs:y,__$id:6200449439}){};module.exports["620044943941"]=w;module.exports["620044943971"]=W;

//# sourceMappingURL=internal.js.map