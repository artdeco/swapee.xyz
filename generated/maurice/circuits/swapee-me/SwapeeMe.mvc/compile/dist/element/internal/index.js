import Module from './element'

/**@extends {xyz.swapee.wc.AbstractSwapeeMe}*/
export class AbstractSwapeeMe extends Module['62004494391'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMe} */
AbstractSwapeeMe.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeMePort} */
export const SwapeeMePort=Module['62004494393']
/**@extends {xyz.swapee.wc.AbstractSwapeeMeController}*/
export class AbstractSwapeeMeController extends Module['62004494394'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeController} */
AbstractSwapeeMeController.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeMeElement} */
export const SwapeeMeElement=Module['62004494398']
/** @type {typeof xyz.swapee.wc.SwapeeMeBuffer} */
export const SwapeeMeBuffer=Module['620044943911']
/**@extends {xyz.swapee.wc.AbstractSwapeeMeComputer}*/
export class AbstractSwapeeMeComputer extends Module['620044943930'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeComputer} */
AbstractSwapeeMeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeMeController} */
export const SwapeeMeController=Module['620044943961']