import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {6200449439} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function g(a,b,e,f){return c["372700389812"](a,b,e,f,!1,void 0)}const h=c.precombined;function m(){}m.prototype={};class n{}class p extends g(n,62004494399,null,{R:1,ea:2}){}p[d]=[m];


const q=function(){return require(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];const r={j:"ab70c",i:"94a08",l:"ea8f5",username:"14c4b",displayName:"4498e",profile:"7d974"};const t={o:"08906"};function u(){}u.prototype={};function v(){this.model={o:null}}class aa{}class w extends g(aa,62004494398,v,{I:1,Z:2}){}function y(){}y.prototype={};function z(){this.model={j:"https://swapee.me/api",i:void 0,l:"",username:"",displayName:"",profile:""}}class ba{}class A extends g(ba,62004494393,z,{M:1,ba:2}){}A[d]=[y,{constructor(){q(this.model,r);q(this.model,t)}}];w[d]=[{},u,A];

const B=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ca=B.defineEmitters,da=B.IntegratedComponentInitialiser,C=B.IntegratedComponent,ea=B["95173443851"];
const D=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const E=D["615055805212"],fa=D["615055805218"];function F(){}F.prototype={};class ha{}class G extends g(ha,62004494391,null,{H:1,X:2}){}G[d]=[F,fa];const H={regulate:E({j:String,i:String,l:String,username:String,displayName:String,profile:String})};
const I=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ia=I.IntegratedController,ja=I.Parametric;const J={...r};function K(){}K.prototype={};function L(){const a={model:null};z.call(a);this.inputs=a.model}class ka{}class M extends g(ka,62004494395,L,{P:1,da:2}){}function N(){}M[d]=[N.prototype={resetPort(){L.call(this)}},K,ja,N.prototype={constructor(){q(this.inputs,J)}}];function O(){}O.prototype={};class la{}class P extends g(la,620044943920,null,{g:1,Y:2}){}function Q(){}
P[d]=[Q.prototype={resetPort(){this.port.resetPort()}},O,Q.prototype={m:h},H,ia,{get Port(){return M}},Q.prototype={constructor(){ca(this,{s:!0})}},Q.prototype={A(){const {g:{setInputs:a}}=this;a({i:""})},D(){const {g:{setInputs:a}}=this;a({l:""})},F(){const {g:{setInputs:a}}=this;a({username:""})},u(){const {g:{setInputs:a}}=this;a({displayName:""})},v(){const {g:{setInputs:a}}=this;a({profile:""})}},Q.prototype={m(){const {g:{s:a}}=this;a()}}];const ma=E({o:String},{silent:!0});const na=Object.keys(t).reduce((a,b)=>{a[t[b]]=b;return a},{});function R(){}R.prototype={};class oa{}class S extends g(oa,620044943910,null,{G:1,W:2}){}S[d]=[R,w,p,C,G,P,{regulateState:ma,stateQPs:na}];function pa({i:a,j:b}){return{j:b,i:a}};const qa=require(eval('"@type.engineering/web-computing"')).h;function ra(){return qa("div",{$id:"SwapeeMe"})};const sa=require(eval('"@type.engineering/web-computing"')).h;function ta(){return sa("div",{$id:"SwapeeMe"})};const ua=P.__trait({U:function(){return this.T()},V:function(){const {g:{u:a,v:b,A:e,D:f,F:k,m:l}}=this;a();b();e();f();k();l()}});var T=class extends P.implements(ua){};require("https");require("http");const va=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}va("aqt");require("fs");require("child_process");
const U=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const wa=U.ElementBase,xa=U.HTMLBlocker;function V(){}V.prototype={};function ya(){this.inputs={noSolder:!1}}class za{}class W extends g(za,620044943914,ya,{K:1,aa:2}){}W[d]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"swapee-host":void 0,"display-name":void 0})}}];function X(){}X.prototype={};class Aa{}class Y extends g(Aa,620044943913,null,{J:1,$:2}){}function Z(){}
Y[d]=[X,wa,Z.prototype={calibrate:function({":no-solder":a,":swapee-host":b,":token":e,":userid":f,":username":k,":display-name":l,":profile":x}){const {attributes:{"no-solder":Ba,"swapee-host":Ca,token:Da,userid:Ea,username:Fa,"display-name":Ga,profile:Ha}}=this;return{...(void 0===Ba?{"no-solder":a}:{}),...(void 0===Ca?{"swapee-host":b}:{}),...(void 0===Da?{token:e}:{}),...(void 0===Ea?{userid:f}:{}),...(void 0===Fa?{username:k}:{}),...(void 0===Ga?{"display-name":l}:{}),...(void 0===Ha?{profile:x}:
{})}}},Z.prototype={calibrate:({"no-solder":a,"swapee-host":b,token:e,userid:f,username:k,"display-name":l,profile:x})=>({noSolder:a,j:b,i:e,l:f,username:k,displayName:l,profile:x})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={classes:{UserpicIm:"a95c3"},inputsPQs:J,cachePQs:t,vdus:{}},C,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder swapeeHost token userid username displayName profile no-solder :no-solder swapee-host :swapee-host :token :userid :username display-name :display-name :profile fe646 ab70c 94a08 ea8f5 14c4b 4498e 7d974 children".split(" "))})},
get Port(){return W}}];Y[d]=[S,{rootId:"SwapeeMe",__$id:6200449439,fqn:"xyz.swapee.wc.ISwapeeMe",maurice_element_v3:!0}];class Ia extends Y.implements(T,ea,xa,da,{solder:pa,server:ra,render:ta},{classesMap:!0,rootSelector:".SwapeeMe",stylesheet:"html/styles/SwapeeMe.css",blockName:"html/SwapeeMeBlock.html"}){};module.exports["62004494390"]=S;module.exports["62004494391"]=S;module.exports["62004494393"]=M;module.exports["62004494394"]=P;module.exports["62004494398"]=Ia;module.exports["620044943911"]=H;module.exports["620044943930"]=G;module.exports["620044943961"]=T;
/*! @embed-object-end {6200449439} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule