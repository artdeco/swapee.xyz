/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMe` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMe}
 */
class AbstractSwapeeMe extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeMe_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeMePort}
 */
class SwapeeMePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeController}
 */
class AbstractSwapeeMeController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ISwapeeMe_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.SwapeeMeElement}
 */
class SwapeeMeElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeMeBuffer}
 */
class SwapeeMeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeComputer}
 */
class AbstractSwapeeMeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.SwapeeMeController}
 */
class SwapeeMeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeMe = AbstractSwapeeMe
module.exports.SwapeeMePort = SwapeeMePort
module.exports.AbstractSwapeeMeController = AbstractSwapeeMeController
module.exports.SwapeeMeElement = SwapeeMeElement
module.exports.SwapeeMeBuffer = SwapeeMeBuffer
module.exports.AbstractSwapeeMeComputer = AbstractSwapeeMeComputer
module.exports.SwapeeMeController = SwapeeMeController

Object.defineProperties(module.exports, {
 'AbstractSwapeeMe': {get: () => require('./precompile/internal')[62004494391]},
 [62004494391]: {get: () => module.exports['AbstractSwapeeMe']},
 'SwapeeMePort': {get: () => require('./precompile/internal')[62004494393]},
 [62004494393]: {get: () => module.exports['SwapeeMePort']},
 'AbstractSwapeeMeController': {get: () => require('./precompile/internal')[62004494394]},
 [62004494394]: {get: () => module.exports['AbstractSwapeeMeController']},
 'SwapeeMeElement': {get: () => require('./precompile/internal')[62004494398]},
 [62004494398]: {get: () => module.exports['SwapeeMeElement']},
 'SwapeeMeBuffer': {get: () => require('./precompile/internal')[620044943911]},
 [620044943911]: {get: () => module.exports['SwapeeMeBuffer']},
 'AbstractSwapeeMeComputer': {get: () => require('./precompile/internal')[620044943930]},
 [620044943930]: {get: () => module.exports['AbstractSwapeeMeComputer']},
 'SwapeeMeController': {get: () => require('./precompile/internal')[620044943961]},
 [620044943961]: {get: () => module.exports['SwapeeMeController']},
})