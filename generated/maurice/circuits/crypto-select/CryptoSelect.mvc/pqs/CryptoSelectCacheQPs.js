import {CryptoSelectCachePQs} from './CryptoSelectCachePQs'
export const CryptoSelectCacheQPs=/**@type {!xyz.swapee.wc.CryptoSelectCacheQPs}*/(Object.keys(CryptoSelectCachePQs)
 .reduce((a,k)=>{a[CryptoSelectCachePQs[k]]=k;return a},{}))