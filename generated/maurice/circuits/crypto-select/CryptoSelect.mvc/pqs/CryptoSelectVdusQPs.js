import {CryptoSelectVdusPQs} from './CryptoSelectVdusPQs'
export const CryptoSelectVdusQPs=/**@type {!xyz.swapee.wc.CryptoSelectVdusQPs}*/(Object.keys(CryptoSelectVdusPQs)
 .reduce((a,k)=>{a[CryptoSelectVdusPQs[k]]=k;return a},{}))