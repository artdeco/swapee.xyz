import {CryptoSelectMemoryPQs} from './CryptoSelectMemoryPQs'
export const CryptoSelectMemoryQPs=/**@type {!xyz.swapee.wc.CryptoSelectMemoryQPs}*/(Object.keys(CryptoSelectMemoryPQs)
 .reduce((a,k)=>{a[CryptoSelectMemoryPQs[k]]=k;return a},{}))