export const CryptoSelectVdusPQs=/**@type {!xyz.swapee.wc.CryptoSelectVdusPQs}*/({
 CryptoSelectedBl:'a3b31',
 CryptoSelectedImWr:'a3b32',
 CryptoSelectedNameIn:'a3b33',
 CryptoMenu:'a3b34',
 MenuItems:'a3b35',
 CryptoDropItem:'a3b36',
 ChevronUp:'a3b37',
 ChevronDown:'a3b38',
 NoCryptoDropItem:'a3b39',
 CryptoDown:'a3b310',
 Popup:'a3b311',
 InnerSpan:'a3b312',
})