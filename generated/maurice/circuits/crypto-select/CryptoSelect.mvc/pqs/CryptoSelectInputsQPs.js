import {CryptoSelectInputsPQs} from './CryptoSelectInputsPQs'
export const CryptoSelectInputsQPs=/**@type {!xyz.swapee.wc.CryptoSelectInputsQPs}*/(Object.keys(CryptoSelectInputsPQs)
 .reduce((a,k)=>{a[CryptoSelectInputsPQs[k]]=k;return a},{}))