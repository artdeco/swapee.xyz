import CryptoSelectClassesPQs from './CryptoSelectClassesPQs'
export const CryptoSelectClassesQPs=/**@type {!xyz.swapee.wc.CryptoSelectClassesQPs}*/(Object.keys(CryptoSelectClassesPQs)
 .reduce((a,k)=>{a[CryptoSelectClassesPQs[k]]=k;return a},{}))