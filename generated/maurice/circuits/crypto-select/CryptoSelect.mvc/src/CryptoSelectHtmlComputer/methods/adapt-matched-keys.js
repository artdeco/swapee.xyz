/** @type {xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys} */
export const adaptMatchedKeys=({
 search:search,cryptos:cryptos,ignoreCryptos:ignoreCryptos,
})=>{
 const re=new RegExp(search,'i')
 const matchedKeys=new Set
 for(const key of cryptos.keys()) {
  const{displayName:displayName}=cryptos.get(key)
  if(re.test(key)||re.test(displayName)) {
   matchedKeys.add(key)
  }
 }
 for(const key of ignoreCryptos) {
  matchedKeys.delete(key)
 }
 return{
  matchedKeys:matchedKeys,
 }
}

export default adaptMatchedKeys