/** @type {xyz.swapee.wc.ICryptoSelectComputer._adaptPopupShown} */
export default function adaptPopupShown({menuExpanded:menuExpanded}){
 return{
  shown:menuExpanded,
 }
}