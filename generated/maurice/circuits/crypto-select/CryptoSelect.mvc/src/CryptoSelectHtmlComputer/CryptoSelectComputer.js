import adaptPopupShown from './methods/adapt-popup-shown'
import adaptSearchInput from './methods/adapt-search-input'
import adaptMatchedKeys from './methods/adapt-matched-keys'
import CryptoSelectSharedComputer from '../CryptoSelectSharedComputer'
import {preadaptPopupShown,preadaptSearchInput,preadaptMatchedKeys} from '../../gen/AbstractCryptoSelectComputer/preadapters'
import AbstractCryptoSelectComputer from '../../gen/AbstractCryptoSelectComputer'

/** @extends {xyz.swapee.wc.CryptoSelectComputer} */
export default class CryptoSelectHtmlComputer extends AbstractCryptoSelectComputer.implements(
 CryptoSelectSharedComputer,
 /** @type {!xyz.swapee.wc.ICryptoSelectComputer} */ ({
  adaptPopupShown:adaptPopupShown,
  adaptSearchInput:adaptSearchInput,
  adaptMatchedKeys:adaptMatchedKeys,
  adapt:[preadaptPopupShown,preadaptSearchInput,preadaptMatchedKeys],
 }),
){}