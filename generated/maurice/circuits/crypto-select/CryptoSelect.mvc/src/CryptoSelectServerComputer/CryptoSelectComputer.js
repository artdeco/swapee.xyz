import CryptoSelectSharedComputer from '../CryptoSelectSharedComputer'
import AbstractCryptoSelectComputer from '../../gen/AbstractCryptoSelectComputer'

/** @extends {xyz.swapee.wc.CryptoSelectComputer} */
export default class CryptoSelectServerComputer extends AbstractCryptoSelectComputer.implements(
 CryptoSelectSharedComputer,
){}