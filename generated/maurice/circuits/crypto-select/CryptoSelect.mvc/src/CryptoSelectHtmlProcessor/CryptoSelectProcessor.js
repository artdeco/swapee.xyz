import flipMenuExpanded from './methods/flip-menu-expanded'
import AbstractCryptoSelectProcessor from '../../gen/AbstractCryptoSelectProcessor'

/** @extends {xyz.swapee.wc.CryptoSelectProcessor} */
export default class extends AbstractCryptoSelectProcessor.implements(
 /** @type {!xyz.swapee.wc.ICryptoSelectProcessor} */ ({
  flipMenuExpanded:flipMenuExpanded,
 }),
){}