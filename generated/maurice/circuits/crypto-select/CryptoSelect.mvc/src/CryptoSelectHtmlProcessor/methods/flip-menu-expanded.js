/** @type {xyz.swapee.wc.ICryptoSelectProcessor._flipMenuExpanded} */
export default function flipMenuExpanded() {
 const{model:{menuExpanded},asICryptoSelectController:{setInputs}}=/**@type {!xyz.swapee.wc.ICryptoSelectProcessor}*/(this)
 setInputs({menuExpanded:!menuExpanded})
}