import {Painter} from '@webcircuits/webcircuits'

const CryptoSelectVirtualScreen=Painter.__trait(
 /**@type {!xyz.swapee.wc.ICryptoSelectVirtualScreen}*/({
  paint:[
   function paint_stashSelectedInLocalStorage({selected:selected},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({selected:selected})
    t_pa({
     pid:'d3f701d',
     mem:_mem,
     pre:_pre,
    })
   },
   function paint_stashIsMenuExpanded({menuExpanded:menuExpanded},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({menuExpanded:menuExpanded})
    t_pa({
     pid:'cdf68a5',
     mem:_mem,
     pre:_pre,
    })
   },
   function paint_stashKeyboardItemAfterMenuExpanded({menuExpanded:menuExpanded,selected:selected},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({menuExpanded:menuExpanded,selected:selected})
    t_pa({
     pid:'4f3ddab',
     mem:_mem,
     pre:_pre,
    })
   },
   function paint_stashVisibleItems({matchedKeys:matchedKeys},_,prev){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _pre={}
    Object.assign(_pre,serMemory(prev))
    const _mem=serMemory({matchedKeys:matchedKeys})
    t_pa({
     pid:'c136d87',
     mem:_mem,
     pre:_pre,
    })
   },
  ],
 }),
)
export default CryptoSelectVirtualScreen