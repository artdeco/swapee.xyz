/** @type {xyz.swapee.wc.ICryptoSelectDisplay._paintBeforeHovering} */
export default function paintBeforeHovering({
 hoveringIndex:hoveringIndex,
}) {
 const{asICryptoSelectDisplay:{
  resolveItem:resolveItem,
  CryptoDown:CryptoDown,
 }}=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 const before=/**@type {HTMLElement} */(CryptoDown.querySelector(`[data-previous-hover]`))
 if(before) {
  before.dataset['previousHover']=''
  delete before.dataset['previousHover']
  before.style.borderBottomColor=''
  delete before.style.borderBottomColor
 }

 if(hoveringIndex==-1){
  return
 }

 const prevHoveringIndex=hoveringIndex-1
 const previousItem=resolveItem(prevHoveringIndex)

 if(previousItem) {
  previousItem.style.borderBottomColor='#521d71'//'#5d2b7a'
  previousItem.dataset['previousHover']='true'
 }
}