/** @type {xyz.swapee.wc.ICryptoSelectDisplay._paintHoveringIndex} */
export default function paintHoveringIndex({
 hoveringIndex:hoveringIndex,
}) {
 const{
  asICryptoSelectTouchscreen: {
   asICryptoSelectDisplay:{
    CryptoDown,
    scrollItemIntoView:scrollItemIntoView,
    resolveItem:resolveItem,
   },
   classes:{
    ItemHovered:ItemHovered,
   },
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 const before=CryptoDown.querySelectorAll(`.${ItemHovered}`)
 ;[...before].forEach((b)=>{
  b.classList.remove(ItemHovered)
 })

 const item=resolveItem(hoveringIndex)
 if(item) {
  item.classList.add(ItemHovered)
  scrollItemIntoView(item)
 }
}