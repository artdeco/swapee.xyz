/** @type {xyz.swapee.wc.ICryptoSelectDisplay._paintSearchInput} */
export default function paintSearchInput({
 selected:selected,
 menuExpanded:menuExpanded,
 selectedCrypto:selectedCrypto,
 sm:sm,
}){
 const{
  asICryptoSelectDisplay:{
   CryptoSelectedNameIn:CryptoSelectedNameIn,
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 // debugger
 if(menuExpanded) { // just menuExpanded
  CryptoSelectedNameIn.value=''
 }else { // just collapsed
  CryptoSelectedNameIn.value=!sm?selectedCrypto.displayName||selected:selected
 }
}