/** @type {xyz.swapee.wc.ICryptoSelectDisplay._resolveItemByKey} */
export default function resolveItemByKey(key) {
 const{
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 const item=CryptoDown.querySelector(`[data-value="${key}"]`)
 return /** @type {!HTMLElement} */(item)||null
}