/** @type {xyz.swapee.wc.ICryptoSelectDisplay._resolveMouseItem} */
export default function resolveMouseItem(ev) {
 const{
  asICryptoSelectTouchscreen:{
   classes:{
    CryptoDropItem:CryptoDropItem,
   },
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 const _item=/**@type {!HTMLElement}*/(ev.target)
 const item=_item.classList.contains(CryptoDropItem)?_item:_item.closest(`.${CryptoDropItem}`)
 return/**@type {!HTMLElement} */(item)||null
}