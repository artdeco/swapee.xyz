/** @type {xyz.swapee.wc.ICryptoSelectDisplay._paintSmHeight} */
export default function paintSmHeight({
 matchedKeys:matchedKeys,
}) { //
 return
 const{
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 let h=''
 const{size:size}=matchedKeys
 // debugger
 if(size==1||size==0) h=`1.35rem`
 if(size==2) h=`2.85rem`
 if(size==3) h=`4.25rem`
 if(size==4) h=`5.65rem`
 // if(size==5) h=`5.85rem`
 if(size==5||matchedKeys.size>5) h=`7.15rem`
 // remove
 const tr=CryptoDown.style.transition
 CryptoDown.style.transition='none'
 CryptoDown.style.height=h
 // CryptoDown.style.height=h
 setTimeout(()=>{
  setTimeout(()=>{
   CryptoDown.style.transition=tr
  },1)
 },1)
}