/** @type {xyz.swapee.wc.ICryptoSelectDisplay._resolveItem} */
export default function resolveItem(index) {
 const{
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 const item=CryptoDown.querySelector(`[data-i="${index}"]`)
 return /** @type {!HTMLElement} */(item)||null
}