/** @type {xyz.swapee.wc.ICryptoSelectDisplay._paintSelectedImg} */
export default function paintSelectedImg({selected:selected}) {
 // if(!prevSelected) return
 if(selected) {
  const{
   asICryptoSelectDisplay:{
    CryptoSelectedImWr:CryptoSelectedImWr,
    CryptoDown:CryptoDown,
   },
   asICryptoSelectTouchscreen:{
    setInputs:setInputs,
    classes:{CryptoDropItem:CryptoDropItem,ImgWr:ImgWr},
   },
  }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
  if(CryptoSelectedImWr.dataset['selected']==selected) return
  else CryptoSelectedImWr.removeAttribute('data-selected')

  const item=CryptoDown.querySelector(`.${CryptoDropItem}[data-value=${selected}]`)
  const imgWr=item.querySelector(`.${ImgWr}`)
  setInputs({selectedIcon:imgWr.innerHTML})
 }
}