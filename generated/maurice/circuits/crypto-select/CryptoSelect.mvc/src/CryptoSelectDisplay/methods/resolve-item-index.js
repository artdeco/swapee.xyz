/** @type {xyz.swapee.wc.ICryptoSelectDisplay._resolveItemIndex} */
export default function resolveItemIndex(item) {
 const i =item.dataset['i']
 if(i) return parseFloat(i)
 return -1
}