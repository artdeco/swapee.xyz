/** @type {xyz.swapee.wc.ICryptoSelectDisplay._scrollItemIntoView} */
export default function scrollItemIntoView(item) {
 const{
  asICryptoSelectDisplay:{
   resolveItem:resolveItem,
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 const clientRect=item.getBoundingClientRect()
 const parentRect=item.parentElement.getBoundingClientRect()
 //  console.log(item,item.parentElement,item.parentElement.scrollTop)
 const HEIGHT_IN_ITEMS=Math.floor(parentRect.height/clientRect.height)

 if(clientRect.y>parentRect.y+parentRect.height) {
  // scroll
  item.scrollIntoView({
   behavior:'smooth',
   block:'nearest',
   inline:'nearest',
  })
  return
 }

 if(clientRect.y+clientRect.height>parentRect.y+parentRect.height) {
  item.scrollIntoView({
   behavior:'smooth',
   block:'nearest',
   inline:'nearest',
  })
  return
  // ends after the scroll
 }

 if(clientRect.y<parentRect.y) {
  // item.parentElement.offsetTop
  // get item at
  const ind=Math.round(item.parentElement.scrollTop/clientRect.height)
  let newInd=ind-HEIGHT_IN_ITEMS
  if(newInd<-1) newInd=0
  // console.log('ind',ind.newInd)
  const i=resolveItem(newInd)
  // debugger
  if(i) i.scrollIntoView({
   block:'nearest',
   behavior:'smooth',
   inline:'nearest',
  })
  else item.scrollIntoView({
   behavior:'smooth',
   inline:'nearest',
   block:'nearest',
  })
  // debugger
 }
}