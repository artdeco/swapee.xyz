/** @type {xyz.swapee.wc.ICryptoSelectDisplay._paintSearch} */
export default function paintSearch({
 matchedKeys:matchedKeys,
}) {
 const{
  asICryptoSelectTouchscreen:{
   classes:{CryptoDropItem:CryptoDropItem},
  },
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/(this)
 /** @type {!Array<!HTMLDivElement>} */
 const items=[...CryptoDown.querySelectorAll(`.${CryptoDropItem}`)]
 for(const item of items) {
  const d=item.dataset['value']
  if(!d) return
  if(matchedKeys.has(d)) {
   item.style.display=''
  }else {
   item.style.display='none'
  }
 }
}