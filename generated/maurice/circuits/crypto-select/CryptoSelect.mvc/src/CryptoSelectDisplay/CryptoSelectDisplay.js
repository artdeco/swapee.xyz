import paintSmHeight from './methods/paint-sm-height'
import paintHoveringIndex from './methods/paint-hovering-index'
import paintSelectedImg from './methods/paint-selected-img'
import paintSearchInput from './methods/paint-search-input'
import paintBeforeHovering from './methods/paint-before-hovering'
import paintSearch from './methods/paint-search'
import scrollItemIntoView from './methods/scroll-item-into-view'
import resolveItemIndex from './methods/resolve-item-index'
import resolveItemByKey from './methods/resolve-item-by-key'
import resolveItem from './methods/resolve-item'
import resolveMouseItem from './methods/resolve-mouse-item'
import prepaintSmHeight from '../../../parts/40-display/src/methods/prepaint-sm-height'
import prepaintSearch from '../../../parts/40-display/src/methods/prepaint-search'
import prepaintSelectedImg from '../../../parts/40-display/src/methods/prepaint-selected-img'
import prepaintHoveringIndex from '../../../parts/40-display/src/methods/prepaint-hovering-index'
import prepaintBeforeHovering from '../../../parts/40-display/src/methods/prepaint-before-hovering'
import prepaintSearchInput from '../../../parts/40-display/src/methods/prepaint-search-input'
import AbstractCryptoSelectDisplay from '../../gen/AbstractCryptoSelectDisplay'

/** @extends {xyz.swapee.wc.CryptoSelectDisplay} */
export default class extends AbstractCryptoSelectDisplay.implements(
 /**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/({
  paint:[
   prepaintSmHeight,
   prepaintSearch,
   prepaintSelectedImg,
   prepaintHoveringIndex,
   prepaintBeforeHovering,
   prepaintSearchInput,
  ],
 }),
 /** @type {!xyz.swapee.wc.ICryptoSelectDisplay} */ ({
  paintSmHeight:paintSmHeight,
  paintHoveringIndex:paintHoveringIndex,
  paintSelectedImg:paintSelectedImg,
  paintSearchInput:paintSearchInput,
  paintBeforeHovering:paintBeforeHovering,
  paintSearch:paintSearch,
  scrollItemIntoView:scrollItemIntoView,
  resolveItemIndex:resolveItemIndex,
  resolveItemByKey:resolveItemByKey,
  resolveItem:resolveItem,
  resolveMouseItem:resolveMouseItem,
 }),
/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/({
   // need to analyse paint rather than prepaint for the virtual method
  }),
){}