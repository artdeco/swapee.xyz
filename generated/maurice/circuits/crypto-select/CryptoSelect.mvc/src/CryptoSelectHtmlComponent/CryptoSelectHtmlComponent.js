import CryptoSelectHtmlController from '../CryptoSelectHtmlController'
import CryptoSelectHtmlComputer from '../CryptoSelectHtmlComputer'
import CryptoSelectHtmlProcessor from '../CryptoSelectHtmlProcessor'
import CryptoSelectVirtualDisplay from '../CryptoSelectVirtualDisplay'
import CryptoSelectVirtualScreen from '../CryptoSelectVirtualScreen'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractCryptoSelectHtmlComponent} from '../../gen/AbstractCryptoSelectHtmlComponent'

/** @extends {xyz.swapee.wc.CryptoSelectHtmlComponent} */
export default class extends AbstractCryptoSelectHtmlComponent.implements(
 CryptoSelectHtmlController,
 CryptoSelectHtmlComputer,
 CryptoSelectHtmlProcessor,
 CryptoSelectVirtualDisplay,
 CryptoSelectVirtualScreen,
 IntegratedComponentInitialiser,
){}