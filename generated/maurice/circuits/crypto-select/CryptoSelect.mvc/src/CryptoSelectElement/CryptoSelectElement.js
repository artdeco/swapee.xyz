import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import CryptoSelectServerController from '../CryptoSelectServerController'
import CryptoSelectServerComputer from '../CryptoSelectServerComputer'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractCryptoSelectElement from '../../gen/AbstractCryptoSelectElement'

/** @extends {xyz.swapee.wc.CryptoSelectElement} */
export default class CryptoSelectElement extends AbstractCryptoSelectElement.implements(
 CryptoSelectServerController,
 CryptoSelectServerComputer,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ICryptoSelectElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
   classesMap: true,
   rootSelector:     `.CryptoSelect`,
   stylesheets:       [
    'html/styles/CryptoSelect.css',
    'html/styles/KeyboardSelect.css',
    'html/styles/CryptoPill.css',
    'html/styles/Highlight.css',
    'html/styles/Chevron.css',
    'html/styles/CryptoDropItem.css',
    'html/styles/Item.css',
    'html/styles/FiatIcon.css',
    'html/styles/BackgroundStalk.css',
    // 'html/styles/Sm.css',
    // 'html/styles/Menu.css',
    'html/styles/3D.css',
    // background stalk can be realised as a directive, if variable names are exposed.
   ],
   blockName: 'html/CryptoSelectBlock.html',
   // short(_,{Popup}){},
  }),
){}

// thank you for using web circuits
