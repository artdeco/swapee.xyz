/** @type {xyz.swapee.wc.ICryptoSelectElement._render} */
export default function CryptoSelectRender({
 fiat:fiat,isMouseOver:isMouseOver,hoveringIndex:hoveringIndex,
 selectedIcon:selectedIcon,
 menuExpanded:menuExpanded,matchedKeys:matchedKeys,sm:sm,wideness:wideness,
},{
 onInputFocus,onInputBlur,onInputKeyDown,onMouseLeave,inputKeyboardScroll,
 inputKeyUp,addMouseOver,selectedBlCl,menuMouseMv,menuItemCl,
}) {
 let ev
 return (<div $id="CryptoSelect" onMouseLeave={onMouseLeave(ev)}
  Sm={sm} SmWide={sm&&(wideness=='wide')} SmWider={sm&&(wideness=='wider')}
  Expanded={menuExpanded}
 >
  <div $id="CryptoSelectedBl" onClick={selectedBlCl(ev)}
   MouseOver={isMouseOver} onMouseEnter={addMouseOver(ev)}
   Fiat={fiat} Expanded={menuExpanded}
  >
   <span $id="CryptoSelectedImWr">{selectedIcon}</span>
   <input $id="CryptoSelectedNameIn" onFocus={onInputFocus} onBlur={onInputBlur}
    onKeyDown={[onInputKeyDown(ev),inputKeyboardScroll(ev)]}
    onKeyUp={inputKeyUp(ev)} />
  </div>
  <div $id="CryptoMenu" HoveringOverItem={hoveringIndex!=-1}>
   <div $id="MenuItem"
    onMouseMove={menuMouseMv(ev)} onMouseDown={menuItemCl(ev)}>
   </div>
  </div>
  <div $id="CryptoDropItem" />
  {/* todo: make it one vdu that rotates 180deg */}
  <img $id="ChevronUp" $reveal={menuExpanded} />
  <img $id="ChevronDown" $conceal={menuExpanded} />
  <div $id="NoCryptoDropItem" $conceal={matchedKeys.size} />
 </div>)
}