/** @type {xyz.swapee.wc.ICryptoSelectElement._server} */
export default function server({
 selectedCrypto:selectedCrypto,selected:selected,
 fiat:fiat,cryptos:cryptos,iconFolder:iconFolder,sm:sm,
}) {
 if(!selectedCrypto){
  selectedCrypto={}
 }
  // throw new Error(`Have to select correct crypto via selectedCrypto. "${selected}" key not found on the map.`)

 const{asIElement:{elementRelative},asICacher:{md5:md5}}=this

 const selectedIconSrc=selected?elementRelative(`html/${iconFolder}/${selected}.png`):null
 // const selectedCrypto=cryptos.get(selected)
 const selectedBrightness=0//this.brightnessMap.get(selected)
 const CRYPTO_NAME=fiat?(selectedCrypto.name||selected):(selectedCrypto.displayName||selected)

 const keys=cryptos?[...cryptos.keys()]:[]

 return(<div $id="CryptoSelect">
  <div $id="CryptoSelectedBl" CryptoSelectedBl>
   <span $id="CryptoSelectedImWr" FiatIcon={fiat} data-selected={selected}>
    <splendid-img filter={selectedBrightness?`brightness(${selectedBrightness})`:undefined}
     src={selectedIconSrc?`app://${selectedIconSrc}`:void 0} alt={CRYPTO_NAME} title={CRYPTO_NAME} />
   </span>
   <input $id="CryptoSelectedNameIn" value={!sm?CRYPTO_NAME||selected:selected} />
  </div>

  <div $id="CryptoDown" CryptoDown $cacheId={
   (fiat?'fiat':'crypto')+(sm?'-sm':'')
  } $cacheBlock={md5(keys.join('')+3)}>
   {keys.reduce((acc,key,i)=>{
    const iconSrc=elementRelative('html',iconFolder,`${key}.png`)
    const val=cryptos.get(key)
    // const brightness=this.brightnessMap.get(key)
    const NAME=(fiat?val.name:val.displayName)

    // const style=`background-image:url(${val.icon}).`
    acc.push('\n',<div $id="a3b35" CryptoDropItem BackgroundStalked z-index={cryptos.size-i}
     data-value={key} data-name={NAME} data-i={i}
    >
     <span ItemWr title={NAME||key}>
      <span ImgWr FiatIcon={fiat}>
       <splendid-img
        src={`app://${iconSrc}`} alt={NAME||key} />
      </span>
      <span ImgWr KeyboardSelect FiatIcon={fiat}>
       <splendid-img src={`app://${iconSrc}`} alt={''} />
      </span>
      <div LabelWr>
       <div >{!sm?NAME||key:key}</div>
       {val.network?<div NetworkLabel>{val.network}</div>:null}
      </div>
     </span>
    </div>)
    return acc
   },[])}

   {/* NoCryptoDropItem */}
   <div $id="a3b39" CryptoDropItem>
    <span ItemWr>
     <splendid-img FiatIcon={fiat} src={'app://'+elementRelative('./html/img/trex2.svg')} alt="404" />
     <span NoCoins>
      {sm?`${fiat?'No fiats':'No coins'}`:`No ${fiat?'fiats':'coins'} found`}
     </span>
    </span>
   </div>
  </div>
 </div>)
}