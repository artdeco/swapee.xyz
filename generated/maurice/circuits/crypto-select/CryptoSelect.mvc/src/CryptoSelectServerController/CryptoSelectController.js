import calibrateDataSource from './methods/calibrate-data-source'
import {precalibrateDataSource} from '../../gen/AbstractCryptoSelectController/precalibrators'
import AbstractCryptoSelectController from '../../gen/AbstractCryptoSelectController'

/** @extends {xyz.swapee.wc.CryptoSelectController} */
export default class extends AbstractCryptoSelectController.implements(
 /** @type {!xyz.swapee.wc.ICryptoSelectController} */ ({
  calibrateDataSource:calibrateDataSource,
  calibrate:[precalibrateDataSource],
 }),
){}