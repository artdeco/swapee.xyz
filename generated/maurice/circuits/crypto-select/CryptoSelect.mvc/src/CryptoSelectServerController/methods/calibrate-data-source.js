/** @type {xyz.swapee.wc.ICryptoSelectController._calibrateDataSource} */
export default async function calibrateDataSource({source:source}){
 const{
  asIElement:{elementRelative:elementRelative},
  asIApper:{readApp:readApp},
 }=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/(this)
 /**@type {!Map}*/
 let cryptos
 let path='',f
 if(source=='onramper-cryptos') {
  f='cryptos'
  path=elementRelative('./data/cryptos.json')
 }else if (source=='onramper-fiats') {
  f='fiats'
  path=elementRelative('./data/fiats.json')
 }
 if(!path) return

 const data=await readApp(path)
 const _cryptos=JSON.parse(data)
 // const _cryptos=require1('./test/fixture/cryptos.json')
 cryptos=new Map(_cryptos)
 console.log('Loaded %s %s locally', cryptos.size, f)

 if(!cryptos) return

 return {
  cryptos:cryptos,
 }
}