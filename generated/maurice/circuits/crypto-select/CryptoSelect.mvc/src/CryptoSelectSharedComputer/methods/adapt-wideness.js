/** @type {xyz.swapee.wc.ICryptoSelectComputer.adaptWideness} */
export const adaptWideness=({
 selected:selected,
}) => {
 if(selected.length==4) {
  return{
   wideness:'wider',
  }
 }
 if(selected.length>3) {
  return{
   wideness:'wide',
  }
 }
 return{
  wideness:'',
 }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQTBjd0QsQ0FBQztDQUNBLFFBQVEsQ0FBQyxRQUFRO0FBQ2xCLENBQUMsRUFBRSxDQUFDLEVBQUU7Q0FDTCxFQUFFLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtFQUN0QixNQUFNO0dBQ0wsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDO0VBQ2pCO0NBQ0Q7Q0FDQSxFQUFFLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUU7RUFDckIsTUFBTTtHQUNMLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQztFQUNoQjtDQUNEO0NBQ0EsTUFBTTtFQUNMLFFBQVEsQ0FBQyxDQUFDLENBQUM7Q0FDWjtBQUNEIn0=

export default adaptWideness