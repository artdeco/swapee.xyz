import adaptWideness from './methods/adapt-wideness'
import adaptSelectedCrypto from './methods/adapt-selected-crypto'
import {preadaptWideness,preadaptSelectedCrypto} from '../../gen/AbstractCryptoSelectComputer/preadapters'
import AbstractCryptoSelectComputer from '../../gen/AbstractCryptoSelectComputer'

const CryptoSelectSharedComputer=AbstractCryptoSelectComputer.__trait(
 /** @type {!xyz.swapee.wc.ICryptoSelectComputer} */ ({
  adaptWideness:adaptWideness,
  adaptSelectedCrypto:adaptSelectedCrypto,
  adapt:[preadaptWideness,preadaptSelectedCrypto],
 }),
)
export default CryptoSelectSharedComputer