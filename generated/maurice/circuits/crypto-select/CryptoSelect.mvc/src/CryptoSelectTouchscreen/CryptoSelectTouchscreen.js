import deduceInputs from './methods/deduce-inputs'
import stashSelectedInLocalStorage from './methods/stash-selected-in-local-storage'
import stashIsMenuExpanded from './methods/stash-is-menu-expanded'
import stashVisibleItems from './methods/stash-visible-items'
import stashKeyboardItemAfterMenuExpanded from './methods/stash-keyboard-item-after-menu-expanded'
import AbstractCryptoSelectControllerAT from '../../gen/AbstractCryptoSelectControllerAT'
import HyperCryptoSelectInterruptLine from '../../../parts/120-interrupt-line/src/CryptoSelectInterruptLine/aop/HyperCryptoSelectInterruptLine'
import paint_stashSelectedInLocalStorage from '../../../parts/70-touchscreen/src/methods/paint-stash-selected-in-local-storage'
import paint_stashIsMenuExpanded from '../../../parts/70-touchscreen/src/methods/paint-stash-is-menu-expanded'
import paint_stashKeyboardItemAfterMenuExpanded from '../../../parts/70-touchscreen/src/methods/paint-stash-keyboard-item-after-menu-expanded'
import paint_stashVisibleItems from '../../../parts/70-touchscreen/src/methods/paint-stash-visible-items'
import CryptoSelectDisplay from '../CryptoSelectDisplay'
import AbstractCryptoSelectTouchscreen from '../../gen/AbstractCryptoSelectTouchscreen'

/** @extends {xyz.swapee.wc.CryptoSelectTouchscreen} */
export default class extends AbstractCryptoSelectTouchscreen.implements(
 AbstractCryptoSelectControllerAT,
 HyperCryptoSelectInterruptLine,
 /**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({
  paint:[
   paint_stashSelectedInLocalStorage,
   paint_stashIsMenuExpanded,
   paint_stashKeyboardItemAfterMenuExpanded,
   paint_stashVisibleItems,
  ],
 }),
 CryptoSelectDisplay,
 /**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ICryptoSelectTouchscreen} */ ({
  deduceInputs:deduceInputs,
  stashSelectedInLocalStorage:stashSelectedInLocalStorage,
  stashIsMenuExpanded:stashIsMenuExpanded,
  stashVisibleItems:stashVisibleItems,
  stashKeyboardItemAfterMenuExpanded:stashKeyboardItemAfterMenuExpanded,
  __$id:3545350742,
 }),
/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({
   // paint({menuExpanded:menuExpanded}){
   //  this._menuExpanded=menuExpanded
   // },
  }),
){}