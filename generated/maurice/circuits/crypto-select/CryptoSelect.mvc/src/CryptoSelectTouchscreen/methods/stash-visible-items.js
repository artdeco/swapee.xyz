/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashVisibleItems} */
export default function stashVisibleItems({
 matchedKeys:matchedKeys,
}) {
 // console.log('stash visible items')
 const{
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
  asICryptoSelectTouchscreen:{
   classes:{CryptoDropItem:CryptoDropItem},
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/(this)
 const items=[...CryptoDown.querySelectorAll(`.${CryptoDropItem}`)]
 const visibleItems=items.filter((item)=>{
  return matchedKeys.has(item.dataset['value'])
 })
 return {
  visibleItems:visibleItems,
 }
}