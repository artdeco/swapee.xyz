/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashSelectedInLocalStorage} */
export default function stashSelectedInLocalStorage({
 selected:selected,
}) {
 const{element:{id:id}}=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/(this)
 if(!id) return
 const current=window.localStorage.getItem(`#${id}.selected`)
 if(current!=selected) {
  //  console.log('updating local storage to', selected)
  window.localStorage.setItem(`#${id}.selected`, selected)
 }
}