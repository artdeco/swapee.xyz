/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashIsMenuExpanded} */
export default function stashIsMenuExpanded({menuExpanded:menuExpanded}){
 return{
  isMenuExpanded:menuExpanded,
 }
}