/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashKeyboardItemAfterMenuExpanded} */
export default function stashKeyboardItemAfterMenuExpanded({
 selected:selected,
 menuExpanded:menuExpanded,
}) {
 const{
  asICryptoSelectDisplay:{resolveItemByKey:resolveItemByKey},
 }=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/(this)
 if(!menuExpanded) {
  return{
   keyboardItem:null,
  }
 }
 const keyboardItem=resolveItemByKey(selected)
 return{
  keyboardItem:keyboardItem,
 }
}