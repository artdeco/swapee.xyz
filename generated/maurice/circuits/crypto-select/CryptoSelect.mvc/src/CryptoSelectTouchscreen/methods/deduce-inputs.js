/** @type {!com.webcircuits.IPortFrontend._deduceInputs} */
export default function deduceInputs(el){
 const{
  // asICryptoSelectDisplay:{
  //  CryptoSelectedImWr:CryptoSelectedImWr,
  // },
  asICryptoSelectTouchscreen:{
   classes:{
    Sm:Sm,
    CryptoDropItem:CryptoDropItem,
   },
  },
 }=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/(this)
 const items=[...el.querySelectorAll(`.${CryptoDropItem}`)].map(
  (cryptoItemEl)=>{
   const key=cryptoItemEl.dataset['value']
   if(!key) return
   const displayName=cryptoItemEl.dataset['name']
   return [key, { displayName: displayName }]
  }).filter(Boolean)
 // const imHtml=CryptoSelectedImWr?CryptoSelectedImWr.innerHTML:undefined
 const cryptos=new Map(items)

 const sm=el.classList.contains(Sm)
 // const sm=classList.contains(classes.SmWide)
 // console.log(123)

 let selected
 if(el.id) {
 //  debugger
  const localSelected=window.localStorage.getItem(`#${el.id}.selected`)
  if(localSelected) {
   selected=localSelected
  }
 }
 return {
 //  imHtml:imHtml,1
  cryptos:cryptos,
  sm:sm,
  ...(selected?{selected:selected}:{}),
  // selected:selected,
  // menuExpanded:true,
 }
}