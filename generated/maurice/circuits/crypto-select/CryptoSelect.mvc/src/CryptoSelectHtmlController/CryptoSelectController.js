import calibrateResetMouseOver from './methods/calibrate-reset-mouse-over'
import {precalibrateResetMouseOver} from '../../gen/AbstractCryptoSelectController/precalibrators'
import AbstractCryptoSelectControllerBack from '../../gen/AbstractCryptoSelectControllerBack'

/** @extends {xyz.swapee.wc.back.CryptoSelectController} */
export default class extends AbstractCryptoSelectControllerBack.implements(
 /** @type {!xyz.swapee.wc.back.ICryptoSelectController} */ ({
  calibrateResetMouseOver:calibrateResetMouseOver,
  calibrate:[precalibrateResetMouseOver],
 }),
){}