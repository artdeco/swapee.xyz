/** @type {xyz.swapee.wc.ICryptoSelectController._calibrateResetMouseOver} */
export default async function calibrateResetMouseOver() {
 await new Promise(r=>setTimeout(r,1250))
 return {
  isMouseOver:false,
 }
}