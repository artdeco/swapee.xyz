import {Painter} from '@webcircuits/webcircuits'

const CryptoSelectVirtualDisplay=Painter.__trait(
 /**@type {!xyz.swapee.wc.ICryptoSelectVirtualDisplay}*/({
  paint:[
   function paintSmHeight({matchedKeys:matchedKeys}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _mem=serMemory({matchedKeys:matchedKeys})
    t_pa({
     pid:'3b37ffd',
     mem:_mem,
    })
   },
   function paintSearch({matchedKeys:matchedKeys}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _mem=serMemory({matchedKeys:matchedKeys})
    t_pa({
     pid:'21817fe',
     mem:_mem,
    })
   },
   function paintSelectedImg({selected:selected}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _mem=serMemory({selected:selected})
    t_pa({
     pid:'124a057',
     mem:_mem,
    })
   },
   function paintHoveringIndex({hoveringIndex:hoveringIndex}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _mem=serMemory({hoveringIndex:hoveringIndex})
    t_pa({
     pid:'3efb8ee',
     mem:_mem,
    })
   },
   function paintBeforeHovering({hoveringIndex:hoveringIndex}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _mem=serMemory({hoveringIndex:hoveringIndex})
    t_pa({
     pid:'480f48f',
     mem:_mem,
    })
   },
   function paintSearchInput({selectedCrypto:selectedCrypto,menuExpanded:menuExpanded,selected:selected,sm:sm}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _mem=serMemory({selectedCrypto:selectedCrypto,menuExpanded:menuExpanded,selected:selected,sm:sm})
    t_pa({
     pid:'5e737b7',
     mem:_mem,
    })
   },
  ],
 }),
)
export default CryptoSelectVirtualDisplay