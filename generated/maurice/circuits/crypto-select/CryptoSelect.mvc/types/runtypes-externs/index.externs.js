/**
 * @fileoverview
 * @externs
 */

xyz.swapee.wc.ICryptoSelectCore={}
xyz.swapee.wc.ICryptoSelectCore.Model={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ICryptoSelectController={}
xyz.swapee.wc.ICryptoSelectDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ICryptoSelectDisplay={}
xyz.swapee.wc.ICryptoSelectController={}
xyz.swapee.wc.ICryptoSelectController.Inputs={}
xyz.swapee.wc.ICryptoSelectController.WeakInputs={}
xyz.swapee.wc.ICryptoSelectTouchscreen={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.wc={}
$$xyz.swapee.wc.ICryptoSelectCore={}
$$xyz.swapee.wc.ICryptoSelectDisplay={}
$$xyz.swapee.wc.back={}
$$xyz.swapee.wc.back.ICryptoSelectDisplay={}
$$xyz.swapee.wc.ICryptoSelectController={}
$$xyz.swapee.wc.front={}
$$xyz.swapee.wc.front.ICryptoSelectController={}
$$xyz.swapee.wc.ICryptoSelectTouchscreen={}
$$xyz.swapee.wc.ICryptoSelectComputer={}
$$xyz.swapee.wc.ICryptoSelectPort={}
$$xyz.swapee.wc.ICryptoSelectHtmlComponentUtil={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
xyz.swapee.wc.ICryptoSelectComputer.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @interface */
xyz.swapee.wc.ICryptoSelectComputerFields

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @interface */
xyz.swapee.wc.ICryptoSelectComputerCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectComputer} */
xyz.swapee.wc.ICryptoSelectComputerCaster.prototype.asICryptoSelectComputer
/** @type {!xyz.swapee.wc.BoundCryptoSelectComputer} */
xyz.swapee.wc.ICryptoSelectComputerCaster.prototype.superCryptoSelectComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectComputerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.CryptoSelectLand>}
 */
xyz.swapee.wc.ICryptoSelectComputer = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init */
xyz.swapee.wc.ICryptoSelectComputer.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)}
 */
xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptWideness = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)}
 */
xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptSelectedCrypto = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)}
 */
xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptSearchInput = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)}
 */
xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptPopupShown = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)}
 */
xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptMatchedKeys = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.CryptoSelectMemory} mem
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.compute.Land} land
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectComputer.prototype.compute = function(mem, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.CryptoSelectComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectComputer.Initialese>}
 */
xyz.swapee.wc.CryptoSelectComputer = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init */
xyz.swapee.wc.CryptoSelectComputer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.CryptoSelectComputer.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.AbstractCryptoSelectComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.CryptoSelectComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectComputer, ...!xyz.swapee.wc.ICryptoSelectComputer.Initialese)} */
xyz.swapee.wc.CryptoSelectComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.RecordICryptoSelectComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @typedef {{ adaptWideness: xyz.swapee.wc.ICryptoSelectComputer.adaptWideness, adaptSelectedCrypto: xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto, adaptSearchInput: xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput, adaptPopupShown: xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown, adaptMatchedKeys: xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys, compute: xyz.swapee.wc.ICryptoSelectComputer.compute }} */
xyz.swapee.wc.RecordICryptoSelectComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.BoundICryptoSelectComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectComputerFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.CryptoSelectLand>}
 */
xyz.swapee.wc.BoundICryptoSelectComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.BoundCryptoSelectComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptWideness exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectComputer.__adaptWideness = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)} */
xyz.swapee.wc.ICryptoSelectComputer.adaptWideness
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectComputer, !xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)} */
xyz.swapee.wc.ICryptoSelectComputer._adaptWideness
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectComputer.__adaptWideness} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptWideness

// nss:xyz.swapee.wc.ICryptoSelectComputer,$$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectComputer.__adaptSelectedCrypto = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectComputer, !xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)} */
xyz.swapee.wc.ICryptoSelectComputer._adaptSelectedCrypto
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectComputer.__adaptSelectedCrypto} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptSelectedCrypto

// nss:xyz.swapee.wc.ICryptoSelectComputer,$$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectComputer.__adaptSearchInput = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectComputer, !xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)} */
xyz.swapee.wc.ICryptoSelectComputer._adaptSearchInput
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectComputer.__adaptSearchInput} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptSearchInput

// nss:xyz.swapee.wc.ICryptoSelectComputer,$$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectComputer.__adaptPopupShown = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)} */
xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectComputer, !xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)} */
xyz.swapee.wc.ICryptoSelectComputer._adaptPopupShown
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectComputer.__adaptPopupShown} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptPopupShown

// nss:xyz.swapee.wc.ICryptoSelectComputer,$$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectComputer.__adaptMatchedKeys = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)} */
xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectComputer, !xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form, xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form): (undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)} */
xyz.swapee.wc.ICryptoSelectComputer._adaptMatchedKeys
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectComputer.__adaptMatchedKeys} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptMatchedKeys

// nss:xyz.swapee.wc.ICryptoSelectComputer,$$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.CryptoSelectMemory} mem
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.compute.Land} land
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectComputer.__compute = function(mem, land) {}
/** @typedef {function(xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectComputer.compute.Land): void} */
xyz.swapee.wc.ICryptoSelectComputer.compute
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectComputer, xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectComputer.compute.Land): void} */
xyz.swapee.wc.ICryptoSelectComputer._compute
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectComputer.__compute} */
xyz.swapee.wc.ICryptoSelectComputer.__compute

// nss:xyz.swapee.wc.ICryptoSelectComputer,$$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe.prototype.selected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Wideness exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @record */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness.prototype.wideness

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Wideness}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe = function() {}
/** @type {Map<string, { icon: string, displayName: string, name: string }>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe.prototype.cryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto = function() {}
/** @type {(?{ icon: string, displayName: string, name: string })|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto.prototype.selectedCrypto

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe.prototype.menuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search.prototype.search

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Search exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Search = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Search}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {com.webcircuits.ui.IPopupPort.Inputs.Shown}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe.prototype.search

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe = function() {}
/** @type {!Set<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe.prototype.ignoreCryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys = function() {}
/** @type {(!Set<string>)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys.prototype.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys}
 */
xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.compute.Land exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @record */
xyz.swapee.wc.ICryptoSelectComputer.compute.Land = function() {}
/** @type {!Object} */
xyz.swapee.wc.ICryptoSelectComputer.compute.Land.prototype.Popup

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @interface */
xyz.swapee.wc.ICryptoSelectOuterCoreFields
/** @type {!xyz.swapee.wc.ICryptoSelectOuterCore.Model} */
xyz.swapee.wc.ICryptoSelectOuterCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @interface */
xyz.swapee.wc.ICryptoSelectOuterCoreCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectOuterCore} */
xyz.swapee.wc.ICryptoSelectOuterCoreCaster.prototype.asICryptoSelectOuterCore
/** @type {!xyz.swapee.wc.BoundCryptoSelectOuterCore} */
xyz.swapee.wc.ICryptoSelectOuterCoreCaster.prototype.superCryptoSelectOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreCaster}
 */
xyz.swapee.wc.ICryptoSelectOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.CryptoSelectOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectOuterCore.Initialese>}
 */
xyz.swapee.wc.CryptoSelectOuterCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.CryptoSelectOuterCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.AbstractCryptoSelectOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore = function() {}
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.CryptoSelectMemoryPQs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.selected
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.iconFolder
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.fiat
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.imHtml
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.search
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.menuExpanded
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.isMouseOver
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.sm
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.isSearching
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.hoveringIndex
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.cryptos
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.visibleItems
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.keyboardSelected
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.selectedCrypto
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.CryptoSelectMemoryQPs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.ef7de
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f756a
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.hd5f3
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.b593a
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.a6a94
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.d353a
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.h1df4
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.ed79a
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f8419
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.c7b81
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.ja8a8
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.adbda
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f1606
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.b1a11
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f86aa

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.RecordICryptoSelectOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.BoundICryptoSelectOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreCaster}
 */
xyz.swapee.wc.BoundICryptoSelectOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.BoundCryptoSelectOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected.selected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected.selected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder.iconFolder exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder.iconFolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat.fiat exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat.fiat

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml.imHtml exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml.imHtml

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search.search exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search.search

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon.selectedIcon exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon.selectedIcon

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded.menuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded.menuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver.isMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver.isMouseOver

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm.sm exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm.sm

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching.isSearching exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching.isSearching

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex.hoveringIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {number} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex.hoveringIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos.cryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Map<string, { icon: string, displayName: string, name: string }>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos.cryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos.ignoreCryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Set<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos.ignoreCryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems.visibleItems exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Array<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems.visibleItems

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected.keyboardSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected.keyboardSelected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto.selectedCrypto exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {{ icon: string, displayName: string, name: string }} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto.selectedCrypto

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys.matchedKeys exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Set<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected.prototype.selected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder.prototype.iconFolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat.prototype.fiat

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml.prototype.imHtml

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon.prototype.selectedIcon

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded.prototype.menuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver.prototype.isMouseOver

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm.prototype.sm

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching.prototype.isSearching

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex = function() {}
/** @type {number|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex.prototype.hoveringIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos = function() {}
/** @type {(Map<string, { icon: string, displayName: string, name: string }>)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos.prototype.cryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos = function() {}
/** @type {(!Set<string>)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos.prototype.ignoreCryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems = function() {}
/** @type {(!Array<string>)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems.prototype.visibleItems

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected.prototype.keyboardSelected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys}
 */
xyz.swapee.wc.ICryptoSelectOuterCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected.prototype.selected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder.prototype.iconFolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat.prototype.fiat

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml.prototype.imHtml

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search.prototype.search

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon.prototype.selectedIcon

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded.prototype.menuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver.prototype.isMouseOver

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm.prototype.sm

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching.prototype.isSearching

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex.prototype.hoveringIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos.prototype.cryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos.prototype.ignoreCryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems.prototype.visibleItems

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected.prototype.keyboardSelected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto.prototype.selectedCrypto

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys.prototype.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys}
 */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
xyz.swapee.wc.ICryptoSelectPort.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @interface */
xyz.swapee.wc.ICryptoSelectPortFields
/** @type {!xyz.swapee.wc.ICryptoSelectPort.Inputs} */
xyz.swapee.wc.ICryptoSelectPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ICryptoSelectPort.Inputs} */
xyz.swapee.wc.ICryptoSelectPortFields.prototype.props

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @interface */
xyz.swapee.wc.ICryptoSelectPortCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectPort} */
xyz.swapee.wc.ICryptoSelectPortCaster.prototype.asICryptoSelectPort
/** @type {!xyz.swapee.wc.BoundCryptoSelectPort} */
xyz.swapee.wc.ICryptoSelectPortCaster.prototype.superCryptoSelectPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ICryptoSelectPort.Inputs>}
 */
xyz.swapee.wc.ICryptoSelectPort = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init */
xyz.swapee.wc.ICryptoSelectPort.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.ICryptoSelectPort.prototype.resetPort = function() {}
/** @return {void} */
xyz.swapee.wc.ICryptoSelectPort.prototype.resetCryptoSelectPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.CryptoSelectPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectPort.Initialese>}
 */
xyz.swapee.wc.CryptoSelectPort = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init */
xyz.swapee.wc.CryptoSelectPort.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.CryptoSelectPort.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.AbstractCryptoSelectPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectPort|typeof xyz.swapee.wc.CryptoSelectPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectPort.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectPort|typeof xyz.swapee.wc.CryptoSelectPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectPort|typeof xyz.swapee.wc.CryptoSelectPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.CryptoSelectPortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectPort, ...!xyz.swapee.wc.ICryptoSelectPort.Initialese)} */
xyz.swapee.wc.CryptoSelectPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.CryptoSelectInputsPQs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectInputsPQs.prototype.source

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.CryptoSelectInputsQPs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectInputsQPs.prototype.d6cd3

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.RecordICryptoSelectPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @typedef {{ resetPort: xyz.swapee.wc.ICryptoSelectPort.resetPort, resetCryptoSelectPort: xyz.swapee.wc.ICryptoSelectPort.resetCryptoSelectPort }} */
xyz.swapee.wc.RecordICryptoSelectPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.BoundICryptoSelectPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPortFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ICryptoSelectPort.Inputs>}
 */
xyz.swapee.wc.BoundICryptoSelectPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.BoundCryptoSelectPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectPort): void} */
xyz.swapee.wc.ICryptoSelectPort._resetPort
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectPort.__resetPort} */
xyz.swapee.wc.ICryptoSelectPort.__resetPort

// nss:xyz.swapee.wc.ICryptoSelectPort,$$xyz.swapee.wc.ICryptoSelectPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.resetCryptoSelectPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectPort.__resetCryptoSelectPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectPort.resetCryptoSelectPort
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectPort): void} */
xyz.swapee.wc.ICryptoSelectPort._resetCryptoSelectPort
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectPort.__resetCryptoSelectPort} */
xyz.swapee.wc.ICryptoSelectPort.__resetCryptoSelectPort

// nss:xyz.swapee.wc.ICryptoSelectPort,$$xyz.swapee.wc.ICryptoSelectPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Source.source exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source.source

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Source exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source.prototype.source

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel}
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source.prototype.source

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel}
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @interface */
xyz.swapee.wc.ICryptoSelectPortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.CryptoSelectPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectPortInterface}
 */
xyz.swapee.wc.CryptoSelectPortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortInterface.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
xyz.swapee.wc.ICryptoSelectPortInterface.Props = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.selected
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.iconFolder
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.fiat
/** @type {string} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.imHtml
/** @type {string} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.search
/** @type {string} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.selectedIcon
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.menuExpanded
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.isMouseOver
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.sm
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.isSearching
/** @type {number|undefined} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.hoveringIndex
/** @type {Map<string, { icon: string, displayName: string, name: string }>} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.cryptos
/** @type {!Set<string>} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.ignoreCryptos
/** @type {!Array<string>} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.visibleItems
/** @type {string} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.keyboardSelected
/** @type {?{ icon: string, displayName: string, name: string }} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.selectedCrypto
/** @type {!Set<string>} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @record */
xyz.swapee.wc.ICryptoSelectCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @interface */
xyz.swapee.wc.ICryptoSelectCoreFields
/** @type {!xyz.swapee.wc.ICryptoSelectCore.Model} */
xyz.swapee.wc.ICryptoSelectCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @interface */
xyz.swapee.wc.ICryptoSelectCoreCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectCore} */
xyz.swapee.wc.ICryptoSelectCoreCaster.prototype.asICryptoSelectCore
/** @type {!xyz.swapee.wc.BoundCryptoSelectCore} */
xyz.swapee.wc.ICryptoSelectCoreCaster.prototype.superCryptoSelectCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCoreCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore}
 */
xyz.swapee.wc.ICryptoSelectCore = function() {}
/** @return {void} */
xyz.swapee.wc.ICryptoSelectCore.prototype.resetCore = function() {}
/** @return {void} */
xyz.swapee.wc.ICryptoSelectCore.prototype.resetCryptoSelectCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.CryptoSelectCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectCore.Initialese>}
 */
xyz.swapee.wc.CryptoSelectCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.CryptoSelectCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.AbstractCryptoSelectCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @constructor
 * @extends {xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore = function() {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectCachePQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.CryptoSelectCachePQs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectCachePQs.prototype.wideness

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectCacheQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.CryptoSelectCacheQPs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectCacheQPs.prototype.b67ac

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.RecordICryptoSelectCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @typedef {{ resetCore: xyz.swapee.wc.ICryptoSelectCore.resetCore, resetCryptoSelectCore: xyz.swapee.wc.ICryptoSelectCore.resetCryptoSelectCore }} */
xyz.swapee.wc.RecordICryptoSelectCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.BoundICryptoSelectCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCoreFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCoreCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectOuterCore}
 */
xyz.swapee.wc.BoundICryptoSelectCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.BoundCryptoSelectCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectCore): void} */
xyz.swapee.wc.ICryptoSelectCore._resetCore
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectCore.__resetCore} */
xyz.swapee.wc.ICryptoSelectCore.__resetCore

// nss:xyz.swapee.wc.ICryptoSelectCore,$$xyz.swapee.wc.ICryptoSelectCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.resetCryptoSelectCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectCore.__resetCryptoSelectCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectCore.resetCryptoSelectCore
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectCore): void} */
xyz.swapee.wc.ICryptoSelectCore._resetCryptoSelectCore
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectCore.__resetCryptoSelectCore} */
xyz.swapee.wc.ICryptoSelectCore.__resetCryptoSelectCore

// nss:xyz.swapee.wc.ICryptoSelectCore,$$xyz.swapee.wc.ICryptoSelectCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Wideness.wideness exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness.wideness

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Wideness}
 */
xyz.swapee.wc.ICryptoSelectCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel>}
 */
xyz.swapee.wc.ICryptoSelectController.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.ICryptoSelectProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectComputer.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
xyz.swapee.wc.ICryptoSelectProcessor.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.ICryptoSelectProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/** @interface */
xyz.swapee.wc.ICryptoSelectProcessorCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectProcessor} */
xyz.swapee.wc.ICryptoSelectProcessorCaster.prototype.asICryptoSelectProcessor
/** @type {!xyz.swapee.wc.BoundCryptoSelectProcessor} */
xyz.swapee.wc.ICryptoSelectProcessorCaster.prototype.superCryptoSelectProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @interface */
xyz.swapee.wc.ICryptoSelectControllerFields
/** @type {!xyz.swapee.wc.ICryptoSelectController.Inputs} */
xyz.swapee.wc.ICryptoSelectControllerFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @interface */
xyz.swapee.wc.ICryptoSelectControllerCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectController} */
xyz.swapee.wc.ICryptoSelectControllerCaster.prototype.asICryptoSelectController
/** @type {!xyz.swapee.wc.BoundICryptoSelectProcessor} */
xyz.swapee.wc.ICryptoSelectControllerCaster.prototype.asICryptoSelectProcessor
/** @type {!xyz.swapee.wc.BoundCryptoSelectController} */
xyz.swapee.wc.ICryptoSelectControllerCaster.prototype.superCryptoSelectController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
xyz.swapee.wc.ICryptoSelectController = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init */
xyz.swapee.wc.ICryptoSelectController.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.ICryptoSelectController.prototype.resetPort = function() {}
/** @return {?} */
xyz.swapee.wc.ICryptoSelectController.prototype.flipMenuExpanded = function() {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)}
 */
xyz.swapee.wc.ICryptoSelectController.prototype.calibrateDataSource = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)}
 */
xyz.swapee.wc.ICryptoSelectController.prototype.calibrateResetMouseOver = function(form, changes) {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectController.prototype.setSelected = function(val) {}
/** @return {void} */
xyz.swapee.wc.ICryptoSelectController.prototype.unsetSelected = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.ICryptoSelectProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessorCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer}
 * @extends {xyz.swapee.wc.ICryptoSelectCore}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 */
xyz.swapee.wc.ICryptoSelectProcessor = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init */
xyz.swapee.wc.ICryptoSelectProcessor.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.CryptoSelectProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectProcessor.Initialese>}
 */
xyz.swapee.wc.CryptoSelectProcessor = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init */
xyz.swapee.wc.CryptoSelectProcessor.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.CryptoSelectProcessor.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.AbstractCryptoSelectProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.CryptoSelectProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectProcessor, ...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese)} */
xyz.swapee.wc.CryptoSelectProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.RecordICryptoSelectProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.RecordICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @typedef {{ resetPort: xyz.swapee.wc.ICryptoSelectController.resetPort, flipMenuExpanded: xyz.swapee.wc.ICryptoSelectController.flipMenuExpanded, calibrateDataSource: xyz.swapee.wc.ICryptoSelectController.calibrateDataSource, calibrateResetMouseOver: xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver, setSelected: xyz.swapee.wc.ICryptoSelectController.setSelected, unsetSelected: xyz.swapee.wc.ICryptoSelectController.unsetSelected }} */
xyz.swapee.wc.RecordICryptoSelectController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.BoundICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectControllerFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
xyz.swapee.wc.BoundICryptoSelectController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.BoundICryptoSelectProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordICryptoSelectProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessorCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 * @extends {xyz.swapee.wc.BoundICryptoSelectCore}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 */
xyz.swapee.wc.BoundICryptoSelectProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.BoundCryptoSelectProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/100-CryptoSelectMemory.xml} xyz.swapee.wc.CryptoSelectMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ac88f349dc4b42767f75c1caa5ad6deb */
/** @record */
xyz.swapee.wc.CryptoSelectMemory = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemory.prototype.selected
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemory.prototype.iconFolder
/** @type {boolean} */
xyz.swapee.wc.CryptoSelectMemory.prototype.fiat
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemory.prototype.imHtml
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemory.prototype.search
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemory.prototype.selectedIcon
/** @type {boolean} */
xyz.swapee.wc.CryptoSelectMemory.prototype.menuExpanded
/** @type {boolean} */
xyz.swapee.wc.CryptoSelectMemory.prototype.isMouseOver
/** @type {boolean} */
xyz.swapee.wc.CryptoSelectMemory.prototype.sm
/** @type {boolean} */
xyz.swapee.wc.CryptoSelectMemory.prototype.isSearching
/** @type {number} */
xyz.swapee.wc.CryptoSelectMemory.prototype.hoveringIndex
/** @type {Map<string, { icon: string, displayName: string, name: string }>} */
xyz.swapee.wc.CryptoSelectMemory.prototype.cryptos
/** @type {!Set<string>} */
xyz.swapee.wc.CryptoSelectMemory.prototype.ignoreCryptos
/** @type {!Array<string>} */
xyz.swapee.wc.CryptoSelectMemory.prototype.visibleItems
/** @type {string} */
xyz.swapee.wc.CryptoSelectMemory.prototype.keyboardSelected
/** @type {?{ icon: string, displayName: string, name: string }} */
xyz.swapee.wc.CryptoSelectMemory.prototype.selectedCrypto
/** @type {!Set<string>} */
xyz.swapee.wc.CryptoSelectMemory.prototype.matchedKeys
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectMemory.prototype.wideness

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/102-CryptoSelectInputs.xml} xyz.swapee.wc.front.CryptoSelectInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7f0df2956b126c9f1fa3d85a696e7da0 */
/** @record */
xyz.swapee.wc.front.CryptoSelectInputs = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.source
/** @type {string|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.selected
/** @type {string|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.iconFolder
/** @type {boolean|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.fiat
/** @type {string|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.imHtml
/** @type {string|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.search
/** @type {string|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.selectedIcon
/** @type {boolean|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.menuExpanded
/** @type {boolean|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.isMouseOver
/** @type {boolean|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.sm
/** @type {boolean|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.isSearching
/** @type {number|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.hoveringIndex
/** @type {(Map<string, { icon: string, displayName: string, name: string }>)|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.cryptos
/** @type {(!Set<string>)|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.ignoreCryptos
/** @type {(!Array<string>)|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.visibleItems
/** @type {string|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.keyboardSelected
/** @type {(?{ icon: string, displayName: string, name: string })|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.selectedCrypto
/** @type {(!Set<string>)|undefined} */
xyz.swapee.wc.front.CryptoSelectInputs.prototype.matchedKeys

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelectEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @record */
xyz.swapee.wc.CryptoSelectEnv = function() {}
/** @type {xyz.swapee.wc.ICryptoSelect} */
xyz.swapee.wc.CryptoSelectEnv.prototype.cryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
xyz.swapee.wc.ICryptoSelect.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelectFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @interface */
xyz.swapee.wc.ICryptoSelectFields
/** @type {!xyz.swapee.wc.ICryptoSelect.Pinout} */
xyz.swapee.wc.ICryptoSelectFields.prototype.pinout

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelectCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @interface */
xyz.swapee.wc.ICryptoSelectCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelect} */
xyz.swapee.wc.ICryptoSelectCaster.prototype.asICryptoSelect
/** @type {!xyz.swapee.wc.BoundCryptoSelect} */
xyz.swapee.wc.ICryptoSelectCaster.prototype.superCryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectLand>}
 */
xyz.swapee.wc.ICryptoSelect = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init */
xyz.swapee.wc.ICryptoSelect.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelect exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelect}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelect.Initialese>}
 */
xyz.swapee.wc.CryptoSelect = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init */
xyz.swapee.wc.CryptoSelect.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.CryptoSelect.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.AbstractCryptoSelect exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelect.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelectConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelect, ...!xyz.swapee.wc.ICryptoSelect.Initialese)} */
xyz.swapee.wc.CryptoSelectConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @record */
xyz.swapee.wc.ICryptoSelect.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.ICryptoSelect.Pinout)|undefined} */
xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.ICryptoSelect.Pinout)|undefined} */
xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.ICryptoSelect.Pinout} */
xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.CryptoSelectMemory)|undefined} */
xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.CryptoSelectClasses)|undefined} */
xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.classes

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.RecordICryptoSelect exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.BoundICryptoSelect exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelect}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectLand>}
 */
xyz.swapee.wc.BoundICryptoSelect = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.BoundCryptoSelect exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelect}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelect = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs}
 */
xyz.swapee.wc.ICryptoSelectController.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectController.Inputs}
 */
xyz.swapee.wc.ICryptoSelect.Pinout = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelectBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
xyz.swapee.wc.ICryptoSelectBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelectBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectBuffer}
 */
xyz.swapee.wc.CryptoSelectBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese}
 */
xyz.swapee.wc.ICryptoSelectGPU.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectController.Initialese}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelect.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer.Initialese}
 */
xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/** @interface */
xyz.swapee.wc.ICryptoSelectHtmlComponentCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectHtmlComponent} */
xyz.swapee.wc.ICryptoSelectHtmlComponentCaster.prototype.asICryptoSelectHtmlComponent
/** @type {!xyz.swapee.wc.BoundCryptoSelectHtmlComponent} */
xyz.swapee.wc.ICryptoSelectHtmlComponentCaster.prototype.superCryptoSelectHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.ICryptoSelectGPUFields
/** @type {!Object<string, string>} */
xyz.swapee.wc.ICryptoSelectGPUFields.prototype.vdusPQs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.ICryptoSelectGPUCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectGPU} */
xyz.swapee.wc.ICryptoSelectGPUCaster.prototype.asICryptoSelectGPU
/** @type {!xyz.swapee.wc.BoundCryptoSelectGPU} */
xyz.swapee.wc.ICryptoSelectGPUCaster.prototype.superCryptoSelectGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!CryptoSelectMemory,.!CryptoSelectLand>}
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplay}
 */
xyz.swapee.wc.ICryptoSelectGPU = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init */
xyz.swapee.wc.ICryptoSelectGPU.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.ICryptoSelectHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.ICryptoSelectController}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreen}
 * @extends {xyz.swapee.wc.ICryptoSelect}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.ICryptoSelectGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !HTMLDivElement, !xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer}
 */
xyz.swapee.wc.ICryptoSelectHtmlComponent = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init */
xyz.swapee.wc.ICryptoSelectHtmlComponent.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.CryptoSelectHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese>}
 */
xyz.swapee.wc.CryptoSelectHtmlComponent = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init */
xyz.swapee.wc.CryptoSelectHtmlComponent.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.CryptoSelectHtmlComponent.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.AbstractCryptoSelectHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectHtmlComponent|typeof xyz.swapee.wc.CryptoSelectHtmlComponent)|(!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectHtmlComponent|typeof xyz.swapee.wc.CryptoSelectHtmlComponent)|(!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectHtmlComponent|typeof xyz.swapee.wc.CryptoSelectHtmlComponent)|(!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.CryptoSelectHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectHtmlComponent, ...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese)} */
xyz.swapee.wc.CryptoSelectHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentUtilFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @interface */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtilFields
/** @type {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtilFields.prototype.RouterNet
/** @type {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtilFields.prototype.RouterPorts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectHtmlComponentUtilFields}
 */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil = function() {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts} [ports]
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.prototype.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.CryptoSelectHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectHtmlComponentUtil}
 */
xyz.swapee.wc.CryptoSelectHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.RecordICryptoSelectHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @typedef {{ router: xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.router }} */
xyz.swapee.wc.RecordICryptoSelectHtmlComponentUtil

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.BoundICryptoSelectHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectHtmlComponentUtilFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectHtmlComponentUtil}
 */
xyz.swapee.wc.BoundICryptoSelectHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.BoundCryptoSelectHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectHtmlComponentUtil}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.router exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts} [ports]
 */
$$xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.__router = function(net, cores, ports) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.router
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectHtmlComponentUtil, !xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil._router
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.__router} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.__router

// nss:xyz.swapee.wc.ICryptoSelectHtmlComponentUtil,$$xyz.swapee.wc.ICryptoSelectHtmlComponentUtil,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet = function() {}
/** @type {typeof com.webcircuits.ui.IPopupPort} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet.prototype.Popup
/** @type {typeof xyz.swapee.wc.ICryptoSelectPort} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterNet.prototype.CryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores = function() {}
/** @type {!com.webcircuits.ui.PopupMemory} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores.prototype.Popup
/** @type {!xyz.swapee.wc.CryptoSelectMemory} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterCores.prototype.CryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.element.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts = function() {}
/** @type {!com.webcircuits.ui.IPopup.Pinout} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts.prototype.Popup
/** @type {!xyz.swapee.wc.ICryptoSelect.Pinout} */
xyz.swapee.wc.ICryptoSelectHtmlComponentUtil.RouterPorts.prototype.CryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.RecordICryptoSelectHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.RecordICryptoSelectGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.BoundICryptoSelectGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectGPUFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!CryptoSelectMemory,.!CryptoSelectLand>}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectDisplay}
 */
xyz.swapee.wc.BoundICryptoSelectGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.BoundICryptoSelectHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordICryptoSelectHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectController}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreen}
 * @extends {xyz.swapee.wc.BoundICryptoSelect}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.BoundICryptoSelectGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !HTMLDivElement, !xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.BoundICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 */
xyz.swapee.wc.BoundICryptoSelectHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.BoundCryptoSelectHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @interface */
xyz.swapee.wc.ICryptoSelectDesigner = function() {}
/**
 * @param {xyz.swapee.wc.CryptoSelectClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ICryptoSelectDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.CryptoSelectClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ICryptoSelectDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh} mesh
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool} memPool
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.CryptoSelectClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ICryptoSelectDesigner.prototype.lendClasses = function(classes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.CryptoSelectDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectDesigner}
 */
xyz.swapee.wc.CryptoSelectDesigner = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @record */
xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh.prototype.Popup
/** @type {typeof xyz.swapee.wc.ICryptoSelectController} */
xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh.prototype.CryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @record */
xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh.prototype.Popup
/** @type {typeof xyz.swapee.wc.ICryptoSelectController} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh.prototype.CryptoSelect
/** @type {typeof xyz.swapee.wc.ICryptoSelectController} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @record */
xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool = function() {}
/** @type {!Object} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool.prototype.Popup
/** @type {!xyz.swapee.wc.CryptoSelectMemory} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool.prototype.CryptoSelect
/** @type {!xyz.swapee.wc.CryptoSelectMemory} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/200-CryptoSelectLand.xml} xyz.swapee.wc.CryptoSelectLand exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f86d8189b911518a81a2be6babfa7a37 */
/** @record */
xyz.swapee.wc.CryptoSelectLand = function() {}
/** @type {!Object} */
xyz.swapee.wc.CryptoSelectLand.prototype.Popup

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings>}
 */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese = function() {}
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoMenu
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoDown
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedBl
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.NoCryptoDropItem
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedImWr
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.ChevronUp
/** @type {HTMLDivElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.ChevronDown
/** @type {(!Array<!HTMLSpanElement>)|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.MenuItems
/** @type {HTMLInputElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedNameIn
/** @type {HTMLSpanElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.InnerSpan
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.Popup

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @interface */
xyz.swapee.wc.ICryptoSelectDisplayFields
/** @type {!xyz.swapee.wc.ICryptoSelectDisplay.Settings} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.ICryptoSelectDisplay.Queries} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.queries
/** @type {HTMLDivElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoMenu
/** @type {HTMLDivElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoDown
/** @type {HTMLDivElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoSelectedBl
/** @type {HTMLDivElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.NoCryptoDropItem
/** @type {HTMLSpanElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoSelectedImWr
/** @type {HTMLDivElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.ChevronUp
/** @type {HTMLDivElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.ChevronDown
/** @type {!Array<!HTMLSpanElement>} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.MenuItems
/** @type {HTMLInputElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoSelectedNameIn
/** @type {HTMLSpanElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.InnerSpan
/** @type {HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.Popup

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @interface */
xyz.swapee.wc.ICryptoSelectDisplayCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectDisplay} */
xyz.swapee.wc.ICryptoSelectDisplayCaster.prototype.asICryptoSelectDisplay
/** @type {!xyz.swapee.wc.BoundICryptoSelectTouchscreen} */
xyz.swapee.wc.ICryptoSelectDisplayCaster.prototype.asICryptoSelectTouchscreen
/** @type {!xyz.swapee.wc.BoundCryptoSelectDisplay} */
xyz.swapee.wc.ICryptoSelectDisplayCaster.prototype.superCryptoSelectDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.CryptoSelectMemory, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, xyz.swapee.wc.ICryptoSelectDisplay.Queries, null>}
 */
xyz.swapee.wc.ICryptoSelectDisplay = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.constructor = function(...init) {}
/**
 * @param {!MouseEvent} ev
 * @return {HTMLElement}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveMouseItem = function(ev) {}
/**
 * @param {number} index
 * @return {HTMLElement}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveItem = function(index) {}
/**
 * @param {string} key
 * @return {HTMLElement}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveItemByKey = function(key) {}
/**
 * @param {HTMLElement} cryptoItem
 * @return {number}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveItemIndex = function(cryptoItem) {}
/**
 * @param {HTMLElement} cryptoItem
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.scrollItemIntoView = function(cryptoItem) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {null} land
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.paint = function(memory, land) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory} memory
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSmHeight = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory} memory
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSearch = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory} memory
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSelectedImg = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory} memory
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintHoveringIndex = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory} memory
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintBeforeHovering = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory} memory
 * @return {?}
 */
xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSearchInput = function(memory) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.CryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectDisplay.Initialese>}
 */
xyz.swapee.wc.CryptoSelectDisplay = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init */
xyz.swapee.wc.CryptoSelectDisplay.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.CryptoSelectDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.AbstractCryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.CryptoSelectDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectDisplay, ...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese)} */
xyz.swapee.wc.CryptoSelectDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.CryptoSelectGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectGPU.Initialese>}
 */
xyz.swapee.wc.CryptoSelectGPU = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init */
xyz.swapee.wc.CryptoSelectGPU.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.CryptoSelectGPU.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.AbstractCryptoSelectGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.CryptoSelectGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectGPU, ...!xyz.swapee.wc.ICryptoSelectGPU.Initialese)} */
xyz.swapee.wc.CryptoSelectGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.BoundCryptoSelectGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.RecordICryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @typedef {{ resolveMouseItem: xyz.swapee.wc.ICryptoSelectDisplay.resolveMouseItem, resolveItem: xyz.swapee.wc.ICryptoSelectDisplay.resolveItem, resolveItemByKey: xyz.swapee.wc.ICryptoSelectDisplay.resolveItemByKey, resolveItemIndex: xyz.swapee.wc.ICryptoSelectDisplay.resolveItemIndex, scrollItemIntoView: xyz.swapee.wc.ICryptoSelectDisplay.scrollItemIntoView, paint: xyz.swapee.wc.ICryptoSelectDisplay.paint, paintSmHeight: xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight, paintSearch: xyz.swapee.wc.ICryptoSelectDisplay.paintSearch, paintSelectedImg: xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg, paintHoveringIndex: xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex, paintBeforeHovering: xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering, paintSearchInput: xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput }} */
xyz.swapee.wc.RecordICryptoSelectDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.BoundICryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.CryptoSelectMemory, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, xyz.swapee.wc.ICryptoSelectDisplay.Queries, null>}
 */
xyz.swapee.wc.BoundICryptoSelectDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.BoundCryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveMouseItem exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!MouseEvent} ev
 * @return {HTMLElement}
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__resolveMouseItem = function(ev) {}
/** @typedef {function(!MouseEvent): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveMouseItem
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !MouseEvent): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveMouseItem
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__resolveMouseItem} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveMouseItem

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveItem exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} index
 * @return {HTMLElement}
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItem = function(index) {}
/** @typedef {function(number): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveItem
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, number): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveItem
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItem} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveItem

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveItemByKey exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} key
 * @return {HTMLElement}
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemByKey = function(key) {}
/** @typedef {function(string): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveItemByKey
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, string): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveItemByKey
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemByKey} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemByKey

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveItemIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {HTMLElement} cryptoItem
 * @return {number}
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemIndex = function(cryptoItem) {}
/** @typedef {function(HTMLElement): number} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveItemIndex
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, HTMLElement): number} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveItemIndex
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemIndex} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemIndex

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.scrollItemIntoView exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {HTMLElement} cryptoItem
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__scrollItemIntoView = function(cryptoItem) {}
/** @typedef {function(HTMLElement)} */
xyz.swapee.wc.ICryptoSelectDisplay.scrollItemIntoView
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, HTMLElement)} */
xyz.swapee.wc.ICryptoSelectDisplay._scrollItemIntoView
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__scrollItemIntoView} */
xyz.swapee.wc.ICryptoSelectDisplay.__scrollItemIntoView

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {null} land
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.CryptoSelectMemory, null): void} */
xyz.swapee.wc.ICryptoSelectDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.CryptoSelectMemory, null): void} */
xyz.swapee.wc.ICryptoSelectDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__paint} */
xyz.swapee.wc.ICryptoSelectDisplay.__paint

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory} memory
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__paintSmHeight = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSmHeight
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__paintSmHeight} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSmHeight

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearch exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory} memory
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__paintSearch = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearch
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSearch
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__paintSearch} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSearch

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory} memory
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__paintSelectedImg = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSelectedImg
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__paintSelectedImg} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSelectedImg

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory} memory
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__paintHoveringIndex = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay._paintHoveringIndex
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__paintHoveringIndex} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintHoveringIndex

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory} memory
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__paintBeforeHovering = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay._paintBeforeHovering
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__paintBeforeHovering} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintBeforeHovering

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory} memory
 */
$$xyz.swapee.wc.ICryptoSelectDisplay.__paintSearchInput = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory)} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSearchInput
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectDisplay.__paintSearchInput} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSearchInput

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @record */
xyz.swapee.wc.ICryptoSelectDisplay.Queries = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectDisplay.Queries}
 */
xyz.swapee.wc.ICryptoSelectDisplay.Settings = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe = function() {}
/** @type {!Set<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe.prototype.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe}
 */
xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe}
 */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe = function() {}
/** @type {number} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe.prototype.hoveringIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe}
 */
xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe}
 */
xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe = function() {}
/** @type {?{ icon: string, displayName: string, name: string }} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe.prototype.selectedCrypto

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe.prototype.sm

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe}
 */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.CryptoSelectClasses>}
 */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoMenu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoDown
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedBl
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.NoCryptoDropItem
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedImWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.ChevronUp
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.ChevronDown
/** @type {(!Array<!com.webcircuits.IHtmlTwin>)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.MenuItems
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedNameIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.InnerSpan
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.Popup

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/** @interface */
xyz.swapee.wc.back.ICryptoSelectDisplayFields
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoMenu
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoDown
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoSelectedBl
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.NoCryptoDropItem
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoSelectedImWr
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.ChevronUp
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.ChevronDown
/** @type {!Array<!com.webcircuits.IHtmlTwin>} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.MenuItems
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoSelectedNameIn
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.InnerSpan
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.Popup

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/** @interface */
xyz.swapee.wc.back.ICryptoSelectDisplayCaster
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectDisplay} */
xyz.swapee.wc.back.ICryptoSelectDisplayCaster.prototype.asICryptoSelectDisplay
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectDisplay} */
xyz.swapee.wc.back.ICryptoSelectDisplayCaster.prototype.superCryptoSelectDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.CryptoSelectClasses, !xyz.swapee.wc.CryptoSelectLand>}
 */
xyz.swapee.wc.back.ICryptoSelectDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [memory]
 * @param {!xyz.swapee.wc.CryptoSelectLand} [land]
 * @return {void}
 */
xyz.swapee.wc.back.ICryptoSelectDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.CryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.ICryptoSelectDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese>}
 */
xyz.swapee.wc.back.CryptoSelectDisplay = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.CryptoSelectDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.AbstractCryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay = function() {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.CryptoSelectVdusPQs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoSelectedBl
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoSelectedImWr
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoSelectedNameIn
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoMenu
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.MenuItems
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoDropItem
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.ChevronUp
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.ChevronDown
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.NoCryptoDropItem
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoDown
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusPQs.prototype.Popup

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.CryptoSelectVdusQPs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b31
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b32
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b33
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b34
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b35
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b36
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b37
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b38
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b39
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b310
/** @type {string} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b311

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.RecordICryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/** @typedef {{ paint: xyz.swapee.wc.back.ICryptoSelectDisplay.paint }} */
xyz.swapee.wc.back.RecordICryptoSelectDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.BoundICryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.CryptoSelectClasses, !xyz.swapee.wc.CryptoSelectLand>}
 */
xyz.swapee.wc.back.BoundICryptoSelectDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.BoundCryptoSelectDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundCryptoSelectDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [memory]
 * @param {!xyz.swapee.wc.CryptoSelectLand} [land]
 * @return {void}
 */
$$xyz.swapee.wc.back.ICryptoSelectDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.CryptoSelectMemory=, !xyz.swapee.wc.CryptoSelectLand=): void} */
xyz.swapee.wc.back.ICryptoSelectDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.ICryptoSelectDisplay, !xyz.swapee.wc.CryptoSelectMemory=, !xyz.swapee.wc.CryptoSelectLand=): void} */
xyz.swapee.wc.back.ICryptoSelectDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.back.ICryptoSelectDisplay.__paint} */
xyz.swapee.wc.back.ICryptoSelectDisplay.__paint

// nss:xyz.swapee.wc.back.ICryptoSelectDisplay,$$xyz.swapee.wc.back.ICryptoSelectDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectClassesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.CryptoSelectClassesPQs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDownBorder
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Fiat
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.ItemHovered
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDropItem
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDownCollapsed
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDown
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Sm
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Expanded
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.SmWide
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.MouseOver
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.DropDownKeyboardFocus
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.HoveringOverItem
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.ImgWr
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.KeyboardSelect
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Highlight
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Chevron
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.NoCoins
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Pill
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDropItemBeforeHover
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesPQs.prototype.BackgroundStalk

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectClassesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.CryptoSelectClassesQPs = function() {}
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.d7551
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.c880c
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ie5a0
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.hdecb
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ga1d1
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.hbd64
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.c0c4c
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.g3f6b
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.g785a
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.h3365
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.i3f9e
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.i4661
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.cdcdb
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.eefef
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ab905
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ja37a
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.eeda9
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.f5797
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.adf16
/** @type {string} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.db2f0

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/41-CryptoSelectClasses.xml} xyz.swapee.wc.CryptoSelectClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1c6e1ed1f55d84e6ac42e3df45101927 */
/** @record */
xyz.swapee.wc.CryptoSelectClasses = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDownBorder
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.Fiat
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.ItemHovered
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDropItem
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDownCollapsed
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDown
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.Sm
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.Expanded
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.SmWide
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.MouseOver
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.DropDownKeyboardFocus
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.HoveringOverItem
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.ImgWr
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.KeyboardSelect
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.Highlight
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.Chevron
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.NoCoins
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.Pill
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDropItemBeforeHover
/** @type {string|undefined} */
xyz.swapee.wc.CryptoSelectClasses.prototype.BackgroundStalk

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.CryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectController.Initialese>}
 */
xyz.swapee.wc.CryptoSelectController = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init */
xyz.swapee.wc.CryptoSelectController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.CryptoSelectController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.AbstractCryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.CryptoSelectControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectController, ...!xyz.swapee.wc.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.CryptoSelectControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource = function() {}
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props)|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.prototype.props
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability)|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.prototype.variability

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability.prototype.field_source
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability.prototype.required_source
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability.prototype.return_cryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability}
 */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver = function() {}
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props)|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.prototype.props
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability)|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.prototype.variability

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability.prototype.field_isMouseOver
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability.prototype.required_isMouseOver
/** @type {string|undefined} */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability.prototype.return_isMouseOver

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability}
 */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.BoundCryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectController.resetPort
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController): void} */
xyz.swapee.wc.ICryptoSelectController._resetPort
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectController.__resetPort} */
xyz.swapee.wc.ICryptoSelectController.__resetPort

// nss:xyz.swapee.wc.ICryptoSelectController,$$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.flipMenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.ICryptoSelectController.__flipMenuExpanded = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ICryptoSelectController.flipMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController)} */
xyz.swapee.wc.ICryptoSelectController._flipMenuExpanded
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectController.__flipMenuExpanded} */
xyz.swapee.wc.ICryptoSelectController.__flipMenuExpanded

// nss:xyz.swapee.wc.ICryptoSelectController,$$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateDataSource exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectController.__calibrateDataSource = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form, xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form): (undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)} */
xyz.swapee.wc.ICryptoSelectController.calibrateDataSource
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController, !xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form, xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form): (undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)} */
xyz.swapee.wc.ICryptoSelectController._calibrateDataSource
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectController.__calibrateDataSource} */
xyz.swapee.wc.ICryptoSelectController.__calibrateDataSource

// nss:xyz.swapee.wc.ICryptoSelectController,$$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectController.__calibrateResetMouseOver = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form, xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form): (undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)} */
xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController, !xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form, xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form): (undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)} */
xyz.swapee.wc.ICryptoSelectController._calibrateResetMouseOver
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectController.__calibrateResetMouseOver} */
xyz.swapee.wc.ICryptoSelectController.__calibrateResetMouseOver

// nss:xyz.swapee.wc.ICryptoSelectController,$$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.setSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectController.__setSelected = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ICryptoSelectController.setSelected
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController, string): void} */
xyz.swapee.wc.ICryptoSelectController._setSelected
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectController.__setSelected} */
xyz.swapee.wc.ICryptoSelectController.__setSelected

// nss:xyz.swapee.wc.ICryptoSelectController,$$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.unsetSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectController.__unsetSelected = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectController.unsetSelected
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController): void} */
xyz.swapee.wc.ICryptoSelectController._unsetSelected
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectController.__unsetSelected} */
xyz.swapee.wc.ICryptoSelectController.__unsetSelected

// nss:xyz.swapee.wc.ICryptoSelectController,$$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs}
 */
xyz.swapee.wc.ICryptoSelectController.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe.prototype.source

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe}
 */
xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos}
 */
xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe.prototype.isMouseOver

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe}
 */
xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver}
 */
xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @record */
xyz.swapee.wc.front.ICryptoSelectController.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @interface */
xyz.swapee.wc.front.ICryptoSelectControllerCaster
/** @type {!xyz.swapee.wc.front.BoundICryptoSelectController} */
xyz.swapee.wc.front.ICryptoSelectControllerCaster.prototype.asICryptoSelectController
/** @type {!xyz.swapee.wc.front.BoundCryptoSelectController} */
xyz.swapee.wc.front.ICryptoSelectControllerCaster.prototype.superCryptoSelectController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.ICryptoSelectControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/** @interface */
xyz.swapee.wc.front.ICryptoSelectControllerATCaster
/** @type {!xyz.swapee.wc.front.BoundICryptoSelectControllerAT} */
xyz.swapee.wc.front.ICryptoSelectControllerATCaster.prototype.asICryptoSelectControllerAT
/** @type {!xyz.swapee.wc.front.BoundCryptoSelectControllerAT} */
xyz.swapee.wc.front.ICryptoSelectControllerATCaster.prototype.superCryptoSelectControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.ICryptoSelectControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.front.ICryptoSelectControllerAT = function() {}
/** @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init */
xyz.swapee.wc.front.ICryptoSelectControllerAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerAT}
 */
xyz.swapee.wc.front.ICryptoSelectController = function() {}
/** @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init */
xyz.swapee.wc.front.ICryptoSelectController.prototype.constructor = function(...init) {}
/** @return {?} */
xyz.swapee.wc.front.ICryptoSelectController.prototype.flipMenuExpanded = function() {}
/**
 * @param {string} val
 * @return {void}
 */
xyz.swapee.wc.front.ICryptoSelectController.prototype.setSelected = function(val) {}
/** @return {void} */
xyz.swapee.wc.front.ICryptoSelectController.prototype.unsetSelected = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.CryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init
 * @implements {xyz.swapee.wc.front.ICryptoSelectController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ICryptoSelectController.Initialese>}
 */
xyz.swapee.wc.front.CryptoSelectController = function(...init) {}
/** @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init */
xyz.swapee.wc.front.CryptoSelectController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.CryptoSelectController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.AbstractCryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init
 * @extends {xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractCryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.CryptoSelectControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @typedef {function(new: xyz.swapee.wc.front.ICryptoSelectController, ...!xyz.swapee.wc.front.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.front.CryptoSelectControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.RecordICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @typedef {{ flipMenuExpanded: xyz.swapee.wc.front.ICryptoSelectController.flipMenuExpanded, setSelected: xyz.swapee.wc.front.ICryptoSelectController.setSelected, unsetSelected: xyz.swapee.wc.front.ICryptoSelectController.unsetSelected }} */
xyz.swapee.wc.front.RecordICryptoSelectController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.RecordICryptoSelectControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordICryptoSelectControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.BoundICryptoSelectControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordICryptoSelectControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.front.BoundICryptoSelectControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.BoundICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordICryptoSelectController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectControllerAT}
 */
xyz.swapee.wc.front.BoundICryptoSelectController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.BoundCryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundCryptoSelectController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.flipMenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @this {THIS}
 * @template THIS
 */
$$xyz.swapee.wc.front.ICryptoSelectController.__flipMenuExpanded = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ICryptoSelectController.flipMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.front.ICryptoSelectController)} */
xyz.swapee.wc.front.ICryptoSelectController._flipMenuExpanded
/** @typedef {typeof $$xyz.swapee.wc.front.ICryptoSelectController.__flipMenuExpanded} */
xyz.swapee.wc.front.ICryptoSelectController.__flipMenuExpanded

// nss:xyz.swapee.wc.front.ICryptoSelectController,$$xyz.swapee.wc.front.ICryptoSelectController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.setSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$$xyz.swapee.wc.front.ICryptoSelectController.__setSelected = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ICryptoSelectController.setSelected
/** @typedef {function(this: xyz.swapee.wc.front.ICryptoSelectController, string): void} */
xyz.swapee.wc.front.ICryptoSelectController._setSelected
/** @typedef {typeof $$xyz.swapee.wc.front.ICryptoSelectController.__setSelected} */
xyz.swapee.wc.front.ICryptoSelectController.__setSelected

// nss:xyz.swapee.wc.front.ICryptoSelectController,$$xyz.swapee.wc.front.ICryptoSelectController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.unsetSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.ICryptoSelectController.__unsetSelected = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ICryptoSelectController.unsetSelected
/** @typedef {function(this: xyz.swapee.wc.front.ICryptoSelectController): void} */
xyz.swapee.wc.front.ICryptoSelectController._unsetSelected
/** @typedef {typeof $$xyz.swapee.wc.front.ICryptoSelectController.__unsetSelected} */
xyz.swapee.wc.front.ICryptoSelectController.__unsetSelected

// nss:xyz.swapee.wc.front.ICryptoSelectController,$$xyz.swapee.wc.front.ICryptoSelectController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.ICryptoSelectController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
xyz.swapee.wc.back.ICryptoSelectController.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.ICryptoSelectControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/** @interface */
xyz.swapee.wc.back.ICryptoSelectControllerCaster
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectController} */
xyz.swapee.wc.back.ICryptoSelectControllerCaster.prototype.asICryptoSelectController
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectController} */
xyz.swapee.wc.back.ICryptoSelectControllerCaster.prototype.superCryptoSelectController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.ICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
xyz.swapee.wc.back.ICryptoSelectController = function() {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init */
xyz.swapee.wc.back.ICryptoSelectController.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.CryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectController.Initialese>}
 */
xyz.swapee.wc.back.CryptoSelectController = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init */
xyz.swapee.wc.back.CryptoSelectController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.CryptoSelectController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.AbstractCryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.CryptoSelectControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectController, ...!xyz.swapee.wc.back.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.back.CryptoSelectControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.RecordICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.BoundICryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
xyz.swapee.wc.back.BoundICryptoSelectController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.BoundCryptoSelectController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundCryptoSelectController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.ICryptoSelectControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/** @interface */
xyz.swapee.wc.back.ICryptoSelectControllerARCaster
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectControllerAR} */
xyz.swapee.wc.back.ICryptoSelectControllerARCaster.prototype.asICryptoSelectControllerAR
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectControllerAR} */
xyz.swapee.wc.back.ICryptoSelectControllerARCaster.prototype.superCryptoSelectControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.ICryptoSelectControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 */
xyz.swapee.wc.back.ICryptoSelectControllerAR = function() {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init */
xyz.swapee.wc.back.ICryptoSelectControllerAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.CryptoSelectControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese>}
 */
xyz.swapee.wc.back.CryptoSelectControllerAR = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init */
xyz.swapee.wc.back.CryptoSelectControllerAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.CryptoSelectControllerAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.AbstractCryptoSelectControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectControllerAR|typeof xyz.swapee.wc.back.CryptoSelectControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectControllerAR|typeof xyz.swapee.wc.back.CryptoSelectControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectControllerAR|typeof xyz.swapee.wc.back.CryptoSelectControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.CryptoSelectControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectControllerAR, ...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese)} */
xyz.swapee.wc.back.CryptoSelectControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.RecordICryptoSelectControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.BoundICryptoSelectControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 */
xyz.swapee.wc.back.BoundICryptoSelectControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.BoundCryptoSelectControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundCryptoSelectControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.CryptoSelectControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.ICryptoSelectControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese>}
 */
xyz.swapee.wc.front.CryptoSelectControllerAT = function(...init) {}
/** @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init */
xyz.swapee.wc.front.CryptoSelectControllerAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.CryptoSelectControllerAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.AbstractCryptoSelectControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractCryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.CryptoSelectControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/** @typedef {function(new: xyz.swapee.wc.front.ICryptoSelectControllerAT, ...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese)} */
xyz.swapee.wc.front.CryptoSelectControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.BoundCryptoSelectControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundCryptoSelectControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.front.CryptoSelectInputs, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, !xyz.swapee.wc.ICryptoSelectDisplay.Queries, !xyz.swapee.wc.CryptoSelectClasses>}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplay.Initialese}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreenFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @interface */
xyz.swapee.wc.ICryptoSelectTouchscreenFields
/** @type {HTMLElement} */
xyz.swapee.wc.ICryptoSelectTouchscreenFields.prototype.keyboardItem
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectTouchscreenFields.prototype.isBlurringInput
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectTouchscreenFields.prototype.isMenuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @interface */
xyz.swapee.wc.ICryptoSelectTouchscreenCaster
/** @type {!xyz.swapee.wc.BoundICryptoSelectTouchscreen} */
xyz.swapee.wc.ICryptoSelectTouchscreenCaster.prototype.asICryptoSelectTouchscreen
/** @type {!xyz.swapee.wc.BoundCryptoSelectTouchscreen} */
xyz.swapee.wc.ICryptoSelectTouchscreenCaster.prototype.superCryptoSelectTouchscreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.front.CryptoSelectInputs, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, !xyz.swapee.wc.ICryptoSelectDisplay.Queries, null, !xyz.swapee.wc.CryptoSelectClasses>}
 * @extends {xyz.swapee.wc.front.ICryptoSelectController}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLine}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplay}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen = function() {}
/** @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init */
xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory} memory
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashSelectedInLocalStorage = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashIsMenuExpanded = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashKeyboardItemAfterMenuExpanded = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashVisibleItems = function(memory) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.CryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese>}
 */
xyz.swapee.wc.CryptoSelectTouchscreen = function(...init) {}
/** @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init */
xyz.swapee.wc.CryptoSelectTouchscreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.CryptoSelectTouchscreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.AbstractCryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.CryptoSelectTouchscreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectTouchscreen, ...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese)} */
xyz.swapee.wc.CryptoSelectTouchscreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.RecordICryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @typedef {{ stashSelectedInLocalStorage: xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage, stashIsMenuExpanded: xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded, stashKeyboardItemAfterMenuExpanded: xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded, stashVisibleItems: xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems }} */
xyz.swapee.wc.RecordICryptoSelectTouchscreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.BoundICryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.front.CryptoSelectInputs, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, !xyz.swapee.wc.ICryptoSelectDisplay.Queries, null, !xyz.swapee.wc.CryptoSelectClasses>}
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectController}
 * @extends {xyz.swapee.wc.BoundICryptoSelectInterruptLine}
 * @extends {xyz.swapee.wc.BoundICryptoSelectDisplay}
 */
xyz.swapee.wc.BoundICryptoSelectTouchscreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.BoundCryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundCryptoSelectTouchscreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory} memory
 * @return {void}
 */
$$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashSelectedInLocalStorage = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory): void} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectTouchscreen, !xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory): void} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashSelectedInLocalStorage
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashSelectedInLocalStorage} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashSelectedInLocalStorage

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashIsMenuExpanded = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory): (undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectTouchscreen, !xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory): (undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashIsMenuExpanded
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashIsMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashIsMenuExpanded

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashKeyboardItemAfterMenuExpanded = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory): (undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectTouchscreen, !xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory): (undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashKeyboardItemAfterMenuExpanded
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashKeyboardItemAfterMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashKeyboardItemAfterMenuExpanded

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)}
 */
$$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashVisibleItems = function(memory) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory): (undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectTouchscreen, !xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory): (undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashVisibleItems
/** @typedef {typeof $$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashVisibleItems} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashVisibleItems

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @record */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return.prototype.isMenuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @record */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return.prototype.keyboardItem

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @record */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return.prototype.visibleItems

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/** @interface */
xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectTouchscreen} */
xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster.prototype.asICryptoSelectTouchscreen
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectTouchscreen} */
xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster.prototype.superCryptoSelectTouchscreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/** @interface */
xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT} */
xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster.prototype.asICryptoSelectTouchscreenAT
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT} */
xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster.prototype.superCryptoSelectTouchscreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreenAT = function() {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init */
xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreen = function() {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init */
xyz.swapee.wc.back.ICryptoSelectTouchscreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.CryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese>}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreen = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init */
xyz.swapee.wc.back.CryptoSelectTouchscreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.CryptoSelectTouchscreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectTouchscreen, ...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese)} */
xyz.swapee.wc.back.CryptoSelectTouchscreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.RecordICryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectTouchscreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.RecordICryptoSelectTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectTouchscreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectTouchscreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.BoundICryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.BoundICryptoSelectTouchscreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.BoundCryptoSelectTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundCryptoSelectTouchscreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese}
 */
xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/** @interface */
xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster
/** @type {!xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR} */
xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster.prototype.asICryptoSelectTouchscreenAR
/** @type {!xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR} */
xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster.prototype.superCryptoSelectTouchscreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.ICryptoSelectTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreen}
 */
xyz.swapee.wc.front.ICryptoSelectTouchscreenAR = function() {}
/** @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init */
xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.CryptoSelectTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.ICryptoSelectTouchscreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese>}
 */
xyz.swapee.wc.front.CryptoSelectTouchscreenAR = function(...init) {}
/** @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init */
xyz.swapee.wc.front.CryptoSelectTouchscreenAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.CryptoSelectTouchscreenAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR|typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR|typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR|typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.CryptoSelectTouchscreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/** @typedef {function(new: xyz.swapee.wc.front.ICryptoSelectTouchscreenAR, ...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese)} */
xyz.swapee.wc.front.CryptoSelectTouchscreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.RecordICryptoSelectTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordICryptoSelectTouchscreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordICryptoSelectTouchscreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundICryptoSelectTouchscreen}
 */
xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.CryptoSelectTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectTouchscreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese>}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreenAT = function(...init) {}
/** @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init */
xyz.swapee.wc.back.CryptoSelectTouchscreenAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreenAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.CryptoSelectTouchscreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectTouchscreenAT, ...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese)} */
xyz.swapee.wc.back.CryptoSelectTouchscreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe.prototype.iconFolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe.prototype.fiat

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe.prototype.imHtml

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe.prototype.selectedIcon

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe.prototype.isMouseOver

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe.prototype.isSearching

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe = function() {}
/** @type {!Array<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe.prototype.visibleItems

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe.prototype.keyboardSelected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe.prototype.selected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe.prototype.iconFolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe.prototype.fiat

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe.prototype.imHtml

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe.prototype.search

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe.prototype.selectedIcon

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe.prototype.menuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe.prototype.sm

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe.prototype.isSearching

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe.prototype.hoveringIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe.prototype.cryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe.prototype.ignoreCryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe.prototype.visibleItems

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe.prototype.keyboardSelected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe.prototype.selectedCrypto

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe.prototype.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Search exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Search = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Search_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Search_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe.prototype.source

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @record */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe.prototype.wideness

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Selected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Selected = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Fiat exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Fiat = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Fiat_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Fiat_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Sm exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Sm = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe}
 */
xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.Inputs.Source exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source}
 */
xyz.swapee.wc.ICryptoSelectController.Inputs.Source = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.Inputs.Source_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe}
 */
xyz.swapee.wc.ICryptoSelectController.Inputs.Source_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source}
 */
xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe}
 */
xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */