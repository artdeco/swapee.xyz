/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/102-CryptoSelectInputs.xml} xyz.swapee.wc.front.CryptoSelectInputs filter:!ControllerPlugin~props 7f0df2956b126c9f1fa3d85a696e7da0 */
/** @record */
$xyz.swapee.wc.front.CryptoSelectInputs = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.source
/** @type {string|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.selected
/** @type {string|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.iconFolder
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.fiat
/** @type {string|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.imHtml
/** @type {string|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.search
/** @type {string|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.selectedIcon
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.menuExpanded
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.isMouseOver
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.sm
/** @type {boolean|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.isSearching
/** @type {number|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.hoveringIndex
/** @type {(Map<string, { icon: string, displayName: string, name: string }>)|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.cryptos
/** @type {(!Set<string>)|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.ignoreCryptos
/** @type {(!Array<string>)|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.visibleItems
/** @type {string|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.keyboardSelected
/** @type {(?{ icon: string, displayName: string, name: string })|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.selectedCrypto
/** @type {(!Set<string>)|undefined} */
$xyz.swapee.wc.front.CryptoSelectInputs.prototype.matchedKeys
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.CryptoSelectInputs}
 */
xyz.swapee.wc.front.CryptoSelectInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */