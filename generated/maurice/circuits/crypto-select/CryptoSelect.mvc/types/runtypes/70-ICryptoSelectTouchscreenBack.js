/** @const {?} */ $xyz.swapee.wc.back.ICryptoSelectTouchscreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese}
 */
$xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} */
xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ICryptoSelectTouchscreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/** @interface */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectTouchscreen} */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster.prototype.asICryptoSelectTouchscreen
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectTouchscreen} */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster.prototype.superCryptoSelectTouchscreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreen filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenAT}
 */
$xyz.swapee.wc.back.ICryptoSelectTouchscreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.CryptoSelectTouchscreen filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese>}
 */
$xyz.swapee.wc.back.CryptoSelectTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreen
/** @type {function(new: xyz.swapee.wc.back.ICryptoSelectTouchscreen, ...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese)} */
xyz.swapee.wc.back.CryptoSelectTouchscreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
$xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen
/** @type {function(new: xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen)} */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.CryptoSelectTouchscreenConstructor filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectTouchscreen, ...!xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese)} */
xyz.swapee.wc.back.CryptoSelectTouchscreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.RecordICryptoSelectTouchscreen filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectTouchscreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.BoundICryptoSelectTouchscreen filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT}
 */
$xyz.swapee.wc.back.BoundICryptoSelectTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundICryptoSelectTouchscreen} */
xyz.swapee.wc.back.BoundICryptoSelectTouchscreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreenBack.xml} xyz.swapee.wc.back.BoundCryptoSelectTouchscreen filter:!ControllerPlugin~props d93bacb601f1e57871b6eea315926c36 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundCryptoSelectTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundCryptoSelectTouchscreen} */
xyz.swapee.wc.back.BoundCryptoSelectTouchscreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */