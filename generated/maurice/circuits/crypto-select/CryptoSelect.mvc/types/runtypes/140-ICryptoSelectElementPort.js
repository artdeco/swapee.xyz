/** @const {?} */ $xyz.swapee.wc.ICryptoSelectElementPort
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Initialese filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Initialese} */
xyz.swapee.wc.ICryptoSelectElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPortFields filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectElementPortFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelectElementPort.Inputs} */
$xyz.swapee.wc.ICryptoSelectElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ICryptoSelectElementPort.Inputs} */
$xyz.swapee.wc.ICryptoSelectElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectElementPortFields}
 */
xyz.swapee.wc.ICryptoSelectElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPortCaster filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectElementPort} */
$xyz.swapee.wc.ICryptoSelectElementPortCaster.prototype.asICryptoSelectElementPort
/** @type {!xyz.swapee.wc.BoundCryptoSelectElementPort} */
$xyz.swapee.wc.ICryptoSelectElementPortCaster.prototype.superCryptoSelectElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectElementPortCaster}
 */
xyz.swapee.wc.ICryptoSelectElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ICryptoSelectElementPort.Inputs>}
 */
$xyz.swapee.wc.ICryptoSelectElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectElementPort}
 */
xyz.swapee.wc.ICryptoSelectElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.CryptoSelectElementPort filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElementPort.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectElementPort.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectElementPort}
 */
xyz.swapee.wc.CryptoSelectElementPort
/** @type {function(new: xyz.swapee.wc.ICryptoSelectElementPort, ...!xyz.swapee.wc.ICryptoSelectElementPort.Initialese)} */
xyz.swapee.wc.CryptoSelectElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectElementPort}
 */
xyz.swapee.wc.CryptoSelectElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.AbstractCryptoSelectElementPort filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElementPort.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectElementPort}
 */
$xyz.swapee.wc.AbstractCryptoSelectElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectElementPort}
 */
xyz.swapee.wc.AbstractCryptoSelectElementPort
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectElementPort)} */
xyz.swapee.wc.AbstractCryptoSelectElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectElementPort|typeof xyz.swapee.wc.CryptoSelectElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectElementPort}
 */
xyz.swapee.wc.AbstractCryptoSelectElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectElementPort}
 */
xyz.swapee.wc.AbstractCryptoSelectElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectElementPort|typeof xyz.swapee.wc.CryptoSelectElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectElementPort}
 */
xyz.swapee.wc.AbstractCryptoSelectElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectElementPort|typeof xyz.swapee.wc.CryptoSelectElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectElementPort}
 */
xyz.swapee.wc.AbstractCryptoSelectElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.CryptoSelectElementPortConstructor filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectElementPort, ...!xyz.swapee.wc.ICryptoSelectElementPort.Initialese)} */
xyz.swapee.wc.CryptoSelectElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.RecordICryptoSelectElementPort filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.BoundICryptoSelectElementPort filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectElementPortFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ICryptoSelectElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundICryptoSelectElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectElementPort} */
xyz.swapee.wc.BoundICryptoSelectElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.BoundCryptoSelectElementPort filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectElementPort} */
xyz.swapee.wc.BoundCryptoSelectElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts.cryptoMenuOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts.cryptoMenuOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts.cryptoDownOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts.cryptoDownOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts.cryptoSelectedBlOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts.cryptoSelectedBlOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts.noCryptoDropItemOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts.noCryptoDropItemOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts.cryptoSelectedImWrOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts.cryptoSelectedImWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts.chevronUpOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts.chevronUpOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts.chevronDownOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts.chevronDownOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts.menuItemOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts.menuItemOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts.cryptoSelectedNameInOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts.cryptoSelectedNameInOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts.innerSpanOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts.innerSpanOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts.popupOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @typedef {!Object} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts.popupOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts.prototype.cryptoMenuOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts.prototype.cryptoDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts.prototype.cryptoSelectedBlOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts.prototype.noCryptoDropItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts.prototype.cryptoSelectedImWrOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts.prototype.chevronUpOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts.prototype.chevronDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts.prototype.menuItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts.prototype.cryptoSelectedNameInOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts.prototype.innerSpanOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts.prototype.popupOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts}
 */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs}
 */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts.prototype.cryptoMenuOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts.prototype.cryptoDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts.prototype.cryptoSelectedBlOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts.prototype.noCryptoDropItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts.prototype.cryptoSelectedImWrOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts.prototype.chevronUpOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts.prototype.chevronDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts.prototype.menuItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts.prototype.cryptoSelectedNameInOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts.prototype.innerSpanOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts.prototype.popupOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts}
 */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs}
 */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts_Safe.prototype.cryptoMenuOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoMenuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts_Safe.prototype.cryptoDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoDownOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts_Safe.prototype.cryptoSelectedBlOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedBlOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts_Safe.prototype.noCryptoDropItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.NoCryptoDropItemOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts_Safe.prototype.cryptoSelectedImWrOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedImWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts_Safe.prototype.chevronUpOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronUpOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts_Safe.prototype.chevronDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.ChevronDownOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts_Safe.prototype.menuItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.MenuItemOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts_Safe.prototype.cryptoSelectedNameInOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.CryptoSelectedNameInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts_Safe.prototype.innerSpanOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.InnerSpanOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts_Safe.prototype.popupOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.Inputs.PopupOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts_Safe.prototype.cryptoMenuOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoMenuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts_Safe.prototype.cryptoDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoDownOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts_Safe.prototype.cryptoSelectedBlOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedBlOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts_Safe.prototype.noCryptoDropItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.NoCryptoDropItemOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts_Safe.prototype.cryptoSelectedImWrOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedImWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts_Safe.prototype.chevronUpOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronUpOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts_Safe.prototype.chevronDownOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.ChevronDownOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts_Safe.prototype.menuItemOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.MenuItemOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts_Safe.prototype.cryptoSelectedNameInOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.CryptoSelectedNameInOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts_Safe.prototype.innerSpanOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.InnerSpanOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/140-ICryptoSelectElementPort.xml} xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts_Safe filter:!ControllerPlugin~props c8b65c1603733b231c1ec8ceaafd3917 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts_Safe.prototype.popupOpts
/** @typedef {$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts_Safe} */
xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs.PopupOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElementPort.WeakInputs
/* @typal-end */