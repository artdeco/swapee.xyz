/** @const {?} */ $xyz.swapee.wc.back.ICryptoSelectTouchscreenAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} */
xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ICryptoSelectTouchscreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/** @interface */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT} */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster.prototype.asICryptoSelectTouchscreenAT
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT} */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster.prototype.superCryptoSelectTouchscreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.ICryptoSelectTouchscreenAT filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.ICryptoSelectTouchscreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.ICryptoSelectTouchscreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.CryptoSelectTouchscreenAT filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectTouchscreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese>}
 */
$xyz.swapee.wc.back.CryptoSelectTouchscreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreenAT
/** @type {function(new: xyz.swapee.wc.back.ICryptoSelectTouchscreenAT, ...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese)} */
xyz.swapee.wc.back.CryptoSelectTouchscreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.CryptoSelectTouchscreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
$xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT)} */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT|typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.CryptoSelectTouchscreenATConstructor filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectTouchscreenAT, ...!xyz.swapee.wc.back.ICryptoSelectTouchscreenAT.Initialese)} */
xyz.swapee.wc.back.CryptoSelectTouchscreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.RecordICryptoSelectTouchscreenAT filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectTouchscreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectTouchscreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT} */
xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/74-ICryptoSelectTouchscreenAT.xml} xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT filter:!ControllerPlugin~props 4c832c03aaaa5d68fb969fb5f843e540 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT} */
xyz.swapee.wc.back.BoundCryptoSelectTouchscreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */