/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/41-CryptoSelectClasses.xml} xyz.swapee.wc.CryptoSelectClasses filter:!ControllerPlugin~props 1c6e1ed1f55d84e6ac42e3df45101927 */
/** @record */
$xyz.swapee.wc.CryptoSelectClasses = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDownBorder
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.Fiat
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.ItemHovered
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDropItem
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDownCollapsed
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDown
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.Sm
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.Expanded
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.SmWide
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.MouseOver
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.DropDownKeyboardFocus
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.HoveringOverItem
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.ImgWr
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.KeyboardSelect
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.Highlight
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.Chevron
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.NoCoins
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.Pill
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.CryptoDropItemBeforeHover
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectClasses.prototype.BackgroundStalk
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.CryptoSelectClasses}
 */
xyz.swapee.wc.CryptoSelectClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */