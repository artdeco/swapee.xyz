/** @const {?} */ $xyz.swapee.wc.ICryptoSelectProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.ICryptoSelectProcessor.Initialese filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectComputer.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectProcessor.Initialese} */
xyz.swapee.wc.ICryptoSelectProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.ICryptoSelectProcessorCaster filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectProcessor} */
$xyz.swapee.wc.ICryptoSelectProcessorCaster.prototype.asICryptoSelectProcessor
/** @type {!xyz.swapee.wc.BoundCryptoSelectProcessor} */
$xyz.swapee.wc.ICryptoSelectProcessorCaster.prototype.superCryptoSelectProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectProcessorCaster}
 */
xyz.swapee.wc.ICryptoSelectProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.ICryptoSelectProcessor filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessorCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer}
 * @extends {xyz.swapee.wc.ICryptoSelectCore}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 */
$xyz.swapee.wc.ICryptoSelectProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectProcessor}
 */
xyz.swapee.wc.ICryptoSelectProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.CryptoSelectProcessor filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectProcessor.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.CryptoSelectProcessor
/** @type {function(new: xyz.swapee.wc.ICryptoSelectProcessor, ...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese)} */
xyz.swapee.wc.CryptoSelectProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.CryptoSelectProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.AbstractCryptoSelectProcessor filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectProcessor}
 */
$xyz.swapee.wc.AbstractCryptoSelectProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectProcessor)} */
xyz.swapee.wc.AbstractCryptoSelectProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectProcessor}
 */
xyz.swapee.wc.AbstractCryptoSelectProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.CryptoSelectProcessorConstructor filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectProcessor, ...!xyz.swapee.wc.ICryptoSelectProcessor.Initialese)} */
xyz.swapee.wc.CryptoSelectProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.RecordICryptoSelectProcessor filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.BoundICryptoSelectProcessor filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordICryptoSelectProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessorCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 * @extends {xyz.swapee.wc.BoundICryptoSelectCore}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 */
$xyz.swapee.wc.BoundICryptoSelectProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectProcessor} */
xyz.swapee.wc.BoundICryptoSelectProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/10-ICryptoSelectProcessor.xml} xyz.swapee.wc.BoundCryptoSelectProcessor filter:!ControllerPlugin~props 59cbcab364a5589a883f556c3f43a000 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectProcessor} */
xyz.swapee.wc.BoundCryptoSelectProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */