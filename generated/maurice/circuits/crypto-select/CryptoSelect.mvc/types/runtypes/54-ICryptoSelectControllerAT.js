/** @const {?} */ $xyz.swapee.wc.front.ICryptoSelectControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} */
xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ICryptoSelectControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.ICryptoSelectControllerATCaster filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/** @interface */
$xyz.swapee.wc.front.ICryptoSelectControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundICryptoSelectControllerAT} */
$xyz.swapee.wc.front.ICryptoSelectControllerATCaster.prototype.asICryptoSelectControllerAT
/** @type {!xyz.swapee.wc.front.BoundCryptoSelectControllerAT} */
$xyz.swapee.wc.front.ICryptoSelectControllerATCaster.prototype.superCryptoSelectControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ICryptoSelectControllerATCaster}
 */
xyz.swapee.wc.front.ICryptoSelectControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.ICryptoSelectControllerAT filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.ICryptoSelectControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ICryptoSelectControllerAT}
 */
xyz.swapee.wc.front.ICryptoSelectControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.CryptoSelectControllerAT filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.ICryptoSelectControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.CryptoSelectControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.CryptoSelectControllerAT
/** @type {function(new: xyz.swapee.wc.front.ICryptoSelectControllerAT, ...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese)} */
xyz.swapee.wc.front.CryptoSelectControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.CryptoSelectControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.AbstractCryptoSelectControllerAT filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
$xyz.swapee.wc.front.AbstractCryptoSelectControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractCryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractCryptoSelectControllerAT)} */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractCryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectControllerAT}
 */
xyz.swapee.wc.front.AbstractCryptoSelectControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.CryptoSelectControllerATConstructor filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/** @typedef {function(new: xyz.swapee.wc.front.ICryptoSelectControllerAT, ...!xyz.swapee.wc.front.ICryptoSelectControllerAT.Initialese)} */
xyz.swapee.wc.front.CryptoSelectControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.RecordICryptoSelectControllerAT filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordICryptoSelectControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.BoundICryptoSelectControllerAT filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordICryptoSelectControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundICryptoSelectControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundICryptoSelectControllerAT} */
xyz.swapee.wc.front.BoundICryptoSelectControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/54-ICryptoSelectControllerAT.xml} xyz.swapee.wc.front.BoundCryptoSelectControllerAT filter:!ControllerPlugin~props b622e1f612432744a05c01569a008424 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundCryptoSelectControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundCryptoSelectControllerAT} */
xyz.swapee.wc.front.BoundCryptoSelectControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */