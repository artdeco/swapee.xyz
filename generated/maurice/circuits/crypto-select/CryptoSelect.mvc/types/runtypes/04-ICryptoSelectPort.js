/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController.Inputs
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController.WeakInputs
/** @const {?} */ xyz.swapee.wc.ICryptoSelectController
/** @const {?} */ xyz.swapee.wc.ICryptoSelectController.Inputs
/** @const {?} */ xyz.swapee.wc.ICryptoSelectController.WeakInputs
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Initialese filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Initialese} */
xyz.swapee.wc.ICryptoSelectPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortFields filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectPortFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelectPort.Inputs} */
$xyz.swapee.wc.ICryptoSelectPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ICryptoSelectPort.Inputs} */
$xyz.swapee.wc.ICryptoSelectPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectPortFields}
 */
xyz.swapee.wc.ICryptoSelectPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortCaster filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectPort} */
$xyz.swapee.wc.ICryptoSelectPortCaster.prototype.asICryptoSelectPort
/** @type {!xyz.swapee.wc.BoundCryptoSelectPort} */
$xyz.swapee.wc.ICryptoSelectPortCaster.prototype.superCryptoSelectPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectPortCaster}
 */
xyz.swapee.wc.ICryptoSelectPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ICryptoSelectPort.Inputs>}
 */
$xyz.swapee.wc.ICryptoSelectPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectPort.prototype.resetCryptoSelectPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectPort}
 */
xyz.swapee.wc.ICryptoSelectPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.CryptoSelectPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectPort.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.CryptoSelectPort
/** @type {function(new: xyz.swapee.wc.ICryptoSelectPort, ...!xyz.swapee.wc.ICryptoSelectPort.Initialese)} */
xyz.swapee.wc.CryptoSelectPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.CryptoSelectPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.AbstractCryptoSelectPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectPort}
 */
$xyz.swapee.wc.AbstractCryptoSelectPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectPort)} */
xyz.swapee.wc.AbstractCryptoSelectPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectPort|typeof xyz.swapee.wc.CryptoSelectPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectPort|typeof xyz.swapee.wc.CryptoSelectPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectPort|typeof xyz.swapee.wc.CryptoSelectPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectPort}
 */
xyz.swapee.wc.AbstractCryptoSelectPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.CryptoSelectPortConstructor filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectPort, ...!xyz.swapee.wc.ICryptoSelectPort.Initialese)} */
xyz.swapee.wc.CryptoSelectPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.RecordICryptoSelectPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @typedef {{ resetPort: xyz.swapee.wc.ICryptoSelectPort.resetPort, resetCryptoSelectPort: xyz.swapee.wc.ICryptoSelectPort.resetCryptoSelectPort }} */
xyz.swapee.wc.RecordICryptoSelectPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.BoundICryptoSelectPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPortFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ICryptoSelectPort.Inputs>}
 */
$xyz.swapee.wc.BoundICryptoSelectPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectPort} */
xyz.swapee.wc.BoundICryptoSelectPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.BoundCryptoSelectPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectPort} */
xyz.swapee.wc.BoundCryptoSelectPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.resetPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectPort): void} */
xyz.swapee.wc.ICryptoSelectPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectPort.__resetPort} */
xyz.swapee.wc.ICryptoSelectPort.__resetPort

// nss:xyz.swapee.wc.ICryptoSelectPort,$xyz.swapee.wc.ICryptoSelectPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.resetCryptoSelectPort filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectPort.__resetCryptoSelectPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectPort.resetCryptoSelectPort
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectPort): void} */
xyz.swapee.wc.ICryptoSelectPort._resetCryptoSelectPort
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectPort.__resetCryptoSelectPort} */
xyz.swapee.wc.ICryptoSelectPort.__resetCryptoSelectPort

// nss:xyz.swapee.wc.ICryptoSelectPort,$xyz.swapee.wc.ICryptoSelectPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Source.source filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source.source

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Source filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Source = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Source.prototype.source
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Source} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel}
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ICryptoSelectPort.Inputs}
 */
xyz.swapee.wc.ICryptoSelectPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source.prototype.source
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel}
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs}
 */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortInterface filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectPortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectPortInterface}
 */
xyz.swapee.wc.ICryptoSelectPortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.CryptoSelectPortInterface filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectPortInterface}
 */
$xyz.swapee.wc.CryptoSelectPortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectPortInterface}
 */
xyz.swapee.wc.CryptoSelectPortInterface
/** @type {function(new: xyz.swapee.wc.ICryptoSelectPortInterface)} */
xyz.swapee.wc.CryptoSelectPortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPortInterface.Props filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.selected
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.iconFolder
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.fiat
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.imHtml
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.search
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.selectedIcon
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.menuExpanded
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.isMouseOver
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.sm
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.isSearching
/** @type {number|undefined} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.hoveringIndex
/** @type {Map<string, { icon: string, displayName: string, name: string }>} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.cryptos
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.ignoreCryptos
/** @type {!Array<string>} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.visibleItems
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.keyboardSelected
/** @type {?{ icon: string, displayName: string, name: string }} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.selectedCrypto
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectPortInterface.Props.prototype.matchedKeys
/** @typedef {$xyz.swapee.wc.ICryptoSelectPortInterface.Props} */
xyz.swapee.wc.ICryptoSelectPortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPortInterface
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe.prototype.source
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/** @record */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe.prototype.source
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.Inputs.Source filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source}
 */
$xyz.swapee.wc.ICryptoSelectController.Inputs.Source = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.Inputs.Source} */
xyz.swapee.wc.ICryptoSelectController.Inputs.Source

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.Inputs.Source_Safe filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe}
 */
$xyz.swapee.wc.ICryptoSelectController.Inputs.Source_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.Inputs.Source_Safe} */
xyz.swapee.wc.ICryptoSelectController.Inputs.Source_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source}
 */
$xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source} */
xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/04-ICryptoSelectPort.xml} xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source_Safe filter:!ControllerPlugin~props 8279f1256cf1083eec76d14716017c13 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Source_Safe}
 */
$xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source_Safe} */
xyz.swapee.wc.ICryptoSelectController.WeakInputs.Source_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.WeakInputs
/* @typal-end */