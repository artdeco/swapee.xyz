/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController.calibrateDataSource
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver
/** @const {?} */ xyz.swapee.wc.ICryptoSelectController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.Initialese filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel>}
 */
$xyz.swapee.wc.ICryptoSelectController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.Initialese} */
xyz.swapee.wc.ICryptoSelectController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectControllerFields filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectControllerFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelectController.Inputs} */
$xyz.swapee.wc.ICryptoSelectControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectControllerFields}
 */
xyz.swapee.wc.ICryptoSelectControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectControllerCaster filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectController} */
$xyz.swapee.wc.ICryptoSelectControllerCaster.prototype.asICryptoSelectController
/** @type {!xyz.swapee.wc.BoundICryptoSelectProcessor} */
$xyz.swapee.wc.ICryptoSelectControllerCaster.prototype.asICryptoSelectProcessor
/** @type {!xyz.swapee.wc.BoundCryptoSelectController} */
$xyz.swapee.wc.ICryptoSelectControllerCaster.prototype.superCryptoSelectController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectControllerCaster}
 */
xyz.swapee.wc.ICryptoSelectControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
$xyz.swapee.wc.ICryptoSelectController = function() {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectController.prototype.resetPort = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectController.prototype.flipMenuExpanded = function() {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)}
 */
$xyz.swapee.wc.ICryptoSelectController.prototype.calibrateDataSource = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)}
 */
$xyz.swapee.wc.ICryptoSelectController.prototype.calibrateResetMouseOver = function(form, changes) {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectController.prototype.setSelected = function(val) {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectController.prototype.unsetSelected = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectController}
 */
xyz.swapee.wc.ICryptoSelectController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.CryptoSelectController filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectController.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.CryptoSelectController
/** @type {function(new: xyz.swapee.wc.ICryptoSelectController, ...!xyz.swapee.wc.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.CryptoSelectController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.CryptoSelectController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.AbstractCryptoSelectController filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectController}
 */
$xyz.swapee.wc.AbstractCryptoSelectController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectController)} */
xyz.swapee.wc.AbstractCryptoSelectController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectController}
 */
xyz.swapee.wc.AbstractCryptoSelectController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.CryptoSelectControllerConstructor filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectController, ...!xyz.swapee.wc.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.CryptoSelectControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource = __$te_Mixin()
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props)|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.prototype.props
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability)|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.prototype.variability
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource}
 */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability.prototype.field_source
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability.prototype.required_source
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability.prototype.return_cryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability} */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Variability}
 */
$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props} */
xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.CalibrateDataSource
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver = __$te_Mixin()
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props)|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.prototype.props
/** @type {(!xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability)|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.prototype.variability
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver}
 */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @record */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability.prototype.field_isMouseOver
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability.prototype.required_isMouseOver
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability.prototype.return_isMouseOver
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability} */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Variability}
 */
$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props} */
xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.CalibrateResetMouseOver
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.RecordICryptoSelectController filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/** @typedef {{ resetPort: xyz.swapee.wc.ICryptoSelectController.resetPort, flipMenuExpanded: xyz.swapee.wc.ICryptoSelectController.flipMenuExpanded, calibrateDataSource: xyz.swapee.wc.ICryptoSelectController.calibrateDataSource, calibrateResetMouseOver: xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver, setSelected: xyz.swapee.wc.ICryptoSelectController.setSelected, unsetSelected: xyz.swapee.wc.ICryptoSelectController.unsetSelected }} */
xyz.swapee.wc.RecordICryptoSelectController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.BoundICryptoSelectController filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectControllerFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.ICryptoSelectController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
$xyz.swapee.wc.BoundICryptoSelectController = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectController} */
xyz.swapee.wc.BoundICryptoSelectController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.BoundCryptoSelectController filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectController = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectController} */
xyz.swapee.wc.BoundCryptoSelectController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.resetPort filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectController.resetPort
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController): void} */
xyz.swapee.wc.ICryptoSelectController._resetPort
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.__resetPort} */
xyz.swapee.wc.ICryptoSelectController.__resetPort

// nss:xyz.swapee.wc.ICryptoSelectController,$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.flipMenuExpanded filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.ICryptoSelectController.__flipMenuExpanded = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ICryptoSelectController.flipMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController)} */
xyz.swapee.wc.ICryptoSelectController._flipMenuExpanded
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.__flipMenuExpanded} */
xyz.swapee.wc.ICryptoSelectController.__flipMenuExpanded

// nss:xyz.swapee.wc.ICryptoSelectController,$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateDataSource filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)}
 */
$xyz.swapee.wc.ICryptoSelectController.calibrateDataSource = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectController}
 */
$xyz.swapee.wc.ICryptoSelectController._calibrateDataSource = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectController.__calibrateDataSource = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.calibrateDataSource} */
xyz.swapee.wc.ICryptoSelectController.calibrateDataSource
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController._calibrateDataSource} */
xyz.swapee.wc.ICryptoSelectController._calibrateDataSource
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.__calibrateDataSource} */
xyz.swapee.wc.ICryptoSelectController.__calibrateDataSource

// nss:xyz.swapee.wc.ICryptoSelectController,$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)}
 */
$xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectController}
 */
$xyz.swapee.wc.ICryptoSelectController._calibrateResetMouseOver = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectController.__calibrateResetMouseOver = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver} */
xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController._calibrateResetMouseOver} */
xyz.swapee.wc.ICryptoSelectController._calibrateResetMouseOver
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.__calibrateResetMouseOver} */
xyz.swapee.wc.ICryptoSelectController.__calibrateResetMouseOver

// nss:xyz.swapee.wc.ICryptoSelectController,$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.setSelected filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectController.__setSelected = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.ICryptoSelectController.setSelected
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController, string): void} */
xyz.swapee.wc.ICryptoSelectController._setSelected
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.__setSelected} */
xyz.swapee.wc.ICryptoSelectController.__setSelected

// nss:xyz.swapee.wc.ICryptoSelectController,$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.unsetSelected filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectController.__unsetSelected = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectController.unsetSelected
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectController): void} */
xyz.swapee.wc.ICryptoSelectController._unsetSelected
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectController.__unsetSelected} */
xyz.swapee.wc.ICryptoSelectController.__unsetSelected

// nss:xyz.swapee.wc.ICryptoSelectController,$xyz.swapee.wc.ICryptoSelectController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.Inputs filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs}
 */
$xyz.swapee.wc.ICryptoSelectController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.Inputs} */
xyz.swapee.wc.ICryptoSelectController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.WeakInputs filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.WeakInputs}
 */
$xyz.swapee.wc.ICryptoSelectController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.WeakInputs} */
xyz.swapee.wc.ICryptoSelectController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Source_Safe}
 */
$xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form} */
xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.calibrateDataSource
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos}
 */
$xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return} */
xyz.swapee.wc.ICryptoSelectController.calibrateDataSource.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.calibrateDataSource
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe}
 */
$xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form} */
xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/50-ICryptoSelectController.xml} xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return filter:!ControllerPlugin~props 0a756e0257e82f3c83b96a246c9ac071 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver}
 */
$xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return} */
xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectController.calibrateResetMouseOver
/* @typal-end */