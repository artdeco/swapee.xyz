/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.CryptoSelectMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.selected
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.iconFolder
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.fiat
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.imHtml
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.search
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.menuExpanded
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.isMouseOver
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.sm
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.isSearching
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.hoveringIndex
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.cryptos
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.visibleItems
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.keyboardSelected
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.selectedCrypto
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryPQs.prototype.matchedKeys
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.CryptoSelectMemoryPQs}
 */
xyz.swapee.wc.CryptoSelectMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.CryptoSelectMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.ef7de
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f756a
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.hd5f3
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.b593a
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.a6a94
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.d353a
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.h1df4
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.ed79a
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f8419
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.c7b81
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.ja8a8
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.adbda
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f1606
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.b1a11
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.f86aa
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectMemoryQPs}
 */
xyz.swapee.wc.CryptoSelectMemoryQPs
/** @type {function(new: xyz.swapee.wc.CryptoSelectMemoryQPs)} */
xyz.swapee.wc.CryptoSelectMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.CryptoSelectInputsPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.CryptoSelectInputsPQs.prototype.source
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.CryptoSelectInputsPQs}
 */
xyz.swapee.wc.CryptoSelectInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.CryptoSelectInputsQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.CryptoSelectInputsQPs.prototype.d6cd3
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectInputsQPs}
 */
xyz.swapee.wc.CryptoSelectInputsQPs
/** @type {function(new: xyz.swapee.wc.CryptoSelectInputsQPs)} */
xyz.swapee.wc.CryptoSelectInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectCachePQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.CryptoSelectCachePQs = function() {}
/** @type {string} */
$xyz.swapee.wc.CryptoSelectCachePQs.prototype.wideness
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.CryptoSelectCachePQs}
 */
xyz.swapee.wc.CryptoSelectCachePQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectCacheQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.CryptoSelectCacheQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.CryptoSelectCacheQPs.prototype.b67ac
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectCacheQPs}
 */
xyz.swapee.wc.CryptoSelectCacheQPs
/** @type {function(new: xyz.swapee.wc.CryptoSelectCacheQPs)} */
xyz.swapee.wc.CryptoSelectCacheQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.CryptoSelectVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoSelectedBl
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoSelectedImWr
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoSelectedNameIn
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoMenu
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.MenuItems
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoDropItem
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.ChevronUp
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.ChevronDown
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.NoCryptoDropItem
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.CryptoDown
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusPQs.prototype.Popup
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.CryptoSelectVdusPQs}
 */
xyz.swapee.wc.CryptoSelectVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.CryptoSelectVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b31
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b32
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b33
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b34
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b35
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b36
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b37
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b38
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b39
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b310
/** @type {string} */
$xyz.swapee.wc.CryptoSelectVdusQPs.prototype.a3b311
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectVdusQPs}
 */
xyz.swapee.wc.CryptoSelectVdusQPs
/** @type {function(new: xyz.swapee.wc.CryptoSelectVdusQPs)} */
xyz.swapee.wc.CryptoSelectVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectClassesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.CryptoSelectClassesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDownBorder
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Fiat
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.ItemHovered
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDropItem
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDownCollapsed
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDown
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Sm
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Expanded
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.SmWide
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.MouseOver
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.DropDownKeyboardFocus
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.HoveringOverItem
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.ImgWr
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.KeyboardSelect
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Highlight
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Chevron
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.NoCoins
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.Pill
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.CryptoDropItemBeforeHover
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesPQs.prototype.BackgroundStalk
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.CryptoSelectClassesPQs}
 */
xyz.swapee.wc.CryptoSelectClassesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/110-CryptoSelectSerDes.xml} xyz.swapee.wc.CryptoSelectClassesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.CryptoSelectClassesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.d7551
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.c880c
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ie5a0
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.hdecb
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ga1d1
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.hbd64
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.c0c4c
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.g3f6b
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.g785a
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.h3365
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.i3f9e
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.i4661
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.cdcdb
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.eefef
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ab905
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.ja37a
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.eeda9
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.f5797
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.adf16
/** @type {string} */
$xyz.swapee.wc.CryptoSelectClassesQPs.prototype.db2f0
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectClassesQPs}
 */
xyz.swapee.wc.CryptoSelectClassesQPs
/** @type {function(new: xyz.swapee.wc.CryptoSelectClassesQPs)} */
xyz.swapee.wc.CryptoSelectClassesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */