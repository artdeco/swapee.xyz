/** @const {?} */ $xyz.swapee.wc.ICryptoSelectHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectController.Initialese}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreen.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelect.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} */
xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.ICryptoSelectHtmlComponentCaster filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectHtmlComponent} */
$xyz.swapee.wc.ICryptoSelectHtmlComponentCaster.prototype.asICryptoSelectHtmlComponent
/** @type {!xyz.swapee.wc.BoundCryptoSelectHtmlComponent} */
$xyz.swapee.wc.ICryptoSelectHtmlComponentCaster.prototype.superCryptoSelectHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectHtmlComponentCaster}
 */
xyz.swapee.wc.ICryptoSelectHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.ICryptoSelectHtmlComponent filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.ICryptoSelectController}
 * @extends {xyz.swapee.wc.back.ICryptoSelectTouchscreen}
 * @extends {xyz.swapee.wc.ICryptoSelect}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.ICryptoSelectGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !HTMLDivElement, !xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer}
 */
$xyz.swapee.wc.ICryptoSelectHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectHtmlComponent}
 */
xyz.swapee.wc.ICryptoSelectHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.CryptoSelectHtmlComponent filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.CryptoSelectHtmlComponent
/** @type {function(new: xyz.swapee.wc.ICryptoSelectHtmlComponent, ...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese)} */
xyz.swapee.wc.CryptoSelectHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.CryptoSelectHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.AbstractCryptoSelectHtmlComponent filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
$xyz.swapee.wc.AbstractCryptoSelectHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectHtmlComponent)} */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectHtmlComponent|typeof xyz.swapee.wc.CryptoSelectHtmlComponent)|(!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectHtmlComponent|typeof xyz.swapee.wc.CryptoSelectHtmlComponent)|(!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectHtmlComponent|typeof xyz.swapee.wc.CryptoSelectHtmlComponent)|(!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.back.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.back.CryptoSelectTouchscreen)|(!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
xyz.swapee.wc.AbstractCryptoSelectHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.CryptoSelectHtmlComponentConstructor filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectHtmlComponent, ...!xyz.swapee.wc.ICryptoSelectHtmlComponent.Initialese)} */
xyz.swapee.wc.CryptoSelectHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.RecordICryptoSelectHtmlComponent filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.BoundICryptoSelectHtmlComponent filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordICryptoSelectHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectController}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectTouchscreen}
 * @extends {xyz.swapee.wc.BoundICryptoSelect}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.BoundICryptoSelectGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !HTMLDivElement, !xyz.swapee.wc.CryptoSelectLand>}
 * @extends {xyz.swapee.wc.BoundICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 */
$xyz.swapee.wc.BoundICryptoSelectHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectHtmlComponent} */
xyz.swapee.wc.BoundICryptoSelectHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/12-ICryptoSelectHtmlComponent.xml} xyz.swapee.wc.BoundCryptoSelectHtmlComponent filter:!ControllerPlugin~props 6b3586c40a016e50411760e277d8adc5 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectHtmlComponent} */
xyz.swapee.wc.BoundCryptoSelectHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */