/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDesigner
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @interface */
$xyz.swapee.wc.ICryptoSelectDesigner = function() {}
/**
 * @param {xyz.swapee.wc.CryptoSelectClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ICryptoSelectDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.CryptoSelectClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ICryptoSelectDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.CryptoSelectClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ICryptoSelectDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectDesigner}
 */
xyz.swapee.wc.ICryptoSelectDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.CryptoSelectDesigner filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectDesigner}
 */
$xyz.swapee.wc.CryptoSelectDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectDesigner}
 */
xyz.swapee.wc.CryptoSelectDesigner
/** @type {function(new: xyz.swapee.wc.ICryptoSelectDesigner)} */
xyz.swapee.wc.CryptoSelectDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @record */
$xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh.prototype.Popup
/** @type {typeof xyz.swapee.wc.ICryptoSelectController} */
$xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh.prototype.CryptoSelect
/** @typedef {$xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh} */
xyz.swapee.wc.ICryptoSelectDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @record */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh.prototype.Popup
/** @type {typeof xyz.swapee.wc.ICryptoSelectController} */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh.prototype.CryptoSelect
/** @type {typeof xyz.swapee.wc.ICryptoSelectController} */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/170-ICryptoSelectDesigner.xml} xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool filter:!ControllerPlugin~props d475ff86fee094a5044bf58be085b4ec */
/** @record */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool.prototype.Popup
/** @type {!xyz.swapee.wc.CryptoSelectMemory} */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool.prototype.CryptoSelect
/** @type {!xyz.swapee.wc.CryptoSelectMemory} */
$xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool} */
xyz.swapee.wc.ICryptoSelectDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDesigner.relay
/* @typal-end */