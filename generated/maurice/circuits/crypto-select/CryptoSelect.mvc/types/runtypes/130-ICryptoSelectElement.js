/** @const {?} */ $xyz.swapee.wc.ICryptoSelectElement
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectElement.build
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectElement.short
/** @const {?} */ xyz.swapee.wc.ICryptoSelectElement
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.Initialese filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @record
 * @extends {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.CryptoSelectLand>}
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectElement.Initialese} */
xyz.swapee.wc.ICryptoSelectElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElementFields filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectElementFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelectElement.Inputs} */
$xyz.swapee.wc.ICryptoSelectElementFields.prototype.inputs
/** @type {!Object<string, !Object<string, number>>} */
$xyz.swapee.wc.ICryptoSelectElementFields.prototype.buildees
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectElementFields}
 */
xyz.swapee.wc.ICryptoSelectElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElementCaster filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectElement} */
$xyz.swapee.wc.ICryptoSelectElementCaster.prototype.asICryptoSelectElement
/** @type {!xyz.swapee.wc.BoundCryptoSelectElement} */
$xyz.swapee.wc.ICryptoSelectElementCaster.prototype.superCryptoSelectElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectElementCaster}
 */
xyz.swapee.wc.ICryptoSelectElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectElement.Inputs, !xyz.swapee.wc.CryptoSelectLand>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.ICryptoSelectElement = function() {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.ICryptoSelectElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ICryptoSelectElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Cores} cores
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Instances} instances
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectElement.prototype.build = function(cores, instances) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectElement.prototype.buildPopup = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Ports} ports
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Cores} cores
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectElement.prototype.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ICryptoSelectElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [model]
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectElement}
 */
xyz.swapee.wc.ICryptoSelectElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.CryptoSelectElement filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElement.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectElement.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectElement}
 */
xyz.swapee.wc.CryptoSelectElement
/** @type {function(new: xyz.swapee.wc.ICryptoSelectElement, ...!xyz.swapee.wc.ICryptoSelectElement.Initialese)} */
xyz.swapee.wc.CryptoSelectElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectElement}
 */
xyz.swapee.wc.CryptoSelectElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.AbstractCryptoSelectElement filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElement.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectElement}
 */
$xyz.swapee.wc.AbstractCryptoSelectElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectElement}
 */
xyz.swapee.wc.AbstractCryptoSelectElement
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectElement)} */
xyz.swapee.wc.AbstractCryptoSelectElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectElement|typeof xyz.swapee.wc.CryptoSelectElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectElement}
 */
xyz.swapee.wc.AbstractCryptoSelectElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectElement}
 */
xyz.swapee.wc.AbstractCryptoSelectElement.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectElement|typeof xyz.swapee.wc.CryptoSelectElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectElement}
 */
xyz.swapee.wc.AbstractCryptoSelectElement.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectElement|typeof xyz.swapee.wc.CryptoSelectElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectElement}
 */
xyz.swapee.wc.AbstractCryptoSelectElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.CryptoSelectElementConstructor filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectElement, ...!xyz.swapee.wc.ICryptoSelectElement.Initialese)} */
xyz.swapee.wc.CryptoSelectElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.RecordICryptoSelectElement filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @typedef {{ solder: xyz.swapee.wc.ICryptoSelectElement.solder, render: xyz.swapee.wc.ICryptoSelectElement.render, build: xyz.swapee.wc.ICryptoSelectElement.build, buildPopup: xyz.swapee.wc.ICryptoSelectElement.buildPopup, short: xyz.swapee.wc.ICryptoSelectElement.short, server: xyz.swapee.wc.ICryptoSelectElement.server, inducer: xyz.swapee.wc.ICryptoSelectElement.inducer }} */
xyz.swapee.wc.RecordICryptoSelectElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.BoundICryptoSelectElement filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectElementFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectElement.Inputs, !xyz.swapee.wc.CryptoSelectLand>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.BoundICryptoSelectElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectElement} */
xyz.swapee.wc.BoundICryptoSelectElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.BoundCryptoSelectElement filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectElement} */
xyz.swapee.wc.BoundCryptoSelectElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.solder filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.ICryptoSelectElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.ICryptoSelectElement}
 */
$xyz.swapee.wc.ICryptoSelectElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.solder} */
xyz.swapee.wc.ICryptoSelectElement.solder
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement._solder} */
xyz.swapee.wc.ICryptoSelectElement._solder
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.__solder} */
xyz.swapee.wc.ICryptoSelectElement.__solder

// nss:xyz.swapee.wc.ICryptoSelectElement,$xyz.swapee.wc.ICryptoSelectElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.render filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ICryptoSelectElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.CryptoSelectMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ICryptoSelectElement.render
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectElement, !xyz.swapee.wc.CryptoSelectMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ICryptoSelectElement._render
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.__render} */
xyz.swapee.wc.ICryptoSelectElement.__render

// nss:xyz.swapee.wc.ICryptoSelectElement,$xyz.swapee.wc.ICryptoSelectElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.build filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Cores} cores
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Instances} instances
 */
$xyz.swapee.wc.ICryptoSelectElement.build = function(cores, instances) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Cores} cores
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Instances} instances
 * @this {xyz.swapee.wc.ICryptoSelectElement}
 */
$xyz.swapee.wc.ICryptoSelectElement._build = function(cores, instances) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Cores} cores
 * @param {!xyz.swapee.wc.ICryptoSelectElement.build.Instances} instances
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectElement.__build = function(cores, instances) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.build} */
xyz.swapee.wc.ICryptoSelectElement.build
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement._build} */
xyz.swapee.wc.ICryptoSelectElement._build
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.__build} */
xyz.swapee.wc.ICryptoSelectElement.__build

// nss:xyz.swapee.wc.ICryptoSelectElement,$xyz.swapee.wc.ICryptoSelectElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.buildPopup filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.ICryptoSelectElement.__buildPopup = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.ICryptoSelectElement.buildPopup
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectElement, !Object, !Object)} */
xyz.swapee.wc.ICryptoSelectElement._buildPopup
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.__buildPopup} */
xyz.swapee.wc.ICryptoSelectElement.__buildPopup

// nss:xyz.swapee.wc.ICryptoSelectElement,$xyz.swapee.wc.ICryptoSelectElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.short filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Ports} ports
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Cores} cores
 */
$xyz.swapee.wc.ICryptoSelectElement.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Ports} ports
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Cores} cores
 * @this {xyz.swapee.wc.ICryptoSelectElement}
 */
$xyz.swapee.wc.ICryptoSelectElement._short = function(model, ports, cores) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} model
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Ports} ports
 * @param {!xyz.swapee.wc.ICryptoSelectElement.short.Cores} cores
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectElement.__short = function(model, ports, cores) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.short} */
xyz.swapee.wc.ICryptoSelectElement.short
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement._short} */
xyz.swapee.wc.ICryptoSelectElement._short
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.__short} */
xyz.swapee.wc.ICryptoSelectElement.__short

// nss:xyz.swapee.wc.ICryptoSelectElement,$xyz.swapee.wc.ICryptoSelectElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.server filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ICryptoSelectElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.ICryptoSelectElement}
 */
$xyz.swapee.wc.ICryptoSelectElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.server} */
xyz.swapee.wc.ICryptoSelectElement.server
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement._server} */
xyz.swapee.wc.ICryptoSelectElement._server
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.__server} */
xyz.swapee.wc.ICryptoSelectElement.__server

// nss:xyz.swapee.wc.ICryptoSelectElement,$xyz.swapee.wc.ICryptoSelectElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.inducer filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [model]
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} [port]
 */
$xyz.swapee.wc.ICryptoSelectElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [model]
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} [port]
 * @this {xyz.swapee.wc.ICryptoSelectElement}
 */
$xyz.swapee.wc.ICryptoSelectElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [model]
 * @param {!xyz.swapee.wc.ICryptoSelectElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.inducer} */
xyz.swapee.wc.ICryptoSelectElement.inducer
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement._inducer} */
xyz.swapee.wc.ICryptoSelectElement._inducer
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectElement.__inducer} */
xyz.swapee.wc.ICryptoSelectElement.__inducer

// nss:xyz.swapee.wc.ICryptoSelectElement,$xyz.swapee.wc.ICryptoSelectElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.Inputs filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectPort.Inputs}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplay.Queries}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.ICryptoSelectElementPort.Inputs}
 */
$xyz.swapee.wc.ICryptoSelectElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectElement.Inputs} */
xyz.swapee.wc.ICryptoSelectElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.build.Cores filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElement.build.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElement.build.Cores.prototype.Popup
/** @typedef {$xyz.swapee.wc.ICryptoSelectElement.build.Cores} */
xyz.swapee.wc.ICryptoSelectElement.build.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.build.Instances filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElement.build.Instances = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElement.build.Instances.prototype.Popup
/** @typedef {$xyz.swapee.wc.ICryptoSelectElement.build.Instances} */
xyz.swapee.wc.ICryptoSelectElement.build.Instances

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.short.Ports filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElement.short.Ports = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElement.short.Ports.prototype.Popup
/** @typedef {$xyz.swapee.wc.ICryptoSelectElement.short.Ports} */
xyz.swapee.wc.ICryptoSelectElement.short.Ports

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElement.short
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/130-ICryptoSelectElement.xml} xyz.swapee.wc.ICryptoSelectElement.short.Cores filter:!ControllerPlugin~props 64b803208e3b500b31d0b7dabbeb6d48 */
/** @record */
$xyz.swapee.wc.ICryptoSelectElement.short.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectElement.short.Cores.prototype.Popup
/** @typedef {$xyz.swapee.wc.ICryptoSelectElement.short.Cores} */
xyz.swapee.wc.ICryptoSelectElement.short.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectElement.short
/* @typal-end */