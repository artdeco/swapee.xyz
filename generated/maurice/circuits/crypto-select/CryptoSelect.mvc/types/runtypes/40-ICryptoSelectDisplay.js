/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDisplay
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDisplay.paintSearch
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput
/** @const {?} */ xyz.swapee.wc.ICryptoSelectDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.Initialese filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings>}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese = function() {}
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoMenu
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoDown
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedBl
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.NoCryptoDropItem
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedImWr
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.ChevronUp
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.ChevronDown
/** @type {(!Array<!HTMLSpanElement>)|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.MenuItems
/** @type {HTMLInputElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedNameIn
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.InnerSpan
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.ICryptoSelectDisplay.Initialese.prototype.Popup
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.Initialese} */
xyz.swapee.wc.ICryptoSelectDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplayFields filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectDisplayFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelectDisplay.Settings} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.ICryptoSelectDisplay.Queries} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.queries
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoMenu
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoDown
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoSelectedBl
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.NoCryptoDropItem
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoSelectedImWr
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.ChevronUp
/** @type {HTMLDivElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.ChevronDown
/** @type {!Array<!HTMLSpanElement>} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.MenuItems
/** @type {HTMLInputElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.CryptoSelectedNameIn
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.InnerSpan
/** @type {HTMLElement} */
$xyz.swapee.wc.ICryptoSelectDisplayFields.prototype.Popup
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectDisplayFields}
 */
xyz.swapee.wc.ICryptoSelectDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplayCaster filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectDisplay} */
$xyz.swapee.wc.ICryptoSelectDisplayCaster.prototype.asICryptoSelectDisplay
/** @type {!xyz.swapee.wc.BoundICryptoSelectTouchscreen} */
$xyz.swapee.wc.ICryptoSelectDisplayCaster.prototype.asICryptoSelectTouchscreen
/** @type {!xyz.swapee.wc.BoundCryptoSelectDisplay} */
$xyz.swapee.wc.ICryptoSelectDisplayCaster.prototype.superCryptoSelectDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectDisplayCaster}
 */
xyz.swapee.wc.ICryptoSelectDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.CryptoSelectMemory, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, xyz.swapee.wc.ICryptoSelectDisplay.Queries, null>}
 */
$xyz.swapee.wc.ICryptoSelectDisplay = function() {}
/**
 * @param {!MouseEvent} ev
 * @return {HTMLElement}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveMouseItem = function(ev) {}
/**
 * @param {number} index
 * @return {HTMLElement}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveItem = function(index) {}
/**
 * @param {string} key
 * @return {HTMLElement}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveItemByKey = function(key) {}
/**
 * @param {HTMLElement} cryptoItem
 * @return {number}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.resolveItemIndex = function(cryptoItem) {}
/**
 * @param {HTMLElement} cryptoItem
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.scrollItemIntoView = function(cryptoItem) {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.paint = function(memory, land) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory} memory
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSmHeight = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory} memory
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSearch = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory} memory
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSelectedImg = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory} memory
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintHoveringIndex = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory} memory
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintBeforeHovering = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory} memory
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.prototype.paintSearchInput = function(memory) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectDisplay}
 */
xyz.swapee.wc.ICryptoSelectDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.CryptoSelectDisplay filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectDisplay.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.CryptoSelectDisplay
/** @type {function(new: xyz.swapee.wc.ICryptoSelectDisplay, ...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese)} */
xyz.swapee.wc.CryptoSelectDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.CryptoSelectDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.AbstractCryptoSelectDisplay filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectDisplay}
 */
$xyz.swapee.wc.AbstractCryptoSelectDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectDisplay)} */
xyz.swapee.wc.AbstractCryptoSelectDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectDisplay}
 */
xyz.swapee.wc.AbstractCryptoSelectDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.CryptoSelectDisplayConstructor filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectDisplay, ...!xyz.swapee.wc.ICryptoSelectDisplay.Initialese)} */
xyz.swapee.wc.CryptoSelectDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.RecordICryptoSelectDisplay filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @typedef {{ resolveMouseItem: xyz.swapee.wc.ICryptoSelectDisplay.resolveMouseItem, resolveItem: xyz.swapee.wc.ICryptoSelectDisplay.resolveItem, resolveItemByKey: xyz.swapee.wc.ICryptoSelectDisplay.resolveItemByKey, resolveItemIndex: xyz.swapee.wc.ICryptoSelectDisplay.resolveItemIndex, scrollItemIntoView: xyz.swapee.wc.ICryptoSelectDisplay.scrollItemIntoView, paint: xyz.swapee.wc.ICryptoSelectDisplay.paint, paintSmHeight: xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight, paintSearch: xyz.swapee.wc.ICryptoSelectDisplay.paintSearch, paintSelectedImg: xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg, paintHoveringIndex: xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex, paintBeforeHovering: xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering, paintSearchInput: xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput }} */
xyz.swapee.wc.RecordICryptoSelectDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.BoundICryptoSelectDisplay filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.CryptoSelectMemory, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, xyz.swapee.wc.ICryptoSelectDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundICryptoSelectDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectDisplay} */
xyz.swapee.wc.BoundICryptoSelectDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.BoundCryptoSelectDisplay filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectDisplay} */
xyz.swapee.wc.BoundCryptoSelectDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveMouseItem filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!MouseEvent} ev
 * @return {HTMLElement}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__resolveMouseItem = function(ev) {}
/** @typedef {function(!MouseEvent): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveMouseItem
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !MouseEvent): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveMouseItem
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__resolveMouseItem} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveMouseItem

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveItem filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} index
 * @return {HTMLElement}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItem = function(index) {}
/** @typedef {function(number): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveItem
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, number): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveItem
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__resolveItem} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveItem

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveItemByKey filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} key
 * @return {HTMLElement}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemByKey = function(key) {}
/** @typedef {function(string): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveItemByKey
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, string): HTMLElement} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveItemByKey
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemByKey} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemByKey

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.resolveItemIndex filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {HTMLElement} cryptoItem
 * @return {number}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemIndex = function(cryptoItem) {}
/** @typedef {function(HTMLElement): number} */
xyz.swapee.wc.ICryptoSelectDisplay.resolveItemIndex
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, HTMLElement): number} */
xyz.swapee.wc.ICryptoSelectDisplay._resolveItemIndex
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemIndex} */
xyz.swapee.wc.ICryptoSelectDisplay.__resolveItemIndex

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.scrollItemIntoView filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {HTMLElement} cryptoItem
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__scrollItemIntoView = function(cryptoItem) {}
/** @typedef {function(HTMLElement)} */
xyz.swapee.wc.ICryptoSelectDisplay.scrollItemIntoView
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, HTMLElement)} */
xyz.swapee.wc.ICryptoSelectDisplay._scrollItemIntoView
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__scrollItemIntoView} */
xyz.swapee.wc.ICryptoSelectDisplay.__scrollItemIntoView

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paint filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.CryptoSelectMemory, null): void} */
xyz.swapee.wc.ICryptoSelectDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectDisplay, !xyz.swapee.wc.CryptoSelectMemory, null): void} */
xyz.swapee.wc.ICryptoSelectDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__paint} */
xyz.swapee.wc.ICryptoSelectDisplay.__paint

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory} memory */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory} memory
 * @this {xyz.swapee.wc.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectDisplay._paintSmHeight = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory} memory
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__paintSmHeight = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay._paintSmHeight} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSmHeight
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__paintSmHeight} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSmHeight

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearch filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory} memory */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSearch = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory} memory
 * @this {xyz.swapee.wc.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectDisplay._paintSearch = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory} memory
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__paintSearch = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.paintSearch} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearch
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay._paintSearch} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSearch
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__paintSearch} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSearch

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory} memory */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory} memory
 * @this {xyz.swapee.wc.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectDisplay._paintSelectedImg = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory} memory
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__paintSelectedImg = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay._paintSelectedImg} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSelectedImg
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__paintSelectedImg} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSelectedImg

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory} memory */
$xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory} memory
 * @this {xyz.swapee.wc.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectDisplay._paintHoveringIndex = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory} memory
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__paintHoveringIndex = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex} */
xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay._paintHoveringIndex} */
xyz.swapee.wc.ICryptoSelectDisplay._paintHoveringIndex
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__paintHoveringIndex} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintHoveringIndex

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory} memory */
$xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory} memory
 * @this {xyz.swapee.wc.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectDisplay._paintBeforeHovering = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory} memory
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__paintBeforeHovering = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering} */
xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay._paintBeforeHovering} */
xyz.swapee.wc.ICryptoSelectDisplay._paintBeforeHovering
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__paintBeforeHovering} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintBeforeHovering

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory} memory */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory} memory
 * @this {xyz.swapee.wc.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectDisplay._paintSearchInput = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory} memory
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.__paintSearchInput = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay._paintSearchInput} */
xyz.swapee.wc.ICryptoSelectDisplay._paintSearchInput
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectDisplay.__paintSearchInput} */
xyz.swapee.wc.ICryptoSelectDisplay.__paintSearchInput

// nss:xyz.swapee.wc.ICryptoSelectDisplay,$xyz.swapee.wc.ICryptoSelectDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.Queries filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/** @record */
$xyz.swapee.wc.ICryptoSelectDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.Queries} */
xyz.swapee.wc.ICryptoSelectDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.Settings filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectDisplay.Queries}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.Settings} */
xyz.swapee.wc.ICryptoSelectDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay.paintSmHeight
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearch.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay.paintSearch
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay.paintSelectedImg
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory} */
xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay.paintHoveringIndex
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory} */
xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay.paintBeforeHovering
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplay.xml} xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory filter:!ControllerPlugin~props 6153eace30c51b40ec89c52adf21b092 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe}
 */
$xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory} */
xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectDisplay.paintSearchInput
/* @typal-end */