/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.Initialese filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.Initialese} */
xyz.swapee.wc.ICryptoSelectComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputerFields filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectComputerFields = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectComputerFields}
 */
xyz.swapee.wc.ICryptoSelectComputerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputerCaster filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectComputer} */
$xyz.swapee.wc.ICryptoSelectComputerCaster.prototype.asICryptoSelectComputer
/** @type {!xyz.swapee.wc.BoundCryptoSelectComputer} */
$xyz.swapee.wc.ICryptoSelectComputerCaster.prototype.superCryptoSelectComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectComputerCaster}
 */
xyz.swapee.wc.ICryptoSelectComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectComputerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.ICryptoSelectComputer = function() {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptWideness = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptSelectedCrypto = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptSearchInput = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptPopupShown = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.prototype.adaptMatchedKeys = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.CryptoSelectMemory} mem
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectComputer.prototype.compute = function(mem, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectComputer}
 */
xyz.swapee.wc.ICryptoSelectComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.CryptoSelectComputer filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectComputer.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.CryptoSelectComputer
/** @type {function(new: xyz.swapee.wc.ICryptoSelectComputer, ...!xyz.swapee.wc.ICryptoSelectComputer.Initialese)} */
xyz.swapee.wc.CryptoSelectComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.CryptoSelectComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.AbstractCryptoSelectComputer filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectComputer}
 */
$xyz.swapee.wc.AbstractCryptoSelectComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectComputer)} */
xyz.swapee.wc.AbstractCryptoSelectComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectComputer}
 */
xyz.swapee.wc.AbstractCryptoSelectComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.CryptoSelectComputerConstructor filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectComputer, ...!xyz.swapee.wc.ICryptoSelectComputer.Initialese)} */
xyz.swapee.wc.CryptoSelectComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.RecordICryptoSelectComputer filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @typedef {{ adaptWideness: xyz.swapee.wc.ICryptoSelectComputer.adaptWideness, adaptSelectedCrypto: xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto, adaptSearchInput: xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput, adaptPopupShown: xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown, adaptMatchedKeys: xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys, compute: xyz.swapee.wc.ICryptoSelectComputer.compute }} */
xyz.swapee.wc.RecordICryptoSelectComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.BoundICryptoSelectComputer filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectComputerFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.CryptoSelectMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.BoundICryptoSelectComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectComputer} */
xyz.swapee.wc.BoundICryptoSelectComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.BoundCryptoSelectComputer filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectComputer} */
xyz.swapee.wc.BoundCryptoSelectComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptWideness filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptWideness = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectComputer}
 */
$xyz.swapee.wc.ICryptoSelectComputer._adaptWideness = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectComputer.__adaptWideness = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.adaptWideness} */
xyz.swapee.wc.ICryptoSelectComputer.adaptWideness
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer._adaptWideness} */
xyz.swapee.wc.ICryptoSelectComputer._adaptWideness
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.__adaptWideness} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptWideness

// nss:xyz.swapee.wc.ICryptoSelectComputer,$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectComputer}
 */
$xyz.swapee.wc.ICryptoSelectComputer._adaptSelectedCrypto = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectComputer.__adaptSelectedCrypto = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer._adaptSelectedCrypto} */
xyz.swapee.wc.ICryptoSelectComputer._adaptSelectedCrypto
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.__adaptSelectedCrypto} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptSelectedCrypto

// nss:xyz.swapee.wc.ICryptoSelectComputer,$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectComputer}
 */
$xyz.swapee.wc.ICryptoSelectComputer._adaptSearchInput = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectComputer.__adaptSearchInput = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer._adaptSearchInput} */
xyz.swapee.wc.ICryptoSelectComputer._adaptSearchInput
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.__adaptSearchInput} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptSearchInput

// nss:xyz.swapee.wc.ICryptoSelectComputer,$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectComputer}
 */
$xyz.swapee.wc.ICryptoSelectComputer._adaptPopupShown = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectComputer.__adaptPopupShown = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown} */
xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer._adaptPopupShown} */
xyz.swapee.wc.ICryptoSelectComputer._adaptPopupShown
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.__adaptPopupShown} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptPopupShown

// nss:xyz.swapee.wc.ICryptoSelectComputer,$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectComputer}
 */
$xyz.swapee.wc.ICryptoSelectComputer._adaptMatchedKeys = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} form
 * @param {xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} changes
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectComputer.__adaptMatchedKeys = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys} */
xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer._adaptMatchedKeys} */
xyz.swapee.wc.ICryptoSelectComputer._adaptMatchedKeys
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.__adaptMatchedKeys} */
xyz.swapee.wc.ICryptoSelectComputer.__adaptMatchedKeys

// nss:xyz.swapee.wc.ICryptoSelectComputer,$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.compute filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @param {xyz.swapee.wc.CryptoSelectMemory} mem
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectComputer.compute = function(mem, land) {}
/**
 * @param {xyz.swapee.wc.CryptoSelectMemory} mem
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.compute.Land} land
 * @return {void}
 * @this {xyz.swapee.wc.ICryptoSelectComputer}
 */
$xyz.swapee.wc.ICryptoSelectComputer._compute = function(mem, land) {}
/**
 * @template THIS
 * @param {xyz.swapee.wc.CryptoSelectMemory} mem
 * @param {!xyz.swapee.wc.ICryptoSelectComputer.compute.Land} land
 * @return {void}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectComputer.__compute = function(mem, land) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.compute} */
xyz.swapee.wc.ICryptoSelectComputer.compute
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer._compute} */
xyz.swapee.wc.ICryptoSelectComputer._compute
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectComputer.__compute} */
xyz.swapee.wc.ICryptoSelectComputer.__compute

// nss:xyz.swapee.wc.ICryptoSelectComputer,$xyz.swapee.wc.ICryptoSelectComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form} */
xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptWideness
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Wideness}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return} */
xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptWideness
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Search}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return} */
xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form} */
xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {com.webcircuits.ui.IPopupPort.Inputs.Shown}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return} */
xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form} */
xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys}
 */
$xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return} */
xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/02-ICryptoSelectComputer.xml} xyz.swapee.wc.ICryptoSelectComputer.compute.Land filter:!ControllerPlugin~props 392912af7d7261d9588f911e81e0d689 */
/** @record */
$xyz.swapee.wc.ICryptoSelectComputer.compute.Land = function() {}
/** @type {!Object} */
$xyz.swapee.wc.ICryptoSelectComputer.compute.Land.prototype.Popup
/** @typedef {$xyz.swapee.wc.ICryptoSelectComputer.compute.Land} */
xyz.swapee.wc.ICryptoSelectComputer.compute.Land

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectComputer.compute
/* @typal-end */