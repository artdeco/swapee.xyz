/** @const {?} */ $xyz.swapee.wc.front.ICryptoSelectTouchscreenAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese}
 */
$xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} */
xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ICryptoSelectTouchscreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/** @interface */
$xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR} */
$xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster.prototype.asICryptoSelectTouchscreenAR
/** @type {!xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR} */
$xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster.prototype.superCryptoSelectTouchscreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster}
 */
xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.ICryptoSelectTouchscreenAR filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreen}
 */
$xyz.swapee.wc.front.ICryptoSelectTouchscreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ICryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.ICryptoSelectTouchscreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.CryptoSelectTouchscreenAR filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.ICryptoSelectTouchscreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese>}
 */
$xyz.swapee.wc.front.CryptoSelectTouchscreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.CryptoSelectTouchscreenAR
/** @type {function(new: xyz.swapee.wc.front.ICryptoSelectTouchscreenAR, ...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese)} */
xyz.swapee.wc.front.CryptoSelectTouchscreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.CryptoSelectTouchscreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
$xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR)} */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR|typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR|typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR|typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.CryptoSelectTouchscreenARConstructor filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/** @typedef {function(new: xyz.swapee.wc.front.ICryptoSelectTouchscreenAR, ...!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR.Initialese)} */
xyz.swapee.wc.front.CryptoSelectTouchscreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.RecordICryptoSelectTouchscreenAR filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordICryptoSelectTouchscreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordICryptoSelectTouchscreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectTouchscreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundICryptoSelectTouchscreen}
 */
$xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR} */
xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/73-ICryptoSelectTouchscreenAR.xml} xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR filter:!ControllerPlugin~props 1ac073cc1d2be2eea6e001369e7bbfbe */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectTouchscreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR} */
xyz.swapee.wc.front.BoundCryptoSelectTouchscreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */