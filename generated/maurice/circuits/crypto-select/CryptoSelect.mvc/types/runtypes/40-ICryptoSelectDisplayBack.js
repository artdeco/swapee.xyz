/** @const {?} */ $xyz.swapee.wc.back.ICryptoSelectDisplay
/** @const {?} */ xyz.swapee.wc.back.ICryptoSelectDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.CryptoSelectClasses>}
 */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoMenu
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoDown
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedBl
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.NoCryptoDropItem
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedImWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.ChevronUp
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.ChevronDown
/** @type {(!Array<!com.webcircuits.IHtmlTwin>)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.MenuItems
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.CryptoSelectedNameIn
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.InnerSpan
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese.prototype.Popup
/** @typedef {$xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese} */
xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ICryptoSelectDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplayFields filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/** @interface */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoMenu
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoDown
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoSelectedBl
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.NoCryptoDropItem
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoSelectedImWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.ChevronUp
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.ChevronDown
/** @type {!Array<!com.webcircuits.IHtmlTwin>} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.MenuItems
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.CryptoSelectedNameIn
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.InnerSpan
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.ICryptoSelectDisplayFields.prototype.Popup
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectDisplayFields}
 */
xyz.swapee.wc.back.ICryptoSelectDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplayCaster filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/** @interface */
$xyz.swapee.wc.back.ICryptoSelectDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectDisplay} */
$xyz.swapee.wc.back.ICryptoSelectDisplayCaster.prototype.asICryptoSelectDisplay
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectDisplay} */
$xyz.swapee.wc.back.ICryptoSelectDisplayCaster.prototype.superCryptoSelectDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectDisplayCaster}
 */
xyz.swapee.wc.back.ICryptoSelectDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplay filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.CryptoSelectClasses, !xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.back.ICryptoSelectDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [memory]
 * @param {!xyz.swapee.wc.CryptoSelectLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ICryptoSelectDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectDisplay}
 */
xyz.swapee.wc.back.ICryptoSelectDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.CryptoSelectDisplay filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.ICryptoSelectDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese>}
 */
$xyz.swapee.wc.back.CryptoSelectDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.CryptoSelectDisplay
/** @type {function(new: xyz.swapee.wc.back.ICryptoSelectDisplay)} */
xyz.swapee.wc.back.CryptoSelectDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.CryptoSelectDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.AbstractCryptoSelectDisplay filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.CryptoSelectDisplay}
 */
$xyz.swapee.wc.back.AbstractCryptoSelectDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractCryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractCryptoSelectDisplay)} */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectDisplay}
 */
xyz.swapee.wc.back.AbstractCryptoSelectDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.RecordICryptoSelectDisplay filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/** @typedef {{ paint: xyz.swapee.wc.back.ICryptoSelectDisplay.paint }} */
xyz.swapee.wc.back.RecordICryptoSelectDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.BoundICryptoSelectDisplay filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.CryptoSelectClasses, !xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.back.BoundICryptoSelectDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundICryptoSelectDisplay} */
xyz.swapee.wc.back.BoundICryptoSelectDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.BoundCryptoSelectDisplay filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundCryptoSelectDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundCryptoSelectDisplay} */
xyz.swapee.wc.back.BoundCryptoSelectDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/40-ICryptoSelectDisplayBack.xml} xyz.swapee.wc.back.ICryptoSelectDisplay.paint filter:!ControllerPlugin~props 98f5ef62c8af196fc8504b2440c07768 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.CryptoSelectMemory} [memory]
 * @param {!xyz.swapee.wc.CryptoSelectLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ICryptoSelectDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.CryptoSelectMemory=, !xyz.swapee.wc.CryptoSelectLand=): void} */
xyz.swapee.wc.back.ICryptoSelectDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.ICryptoSelectDisplay, !xyz.swapee.wc.CryptoSelectMemory=, !xyz.swapee.wc.CryptoSelectLand=): void} */
xyz.swapee.wc.back.ICryptoSelectDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.ICryptoSelectDisplay.__paint} */
xyz.swapee.wc.back.ICryptoSelectDisplay.__paint

// nss:xyz.swapee.wc.back.ICryptoSelectDisplay,$xyz.swapee.wc.back.ICryptoSelectDisplay,xyz.swapee.wc.back
/* @typal-end */