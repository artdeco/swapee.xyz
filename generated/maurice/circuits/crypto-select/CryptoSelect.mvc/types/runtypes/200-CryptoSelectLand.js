/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/200-CryptoSelectLand.xml} xyz.swapee.wc.CryptoSelectLand filter:!ControllerPlugin~props f86d8189b911518a81a2be6babfa7a37 */
/** @record */
$xyz.swapee.wc.CryptoSelectLand = __$te_Mixin()
/** @type {!Object} */
$xyz.swapee.wc.CryptoSelectLand.prototype.Popup
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.CryptoSelectLand}
 */
xyz.swapee.wc.CryptoSelectLand

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */