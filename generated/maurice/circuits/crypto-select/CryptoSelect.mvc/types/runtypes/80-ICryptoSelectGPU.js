/** @const {?} */ $xyz.swapee.wc.ICryptoSelectGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectGPU.Initialese} */
xyz.swapee.wc.ICryptoSelectGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ICryptoSelectGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.ICryptoSelectGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectGPUFields}
 */
xyz.swapee.wc.ICryptoSelectGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ICryptoSelectGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectGPU} */
$xyz.swapee.wc.ICryptoSelectGPUCaster.prototype.asICryptoSelectGPU
/** @type {!xyz.swapee.wc.BoundCryptoSelectGPU} */
$xyz.swapee.wc.ICryptoSelectGPUCaster.prototype.superCryptoSelectGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectGPUCaster}
 */
xyz.swapee.wc.ICryptoSelectGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.ICryptoSelectGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!CryptoSelectMemory,.!CryptoSelectLand>}
 * @extends {xyz.swapee.wc.back.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectGPU}
 */
xyz.swapee.wc.ICryptoSelectGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.CryptoSelectGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectGPU.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.CryptoSelectGPU
/** @type {function(new: xyz.swapee.wc.ICryptoSelectGPU, ...!xyz.swapee.wc.ICryptoSelectGPU.Initialese)} */
xyz.swapee.wc.CryptoSelectGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.CryptoSelectGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.AbstractCryptoSelectGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectGPU}
 */
$xyz.swapee.wc.AbstractCryptoSelectGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectGPU)} */
xyz.swapee.wc.AbstractCryptoSelectGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectGPU|typeof xyz.swapee.wc.CryptoSelectGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ICryptoSelectDisplay|typeof xyz.swapee.wc.back.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectGPU}
 */
xyz.swapee.wc.AbstractCryptoSelectGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.CryptoSelectGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectGPU, ...!xyz.swapee.wc.ICryptoSelectGPU.Initialese)} */
xyz.swapee.wc.CryptoSelectGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.RecordICryptoSelectGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.BoundICryptoSelectGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectGPUFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!CryptoSelectMemory,.!CryptoSelectLand>}
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectDisplay}
 */
$xyz.swapee.wc.BoundICryptoSelectGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectGPU} */
xyz.swapee.wc.BoundICryptoSelectGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/80-ICryptoSelectGPU.xml} xyz.swapee.wc.BoundCryptoSelectGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectGPU} */
xyz.swapee.wc.BoundCryptoSelectGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */