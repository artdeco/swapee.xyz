/** @const {?} */ $xyz.swapee.wc.ICryptoSelectCore
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectCore.Model
/** @const {?} */ xyz.swapee.wc.ICryptoSelectCore
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Initialese filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @record */
$xyz.swapee.wc.ICryptoSelectCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Initialese} */
xyz.swapee.wc.ICryptoSelectCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCoreFields filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @interface */
$xyz.swapee.wc.ICryptoSelectCoreFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelectCore.Model} */
$xyz.swapee.wc.ICryptoSelectCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectCoreFields}
 */
xyz.swapee.wc.ICryptoSelectCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCoreCaster filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @interface */
$xyz.swapee.wc.ICryptoSelectCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectCore} */
$xyz.swapee.wc.ICryptoSelectCoreCaster.prototype.asICryptoSelectCore
/** @type {!xyz.swapee.wc.BoundCryptoSelectCore} */
$xyz.swapee.wc.ICryptoSelectCoreCaster.prototype.superCryptoSelectCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectCoreCaster}
 */
xyz.swapee.wc.ICryptoSelectCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCoreCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore}
 */
$xyz.swapee.wc.ICryptoSelectCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectCore.prototype.resetCryptoSelectCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectCore}
 */
xyz.swapee.wc.ICryptoSelectCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.CryptoSelectCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectCore.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.CryptoSelectCore
/** @type {function(new: xyz.swapee.wc.ICryptoSelectCore)} */
xyz.swapee.wc.CryptoSelectCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.CryptoSelectCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.AbstractCryptoSelectCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @constructor
 * @extends {xyz.swapee.wc.CryptoSelectCore}
 */
$xyz.swapee.wc.AbstractCryptoSelectCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectCore)} */
xyz.swapee.wc.AbstractCryptoSelectCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectCore|typeof xyz.swapee.wc.CryptoSelectCore)|(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectCore}
 */
xyz.swapee.wc.AbstractCryptoSelectCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.RecordICryptoSelectCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @typedef {{ resetCore: xyz.swapee.wc.ICryptoSelectCore.resetCore, resetCryptoSelectCore: xyz.swapee.wc.ICryptoSelectCore.resetCryptoSelectCore }} */
xyz.swapee.wc.RecordICryptoSelectCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.BoundICryptoSelectCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCoreFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCoreCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectOuterCore}
 */
$xyz.swapee.wc.BoundICryptoSelectCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectCore} */
xyz.swapee.wc.BoundICryptoSelectCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.BoundCryptoSelectCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectCore} */
xyz.swapee.wc.BoundCryptoSelectCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.resetCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectCore): void} */
xyz.swapee.wc.ICryptoSelectCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectCore.__resetCore} */
xyz.swapee.wc.ICryptoSelectCore.__resetCore

// nss:xyz.swapee.wc.ICryptoSelectCore,$xyz.swapee.wc.ICryptoSelectCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.resetCryptoSelectCore filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectCore.__resetCryptoSelectCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ICryptoSelectCore.resetCryptoSelectCore
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectCore): void} */
xyz.swapee.wc.ICryptoSelectCore._resetCryptoSelectCore
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectCore.__resetCryptoSelectCore} */
xyz.swapee.wc.ICryptoSelectCore.__resetCryptoSelectCore

// nss:xyz.swapee.wc.ICryptoSelectCore,$xyz.swapee.wc.ICryptoSelectCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Wideness.wideness filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness.wideness

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Wideness filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @record */
$xyz.swapee.wc.ICryptoSelectCore.Model.Wideness = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectCore.Model.Wideness.prototype.wideness
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Wideness} */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Wideness}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model} */
xyz.swapee.wc.ICryptoSelectCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/09-ICryptoSelectCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe filter:!ControllerPlugin~props 0d9a2c4f06802c92d6b7903685c0c00b */
/** @record */
$xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe.prototype.wideness
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.Wideness_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */