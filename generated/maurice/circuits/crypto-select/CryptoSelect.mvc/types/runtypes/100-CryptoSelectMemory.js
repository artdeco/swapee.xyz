/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/100-CryptoSelectMemory.xml} xyz.swapee.wc.CryptoSelectMemory filter:!ControllerPlugin~props ac88f349dc4b42767f75c1caa5ad6deb */
/** @record */
$xyz.swapee.wc.CryptoSelectMemory = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.selected
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.iconFolder
/** @type {boolean} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.fiat
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.imHtml
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.search
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.selectedIcon
/** @type {boolean} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.menuExpanded
/** @type {boolean} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.isMouseOver
/** @type {boolean} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.sm
/** @type {boolean} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.isSearching
/** @type {number} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.hoveringIndex
/** @type {Map<string, { icon: string, displayName: string, name: string }>} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.cryptos
/** @type {!Set<string>} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.ignoreCryptos
/** @type {!Array<string>} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.visibleItems
/** @type {string} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.keyboardSelected
/** @type {?{ icon: string, displayName: string, name: string }} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.selectedCrypto
/** @type {!Set<string>} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.matchedKeys
/** @type {string|undefined} */
$xyz.swapee.wc.CryptoSelectMemory.prototype.wideness
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.CryptoSelectMemory}
 */
xyz.swapee.wc.CryptoSelectMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */