/** @const {?} */ $xyz.swapee.wc.front.ICryptoSelectController
/** @const {?} */ xyz.swapee.wc.front.ICryptoSelectController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.Initialese filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @record */
$xyz.swapee.wc.front.ICryptoSelectController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ICryptoSelectController.Initialese} */
xyz.swapee.wc.front.ICryptoSelectController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ICryptoSelectController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectControllerCaster filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @interface */
$xyz.swapee.wc.front.ICryptoSelectControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundICryptoSelectController} */
$xyz.swapee.wc.front.ICryptoSelectControllerCaster.prototype.asICryptoSelectController
/** @type {!xyz.swapee.wc.front.BoundCryptoSelectController} */
$xyz.swapee.wc.front.ICryptoSelectControllerCaster.prototype.superCryptoSelectController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ICryptoSelectControllerCaster}
 */
xyz.swapee.wc.front.ICryptoSelectControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerAT}
 */
$xyz.swapee.wc.front.ICryptoSelectController = function() {}
/** @return {?} */
$xyz.swapee.wc.front.ICryptoSelectController.prototype.flipMenuExpanded = function() {}
/**
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ICryptoSelectController.prototype.setSelected = function(val) {}
/** @return {void} */
$xyz.swapee.wc.front.ICryptoSelectController.prototype.unsetSelected = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ICryptoSelectController}
 */
xyz.swapee.wc.front.ICryptoSelectController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.CryptoSelectController filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init
 * @implements {xyz.swapee.wc.front.ICryptoSelectController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ICryptoSelectController.Initialese>}
 */
$xyz.swapee.wc.front.CryptoSelectController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.CryptoSelectController
/** @type {function(new: xyz.swapee.wc.front.ICryptoSelectController, ...!xyz.swapee.wc.front.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.front.CryptoSelectController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.CryptoSelectController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.AbstractCryptoSelectController filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init
 * @extends {xyz.swapee.wc.front.CryptoSelectController}
 */
$xyz.swapee.wc.front.AbstractCryptoSelectController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ICryptoSelectController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractCryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController
/** @type {function(new: xyz.swapee.wc.front.AbstractCryptoSelectController)} */
xyz.swapee.wc.front.AbstractCryptoSelectController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractCryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.continues
/**
 * @param {...((!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.front.ICryptoSelectControllerAT|typeof xyz.swapee.wc.front.CryptoSelectControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.CryptoSelectController}
 */
xyz.swapee.wc.front.AbstractCryptoSelectController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.CryptoSelectControllerConstructor filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @typedef {function(new: xyz.swapee.wc.front.ICryptoSelectController, ...!xyz.swapee.wc.front.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.front.CryptoSelectControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.RecordICryptoSelectController filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/** @typedef {{ flipMenuExpanded: xyz.swapee.wc.front.ICryptoSelectController.flipMenuExpanded, setSelected: xyz.swapee.wc.front.ICryptoSelectController.setSelected, unsetSelected: xyz.swapee.wc.front.ICryptoSelectController.unsetSelected }} */
xyz.swapee.wc.front.RecordICryptoSelectController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.BoundICryptoSelectController filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordICryptoSelectController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectControllerAT}
 */
$xyz.swapee.wc.front.BoundICryptoSelectController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundICryptoSelectController} */
xyz.swapee.wc.front.BoundICryptoSelectController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.BoundCryptoSelectController filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundCryptoSelectController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundCryptoSelectController} */
xyz.swapee.wc.front.BoundCryptoSelectController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.flipMenuExpanded filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.ICryptoSelectController.__flipMenuExpanded = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ICryptoSelectController.flipMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.front.ICryptoSelectController)} */
xyz.swapee.wc.front.ICryptoSelectController._flipMenuExpanded
/** @typedef {typeof $xyz.swapee.wc.front.ICryptoSelectController.__flipMenuExpanded} */
xyz.swapee.wc.front.ICryptoSelectController.__flipMenuExpanded

// nss:xyz.swapee.wc.front.ICryptoSelectController,$xyz.swapee.wc.front.ICryptoSelectController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.setSelected filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} val
 * @return {void}
 */
$xyz.swapee.wc.front.ICryptoSelectController.__setSelected = function(val) {}
/** @typedef {function(string): void} */
xyz.swapee.wc.front.ICryptoSelectController.setSelected
/** @typedef {function(this: xyz.swapee.wc.front.ICryptoSelectController, string): void} */
xyz.swapee.wc.front.ICryptoSelectController._setSelected
/** @typedef {typeof $xyz.swapee.wc.front.ICryptoSelectController.__setSelected} */
xyz.swapee.wc.front.ICryptoSelectController.__setSelected

// nss:xyz.swapee.wc.front.ICryptoSelectController,$xyz.swapee.wc.front.ICryptoSelectController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/51-ICryptoSelectControllerFront.xml} xyz.swapee.wc.front.ICryptoSelectController.unsetSelected filter:!ControllerPlugin~props 07dd053406336f6c2cf335e6029d55bc */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.ICryptoSelectController.__unsetSelected = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.ICryptoSelectController.unsetSelected
/** @typedef {function(this: xyz.swapee.wc.front.ICryptoSelectController): void} */
xyz.swapee.wc.front.ICryptoSelectController._unsetSelected
/** @typedef {typeof $xyz.swapee.wc.front.ICryptoSelectController.__unsetSelected} */
xyz.swapee.wc.front.ICryptoSelectController.__unsetSelected

// nss:xyz.swapee.wc.front.ICryptoSelectController,$xyz.swapee.wc.front.ICryptoSelectController,xyz.swapee.wc.front
/* @typal-end */