/** @const {?} */ $xyz.swapee.wc.ICryptoSelect
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelectEnv filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @record */
$xyz.swapee.wc.CryptoSelectEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.ICryptoSelect} */
$xyz.swapee.wc.CryptoSelectEnv.prototype.cryptoSelect
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.CryptoSelectEnv}
 */
xyz.swapee.wc.CryptoSelectEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect.Initialese filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
$xyz.swapee.wc.ICryptoSelect.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelect.Initialese} */
xyz.swapee.wc.ICryptoSelect.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelect
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelectFields filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @interface */
$xyz.swapee.wc.ICryptoSelectFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelect.Pinout} */
$xyz.swapee.wc.ICryptoSelectFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectFields}
 */
xyz.swapee.wc.ICryptoSelectFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelectCaster filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @interface */
$xyz.swapee.wc.ICryptoSelectCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelect} */
$xyz.swapee.wc.ICryptoSelectCaster.prototype.asICryptoSelect
/** @type {!xyz.swapee.wc.BoundCryptoSelect} */
$xyz.swapee.wc.ICryptoSelectCaster.prototype.superCryptoSelect
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectCaster}
 */
xyz.swapee.wc.ICryptoSelectCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.ICryptoSelectComputer}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.ICryptoSelect = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelect}
 */
xyz.swapee.wc.ICryptoSelect

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelect filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelect}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelect.Initialese>}
 */
$xyz.swapee.wc.CryptoSelect = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.CryptoSelect
/** @type {function(new: xyz.swapee.wc.ICryptoSelect, ...!xyz.swapee.wc.ICryptoSelect.Initialese)} */
xyz.swapee.wc.CryptoSelect.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.CryptoSelect.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.AbstractCryptoSelect filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelect}
 */
$xyz.swapee.wc.AbstractCryptoSelect = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelect.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelect)} */
xyz.swapee.wc.AbstractCryptoSelect.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelect.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelect|typeof xyz.swapee.wc.CryptoSelect)|(!xyz.swapee.wc.ICryptoSelectProcessor|typeof xyz.swapee.wc.CryptoSelectProcessor)|(!xyz.swapee.wc.ICryptoSelectComputer|typeof xyz.swapee.wc.CryptoSelectComputer)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelect}
 */
xyz.swapee.wc.AbstractCryptoSelect.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelectConstructor filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelect, ...!xyz.swapee.wc.ICryptoSelect.Initialese)} */
xyz.swapee.wc.CryptoSelectConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect.MVCOptions filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @record */
$xyz.swapee.wc.ICryptoSelect.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.ICryptoSelect.Pinout)|undefined} */
$xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.ICryptoSelect.Pinout)|undefined} */
$xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.ICryptoSelect.Pinout} */
$xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.CryptoSelectMemory)|undefined} */
$xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.CryptoSelectClasses)|undefined} */
$xyz.swapee.wc.ICryptoSelect.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.ICryptoSelect.MVCOptions} */
xyz.swapee.wc.ICryptoSelect.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelect
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.RecordICryptoSelect filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelect

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.BoundICryptoSelect filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelect}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectProcessor}
 * @extends {xyz.swapee.wc.BoundICryptoSelectComputer}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.ICryptoSelectController.Inputs, !xyz.swapee.wc.CryptoSelectLand>}
 */
$xyz.swapee.wc.BoundICryptoSelect = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelect} */
xyz.swapee.wc.BoundICryptoSelect

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.BoundCryptoSelect filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelect}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelect = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelect} */
xyz.swapee.wc.BoundCryptoSelect

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelect.Pinout filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectController.Inputs}
 */
$xyz.swapee.wc.ICryptoSelect.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelect.Pinout} */
xyz.swapee.wc.ICryptoSelect.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelect
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.ICryptoSelectBuffer filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
$xyz.swapee.wc.ICryptoSelectBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectBuffer}
 */
xyz.swapee.wc.ICryptoSelectBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/11-ICryptoSelect.xml} xyz.swapee.wc.CryptoSelectBuffer filter:!ControllerPlugin~props f21629dd2233d0dca0f76d92d4a9e28f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectBuffer}
 */
$xyz.swapee.wc.CryptoSelectBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectBuffer}
 */
xyz.swapee.wc.CryptoSelectBuffer
/** @type {function(new: xyz.swapee.wc.ICryptoSelectBuffer)} */
xyz.swapee.wc.CryptoSelectBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */