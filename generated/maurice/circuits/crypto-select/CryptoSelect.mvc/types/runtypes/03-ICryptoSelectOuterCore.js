/** @const {?} */ $xyz.swapee.wc.ICryptoSelectPort
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectPort.Inputs
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectCore
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectCore.Model
/** @const {?} */ xyz.swapee.wc.ICryptoSelectPort
/** @const {?} */ xyz.swapee.wc.ICryptoSelectPort.Inputs
/** @const {?} */ xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/** @const {?} */ xyz.swapee.wc.ICryptoSelectCore
/** @const {?} */ xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Initialese filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Initialese} */
xyz.swapee.wc.ICryptoSelectOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCoreFields filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.ICryptoSelectOuterCore.Model} */
$xyz.swapee.wc.ICryptoSelectOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectOuterCoreFields}
 */
xyz.swapee.wc.ICryptoSelectOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCoreCaster filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectOuterCore} */
$xyz.swapee.wc.ICryptoSelectOuterCoreCaster.prototype.asICryptoSelectOuterCore
/** @type {!xyz.swapee.wc.BoundCryptoSelectOuterCore} */
$xyz.swapee.wc.ICryptoSelectOuterCoreCaster.prototype.superCryptoSelectOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectOuterCoreCaster}
 */
xyz.swapee.wc.ICryptoSelectOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreCaster}
 */
$xyz.swapee.wc.ICryptoSelectOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectOuterCore}
 */
xyz.swapee.wc.ICryptoSelectOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.CryptoSelectOuterCore filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectOuterCore.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.CryptoSelectOuterCore
/** @type {function(new: xyz.swapee.wc.ICryptoSelectOuterCore)} */
xyz.swapee.wc.CryptoSelectOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.CryptoSelectOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.AbstractCryptoSelectOuterCore filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.CryptoSelectOuterCore}
 */
$xyz.swapee.wc.AbstractCryptoSelectOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectOuterCore)} */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectOuterCore|typeof xyz.swapee.wc.CryptoSelectOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectOuterCore}
 */
xyz.swapee.wc.AbstractCryptoSelectOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.RecordICryptoSelectOuterCore filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordICryptoSelectOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.BoundICryptoSelectOuterCore filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCoreCaster}
 */
$xyz.swapee.wc.BoundICryptoSelectOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectOuterCore} */
xyz.swapee.wc.BoundICryptoSelectOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.BoundCryptoSelectOuterCore filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectOuterCore} */
xyz.swapee.wc.BoundCryptoSelectOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected.selected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected.selected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder.iconFolder filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder.iconFolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat.fiat filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat.fiat

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml.imHtml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml.imHtml

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search.search filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search.search

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon.selectedIcon filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon.selectedIcon

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded.menuExpanded filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded.menuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver.isMouseOver filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver.isMouseOver

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm.sm filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm.sm

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching.isSearching filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {boolean} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching.isSearching

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex.hoveringIndex filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {number} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex.hoveringIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos.cryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Map<string, { icon: string, displayName: string, name: string }>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos.cryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos.ignoreCryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Set<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos.ignoreCryptos

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems.visibleItems filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Array<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems.visibleItems

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected.keyboardSelected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {string} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected.keyboardSelected

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto.selectedCrypto filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {{ icon: string, displayName: string, name: string }} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto.selectedCrypto

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys.matchedKeys filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @typedef {!Set<string>} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys.matchedKeys

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected.prototype.selected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder.prototype.iconFolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat.prototype.fiat
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml.prototype.imHtml
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search.prototype.search
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon.prototype.selectedIcon
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded.prototype.menuExpanded
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver.prototype.isMouseOver
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm.prototype.sm
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching.prototype.isSearching
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex.prototype.hoveringIndex
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos = function() {}
/** @type {(Map<string, { icon: string, displayName: string, name: string }>)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos.prototype.cryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos = function() {}
/** @type {(!Set<string>)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos.prototype.ignoreCryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems = function() {}
/** @type {(!Array<string>)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems.prototype.visibleItems
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected.prototype.keyboardSelected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto = function() {}
/** @type {(?{ icon: string, displayName: string, name: string })|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto.prototype.selectedCrypto
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys = function() {}
/** @type {(!Set<string>)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys.prototype.matchedKeys
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys}
 */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected.prototype.selected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder.prototype.iconFolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat.prototype.fiat
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml.prototype.imHtml
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search.prototype.search
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon.prototype.selectedIcon
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded.prototype.menuExpanded
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver.prototype.isMouseOver
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm.prototype.sm
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching.prototype.isSearching
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex.prototype.hoveringIndex
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos.prototype.cryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos.prototype.ignoreCryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems.prototype.visibleItems
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected.prototype.keyboardSelected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto.prototype.selectedCrypto
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys.prototype.matchedKeys
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto}
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys}
 */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe.prototype.selected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe.prototype.iconFolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe.prototype.fiat
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe.prototype.imHtml
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe.prototype.search
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe.prototype.selectedIcon
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe.prototype.menuExpanded
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe.prototype.isMouseOver
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe.prototype.sm
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe.prototype.isSearching
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe.prototype.hoveringIndex
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe = function() {}
/** @type {Map<string, { icon: string, displayName: string, name: string }>} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe.prototype.cryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe.prototype.ignoreCryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe = function() {}
/** @type {!Array<string>} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe.prototype.visibleItems
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe.prototype.keyboardSelected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe = function() {}
/** @type {?{ icon: string, displayName: string, name: string }} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe.prototype.selectedCrypto
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe.prototype.matchedKeys
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe.prototype.selected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe.prototype.iconFolder
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe.prototype.fiat
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe.prototype.imHtml
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe.prototype.search
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe.prototype.selectedIcon
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe.prototype.menuExpanded
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe.prototype.isMouseOver
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe.prototype.sm
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe.prototype.isSearching
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe.prototype.hoveringIndex
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe.prototype.cryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe.prototype.ignoreCryptos
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe.prototype.visibleItems
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe.prototype.keyboardSelected
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe.prototype.selectedCrypto
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/** @record */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe.prototype.matchedKeys
/** @typedef {$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe} */
xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Selected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IconFolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Fiat_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.ImHtml_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Search filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Search = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Search} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Search

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Search_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Search_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Search_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Search_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedIcon_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MenuExpanded_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsMouseOver_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Sm_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IsSearching_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.HoveringIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.Cryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.VisibleItems_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.KeyboardSelected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.SelectedCrypto_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys_Safe} */
xyz.swapee.wc.ICryptoSelectPort.Inputs.MatchedKeys_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Selected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Selected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IconFolder_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IconFolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Fiat_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Fiat_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.ImHtml_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.ImHtml_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Search_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Search_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedIcon_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedIcon_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MenuExpanded_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MenuExpanded_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsMouseOver_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsMouseOver_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Sm_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Sm_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IsSearching_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IsSearching_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.HoveringIndex_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.HoveringIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.Cryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.Cryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.IgnoreCryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.IgnoreCryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.VisibleItems_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.VisibleItems_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.KeyboardSelected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.KeyboardSelected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.SelectedCrypto_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.SelectedCrypto_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.WeakModel.MatchedKeys_Safe}
 */
$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys_Safe} */
xyz.swapee.wc.ICryptoSelectPort.WeakInputs.MatchedKeys_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Selected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Selected = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Selected} */
xyz.swapee.wc.ICryptoSelectCore.Model.Selected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Selected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder} */
xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IconFolder_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.IconFolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Fiat filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Fiat = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Fiat} */
xyz.swapee.wc.ICryptoSelectCore.Model.Fiat

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Fiat_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Fiat_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Fiat_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Fiat_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.Fiat_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml} */
xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.ImHtml_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.ImHtml_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Search filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Search = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Search} */
xyz.swapee.wc.ICryptoSelectCore.Model.Search

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Search_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.Search_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon} */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedIcon_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedIcon_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded} */
xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MenuExpanded_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver} */
xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsMouseOver_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.IsMouseOver_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Sm filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Sm = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Sm} */
xyz.swapee.wc.ICryptoSelectCore.Model.Sm

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Sm_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.Sm_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching} */
xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IsSearching_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.IsSearching_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex} */
xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.HoveringIndex_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.HoveringIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos} */
xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.Cryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.Cryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos} */
xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.IgnoreCryptos_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.IgnoreCryptos_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems} */
xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.VisibleItems_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.VisibleItems_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected} */
xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.KeyboardSelected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.KeyboardSelected_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto} */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.SelectedCrypto_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.SelectedCrypto_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys} */
xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/03-ICryptoSelectOuterCore.xml} xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe filter:!ControllerPlugin~props 65d7ef3c8d8ef1d333b4208079ce1f60 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectOuterCore.Model.MatchedKeys_Safe}
 */
$xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe} */
xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectCore.Model
/* @typal-end */