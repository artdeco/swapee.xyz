/** @const {?} */ $xyz.swapee.wc.back.ICryptoSelectController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.ICryptoSelectController.Initialese filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
$xyz.swapee.wc.back.ICryptoSelectController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ICryptoSelectController.Initialese} */
xyz.swapee.wc.back.ICryptoSelectController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ICryptoSelectController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.ICryptoSelectControllerCaster filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/** @interface */
$xyz.swapee.wc.back.ICryptoSelectControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectController} */
$xyz.swapee.wc.back.ICryptoSelectControllerCaster.prototype.asICryptoSelectController
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectController} */
$xyz.swapee.wc.back.ICryptoSelectControllerCaster.prototype.superCryptoSelectController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectControllerCaster}
 */
xyz.swapee.wc.back.ICryptoSelectControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.ICryptoSelectController filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
$xyz.swapee.wc.back.ICryptoSelectController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectController}
 */
xyz.swapee.wc.back.ICryptoSelectController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.CryptoSelectController filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectController.Initialese>}
 */
$xyz.swapee.wc.back.CryptoSelectController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.CryptoSelectController
/** @type {function(new: xyz.swapee.wc.back.ICryptoSelectController, ...!xyz.swapee.wc.back.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.back.CryptoSelectController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.CryptoSelectController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.AbstractCryptoSelectController filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectController}
 */
$xyz.swapee.wc.back.AbstractCryptoSelectController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractCryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController
/** @type {function(new: xyz.swapee.wc.back.AbstractCryptoSelectController)} */
xyz.swapee.wc.back.AbstractCryptoSelectController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.continues
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectController|typeof xyz.swapee.wc.back.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectController}
 */
xyz.swapee.wc.back.AbstractCryptoSelectController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.CryptoSelectControllerConstructor filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectController, ...!xyz.swapee.wc.back.ICryptoSelectController.Initialese)} */
xyz.swapee.wc.back.CryptoSelectControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.RecordICryptoSelectController filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.BoundICryptoSelectController filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerCaster}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.ICryptoSelectController.Inputs>}
 */
$xyz.swapee.wc.back.BoundICryptoSelectController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundICryptoSelectController} */
xyz.swapee.wc.back.BoundICryptoSelectController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/52-ICryptoSelectControllerBack.xml} xyz.swapee.wc.back.BoundCryptoSelectController filter:!ControllerPlugin~props 4bccf7cfe18ed90f36857673dae54d2a */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundCryptoSelectController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundCryptoSelectController} */
xyz.swapee.wc.back.BoundCryptoSelectController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */