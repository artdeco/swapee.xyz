/** @const {?} */ $xyz.swapee.wc.ICryptoSelectTouchscreen
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems
/** @const {?} */ xyz.swapee.wc.ICryptoSelectTouchscreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.front.CryptoSelectInputs, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, !xyz.swapee.wc.ICryptoSelectDisplay.Queries, !xyz.swapee.wc.CryptoSelectClasses>}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplay.Initialese}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} */
xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreenFields filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectTouchscreenFields = function() {}
/** @type {HTMLElement} */
$xyz.swapee.wc.ICryptoSelectTouchscreenFields.prototype.keyboardItem
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectTouchscreenFields.prototype.isBlurringInput
/** @type {boolean} */
$xyz.swapee.wc.ICryptoSelectTouchscreenFields.prototype.isMenuExpanded
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectTouchscreenFields}
 */
xyz.swapee.wc.ICryptoSelectTouchscreenFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreenCaster filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectTouchscreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectTouchscreen} */
$xyz.swapee.wc.ICryptoSelectTouchscreenCaster.prototype.asICryptoSelectTouchscreen
/** @type {!xyz.swapee.wc.BoundCryptoSelectTouchscreen} */
$xyz.swapee.wc.ICryptoSelectTouchscreenCaster.prototype.superCryptoSelectTouchscreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectTouchscreenCaster}
 */
xyz.swapee.wc.ICryptoSelectTouchscreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.front.CryptoSelectInputs, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, !xyz.swapee.wc.ICryptoSelectDisplay.Queries, null, !xyz.swapee.wc.CryptoSelectClasses>}
 * @extends {xyz.swapee.wc.front.ICryptoSelectController}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLine}
 * @extends {xyz.swapee.wc.ICryptoSelectDisplay}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen = function() {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory} memory
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashSelectedInLocalStorage = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashIsMenuExpanded = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashKeyboardItemAfterMenuExpanded = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.prototype.stashVisibleItems = function(memory) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectTouchscreen}
 */
xyz.swapee.wc.ICryptoSelectTouchscreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.CryptoSelectTouchscreen filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.CryptoSelectTouchscreen
/** @type {function(new: xyz.swapee.wc.ICryptoSelectTouchscreen, ...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese)} */
xyz.swapee.wc.CryptoSelectTouchscreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.CryptoSelectTouchscreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.AbstractCryptoSelectTouchscreen filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.CryptoSelectTouchscreen}
 */
$xyz.swapee.wc.AbstractCryptoSelectTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectTouchscreen)} */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectTouchscreen|typeof xyz.swapee.wc.CryptoSelectTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectDisplay|typeof xyz.swapee.wc.CryptoSelectDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectTouchscreen}
 */
xyz.swapee.wc.AbstractCryptoSelectTouchscreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.CryptoSelectTouchscreenConstructor filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectTouchscreen, ...!xyz.swapee.wc.ICryptoSelectTouchscreen.Initialese)} */
xyz.swapee.wc.CryptoSelectTouchscreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.RecordICryptoSelectTouchscreen filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @typedef {{ stashSelectedInLocalStorage: xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage, stashIsMenuExpanded: xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded, stashKeyboardItemAfterMenuExpanded: xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded, stashVisibleItems: xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems }} */
xyz.swapee.wc.RecordICryptoSelectTouchscreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.BoundICryptoSelectTouchscreen filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenFields}
 * @extends {xyz.swapee.wc.RecordICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectTouchscreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.CryptoSelectMemory, !xyz.swapee.wc.front.CryptoSelectInputs, !HTMLDivElement, !xyz.swapee.wc.ICryptoSelectDisplay.Settings, !xyz.swapee.wc.ICryptoSelectDisplay.Queries, null, !xyz.swapee.wc.CryptoSelectClasses>}
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectController}
 * @extends {xyz.swapee.wc.BoundICryptoSelectInterruptLine}
 * @extends {xyz.swapee.wc.BoundICryptoSelectDisplay}
 */
$xyz.swapee.wc.BoundICryptoSelectTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectTouchscreen} */
xyz.swapee.wc.BoundICryptoSelectTouchscreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.BoundCryptoSelectTouchscreen filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectTouchscreen} */
xyz.swapee.wc.BoundCryptoSelectTouchscreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory} memory
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory} memory
 * @return {void}
 * @this {xyz.swapee.wc.ICryptoSelectTouchscreen}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen._stashSelectedInLocalStorage = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory} memory
 * @return {void}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashSelectedInLocalStorage = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen._stashSelectedInLocalStorage} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashSelectedInLocalStorage
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.__stashSelectedInLocalStorage} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashSelectedInLocalStorage

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectTouchscreen}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen._stashIsMenuExpanded = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashIsMenuExpanded = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen._stashIsMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashIsMenuExpanded
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.__stashIsMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashIsMenuExpanded

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectTouchscreen}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen._stashKeyboardItemAfterMenuExpanded = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashKeyboardItemAfterMenuExpanded = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen._stashKeyboardItemAfterMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashKeyboardItemAfterMenuExpanded
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.__stashKeyboardItemAfterMenuExpanded} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashKeyboardItemAfterMenuExpanded

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems = function(memory) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)}
 * @this {xyz.swapee.wc.ICryptoSelectTouchscreen}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen._stashVisibleItems = function(memory) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory} memory
 * @return {(undefined|xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.__stashVisibleItems = function(memory) {}
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen._stashVisibleItems} */
xyz.swapee.wc.ICryptoSelectTouchscreen._stashVisibleItems
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectTouchscreen.__stashVisibleItems} */
xyz.swapee.wc.ICryptoSelectTouchscreen.__stashVisibleItems

// nss:xyz.swapee.wc.ICryptoSelectTouchscreen,$xyz.swapee.wc.ICryptoSelectTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen.stashSelectedInLocalStorage
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @record */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return.prototype.isMenuExpanded
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen.stashIsMenuExpanded
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MenuExpanded_Safe}
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.Selected_Safe}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @record */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return.prototype.keyboardItem
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen.stashKeyboardItemAfterMenuExpanded
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectCore.Model.MatchedKeys_Safe}
 */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Memory

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/70-ICryptoSelectTouchscreen.xml} xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return filter:!ControllerPlugin~props 62ce1d2e2e34cbbff63e783f244fdca9 */
/** @record */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return = function() {}
/** @type {*} */
$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return.prototype.visibleItems
/** @typedef {$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return} */
xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectTouchscreen.stashVisibleItems
/* @typal-end */