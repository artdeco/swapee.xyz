/** @const {?} */ $xyz.swapee.wc.back.ICryptoSelectControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ICryptoSelectController.Initialese}
 */
$xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} */
xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ICryptoSelectControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.ICryptoSelectControllerARCaster filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/** @interface */
$xyz.swapee.wc.back.ICryptoSelectControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundICryptoSelectControllerAR} */
$xyz.swapee.wc.back.ICryptoSelectControllerARCaster.prototype.asICryptoSelectControllerAR
/** @type {!xyz.swapee.wc.back.BoundCryptoSelectControllerAR} */
$xyz.swapee.wc.back.ICryptoSelectControllerARCaster.prototype.superCryptoSelectControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectControllerARCaster}
 */
xyz.swapee.wc.back.ICryptoSelectControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.ICryptoSelectControllerAR filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ICryptoSelectController}
 */
$xyz.swapee.wc.back.ICryptoSelectControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ICryptoSelectControllerAR}
 */
xyz.swapee.wc.back.ICryptoSelectControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.CryptoSelectControllerAR filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.ICryptoSelectControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.CryptoSelectControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.CryptoSelectControllerAR
/** @type {function(new: xyz.swapee.wc.back.ICryptoSelectControllerAR, ...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese)} */
xyz.swapee.wc.back.CryptoSelectControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.CryptoSelectControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.AbstractCryptoSelectControllerAR filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
$xyz.swapee.wc.back.AbstractCryptoSelectControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractCryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractCryptoSelectControllerAR)} */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectControllerAR|typeof xyz.swapee.wc.back.CryptoSelectControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractCryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectControllerAR|typeof xyz.swapee.wc.back.CryptoSelectControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.ICryptoSelectControllerAR|typeof xyz.swapee.wc.back.CryptoSelectControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ICryptoSelectController|typeof xyz.swapee.wc.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.back.CryptoSelectControllerAR}
 */
xyz.swapee.wc.back.AbstractCryptoSelectControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.CryptoSelectControllerARConstructor filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/** @typedef {function(new: xyz.swapee.wc.back.ICryptoSelectControllerAR, ...!xyz.swapee.wc.back.ICryptoSelectControllerAR.Initialese)} */
xyz.swapee.wc.back.CryptoSelectControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.RecordICryptoSelectControllerAR filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordICryptoSelectControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.BoundICryptoSelectControllerAR filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordICryptoSelectControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ICryptoSelectControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundICryptoSelectController}
 */
$xyz.swapee.wc.back.BoundICryptoSelectControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundICryptoSelectControllerAR} */
xyz.swapee.wc.back.BoundICryptoSelectControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/CryptoSelect.mvc/design/53-ICryptoSelectControllerAR.xml} xyz.swapee.wc.back.BoundCryptoSelectControllerAR filter:!ControllerPlugin~props 99aa3f60a2178ab8d537b2a1df850e86 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundICryptoSelectControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundCryptoSelectControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundCryptoSelectControllerAR} */
xyz.swapee.wc.back.BoundCryptoSelectControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */