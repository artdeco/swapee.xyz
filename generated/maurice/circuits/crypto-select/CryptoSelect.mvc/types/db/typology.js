/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ICryptoSelectComputer': {
  'id': 35453507421,
  'symbols': {},
  'methods': {
   'adaptWideness': 1,
   'adaptSelectedCrypto': 2,
   'adaptSearchInput': 3,
   'adaptMatchedKeys': 4,
   'compute': 7,
   'adaptPopupShown': 8
  }
 },
 'xyz.swapee.wc.CryptoSelectMemoryPQs': {
  'id': 35453507422,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectOuterCore': {
  'id': 35453507423,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.CryptoSelectInputsPQs': {
  'id': 35453507424,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectPort': {
  'id': 35453507425,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetCryptoSelectPort': 2
  }
 },
 'xyz.swapee.wc.CryptoSelectCachePQs': {
  'id': 35453507426,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectCore': {
  'id': 35453507427,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetCryptoSelectCore': 2
  }
 },
 'xyz.swapee.wc.ICryptoSelectProcessor': {
  'id': 35453507428,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelect': {
  'id': 35453507429,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectBuffer': {
  'id': 354535074210,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectHtmlComponentUtil': {
  'id': 354535074211,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectHtmlComponent': {
  'id': 354535074212,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectElement': {
  'id': 354535074213,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildPopup': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ICryptoSelectElementPort': {
  'id': 354535074214,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectDesigner': {
  'id': 354535074215,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ICryptoSelectGPU': {
  'id': 354535074216,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectDisplay': {
  'id': 354535074217,
  'symbols': {},
  'methods': {
   'resolveMouseItem': 1,
   'resolveItem': 2,
   'resolveItemByKey': 3,
   'resolveItemIndex': 4,
   'scrollItemIntoView': 5,
   'paint': 6,
   'paintSmHeight': 7,
   'paintSearch': 8,
   'paintSelectedImg': 9,
   'paintHoveringIndex': 10,
   'paintBeforeHovering': 11,
   'paintSearchInput': 12
  }
 },
 'xyz.swapee.wc.CryptoSelectVdusPQs': {
  'id': 354535074218,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectDisplay': {
  'id': 354535074219,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.CryptoSelectClassesPQs': {
  'id': 354535074220,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectController': {
  'id': 354535074221,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'flipMenuExpanded': 2,
   'calibrateDataSource': 3,
   'calibrateResetMouseOver': 4,
   'setSelected': 5,
   'unsetSelected': 6
  }
 },
 'xyz.swapee.wc.front.ICryptoSelectController': {
  'id': 354535074222,
  'symbols': {},
  'methods': {
   'flipMenuExpanded': 1,
   'setSelected': 2,
   'unsetSelected': 3
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectController': {
  'id': 354535074223,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ICryptoSelectControllerAR': {
  'id': 354535074224,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ICryptoSelectControllerAT': {
  'id': 354535074225,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectTouchscreen': {
  'id': 354535074226,
  'symbols': {},
  'methods': {
   'stashSelectedInLocalStorage': 1,
   'stashIsMenuExpanded': 2,
   'stashKeyboardItemAfterMenuExpanded': 3,
   'stashVisibleItems': 4
  }
 },
 'xyz.swapee.wc.back.ICryptoSelectTouchscreen': {
  'id': 354535074227,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ICryptoSelectTouchscreenAR': {
  'id': 354535074228,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ICryptoSelectTouchscreenAT': {
  'id': 354535074229,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectPortInterface': {
  'id': 354535074230,
  'symbols': {},
  'methods': {}
 }
})