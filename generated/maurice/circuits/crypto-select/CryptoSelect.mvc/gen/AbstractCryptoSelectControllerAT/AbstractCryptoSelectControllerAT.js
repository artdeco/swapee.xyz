import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectControllerAT}
 */
function __AbstractCryptoSelectControllerAT() {}
__AbstractCryptoSelectControllerAT.prototype = /** @type {!_AbstractCryptoSelectControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractCryptoSelectControllerAT}
 */
class _AbstractCryptoSelectControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ICryptoSelectControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractCryptoSelectControllerAT} ‎
 */
class AbstractCryptoSelectControllerAT extends newAbstract(
 _AbstractCryptoSelectControllerAT,354535074225,null,{
  asICryptoSelectControllerAT:1,
  superCryptoSelectControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractCryptoSelectControllerAT} */
AbstractCryptoSelectControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractCryptoSelectControllerAT} */
function AbstractCryptoSelectControllerATClass(){}

export default AbstractCryptoSelectControllerAT


AbstractCryptoSelectControllerAT[$implementations]=[
 __AbstractCryptoSelectControllerAT,
 UartUniversal,
 AbstractCryptoSelectControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ICryptoSelectControllerAT}*/({
  get asICryptoSelectController(){
   return this
  },
  flipMenuExpanded(){
   this.uart.t("inv",{mid:'f4b8d'})
  },
  setSelected(){
   this.uart.t("inv",{mid:'b5e89',args:[...arguments]})
  },
  unsetSelected(){
   this.uart.t("inv",{mid:'31ff5'})
  },
 }),
]