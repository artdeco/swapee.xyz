import AbstractCryptoSelectDisplay from '../AbstractCryptoSelectDisplayBack'
import CryptoSelectClassesPQs from '../../pqs/CryptoSelectClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {CryptoSelectClassesQPs} from '../../pqs/CryptoSelectClassesQPs'
import {CryptoSelectVdusPQs} from '../../pqs/CryptoSelectVdusPQs'
import {CryptoSelectVdusQPs} from '../../pqs/CryptoSelectVdusQPs'
import {CryptoSelectMemoryPQs} from '../../pqs/CryptoSelectMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectGPU}
 */
function __AbstractCryptoSelectGPU() {}
__AbstractCryptoSelectGPU.prototype = /** @type {!_AbstractCryptoSelectGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectGPU}
 */
class _AbstractCryptoSelectGPU { }
/**
 * Handles the periphery of the _ICryptoSelectDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectGPU} ‎
 */
class AbstractCryptoSelectGPU extends newAbstract(
 _AbstractCryptoSelectGPU,354535074216,null,{
  asICryptoSelectGPU:1,
  superCryptoSelectGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectGPU} */
AbstractCryptoSelectGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectGPU} */
function AbstractCryptoSelectGPUClass(){}

export default AbstractCryptoSelectGPU


AbstractCryptoSelectGPU[$implementations]=[
 __AbstractCryptoSelectGPU,
 AbstractCryptoSelectGPUClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectGPU}*/({
  classesQPs:CryptoSelectClassesQPs,
  vdusPQs:CryptoSelectVdusPQs,
  vdusQPs:CryptoSelectVdusQPs,
  memoryPQs:CryptoSelectMemoryPQs,
 }),
 AbstractCryptoSelectDisplay,
 BrowserView,
 AbstractCryptoSelectGPUClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectGPU}*/({
  allocator(){
   pressFit(this.classes,'',CryptoSelectClassesPQs)
  },
 }),
]