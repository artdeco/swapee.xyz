import {mountPins} from '@type.engineering/seers'
import {CryptoSelectMemoryPQs} from '../../pqs/CryptoSelectMemoryPQs'
import {CryptoSelectCachePQs} from '../../pqs/CryptoSelectCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_CryptoSelectCore}
 */
function __CryptoSelectCore() {}
__CryptoSelectCore.prototype = /** @type {!_CryptoSelectCore} */ ({ })
/** @this {xyz.swapee.wc.CryptoSelectCore} */ function CryptoSelectCoreConstructor() {
  /**@type {!xyz.swapee.wc.ICryptoSelectCore.Model}*/
  this.model={
    wideness: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectCore}
 */
class _CryptoSelectCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectCore} ‎
 */
class CryptoSelectCore extends newAbstract(
 _CryptoSelectCore,35453507427,CryptoSelectCoreConstructor,{
  asICryptoSelectCore:1,
  superCryptoSelectCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectCore} */
CryptoSelectCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectCore} */
function CryptoSelectCoreClass(){}

export default CryptoSelectCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_CryptoSelectOuterCore}
 */
function __CryptoSelectOuterCore() {}
__CryptoSelectOuterCore.prototype = /** @type {!_CryptoSelectOuterCore} */ ({ })
/** @this {xyz.swapee.wc.CryptoSelectOuterCore} */
export function CryptoSelectOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ICryptoSelectOuterCore.Model}*/
  this.model={
    selected: '',
    iconFolder: 'icons',
    fiat: false,
    imHtml: '',
    search: '',
    selectedIcon: '',
    menuExpanded: false,
    isMouseOver: false,
    sm: false,
    isSearching: false,
    hoveringIndex: -1,
    cryptos: null,
    ignoreCryptos: new Set,
    visibleItems: [],
    keyboardSelected: '',
    selectedCrypto: null,
    matchedKeys: new Set,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectOuterCore}
 */
class _CryptoSelectOuterCore { }
/**
 * The _ICryptoSelect_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectOuterCore} ‎
 */
export class CryptoSelectOuterCore extends newAbstract(
 _CryptoSelectOuterCore,35453507423,CryptoSelectOuterCoreConstructor,{
  asICryptoSelectOuterCore:1,
  superCryptoSelectOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectOuterCore} */
CryptoSelectOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectOuterCore} */
function CryptoSelectOuterCoreClass(){}


CryptoSelectOuterCore[$implementations]=[
 __CryptoSelectOuterCore,
 CryptoSelectOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectOuterCore}*/({
  constructor(){
   mountPins(this.model,CryptoSelectMemoryPQs)
   mountPins(this.model,CryptoSelectCachePQs)
  },
 }),
]

CryptoSelectCore[$implementations]=[
 CryptoSelectCoreClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectCore}*/({
  resetCore(){
   this.resetCryptoSelectCore()
  },
  resetCryptoSelectCore(){
   CryptoSelectCoreConstructor.call(
    /**@type {xyz.swapee.wc.CryptoSelectCore}*/(this),
   )
   CryptoSelectOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.CryptoSelectOuterCore}*/(
     /**@type {!xyz.swapee.wc.ICryptoSelectOuterCore}*/(this)),
   )
  },
 }),
 __CryptoSelectCore,
 CryptoSelectOuterCore,
]

export {CryptoSelectCore}