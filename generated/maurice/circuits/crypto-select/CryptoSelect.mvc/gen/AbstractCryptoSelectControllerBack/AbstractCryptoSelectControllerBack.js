import AbstractCryptoSelectControllerAR from '../AbstractCryptoSelectControllerAR'
import {AbstractCryptoSelectController} from '../AbstractCryptoSelectController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectControllerBack}
 */
function __AbstractCryptoSelectControllerBack() {}
__AbstractCryptoSelectControllerBack.prototype = /** @type {!_AbstractCryptoSelectControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectController}
 */
class _AbstractCryptoSelectControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectController} ‎
 */
class AbstractCryptoSelectControllerBack extends newAbstract(
 _AbstractCryptoSelectControllerBack,354535074223,null,{
  asICryptoSelectController:1,
  superCryptoSelectController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectController} */
AbstractCryptoSelectControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectController} */
function AbstractCryptoSelectControllerBackClass(){}

export default AbstractCryptoSelectControllerBack


AbstractCryptoSelectControllerBack[$implementations]=[
 __AbstractCryptoSelectControllerBack,
 AbstractCryptoSelectController,
 AbstractCryptoSelectControllerAR,
 DriverBack,
]