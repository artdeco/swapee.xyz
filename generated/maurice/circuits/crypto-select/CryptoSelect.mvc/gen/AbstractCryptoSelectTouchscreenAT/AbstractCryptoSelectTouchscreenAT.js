import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectTouchscreenAT}
 */
function __AbstractCryptoSelectTouchscreenAT() {}
__AbstractCryptoSelectTouchscreenAT.prototype = /** @type {!_AbstractCryptoSelectTouchscreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT}
 */
class _AbstractCryptoSelectTouchscreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ICryptoSelectTouchscreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT} ‎
 */
class AbstractCryptoSelectTouchscreenAT extends newAbstract(
 _AbstractCryptoSelectTouchscreenAT,354535074229,null,{
  asICryptoSelectTouchscreenAT:1,
  superCryptoSelectTouchscreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT} */
AbstractCryptoSelectTouchscreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreenAT} */
function AbstractCryptoSelectTouchscreenATClass(){}

export default AbstractCryptoSelectTouchscreenAT


AbstractCryptoSelectTouchscreenAT[$implementations]=[
 __AbstractCryptoSelectTouchscreenAT,
 UartUniversal,
]