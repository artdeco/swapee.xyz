import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectProcessor}
 */
function __AbstractCryptoSelectProcessor() {}
__AbstractCryptoSelectProcessor.prototype = /** @type {!_AbstractCryptoSelectProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectProcessor}
 */
class _AbstractCryptoSelectProcessor { }
/**
 * The processor to compute changes to the memory for the _ICryptoSelect_.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectProcessor} ‎
 */
class AbstractCryptoSelectProcessor extends newAbstract(
 _AbstractCryptoSelectProcessor,35453507428,null,{
  asICryptoSelectProcessor:1,
  superCryptoSelectProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectProcessor} */
AbstractCryptoSelectProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectProcessor} */
function AbstractCryptoSelectProcessorClass(){}

export default AbstractCryptoSelectProcessor


AbstractCryptoSelectProcessor[$implementations]=[
 __AbstractCryptoSelectProcessor,
]