import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_CryptoSelectElementPort}
 */
function __CryptoSelectElementPort() {}
__CryptoSelectElementPort.prototype = /** @type {!_CryptoSelectElementPort} */ ({ })
/** @this {xyz.swapee.wc.CryptoSelectElementPort} */ function CryptoSelectElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ICryptoSelectElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    cryptoMenuOpts: {},
    cryptoDownOpts: {},
    cryptoSelectedBlOpts: {},
    noCryptoDropItemOpts: {},
    cryptoSelectedImWrOpts: {},
    chevronUpOpts: {},
    chevronDownOpts: {},
    menuItemOpts: {},
    cryptoSelectedNameInOpts: {},
    innerSpanOpts: {},
    popupOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectElementPort}
 */
class _CryptoSelectElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectElementPort} ‎
 */
class CryptoSelectElementPort extends newAbstract(
 _CryptoSelectElementPort,354535074214,CryptoSelectElementPortConstructor,{
  asICryptoSelectElementPort:1,
  superCryptoSelectElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectElementPort} */
CryptoSelectElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectElementPort} */
function CryptoSelectElementPortClass(){}

export default CryptoSelectElementPort


CryptoSelectElementPort[$implementations]=[
 __CryptoSelectElementPort,
 CryptoSelectElementPortClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'crypto-menu-opts':undefined,
    'crypto-down-opts':undefined,
    'crypto-selected-bl-opts':undefined,
    'no-crypto-drop-item-opts':undefined,
    'crypto-selected-im-wr-opts':undefined,
    'chevron-up-opts':undefined,
    'chevron-down-opts':undefined,
    'menu-item-opts':undefined,
    'crypto-selected-name-in-opts':undefined,
    'inner-span-opts':undefined,
    'popup-opts':undefined,
    'icon-folder':undefined,
    'im-html':undefined,
    'selected-icon':undefined,
    'menu-expanded':undefined,
    'is-mouse-over':undefined,
    'is-searching':undefined,
    'hovering-index':undefined,
    'ignore-cryptos':undefined,
    'visible-items':undefined,
    'keyboard-selected':undefined,
    'selected-crypto':undefined,
    'matched-keys':undefined,
   })
  },
 }),
]