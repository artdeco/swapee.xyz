import AbstractCryptoSelectGPU from '../AbstractCryptoSelectGPU'
import AbstractCryptoSelectTouchscreenBack from '../AbstractCryptoSelectTouchscreenBack'
import {HtmlComponent,Landed,makeRevealConcealPaints,mvc} from '@webcircuits/webcircuits'
import {CryptoSelectInputsQPs} from '../../pqs/CryptoSelectInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractCryptoSelect from '../AbstractCryptoSelect'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectHtmlComponent}
 */
function __AbstractCryptoSelectHtmlComponent() {}
__AbstractCryptoSelectHtmlComponent.prototype = /** @type {!_AbstractCryptoSelectHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectHtmlComponent}
 */
class _AbstractCryptoSelectHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.CryptoSelectHtmlComponent} */ (res)
  }
}
/**
 * The _ICryptoSelect_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectHtmlComponent} ‎
 */
export class AbstractCryptoSelectHtmlComponent extends newAbstract(
 _AbstractCryptoSelectHtmlComponent,354535074212,null,{
  asICryptoSelectHtmlComponent:1,
  superCryptoSelectHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectHtmlComponent} */
AbstractCryptoSelectHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectHtmlComponent} */
function AbstractCryptoSelectHtmlComponentClass(){}


AbstractCryptoSelectHtmlComponent[$implementations]=[
 __AbstractCryptoSelectHtmlComponent,
 HtmlComponent,
 AbstractCryptoSelect,
 AbstractCryptoSelectGPU,
 AbstractCryptoSelectTouchscreenBack,
 Landed,
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  constructor(){
   this.land={
    Popup:null,
   }
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  deduceProps(el){
   const{
    asIBrowserView:{weHaveClass:weHaveClass},
    classes:{
     Sm:Sm,
     SmWide:SmWide,
     SmWider:SmWider,
     Expanded:Expanded,
    },
   }=this

   const sm=weHaveClass(Sm)
   const menuExpanded=weHaveClass(Expanded)
   return{
    sm:sm,
    menuExpanded:menuExpanded,
   }
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  inputsQPs:CryptoSelectInputsQPs,
 }),

/** @type {!AbstractCryptoSelectHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/function paintCryptoSelectedImWr() {
   this.CryptoSelectedImWr.setImg(this.model.selectedIcon)
  }
 ] }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_MouseOver_on_CryptoSelectedBl({isMouseOver:isMouseOver}){
   const{
    asICryptoSelectGPU:{
     CryptoSelectedBl:CryptoSelectedBl,
    },
    classes:{MouseOver:MouseOver},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(isMouseOver) addClass(CryptoSelectedBl,MouseOver)
   else removeClass(CryptoSelectedBl,MouseOver)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_Fiat_on_CryptoSelectedBl({fiat:fiat}){
   const{
    asICryptoSelectGPU:{
     CryptoSelectedBl:CryptoSelectedBl,
    },
    classes:{Fiat:Fiat},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(fiat) addClass(CryptoSelectedBl,Fiat)
   else removeClass(CryptoSelectedBl,Fiat)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_Expanded_on_CryptoSelectedBl({menuExpanded:menuExpanded}){
   const{
    asICryptoSelectGPU:{
     CryptoSelectedBl:CryptoSelectedBl,
    },
    classes:{Expanded:Expanded},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(menuExpanded) addClass(CryptoSelectedBl,Expanded)
   else removeClass(CryptoSelectedBl,Expanded)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_HoveringOverItem_on_CryptoMenu({hoveringIndex:hoveringIndex}){
   const{
    asICryptoSelectGPU:{
     CryptoMenu:CryptoMenu,
    },
    classes:{HoveringOverItem:HoveringOverItem},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(hoveringIndex!=-1) addClass(CryptoMenu,HoveringOverItem)
   else removeClass(CryptoMenu,HoveringOverItem)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function $conceal_NoCryptoDropItem({matchedKeys:matchedKeys}){
   const{
    asICryptoSelectGPU:{NoCryptoDropItem:NoCryptoDropItem},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(NoCryptoDropItem,matchedKeys.size)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_Sm_on_element({sm:sm}){
   const{
    asICryptoSelectGPU:{
     element:element,
    },
    classes:{Sm:Sm},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(sm) addClass(element,Sm)
   else removeClass(element,Sm)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_SmWide_on_element({sm:sm,wideness:wideness}){
   const{
    asICryptoSelectGPU:{
     element:element,
    },
    classes:{SmWide:SmWide},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(sm&&(wideness=='wide')) addClass(element,SmWide)
   else removeClass(element,SmWide)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_SmWider_on_element({sm:sm,wideness:wideness}){
   const{
    asICryptoSelectGPU:{
     element:element,
    },
    classes:{SmWider:SmWider},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(sm&&(wideness=='wider')) addClass(element,SmWider)
   else removeClass(element,SmWider)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:function paint_Expanded_on_element({menuExpanded:menuExpanded}){
   const{
    asICryptoSelectGPU:{
     element:element,
    },
    classes:{Expanded:Expanded},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(menuExpanded) addClass(element,Expanded)
   else removeClass(element,Expanded)
  },
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  paint:makeRevealConcealPaints({
   ChevronUp:{menuExpanded:1},
   ChevronDown:{menuExpanded:0},
  }),
 }),
 AbstractCryptoSelectHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{build:build},
    asICryptoSelectGPU:{
     Popup:Popup,
    },
   }=this
   build(2166517291,{Popup:Popup},{
    menuExpanded:'3fdec', // -> shown
   })
  },
 }),
]