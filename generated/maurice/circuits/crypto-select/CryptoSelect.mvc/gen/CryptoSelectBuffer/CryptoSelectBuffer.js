import {makeBuffers} from '@webcircuits/webcircuits'

export const CryptoSelectBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  selected:String,
  iconFolder:String,
  fiat:Boolean,
  imHtml:String,
  search:String,
  selectedIcon:String,
  menuExpanded:Boolean,
  isMouseOver:Boolean,
  sm:Boolean,
  isSearching:Boolean,
  hoveringIndex:Number,
  cryptos:Map,
  ignoreCryptos:Set,
  visibleItems:[String],
  keyboardSelected:String,
  selectedCrypto:5,
  matchedKeys:Set,
 }),
})

export default CryptoSelectBuffer