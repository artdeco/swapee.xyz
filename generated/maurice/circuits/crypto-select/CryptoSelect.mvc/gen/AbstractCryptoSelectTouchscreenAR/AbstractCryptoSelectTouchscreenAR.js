import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectTouchscreenAR}
 */
function __AbstractCryptoSelectTouchscreenAR() {}
__AbstractCryptoSelectTouchscreenAR.prototype = /** @type {!_AbstractCryptoSelectTouchscreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR}
 */
class _AbstractCryptoSelectTouchscreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ICryptoSelectTouchscreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR} ‎
 */
class AbstractCryptoSelectTouchscreenAR extends newAbstract(
 _AbstractCryptoSelectTouchscreenAR,354535074228,null,{
  asICryptoSelectTouchscreenAR:1,
  superCryptoSelectTouchscreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR} */
AbstractCryptoSelectTouchscreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractCryptoSelectTouchscreenAR} */
function AbstractCryptoSelectTouchscreenARClass(){}

export default AbstractCryptoSelectTouchscreenAR


AbstractCryptoSelectTouchscreenAR[$implementations]=[
 __AbstractCryptoSelectTouchscreenAR,
 AR,
 AbstractCryptoSelectTouchscreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ICryptoSelectTouchscreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractCryptoSelectTouchscreenAR}