import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {CryptoSelectInputsPQs} from '../../pqs/CryptoSelectInputsPQs'
import {CryptoSelectOuterCoreConstructor} from '../CryptoSelectCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_CryptoSelectPort}
 */
function __CryptoSelectPort() {}
__CryptoSelectPort.prototype = /** @type {!_CryptoSelectPort} */ ({ })
/** @this {xyz.swapee.wc.CryptoSelectPort} */ function CryptoSelectPortConstructor() {
  /**@type {!xyz.swapee.wc.ICryptoSelectPort.Inputs}*/
  this.inputs={
    source: 'onramper-cryptos',
  }
  const self=/** @type {!xyz.swapee.wc.CryptoSelectOuterCore} */ ({model:null})
  CryptoSelectOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectPort}
 */
class _CryptoSelectPort { }
/**
 * The port that serves as an interface to the _ICryptoSelect_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectPort} ‎
 */
export class CryptoSelectPort extends newAbstract(
 _CryptoSelectPort,35453507425,CryptoSelectPortConstructor,{
  asICryptoSelectPort:1,
  superCryptoSelectPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectPort} */
CryptoSelectPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectPort} */
function CryptoSelectPortClass(){}

export const CryptoSelectPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ICryptoSelect.Pinout>}*/({
 get Port() { return CryptoSelectPort },
})

CryptoSelectPort[$implementations]=[
 CryptoSelectPortClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectPort}*/({
  resetPort(){
   this.resetCryptoSelectPort()
  },
  resetCryptoSelectPort(){
   CryptoSelectPortConstructor.call(this)
  },
 }),
 __CryptoSelectPort,
 Parametric,
 CryptoSelectPortClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectPort}*/({
  constructor(){
   mountPins(this.inputs,CryptoSelectInputsPQs)
  },
 }),
]


export default CryptoSelectPort