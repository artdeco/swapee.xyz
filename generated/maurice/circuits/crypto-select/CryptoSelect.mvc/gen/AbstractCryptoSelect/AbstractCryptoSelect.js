import AbstractCryptoSelectProcessor from '../AbstractCryptoSelectProcessor'
import {CryptoSelectCore} from '../CryptoSelectCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractCryptoSelectComputer} from '../AbstractCryptoSelectComputer'
import {AbstractCryptoSelectController} from '../AbstractCryptoSelectController'
import {regulateCryptoSelectCache} from './methods/regulateCryptoSelectCache'
import {CryptoSelectCacheQPs} from '../../pqs/CryptoSelectCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelect}
 */
function __AbstractCryptoSelect() {}
__AbstractCryptoSelect.prototype = /** @type {!_AbstractCryptoSelect} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelect}
 */
class _AbstractCryptoSelect { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractCryptoSelect} ‎
 */
class AbstractCryptoSelect extends newAbstract(
 _AbstractCryptoSelect,35453507429,null,{
  asICryptoSelect:1,
  superCryptoSelect:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelect} */
AbstractCryptoSelect.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelect} */
function AbstractCryptoSelectClass(){}

export default AbstractCryptoSelect


AbstractCryptoSelect[$implementations]=[
 __AbstractCryptoSelect,
 CryptoSelectCore,
 AbstractCryptoSelectProcessor,
 IntegratedComponent,
 AbstractCryptoSelectComputer,
 AbstractCryptoSelectController,
 AbstractCryptoSelectClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelect}*/({
  regulateState:regulateCryptoSelectCache,
  stateQPs:CryptoSelectCacheQPs,
 }),
]


export {AbstractCryptoSelect}