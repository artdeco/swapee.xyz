import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectDisplay}
 */
function __AbstractCryptoSelectDisplay() {}
__AbstractCryptoSelectDisplay.prototype = /** @type {!_AbstractCryptoSelectDisplay} */ ({ })
/** @this {xyz.swapee.wc.CryptoSelectDisplay} */ function CryptoSelectDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.CryptoMenu=null
  /** @type {HTMLDivElement} */ this.CryptoDown=null
  /** @type {HTMLDivElement} */ this.CryptoSelectedBl=null
  /** @type {HTMLDivElement} */ this.NoCryptoDropItem=null
  /** @type {HTMLSpanElement} */ this.CryptoSelectedImWr=null
  /** @type {HTMLDivElement} */ this.ChevronUp=null
  /** @type {HTMLDivElement} */ this.ChevronDown=null
  /** @type {!Array<!HTMLSpanElement>} */ this.MenuItems=[]
  /** @type {HTMLInputElement} */ this.CryptoSelectedNameIn=null
  /** @type {HTMLSpanElement} */ this.InnerSpan=null
  /** @type {HTMLElement} */ this.Popup=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectDisplay}
 */
class _AbstractCryptoSelectDisplay { }
/**
 * Display for presenting information from the _ICryptoSelect_.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectDisplay} ‎
 */
class AbstractCryptoSelectDisplay extends newAbstract(
 _AbstractCryptoSelectDisplay,354535074217,CryptoSelectDisplayConstructor,{
  asICryptoSelectDisplay:1,
  superCryptoSelectDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectDisplay} */
AbstractCryptoSelectDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectDisplay} */
function AbstractCryptoSelectDisplayClass(){}

export default AbstractCryptoSelectDisplay


AbstractCryptoSelectDisplay[$implementations]=[
 __AbstractCryptoSelectDisplay,
 Display,
 AbstractCryptoSelectDisplayClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asICryptoSelectTouchscreen:{vdusPQs:{
    CryptoSelectedBl:CryptoSelectedBl,
    CryptoSelectedImWr:CryptoSelectedImWr,
    CryptoSelectedNameIn:CryptoSelectedNameIn,
    CryptoMenu:CryptoMenu,
    CryptoDropItem:CryptoDropItem,
    ChevronUp:ChevronUp,
    ChevronDown:ChevronDown,
    NoCryptoDropItem:NoCryptoDropItem,
    CryptoDown:CryptoDown,
    InnerSpan:InnerSpan,
    Popup:Popup,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    CryptoSelectedBl:/**@type {HTMLDivElement}*/(children[CryptoSelectedBl]),
    CryptoSelectedImWr:/**@type {HTMLSpanElement}*/(children[CryptoSelectedImWr]),
    CryptoSelectedNameIn:/**@type {HTMLInputElement}*/(children[CryptoSelectedNameIn]),
    CryptoMenu:/**@type {HTMLDivElement}*/(children[CryptoMenu]),
    MenuItems:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=a3b35]')]):[],
    CryptoDropItem:/**@type {HTMLDivElement}*/(children[CryptoDropItem]),
    ChevronUp:/**@type {HTMLDivElement}*/(children[ChevronUp]),
    ChevronDown:/**@type {HTMLDivElement}*/(children[ChevronDown]),
    NoCryptoDropItem:/**@type {HTMLDivElement}*/(children[NoCryptoDropItem]),
    CryptoDown:/**@type {HTMLDivElement}*/(children[CryptoDown]),
    InnerSpan:/**@type {HTMLSpanElement}*/(children[InnerSpan]),
    Popup:/**@type {HTMLElement}*/(children[Popup]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.CryptoSelectDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.ICryptoSelectDisplay.Initialese}*/({
   CryptoMenu:1,
   CryptoDown:1,
   CryptoSelectedBl:1,
   NoCryptoDropItem:1,
   CryptoSelectedImWr:1,
   ChevronUp:1,
   ChevronDown:1,
   MenuItems:1,
   CryptoSelectedNameIn:1,
   InnerSpan:1,
   Popup:1,
  }),
  initializer({
   CryptoMenu:_CryptoMenu,
   CryptoDown:_CryptoDown,
   CryptoSelectedBl:_CryptoSelectedBl,
   NoCryptoDropItem:_NoCryptoDropItem,
   CryptoSelectedImWr:_CryptoSelectedImWr,
   ChevronUp:_ChevronUp,
   ChevronDown:_ChevronDown,
   MenuItems:_MenuItems,
   CryptoSelectedNameIn:_CryptoSelectedNameIn,
   InnerSpan:_InnerSpan,
   Popup:_Popup,
  }) {
   if(_CryptoMenu!==undefined) this.CryptoMenu=_CryptoMenu
   if(_CryptoDown!==undefined) this.CryptoDown=_CryptoDown
   if(_CryptoSelectedBl!==undefined) this.CryptoSelectedBl=_CryptoSelectedBl
   if(_NoCryptoDropItem!==undefined) this.NoCryptoDropItem=_NoCryptoDropItem
   if(_CryptoSelectedImWr!==undefined) this.CryptoSelectedImWr=_CryptoSelectedImWr
   if(_ChevronUp!==undefined) this.ChevronUp=_ChevronUp
   if(_ChevronDown!==undefined) this.ChevronDown=_ChevronDown
   if(_MenuItems!==undefined) this.MenuItems=_MenuItems
   if(_CryptoSelectedNameIn!==undefined) this.CryptoSelectedNameIn=_CryptoSelectedNameIn
   if(_InnerSpan!==undefined) this.InnerSpan=_InnerSpan
   if(_Popup!==undefined) this.Popup=_Popup
  },
 }),
]