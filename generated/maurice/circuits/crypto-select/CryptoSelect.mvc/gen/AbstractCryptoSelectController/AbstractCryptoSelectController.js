import CryptoSelectBuffer from '../CryptoSelectBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {CryptoSelectPortConnector} from '../CryptoSelectPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectController}
 */
function __AbstractCryptoSelectController() {}
__AbstractCryptoSelectController.prototype = /** @type {!_AbstractCryptoSelectController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectController}
 */
class _AbstractCryptoSelectController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectController} ‎
 */
export class AbstractCryptoSelectController extends newAbstract(
 _AbstractCryptoSelectController,354535074221,null,{
  asICryptoSelectController:1,
  superCryptoSelectController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectController} */
AbstractCryptoSelectController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectController} */
function AbstractCryptoSelectControllerClass(){}


AbstractCryptoSelectController[$implementations]=[
 AbstractCryptoSelectControllerClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.ICryptoSelectPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractCryptoSelectController,
 CryptoSelectBuffer,
 IntegratedController,
 /**@type {!AbstractCryptoSelectController}*/(CryptoSelectPortConnector),
 AbstractCryptoSelectControllerClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectController}*/({
  setSelected(val){
   const{asICryptoSelectController:{setInputs:setInputs}}=this
   setInputs({selected:val})
  },
  unsetSelected(){
   const{asICryptoSelectController:{setInputs:setInputs}}=this
   setInputs({selected:''})
  },
 }),
]


export default AbstractCryptoSelectController