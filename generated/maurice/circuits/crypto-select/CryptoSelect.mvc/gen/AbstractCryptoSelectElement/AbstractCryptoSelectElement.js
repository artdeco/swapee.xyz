import CryptoSelectRenderVdus from './methods/render-vdus'
import CryptoSelectElementPort from '../CryptoSelectElementPort'
import {precalibrateDataSource,precalibrateResetMouseOver} from './precalibrators'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {CryptoSelectInputsPQs} from '../../pqs/CryptoSelectInputsPQs'
import {CryptoSelectCachePQs} from '../../pqs/CryptoSelectCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractCryptoSelect from '../AbstractCryptoSelect'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectElement}
 */
function __AbstractCryptoSelectElement() {}
__AbstractCryptoSelectElement.prototype = /** @type {!_AbstractCryptoSelectElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectElement}
 */
class _AbstractCryptoSelectElement { }
/**
 * A component description.
 *
 * The _ICryptoSelect_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectElement} ‎
 */
class AbstractCryptoSelectElement extends newAbstract(
 _AbstractCryptoSelectElement,354535074213,null,{
  asICryptoSelectElement:1,
  superCryptoSelectElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectElement} */
AbstractCryptoSelectElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectElement} */
function AbstractCryptoSelectElementClass(){}

export default AbstractCryptoSelectElement


AbstractCryptoSelectElement[$implementations]=[
 __AbstractCryptoSelectElement,
 ElementBase,
 AbstractCryptoSelectElementClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
  calibrate:function induceColonInputs({
   ':source':sourceColAttr,
   ':no-solder':noSolderColAttr,
   ':selected':selectedColAttr,
   ':icon-folder':iconFolderColAttr,
   ':fiat':fiatColAttr,
   ':im-html':imHtmlColAttr,
   ':search':searchColAttr,
   ':selected-icon':selectedIconColAttr,
   ':menu-expanded':menuExpandedColAttr,
   ':is-mouse-over':isMouseOverColAttr,
   ':sm':smColAttr,
   ':is-searching':isSearchingColAttr,
   ':hovering-index':hoveringIndexColAttr,
   ':cryptos':cryptosColAttr,
   ':ignore-cryptos':ignoreCryptosColAttr,
   ':visible-items':visibleItemsColAttr,
   ':keyboard-selected':keyboardSelectedColAttr,
   ':selected-crypto':selectedCryptoColAttr,
   ':matched-keys':matchedKeysColAttr,
  }){
   const{attributes:{
    'source':sourceAttr,
    'no-solder':noSolderAttr,
    'selected':selectedAttr,
    'icon-folder':iconFolderAttr,
    'fiat':fiatAttr,
    'im-html':imHtmlAttr,
    'search':searchAttr,
    'selected-icon':selectedIconAttr,
    'menu-expanded':menuExpandedAttr,
    'is-mouse-over':isMouseOverAttr,
    'sm':smAttr,
    'is-searching':isSearchingAttr,
    'hovering-index':hoveringIndexAttr,
    'cryptos':cryptosAttr,
    'ignore-cryptos':ignoreCryptosAttr,
    'visible-items':visibleItemsAttr,
    'keyboard-selected':keyboardSelectedAttr,
    'selected-crypto':selectedCryptoAttr,
    'matched-keys':matchedKeysAttr,
   }}=this
   return{
    ...(sourceAttr===undefined?{'source':sourceColAttr}:{}),
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(selectedAttr===undefined?{'selected':selectedColAttr}:{}),
    ...(iconFolderAttr===undefined?{'icon-folder':iconFolderColAttr}:{}),
    ...(fiatAttr===undefined?{'fiat':fiatColAttr}:{}),
    ...(imHtmlAttr===undefined?{'im-html':imHtmlColAttr}:{}),
    ...(searchAttr===undefined?{'search':searchColAttr}:{}),
    ...(selectedIconAttr===undefined?{'selected-icon':selectedIconColAttr}:{}),
    ...(menuExpandedAttr===undefined?{'menu-expanded':menuExpandedColAttr}:{}),
    ...(isMouseOverAttr===undefined?{'is-mouse-over':isMouseOverColAttr}:{}),
    ...(smAttr===undefined?{'sm':smColAttr}:{}),
    ...(isSearchingAttr===undefined?{'is-searching':isSearchingColAttr}:{}),
    ...(hoveringIndexAttr===undefined?{'hovering-index':hoveringIndexColAttr}:{}),
    ...(cryptosAttr===undefined?{'cryptos':cryptosColAttr}:{}),
    ...(ignoreCryptosAttr===undefined?{'ignore-cryptos':ignoreCryptosColAttr}:{}),
    ...(visibleItemsAttr===undefined?{'visible-items':visibleItemsColAttr}:{}),
    ...(keyboardSelectedAttr===undefined?{'keyboard-selected':keyboardSelectedColAttr}:{}),
    ...(selectedCryptoAttr===undefined?{'selected-crypto':selectedCryptoColAttr}:{}),
    ...(matchedKeysAttr===undefined?{'matched-keys':matchedKeysColAttr}:{}),
   }
  },
 }),
 AbstractCryptoSelectElementClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
  calibrate:({
   'source':source,
   'no-solder':noSolderAttr,
   'selected':selectedAttr,
   'icon-folder':iconFolderAttr,
   'fiat':fiatAttr,
   'im-html':imHtmlAttr,
   'search':searchAttr,
   'selected-icon':selectedIconAttr,
   'menu-expanded':menuExpandedAttr,
   'is-mouse-over':isMouseOverAttr,
   'sm':smAttr,
   'is-searching':isSearchingAttr,
   'hovering-index':hoveringIndexAttr,
   'cryptos':cryptosAttr,
   'ignore-cryptos':ignoreCryptosAttr,
   'visible-items':visibleItemsAttr,
   'keyboard-selected':keyboardSelectedAttr,
   'selected-crypto':selectedCryptoAttr,
   'matched-keys':matchedKeysAttr,
  })=>{
   return{
    source:source,
    noSolder:noSolderAttr,
    selected:selectedAttr,
    iconFolder:iconFolderAttr,
    fiat:fiatAttr,
    imHtml:imHtmlAttr,
    search:searchAttr,
    selectedIcon:selectedIconAttr,
    menuExpanded:menuExpandedAttr,
    isMouseOver:isMouseOverAttr,
    sm:smAttr,
    isSearching:isSearchingAttr,
    hoveringIndex:hoveringIndexAttr,
    cryptos:cryptosAttr,
    ignoreCryptos:ignoreCryptosAttr,
    visibleItems:visibleItemsAttr,
    keyboardSelected:keyboardSelectedAttr,
    selectedCrypto:selectedCryptoAttr,
    matchedKeys:matchedKeysAttr,
   }
  },
 }),
 AbstractCryptoSelectElementClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractCryptoSelectElementClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
  render:CryptoSelectRenderVdus,
 }),
 AbstractCryptoSelectElementClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
  classes:{
   'CryptoDownBorder': '37551',
   'Fiat': 'c880c',
   'ItemHovered': '8e5a0',
   'CryptoDropItem': '7decb',
   'CryptoDownCollapsed': '6a1d1',
   'CryptoDown': '7bd64',
   'Sm': '20c4c',
   'Expanded': '63f6b',
   'SmWide': '6785a',
   'MouseOver': '73365',
   'DropDownKeyboardFocus': '83f9e',
   'HoveringOverItem': '84661',
   'ImgWr': 'cdcdb',
   'KeyboardSelect': 'eefef',
   'Highlight': '0b905',
   'Chevron': '9a37a',
   'NoCoins': 'eeda9',
   'Pill': '55797',
   'CryptoDropItemBeforeHover': 'adf16',
   'BackgroundStalk': 'db2f0',
  },
  inputsPQs:CryptoSelectInputsPQs,
  cachePQs:CryptoSelectCachePQs,
  vdus:{
   'CryptoSelectedBl': 'a3b31',
   'CryptoSelectedImWr': 'a3b32',
   'CryptoSelectedNameIn': 'a3b33',
   'CryptoMenu': 'a3b34',
   'CryptoDropItem': 'a3b36',
   'ChevronUp': 'a3b37',
   'ChevronDown': 'a3b38',
   'NoCryptoDropItem': 'a3b39',
   'CryptoDown': 'a3b310',
   'Popup': 'a3b311',
   'InnerSpan': 'a3b312',
   'MenuItem': 'a3b35',
  },
 }),
 IntegratedComponent,
 AbstractCryptoSelectElementClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','selected','iconFolder','fiat','imHtml','search','selectedIcon','menuExpanded','isMouseOver','sm','isSearching','hoveringIndex','cryptos','ignoreCryptos','visibleItems','keyboardSelected','selectedCrypto','matchedKeys','source','no-solder',':no-solder',':selected','icon-folder',':icon-folder',':fiat','im-html',':im-html',':search','selected-icon',':selected-icon','menu-expanded',':menu-expanded','is-mouse-over',':is-mouse-over',':sm','is-searching',':is-searching','hovering-index',':hovering-index',':cryptos','ignore-cryptos',':ignore-cryptos','visible-items',':visible-items','keyboard-selected',':keyboard-selected','selected-crypto',':selected-crypto','matched-keys',':matched-keys',':source','fe646','183d1','c0aee','14164','a76ba','f49c8','e35b5','53411','2cc54','c6845','00e36','e6887','ef7de','f756a','7d5f3','1593a','06a94','73071','3353a','71df4','ed79a','58419','27b81','9a8a8','a35e0','0dbda','f1606','11a11','586aa','children']),
   })
  },
  get Port(){
   return CryptoSelectElementPort
  },
 }),
 Landed,
 AbstractCryptoSelectElementClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectElement}*/({
  constructor(){
   this.land={
    Popup:null,
   }
  },
 }),
]



AbstractCryptoSelectElement[$implementations]=[AbstractCryptoSelect,
 /** @type {!AbstractCryptoSelectElement} */ ({
  rootId:'CryptoSelect',
  __$id:3545350742,
  fqn:'xyz.swapee.wc.ICryptoSelect',
  maurice_element_v3:true,
 }),
]