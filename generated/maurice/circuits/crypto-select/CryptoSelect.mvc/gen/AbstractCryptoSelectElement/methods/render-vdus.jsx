export default function CryptoSelectRenderVdus(){
 return (<div $id="CryptoSelect">
  <vdu $id="CryptoSelectedBl" />
  <vdu $id="CryptoSelectedImWr" />
  <vdu $id="CryptoSelectedNameIn" />
  <vdu $id="CryptoMenu" />
  <vdu $id="MenuItem" />
  <vdu $id="CryptoDropItem" />
  <vdu $id="ChevronUp" />
  <vdu $id="ChevronDown" />
  <vdu $id="NoCryptoDropItem" />
  <vdu $id="CryptoDown" />
  <vdu $id="InnerSpan" />
  <vdu $id="Popup" />
 </div>)
}