
/**@this {xyz.swapee.wc.ICryptoSelectElement}*/
export function precalibrateDataSource(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ICryptoSelectElement.calibrateDataSource.Form}*/
 const _inputs={
  source:inputs.source,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.source) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.calibrateDataSource(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ICryptoSelectElement}*/
export function precalibrateResetMouseOver(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ICryptoSelectElement.calibrateResetMouseOver.Form}*/
 const _inputs={
  isMouseOver:inputs.isMouseOver,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.isMouseOver) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.calibrateResetMouseOver(__inputs,__changes)
 return RET
}