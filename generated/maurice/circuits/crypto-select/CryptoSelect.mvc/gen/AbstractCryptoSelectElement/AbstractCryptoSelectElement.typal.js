
import AbstractCryptoSelect from '../AbstractCryptoSelect'

/** @abstract {xyz.swapee.wc.ICryptoSelectElement} */
export default class AbstractCryptoSelectElement { }



AbstractCryptoSelectElement[$implementations]=[AbstractCryptoSelect,
 /** @type {!AbstractCryptoSelectElement} */ ({
  rootId:'CryptoSelect',
  __$id:3545350742,
  fqn:'xyz.swapee.wc.ICryptoSelect',
  maurice_element_v3:true,
 }),
]