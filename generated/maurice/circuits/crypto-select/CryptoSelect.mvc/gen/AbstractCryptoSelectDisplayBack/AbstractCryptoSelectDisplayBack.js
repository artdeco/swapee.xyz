import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectDisplay}
 */
function __AbstractCryptoSelectDisplay() {}
__AbstractCryptoSelectDisplay.prototype = /** @type {!_AbstractCryptoSelectDisplay} */ ({ })
/** @this {xyz.swapee.wc.back.CryptoSelectDisplay} */ function CryptoSelectDisplayConstructor() {
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.MenuItems=[]
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectDisplay}
 */
class _AbstractCryptoSelectDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectDisplay} ‎
 */
class AbstractCryptoSelectDisplay extends newAbstract(
 _AbstractCryptoSelectDisplay,354535074219,CryptoSelectDisplayConstructor,{
  asICryptoSelectDisplay:1,
  superCryptoSelectDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectDisplay} */
AbstractCryptoSelectDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectDisplay} */
function AbstractCryptoSelectDisplayClass(){}

export default AbstractCryptoSelectDisplay


AbstractCryptoSelectDisplay[$implementations]=[
 __AbstractCryptoSelectDisplay,
 GraphicsDriverBack,
 AbstractCryptoSelectDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.ICryptoSelectDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.ICryptoSelectDisplay}*/({
    CryptoSelectedBl:twinMock,
    CryptoSelectedImWr:twinMock,
    CryptoSelectedNameIn:twinMock,
    CryptoMenu:twinMock,
    CryptoDropItem:twinMock,
    ChevronUp:twinMock,
    ChevronDown:twinMock,
    NoCryptoDropItem:twinMock,
    CryptoDown:twinMock,
    InnerSpan:twinMock,
    Popup:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.CryptoSelectDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.ICryptoSelectDisplay.Initialese}*/({
   CryptoMenu:1,
   CryptoDown:1,
   CryptoSelectedBl:1,
   NoCryptoDropItem:1,
   CryptoSelectedImWr:1,
   ChevronUp:1,
   ChevronDown:1,
   MenuItems:1,
   CryptoSelectedNameIn:1,
   InnerSpan:1,
   Popup:1,
  }),
  initializer({
   CryptoMenu:_CryptoMenu,
   CryptoDown:_CryptoDown,
   CryptoSelectedBl:_CryptoSelectedBl,
   NoCryptoDropItem:_NoCryptoDropItem,
   CryptoSelectedImWr:_CryptoSelectedImWr,
   ChevronUp:_ChevronUp,
   ChevronDown:_ChevronDown,
   MenuItems:_MenuItems,
   CryptoSelectedNameIn:_CryptoSelectedNameIn,
   InnerSpan:_InnerSpan,
   Popup:_Popup,
  }) {
   if(_CryptoMenu!==undefined) this.CryptoMenu=_CryptoMenu
   if(_CryptoDown!==undefined) this.CryptoDown=_CryptoDown
   if(_CryptoSelectedBl!==undefined) this.CryptoSelectedBl=_CryptoSelectedBl
   if(_NoCryptoDropItem!==undefined) this.NoCryptoDropItem=_NoCryptoDropItem
   if(_CryptoSelectedImWr!==undefined) this.CryptoSelectedImWr=_CryptoSelectedImWr
   if(_ChevronUp!==undefined) this.ChevronUp=_ChevronUp
   if(_ChevronDown!==undefined) this.ChevronDown=_ChevronDown
   if(_MenuItems!==undefined) this.MenuItems=_MenuItems
   if(_CryptoSelectedNameIn!==undefined) this.CryptoSelectedNameIn=_CryptoSelectedNameIn
   if(_InnerSpan!==undefined) this.InnerSpan=_InnerSpan
   if(_Popup!==undefined) this.Popup=_Popup
  },
 }),
]