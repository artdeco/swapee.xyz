import AbstractCryptoSelectTouchscreenAT from '../AbstractCryptoSelectTouchscreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectTouchscreenBack}
 */
function __AbstractCryptoSelectTouchscreenBack() {}
__AbstractCryptoSelectTouchscreenBack.prototype = /** @type {!_AbstractCryptoSelectTouchscreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen}
 */
class _AbstractCryptoSelectTouchscreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen} ‎
 */
class AbstractCryptoSelectTouchscreenBack extends newAbstract(
 _AbstractCryptoSelectTouchscreenBack,354535074227,null,{
  asICryptoSelectTouchscreen:1,
  superCryptoSelectTouchscreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen} */
AbstractCryptoSelectTouchscreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectTouchscreen} */
function AbstractCryptoSelectTouchscreenBackClass(){}

export default AbstractCryptoSelectTouchscreenBack


AbstractCryptoSelectTouchscreenBack[$implementations]=[
 __AbstractCryptoSelectTouchscreenBack,
 AbstractCryptoSelectTouchscreenAT,
]