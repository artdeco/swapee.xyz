import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectComputer}
 */
function __AbstractCryptoSelectComputer() {}
__AbstractCryptoSelectComputer.prototype = /** @type {!_AbstractCryptoSelectComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectComputer}
 */
class _AbstractCryptoSelectComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectComputer} ‎
 */
export class AbstractCryptoSelectComputer extends newAbstract(
 _AbstractCryptoSelectComputer,35453507421,null,{
  asICryptoSelectComputer:1,
  superCryptoSelectComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectComputer} */
AbstractCryptoSelectComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectComputer} */
function AbstractCryptoSelectComputerClass(){}


AbstractCryptoSelectComputer[$implementations]=[
 __AbstractCryptoSelectComputer,
 Adapter,
]


export default AbstractCryptoSelectComputer