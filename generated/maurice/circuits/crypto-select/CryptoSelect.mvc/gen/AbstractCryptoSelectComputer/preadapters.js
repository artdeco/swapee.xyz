
/**@this {xyz.swapee.wc.ICryptoSelectComputer}*/
export function preadaptWideness(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ICryptoSelectComputer.adaptWideness.Form}*/
 const _inputs={
  selected:inputs.selected,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptWideness(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ICryptoSelectComputer}*/
export function preadaptSelectedCrypto(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ICryptoSelectComputer.adaptSelectedCrypto.Form}*/
 const _inputs={
  selected:inputs.selected,
  cryptos:inputs.cryptos,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.selected)||(!__inputs.cryptos)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptSelectedCrypto(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ICryptoSelectComputer}*/
export function preadaptSearchInput(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ICryptoSelectComputer.adaptSearchInput.Form}*/
 const _inputs={
  menuExpanded:inputs.menuExpanded,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(__inputs.menuExpanded) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptSearchInput(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.ICryptoSelectComputer}*/
export function preadaptPopupShown(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ICryptoSelectComputer.adaptPopupShown.Form}*/
 const _inputs={
  menuExpanded:inputs.menuExpanded,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPopupShown(__inputs,__changes)
 const{shown:shown,...REST}=RET
 const{land:{Popup:Popup}}=this
 if(Popup) Popup.port.inputs['3fdec']=shown
 return REST
}

/**@this {xyz.swapee.wc.ICryptoSelectComputer}*/
export function preadaptMatchedKeys(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ICryptoSelectComputer.adaptMatchedKeys.Form}*/
 const _inputs={
  search:inputs.search,
  cryptos:inputs.cryptos,
  ignoreCryptos:inputs.ignoreCryptos,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.cryptos) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptMatchedKeys(__inputs,__changes)
 return RET
}