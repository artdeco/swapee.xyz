import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectControllerAR}
 */
function __AbstractCryptoSelectControllerAR() {}
__AbstractCryptoSelectControllerAR.prototype = /** @type {!_AbstractCryptoSelectControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectControllerAR}
 */
class _AbstractCryptoSelectControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ICryptoSelectControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractCryptoSelectControllerAR} ‎
 */
class AbstractCryptoSelectControllerAR extends newAbstract(
 _AbstractCryptoSelectControllerAR,354535074224,null,{
  asICryptoSelectControllerAR:1,
  superCryptoSelectControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectControllerAR} */
AbstractCryptoSelectControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractCryptoSelectControllerAR} */
function AbstractCryptoSelectControllerARClass(){}

export default AbstractCryptoSelectControllerAR


AbstractCryptoSelectControllerAR[$implementations]=[
 __AbstractCryptoSelectControllerAR,
 AR,
 AbstractCryptoSelectControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ICryptoSelectControllerAR}*/({
  allocator(){
   this.methods={
    flipMenuExpanded:'f4b8d',
    setSelected:'b5e89',
    unsetSelected:'31ff5',
   }
  },
 }),
]