import CryptoSelectClassesPQs from '../../pqs/CryptoSelectClassesPQs'
import AbstractCryptoSelectTouchscreenAR from '../AbstractCryptoSelectTouchscreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {CryptoSelectInputsPQs} from '../../pqs/CryptoSelectInputsPQs'
import {CryptoSelectMemoryQPs} from '../../pqs/CryptoSelectMemoryQPs'
import {CryptoSelectCacheQPs} from '../../pqs/CryptoSelectCacheQPs'
import {CryptoSelectVdusPQs} from '../../pqs/CryptoSelectVdusPQs'
import {CryptoSelectClassesQPs} from '../../pqs/CryptoSelectClassesQPs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectTouchscreen}
 */
function __AbstractCryptoSelectTouchscreen() {}
__AbstractCryptoSelectTouchscreen.prototype = /** @type {!_AbstractCryptoSelectTouchscreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectTouchscreen}
 */
class _AbstractCryptoSelectTouchscreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectTouchscreen} ‎
 */
class AbstractCryptoSelectTouchscreen extends newAbstract(
 _AbstractCryptoSelectTouchscreen,354535074226,null,{
  asICryptoSelectTouchscreen:1,
  superCryptoSelectTouchscreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectTouchscreen} */
AbstractCryptoSelectTouchscreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectTouchscreen} */
function AbstractCryptoSelectTouchscreenClass(){}

export default AbstractCryptoSelectTouchscreen


AbstractCryptoSelectTouchscreen[$implementations]=[
 __AbstractCryptoSelectTouchscreen,
 AbstractCryptoSelectTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({
  deduceInputs(){
   const{asICryptoSelectDisplay:{
    CryptoSelectedImWr:CryptoSelectedImWr,
   }}=this
   return{
    selectedIcon:CryptoSelectedImWr?.innerHTML,
   }
  },
 }),
 AbstractCryptoSelectTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({
  inputsPQs:CryptoSelectInputsPQs,
  classesPQs:CryptoSelectClassesPQs,
  memoryQPs:CryptoSelectMemoryQPs,
  cacheQPs:CryptoSelectCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractCryptoSelectTouchscreenAR,
 AbstractCryptoSelectTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({
  vdusPQs:CryptoSelectVdusPQs,
 }),
 AbstractCryptoSelectTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({
  classesQPs:CryptoSelectClassesQPs,
 }),
 AbstractCryptoSelectTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectTouchscreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const CryptoSelectedBl=this.CryptoSelectedBl
    if(CryptoSelectedBl){
     CryptoSelectedBl.addEventListener('click',(ev)=>{
      this.selectedBlCl(/**@type {!MouseEvent}*/(ev))
     })
     CryptoSelectedBl.addEventListener('mouseenter',(ev)=>{
      this.addMouseOver(/**@type {!MouseEvent}*/(ev))
     })
    }
    const CryptoSelectedNameIn=this.CryptoSelectedNameIn
    if(CryptoSelectedNameIn){
     CryptoSelectedNameIn.addEventListener('focus',(ev)=>{
      this.onInputFocus()
     })
     CryptoSelectedNameIn.addEventListener('blur',(ev)=>{
      this.onInputBlur()
     })
     CryptoSelectedNameIn.addEventListener('keydown',(ev)=>{
      [this.onInputKeyDown(/**@type {!KeyboardEvent}*/(ev)),this.inputKeyboardScroll(/**@type {!KeyboardEvent}*/(ev))]
     })
     CryptoSelectedNameIn.addEventListener('keyup',(ev)=>{
      this.inputKeyUp(/**@type {!KeyboardEvent}*/(ev))
     })
    }
    const MenuItems=this.MenuItems
    for(const MenuItem of MenuItems){
      MenuItem.addEventListener('mousemove',(ev)=>{
       this.menuMouseMv(/**@type {!MouseEvent}*/(ev))
      })
    }
    for(const MenuItem of MenuItems){
      MenuItem.addEventListener('mousedown',(ev)=>{
       this.menuItemCl(/**@type {!MouseEvent}*/(ev))
      })
    }
    const element=this.element
    if(element){
     element.addEventListener('mouseleave',(ev)=>{
      this.onMouseLeave(/**@type {!MouseEvent}*/(ev))
     })
    }
   })
  },
 }),
]