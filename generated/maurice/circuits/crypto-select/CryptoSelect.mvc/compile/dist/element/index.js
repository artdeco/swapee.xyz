/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelect` interface.
 * @extends {xyz.swapee.wc.AbstractCryptoSelect}
 */
class AbstractCryptoSelect extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ICryptoSelect_, providing input
 * pins.
 * @extends {xyz.swapee.wc.CryptoSelectPort}
 */
class CryptoSelectPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelectController` interface.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectController}
 */
class AbstractCryptoSelectController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ICryptoSelect_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.CryptoSelectElement}
 */
class CryptoSelectElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.CryptoSelectBuffer}
 */
class CryptoSelectBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelectComputer` interface.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectComputer}
 */
class AbstractCryptoSelectComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.CryptoSelectComputer}
 */
class CryptoSelectComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.CryptoSelectController}
 */
class CryptoSelectController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractCryptoSelect = AbstractCryptoSelect
module.exports.CryptoSelectPort = CryptoSelectPort
module.exports.AbstractCryptoSelectController = AbstractCryptoSelectController
module.exports.CryptoSelectElement = CryptoSelectElement
module.exports.CryptoSelectBuffer = CryptoSelectBuffer
module.exports.AbstractCryptoSelectComputer = AbstractCryptoSelectComputer
module.exports.CryptoSelectComputer = CryptoSelectComputer
module.exports.CryptoSelectController = CryptoSelectController

Object.defineProperties(module.exports, {
 'AbstractCryptoSelect': {get: () => require('./precompile/internal')[35453507421]},
 [35453507421]: {get: () => module.exports['AbstractCryptoSelect']},
 'CryptoSelectPort': {get: () => require('./precompile/internal')[35453507423]},
 [35453507423]: {get: () => module.exports['CryptoSelectPort']},
 'AbstractCryptoSelectController': {get: () => require('./precompile/internal')[35453507424]},
 [35453507424]: {get: () => module.exports['AbstractCryptoSelectController']},
 'CryptoSelectElement': {get: () => require('./precompile/internal')[35453507428]},
 [35453507428]: {get: () => module.exports['CryptoSelectElement']},
 'CryptoSelectBuffer': {get: () => require('./precompile/internal')[354535074211]},
 [354535074211]: {get: () => module.exports['CryptoSelectBuffer']},
 'AbstractCryptoSelectComputer': {get: () => require('./precompile/internal')[354535074230]},
 [354535074230]: {get: () => module.exports['AbstractCryptoSelectComputer']},
 'CryptoSelectComputer': {get: () => require('./precompile/internal')[354535074231]},
 [354535074231]: {get: () => module.exports['CryptoSelectComputer']},
 'CryptoSelectController': {get: () => require('./precompile/internal')[354535074261]},
 [354535074261]: {get: () => module.exports['CryptoSelectController']},
})