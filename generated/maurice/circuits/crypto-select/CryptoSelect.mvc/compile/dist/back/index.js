/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelect` interface.
 * @extends {xyz.swapee.wc.AbstractCryptoSelect}
 */
class AbstractCryptoSelect extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ICryptoSelect_, providing input
 * pins.
 * @extends {xyz.swapee.wc.CryptoSelectPort}
 */
class CryptoSelectPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelectController` interface.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectController}
 */
class AbstractCryptoSelectController extends (class {/* lazy-loaded */}) {}
/**
 * The _ICryptoSelect_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.CryptoSelectHtmlComponent}
 */
class CryptoSelectHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.CryptoSelectBuffer}
 */
class CryptoSelectBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelectComputer` interface.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectComputer}
 */
class AbstractCryptoSelectComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.CryptoSelectComputer}
 */
class CryptoSelectComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _ICryptoSelect_.
 * @extends {xyz.swapee.wc.CryptoSelectProcessor}
 */
class CryptoSelectProcessor extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.CryptoSelectController}
 */
class CryptoSelectController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractCryptoSelect = AbstractCryptoSelect
module.exports.CryptoSelectPort = CryptoSelectPort
module.exports.AbstractCryptoSelectController = AbstractCryptoSelectController
module.exports.CryptoSelectHtmlComponent = CryptoSelectHtmlComponent
module.exports.CryptoSelectBuffer = CryptoSelectBuffer
module.exports.AbstractCryptoSelectComputer = AbstractCryptoSelectComputer
module.exports.CryptoSelectComputer = CryptoSelectComputer
module.exports.CryptoSelectProcessor = CryptoSelectProcessor
module.exports.CryptoSelectController = CryptoSelectController