import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractCryptoSelect}*/
export class AbstractCryptoSelect extends Module['35453507421'] {}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelect} */
AbstractCryptoSelect.class=function(){}
/** @type {typeof xyz.swapee.wc.CryptoSelectPort} */
export const CryptoSelectPort=Module['35453507423']
/**@extends {xyz.swapee.wc.AbstractCryptoSelectController}*/
export class AbstractCryptoSelectController extends Module['35453507424'] {}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectController} */
AbstractCryptoSelectController.class=function(){}
/** @type {typeof xyz.swapee.wc.CryptoSelectHtmlComponent} */
export const CryptoSelectHtmlComponent=Module['354535074210']
/** @type {typeof xyz.swapee.wc.CryptoSelectBuffer} */
export const CryptoSelectBuffer=Module['354535074211']
/**@extends {xyz.swapee.wc.AbstractCryptoSelectComputer}*/
export class AbstractCryptoSelectComputer extends Module['354535074230'] {}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectComputer} */
AbstractCryptoSelectComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.CryptoSelectComputer} */
export const CryptoSelectComputer=Module['354535074231']
/** @type {typeof xyz.swapee.wc.CryptoSelectProcessor} */
export const CryptoSelectProcessor=Module['354535074251']
/** @type {typeof xyz.swapee.wc.back.CryptoSelectController} */
export const CryptoSelectController=Module['354535074261']