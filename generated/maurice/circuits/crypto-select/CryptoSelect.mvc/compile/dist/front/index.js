/**
 * Display for presenting information from the _ICryptoSelect_.
 * @extends {xyz.swapee.wc.CryptoSelectDisplay}
 */
class CryptoSelectDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @extends {xyz.swapee.wc.CryptoSelectTouchscreen}
 */
class CryptoSelectTouchscreen extends (class {/* lazy-loaded */}) {}

module.exports.CryptoSelectDisplay = CryptoSelectDisplay
module.exports.CryptoSelectTouchscreen = CryptoSelectTouchscreen