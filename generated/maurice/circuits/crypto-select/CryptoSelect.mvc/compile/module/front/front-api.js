import { CryptoSelectDisplay, CryptoSelectTouchscreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.CryptoSelectDisplay} */
export { CryptoSelectDisplay }
/** @lazy @api {xyz.swapee.wc.CryptoSelectTouchscreen} */
export { CryptoSelectTouchscreen }