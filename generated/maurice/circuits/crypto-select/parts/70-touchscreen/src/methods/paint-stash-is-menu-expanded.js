/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashIsMenuExpanded} */
export default function paint_stashIsMenuExpanded({menuExpanded:menuExpanded},_,{'3353a':prev_menuExpanded}) {
 if(prev_menuExpanded===undefined||menuExpanded===null) return
 const{
  asICryptoSelectTouchscreen:asICryptoSelectTouchscreen,
  asICryptoSelectTouchscreen:{stashIsMenuExpanded:stashIsMenuExpanded},
 }=this
 const res=stashIsMenuExpanded({
  menuExpanded:menuExpanded,
 })
 if(!res) return
 const{
  isMenuExpanded:isMenuExpanded,
 }=res
 
 asICryptoSelectTouchscreen.isMenuExpanded=isMenuExpanded
}
paint_stashIsMenuExpanded['_id']='cdf68a5'