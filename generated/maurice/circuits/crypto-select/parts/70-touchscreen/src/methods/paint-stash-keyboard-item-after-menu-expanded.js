/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashKeyboardItemAfterMenuExpanded} */
export default function paint_stashKeyboardItemAfterMenuExpanded({menuExpanded:menuExpanded,selected:selected},_,{'3353a':prev_menuExpanded,'ef7de':prev_selected}) {
 if(prev_menuExpanded===undefined||menuExpanded===null) return
 if(prev_selected===undefined||selected===null) return
 const{
  asICryptoSelectTouchscreen:asICryptoSelectTouchscreen,
  asICryptoSelectTouchscreen:{stashKeyboardItemAfterMenuExpanded:stashKeyboardItemAfterMenuExpanded},
 }=this
 const res=stashKeyboardItemAfterMenuExpanded({
  menuExpanded:menuExpanded,
  selected:selected,
 })
 if(!res) return
 const{
  keyboardItem:keyboardItem,
 }=res
 
 asICryptoSelectTouchscreen.keyboardItem=keyboardItem
}
paint_stashKeyboardItemAfterMenuExpanded['_id']='4f3ddab'