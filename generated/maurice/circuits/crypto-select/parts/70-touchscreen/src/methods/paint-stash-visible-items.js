/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashVisibleItems} */
export default function paint_stashVisibleItems({matchedKeys:matchedKeys},_,{'586aa':prev_matchedKeys}) {
 if(prev_matchedKeys===undefined||matchedKeys===null) return
 const{
  asICryptoSelectTouchscreen:asICryptoSelectTouchscreen,
  asICryptoSelectTouchscreen:{stashVisibleItems:stashVisibleItems},
 }=this
 const res=stashVisibleItems({
  matchedKeys:matchedKeys,
 })
 if(!res) return
 const{
  visibleItems:visibleItems,
 }=res
 
 asICryptoSelectTouchscreen.visibleItems=visibleItems
}
paint_stashVisibleItems['_id']='c136d87'