/** @type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashSelectedInLocalStorage} */
export default function paint_stashSelectedInLocalStorage({selected:selected},_,{'ef7de':prev_selected}) {
 if(prev_selected===undefined||selected===null) return
 const{
  asICryptoSelectTouchscreen:{stashSelectedInLocalStorage:stashSelectedInLocalStorage},
 }=this
 stashSelectedInLocalStorage({
  selected:selected,
 })
}
paint_stashSelectedInLocalStorage['_id']='d3f701d'