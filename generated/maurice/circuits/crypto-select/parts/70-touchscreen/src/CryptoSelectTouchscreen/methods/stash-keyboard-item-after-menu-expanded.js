/**@type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashKeyboardItemAfterMenuExpanded}*/
export default
function stashKeyboardItemAfterMenuExpanded({
 selected:selected,
 menuExpanded:menuExpanded,
}) {
 const{
  asICryptoSelectDisplay:{resolveItemByKey:resolveItemByKey},
 }=this
 if(!menuExpanded) {
  return{
   keyboardItem:null,
  }
 }
 const keyboardItem=resolveItemByKey(selected)
 return{
  keyboardItem:keyboardItem,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFzaEJHLFNBQVMsa0NBQWtDO0NBQzFDLGlCQUFpQjtDQUNqQix5QkFBeUI7QUFDMUIsQ0FBQztDQUNBO0VBQ0Msd0JBQXdCLGlDQUFpQztHQUN4RDtDQUNGLEVBQUUsQ0FBQztFQUNGO0dBQ0MsaUJBQWlCO0FBQ3BCO0FBQ0E7Q0FDQyxNQUFNLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQztDQUNwQztFQUNDLHlCQUF5QjtBQUMzQjtBQUNBLENBQUYifQ==