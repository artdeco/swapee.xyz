/**@type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashSelectedInLocalStorage}*/
export default
function stashSelectedInLocalStorage({
 selected:selected,
}) {
 const{element:{id:id}}=this
 if(!id) return
 const current=window.localStorage.getItem(`#${id}.selected`)
 if(current!=selected) {
  //  console.log('updating local storage to', selected)
  window.localStorage.setItem(`#${id}.selected`, selected)
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUErZUcsU0FBUywyQkFBMkI7Q0FDbkMsaUJBQWlCO0FBQ2xCLENBQUM7Q0FDQSxNQUFNLFNBQVMsS0FBSyxHQUFHO0NBQ3ZCLEVBQUUsQ0FBQyxLQUFLO0NBQ1IsTUFBTSxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUU7Q0FDbEQsRUFBRSxDQUFDLFFBQVEsQ0FBQztFQUNYLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLE1BQU0sUUFBUSxHQUFHLEVBQUU7RUFDN0MsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsU0FBUyxFQUFFO0FBQ2pEO0FBQ0EsQ0FBRiJ9