import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashIsMenuExpanded} */
export default function stashIsMenuExpanded({menuExpanded:menuExpanded}){
 return{
  isMenuExpanded:menuExpanded,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFpZ0JHLFNBQVMsbUJBQW1CLEVBQUUseUJBQXlCLENBQUM7Q0FDdkQ7RUFDQywyQkFBMkI7QUFDN0I7QUFDQSxDQUFGIn0=