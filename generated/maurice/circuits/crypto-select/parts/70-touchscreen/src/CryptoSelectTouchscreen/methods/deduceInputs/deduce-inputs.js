import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectTouchscreen._deduceInputs} */
export default function deduceInputs(el){
 const{
  // asICryptoSelectDisplay:{
  //  CryptoSelectedImWr:CryptoSelectedImWr,
  // },
  asICryptoSelectTouchscreen:{
   classes:{
    Sm:Sm,
    CryptoDropItem:CryptoDropItem,
   },
  },
 }=this
 const items=[...el.querySelectorAll(`.${CryptoDropItem}`)].map(
  (cryptoItemEl)=>{
   const key=cryptoItemEl.dataset['value']
   if(!key) return
   const displayName=cryptoItemEl.dataset['name']
   return [key, { displayName: displayName }]
  }).filter(Boolean)
 // const imHtml=CryptoSelectedImWr?CryptoSelectedImWr.innerHTML:undefined
 const cryptos=new Map(items)

 const sm=el.classList.contains(Sm)
 // const sm=classList.contains(classes.SmWide)
 // console.log(123)

 let selected
 if(el.id) {
 //  debugger
  const localSelected=window.localStorage.getItem(`#${el.id}.selected`)
  if(localSelected) {
   selected=localSelected
  }
 }
 return {
 //  imHtml:imHtml,1
  cryptos:cryptos,
  sm:sm,
  ...(selected?{selected:selected}:{}),
  // selected:selected,
  // menuExpanded:true,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUEyY0csU0FBUyxZQUFZLENBQUM7Q0FDckI7RUFDQyxHQUFHO0VBQ0gsSUFBSSxxQ0FBcUM7RUFDekMsRUFBRTtFQUNGO0dBQ0M7SUFDQyxLQUFLO0lBQ0wsNkJBQTZCO0FBQ2pDO0FBQ0E7R0FDRztDQUNGLE1BQU0sS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsY0FBYyxDQUFDLEdBQUcsQ0FBQyxHQUFHO0VBQzdELENBQUM7R0FDQSxNQUFNLEdBQUcsQ0FBQyxZQUFZLENBQUM7R0FDdkIsRUFBRSxDQUFDLE1BQU07R0FDVCxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUM7R0FDL0IsT0FBTyxJQUFJLElBQUksYUFBYSxXQUFXLEVBQUU7QUFDNUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO0NBQ1gsR0FBRyxNQUFNLE1BQU0sQ0FBQyxxQ0FBcUMsQ0FBQztDQUN0RCxNQUFNLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQzs7Q0FFdEIsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7Q0FDL0IsR0FBRyxNQUFNLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztDQUN2QyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7O0NBRWYsSUFBSTtDQUNKLEVBQUUsQ0FBQyxFQUFFLENBQUM7Q0FDTixJQUFJO0VBQ0gsTUFBTSxhQUFhLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFO0VBQzNELEVBQUUsQ0FBQztHQUNGLFFBQVEsQ0FBQztBQUNaO0FBQ0E7Q0FDQztDQUNBLElBQUksYUFBYSxDQUFDO0VBQ2pCLGVBQWU7RUFDZixLQUFLO0FBQ1AsS0FBSyxDQUFDLFVBQVUsaUJBQWlCLENBQUMsQ0FBQyxFQUFFLENBQUM7RUFDcEMsR0FBRyxpQkFBaUI7RUFDcEIsR0FBRyxpQkFBaUI7QUFDdEI7QUFDQSxDQUFGIn0=