import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectTouchscreen._stashVisibleItems} */
export default function stashVisibleItems({
 matchedKeys:matchedKeys,
}) {
 // console.log('stash visible items')
 const{
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
  asICryptoSelectTouchscreen:{
   classes:{CryptoDropItem:CryptoDropItem},
  },
 }=this
 const items=[...CryptoDown.querySelectorAll(`.${CryptoDropItem}`)]
 const visibleItems=items.filter((item)=>{
  return matchedKeys.has(item.dataset['value'])
 })
 return {
  visibleItems:visibleItems,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUF5Z0JHLFNBQVMsaUJBQWlCO0NBQ3pCLHVCQUF1QjtBQUN4QixDQUFDO0NBQ0EsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sUUFBUTtDQUM5QjtFQUNDO0dBQ0MscUJBQXFCO0FBQ3hCO0VBQ0U7R0FDQyxTQUFTLDZCQUE2QjtBQUN6QztHQUNHO0NBQ0YsTUFBTSxLQUFLLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxjQUFjLENBQUM7Q0FDL0QsTUFBTSxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0VBQ2hDLE9BQU8sV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUM7QUFDOUIsRUFBRTtDQUNEO0VBQ0MseUJBQXlCO0FBQzNCO0FBQ0EsQ0FBRiJ9