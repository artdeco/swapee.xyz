import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._onInputBlur} */
export default async function onInputBlur() {
 await new Promise(r=>setTimeout(r,20))
 const{
  asICryptoSelectTouchscreen:asICryptoSelectTouchscreen,
 }=this
 asICryptoSelectTouchscreen.isBlurringInput=true
 setTimeout(()=>{
  asICryptoSelectTouchscreen.isBlurringInput=false
 },200)
 return{
  isSearching:   false,
  menuExpanded:  false,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUF1NUJHLE1BQU0sU0FBUyxXQUFXLENBQUM7Q0FDMUIsTUFBTSxJQUFJLE9BQU8sQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUM7Q0FDbEM7RUFDQyxxREFBcUQ7R0FDcEQ7Q0FDRiwwQkFBMEIsQ0FBQyxlQUFlLENBQUM7Q0FDM0MsVUFBVSxDQUFDLENBQUM7RUFDWCwwQkFBMEIsQ0FBQyxlQUFlLENBQUM7QUFDN0MsR0FBRztDQUNGO0VBQ0MsZUFBZSxLQUFLO0VBQ3BCLGVBQWUsS0FBSztBQUN0QjtBQUNBLENBQUYifQ==