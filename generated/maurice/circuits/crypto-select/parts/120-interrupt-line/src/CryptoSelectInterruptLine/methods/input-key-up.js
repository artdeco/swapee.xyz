/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._inputKeyUp}*/
export default
function inputKeyUp(kev) {
 const{
  asICryptoSelectTouchscreen:{
   asICryptoSelectDisplay:{
    CryptoSelectedNameIn:{
     value:value,
    },
   },
  },
 }=this

 if(kev.key=='ArrowUp'||kev.key=='ArrowDown'||kev.key=='Enter') {
  return
 }

 return {
  search:value,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFpNEJHLFNBQVMsVUFBVSxDQUFDO0NBQ25CO0VBQ0M7R0FDQztJQUNDO0tBQ0MsV0FBVztBQUNoQjtBQUNBO0FBQ0E7R0FDRzs7Q0FFRixFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxjQUFjLENBQUMsR0FBRyxFQUFFLGdCQUFnQixDQUFDLEdBQUcsRUFBRTtFQUNyRDtBQUNGOztDQUVDO0VBQ0MsWUFBWTtBQUNkO0FBQ0EsQ0FBRiJ9