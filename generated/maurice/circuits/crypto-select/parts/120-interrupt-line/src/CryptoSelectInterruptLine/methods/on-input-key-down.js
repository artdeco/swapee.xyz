/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._onInputKeyDown}*/
export default
function onInputKeyDown(ev) {
 const{
  asICryptoSelectTouchscreen:{
   asICryptoSelectDisplay:{
    CryptoSelectedNameIn:CryptoSelectedNameIn,
   },
  },
 }=this
 if(ev.key=='Escape') {
  CryptoSelectedNameIn.blur()
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUE2MEJHLFNBQVMsY0FBYyxDQUFDO0NBQ3ZCO0VBQ0M7R0FDQztJQUNDLHlDQUF5QztBQUM3QztBQUNBO0dBQ0c7Q0FDRixFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRTtFQUNWLG9CQUFvQixDQUFDLElBQUksQ0FBQztBQUM1QjtBQUNBLENBQUYifQ==