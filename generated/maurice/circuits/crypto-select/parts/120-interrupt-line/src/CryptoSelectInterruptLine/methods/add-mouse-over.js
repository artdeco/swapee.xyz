/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._addMouseOver}*/
export default
function addMouseOver(ev) {
 // console.log('mouse over',1)
 const{
  asICryptoSelectTouchscreen: {
   asICryptoSelectDisplay: {
    CryptoMenu:CryptoMenu,
   },
   // asICryptoSelect:{CryptoMenu:CryptoMenu},
  },
 }=this
 if(ev.target==CryptoMenu||CryptoMenu.contains(/** @type {Node}*/(ev.target))) {
  return
 }
 return {
  isMouseOver:true,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFpekJHLFNBQVMsWUFBWSxDQUFDO0NBQ3JCLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEtBQUssQ0FBQztDQUM1QjtFQUNDO0dBQ0M7SUFDQyxxQkFBcUI7QUFDekI7R0FDRyxHQUFHLGlCQUFpQixxQkFBcUI7QUFDNUM7R0FDRztDQUNGLEVBQUUsQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLHNCQUFzQixDQUFDLFFBQVEsbUJBQW1CLENBQUMsRUFBRSxDQUFDO0VBQ25FO0FBQ0Y7Q0FDQztFQUNDLGdCQUFnQjtBQUNsQjtBQUNBLENBQUYifQ==