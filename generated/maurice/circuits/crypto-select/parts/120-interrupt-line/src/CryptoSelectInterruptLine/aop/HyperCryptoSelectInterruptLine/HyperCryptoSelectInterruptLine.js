import AbstractHyperCryptoSelectInterruptLine from '../../../../gen/AbstractCryptoSelectInterruptLine/hyper/AbstractHyperCryptoSelectInterruptLine'
import CryptoSelectInterruptLine from '../../CryptoSelectInterruptLine'
import CryptoSelectInterruptLineGeneralAspects from '../CryptoSelectInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperCryptoSelectInterruptLine} */
export default class extends AbstractHyperCryptoSelectInterruptLine
 .consults(
  CryptoSelectInterruptLineGeneralAspects,
 )
 .implements(
  CryptoSelectInterruptLine,
 )
{}