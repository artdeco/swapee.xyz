/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._menuMouseMv}*/
export default
function menuMouseMv(ev) {
 const{
  asICryptoSelectTouchscreen:{
   asICryptoSelectDisplay:{
    resolveMouseItem:resolveMouseItem,
    resolveItemIndex:resolveItemIndex,
   },
  },
 }=this
 const item=resolveMouseItem(ev)
 if(!item) return

 // convert to background stalk
 const{left:left,top:top}=item.getBoundingClientRect()
 const x=ev.clientX-left
 const y=ev.clientY-top
 item.style.setProperty('--x', x + 'px')
 item.style.setProperty('--y', y + 'px')

 const itemIndex=resolveItemIndex(item)

 return{
  hoveringIndex:itemIndex,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUF3eEJHLFNBQVMsV0FBVyxDQUFDO0NBQ3BCO0VBQ0M7R0FDQztJQUNDLGlDQUFpQztJQUNqQyxpQ0FBaUM7QUFDckM7QUFDQTtHQUNHO0NBQ0YsTUFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7Q0FDNUIsRUFBRSxDQUFDLE9BQU87O0NBRVYsR0FBRyxRQUFRLEdBQUcsV0FBVztDQUN6QixNQUFNLFNBQVMsQ0FBQyxTQUFTLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztDQUNwRCxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUM7Q0FDWCxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUM7Q0FDWCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFO0NBQ2xDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxFQUFFLEVBQUU7O0NBRWxDLE1BQU0sU0FBUyxDQUFDLGdCQUFnQixDQUFDOztDQUVqQztFQUNDLHVCQUF1QjtBQUN6QjtBQUNBLENBQUYifQ==