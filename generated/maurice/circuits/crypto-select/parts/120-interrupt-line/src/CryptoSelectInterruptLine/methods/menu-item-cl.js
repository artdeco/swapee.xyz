/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._menuItemCl}*/
export default
function menuItemCl(ev) {
 const{
  asICryptoSelectTouchscreen: {
   asICryptoSelectDisplay:{
    resolveMouseItem:resolveMouseItem,
   },
  },
 }=this
 const item=resolveMouseItem(ev)
 if(!item) return

 // console.log('selecting', item.dataset['value'])
 // const imHtml=item.querySelector(`.${ImgWr}`).innerHTML
 const selected=item.dataset['value']
 return {
  selected: selected,
  menuExpanded: false,
 //  imHtml:   imHtml,
 }
 // and now???
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUE0MkJHLFNBQVMsVUFBVSxDQUFDO0NBQ25CO0VBQ0M7R0FDQztJQUNDLGlDQUFpQztBQUNyQztBQUNBO0dBQ0c7Q0FDRixNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztDQUM1QixFQUFFLENBQUMsT0FBTzs7Q0FFVixHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQztDQUNqQyxHQUFHLE1BQU0sTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUUsQ0FBQztDQUNoRCxNQUFNLFFBQVEsQ0FBQyxJQUFJLENBQUM7Q0FDcEI7RUFDQyxVQUFVLFFBQVE7RUFDbEIsY0FBYyxLQUFLO0NBQ3BCLElBQUksVUFBVSxNQUFNO0FBQ3JCO0NBQ0MsR0FBRyxJQUFJO0FBQ1IsQ0FBRiJ9