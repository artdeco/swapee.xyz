/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._onInputFocus}*/
export default
function onInputFocus() {
 return {
  isSearching: true,
  menuExpanded:true,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUF1MEJHLFNBQVMsWUFBWSxDQUFDO0NBQ3JCO0VBQ0MsYUFBYSxJQUFJO0VBQ2pCLGlCQUFpQjtBQUNuQjtBQUNBLENBQUYifQ==