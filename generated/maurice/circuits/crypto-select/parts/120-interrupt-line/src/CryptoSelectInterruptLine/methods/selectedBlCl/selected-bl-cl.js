import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectInterruptLine._selectedBlCl} */
export default function selectedBlCl(ev) {
 const{
  asICryptoSelectTouchscreen:{
   isBlurringInput:isBlurringInput,
   isMenuExpanded:isMenuExpanded,
   flipMenuExpanded:flipMenuExpanded,
   asICryptoSelectDisplay:{
    CryptoSelectedNameIn:CryptoSelectedNameIn,
   },
  },
 }=this
 const{target:target}=ev
 if(target==CryptoSelectedNameIn) return
 if(isBlurringInput) return

 if(document.activeElement==CryptoSelectedNameIn&&isMenuExpanded) return
 flipMenuExpanded()
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUE2MUJHLFNBQVMsWUFBWSxDQUFDO0NBQ3JCO0VBQ0M7R0FDQywrQkFBK0I7R0FDL0IsNkJBQTZCO0dBQzdCLGlDQUFpQztHQUNqQztJQUNDLHlDQUF5QztBQUM3QztBQUNBO0dBQ0c7Q0FDRixNQUFNLGVBQWU7Q0FDckIsRUFBRSxDQUFDLE1BQU0sRUFBRSxzQkFBc0I7Q0FDakMsRUFBRSxDQUFDLGlCQUFpQjs7Q0FFcEIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsc0NBQXNDO0NBQ2pFLGdCQUFnQixDQUFDO0FBQ2xCLENBQUYifQ==