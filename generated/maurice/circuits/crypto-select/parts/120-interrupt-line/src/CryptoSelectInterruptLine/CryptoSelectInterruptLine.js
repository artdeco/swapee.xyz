import AbstractCryptoSelectInterruptLine from '../../gen/AbstractCryptoSelectInterruptLine'
import menuMouseMv from './methods/menuMouseMv/menu-mouse-mv'
import addMouseOver from './methods/addMouseOver/add-mouse-over'
import onMouseLeave from './methods/onMouseLeave/on-mouse-leave'
import onInputFocus from './methods/onInputFocus/on-input-focus'
import onInputKeyDown from './methods/onInputKeyDown/on-input-key-down'
import selectedBlCl from './methods/selectedBlCl/selected-bl-cl'
import menuItemCl from './methods/menuItemCl/menu-item-cl'
import inputKeyUp from './methods/inputKeyUp/input-key-up'
import onInputBlur from './methods/onInputBlur/on-input-blur'

/**@extends {xyz.swapee.wc.CryptoSelectInterruptLine} */
export default class extends AbstractCryptoSelectInterruptLine.implements(
 AbstractCryptoSelectInterruptLine.class.prototype=/**@type {!xyz.swapee.wc.CryptoSelectInterruptLine}*/({
  menuMouseMv:menuMouseMv,
  addMouseOver:addMouseOver,
  onMouseLeave:onMouseLeave,
  onInputFocus:onInputFocus,
  onInputKeyDown:onInputKeyDown,
  selectedBlCl:selectedBlCl,
  menuItemCl:menuItemCl,
  inputKeyUp:inputKeyUp,
  onInputBlur:onInputBlur,
 }),
){}