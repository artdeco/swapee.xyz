import AbstractCryptoSelectInterruptLineAspects from '../../../../gen/AbstractCryptoSelectInterruptLine/aspects/AbstractCryptoSelectInterruptLineAspects'
import {SetInputs} from '@webcircuits/front'

/**@extends {xyz.swapee.wc.CryptoSelectInterruptLineAspects} */
export default class extends AbstractCryptoSelectInterruptLineAspects.continues(
 /**@type {!xyz.swapee.wc.CryptoSelectInterruptLineAspects}*/({
  afterMenuMouseMvReturns:SetInputs,
  afterMenuItemClReturns:SetInputs,
  afterInputKeyUpReturns:SetInputs,
  afterInputKeyboardScrollReturns:SetInputs,
  afterSelectedBlClReturns:SetInputs,
  afterOnInputKeyDowReturns:SetInputs,
  afterOnInputBlurReturns:SetInputs,
  afterOnInputFocusReturns:SetInputs,
  afterOnMouseLeaveReturns:SetInputs,
  afterOnInputKeyDownReturns:SetInputs,
  afterAddMouseOverReturns:SetInputs,
 }),
){}