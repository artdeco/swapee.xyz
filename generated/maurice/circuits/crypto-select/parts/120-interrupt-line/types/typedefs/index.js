/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ICryptoSelectInterruptLine={}
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel={}
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller={}
xyz.swapee.wc.ICryptoSelectInterruptLineAspects={}
xyz.swapee.wc.IHyperCryptoSelectInterruptLine={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.CryptoSelectInterruptLine)} xyz.swapee.wc.AbstractCryptoSelectInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.CryptoSelectInterruptLine} xyz.swapee.wc.CryptoSelectInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelectInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractCryptoSelectInterruptLine
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractCryptoSelectInterruptLine.constructor&xyz.swapee.wc.CryptoSelectInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractCryptoSelectInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeMenuMouseMv=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuMouseMv|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuMouseMv>} */ (void 0)
    /**
     * After the method.
     */
    this.afterMenuMouseMv=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMv|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMv>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterMenuMouseMvThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterMenuMouseMvReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterMenuMouseMvCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeMenuItemCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuItemCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuItemCl>} */ (void 0)
    /**
     * After the method.
     */
    this.afterMenuItemCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemCl>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterMenuItemClThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterMenuItemClReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterMenuItemClCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeInputKeyUp=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyUp|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyUp>} */ (void 0)
    /**
     * After the method.
     */
    this.afterInputKeyUp=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUp|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUp>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterInputKeyUpThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterInputKeyUpReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterInputKeyUpCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeInputKeyboardScroll=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyboardScroll|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyboardScroll>} */ (void 0)
    /**
     * After the method.
     */
    this.afterInputKeyboardScroll=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScroll|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScroll>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterInputKeyboardScrollThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterInputKeyboardScrollReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterInputKeyboardScrollCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSelectedBlCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeSelectedBlCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeSelectedBlCl>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSelectedBlCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlCl>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSelectedBlClThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSelectedBlClReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSelectedBlClCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputKeyDow=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDow|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDow>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputKeyDow=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDow|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDow>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputKeyDowThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputKeyDowReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputKeyDowCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputBlur=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputBlur>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputBlur=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlur>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputBlurThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputBlurReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputBlurCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputFocus=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputFocus>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputFocus=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocus>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputFocusThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputFocusReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputFocusCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnMouseLeave=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnMouseLeave|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnMouseLeave>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnMouseLeave=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeave|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeave>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnMouseLeaveThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnMouseLeaveReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnMouseLeaveCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputKeyDown=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDown|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDown>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputKeyDown=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDown|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDown>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputKeyDownThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputKeyDownReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputKeyDownCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAddMouseOver=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeAddMouseOver|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeAddMouseOver>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAddMouseOver=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOver|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOver>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAddMouseOverThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAddMouseOverReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAddMouseOverCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverCancels>} */ (void 0)
  }
}
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice

/**
 * A concrete class of _ICryptoSelectInterruptLineJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice = class extends xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice { }
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeMenuMouseMv=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterMenuMouseMv=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterMenuMouseMvThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterMenuMouseMvReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterMenuMouseMvCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeMenuItemCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterMenuItemCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterMenuItemClThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterMenuItemClReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterMenuItemClCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeInputKeyUp=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterInputKeyUp=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterInputKeyUpThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterInputKeyUpReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterInputKeyUpCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeInputKeyboardScroll=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterInputKeyboardScroll=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterInputKeyboardScrollThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterInputKeyboardScrollReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterInputKeyboardScrollCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSelectedBlCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSelectedBlCl=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSelectedBlClThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSelectedBlClReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSelectedBlClCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputKeyDow=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputKeyDow=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputKeyDowThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputKeyDowReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputKeyDowCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputBlur=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputBlur=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputBlurThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputBlurReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputBlurCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputFocus=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputFocus=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputFocusThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputFocusReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputFocusCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnMouseLeave=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnMouseLeave=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnMouseLeaveThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnMouseLeaveReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnMouseLeaveCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeOnInputKeyDown=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterOnInputKeyDown=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterOnInputKeyDownThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterOnInputKeyDownReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterOnInputKeyDownCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAddMouseOver=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAddMouseOver=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAddMouseOverThrows=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAddMouseOverReturns=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAddMouseOverCancels=/** @type {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice

/**
 * A concrete class of _ICryptoSelectInterruptLineJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice = class extends xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice { }
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `ICryptoSelectInterruptLine`'s methods.
 * @interface xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel = class { }
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuMouseMv} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeMenuMouseMv = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMv} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMv = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMvThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMvReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMvCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuItemCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeMenuItemCl = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemCl = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemClThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemClReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemClCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyUp} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeInputKeyUp = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUp} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUp = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUpThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUpReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUpCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyboardScroll} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeInputKeyboardScroll = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScroll} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScroll = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScrollThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScrollReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScrollCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeSelectedBlCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeSelectedBlCl = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlCl = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlClThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlClReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlClCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDow} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputKeyDow = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDow} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDow = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDowThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDowReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDowCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputBlur} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputBlur = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlur} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlur = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlurThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlurReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlurCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputFocus} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputFocus = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocus} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocus = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocusThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocusReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocusCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnMouseLeave} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnMouseLeave = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeave} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeave = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeaveThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeaveReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeaveCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDown} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputKeyDown = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDown} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDown = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDownThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDownReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDownCancels = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeAddMouseOver} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeAddMouseOver = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOver} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOver = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOverThrows = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOverReturns = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOverCancels = function() {}

/**
 * A concrete class of _ICryptoSelectInterruptLineJoinpointModel_ instances.
 * @constructor xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel} An interface that enumerates the joinpoints of `ICryptoSelectInterruptLine`'s methods.
 */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel = class extends xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel { }
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel.prototype.constructor = xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel} */
xyz.swapee.wc.RecordICryptoSelectInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel} xyz.swapee.wc.BoundICryptoSelectInterruptLineJoinpointModel */

/** @typedef {xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel} xyz.swapee.wc.BoundCryptoSelectInterruptLineJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller)} xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller} xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller` interface.
 * @constructor xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.prototype.constructor = xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.class = /** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese[]) => xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller} xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller */
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeMenuMouseMv=/** @type {number} */ (void 0)
    this.afterMenuMouseMv=/** @type {number} */ (void 0)
    this.afterMenuMouseMvThrows=/** @type {number} */ (void 0)
    this.afterMenuMouseMvReturns=/** @type {number} */ (void 0)
    this.afterMenuMouseMvCancels=/** @type {number} */ (void 0)
    this.beforeMenuItemCl=/** @type {number} */ (void 0)
    this.afterMenuItemCl=/** @type {number} */ (void 0)
    this.afterMenuItemClThrows=/** @type {number} */ (void 0)
    this.afterMenuItemClReturns=/** @type {number} */ (void 0)
    this.afterMenuItemClCancels=/** @type {number} */ (void 0)
    this.beforeInputKeyUp=/** @type {number} */ (void 0)
    this.afterInputKeyUp=/** @type {number} */ (void 0)
    this.afterInputKeyUpThrows=/** @type {number} */ (void 0)
    this.afterInputKeyUpReturns=/** @type {number} */ (void 0)
    this.afterInputKeyUpCancels=/** @type {number} */ (void 0)
    this.beforeInputKeyboardScroll=/** @type {number} */ (void 0)
    this.afterInputKeyboardScroll=/** @type {number} */ (void 0)
    this.afterInputKeyboardScrollThrows=/** @type {number} */ (void 0)
    this.afterInputKeyboardScrollReturns=/** @type {number} */ (void 0)
    this.afterInputKeyboardScrollCancels=/** @type {number} */ (void 0)
    this.beforeSelectedBlCl=/** @type {number} */ (void 0)
    this.afterSelectedBlCl=/** @type {number} */ (void 0)
    this.afterSelectedBlClThrows=/** @type {number} */ (void 0)
    this.afterSelectedBlClReturns=/** @type {number} */ (void 0)
    this.afterSelectedBlClCancels=/** @type {number} */ (void 0)
    this.beforeOnInputKeyDow=/** @type {number} */ (void 0)
    this.afterOnInputKeyDow=/** @type {number} */ (void 0)
    this.afterOnInputKeyDowThrows=/** @type {number} */ (void 0)
    this.afterOnInputKeyDowReturns=/** @type {number} */ (void 0)
    this.afterOnInputKeyDowCancels=/** @type {number} */ (void 0)
    this.beforeOnInputBlur=/** @type {number} */ (void 0)
    this.afterOnInputBlur=/** @type {number} */ (void 0)
    this.afterOnInputBlurThrows=/** @type {number} */ (void 0)
    this.afterOnInputBlurReturns=/** @type {number} */ (void 0)
    this.afterOnInputBlurCancels=/** @type {number} */ (void 0)
    this.beforeOnInputFocus=/** @type {number} */ (void 0)
    this.afterOnInputFocus=/** @type {number} */ (void 0)
    this.afterOnInputFocusThrows=/** @type {number} */ (void 0)
    this.afterOnInputFocusReturns=/** @type {number} */ (void 0)
    this.afterOnInputFocusCancels=/** @type {number} */ (void 0)
    this.beforeOnMouseLeave=/** @type {number} */ (void 0)
    this.afterOnMouseLeave=/** @type {number} */ (void 0)
    this.afterOnMouseLeaveThrows=/** @type {number} */ (void 0)
    this.afterOnMouseLeaveReturns=/** @type {number} */ (void 0)
    this.afterOnMouseLeaveCancels=/** @type {number} */ (void 0)
    this.beforeOnInputKeyDown=/** @type {number} */ (void 0)
    this.afterOnInputKeyDown=/** @type {number} */ (void 0)
    this.afterOnInputKeyDownThrows=/** @type {number} */ (void 0)
    this.afterOnInputKeyDownReturns=/** @type {number} */ (void 0)
    this.afterOnInputKeyDownCancels=/** @type {number} */ (void 0)
    this.beforeAddMouseOver=/** @type {number} */ (void 0)
    this.afterAddMouseOver=/** @type {number} */ (void 0)
    this.afterAddMouseOverThrows=/** @type {number} */ (void 0)
    this.afterAddMouseOverReturns=/** @type {number} */ (void 0)
    this.afterAddMouseOverCancels=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  menuMouseMv() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  menuItemCl() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  inputKeyUp() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  inputKeyboardScroll() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  selectedBlCl() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  onInputKeyDow() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  onInputBlur() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  onInputFocus() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  onMouseLeave() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  onInputKeyDown() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  addMouseOver() { }
}
/**
 * Create a new *ICryptoSelectInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese>)} xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller} xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ICryptoSelectInterruptLineAspectsInstaller_ instances.
 * @constructor xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ICryptoSelectInterruptLineAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ICryptoSelectInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.MenuMouseMvNArgs
 * @prop {!MouseEvent} mev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.MenuMouseMvNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.MenuMouseMvNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `menuMouseMv` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `menuMouseMv` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `menuMouseMv` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.MenuItemClNArgs
 * @prop {!MouseEvent} mev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.MenuItemClNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.MenuItemClNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `menuItemCl` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `menuItemCl` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `menuItemCl` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyUpNArgs
 * @prop {KeyboardEvent} kev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyUpNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyUpNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `inputKeyUp` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `inputKeyUp` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `inputKeyUp` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyboardScrollNArgs
 * @prop {KeyboardEvent} kev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyboardScrollNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyboardScrollNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `inputKeyboardScroll` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `inputKeyboardScroll` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `inputKeyboardScroll` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.SelectedBlClNArgs
 * @prop {MouseEvent} mev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.SelectedBlClNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.SelectedBlClNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `selectedBlCl` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `selectedBlCl` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `selectedBlCl` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDowNArgs
 * @prop {KeyboardEvent} kev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDowNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDowNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `onInputKeyDow` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `onInputKeyDow` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `onInputKeyDow` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `onInputBlur` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `onInputBlur` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `onInputBlur` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `onInputFocus` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `onInputFocus` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `onInputFocus` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.OnMouseLeaveNArgs
 * @prop {MouseEvent} mev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.OnMouseLeaveNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.OnMouseLeaveNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `onMouseLeave` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `onMouseLeave` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `onMouseLeave` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDownNArgs
 * @prop {KeyboardEvent} kev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDownNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDownNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `onInputKeyDown` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `onInputKeyDown` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `onInputKeyDown` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLine.AddMouseOverNArgs
 * @prop {MouseEvent} ev
 */

/**
 * @typedef {Object} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.ICryptoSelectInterruptLine.AddMouseOverNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.ICryptoSelectInterruptLine.AddMouseOverNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `addMouseOver` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => void} sub Cancels a call to `addMouseOver` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `addMouseOver` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData
 * @prop {!xyz.swapee.wc.front.CryptoSelectInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.CryptoSelectInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {function(new: xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster<THIS>&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.wc.BCryptoSelectInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *ICryptoSelectInterruptLine* that bind to an instance.
 * @interface xyz.swapee.wc.BCryptoSelectInterruptLineAspects
 * @template THIS
 */
xyz.swapee.wc.BCryptoSelectInterruptLineAspects = class extends /** @type {xyz.swapee.wc.BCryptoSelectInterruptLineAspects.constructor&xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.wc.BCryptoSelectInterruptLineAspects.prototype.constructor = xyz.swapee.wc.BCryptoSelectInterruptLineAspects

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.CryptoSelectInterruptLineAspects)} xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects} xyz.swapee.wc.CryptoSelectInterruptLineAspects.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ICryptoSelectInterruptLineAspects` interface.
 * @constructor xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects = class extends /** @type {xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.constructor&xyz.swapee.wc.CryptoSelectInterruptLineAspects.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.prototype.constructor = xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.class = /** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects)|(!xyz.swapee.wc.BCryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.BCryptoSelectInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects)|(!xyz.swapee.wc.BCryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.BCryptoSelectInterruptLineAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects)|(!xyz.swapee.wc.BCryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.BCryptoSelectInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese[]) => xyz.swapee.wc.ICryptoSelectInterruptLineAspects} xyz.swapee.wc.CryptoSelectInterruptLineAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster&xyz.swapee.wc.BCryptoSelectInterruptLineAspects<!xyz.swapee.wc.ICryptoSelectInterruptLineAspects>)} xyz.swapee.wc.ICryptoSelectInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.BCryptoSelectInterruptLineAspects} xyz.swapee.wc.BCryptoSelectInterruptLineAspects.typeof */
/**
 * The aspects of the *ICryptoSelectInterruptLine*.
 * @interface xyz.swapee.wc.ICryptoSelectInterruptLineAspects
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspects = class extends /** @type {xyz.swapee.wc.ICryptoSelectInterruptLineAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.BCryptoSelectInterruptLineAspects.typeof} */ (class {}) {
}
/**
 * Create a new *ICryptoSelectInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspects&engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese>)} xyz.swapee.wc.CryptoSelectInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineAspects} xyz.swapee.wc.ICryptoSelectInterruptLineAspects.typeof */
/**
 * A concrete class of _ICryptoSelectInterruptLineAspects_ instances.
 * @constructor xyz.swapee.wc.CryptoSelectInterruptLineAspects
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineAspects} The aspects of the *ICryptoSelectInterruptLine*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese>} ‎
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspects = class extends /** @type {xyz.swapee.wc.CryptoSelectInterruptLineAspects.constructor&xyz.swapee.wc.ICryptoSelectInterruptLineAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ICryptoSelectInterruptLineAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ICryptoSelectInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BCryptoSelectInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster
 * @template THIS
 */
xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster = class { }
/**
 * Cast the _BCryptoSelectInterruptLineAspects_ instance into the _BoundICryptoSelectInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundICryptoSelectInterruptLine}
 */
xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster.prototype.asICryptoSelectInterruptLine

/**
 * Contains getters to cast the _ICryptoSelectInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster = class { }
/**
 * Cast the _ICryptoSelectInterruptLineAspects_ instance into the _BoundICryptoSelectInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundICryptoSelectInterruptLine}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster.prototype.asICryptoSelectInterruptLine

/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese} xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.HyperCryptoSelectInterruptLine)} xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine} xyz.swapee.wc.HyperCryptoSelectInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IHyperCryptoSelectInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.constructor&xyz.swapee.wc.HyperCryptoSelectInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IHyperCryptoSelectInterruptLine|typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IHyperCryptoSelectInterruptLine|typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IHyperCryptoSelectInterruptLine|typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspects)} aides The list of aides that advise the ICryptoSelectInterruptLine to implement aspects.
 * @return {typeof xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese[]) => xyz.swapee.wc.IHyperCryptoSelectInterruptLine} xyz.swapee.wc.HyperCryptoSelectInterruptLineConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster&xyz.swapee.wc.ICryptoSelectInterruptLine)} xyz.swapee.wc.IHyperCryptoSelectInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine} xyz.swapee.wc.ICryptoSelectInterruptLine.typeof */
/** @interface xyz.swapee.wc.IHyperCryptoSelectInterruptLine */
xyz.swapee.wc.IHyperCryptoSelectInterruptLine = class extends /** @type {xyz.swapee.wc.IHyperCryptoSelectInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ICryptoSelectInterruptLine.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperCryptoSelectInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IHyperCryptoSelectInterruptLine.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IHyperCryptoSelectInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese>)} xyz.swapee.wc.HyperCryptoSelectInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IHyperCryptoSelectInterruptLine} xyz.swapee.wc.IHyperCryptoSelectInterruptLine.typeof */
/**
 * A concrete class of _IHyperCryptoSelectInterruptLine_ instances.
 * @constructor xyz.swapee.wc.HyperCryptoSelectInterruptLine
 * @implements {xyz.swapee.wc.IHyperCryptoSelectInterruptLine} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.HyperCryptoSelectInterruptLine = class extends /** @type {xyz.swapee.wc.HyperCryptoSelectInterruptLine.constructor&xyz.swapee.wc.IHyperCryptoSelectInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperCryptoSelectInterruptLine* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperCryptoSelectInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.HyperCryptoSelectInterruptLine.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.HyperCryptoSelectInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IHyperCryptoSelectInterruptLine} */
xyz.swapee.wc.RecordIHyperCryptoSelectInterruptLine

/** @typedef {xyz.swapee.wc.IHyperCryptoSelectInterruptLine} xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine */

/** @typedef {xyz.swapee.wc.HyperCryptoSelectInterruptLine} xyz.swapee.wc.BoundHyperCryptoSelectInterruptLine */

/**
 * Contains getters to cast the _IHyperCryptoSelectInterruptLine_ interface.
 * @interface xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster
 */
xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster = class { }
/**
 * Cast the _IHyperCryptoSelectInterruptLine_ instance into the _BoundIHyperCryptoSelectInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster.prototype.asIHyperCryptoSelectInterruptLine
/**
 * Access the _HyperCryptoSelectInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundHyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster.prototype.superHyperCryptoSelectInterruptLine

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ICryptoSelectInterruptLineCaster&xyz.swapee.wc.front.ICryptoSelectController)} xyz.swapee.wc.ICryptoSelectInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ICryptoSelectController} xyz.swapee.wc.front.ICryptoSelectController.typeof */
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @interface xyz.swapee.wc.ICryptoSelectInterruptLine
 */
xyz.swapee.wc.ICryptoSelectInterruptLine = class extends /** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ICryptoSelectController.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.menuMouseMv} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.menuMouseMv = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.menuItemCl} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.menuItemCl = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyUp} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.inputKeyUp = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyboardScroll} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.inputKeyboardScroll = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.selectedBlCl} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.selectedBlCl = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDow} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputKeyDow = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.onInputBlur} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputBlur = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.onInputFocus} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputFocus = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.onMouseLeave} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onMouseLeave = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDown} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputKeyDown = function() {}
/** @type {xyz.swapee.wc.ICryptoSelectInterruptLine.addMouseOver} */
xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.addMouseOver = function() {}

/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese>)} xyz.swapee.wc.CryptoSelectInterruptLine.constructor */
/**
 * A concrete class of _ICryptoSelectInterruptLine_ instances.
 * @constructor xyz.swapee.wc.CryptoSelectInterruptLine
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLine} Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.CryptoSelectInterruptLine = class extends /** @type {xyz.swapee.wc.CryptoSelectInterruptLine.constructor&xyz.swapee.wc.ICryptoSelectInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.CryptoSelectInterruptLine.prototype.constructor = xyz.swapee.wc.CryptoSelectInterruptLine
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.CryptoSelectInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine} */
xyz.swapee.wc.RecordICryptoSelectInterruptLine

/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine} xyz.swapee.wc.BoundICryptoSelectInterruptLine */

/** @typedef {xyz.swapee.wc.CryptoSelectInterruptLine} xyz.swapee.wc.BoundCryptoSelectInterruptLine */

/**
 * Contains getters to cast the _ICryptoSelectInterruptLine_ interface.
 * @interface xyz.swapee.wc.ICryptoSelectInterruptLineCaster
 */
xyz.swapee.wc.ICryptoSelectInterruptLineCaster = class { }
/**
 * Cast the _ICryptoSelectInterruptLine_ instance into the _BoundICryptoSelectInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundICryptoSelectInterruptLine}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineCaster.prototype.asICryptoSelectInterruptLine
/**
 * Cast the _ICryptoSelectInterruptLine_ instance into the _.BoundICryptoSelectTouchscreen_ type.
 * @type {!xyz.swapee.wc.BoundICryptoSelectTouchscreen}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineCaster.prototype.asICryptoSelectTouchscreen
/**
 * Access the _CryptoSelectInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundCryptoSelectInterruptLine}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineCaster.prototype.superCryptoSelectInterruptLine

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuMouseMv */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuMouseMv} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData} [data] Metadata passed to the pointcuts of _menuMouseMv_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.MenuMouseMvNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `menuMouseMv` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `menuMouseMv` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuMouseMvNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuMouseMv = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMv */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMv} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData} [data] Metadata passed to the pointcuts of _menuMouseMv_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuMouseMvNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMv = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData} [data] Metadata passed to the pointcuts of _menuMouseMv_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `menuMouseMv` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuMouseMvNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData} [data] Metadata passed to the pointcuts of _menuMouseMv_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuMouseMvNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData} [data] Metadata passed to the pointcuts of _menuMouseMv_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuMouseMvNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuItemCl */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuItemCl} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData} [data] Metadata passed to the pointcuts of _menuItemCl_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.MenuItemClNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `menuItemCl` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `menuItemCl` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuItemClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuItemCl = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemCl */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemCl} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData} [data] Metadata passed to the pointcuts of _menuItemCl_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuItemClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemCl = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData} [data] Metadata passed to the pointcuts of _menuItemCl_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `menuItemCl` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuItemClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData} [data] Metadata passed to the pointcuts of _menuItemCl_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuItemClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData} [data] Metadata passed to the pointcuts of _menuItemCl_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.MenuItemClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyUp */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyUp} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData} [data] Metadata passed to the pointcuts of _inputKeyUp_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.InputKeyUpNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `inputKeyUp` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `inputKeyUp` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyUpNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyUp = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUp */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUp} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData} [data] Metadata passed to the pointcuts of _inputKeyUp_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyUpNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUp = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData} [data] Metadata passed to the pointcuts of _inputKeyUp_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `inputKeyUp` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyUpNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData} [data] Metadata passed to the pointcuts of _inputKeyUp_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyUpNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData} [data] Metadata passed to the pointcuts of _inputKeyUp_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyUpNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyboardScroll */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyboardScroll} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData} [data] Metadata passed to the pointcuts of _inputKeyboardScroll_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.InputKeyboardScrollNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `inputKeyboardScroll` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `inputKeyboardScroll` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyboardScrollNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyboardScroll = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScroll */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScroll} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData} [data] Metadata passed to the pointcuts of _inputKeyboardScroll_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyboardScrollNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScroll = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData} [data] Metadata passed to the pointcuts of _inputKeyboardScroll_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `inputKeyboardScroll` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyboardScrollNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData} [data] Metadata passed to the pointcuts of _inputKeyboardScroll_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyboardScrollNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData} [data] Metadata passed to the pointcuts of _inputKeyboardScroll_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `args` _ICryptoSelectInterruptLine.InputKeyboardScrollNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeSelectedBlCl */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeSelectedBlCl} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData} [data] Metadata passed to the pointcuts of _selectedBlCl_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.SelectedBlClNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `selectedBlCl` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `selectedBlCl` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.SelectedBlClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeSelectedBlCl = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlCl */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlCl} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData} [data] Metadata passed to the pointcuts of _selectedBlCl_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.SelectedBlClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlCl = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData} [data] Metadata passed to the pointcuts of _selectedBlCl_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `selectedBlCl` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.SelectedBlClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData} [data] Metadata passed to the pointcuts of _selectedBlCl_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.SelectedBlClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData} [data] Metadata passed to the pointcuts of _selectedBlCl_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `args` _ICryptoSelectInterruptLine.SelectedBlClNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDow */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDow} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDow_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.OnInputKeyDowNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `onInputKeyDow` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `onInputKeyDow` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDowNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDow = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDow */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDow} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDow_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDowNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDow = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDow_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `onInputKeyDow` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDowNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDow_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDowNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDow_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDowNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputBlur */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputBlur} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData} [data] Metadata passed to the pointcuts of _onInputBlur_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `onInputBlur` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `onInputBlur` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputBlur = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlur */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlur} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData} [data] Metadata passed to the pointcuts of _onInputBlur_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlur = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData} [data] Metadata passed to the pointcuts of _onInputBlur_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `onInputBlur` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData} [data] Metadata passed to the pointcuts of _onInputBlur_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData} [data] Metadata passed to the pointcuts of _onInputBlur_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputFocus */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputFocus} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData} [data] Metadata passed to the pointcuts of _onInputFocus_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `onInputFocus` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `onInputFocus` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputFocus = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocus */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocus} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData} [data] Metadata passed to the pointcuts of _onInputFocus_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocus = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData} [data] Metadata passed to the pointcuts of _onInputFocus_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `onInputFocus` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData} [data] Metadata passed to the pointcuts of _onInputFocus_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData} [data] Metadata passed to the pointcuts of _onInputFocus_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnMouseLeave */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnMouseLeave} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData} [data] Metadata passed to the pointcuts of _onMouseLeave_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.OnMouseLeaveNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `onMouseLeave` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `onMouseLeave` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnMouseLeaveNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnMouseLeave = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeave */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeave} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData} [data] Metadata passed to the pointcuts of _onMouseLeave_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnMouseLeaveNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeave = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData} [data] Metadata passed to the pointcuts of _onMouseLeave_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `onMouseLeave` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnMouseLeaveNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData} [data] Metadata passed to the pointcuts of _onMouseLeave_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnMouseLeaveNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData} [data] Metadata passed to the pointcuts of _onMouseLeave_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnMouseLeaveNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDown */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDown} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDown_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.OnInputKeyDownNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `onInputKeyDown` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `onInputKeyDown` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDownNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDown = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDown */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDown} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDown_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDownNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDown = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDown_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `onInputKeyDown` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDownNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDown_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDownNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData} [data] Metadata passed to the pointcuts of _onInputKeyDown_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `args` _ICryptoSelectInterruptLine.OnInputKeyDownNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeAddMouseOver */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeAddMouseOver} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData} [data] Metadata passed to the pointcuts of _addMouseOver_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: ICryptoSelectInterruptLine.AddMouseOverNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `addMouseOver` method from being executed.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; void_ Cancels a call to `addMouseOver` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `args` _ICryptoSelectInterruptLine.AddMouseOverNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeAddMouseOver = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOver */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOver} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData} [data] Metadata passed to the pointcuts of _addMouseOver_ at the `after` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `args` _ICryptoSelectInterruptLine.AddMouseOverNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOver = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverThrows */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData} [data] Metadata passed to the pointcuts of _addMouseOver_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `addMouseOver` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `args` _ICryptoSelectInterruptLine.AddMouseOverNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverReturns */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData} [data] Metadata passed to the pointcuts of _addMouseOver_ at the `afterReturns` joinpoint.
 * - `res` _!front.CryptoSelectInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.CryptoSelectInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `args` _ICryptoSelectInterruptLine.AddMouseOverNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData) => void} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel>} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverCancels */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData} [data] Metadata passed to the pointcuts of _addMouseOver_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `args` _ICryptoSelectInterruptLine.AddMouseOverNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData*
 * @return {void}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverCancels = function(data) {}

/**
 * @typedef {(this: THIS, mev: !MouseEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__menuMouseMv
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__menuMouseMv<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._menuMouseMv */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.menuMouseMv} */
/**
 * When the mouse moves over the drop down list, we will figure out which one of
 * the items on the list is currently hovered over for the gradient stalking effect.
 * @param {!MouseEvent} mev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.menuMouseMv = function(mev) {}

/**
 * @typedef {(this: THIS, mev: !MouseEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__menuItemCl
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__menuItemCl<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._menuItemCl */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.menuItemCl} */
/**
 * When the drop down is clicked, figure out exactly which drop item was clicked,
 * and fire a pulse with the option info.
 * @param {!MouseEvent} mev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.menuItemCl = function(mev) {}

/**
 * @typedef {(this: THIS, kev: KeyboardEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyUp
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyUp<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._inputKeyUp */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyUp} */
/**
 * The user has entered something.
 * @param {KeyboardEvent} kev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyUp = function(kev) {}

/**
 * @typedef {(this: THIS, kev: KeyboardEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyboardScroll
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyboardScroll<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._inputKeyboardScroll */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyboardScroll} */
/**
 * When user presses either the up or down arrow keys so that the choice of the
 * items in the crypto-select block can be rotated; or enter to confirm the
 * selection, (or esc to exit the menu and search?)
 * @param {KeyboardEvent} kev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyboardScroll = function(kev) {}

/**
 * @typedef {(this: THIS, mev: MouseEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__selectedBlCl
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__selectedBlCl<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._selectedBlCl */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.selectedBlCl} */
/**
 * When the user clicks on the "currently selected" block to expand/collapse
 * the drop-down.
 * @param {MouseEvent} mev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.selectedBlCl = function(mev) {}

/**
 * @typedef {(this: THIS, kev: KeyboardEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDow
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDow<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._onInputKeyDow */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDow} */
/**
 * When the input is clicked to activate search.
 * @param {KeyboardEvent} kev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDow = function(kev) {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputBlur<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._onInputBlur */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.onInputBlur} */
/**
 * When the input blurs.
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputBlur = function() {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputFocus<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._onInputFocus */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.onInputFocus} */
/**
 * When the input focuses.
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputFocus = function() {}

/**
 * @typedef {(this: THIS, mev: MouseEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__onMouseLeave
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__onMouseLeave<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._onMouseLeave */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.onMouseLeave} */
/**
 * When the dropdown has lost the mouse over property and needs to reset the
 * hover index through props.
 * @param {MouseEvent} mev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.onMouseLeave = function(mev) {}

/**
 * @typedef {(this: THIS, kev: KeyboardEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDown
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDown<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._onInputKeyDown */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDown} */
/**
 * When the key is pressed inside the input, currently just handling `ESC` key
 * to remove focus.
 * @param {KeyboardEvent} kev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDown = function(kev) {}

/**
 * @typedef {(this: THIS, ev: MouseEvent) => (void|!xyz.swapee.wc.front.CryptoSelectInputs)} xyz.swapee.wc.ICryptoSelectInterruptLine.__addMouseOver
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ICryptoSelectInterruptLine.__addMouseOver<!xyz.swapee.wc.ICryptoSelectInterruptLine>} xyz.swapee.wc.ICryptoSelectInterruptLine._addMouseOver */
/** @typedef {typeof xyz.swapee.wc.ICryptoSelectInterruptLine.addMouseOver} */
/**
 * Sets the `isMouseOver` to true, which is reset back by the reset regulator
 * in N ms.
 * @param {MouseEvent} ev
 * @return {void|!xyz.swapee.wc.front.CryptoSelectInputs} The inputs to update on the port controller.
 */
xyz.swapee.wc.ICryptoSelectInterruptLine.addMouseOver = function(ev) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc.ICryptoSelectInterruptLine
/* @typal-end */