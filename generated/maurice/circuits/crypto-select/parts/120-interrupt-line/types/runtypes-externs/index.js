/** @const {?} */ $xyz.swapee.wc.ICryptoSelectInterruptLine
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller
/** @const {?} */ $xyz.swapee.wc.ICryptoSelectInterruptLineAspects
/** @const {?} */ $xyz.swapee.wc.IHyperCryptoSelectInterruptLine
/** @const {?} */ xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/** @const {?} */ xyz.swapee.wc.ICryptoSelectInterruptLine
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @record */
$xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese} */
xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineCaster filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectInterruptLine} */
$xyz.swapee.wc.ICryptoSelectInterruptLineCaster.prototype.asICryptoSelectInterruptLine
/** @type {!xyz.swapee.wc.BoundICryptoSelectTouchscreen} */
$xyz.swapee.wc.ICryptoSelectInterruptLineCaster.prototype.asICryptoSelectTouchscreen
/** @type {!xyz.swapee.wc.BoundCryptoSelectInterruptLine} */
$xyz.swapee.wc.ICryptoSelectInterruptLineCaster.prototype.superCryptoSelectInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLineCaster}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.ICryptoSelectController}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine = function() {}
/**
 * @param {!MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.menuMouseMv = function(mev) {}
/**
 * @param {!MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.menuItemCl = function(mev) {}
/**
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.inputKeyUp = function(kev) {}
/**
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.inputKeyboardScroll = function(kev) {}
/**
 * @param {MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.selectedBlCl = function(mev) {}
/**
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputKeyDow = function(kev) {}
/** @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputBlur = function() {}
/** @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputFocus = function() {}
/**
 * @param {MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onMouseLeave = function(mev) {}
/**
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.onInputKeyDown = function(kev) {}
/**
 * @param {MouseEvent} ev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.prototype.addMouseOver = function(ev) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLine}
 */
xyz.swapee.wc.ICryptoSelectInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.CryptoSelectInterruptLine
/** @type {function(new: xyz.swapee.wc.ICryptoSelectInterruptLine)} */
xyz.swapee.wc.CryptoSelectInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.CryptoSelectInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.AbstractCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.CryptoSelectInterruptLine}
 */
$xyz.swapee.wc.AbstractCryptoSelectInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectInterruptLine)} */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine)|(!xyz.swapee.wc.front.ICryptoSelectController|typeof xyz.swapee.wc.front.CryptoSelectController))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLine.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuMouseMv|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuMouseMv>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeMenuMouseMv
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMv|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMv>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuMouseMv
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuMouseMvThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuMouseMvReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuMouseMvCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuItemCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuItemCl>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeMenuItemCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemCl>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuItemCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuItemClThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuItemClReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterMenuItemClCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyUp|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyUp>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeInputKeyUp
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUp|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUp>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyUp
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyUpThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyUpReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyUpCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyboardScroll|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyboardScroll>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeInputKeyboardScroll
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScroll|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScroll>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyboardScroll
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyboardScrollThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyboardScrollReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterInputKeyboardScrollCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeSelectedBlCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeSelectedBlCl>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeSelectedBlCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlCl|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlCl>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterSelectedBlCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterSelectedBlClThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterSelectedBlClReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterSelectedBlClCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDow|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDow>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeOnInputKeyDow
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDow|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDow>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDow
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDowThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDowReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDowCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputBlur>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeOnInputBlur
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlur>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputBlur
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputBlurThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputBlurReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputBlurCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputFocus>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeOnInputFocus
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocus>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputFocus
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputFocusThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputFocusReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputFocusCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnMouseLeave|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnMouseLeave>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeOnMouseLeave
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeave|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeave>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnMouseLeave
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnMouseLeaveThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnMouseLeaveReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnMouseLeaveCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDown|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDown>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeOnInputKeyDown
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDown|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDown>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDown
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDownThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDownReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterOnInputKeyDownCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeAddMouseOver|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeAddMouseOver>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.beforeAddMouseOver
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOver|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOver>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterAddMouseOver
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverThrows>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterAddMouseOverThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverReturns>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterAddMouseOverReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverCancels>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice.prototype.afterAddMouseOverCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice}
 */
$xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice
/** @type {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice)} */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeMenuMouseMv
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuMouseMv
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuMouseMvThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuMouseMvReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuMouseMvCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeMenuItemCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuItemCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuItemClThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuItemClReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterMenuItemClCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeInputKeyUp
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyUp
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyUpThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyUpReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyUpCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeInputKeyboardScroll
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyboardScroll
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyboardScrollThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyboardScrollReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterInputKeyboardScrollCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeSelectedBlCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterSelectedBlCl
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterSelectedBlClThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterSelectedBlClReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterSelectedBlClCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeOnInputKeyDow
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDow
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDowThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDowReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDowCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeOnInputBlur
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputBlur
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputBlurThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputBlurReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputBlurCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeOnInputFocus
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputFocus
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputFocusThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputFocusReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputFocusCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeOnMouseLeave
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnMouseLeave
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnMouseLeaveThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnMouseLeaveReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnMouseLeaveCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeOnInputKeyDown
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDown
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDownThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDownReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterOnInputKeyDownCancels
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.beforeAddMouseOver
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterAddMouseOver
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterAddMouseOverThrows
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterAddMouseOverReturns
/** @type {(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels<THIS>>)} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.afterAddMouseOverCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @template THIS
 * @extends {$xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice
/** @type {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>)} */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModelBindingHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeMenuMouseMv = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMv = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMvThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMvReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuMouseMvCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeMenuItemCl = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemCl = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemClThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemClReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterMenuItemClCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeInputKeyUp = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUp = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUpThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUpReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyUpCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeInputKeyboardScroll = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScroll = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScrollThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScrollReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterInputKeyboardScrollCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeSelectedBlCl = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlCl = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlClThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlClReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterSelectedBlClCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputKeyDow = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDow = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDowThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDowReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDowCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputBlur = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlur = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlurThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlurReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputBlurCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputFocus = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocus = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocusThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocusReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputFocusCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnMouseLeave = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeave = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeaveThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeaveReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnMouseLeaveCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeOnInputKeyDown = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDown = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDownThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDownReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterOnInputKeyDownCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.beforeAddMouseOver = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOver = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOverThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOverReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.prototype.afterAddMouseOverCancels = function(data) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel}
 */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel
/** @type {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel)} */
xyz.swapee.wc.CryptoSelectInterruptLineJoinpointModel.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.RecordICryptoSelectInterruptLineJoinpointModel filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ beforeMenuMouseMv: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuMouseMv, afterMenuMouseMv: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMv, afterMenuMouseMvThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvThrows, afterMenuMouseMvReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvReturns, afterMenuMouseMvCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvCancels, beforeMenuItemCl: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuItemCl, afterMenuItemCl: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemCl, afterMenuItemClThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClThrows, afterMenuItemClReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClReturns, afterMenuItemClCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClCancels, beforeInputKeyUp: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyUp, afterInputKeyUp: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUp, afterInputKeyUpThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpThrows, afterInputKeyUpReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpReturns, afterInputKeyUpCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpCancels, beforeInputKeyboardScroll: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyboardScroll, afterInputKeyboardScroll: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScroll, afterInputKeyboardScrollThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollThrows, afterInputKeyboardScrollReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollReturns, afterInputKeyboardScrollCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollCancels, beforeSelectedBlCl: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeSelectedBlCl, afterSelectedBlCl: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlCl, afterSelectedBlClThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClThrows, afterSelectedBlClReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClReturns, afterSelectedBlClCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClCancels, beforeOnInputKeyDow: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDow, afterOnInputKeyDow: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDow, afterOnInputKeyDowThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowThrows, afterOnInputKeyDowReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowReturns, afterOnInputKeyDowCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowCancels, beforeOnInputBlur: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputBlur, afterOnInputBlur: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlur, afterOnInputBlurThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurThrows, afterOnInputBlurReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurReturns, afterOnInputBlurCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurCancels, beforeOnInputFocus: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputFocus, afterOnInputFocus: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocus, afterOnInputFocusThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusThrows, afterOnInputFocusReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusReturns, afterOnInputFocusCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusCancels, beforeOnMouseLeave: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnMouseLeave, afterOnMouseLeave: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeave, afterOnMouseLeaveThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveThrows, afterOnMouseLeaveReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveReturns, afterOnMouseLeaveCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveCancels, beforeOnInputKeyDown: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDown, afterOnInputKeyDown: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDown, afterOnInputKeyDownThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownThrows, afterOnInputKeyDownReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownReturns, afterOnInputKeyDownCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownCancels, beforeAddMouseOver: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeAddMouseOver, afterAddMouseOver: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOver, afterAddMouseOverThrows: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverThrows, afterAddMouseOverReturns: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverReturns, afterAddMouseOverCancels: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverCancels }} */
xyz.swapee.wc.RecordICryptoSelectInterruptLineJoinpointModel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BoundICryptoSelectInterruptLineJoinpointModel filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordICryptoSelectInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.BoundICryptoSelectInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundICryptoSelectInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BoundCryptoSelectInterruptLineJoinpointModel filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectInterruptLineJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundCryptoSelectInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuMouseMv filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuMouseMv
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuMouseMv
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuMouseMv

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMv filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMv
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMv
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMv

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuMouseMvCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuMouseMvCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuMouseMvCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuItemCl filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeMenuItemCl
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeMenuItemCl
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeMenuItemCl

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemCl filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemCl
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemCl
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemCl

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterMenuItemClCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterMenuItemClCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterMenuItemClCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyUp filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyUp
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyUp
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyUp

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUp filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUp
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUp
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUp

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyUpCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyUpCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyUpCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyboardScroll filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeInputKeyboardScroll
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeInputKeyboardScroll
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeInputKeyboardScroll

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScroll filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScroll
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScroll
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScroll

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterInputKeyboardScrollCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterInputKeyboardScrollCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterInputKeyboardScrollCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeSelectedBlCl filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeSelectedBlCl
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeSelectedBlCl
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeSelectedBlCl

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlCl filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlCl
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlCl
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlCl

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterSelectedBlClCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterSelectedBlClCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterSelectedBlClCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDow filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDow
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDow
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDow

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDow filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDow
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDow
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDow

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDowCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDowCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDowCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputBlur filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputBlur
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputBlur
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputBlur

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlur filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlur
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlur
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlur

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputBlurCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputBlurCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputBlurCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputFocus filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputFocus
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputFocus
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputFocus

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocus filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocus
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocus
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocus

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputFocusCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputFocusCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputFocusCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnMouseLeave filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnMouseLeave
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnMouseLeave
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnMouseLeave

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeave filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeave
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeave
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeave

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnMouseLeaveCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnMouseLeaveCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnMouseLeaveCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDown filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeOnInputKeyDown
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeOnInputKeyDown
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeOnInputKeyDown

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDown filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDown
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDown
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDown

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterOnInputKeyDownCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterOnInputKeyDownCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterOnInputKeyDownCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeAddMouseOver filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.beforeAddMouseOver
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._beforeAddMouseOver
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__beforeAddMouseOver

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOver filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOver
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOver
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOver

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverThrows filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverThrows
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverThrows
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverThrows

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverReturns filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverReturns
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverReturns
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverReturns

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverCancels filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.afterAddMouseOverCancels
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel, !xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData=): void} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel._afterAddMouseOverCancels
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.__afterAddMouseOverCancels

// nss:xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @record */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese} */
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller = function() {}
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeMenuMouseMv
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuMouseMv
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuMouseMvThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuMouseMvReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuMouseMvCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeMenuItemCl
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuItemCl
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuItemClThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuItemClReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterMenuItemClCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeInputKeyUp
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyUp
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyUpThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyUpReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyUpCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeInputKeyboardScroll
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyboardScroll
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyboardScrollThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyboardScrollReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterInputKeyboardScrollCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeSelectedBlCl
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterSelectedBlCl
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterSelectedBlClThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterSelectedBlClReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterSelectedBlClCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeOnInputKeyDow
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDow
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDowThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDowReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDowCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeOnInputBlur
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputBlur
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputBlurThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputBlurReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputBlurCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeOnInputFocus
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputFocus
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputFocusThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputFocusReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputFocusCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeOnMouseLeave
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnMouseLeave
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnMouseLeaveThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnMouseLeaveReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnMouseLeaveCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeOnInputKeyDown
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDown
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDownThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDownReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterOnInputKeyDownCancels
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.beforeAddMouseOver
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterAddMouseOver
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterAddMouseOverThrows
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterAddMouseOverReturns
/** @type {number} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.afterAddMouseOverCancels
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.menuMouseMv = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.menuItemCl = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.inputKeyUp = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.inputKeyboardScroll = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.selectedBlCl = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.onInputKeyDow = function() {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.onInputBlur = function() {}
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.onInputFocus = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.onMouseLeave = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.onInputKeyDown = function() {}
/** @return {?} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.prototype.addMouseOver = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller, ...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
$xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller)} */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.__extend
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.continues
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstallerConstructor filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller, ...!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsInstallerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.MenuMouseMvNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ mev: !MouseEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.MenuMouseMvNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.MenuMouseMvNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.MenuMouseMvNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuMouseMvPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuMouseMvPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuMouseMvPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuMouseMvPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuMouseMvPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuMouseMvPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.MenuItemClNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ mev: !MouseEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.MenuItemClNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.MenuItemClNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.MenuItemClNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeMenuItemClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterMenuItemClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsMenuItemClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsMenuItemClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.MenuItemClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsMenuItemClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyUpNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ kev: KeyboardEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyUpNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyUpNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyUpNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyUpPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyUpPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyUpPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyUpPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyUpPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyUpPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyboardScrollNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ kev: KeyboardEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyboardScrollNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyboardScrollNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.InputKeyboardScrollNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeInputKeyboardScrollPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterInputKeyboardScrollPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsInputKeyboardScrollPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsInputKeyboardScrollPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.InputKeyboardScrollPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsInputKeyboardScrollPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.SelectedBlClNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ mev: MouseEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.SelectedBlClNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.SelectedBlClNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.SelectedBlClNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeSelectedBlClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterSelectedBlClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsSelectedBlClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsSelectedBlClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.SelectedBlClPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsSelectedBlClPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDowNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ kev: KeyboardEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDowNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDowNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDowNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDowPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDowPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDowPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDowPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDowPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDowPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputBlurPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputFocusPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.OnMouseLeaveNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ mev: MouseEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.OnMouseLeaveNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.OnMouseLeaveNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.OnMouseLeaveNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnMouseLeavePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnMouseLeavePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnMouseLeavePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnMouseLeavePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnMouseLeavePointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnMouseLeavePointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDownNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ kev: KeyboardEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDownNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDownNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.OnInputKeyDownNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeOnInputKeyDownPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterOnInputKeyDownPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsOnInputKeyDownPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsOnInputKeyDownPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.OnInputKeyDownPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsOnInputKeyDownPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.AddMouseOverNArgs filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ev: MouseEvent }} */
xyz.swapee.wc.ICryptoSelectInterruptLine.AddMouseOverNArgs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ ticket: symbol, args: xyz.swapee.wc.ICryptoSelectInterruptLine.AddMouseOverNArgs, proc: !Function }} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData.prototype.cond
/**
 * @param {xyz.swapee.wc.ICryptoSelectInterruptLine.AddMouseOverNArgs} args
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {void}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.BeforeAddMouseOverPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterAddMouseOverPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterThrowsAddMouseOverPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.CryptoSelectInputs} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.CryptoSelectInputs} value
 * @return {?}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterReturnsAddMouseOverPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AddMouseOverPointcutData}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData} */
xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel.AfterCancelsAddMouseOverPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectInterruptLine} */
$xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster.prototype.asICryptoSelectInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster<THIS>}
 */
xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BCryptoSelectInterruptLineAspects filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @interface
 * @extends {xyz.swapee.wc.BCryptoSelectInterruptLineAspectsCaster<THIS>}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.BCryptoSelectInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BCryptoSelectInterruptLineAspects<THIS>}
 */
xyz.swapee.wc.BCryptoSelectInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @record */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese} */
xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ICryptoSelectInterruptLineAspects
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @interface */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundICryptoSelectInterruptLine} */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster.prototype.asICryptoSelectInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLineAspects filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineAspectsCaster}
 * @extends {xyz.swapee.wc.BCryptoSelectInterruptLineAspects<!xyz.swapee.wc.ICryptoSelectInterruptLineAspects>}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ICryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.ICryptoSelectInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLineAspects filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese} init
 * @implements {xyz.swapee.wc.ICryptoSelectInterruptLineAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese>}
 */
$xyz.swapee.wc.CryptoSelectInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspects, ...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese)} */
xyz.swapee.wc.CryptoSelectInterruptLineAspects.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.CryptoSelectInterruptLineAspects.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
$xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects)} */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects)|(!xyz.swapee.wc.BCryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.BCryptoSelectInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.__extend
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects)|(!xyz.swapee.wc.BCryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.BCryptoSelectInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.continues
/**
 * @param {...((!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects)|(!xyz.swapee.wc.BCryptoSelectInterruptLineAspects|typeof xyz.swapee.wc.BCryptoSelectInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.CryptoSelectInterruptLineAspects}
 */
xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.CryptoSelectInterruptLineAspectsConstructor filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspects, ...!xyz.swapee.wc.ICryptoSelectInterruptLineAspects.Initialese)} */
xyz.swapee.wc.CryptoSelectInterruptLineAspectsConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLine.Initialese}
 */
$xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese} */
xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IHyperCryptoSelectInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @interface */
$xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine} */
$xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster.prototype.asIHyperCryptoSelectInterruptLine
/** @type {!xyz.swapee.wc.BoundHyperCryptoSelectInterruptLine} */
$xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster.prototype.superHyperCryptoSelectInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster}
 */
xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.IHyperCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLine}
 */
$xyz.swapee.wc.IHyperCryptoSelectInterruptLine = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.IHyperCryptoSelectInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.HyperCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese} init
 * @implements {xyz.swapee.wc.IHyperCryptoSelectInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese>}
 */
$xyz.swapee.wc.HyperCryptoSelectInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.HyperCryptoSelectInterruptLine
/** @type {function(new: xyz.swapee.wc.IHyperCryptoSelectInterruptLine, ...!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese)} */
xyz.swapee.wc.HyperCryptoSelectInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.HyperCryptoSelectInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
$xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine)} */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IHyperCryptoSelectInterruptLine|typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.IHyperCryptoSelectInterruptLine|typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.IHyperCryptoSelectInterruptLine|typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine)|(!xyz.swapee.wc.ICryptoSelectInterruptLine|typeof xyz.swapee.wc.CryptoSelectInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperCryptoSelectInterruptLine}
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.__trait
/**
 * @param {...(!xyz.swapee.wc.ICryptoSelectInterruptLineAspects|function(new: xyz.swapee.wc.ICryptoSelectInterruptLineAspects))} aides
 * @return {typeof xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine.consults

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.HyperCryptoSelectInterruptLineConstructor filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {function(new: xyz.swapee.wc.IHyperCryptoSelectInterruptLine, ...!xyz.swapee.wc.IHyperCryptoSelectInterruptLine.Initialese)} */
xyz.swapee.wc.HyperCryptoSelectInterruptLineConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.RecordIHyperCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIHyperCryptoSelectInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIHyperCryptoSelectInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IHyperCryptoSelectInterruptLineCaster}
 */
$xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine} */
xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BoundHyperCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIHyperCryptoSelectInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundHyperCryptoSelectInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundHyperCryptoSelectInterruptLine} */
xyz.swapee.wc.BoundHyperCryptoSelectInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.RecordICryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/** @typedef {{ menuMouseMv: xyz.swapee.wc.ICryptoSelectInterruptLine.menuMouseMv, menuItemCl: xyz.swapee.wc.ICryptoSelectInterruptLine.menuItemCl, inputKeyUp: xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyUp, inputKeyboardScroll: xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyboardScroll, selectedBlCl: xyz.swapee.wc.ICryptoSelectInterruptLine.selectedBlCl, onInputKeyDow: xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDow, onInputBlur: xyz.swapee.wc.ICryptoSelectInterruptLine.onInputBlur, onInputFocus: xyz.swapee.wc.ICryptoSelectInterruptLine.onInputFocus, onMouseLeave: xyz.swapee.wc.ICryptoSelectInterruptLine.onMouseLeave, onInputKeyDown: xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDown, addMouseOver: xyz.swapee.wc.ICryptoSelectInterruptLine.addMouseOver }} */
xyz.swapee.wc.RecordICryptoSelectInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BoundICryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordICryptoSelectInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ICryptoSelectInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.BoundICryptoSelectController}
 */
$xyz.swapee.wc.BoundICryptoSelectInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundICryptoSelectInterruptLine} */
xyz.swapee.wc.BoundICryptoSelectInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.BoundCryptoSelectInterruptLine filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundICryptoSelectInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundCryptoSelectInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundCryptoSelectInterruptLine} */
xyz.swapee.wc.BoundCryptoSelectInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.menuMouseMv filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__menuMouseMv = function(mev) {}
/** @typedef {function(!MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.menuMouseMv
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, !MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._menuMouseMv
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__menuMouseMv} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__menuMouseMv

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.menuItemCl filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__menuItemCl = function(mev) {}
/** @typedef {function(!MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.menuItemCl
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, !MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._menuItemCl
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__menuItemCl} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__menuItemCl

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyUp filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyUp = function(kev) {}
/** @typedef {function(KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyUp
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._inputKeyUp
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyUp} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyUp

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyboardScroll filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyboardScroll = function(kev) {}
/** @typedef {function(KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.inputKeyboardScroll
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._inputKeyboardScroll
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyboardScroll} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__inputKeyboardScroll

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.selectedBlCl filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__selectedBlCl = function(mev) {}
/** @typedef {function(MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.selectedBlCl
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._selectedBlCl
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__selectedBlCl} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__selectedBlCl

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDow filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDow = function(kev) {}
/** @typedef {function(KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDow
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._onInputKeyDow
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDow} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDow

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.onInputBlur filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputBlur = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputBlur
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._onInputBlur
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputBlur} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputBlur

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.onInputFocus filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputFocus = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputFocus
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._onInputFocus
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputFocus} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputFocus

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.onMouseLeave filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {MouseEvent} mev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__onMouseLeave = function(mev) {}
/** @typedef {function(MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.onMouseLeave
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._onMouseLeave
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__onMouseLeave} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__onMouseLeave

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDown filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {KeyboardEvent} kev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDown = function(kev) {}
/** @typedef {function(KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.onInputKeyDown
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, KeyboardEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._onInputKeyDown
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDown} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__onInputKeyDown

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/crypto-select/parts/120-interrupt-line/types/design/120-ICryptoSelectInterruptLine.xml} xyz.swapee.wc.ICryptoSelectInterruptLine.addMouseOver filter:!ControllerPlugin~props 1c5a8f8e28c14261fedfdbcebc5368c0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {MouseEvent} ev
 * @return {(undefined|!xyz.swapee.wc.front.CryptoSelectInputs)}
 */
$xyz.swapee.wc.ICryptoSelectInterruptLine.__addMouseOver = function(ev) {}
/** @typedef {function(MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine.addMouseOver
/** @typedef {function(this: xyz.swapee.wc.ICryptoSelectInterruptLine, MouseEvent): (undefined|!xyz.swapee.wc.front.CryptoSelectInputs)} */
xyz.swapee.wc.ICryptoSelectInterruptLine._addMouseOver
/** @typedef {typeof $xyz.swapee.wc.ICryptoSelectInterruptLine.__addMouseOver} */
xyz.swapee.wc.ICryptoSelectInterruptLine.__addMouseOver

// nss:xyz.swapee.wc.ICryptoSelectInterruptLine,$xyz.swapee.wc.ICryptoSelectInterruptLine,xyz.swapee.wc
/* @typal-end */