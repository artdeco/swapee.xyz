/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelHyperslice': {
  'id': 1,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModelBindingHyperslice': {
  'id': 2,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectInterruptLineJoinpointModel': {
  'id': 3,
  'symbols': {},
  'methods': {
   'beforeMenuMouseMv': 1,
   'afterMenuMouseMv': 2,
   'afterMenuMouseMvThrows': 3,
   'afterMenuMouseMvReturns': 4,
   'afterMenuMouseMvCancels': 5,
   'beforeMenuItemCl': 6,
   'afterMenuItemCl': 7,
   'afterMenuItemClThrows': 8,
   'afterMenuItemClReturns': 9,
   'afterMenuItemClCancels': 10,
   'beforeInputKeyUp': 11,
   'afterInputKeyUp': 12,
   'afterInputKeyUpThrows': 13,
   'afterInputKeyUpReturns': 14,
   'afterInputKeyUpCancels': 15,
   'beforeInputKeyboardScroll': 16,
   'afterInputKeyboardScroll': 17,
   'afterInputKeyboardScrollThrows': 18,
   'afterInputKeyboardScrollReturns': 19,
   'afterInputKeyboardScrollCancels': 20,
   'beforeSelectedBlCl': 21,
   'afterSelectedBlCl': 22,
   'afterSelectedBlClThrows': 23,
   'afterSelectedBlClReturns': 24,
   'afterSelectedBlClCancels': 25,
   'beforeOnInputKeyDow': 26,
   'afterOnInputKeyDow': 27,
   'afterOnInputKeyDowThrows': 28,
   'afterOnInputKeyDowReturns': 29,
   'afterOnInputKeyDowCancels': 30,
   'beforeOnInputBlur': 31,
   'afterOnInputBlur': 32,
   'afterOnInputBlurThrows': 33,
   'afterOnInputBlurReturns': 34,
   'afterOnInputBlurCancels': 35,
   'beforeOnInputFocus': 36,
   'afterOnInputFocus': 37,
   'afterOnInputFocusThrows': 38,
   'afterOnInputFocusReturns': 39,
   'afterOnInputFocusCancels': 40,
   'beforeOnMouseLeave': 41,
   'afterOnMouseLeave': 42,
   'afterOnMouseLeaveThrows': 43,
   'afterOnMouseLeaveReturns': 44,
   'afterOnMouseLeaveCancels': 45,
   'beforeOnInputKeyDown': 46,
   'afterOnInputKeyDown': 47,
   'afterOnInputKeyDownThrows': 48,
   'afterOnInputKeyDownReturns': 49,
   'afterOnInputKeyDownCancels': 50,
   'beforeAddMouseOver': 51,
   'afterAddMouseOver': 52,
   'afterAddMouseOverThrows': 53,
   'afterAddMouseOverReturns': 54,
   'afterAddMouseOverCancels': 55
  }
 },
 'xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller': {
  'id': 4,
  'symbols': {},
  'methods': {
   'menuMouseMv': 1,
   'menuItemCl': 2,
   'inputKeyUp': 3,
   'inputKeyboardScroll': 4,
   'selectedBlCl': 5,
   'onInputKeyDow': 6,
   'onInputBlur': 7,
   'onInputFocus': 8,
   'onMouseLeave': 9,
   'onInputKeyDown': 10,
   'addMouseOver': 11
  }
 },
 'xyz.swapee.wc.BCryptoSelectInterruptLineAspects': {
  'id': 5,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ICryptoSelectInterruptLineAspects': {
  'id': 6,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IHyperCryptoSelectInterruptLine': {
  'id': 7,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ICryptoSelectInterruptLine': {
  'id': 8,
  'symbols': {},
  'methods': {
   'menuMouseMv': 1,
   'menuItemCl': 2,
   'inputKeyUp': 3,
   'inputKeyboardScroll': 4,
   'selectedBlCl': 5,
   'onInputKeyDow': 6,
   'onInputBlur': 7,
   'onInputFocus': 8,
   'onMouseLeave': 9,
   'onInputKeyDown': 10,
   'addMouseOver': 11
  }
 }
})