import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_CryptoSelectInterruptLineAspectsInstaller}
 */
function __CryptoSelectInterruptLineAspectsInstaller() {}
__CryptoSelectInterruptLineAspectsInstaller.prototype = /** @type {!_CryptoSelectInterruptLineAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller}
 */
class _CryptoSelectInterruptLineAspectsInstaller { }

_CryptoSelectInterruptLineAspectsInstaller.prototype[$advice]=__CryptoSelectInterruptLineAspectsInstaller

/** @extends {xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller} ‎ */
class CryptoSelectInterruptLineAspectsInstaller extends newAbstract(
 _CryptoSelectInterruptLineAspectsInstaller,4,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller} */
CryptoSelectInterruptLineAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspectsInstaller} */
function CryptoSelectInterruptLineAspectsInstallerClass(){}

export default CryptoSelectInterruptLineAspectsInstaller


CryptoSelectInterruptLineAspectsInstaller[$implementations]=[
 CryptoSelectInterruptLineAspectsInstallerClass.prototype=/**@type {!xyz.swapee.wc.ICryptoSelectInterruptLineAspectsInstaller}*/({
  menuMouseMv(){
   this.beforeMenuMouseMv=1
   this.afterMenuMouseMv=2
   this.aroundMenuMouseMv=3
   this.afterMenuMouseMvThrows=4
   this.afterMenuMouseMvReturns=5
   this.afterMenuMouseMvCancels=7
   return {
    mev:1,
   }
  },
  menuItemCl(){
   this.beforeMenuItemCl=1
   this.afterMenuItemCl=2
   this.aroundMenuItemCl=3
   this.afterMenuItemClThrows=4
   this.afterMenuItemClReturns=5
   this.afterMenuItemClCancels=7
   return {
    mev:1,
   }
  },
  inputKeyUp(){
   this.beforeInputKeyUp=1
   this.afterInputKeyUp=2
   this.aroundInputKeyUp=3
   this.afterInputKeyUpThrows=4
   this.afterInputKeyUpReturns=5
   this.afterInputKeyUpCancels=7
   return {
    kev:1,
   }
  },
  inputKeyboardScroll(){
   this.beforeInputKeyboardScroll=1
   this.afterInputKeyboardScroll=2
   this.aroundInputKeyboardScroll=3
   this.afterInputKeyboardScrollThrows=4
   this.afterInputKeyboardScrollReturns=5
   this.afterInputKeyboardScrollCancels=7
   return {
    kev:1,
   }
  },
  selectedBlCl(){
   this.beforeSelectedBlCl=1
   this.afterSelectedBlCl=2
   this.aroundSelectedBlCl=3
   this.afterSelectedBlClThrows=4
   this.afterSelectedBlClReturns=5
   this.afterSelectedBlClCancels=7
   return {
    mev:1,
   }
  },
  onInputKeyDow(){
   this.beforeOnInputKeyDow=1
   this.afterOnInputKeyDow=2
   this.aroundOnInputKeyDow=3
   this.afterOnInputKeyDowThrows=4
   this.afterOnInputKeyDowReturns=5
   this.afterOnInputKeyDowCancels=7
   return {
    kev:1,
   }
  },
  onInputBlur(){
   this.beforeOnInputBlur=1
   this.afterOnInputBlur=2
   this.aroundOnInputBlur=3
   this.afterOnInputBlurThrows=4
   this.afterOnInputBlurReturns=5
   this.afterOnInputBlurCancels=7
  },
  onInputFocus(){
   this.beforeOnInputFocus=1
   this.afterOnInputFocus=2
   this.aroundOnInputFocus=3
   this.afterOnInputFocusThrows=4
   this.afterOnInputFocusReturns=5
   this.afterOnInputFocusCancels=7
  },
  onMouseLeave(){
   this.beforeOnMouseLeave=1
   this.afterOnMouseLeave=2
   this.aroundOnMouseLeave=3
   this.afterOnMouseLeaveThrows=4
   this.afterOnMouseLeaveReturns=5
   this.afterOnMouseLeaveCancels=7
   return {
    mev:1,
   }
  },
  onInputKeyDown(){
   this.beforeOnInputKeyDown=1
   this.afterOnInputKeyDown=2
   this.aroundOnInputKeyDown=3
   this.afterOnInputKeyDownThrows=4
   this.afterOnInputKeyDownReturns=5
   this.afterOnInputKeyDownCancels=7
   return {
    kev:1,
   }
  },
  addMouseOver(){
   this.beforeAddMouseOver=1
   this.afterAddMouseOver=2
   this.aroundAddMouseOver=3
   this.afterAddMouseOverThrows=4
   this.afterAddMouseOverReturns=5
   this.afterAddMouseOverCancels=7
   return {
    ev:1,
   }
  },
 }),
 __CryptoSelectInterruptLineAspectsInstaller,
]