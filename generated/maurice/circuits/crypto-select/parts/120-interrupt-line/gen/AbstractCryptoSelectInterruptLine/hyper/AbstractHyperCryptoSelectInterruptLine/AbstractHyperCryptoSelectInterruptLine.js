import CryptoSelectInterruptLineAspectsInstaller from '../../aspects-installers/CryptoSelectInterruptLineAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperCryptoSelectInterruptLine}
 */
function __AbstractHyperCryptoSelectInterruptLine() {}
__AbstractHyperCryptoSelectInterruptLine.prototype = /** @type {!_AbstractHyperCryptoSelectInterruptLine} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine}
 */
class _AbstractHyperCryptoSelectInterruptLine {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperCryptoSelectInterruptLine
    .clone({aspectsInstaller:CryptoSelectInterruptLineAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine} ‎ */
class AbstractHyperCryptoSelectInterruptLine extends newAbstract(
 _AbstractHyperCryptoSelectInterruptLine,7,null,{
  asIHyperCryptoSelectInterruptLine:1,
  superHyperCryptoSelectInterruptLine:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine} */
AbstractHyperCryptoSelectInterruptLine.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractHyperCryptoSelectInterruptLine} */
function AbstractHyperCryptoSelectInterruptLineClass(){}

export default AbstractHyperCryptoSelectInterruptLine


AbstractHyperCryptoSelectInterruptLine[$implementations]=[
 __AbstractHyperCryptoSelectInterruptLine,
]