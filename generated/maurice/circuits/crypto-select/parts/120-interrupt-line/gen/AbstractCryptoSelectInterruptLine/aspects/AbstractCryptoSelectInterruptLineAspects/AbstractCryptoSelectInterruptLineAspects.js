import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectInterruptLineAspects}
 */
function __AbstractCryptoSelectInterruptLineAspects() {}
__AbstractCryptoSelectInterruptLineAspects.prototype = /** @type {!_AbstractCryptoSelectInterruptLineAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects}
 */
class _AbstractCryptoSelectInterruptLineAspects { }
/**
 * The aspects of the *ICryptoSelectInterruptLine*.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects} ‎
 */
class AbstractCryptoSelectInterruptLineAspects extends newAspects(
 _AbstractCryptoSelectInterruptLineAspects,6,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects} */
AbstractCryptoSelectInterruptLineAspects.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLineAspects} */
function AbstractCryptoSelectInterruptLineAspectsClass(){}

export default AbstractCryptoSelectInterruptLineAspects


AbstractCryptoSelectInterruptLineAspects[$implementations]=[
 __AbstractCryptoSelectInterruptLineAspects,
]