import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractCryptoSelectInterruptLine}
 */
function __AbstractCryptoSelectInterruptLine() {}
__AbstractCryptoSelectInterruptLine.prototype = /** @type {!_AbstractCryptoSelectInterruptLine} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractCryptoSelectInterruptLine}
 */
class _AbstractCryptoSelectInterruptLine { }
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @extends {xyz.swapee.wc.AbstractCryptoSelectInterruptLine} ‎
 */
class AbstractCryptoSelectInterruptLine extends newAbstract(
 _AbstractCryptoSelectInterruptLine,8,null,{
  asICryptoSelectInterruptLine:1,
  superCryptoSelectInterruptLine:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLine} */
AbstractCryptoSelectInterruptLine.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractCryptoSelectInterruptLine} */
function AbstractCryptoSelectInterruptLineClass(){}

export default AbstractCryptoSelectInterruptLine


AbstractCryptoSelectInterruptLine[$implementations]=[
 __AbstractCryptoSelectInterruptLine,
]

export {AbstractCryptoSelectInterruptLine}