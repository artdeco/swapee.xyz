/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintSmHeight} */
export default function prepaintSmHeight(memory,land) {
 const{asICryptoSelectDisplay:{paintSmHeight:paintSmHeight}}=this

 paintSmHeight(memory,null)
}
prepaintSmHeight['_id']='3b37ffd'