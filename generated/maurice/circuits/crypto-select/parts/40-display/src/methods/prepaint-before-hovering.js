/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintBeforeHovering} */
export default function prepaintBeforeHovering(memory,land) {
 const{asICryptoSelectDisplay:{paintBeforeHovering:paintBeforeHovering}}=this

 paintBeforeHovering(memory,null)
}
prepaintBeforeHovering['_id']='480f48f'