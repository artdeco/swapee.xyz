/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintHoveringIndex} */
export default function prepaintHoveringIndex(memory,land) {
 const{asICryptoSelectDisplay:{paintHoveringIndex:paintHoveringIndex}}=this

 paintHoveringIndex(memory,null)
}
prepaintHoveringIndex['_id']='3efb8ee'