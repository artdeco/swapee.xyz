/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintSearchInput} */
export default function prepaintSearchInput(memory,land) {
 const{asICryptoSelectDisplay:{paintSearchInput:paintSearchInput}}=this

 paintSearchInput(memory,null)
}
prepaintSearchInput['_id']='5e737b7'