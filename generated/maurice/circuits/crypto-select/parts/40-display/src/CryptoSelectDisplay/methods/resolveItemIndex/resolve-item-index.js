import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._resolveItemIndex} */
export default function resolveItemIndex(item) {
 const i =item.dataset['i']
 if(i) return parseFloat(i)
 return -1
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFzdkJHLFNBQVMsZ0JBQWdCLENBQUM7Q0FDekIsTUFBTSxDQUFDLEVBQUUsSUFBSSxDQUFDO0NBQ2QsRUFBRSxDQUFDLEdBQUcsT0FBTyxVQUFVLENBQUM7Q0FDeEIsT0FBTztBQUNSLENBQUYifQ==