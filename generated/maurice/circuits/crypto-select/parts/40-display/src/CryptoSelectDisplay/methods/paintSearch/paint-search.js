import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintSearch} */
export default function paintSearch({
 matchedKeys:matchedKeys,
}) {
 const{
  asICryptoSelectTouchscreen:{
   classes:{CryptoDropItem:CryptoDropItem},
  },
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
 }=this
 /** @type {!Array<!HTMLDivElement>} */
 const items=[...CryptoDown.querySelectorAll(`.${CryptoDropItem}`)]
 for(const item of items) {
  const d=item.dataset['value']
  if(!d) return
  if(matchedKeys.has(d)) {
   item.style.display=''
  }else {
   item.style.display='none'
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUEwcUJHLFNBQVMsV0FBVztDQUNuQix1QkFBdUI7QUFDeEIsQ0FBQztDQUNBO0VBQ0M7R0FDQyxTQUFTLDZCQUE2QjtBQUN6QztFQUNFO0dBQ0MscUJBQXFCO0FBQ3hCO0dBQ0c7O0NBRUYsTUFBTSxLQUFLLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxjQUFjLENBQUM7Q0FDL0QsR0FBRyxDQUFDLE1BQU0sS0FBSyxHQUFHO0VBQ2pCLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQztFQUNiLEVBQUUsQ0FBQyxJQUFJO0VBQ1AsRUFBRSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUM7R0FDbEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7QUFDdEIsR0FBRztHQUNBLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO0FBQ3RCO0FBQ0E7QUFDQSxDQUFGIn0=