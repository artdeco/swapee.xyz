import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._scrollItemIntoView} */
export default function scrollItemIntoView(item) {
 const{
  asICryptoSelectDisplay:{
   resolveItem:resolveItem,
  },
 }=this
 const clientRect=item.getBoundingClientRect()
 const parentRect=item.parentElement.getBoundingClientRect()
 //  console.log(item,item.parentElement,item.parentElement.scrollTop)
 const HEIGHT_IN_ITEMS=Math.floor(parentRect.height/clientRect.height)

 if(clientRect.y>parentRect.y+parentRect.height) {
  // scroll
  item.scrollIntoView({
   behavior:'smooth',
   block:'nearest',
   inline:'nearest',
  })
  return
 }

 if(clientRect.y+clientRect.height>parentRect.y+parentRect.height) {
  item.scrollIntoView({
   behavior:'smooth',
   block:'nearest',
   inline:'nearest',
  })
  return
  // ends after the scroll
 }

 if(clientRect.y<parentRect.y) {
  // item.parentElement.offsetTop
  // get item at
  const ind=Math.round(item.parentElement.scrollTop/clientRect.height)
  let newInd=ind-HEIGHT_IN_ITEMS
  if(newInd<-1) newInd=0
  // console.log('ind',ind.newInd)
  const i=resolveItem(newInd)
  // debugger
  if(i) i.scrollIntoView({
   block:'nearest',
   behavior:'smooth',
   inline:'nearest',
  })
  else item.scrollIntoView({
   behavior:'smooth',
   inline:'nearest',
   block:'nearest',
  })
  // debugger
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFpc0JHLFNBQVMsa0JBQWtCLENBQUM7Q0FDM0I7RUFDQztHQUNDLHVCQUF1QjtBQUMxQjtHQUNHO0NBQ0YsTUFBTSxVQUFVLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDO0NBQzVDLE1BQU0sVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUM7Q0FDMUQsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7Q0FDM0QsTUFBTSxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUM7O0NBRTlELEVBQUUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUM7RUFDdkMsR0FBRztFQUNILElBQUksQ0FBQyxjQUFjO0dBQ2xCLGlCQUFpQjtHQUNqQixlQUFlO0dBQ2YsZ0JBQWdCO0FBQ25CLEdBQUc7RUFDRDtBQUNGOztDQUVDLEVBQUUsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO0VBQ3pELElBQUksQ0FBQyxjQUFjO0dBQ2xCLGlCQUFpQjtHQUNqQixlQUFlO0dBQ2YsZ0JBQWdCO0FBQ25CLEdBQUc7RUFDRDtFQUNBLEdBQUcsS0FBSyxNQUFNLElBQUk7QUFDcEI7O0NBRUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUM7RUFDMUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO0VBQ3RCLEdBQUcsSUFBSSxLQUFLO0VBQ1osTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDO0VBQzdELElBQUksTUFBTSxDQUFDO0VBQ1gsRUFBRSxDQUFDLFdBQVcsTUFBTSxDQUFDO0VBQ3JCLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO0VBQ3pCLE1BQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQztFQUNwQixHQUFHO0VBQ0gsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLGNBQWM7R0FDckIsZUFBZTtHQUNmLGlCQUFpQjtHQUNqQixnQkFBZ0I7QUFDbkIsR0FBRztFQUNELEtBQUssSUFBSSxDQUFDLGNBQWM7R0FDdkIsaUJBQWlCO0dBQ2pCLGdCQUFnQjtHQUNoQixlQUFlO0FBQ2xCLEdBQUc7RUFDRCxHQUFHO0FBQ0w7QUFDQSxDQUFGIn0=