import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._resolveItem} */
export default function resolveItem(index) {
 const{
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
 }=this
 const item=CryptoDown.querySelector(`[data-i="${index}"]`)
 return /** @type {!HTMLElement} */(item)||null
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFvd0JHLFNBQVMsV0FBVyxDQUFDO0NBQ3BCO0VBQ0M7R0FDQyxxQkFBcUI7QUFDeEI7R0FDRztDQUNGLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDO0NBQ3RELGtDQUFrQyxDQUFDO0FBQ3BDLENBQUYifQ==