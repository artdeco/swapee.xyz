import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._resolveItemByKey} */
export default function resolveItemByKey(key) {
 const{
  asICryptoSelectDisplay:{
   CryptoDown:CryptoDown,
  },
 }=this
 const item=CryptoDown.querySelector(`[data-value="${key}"]`)
 return /** @type {!HTMLElement} */(item)||null
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUEydkJHLFNBQVMsZ0JBQWdCLENBQUM7Q0FDekI7RUFDQztHQUNDLHFCQUFxQjtBQUN4QjtHQUNHO0NBQ0YsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsR0FBRyxHQUFHLENBQUM7Q0FDeEQsa0NBQWtDLENBQUM7QUFDcEMsQ0FBRiJ9