import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintHoveringIndex} */
export default function paintHoveringIndex({
 hoveringIndex:hoveringIndex,
}) {
 const{
  asICryptoSelectTouchscreen: {
   asICryptoSelectDisplay:{
    CryptoDown,
    scrollItemIntoView:scrollItemIntoView,
    resolveItem:resolveItem,
   },
   classes:{
    ItemHovered:ItemHovered,
   },
  },
 }=this
 const before=CryptoDown.querySelectorAll(`.${ItemHovered}`)
 ;[...before].forEach((b)=>{
  b.classList.remove(ItemHovered)
 })

 const item=resolveItem(hoveringIndex)
 if(item) {
  item.classList.add(ItemHovered)
  scrollItemIntoView(item)
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUE2a0JHLFNBQVMsa0JBQWtCO0NBQzFCLDJCQUEyQjtBQUM1QixDQUFDO0NBQ0E7RUFDQztHQUNDO0lBQ0MsVUFBVTtJQUNWLHFDQUFxQztJQUNyQyx1QkFBdUI7QUFDM0I7R0FDRztJQUNDLHVCQUF1QjtBQUMzQjtBQUNBO0dBQ0c7Q0FDRixNQUFNLE1BQU0sQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsV0FBVyxDQUFDO0FBQzFELEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztFQUNyQixDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQztBQUNyQixFQUFFOztDQUVELE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQztDQUN2QixFQUFFLENBQUM7RUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztFQUNuQixrQkFBa0IsQ0FBQztBQUNyQjtBQUNBLENBQUYifQ==