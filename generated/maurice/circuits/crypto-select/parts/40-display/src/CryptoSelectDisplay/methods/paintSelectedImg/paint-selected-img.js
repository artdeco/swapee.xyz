import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintSelectedImg} */
export default function paintSelectedImg({selected:selected}) {
 // if(!prevSelected) return
 if(selected) {
  const{
   asICryptoSelectDisplay:{
    CryptoSelectedImWr:CryptoSelectedImWr,
    CryptoDown:CryptoDown,
   },
   asICryptoSelectTouchscreen:{
    setInputs:setInputs,
    classes:{CryptoDropItem:CryptoDropItem,ImgWr:ImgWr},
   },
  }=this
  if(CryptoSelectedImWr.dataset['selected']==selected) return
  else CryptoSelectedImWr.removeAttribute('data-selected')

  const item=CryptoDown.querySelector(`.${CryptoDropItem}[data-value=${selected}]`)
  const imgWr=item.querySelector(`.${ImgWr}`)
  setInputs({selectedIcon:imgWr.innerHTML})
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUF3bUJHLFNBQVMsZ0JBQWdCLEVBQUUsaUJBQWlCLENBQUM7Q0FDNUMsR0FBRyxFQUFFLENBQUMsZUFBZTtDQUNyQixFQUFFLENBQUM7RUFDRjtHQUNDO0lBQ0MscUNBQXFDO0lBQ3JDLHFCQUFxQjtBQUN6QjtHQUNHO0lBQ0MsbUJBQW1CO0lBQ25CLFNBQVMsNkJBQTZCLENBQUMsV0FBVztBQUN0RDtJQUNJO0VBQ0YsRUFBRSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixFQUFFLFVBQVU7RUFDckQsS0FBSyxrQkFBa0IsQ0FBQyxlQUFlLENBQUM7O0VBRXhDLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFLFFBQVEsQ0FBQztFQUM5RSxNQUFNLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQztFQUN6QyxTQUFTLEVBQUUsa0JBQWtCLENBQUMsU0FBUyxDQUFDO0FBQzFDO0FBQ0EsQ0FBRiJ9