import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintSearchInput} */
export default function paintSearchInput({
 selected:selected,
 menuExpanded:menuExpanded,
 selectedCrypto:selectedCrypto,
 sm:sm,
}){
 const{
  asICryptoSelectDisplay:{
   CryptoSelectedNameIn:CryptoSelectedNameIn,
  },
 }=this
 // debugger
 if(menuExpanded) { // just menuExpanded
  CryptoSelectedNameIn.value=''
 }else { // just collapsed
  CryptoSelectedNameIn.value=!sm?selectedCrypto.displayName||selected:selected
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUE2bkJHLFNBQVMsZ0JBQWdCO0NBQ3hCLGlCQUFpQjtDQUNqQix5QkFBeUI7Q0FDekIsNkJBQTZCO0NBQzdCLEtBQUs7QUFDTixDQUFDO0NBQ0E7RUFDQztHQUNDLHlDQUF5QztBQUM1QztHQUNHO0NBQ0YsR0FBRztDQUNILEVBQUUsQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLO0VBQzFCLG9CQUFvQixDQUFDLEtBQUssQ0FBQztBQUM3QixFQUFFLE9BQU8sR0FBRyxLQUFLO0VBQ2Ysb0JBQW9CLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDO0FBQ2hEO0FBQ0EsQ0FBRiJ9