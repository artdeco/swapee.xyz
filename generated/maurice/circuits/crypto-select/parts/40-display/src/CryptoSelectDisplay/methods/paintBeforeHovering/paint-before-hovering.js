import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._paintBeforeHovering} */
export default function paintBeforeHovering({
 hoveringIndex:hoveringIndex,
}) {
 const{asICryptoSelectDisplay:{
  resolveItem:resolveItem,
  CryptoDown:CryptoDown,
 }}=this
 const before=/**@type {HTMLElement} */(CryptoDown.querySelector(`[data-previous-hover]`))
 if(before) {
  before.dataset['previousHover']=''
  delete before.dataset['previousHover']
  before.style.borderBottomColor=''
  delete before.style.borderBottomColor
 }

 if(hoveringIndex==-1){
  return
 }

 const prevHoveringIndex=hoveringIndex-1
 const previousItem=resolveItem(prevHoveringIndex)

 if(previousItem) {
  previousItem.style.borderBottomColor='#521d71'//'#5d2b7a'
  previousItem.dataset['previousHover']='true'
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUErb0JHLFNBQVMsbUJBQW1CO0NBQzNCLDJCQUEyQjtBQUM1QixDQUFDO0NBQ0EsTUFBTTtFQUNMLHVCQUF1QjtFQUN2QixxQkFBcUI7QUFDdkIsSUFBSTtDQUNILE1BQU0sTUFBTSwwQkFBMEIsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO0NBQ2hFLEVBQUUsQ0FBQztFQUNGLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQztFQUNoQyxPQUFPLE1BQU0sQ0FBQztFQUNkLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUM7RUFDL0IsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDO0FBQ3RCOztDQUVDLEVBQUUsQ0FBQyxhQUFhLEVBQUU7RUFDakI7QUFDRjs7Q0FFQyxNQUFNLGlCQUFpQixDQUFDO0NBQ3hCLE1BQU0sWUFBWSxDQUFDLFdBQVcsQ0FBQzs7Q0FFL0IsRUFBRSxDQUFDO0VBQ0YsWUFBWSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztFQUNyQyxZQUFZLENBQUMsd0JBQXdCLENBQUM7QUFDeEM7QUFDQSxDQUFGIn0=