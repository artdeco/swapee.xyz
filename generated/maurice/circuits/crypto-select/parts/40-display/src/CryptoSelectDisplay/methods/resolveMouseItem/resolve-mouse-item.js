import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.ICryptoSelectDisplay._resolveMouseItem} */
export default function resolveMouseItem(ev) {
 const{
  asICryptoSelectTouchscreen:{
   classes:{
    CryptoDropItem:CryptoDropItem,
   },
  },
 }=this
 const _item=/**@type {!HTMLElement}*/(ev.target)
 const item=_item.classList.contains(CryptoDropItem)?_item:_item.closest(`.${CryptoDropItem}`)
 return/**@type {!HTMLElement} */(item)||null
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY3J5cHRvLXNlbGVjdC9jcnlwdG8tc2VsZWN0Lndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUE2d0JHLFNBQVMsZ0JBQWdCLENBQUM7Q0FDekI7RUFDQztHQUNDO0lBQ0MsNkJBQTZCO0FBQ2pDO0FBQ0E7R0FDRztDQUNGLE1BQU0sS0FBSywwQkFBMEIsQ0FBQyxFQUFFLENBQUM7Q0FDekMsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLGNBQWMsQ0FBQztDQUMzRixnQ0FBZ0MsQ0FBQztBQUNsQyxDQUFGIn0=