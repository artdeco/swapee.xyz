/** @extends {xyz.swapee.wc.AbstractCryptoSelect} */
export default class AbstractCryptoSelect extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractCryptoSelectComputer} */
export class AbstractCryptoSelectComputer extends (<computer>
   <adapter name="adaptWideness">
    <xyz.swapee.wc.ICryptoSelectCore selected />
    <outputs>
     <xyz.swapee.wc.ICryptoSelectCore wideness />
    </outputs>

    Changes the wideness based on the selected crypto.
   </adapter>
   <adapter name="adaptSelectedCrypto">
    <xyz.swapee.wc.ICryptoSelectCore selected="required" cryptos="required" />
    <outputs>
     <xyz.swapee.wc.ICryptoSelectCore selectedCrypto />
    </outputs>
    Finds the selected crypto in the map by key.
   </adapter>

   <adapter name="adaptSearchInput" browser>
    <xyz.swapee.wc.ICryptoSelectCore menuExpanded="empty" />
    <outputs>
     <xyz.swapee.wc.ICryptoSelectCore search />
    </outputs>
    Resets the search also on collapse.
   </adapter>

   <adapter name="adaptPopupShown">
    <xyz.swapee.wc.ICryptoSelectCore menuExpanded />
    <outputs>
     <com.webcircuits.ui.IPopupPort shown />
    </outputs>
    Shows/hides the popup.
   </adapter>

   <adapter name="adaptMatchedKeys">
    <xyz.swapee.wc.ICryptoSelectCore search cryptos="required" ignoreCryptos />
    <outputs>
     <xyz.swapee.wc.ICryptoSelectCore matchedKeys />
    </outputs>
    Based on the search, generates the set of matched keys from the cryptos' map.
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractCryptoSelectController} */
export class AbstractCryptoSelectController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractCryptoSelectPort} */
export class CryptoSelectPort extends (<ic-port>
  <inputs>
   <string name="source" default="onramper-cryptos">
    <choice val="onramper-cryptos">
     Chooses cached cryptos as the data source (default option).
    </choice>
    <choice val="onramper-online-cryptos">
     Loads the cryptos via the OnRamper's gateway loader.
    </choice>
    <choice val="onramper-currencies">
     Chooses cached currencies as the data source.
    </choice>
    <choice val="onramper-online-fiats">
     Loads the currencies via the onramper gateway loader.
    </choice>

    The predefined source will determine which list is loaded into the data model.
   </string>

  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractCryptoSelectView} */
export class AbstractCryptoSelectView extends (<view>
  <classes>
   <string opt name="CryptoDownBorder">
    The border for the crypto-down item.
   </string>
   <string opt name="Fiat">
    If this is a crypto select for FIAT.
   </string>
   <string opt name="ItemHovered">
    Added to the item that is hovered to synchronise the first of the type
    `CryptoDropItem`.
   </string>
   <string opt name="CryptoDropItem">
    The single row in the drop-down list. The component does not uses the
    recyrcler renderer for list rendering, but controls all elements via
    the DOM interaction by manipulating classes. This class is set on the
    `$CryptoDropItem` inside the rendered div.
   </string>
   <string opt name="CryptoDownCollapsed">
    Whether the drop down is collapsed.
   </string>
   <string opt name="CryptoDown">
    Applied on the crypto drop-down list.
   </string>
   <string opt name="Sm">
    The smaller version of the styling.
   </string>
   <string opt name="Expanded">
     Added to the root element when component is menuExpanded.
   </string>
   <string opt name="SmWide">
     Added to the small state of crypto select, when the crypto symbol is longer
     than 3 symbols, to make it appear wider.
   </string>
   <string opt name="MouseOver">
     Added to the root component element the mouse has just been over to play
     the glossy effect, and is removed once animation is finished (1.55s).
   </string>
   <string opt name="DropDownKeyboardFocus">
     The drop down can be focused by the keyboard events when the user would
     like to pick the name of the crypto from the dropdown list perhaps after
     performing a search so that he's in control of the keyboard already and
     expected to be able to use the keyboard-driven navigation, so the
     component controls the keyboard with the interrupt line and sets this
     class on the item which corresponds to the currently focused on by the
     keyboard item in the model.
   </string>
   <string opt name="HoveringOverItem">
    Added to the crypto down when hovering over an item.
   </string>
   <string opt name="ImgWr">
    Set on the span that wraps the actual image. Its contents are copied into
    the selected icon via `imHtml`. The keyboard select style requires this
    class to hook the highlighted blurred background on keyboard select events.
   </string>
   <string opt name="KeyboardSelect">
    Added to the `ImgWr` by the keyboard select stylesheet when the item in the
    drop down menu receives focus via keyboard. Expects keyboard down interrupts:

    - `inputKeyboardScroll`: the scroll item;

   </string>
   <string opt name="Highlight">
    Works with `.MouseOver` class - when `.Highlight.MouseOver` is hovered over,
    the reflection stripe is played on the element that has these 2 classes.
    Set on `CryptoSelectedBl`. Expects mouseover adapters.
   </string>
   <string opt name="Chevron">
    These are the chevrons that are drawn with an image (either up or down).
   </string>
   <string opt name="NoCoins">
    Indicates that no cryptos/fiats were found for the entered name.
   </string>
   <string opt name="Pill">
    The same as the crypto-down wrapper. The pill is the wrapper around the
    absolutely-positioned drop down.
   </string>
   <string opt name="CryptoDropItemBeforeHover">
     This class is added to a drop-down item that comes before the one that is
     being hovered over at the moment.
   </string>
   <string opt name="BackgroundStalk">
    The background staling effect class.
   </string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractCryptoSelectElement} */
export class AbstractCryptoSelectElement extends (<element v3 html mv>
 <block src="./CryptoSelect.mvc/src/CryptoSelectElement/methods/render.jsx" />
 <inducer src="./CryptoSelect.mvc/src/CryptoSelectElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractCryptoSelectHtmlComponent} */
export class AbstractCryptoSelectHtmlComponent extends (<html-ic>
  <connectors>

   <com.webcircuits.ui.IPopup via="Popup" />
  </connectors>

</html-ic>) { }
// </class-end>