/** @extends {xyz.swapee.wc.AbstractExchangeStatusHint} */
export default class AbstractExchangeStatusHint extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractExchangeStatusHintComputer} */
export class AbstractExchangeStatusHintComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeStatusHintController} */
export class AbstractExchangeStatusHintController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeStatusHintPort} */
export class ExchangeStatusHintPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeStatusHintView} */
export class AbstractExchangeStatusHintView extends (<view>
  <classes>
   <string opt name="StatusWarn" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeStatusHintElement} */
export class AbstractExchangeStatusHintElement extends (<element v3 html mv>
 <block src="./ExchangeStatusHint.mvc/src/ExchangeStatusHintElement/methods/render.jsx" />
 <inducer src="./ExchangeStatusHint.mvc/src/ExchangeStatusHintElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent} */
export class AbstractExchangeStatusHintHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>