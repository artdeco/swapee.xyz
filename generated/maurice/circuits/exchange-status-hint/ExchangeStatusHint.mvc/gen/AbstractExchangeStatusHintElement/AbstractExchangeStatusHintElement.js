import ExchangeStatusHintRenderVdus from './methods/render-vdus'
import ExchangeStatusHintElementPort from '../ExchangeStatusHintElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {ExchangeStatusHintInputsPQs} from '../../pqs/ExchangeStatusHintInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractExchangeStatusHint from '../AbstractExchangeStatusHint'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintElement}
 */
function __AbstractExchangeStatusHintElement() {}
__AbstractExchangeStatusHintElement.prototype = /** @type {!_AbstractExchangeStatusHintElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintElement}
 */
class _AbstractExchangeStatusHintElement { }
/**
 * A component description.
 *
 * The _IExchangeStatusHint_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintElement} ‎
 */
class AbstractExchangeStatusHintElement extends newAbstract(
 _AbstractExchangeStatusHintElement,270015307612,null,{
  asIExchangeStatusHintElement:1,
  superExchangeStatusHintElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintElement} */
AbstractExchangeStatusHintElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintElement} */
function AbstractExchangeStatusHintElementClass(){}

export default AbstractExchangeStatusHintElement


AbstractExchangeStatusHintElement[$implementations]=[
 __AbstractExchangeStatusHintElement,
 ElementBase,
 AbstractExchangeStatusHintElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':status':statusColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'status':statusAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(statusAttr===undefined?{'status':statusColAttr}:{}),
   }
  },
 }),
 AbstractExchangeStatusHintElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'status':statusAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    status:statusAttr,
   }
  },
 }),
 AbstractExchangeStatusHintElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractExchangeStatusHintElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintElement}*/({
  render:ExchangeStatusHintRenderVdus,
 }),
 AbstractExchangeStatusHintElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintElement}*/({
  classes:{
   'StatusWarn': '45d03',
  },
  inputsPQs:ExchangeStatusHintInputsPQs,
  vdus:{
   'AwaitingStatusLa': 'ee221',
   'ConfirmingStatusLa': 'ee222',
   'ExchangingStatusLa': 'ee223',
   'SendingStatusLa': 'ee224',
   'FinishedStatusLa': 'ee225',
   'FailedStatusLa': 'ee226',
   'RefundedStatusLa': 'ee227',
   'OverdueStatusLa': 'ee228',
   'HoldStatusLa': 'ee229',
   'ExpiredStatusLa': 'ee2210',
  },
 }),
 IntegratedComponent,
 AbstractExchangeStatusHintElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','status','no-solder',':no-solder',':status','fe646','54ab8','f0fe5','8544f','e86ed','d736e','860a0','697a8','0c832','e5575','e2820','9acb4','children']),
   })
  },
  get Port(){
   return ExchangeStatusHintElementPort
  },
 }),
]



AbstractExchangeStatusHintElement[$implementations]=[AbstractExchangeStatusHint,
 /** @type {!AbstractExchangeStatusHintElement} */ ({
  rootId:'ExchangeStatusHint',
  __$id:2700153076,
  fqn:'xyz.swapee.wc.IExchangeStatusHint',
  maurice_element_v3:true,
 }),
]