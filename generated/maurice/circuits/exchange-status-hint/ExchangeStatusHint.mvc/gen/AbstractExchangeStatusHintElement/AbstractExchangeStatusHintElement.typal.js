
import AbstractExchangeStatusHint from '../AbstractExchangeStatusHint'

/** @abstract {xyz.swapee.wc.IExchangeStatusHintElement} */
export default class AbstractExchangeStatusHintElement { }



AbstractExchangeStatusHintElement[$implementations]=[AbstractExchangeStatusHint,
 /** @type {!AbstractExchangeStatusHintElement} */ ({
  rootId:'ExchangeStatusHint',
  __$id:2700153076,
  fqn:'xyz.swapee.wc.IExchangeStatusHint',
  maurice_element_v3:true,
 }),
]