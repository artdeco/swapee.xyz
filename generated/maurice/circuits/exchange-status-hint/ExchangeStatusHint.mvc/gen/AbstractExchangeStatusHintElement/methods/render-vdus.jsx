export default function ExchangeStatusHintRenderVdus(){
 return (<div $id="ExchangeStatusHint">
  <vdu $id="AwaitingStatusLa" />
  <vdu $id="ConfirmingStatusLa" />
  <vdu $id="ExchangingStatusLa" />
  <vdu $id="SendingStatusLa" />
  <vdu $id="FinishedStatusLa" />
  <vdu $id="FailedStatusLa" />
  <vdu $id="RefundedStatusLa" />
  <vdu $id="OverdueStatusLa" />
  <vdu $id="HoldStatusLa" />
  <vdu $id="ExpiredStatusLa" />
 </div>)
}