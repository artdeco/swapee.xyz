import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeStatusHintInputsPQs} from '../../pqs/ExchangeStatusHintInputsPQs'
import {ExchangeStatusHintOuterCoreConstructor} from '../ExchangeStatusHintCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusHintPort}
 */
function __ExchangeStatusHintPort() {}
__ExchangeStatusHintPort.prototype = /** @type {!_ExchangeStatusHintPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusHintPort} */ function ExchangeStatusHintPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ExchangeStatusHintOuterCore} */ ({model:null})
  ExchangeStatusHintOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintPort}
 */
class _ExchangeStatusHintPort { }
/**
 * The port that serves as an interface to the _IExchangeStatusHint_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintPort} ‎
 */
export class ExchangeStatusHintPort extends newAbstract(
 _ExchangeStatusHintPort,27001530765,ExchangeStatusHintPortConstructor,{
  asIExchangeStatusHintPort:1,
  superExchangeStatusHintPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintPort} */
ExchangeStatusHintPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintPort} */
function ExchangeStatusHintPortClass(){}

export const ExchangeStatusHintPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IExchangeStatusHint.Pinout>}*/({
 get Port() { return ExchangeStatusHintPort },
})

ExchangeStatusHintPort[$implementations]=[
 ExchangeStatusHintPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintPort}*/({
  resetPort(){
   this.resetExchangeStatusHintPort()
  },
  resetExchangeStatusHintPort(){
   ExchangeStatusHintPortConstructor.call(this)
  },
 }),
 __ExchangeStatusHintPort,
 Parametric,
 ExchangeStatusHintPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintPort}*/({
  constructor(){
   mountPins(this.inputs,'',ExchangeStatusHintInputsPQs)
  },
 }),
]


export default ExchangeStatusHintPort