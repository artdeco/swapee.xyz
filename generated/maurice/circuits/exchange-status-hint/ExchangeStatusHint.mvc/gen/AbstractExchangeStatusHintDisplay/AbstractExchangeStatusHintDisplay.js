import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintDisplay}
 */
function __AbstractExchangeStatusHintDisplay() {}
__AbstractExchangeStatusHintDisplay.prototype = /** @type {!_AbstractExchangeStatusHintDisplay} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusHintDisplay} */ function ExchangeStatusHintDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.AwaitingStatusLa=null
  /** @type {HTMLSpanElement} */ this.ConfirmingStatusLa=null
  /** @type {HTMLSpanElement} */ this.ExchangingStatusLa=null
  /** @type {HTMLSpanElement} */ this.SendingStatusLa=null
  /** @type {HTMLSpanElement} */ this.FinishedStatusLa=null
  /** @type {HTMLSpanElement} */ this.FailedStatusLa=null
  /** @type {HTMLSpanElement} */ this.RefundedStatusLa=null
  /** @type {HTMLSpanElement} */ this.OverdueStatusLa=null
  /** @type {HTMLSpanElement} */ this.HoldStatusLa=null
  /** @type {HTMLSpanElement} */ this.ExpiredStatusLa=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintDisplay}
 */
class _AbstractExchangeStatusHintDisplay { }
/**
 * Display for presenting information from the _IExchangeStatusHint_.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintDisplay} ‎
 */
class AbstractExchangeStatusHintDisplay extends newAbstract(
 _AbstractExchangeStatusHintDisplay,270015307616,ExchangeStatusHintDisplayConstructor,{
  asIExchangeStatusHintDisplay:1,
  superExchangeStatusHintDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintDisplay} */
AbstractExchangeStatusHintDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintDisplay} */
function AbstractExchangeStatusHintDisplayClass(){}

export default AbstractExchangeStatusHintDisplay


AbstractExchangeStatusHintDisplay[$implementations]=[
 __AbstractExchangeStatusHintDisplay,
 Display,
 AbstractExchangeStatusHintDisplayClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIExchangeStatusHintScreen:{vdusPQs:{
    AwaitingStatusLa:AwaitingStatusLa,
    ConfirmingStatusLa:ConfirmingStatusLa,
    ExchangingStatusLa:ExchangingStatusLa,
    SendingStatusLa:SendingStatusLa,
    FinishedStatusLa:FinishedStatusLa,
    FailedStatusLa:FailedStatusLa,
    RefundedStatusLa:RefundedStatusLa,
    OverdueStatusLa:OverdueStatusLa,
    HoldStatusLa:HoldStatusLa,
    ExpiredStatusLa:ExpiredStatusLa,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    AwaitingStatusLa:/**@type {HTMLSpanElement}*/(children[AwaitingStatusLa]),
    ConfirmingStatusLa:/**@type {HTMLSpanElement}*/(children[ConfirmingStatusLa]),
    ExchangingStatusLa:/**@type {HTMLSpanElement}*/(children[ExchangingStatusLa]),
    SendingStatusLa:/**@type {HTMLSpanElement}*/(children[SendingStatusLa]),
    FinishedStatusLa:/**@type {HTMLSpanElement}*/(children[FinishedStatusLa]),
    FailedStatusLa:/**@type {HTMLSpanElement}*/(children[FailedStatusLa]),
    RefundedStatusLa:/**@type {HTMLSpanElement}*/(children[RefundedStatusLa]),
    OverdueStatusLa:/**@type {HTMLSpanElement}*/(children[OverdueStatusLa]),
    HoldStatusLa:/**@type {HTMLSpanElement}*/(children[HoldStatusLa]),
    ExpiredStatusLa:/**@type {HTMLSpanElement}*/(children[ExpiredStatusLa]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.ExchangeStatusHintDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese}*/({
   AwaitingStatusLa:1,
   ConfirmingStatusLa:1,
   ExchangingStatusLa:1,
   SendingStatusLa:1,
   FinishedStatusLa:1,
   FailedStatusLa:1,
   RefundedStatusLa:1,
   OverdueStatusLa:1,
   HoldStatusLa:1,
   ExpiredStatusLa:1,
  }),
  initializer({
   AwaitingStatusLa:_AwaitingStatusLa,
   ConfirmingStatusLa:_ConfirmingStatusLa,
   ExchangingStatusLa:_ExchangingStatusLa,
   SendingStatusLa:_SendingStatusLa,
   FinishedStatusLa:_FinishedStatusLa,
   FailedStatusLa:_FailedStatusLa,
   RefundedStatusLa:_RefundedStatusLa,
   OverdueStatusLa:_OverdueStatusLa,
   HoldStatusLa:_HoldStatusLa,
   ExpiredStatusLa:_ExpiredStatusLa,
  }) {
   if(_AwaitingStatusLa!==undefined) this.AwaitingStatusLa=_AwaitingStatusLa
   if(_ConfirmingStatusLa!==undefined) this.ConfirmingStatusLa=_ConfirmingStatusLa
   if(_ExchangingStatusLa!==undefined) this.ExchangingStatusLa=_ExchangingStatusLa
   if(_SendingStatusLa!==undefined) this.SendingStatusLa=_SendingStatusLa
   if(_FinishedStatusLa!==undefined) this.FinishedStatusLa=_FinishedStatusLa
   if(_FailedStatusLa!==undefined) this.FailedStatusLa=_FailedStatusLa
   if(_RefundedStatusLa!==undefined) this.RefundedStatusLa=_RefundedStatusLa
   if(_OverdueStatusLa!==undefined) this.OverdueStatusLa=_OverdueStatusLa
   if(_HoldStatusLa!==undefined) this.HoldStatusLa=_HoldStatusLa
   if(_ExpiredStatusLa!==undefined) this.ExpiredStatusLa=_ExpiredStatusLa
  },
 }),
]