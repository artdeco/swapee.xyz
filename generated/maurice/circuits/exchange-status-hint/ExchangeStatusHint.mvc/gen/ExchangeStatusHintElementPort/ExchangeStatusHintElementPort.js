import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusHintElementPort}
 */
function __ExchangeStatusHintElementPort() {}
__ExchangeStatusHintElementPort.prototype = /** @type {!_ExchangeStatusHintElementPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusHintElementPort} */ function ExchangeStatusHintElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    awaitingStatusLaOpts: {},
    confirmingStatusLaOpts: {},
    exchangingStatusLaOpts: {},
    sendingStatusLaOpts: {},
    finishedStatusLaOpts: {},
    failedStatusLaOpts: {},
    refundedStatusLaOpts: {},
    overdueStatusLaOpts: {},
    holdStatusLaOpts: {},
    expiredStatusLaOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintElementPort}
 */
class _ExchangeStatusHintElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintElementPort} ‎
 */
class ExchangeStatusHintElementPort extends newAbstract(
 _ExchangeStatusHintElementPort,270015307613,ExchangeStatusHintElementPortConstructor,{
  asIExchangeStatusHintElementPort:1,
  superExchangeStatusHintElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintElementPort} */
ExchangeStatusHintElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintElementPort} */
function ExchangeStatusHintElementPortClass(){}

export default ExchangeStatusHintElementPort


ExchangeStatusHintElementPort[$implementations]=[
 __ExchangeStatusHintElementPort,
 ExchangeStatusHintElementPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'awaiting-status-la-opts':undefined,
    'confirming-status-la-opts':undefined,
    'exchanging-status-la-opts':undefined,
    'sending-status-la-opts':undefined,
    'finished-status-la-opts':undefined,
    'failed-status-la-opts':undefined,
    'refunded-status-la-opts':undefined,
    'overdue-status-la-opts':undefined,
    'hold-status-la-opts':undefined,
    'expired-status-la-opts':undefined,
   })
  },
 }),
]