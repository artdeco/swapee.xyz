import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintDisplay}
 */
function __AbstractExchangeStatusHintDisplay() {}
__AbstractExchangeStatusHintDisplay.prototype = /** @type {!_AbstractExchangeStatusHintDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay}
 */
class _AbstractExchangeStatusHintDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay} ‎
 */
class AbstractExchangeStatusHintDisplay extends newAbstract(
 _AbstractExchangeStatusHintDisplay,270015307618,null,{
  asIExchangeStatusHintDisplay:1,
  superExchangeStatusHintDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay} */
AbstractExchangeStatusHintDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay} */
function AbstractExchangeStatusHintDisplayClass(){}

export default AbstractExchangeStatusHintDisplay


AbstractExchangeStatusHintDisplay[$implementations]=[
 __AbstractExchangeStatusHintDisplay,
 GraphicsDriverBack,
 AbstractExchangeStatusHintDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeStatusHintDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IExchangeStatusHintDisplay}*/({
    AwaitingStatusLa:twinMock,
    ConfirmingStatusLa:twinMock,
    ExchangingStatusLa:twinMock,
    SendingStatusLa:twinMock,
    FinishedStatusLa:twinMock,
    FailedStatusLa:twinMock,
    RefundedStatusLa:twinMock,
    OverdueStatusLa:twinMock,
    HoldStatusLa:twinMock,
    ExpiredStatusLa:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.ExchangeStatusHintDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IExchangeStatusHintDisplay.Initialese}*/({
   AwaitingStatusLa:1,
   ConfirmingStatusLa:1,
   ExchangingStatusLa:1,
   SendingStatusLa:1,
   FinishedStatusLa:1,
   FailedStatusLa:1,
   RefundedStatusLa:1,
   OverdueStatusLa:1,
   HoldStatusLa:1,
   ExpiredStatusLa:1,
  }),
  initializer({
   AwaitingStatusLa:_AwaitingStatusLa,
   ConfirmingStatusLa:_ConfirmingStatusLa,
   ExchangingStatusLa:_ExchangingStatusLa,
   SendingStatusLa:_SendingStatusLa,
   FinishedStatusLa:_FinishedStatusLa,
   FailedStatusLa:_FailedStatusLa,
   RefundedStatusLa:_RefundedStatusLa,
   OverdueStatusLa:_OverdueStatusLa,
   HoldStatusLa:_HoldStatusLa,
   ExpiredStatusLa:_ExpiredStatusLa,
  }) {
   if(_AwaitingStatusLa!==undefined) this.AwaitingStatusLa=_AwaitingStatusLa
   if(_ConfirmingStatusLa!==undefined) this.ConfirmingStatusLa=_ConfirmingStatusLa
   if(_ExchangingStatusLa!==undefined) this.ExchangingStatusLa=_ExchangingStatusLa
   if(_SendingStatusLa!==undefined) this.SendingStatusLa=_SendingStatusLa
   if(_FinishedStatusLa!==undefined) this.FinishedStatusLa=_FinishedStatusLa
   if(_FailedStatusLa!==undefined) this.FailedStatusLa=_FailedStatusLa
   if(_RefundedStatusLa!==undefined) this.RefundedStatusLa=_RefundedStatusLa
   if(_OverdueStatusLa!==undefined) this.OverdueStatusLa=_OverdueStatusLa
   if(_HoldStatusLa!==undefined) this.HoldStatusLa=_HoldStatusLa
   if(_ExpiredStatusLa!==undefined) this.ExpiredStatusLa=_ExpiredStatusLa
  },
 }),
]