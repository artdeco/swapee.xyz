import AbstractExchangeStatusHintControllerAR from '../AbstractExchangeStatusHintControllerAR'
import {AbstractExchangeStatusHintController} from '../AbstractExchangeStatusHintController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintControllerBack}
 */
function __AbstractExchangeStatusHintControllerBack() {}
__AbstractExchangeStatusHintControllerBack.prototype = /** @type {!_AbstractExchangeStatusHintControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintController}
 */
class _AbstractExchangeStatusHintControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintController} ‎
 */
class AbstractExchangeStatusHintControllerBack extends newAbstract(
 _AbstractExchangeStatusHintControllerBack,270015307621,null,{
  asIExchangeStatusHintController:1,
  superExchangeStatusHintController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintController} */
AbstractExchangeStatusHintControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintController} */
function AbstractExchangeStatusHintControllerBackClass(){}

export default AbstractExchangeStatusHintControllerBack


AbstractExchangeStatusHintControllerBack[$implementations]=[
 __AbstractExchangeStatusHintControllerBack,
 AbstractExchangeStatusHintController,
 AbstractExchangeStatusHintControllerAR,
 DriverBack,
]