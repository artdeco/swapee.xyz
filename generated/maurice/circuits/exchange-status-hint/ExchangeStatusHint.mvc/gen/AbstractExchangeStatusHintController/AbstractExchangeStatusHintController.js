import ExchangeStatusHintBuffer from '../ExchangeStatusHintBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ExchangeStatusHintPortConnector} from '../ExchangeStatusHintPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintController}
 */
function __AbstractExchangeStatusHintController() {}
__AbstractExchangeStatusHintController.prototype = /** @type {!_AbstractExchangeStatusHintController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintController}
 */
class _AbstractExchangeStatusHintController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintController} ‎
 */
export class AbstractExchangeStatusHintController extends newAbstract(
 _AbstractExchangeStatusHintController,270015307619,null,{
  asIExchangeStatusHintController:1,
  superExchangeStatusHintController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintController} */
AbstractExchangeStatusHintController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintController} */
function AbstractExchangeStatusHintControllerClass(){}


AbstractExchangeStatusHintController[$implementations]=[
 AbstractExchangeStatusHintControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IExchangeStatusHintPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractExchangeStatusHintController,
 ExchangeStatusHintBuffer,
 IntegratedController,
 /**@type {!AbstractExchangeStatusHintController}*/(ExchangeStatusHintPortConnector),
]


export default AbstractExchangeStatusHintController