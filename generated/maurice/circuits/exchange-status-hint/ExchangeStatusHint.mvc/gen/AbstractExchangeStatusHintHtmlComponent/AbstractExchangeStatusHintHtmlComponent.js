import AbstractExchangeStatusHintGPU from '../AbstractExchangeStatusHintGPU'
import AbstractExchangeStatusHintScreenBack from '../AbstractExchangeStatusHintScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {ExchangeStatusHintInputsQPs} from '../../pqs/ExchangeStatusHintInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractExchangeStatusHint from '../AbstractExchangeStatusHint'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintHtmlComponent}
 */
function __AbstractExchangeStatusHintHtmlComponent() {}
__AbstractExchangeStatusHintHtmlComponent.prototype = /** @type {!_AbstractExchangeStatusHintHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent}
 */
class _AbstractExchangeStatusHintHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ExchangeStatusHintHtmlComponent} */ (res)
  }
}
/**
 * The _IExchangeStatusHint_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent} ‎
 */
export class AbstractExchangeStatusHintHtmlComponent extends newAbstract(
 _AbstractExchangeStatusHintHtmlComponent,270015307611,null,{
  asIExchangeStatusHintHtmlComponent:1,
  superExchangeStatusHintHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent} */
AbstractExchangeStatusHintHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent} */
function AbstractExchangeStatusHintHtmlComponentClass(){}


AbstractExchangeStatusHintHtmlComponent[$implementations]=[
 __AbstractExchangeStatusHintHtmlComponent,
 HtmlComponent,
 AbstractExchangeStatusHint,
 AbstractExchangeStatusHintGPU,
 AbstractExchangeStatusHintScreenBack,
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  inputsQPs:ExchangeStatusHintInputsQPs,
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_AwaitingStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{AwaitingStatusLa:AwaitingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(AwaitingStatusLa,status=='waiting')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_ConfirmingStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{ConfirmingStatusLa:ConfirmingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ConfirmingStatusLa,status=='confirming')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_ExchangingStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{ExchangingStatusLa:ExchangingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ExchangingStatusLa,status=='exchanging')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_SendingStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{SendingStatusLa:SendingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(SendingStatusLa,status=='sending')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_FinishedStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{FinishedStatusLa:FinishedStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(FinishedStatusLa,status=='finished')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_FailedStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{FailedStatusLa:FailedStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(FailedStatusLa,status=='failed')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_RefundedStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{RefundedStatusLa:RefundedStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(RefundedStatusLa,status=='refunded')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_OverdueStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{OverdueStatusLa:OverdueStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(OverdueStatusLa,status=='overdue')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_HoldStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{HoldStatusLa:HoldStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(HoldStatusLa,status=='hold')
  },
 }),
 AbstractExchangeStatusHintHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintHtmlComponent}*/({
  paint:function $reveal_ExpiredStatusLa({status:status}){
   const{
    asIExchangeStatusHintGPU:{ExpiredStatusLa:ExpiredStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ExpiredStatusLa,status=='expired')
  },
 }),
]