import AbstractExchangeStatusHintProcessor from '../AbstractExchangeStatusHintProcessor'
import {ExchangeStatusHintCore} from '../ExchangeStatusHintCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractExchangeStatusHintComputer} from '../AbstractExchangeStatusHintComputer'
import {AbstractExchangeStatusHintController} from '../AbstractExchangeStatusHintController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHint}
 */
function __AbstractExchangeStatusHint() {}
__AbstractExchangeStatusHint.prototype = /** @type {!_AbstractExchangeStatusHint} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHint}
 */
class _AbstractExchangeStatusHint { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHint} ‎
 */
class AbstractExchangeStatusHint extends newAbstract(
 _AbstractExchangeStatusHint,27001530769,null,{
  asIExchangeStatusHint:1,
  superExchangeStatusHint:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHint} */
AbstractExchangeStatusHint.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHint} */
function AbstractExchangeStatusHintClass(){}

export default AbstractExchangeStatusHint


AbstractExchangeStatusHint[$implementations]=[
 __AbstractExchangeStatusHint,
 ExchangeStatusHintCore,
 AbstractExchangeStatusHintProcessor,
 IntegratedComponent,
 AbstractExchangeStatusHintComputer,
 AbstractExchangeStatusHintController,
]


export {AbstractExchangeStatusHint}