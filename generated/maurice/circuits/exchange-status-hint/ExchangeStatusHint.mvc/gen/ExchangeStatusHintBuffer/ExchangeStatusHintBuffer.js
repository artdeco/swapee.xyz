import {makeBuffers} from '@webcircuits/webcircuits'

export const ExchangeStatusHintBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  status:String,
 }),
})

export default ExchangeStatusHintBuffer