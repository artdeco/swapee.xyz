import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintControllerAR}
 */
function __AbstractExchangeStatusHintControllerAR() {}
__AbstractExchangeStatusHintControllerAR.prototype = /** @type {!_AbstractExchangeStatusHintControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR}
 */
class _AbstractExchangeStatusHintControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeStatusHintControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR} ‎
 */
class AbstractExchangeStatusHintControllerAR extends newAbstract(
 _AbstractExchangeStatusHintControllerAR,270015307622,null,{
  asIExchangeStatusHintControllerAR:1,
  superExchangeStatusHintControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR} */
AbstractExchangeStatusHintControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR} */
function AbstractExchangeStatusHintControllerARClass(){}

export default AbstractExchangeStatusHintControllerAR


AbstractExchangeStatusHintControllerAR[$implementations]=[
 __AbstractExchangeStatusHintControllerAR,
 AR,
 AbstractExchangeStatusHintControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeStatusHintControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]