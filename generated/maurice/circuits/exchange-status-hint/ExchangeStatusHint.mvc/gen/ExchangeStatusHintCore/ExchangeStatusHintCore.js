import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeStatusHintMemoryPQs} from '../../pqs/ExchangeStatusHintMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusHintCore}
 */
function __ExchangeStatusHintCore() {}
__ExchangeStatusHintCore.prototype = /** @type {!_ExchangeStatusHintCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintCore}
 */
class _ExchangeStatusHintCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintCore} ‎
 */
class ExchangeStatusHintCore extends newAbstract(
 _ExchangeStatusHintCore,27001530767,null,{
  asIExchangeStatusHintCore:1,
  superExchangeStatusHintCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintCore} */
ExchangeStatusHintCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintCore} */
function ExchangeStatusHintCoreClass(){}

export default ExchangeStatusHintCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusHintOuterCore}
 */
function __ExchangeStatusHintOuterCore() {}
__ExchangeStatusHintOuterCore.prototype = /** @type {!_ExchangeStatusHintOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusHintOuterCore} */
export function ExchangeStatusHintOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeStatusHintOuterCore.Model}*/
  this.model={
    status: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintOuterCore}
 */
class _ExchangeStatusHintOuterCore { }
/**
 * The _IExchangeStatusHint_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintOuterCore} ‎
 */
export class ExchangeStatusHintOuterCore extends newAbstract(
 _ExchangeStatusHintOuterCore,27001530763,ExchangeStatusHintOuterCoreConstructor,{
  asIExchangeStatusHintOuterCore:1,
  superExchangeStatusHintOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintOuterCore} */
ExchangeStatusHintOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintOuterCore} */
function ExchangeStatusHintOuterCoreClass(){}


ExchangeStatusHintOuterCore[$implementations]=[
 __ExchangeStatusHintOuterCore,
 ExchangeStatusHintOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintOuterCore}*/({
  constructor(){
   mountPins(this.model,'',ExchangeStatusHintMemoryPQs)

  },
 }),
]

ExchangeStatusHintCore[$implementations]=[
 ExchangeStatusHintCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintCore}*/({
  resetCore(){
   this.resetExchangeStatusHintCore()
  },
  resetExchangeStatusHintCore(){
   ExchangeStatusHintOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ExchangeStatusHintOuterCore}*/(
     /**@type {!xyz.swapee.wc.IExchangeStatusHintOuterCore}*/(this)),
   )
  },
 }),
 __ExchangeStatusHintCore,
 ExchangeStatusHintOuterCore,
]

export {ExchangeStatusHintCore}