import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintScreenAR}
 */
function __AbstractExchangeStatusHintScreenAR() {}
__AbstractExchangeStatusHintScreenAR.prototype = /** @type {!_AbstractExchangeStatusHintScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR}
 */
class _AbstractExchangeStatusHintScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeStatusHintScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR} ‎
 */
class AbstractExchangeStatusHintScreenAR extends newAbstract(
 _AbstractExchangeStatusHintScreenAR,270015307626,null,{
  asIExchangeStatusHintScreenAR:1,
  superExchangeStatusHintScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR} */
AbstractExchangeStatusHintScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR} */
function AbstractExchangeStatusHintScreenARClass(){}

export default AbstractExchangeStatusHintScreenAR


AbstractExchangeStatusHintScreenAR[$implementations]=[
 __AbstractExchangeStatusHintScreenAR,
 AR,
 AbstractExchangeStatusHintScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeStatusHintScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractExchangeStatusHintScreenAR}