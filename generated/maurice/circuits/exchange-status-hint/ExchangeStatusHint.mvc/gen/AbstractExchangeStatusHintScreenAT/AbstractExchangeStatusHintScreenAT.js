import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintScreenAT}
 */
function __AbstractExchangeStatusHintScreenAT() {}
__AbstractExchangeStatusHintScreenAT.prototype = /** @type {!_AbstractExchangeStatusHintScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT}
 */
class _AbstractExchangeStatusHintScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeStatusHintScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT} ‎
 */
class AbstractExchangeStatusHintScreenAT extends newAbstract(
 _AbstractExchangeStatusHintScreenAT,270015307627,null,{
  asIExchangeStatusHintScreenAT:1,
  superExchangeStatusHintScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT} */
AbstractExchangeStatusHintScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT} */
function AbstractExchangeStatusHintScreenATClass(){}

export default AbstractExchangeStatusHintScreenAT


AbstractExchangeStatusHintScreenAT[$implementations]=[
 __AbstractExchangeStatusHintScreenAT,
 UartUniversal,
]