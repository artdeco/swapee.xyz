import AbstractExchangeStatusHintScreenAT from '../AbstractExchangeStatusHintScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintScreenBack}
 */
function __AbstractExchangeStatusHintScreenBack() {}
__AbstractExchangeStatusHintScreenBack.prototype = /** @type {!_AbstractExchangeStatusHintScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintScreen}
 */
class _AbstractExchangeStatusHintScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusHintScreen} ‎
 */
class AbstractExchangeStatusHintScreenBack extends newAbstract(
 _AbstractExchangeStatusHintScreenBack,270015307625,null,{
  asIExchangeStatusHintScreen:1,
  superExchangeStatusHintScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreen} */
AbstractExchangeStatusHintScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreen} */
function AbstractExchangeStatusHintScreenBackClass(){}

export default AbstractExchangeStatusHintScreenBack


AbstractExchangeStatusHintScreenBack[$implementations]=[
 __AbstractExchangeStatusHintScreenBack,
 AbstractExchangeStatusHintScreenAT,
]