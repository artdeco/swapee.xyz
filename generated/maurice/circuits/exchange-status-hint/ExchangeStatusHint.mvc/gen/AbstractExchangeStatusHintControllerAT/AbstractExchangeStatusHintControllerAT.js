import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintControllerAT}
 */
function __AbstractExchangeStatusHintControllerAT() {}
__AbstractExchangeStatusHintControllerAT.prototype = /** @type {!_AbstractExchangeStatusHintControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT}
 */
class _AbstractExchangeStatusHintControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeStatusHintControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT} ‎
 */
class AbstractExchangeStatusHintControllerAT extends newAbstract(
 _AbstractExchangeStatusHintControllerAT,270015307623,null,{
  asIExchangeStatusHintControllerAT:1,
  superExchangeStatusHintControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT} */
AbstractExchangeStatusHintControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT} */
function AbstractExchangeStatusHintControllerATClass(){}

export default AbstractExchangeStatusHintControllerAT


AbstractExchangeStatusHintControllerAT[$implementations]=[
 __AbstractExchangeStatusHintControllerAT,
 UartUniversal,
 AbstractExchangeStatusHintControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeStatusHintControllerAT}*/({
  get asIExchangeStatusHintController(){
   return this
  },
 }),
]