import ExchangeStatusHintClassesPQs from '../../pqs/ExchangeStatusHintClassesPQs'
import AbstractExchangeStatusHintScreenAR from '../AbstractExchangeStatusHintScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {ExchangeStatusHintInputsPQs} from '../../pqs/ExchangeStatusHintInputsPQs'
import {ExchangeStatusHintMemoryQPs} from '../../pqs/ExchangeStatusHintMemoryQPs'
import {ExchangeStatusHintVdusPQs} from '../../pqs/ExchangeStatusHintVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintScreen}
 */
function __AbstractExchangeStatusHintScreen() {}
__AbstractExchangeStatusHintScreen.prototype = /** @type {!_AbstractExchangeStatusHintScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintScreen}
 */
class _AbstractExchangeStatusHintScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintScreen} ‎
 */
class AbstractExchangeStatusHintScreen extends newAbstract(
 _AbstractExchangeStatusHintScreen,270015307624,null,{
  asIExchangeStatusHintScreen:1,
  superExchangeStatusHintScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintScreen} */
AbstractExchangeStatusHintScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintScreen} */
function AbstractExchangeStatusHintScreenClass(){}

export default AbstractExchangeStatusHintScreen


AbstractExchangeStatusHintScreen[$implementations]=[
 __AbstractExchangeStatusHintScreen,
 AbstractExchangeStatusHintScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintScreen}*/({
  inputsPQs:ExchangeStatusHintInputsPQs,
  classesPQs:ExchangeStatusHintClassesPQs,
  memoryQPs:ExchangeStatusHintMemoryQPs,
 }),
 Screen,
 AbstractExchangeStatusHintScreenAR,
 AbstractExchangeStatusHintScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintScreen}*/({
  vdusPQs:ExchangeStatusHintVdusPQs,
 }),
 AbstractExchangeStatusHintScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintScreen}*/({
  deduceInputs(){
   const{asIExchangeStatusHintDisplay:{
   }}=this
   return{
   }
  },
 }),
]