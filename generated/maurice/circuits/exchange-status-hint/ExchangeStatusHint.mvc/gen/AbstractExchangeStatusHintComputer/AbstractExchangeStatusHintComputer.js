import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintComputer}
 */
function __AbstractExchangeStatusHintComputer() {}
__AbstractExchangeStatusHintComputer.prototype = /** @type {!_AbstractExchangeStatusHintComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintComputer}
 */
class _AbstractExchangeStatusHintComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintComputer} ‎
 */
export class AbstractExchangeStatusHintComputer extends newAbstract(
 _AbstractExchangeStatusHintComputer,27001530761,null,{
  asIExchangeStatusHintComputer:1,
  superExchangeStatusHintComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintComputer} */
AbstractExchangeStatusHintComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintComputer} */
function AbstractExchangeStatusHintComputerClass(){}


AbstractExchangeStatusHintComputer[$implementations]=[
 __AbstractExchangeStatusHintComputer,
 Adapter,
]


export default AbstractExchangeStatusHintComputer