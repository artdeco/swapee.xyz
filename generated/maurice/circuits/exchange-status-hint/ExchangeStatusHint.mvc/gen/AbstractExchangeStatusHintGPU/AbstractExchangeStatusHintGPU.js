import AbstractExchangeStatusHintDisplay from '../AbstractExchangeStatusHintDisplayBack'
import ExchangeStatusHintClassesPQs from '../../pqs/ExchangeStatusHintClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {ExchangeStatusHintClassesQPs} from '../../pqs/ExchangeStatusHintClassesQPs'
import {ExchangeStatusHintVdusPQs} from '../../pqs/ExchangeStatusHintVdusPQs'
import {ExchangeStatusHintVdusQPs} from '../../pqs/ExchangeStatusHintVdusQPs'
import {ExchangeStatusHintMemoryPQs} from '../../pqs/ExchangeStatusHintMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusHintGPU}
 */
function __AbstractExchangeStatusHintGPU() {}
__AbstractExchangeStatusHintGPU.prototype = /** @type {!_AbstractExchangeStatusHintGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintGPU}
 */
class _AbstractExchangeStatusHintGPU { }
/**
 * Handles the periphery of the _IExchangeStatusHintDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintGPU} ‎
 */
class AbstractExchangeStatusHintGPU extends newAbstract(
 _AbstractExchangeStatusHintGPU,270015307615,null,{
  asIExchangeStatusHintGPU:1,
  superExchangeStatusHintGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintGPU} */
AbstractExchangeStatusHintGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintGPU} */
function AbstractExchangeStatusHintGPUClass(){}

export default AbstractExchangeStatusHintGPU


AbstractExchangeStatusHintGPU[$implementations]=[
 __AbstractExchangeStatusHintGPU,
 AbstractExchangeStatusHintGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintGPU}*/({
  classesQPs:ExchangeStatusHintClassesQPs,
  vdusPQs:ExchangeStatusHintVdusPQs,
  vdusQPs:ExchangeStatusHintVdusQPs,
  memoryPQs:ExchangeStatusHintMemoryPQs,
 }),
 AbstractExchangeStatusHintDisplay,
 BrowserView,
 AbstractExchangeStatusHintGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusHintGPU}*/({
  allocator(){
   pressFit(this.classes,'',ExchangeStatusHintClassesPQs)
  },
 }),
]