import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractExchangeStatusHint}*/
export class AbstractExchangeStatusHint extends Module['27001530761'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHint} */
AbstractExchangeStatusHint.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeStatusHintPort} */
export const ExchangeStatusHintPort=Module['27001530763']
/**@extends {xyz.swapee.wc.AbstractExchangeStatusHintController}*/
export class AbstractExchangeStatusHintController extends Module['27001530764'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintController} */
AbstractExchangeStatusHintController.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent} */
export const ExchangeStatusHintHtmlComponent=Module['270015307610']
/** @type {typeof xyz.swapee.wc.ExchangeStatusHintBuffer} */
export const ExchangeStatusHintBuffer=Module['270015307611']
/**@extends {xyz.swapee.wc.AbstractExchangeStatusHintComputer}*/
export class AbstractExchangeStatusHintComputer extends Module['270015307630'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintComputer} */
AbstractExchangeStatusHintComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.back.ExchangeStatusHintController} */
export const ExchangeStatusHintController=Module['270015307661']