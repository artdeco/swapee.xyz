/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHint` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHint}
 */
class AbstractExchangeStatusHint extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeStatusHint_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeStatusHintPort}
 */
class ExchangeStatusHintPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintController}
 */
class AbstractExchangeStatusHintController extends (class {/* lazy-loaded */}) {}
/**
 * The _IExchangeStatusHint_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ExchangeStatusHintHtmlComponent}
 */
class ExchangeStatusHintHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeStatusHintBuffer}
 */
class ExchangeStatusHintBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintComputer}
 */
class AbstractExchangeStatusHintComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ExchangeStatusHintController}
 */
class ExchangeStatusHintController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeStatusHint = AbstractExchangeStatusHint
module.exports.ExchangeStatusHintPort = ExchangeStatusHintPort
module.exports.AbstractExchangeStatusHintController = AbstractExchangeStatusHintController
module.exports.ExchangeStatusHintHtmlComponent = ExchangeStatusHintHtmlComponent
module.exports.ExchangeStatusHintBuffer = ExchangeStatusHintBuffer
module.exports.AbstractExchangeStatusHintComputer = AbstractExchangeStatusHintComputer
module.exports.ExchangeStatusHintController = ExchangeStatusHintController