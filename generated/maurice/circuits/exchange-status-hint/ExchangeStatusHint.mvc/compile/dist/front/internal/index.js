import Module from './browser'

/** @type {typeof xyz.swapee.wc.ExchangeStatusHintDisplay} */
export const ExchangeStatusHintDisplay=Module['270015307641']
/** @type {typeof xyz.swapee.wc.ExchangeStatusHintScreen} */
export const ExchangeStatusHintScreen=Module['270015307671']