/**
 * Display for presenting information from the _IExchangeStatusHint_.
 * @extends {xyz.swapee.wc.ExchangeStatusHintDisplay}
 */
class ExchangeStatusHintDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ExchangeStatusHintScreen}
 */
class ExchangeStatusHintScreen extends (class {/* lazy-loaded */}) {}

module.exports.ExchangeStatusHintDisplay = ExchangeStatusHintDisplay
module.exports.ExchangeStatusHintScreen = ExchangeStatusHintScreen