/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHint` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHint}
 */
class AbstractExchangeStatusHint extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeStatusHint_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeStatusHintPort}
 */
class ExchangeStatusHintPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintController}
 */
class AbstractExchangeStatusHintController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IExchangeStatusHint_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ExchangeStatusHintElement}
 */
class ExchangeStatusHintElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeStatusHintBuffer}
 */
class ExchangeStatusHintBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusHintComputer}
 */
class AbstractExchangeStatusHintComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ExchangeStatusHintController}
 */
class ExchangeStatusHintController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeStatusHint = AbstractExchangeStatusHint
module.exports.ExchangeStatusHintPort = ExchangeStatusHintPort
module.exports.AbstractExchangeStatusHintController = AbstractExchangeStatusHintController
module.exports.ExchangeStatusHintElement = ExchangeStatusHintElement
module.exports.ExchangeStatusHintBuffer = ExchangeStatusHintBuffer
module.exports.AbstractExchangeStatusHintComputer = AbstractExchangeStatusHintComputer
module.exports.ExchangeStatusHintController = ExchangeStatusHintController

Object.defineProperties(module.exports, {
 'AbstractExchangeStatusHint': {get: () => require('./precompile/internal')[27001530761]},
 [27001530761]: {get: () => module.exports['AbstractExchangeStatusHint']},
 'ExchangeStatusHintPort': {get: () => require('./precompile/internal')[27001530763]},
 [27001530763]: {get: () => module.exports['ExchangeStatusHintPort']},
 'AbstractExchangeStatusHintController': {get: () => require('./precompile/internal')[27001530764]},
 [27001530764]: {get: () => module.exports['AbstractExchangeStatusHintController']},
 'ExchangeStatusHintElement': {get: () => require('./precompile/internal')[27001530768]},
 [27001530768]: {get: () => module.exports['ExchangeStatusHintElement']},
 'ExchangeStatusHintBuffer': {get: () => require('./precompile/internal')[270015307611]},
 [270015307611]: {get: () => module.exports['ExchangeStatusHintBuffer']},
 'AbstractExchangeStatusHintComputer': {get: () => require('./precompile/internal')[270015307630]},
 [270015307630]: {get: () => module.exports['AbstractExchangeStatusHintComputer']},
 'ExchangeStatusHintController': {get: () => require('./precompile/internal')[270015307661]},
 [270015307661]: {get: () => module.exports['ExchangeStatusHintController']},
})