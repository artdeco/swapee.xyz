/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IExchangeStatusHintComputer={}
xyz.swapee.wc.IExchangeStatusHintOuterCore={}
xyz.swapee.wc.IExchangeStatusHintOuterCore.Model={}
xyz.swapee.wc.IExchangeStatusHintOuterCore.Model.Status={}
xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel={}
xyz.swapee.wc.IExchangeStatusHintPort={}
xyz.swapee.wc.IExchangeStatusHintPort.Inputs={}
xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs={}
xyz.swapee.wc.IExchangeStatusHintCore={}
xyz.swapee.wc.IExchangeStatusHintCore.Model={}
xyz.swapee.wc.IExchangeStatusHintPortInterface={}
xyz.swapee.wc.IExchangeStatusHintProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IExchangeStatusHintController={}
xyz.swapee.wc.front.IExchangeStatusHintControllerAT={}
xyz.swapee.wc.front.IExchangeStatusHintScreenAR={}
xyz.swapee.wc.IExchangeStatusHint={}
xyz.swapee.wc.IExchangeStatusHintHtmlComponent={}
xyz.swapee.wc.IExchangeStatusHintElement={}
xyz.swapee.wc.IExchangeStatusHintElementPort={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs={}
xyz.swapee.wc.IExchangeStatusHintDesigner={}
xyz.swapee.wc.IExchangeStatusHintDesigner.communicator={}
xyz.swapee.wc.IExchangeStatusHintDesigner.relay={}
xyz.swapee.wc.IExchangeStatusHintDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IExchangeStatusHintDisplay={}
xyz.swapee.wc.back.IExchangeStatusHintController={}
xyz.swapee.wc.back.IExchangeStatusHintControllerAR={}
xyz.swapee.wc.back.IExchangeStatusHintScreen={}
xyz.swapee.wc.back.IExchangeStatusHintScreenAT={}
xyz.swapee.wc.IExchangeStatusHintController={}
xyz.swapee.wc.IExchangeStatusHintScreen={}
xyz.swapee.wc.IExchangeStatusHintGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/02-IExchangeStatusHintComputer.xml}  d034ef142c59c1823551f2c63d0bf8df */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IExchangeStatusHintComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintComputer)} xyz.swapee.wc.AbstractExchangeStatusHintComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintComputer} xyz.swapee.wc.ExchangeStatusHintComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintComputer` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintComputer
 */
xyz.swapee.wc.AbstractExchangeStatusHintComputer = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintComputer.constructor&xyz.swapee.wc.ExchangeStatusHintComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintComputer.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintComputer.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusHintComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusHintComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusHintComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusHintComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintComputer.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintComputer} xyz.swapee.wc.ExchangeStatusHintComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.ExchangeStatusHintMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IExchangeStatusHintComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IExchangeStatusHintComputer
 */
xyz.swapee.wc.IExchangeStatusHintComputer = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusHintComputer.compute} */
xyz.swapee.wc.IExchangeStatusHintComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintComputer.Initialese>)} xyz.swapee.wc.ExchangeStatusHintComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintComputer} xyz.swapee.wc.IExchangeStatusHintComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeStatusHintComputer_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintComputer
 * @implements {xyz.swapee.wc.IExchangeStatusHintComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintComputer.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintComputer = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintComputer.constructor&xyz.swapee.wc.IExchangeStatusHintComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintComputer}
 */
xyz.swapee.wc.ExchangeStatusHintComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusHintComputer} */
xyz.swapee.wc.RecordIExchangeStatusHintComputer

/** @typedef {xyz.swapee.wc.IExchangeStatusHintComputer} xyz.swapee.wc.BoundIExchangeStatusHintComputer */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintComputer} xyz.swapee.wc.BoundExchangeStatusHintComputer */

/**
 * Contains getters to cast the _IExchangeStatusHintComputer_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintComputerCaster
 */
xyz.swapee.wc.IExchangeStatusHintComputerCaster = class { }
/**
 * Cast the _IExchangeStatusHintComputer_ instance into the _BoundIExchangeStatusHintComputer_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintComputer}
 */
xyz.swapee.wc.IExchangeStatusHintComputerCaster.prototype.asIExchangeStatusHintComputer
/**
 * Access the _ExchangeStatusHintComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintComputer}
 */
xyz.swapee.wc.IExchangeStatusHintComputerCaster.prototype.superExchangeStatusHintComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.ExchangeStatusHintMemory) => void} xyz.swapee.wc.IExchangeStatusHintComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintComputer.__compute<!xyz.swapee.wc.IExchangeStatusHintComputer>} xyz.swapee.wc.IExchangeStatusHintComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.ExchangeStatusHintMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusHintComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusHintComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/03-IExchangeStatusHintOuterCore.xml}  b43fb6150749258247778de2d08234b1 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeStatusHintOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintOuterCore)} xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintOuterCore} xyz.swapee.wc.ExchangeStatusHintOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintOuterCore
 */
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.constructor&xyz.swapee.wc.ExchangeStatusHintOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintOuterCore|typeof xyz.swapee.wc.ExchangeStatusHintOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintOuterCore|typeof xyz.swapee.wc.ExchangeStatusHintOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintOuterCore|typeof xyz.swapee.wc.ExchangeStatusHintOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintOuterCoreCaster)} xyz.swapee.wc.IExchangeStatusHintOuterCore.constructor */
/**
 * The _IExchangeStatusHint_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IExchangeStatusHintOuterCore
 */
xyz.swapee.wc.IExchangeStatusHintOuterCore = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusHintOuterCore.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintOuterCore

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintOuterCore.Initialese>)} xyz.swapee.wc.ExchangeStatusHintOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintOuterCore} xyz.swapee.wc.IExchangeStatusHintOuterCore.typeof */
/**
 * A concrete class of _IExchangeStatusHintOuterCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintOuterCore
 * @implements {xyz.swapee.wc.IExchangeStatusHintOuterCore} The _IExchangeStatusHint_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintOuterCore = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintOuterCore.constructor&xyz.swapee.wc.IExchangeStatusHintOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusHintOuterCore.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintOuterCore}
 */
xyz.swapee.wc.ExchangeStatusHintOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintOuterCore.
 * @interface xyz.swapee.wc.IExchangeStatusHintOuterCoreFields
 */
xyz.swapee.wc.IExchangeStatusHintOuterCoreFields = class { }
/**
 * The _IExchangeStatusHint_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IExchangeStatusHintOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeStatusHintOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore} */
xyz.swapee.wc.RecordIExchangeStatusHintOuterCore

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore} xyz.swapee.wc.BoundIExchangeStatusHintOuterCore */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintOuterCore} xyz.swapee.wc.BoundExchangeStatusHintOuterCore */

/**
 * The status.
 * @typedef {string}
 */
xyz.swapee.wc.IExchangeStatusHintOuterCore.Model.Status.status

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.Model.Status} xyz.swapee.wc.IExchangeStatusHintOuterCore.Model The _IExchangeStatusHint_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.Status} xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel The _IExchangeStatusHint_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IExchangeStatusHintOuterCore_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintOuterCoreCaster
 */
xyz.swapee.wc.IExchangeStatusHintOuterCoreCaster = class { }
/**
 * Cast the _IExchangeStatusHintOuterCore_ instance into the _BoundIExchangeStatusHintOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintOuterCore}
 */
xyz.swapee.wc.IExchangeStatusHintOuterCoreCaster.prototype.asIExchangeStatusHintOuterCore
/**
 * Access the _ExchangeStatusHintOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintOuterCore}
 */
xyz.swapee.wc.IExchangeStatusHintOuterCoreCaster.prototype.superExchangeStatusHintOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintOuterCore.Model.Status The status (optional overlay).
 * @prop {string} [status=""] The status. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintOuterCore.Model.Status_Safe The status (required overlay).
 * @prop {string} status The status.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.Status The status (optional overlay).
 * @prop {*} [status=null] The status. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.Status_Safe The status (required overlay).
 * @prop {*} status The status.
 */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.Status} xyz.swapee.wc.IExchangeStatusHintPort.Inputs.Status The status (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.Status_Safe} xyz.swapee.wc.IExchangeStatusHintPort.Inputs.Status_Safe The status (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.Status} xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs.Status The status (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.Status_Safe} xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs.Status_Safe The status (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.Model.Status} xyz.swapee.wc.IExchangeStatusHintCore.Model.Status The status (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.Model.Status_Safe} xyz.swapee.wc.IExchangeStatusHintCore.Model.Status_Safe The status (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/04-IExchangeStatusHintPort.xml}  60ce5814e61cc10e1d9574c14b4d0360 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeStatusHintPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintPort)} xyz.swapee.wc.AbstractExchangeStatusHintPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintPort} xyz.swapee.wc.ExchangeStatusHintPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintPort
 */
xyz.swapee.wc.AbstractExchangeStatusHintPort = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintPort.constructor&xyz.swapee.wc.ExchangeStatusHintPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintPort.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintPort|typeof xyz.swapee.wc.ExchangeStatusHintPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintPort|typeof xyz.swapee.wc.ExchangeStatusHintPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintPort|typeof xyz.swapee.wc.ExchangeStatusHintPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintPort.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintPort} xyz.swapee.wc.ExchangeStatusHintPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeStatusHintPort.Inputs>)} xyz.swapee.wc.IExchangeStatusHintPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IExchangeStatusHint_, providing input
 * pins.
 * @interface xyz.swapee.wc.IExchangeStatusHintPort
 */
xyz.swapee.wc.IExchangeStatusHintPort = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusHintPort.resetPort} */
xyz.swapee.wc.IExchangeStatusHintPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusHintPort.resetExchangeStatusHintPort} */
xyz.swapee.wc.IExchangeStatusHintPort.prototype.resetExchangeStatusHintPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintPort.Initialese>)} xyz.swapee.wc.ExchangeStatusHintPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintPort} xyz.swapee.wc.IExchangeStatusHintPort.typeof */
/**
 * A concrete class of _IExchangeStatusHintPort_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintPort
 * @implements {xyz.swapee.wc.IExchangeStatusHintPort} The port that serves as an interface to the _IExchangeStatusHint_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintPort = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintPort.constructor&xyz.swapee.wc.IExchangeStatusHintPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintPort}
 */
xyz.swapee.wc.ExchangeStatusHintPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintPort.
 * @interface xyz.swapee.wc.IExchangeStatusHintPortFields
 */
xyz.swapee.wc.IExchangeStatusHintPortFields = class { }
/**
 * The inputs to the _IExchangeStatusHint_'s controller via its port.
 */
xyz.swapee.wc.IExchangeStatusHintPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusHintPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeStatusHintPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeStatusHintPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintPort} */
xyz.swapee.wc.RecordIExchangeStatusHintPort

/** @typedef {xyz.swapee.wc.IExchangeStatusHintPort} xyz.swapee.wc.BoundIExchangeStatusHintPort */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintPort} xyz.swapee.wc.BoundExchangeStatusHintPort */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel)} xyz.swapee.wc.IExchangeStatusHintPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel} xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IExchangeStatusHint_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeStatusHintPort.Inputs
 */
xyz.swapee.wc.IExchangeStatusHintPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintPort.Inputs.constructor&xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusHintPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel)} xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs.constructor */
/**
 * The inputs to the _IExchangeStatusHint_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs
 */
xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs.constructor&xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintPortInterface
 */
xyz.swapee.wc.IExchangeStatusHintPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IExchangeStatusHintPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IExchangeStatusHintPortInterface.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintPortInterface

/**
 * A concrete class of _IExchangeStatusHintPortInterface_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintPortInterface
 * @implements {xyz.swapee.wc.IExchangeStatusHintPortInterface} The port interface.
 */
xyz.swapee.wc.ExchangeStatusHintPortInterface = class extends xyz.swapee.wc.IExchangeStatusHintPortInterface { }
xyz.swapee.wc.ExchangeStatusHintPortInterface.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintPortInterface.Props
 * @prop {string} status The status.
 */

/**
 * Contains getters to cast the _IExchangeStatusHintPort_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintPortCaster
 */
xyz.swapee.wc.IExchangeStatusHintPortCaster = class { }
/**
 * Cast the _IExchangeStatusHintPort_ instance into the _BoundIExchangeStatusHintPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintPort}
 */
xyz.swapee.wc.IExchangeStatusHintPortCaster.prototype.asIExchangeStatusHintPort
/**
 * Access the _ExchangeStatusHintPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintPort}
 */
xyz.swapee.wc.IExchangeStatusHintPortCaster.prototype.superExchangeStatusHintPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusHintPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintPort.__resetPort<!xyz.swapee.wc.IExchangeStatusHintPort>} xyz.swapee.wc.IExchangeStatusHintPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintPort.resetPort} */
/**
 * Resets the _IExchangeStatusHint_ port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusHintPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusHintPort.__resetExchangeStatusHintPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintPort.__resetExchangeStatusHintPort<!xyz.swapee.wc.IExchangeStatusHintPort>} xyz.swapee.wc.IExchangeStatusHintPort._resetExchangeStatusHintPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintPort.resetExchangeStatusHintPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusHintPort.resetExchangeStatusHintPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusHintPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/09-IExchangeStatusHintCore.xml}  8c3f513bda86b6bc426911ec1ad7f97b */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeStatusHintCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintCore)} xyz.swapee.wc.AbstractExchangeStatusHintCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintCore} xyz.swapee.wc.ExchangeStatusHintCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintCore
 */
xyz.swapee.wc.AbstractExchangeStatusHintCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintCore.constructor&xyz.swapee.wc.ExchangeStatusHintCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintCore|typeof xyz.swapee.wc.ExchangeStatusHintCore)|(!xyz.swapee.wc.IExchangeStatusHintOuterCore|typeof xyz.swapee.wc.ExchangeStatusHintOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintCore|typeof xyz.swapee.wc.ExchangeStatusHintCore)|(!xyz.swapee.wc.IExchangeStatusHintOuterCore|typeof xyz.swapee.wc.ExchangeStatusHintOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintCore|typeof xyz.swapee.wc.ExchangeStatusHintCore)|(!xyz.swapee.wc.IExchangeStatusHintOuterCore|typeof xyz.swapee.wc.ExchangeStatusHintOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintCore}
 */
xyz.swapee.wc.AbstractExchangeStatusHintCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintCoreCaster&xyz.swapee.wc.IExchangeStatusHintOuterCore)} xyz.swapee.wc.IExchangeStatusHintCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IExchangeStatusHintCore
 */
xyz.swapee.wc.IExchangeStatusHintCore = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusHintOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IExchangeStatusHintCore.resetCore} */
xyz.swapee.wc.IExchangeStatusHintCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusHintCore.resetExchangeStatusHintCore} */
xyz.swapee.wc.IExchangeStatusHintCore.prototype.resetExchangeStatusHintCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintCore.Initialese>)} xyz.swapee.wc.ExchangeStatusHintCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintCore} xyz.swapee.wc.IExchangeStatusHintCore.typeof */
/**
 * A concrete class of _IExchangeStatusHintCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintCore
 * @implements {xyz.swapee.wc.IExchangeStatusHintCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintCore = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintCore.constructor&xyz.swapee.wc.IExchangeStatusHintCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusHintCore.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintCore}
 */
xyz.swapee.wc.ExchangeStatusHintCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintCore.
 * @interface xyz.swapee.wc.IExchangeStatusHintCoreFields
 */
xyz.swapee.wc.IExchangeStatusHintCoreFields = class { }
/**
 * The _IExchangeStatusHint_'s memory.
 */
xyz.swapee.wc.IExchangeStatusHintCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeStatusHintCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IExchangeStatusHintCoreFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeStatusHintCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintCore} */
xyz.swapee.wc.RecordIExchangeStatusHintCore

/** @typedef {xyz.swapee.wc.IExchangeStatusHintCore} xyz.swapee.wc.BoundIExchangeStatusHintCore */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintCore} xyz.swapee.wc.BoundExchangeStatusHintCore */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintOuterCore.Model} xyz.swapee.wc.IExchangeStatusHintCore.Model The _IExchangeStatusHint_'s memory. */

/**
 * Contains getters to cast the _IExchangeStatusHintCore_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintCoreCaster
 */
xyz.swapee.wc.IExchangeStatusHintCoreCaster = class { }
/**
 * Cast the _IExchangeStatusHintCore_ instance into the _BoundIExchangeStatusHintCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintCore}
 */
xyz.swapee.wc.IExchangeStatusHintCoreCaster.prototype.asIExchangeStatusHintCore
/**
 * Access the _ExchangeStatusHintCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintCore}
 */
xyz.swapee.wc.IExchangeStatusHintCoreCaster.prototype.superExchangeStatusHintCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusHintCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintCore.__resetCore<!xyz.swapee.wc.IExchangeStatusHintCore>} xyz.swapee.wc.IExchangeStatusHintCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintCore.resetCore} */
/**
 * Resets the _IExchangeStatusHint_ core.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusHintCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusHintCore.__resetExchangeStatusHintCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintCore.__resetExchangeStatusHintCore<!xyz.swapee.wc.IExchangeStatusHintCore>} xyz.swapee.wc.IExchangeStatusHintCore._resetExchangeStatusHintCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintCore.resetExchangeStatusHintCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusHintCore.resetExchangeStatusHintCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusHintCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/10-IExchangeStatusHintProcessor.xml}  cfbd79caec8a581b580245b9365bad91 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintComputer.Initialese&xyz.swapee.wc.IExchangeStatusHintController.Initialese} xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintProcessor)} xyz.swapee.wc.AbstractExchangeStatusHintProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintProcessor} xyz.swapee.wc.ExchangeStatusHintProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintProcessor
 */
xyz.swapee.wc.AbstractExchangeStatusHintProcessor = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintProcessor.constructor&xyz.swapee.wc.ExchangeStatusHintProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintProcessor.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!xyz.swapee.wc.IExchangeStatusHintCore|typeof xyz.swapee.wc.ExchangeStatusHintCore)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusHintProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusHintProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!xyz.swapee.wc.IExchangeStatusHintCore|typeof xyz.swapee.wc.ExchangeStatusHintCore)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusHintProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!xyz.swapee.wc.IExchangeStatusHintCore|typeof xyz.swapee.wc.ExchangeStatusHintCore)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusHintProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintProcessor} xyz.swapee.wc.ExchangeStatusHintProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintProcessorCaster&xyz.swapee.wc.IExchangeStatusHintComputer&xyz.swapee.wc.IExchangeStatusHintCore&xyz.swapee.wc.IExchangeStatusHintController)} xyz.swapee.wc.IExchangeStatusHintProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintController} xyz.swapee.wc.IExchangeStatusHintController.typeof */
/**
 * The processor to compute changes to the memory for the _IExchangeStatusHint_.
 * @interface xyz.swapee.wc.IExchangeStatusHintProcessor
 */
xyz.swapee.wc.IExchangeStatusHintProcessor = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusHintComputer.typeof&xyz.swapee.wc.IExchangeStatusHintCore.typeof&xyz.swapee.wc.IExchangeStatusHintController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese>)} xyz.swapee.wc.ExchangeStatusHintProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintProcessor} xyz.swapee.wc.IExchangeStatusHintProcessor.typeof */
/**
 * A concrete class of _IExchangeStatusHintProcessor_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintProcessor
 * @implements {xyz.swapee.wc.IExchangeStatusHintProcessor} The processor to compute changes to the memory for the _IExchangeStatusHint_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintProcessor = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintProcessor.constructor&xyz.swapee.wc.IExchangeStatusHintProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintProcessor}
 */
xyz.swapee.wc.ExchangeStatusHintProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusHintProcessor} */
xyz.swapee.wc.RecordIExchangeStatusHintProcessor

/** @typedef {xyz.swapee.wc.IExchangeStatusHintProcessor} xyz.swapee.wc.BoundIExchangeStatusHintProcessor */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintProcessor} xyz.swapee.wc.BoundExchangeStatusHintProcessor */

/**
 * Contains getters to cast the _IExchangeStatusHintProcessor_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintProcessorCaster
 */
xyz.swapee.wc.IExchangeStatusHintProcessorCaster = class { }
/**
 * Cast the _IExchangeStatusHintProcessor_ instance into the _BoundIExchangeStatusHintProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintProcessor}
 */
xyz.swapee.wc.IExchangeStatusHintProcessorCaster.prototype.asIExchangeStatusHintProcessor
/**
 * Access the _ExchangeStatusHintProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintProcessor}
 */
xyz.swapee.wc.IExchangeStatusHintProcessorCaster.prototype.superExchangeStatusHintProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/100-ExchangeStatusHintMemory.xml}  43df59c7bd6033716f4940a0a361d132 */
/**
 * The memory of the _IExchangeStatusHint_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.ExchangeStatusHintMemory
 */
xyz.swapee.wc.ExchangeStatusHintMemory = class { }
/**
 * The status. Default empty string.
 */
xyz.swapee.wc.ExchangeStatusHintMemory.prototype.status = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/102-ExchangeStatusHintInputs.xml}  68347356b38116c6cf96095a52d963e4 */
/**
 * The inputs of the _IExchangeStatusHint_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.ExchangeStatusHintInputs
 */
xyz.swapee.wc.front.ExchangeStatusHintInputs = class { }
/**
 * The status. Default empty string.
 */
xyz.swapee.wc.front.ExchangeStatusHintInputs.prototype.status = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/11-IExchangeStatusHint.xml}  80735daa7e0f4de9411b6a7c7d414ee0 */
/**
 * An atomic wrapper for the _IExchangeStatusHint_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.ExchangeStatusHintEnv
 */
xyz.swapee.wc.ExchangeStatusHintEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.ExchangeStatusHintEnv.prototype.exchangeStatusHint = /** @type {xyz.swapee.wc.IExchangeStatusHint} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.IExchangeStatusHintController.Inputs>&xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese&xyz.swapee.wc.IExchangeStatusHintComputer.Initialese&xyz.swapee.wc.IExchangeStatusHintController.Initialese} xyz.swapee.wc.IExchangeStatusHint.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHint)} xyz.swapee.wc.AbstractExchangeStatusHint.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHint} xyz.swapee.wc.ExchangeStatusHint.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHint` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHint
 */
xyz.swapee.wc.AbstractExchangeStatusHint = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHint.constructor&xyz.swapee.wc.ExchangeStatusHint.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHint.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHint
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHint.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHint} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHint|typeof xyz.swapee.wc.ExchangeStatusHint)|(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHint}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHint.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHint}
 */
xyz.swapee.wc.AbstractExchangeStatusHint.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHint}
 */
xyz.swapee.wc.AbstractExchangeStatusHint.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHint|typeof xyz.swapee.wc.ExchangeStatusHint)|(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHint}
 */
xyz.swapee.wc.AbstractExchangeStatusHint.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHint|typeof xyz.swapee.wc.ExchangeStatusHint)|(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHint}
 */
xyz.swapee.wc.AbstractExchangeStatusHint.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHint.Initialese[]) => xyz.swapee.wc.IExchangeStatusHint} xyz.swapee.wc.ExchangeStatusHintConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHint.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IExchangeStatusHint.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IExchangeStatusHint.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IExchangeStatusHint.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.ExchangeStatusHintMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.ExchangeStatusHintClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintCaster&xyz.swapee.wc.IExchangeStatusHintProcessor&xyz.swapee.wc.IExchangeStatusHintComputer&xyz.swapee.wc.IExchangeStatusHintController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.IExchangeStatusHintController.Inputs, null>)} xyz.swapee.wc.IExchangeStatusHint.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IExchangeStatusHint
 */
xyz.swapee.wc.IExchangeStatusHint = class extends /** @type {xyz.swapee.wc.IExchangeStatusHint.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusHintProcessor.typeof&xyz.swapee.wc.IExchangeStatusHintComputer.typeof&xyz.swapee.wc.IExchangeStatusHintController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHint* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHint.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHint.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHint&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHint.Initialese>)} xyz.swapee.wc.ExchangeStatusHint.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHint} xyz.swapee.wc.IExchangeStatusHint.typeof */
/**
 * A concrete class of _IExchangeStatusHint_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHint
 * @implements {xyz.swapee.wc.IExchangeStatusHint} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHint.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHint = class extends /** @type {xyz.swapee.wc.ExchangeStatusHint.constructor&xyz.swapee.wc.IExchangeStatusHint.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHint* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHint.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHint* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHint.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHint.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHint}
 */
xyz.swapee.wc.ExchangeStatusHint.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHint.
 * @interface xyz.swapee.wc.IExchangeStatusHintFields
 */
xyz.swapee.wc.IExchangeStatusHintFields = class { }
/**
 * The input pins of the _IExchangeStatusHint_ port.
 */
xyz.swapee.wc.IExchangeStatusHintFields.prototype.pinout = /** @type {!xyz.swapee.wc.IExchangeStatusHint.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHint} */
xyz.swapee.wc.RecordIExchangeStatusHint

/** @typedef {xyz.swapee.wc.IExchangeStatusHint} xyz.swapee.wc.BoundIExchangeStatusHint */

/** @typedef {xyz.swapee.wc.ExchangeStatusHint} xyz.swapee.wc.BoundExchangeStatusHint */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintController.Inputs} xyz.swapee.wc.IExchangeStatusHint.Pinout The input pins of the _IExchangeStatusHint_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeStatusHintController.Inputs>)} xyz.swapee.wc.IExchangeStatusHintBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IExchangeStatusHintBuffer
 */
xyz.swapee.wc.IExchangeStatusHintBuffer = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusHintBuffer.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintBuffer

/**
 * A concrete class of _IExchangeStatusHintBuffer_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintBuffer
 * @implements {xyz.swapee.wc.IExchangeStatusHintBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.ExchangeStatusHintBuffer = class extends xyz.swapee.wc.IExchangeStatusHintBuffer { }
xyz.swapee.wc.ExchangeStatusHintBuffer.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintBuffer

/**
 * Contains getters to cast the _IExchangeStatusHint_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintCaster
 */
xyz.swapee.wc.IExchangeStatusHintCaster = class { }
/**
 * Cast the _IExchangeStatusHint_ instance into the _BoundIExchangeStatusHint_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHint}
 */
xyz.swapee.wc.IExchangeStatusHintCaster.prototype.asIExchangeStatusHint
/**
 * Access the _ExchangeStatusHint_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHint}
 */
xyz.swapee.wc.IExchangeStatusHintCaster.prototype.superExchangeStatusHint

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/110-ExchangeStatusHintSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeStatusHintMemoryPQs
 */
xyz.swapee.wc.ExchangeStatusHintMemoryPQs = class {
  constructor() {
    /**
     * `jacb4`
     */
    this.status=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeStatusHintMemoryPQs.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeStatusHintMemoryQPs
 * @dict
 */
xyz.swapee.wc.ExchangeStatusHintMemoryQPs = class { }
/**
 * `status`
 */
xyz.swapee.wc.ExchangeStatusHintMemoryQPs.prototype.jacb4 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintMemoryPQs)} xyz.swapee.wc.ExchangeStatusHintInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintMemoryPQs} xyz.swapee.wc.ExchangeStatusHintMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeStatusHintInputsPQs
 */
xyz.swapee.wc.ExchangeStatusHintInputsPQs = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintInputsPQs.constructor&xyz.swapee.wc.ExchangeStatusHintMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusHintInputsPQs.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintInputsPQs

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintMemoryPQs)} xyz.swapee.wc.ExchangeStatusHintInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeStatusHintInputsQPs
 * @dict
 */
xyz.swapee.wc.ExchangeStatusHintInputsQPs = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintInputsQPs.constructor&xyz.swapee.wc.ExchangeStatusHintMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusHintInputsQPs.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeStatusHintVdusPQs
 */
xyz.swapee.wc.ExchangeStatusHintVdusPQs = class {
  constructor() {
    /**
     * `ee221`
     */
    this.AwaitingStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee222`
     */
    this.ConfirmingStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee223`
     */
    this.ExchangingStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee224`
     */
    this.SendingStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee225`
     */
    this.FinishedStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee226`
     */
    this.FailedStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee227`
     */
    this.RefundedStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee228`
     */
    this.OverdueStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee229`
     */
    this.HoldStatusLa=/** @type {string} */ (void 0)
    /**
     * `ee2210`
     */
    this.ExpiredStatusLa=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeStatusHintVdusPQs.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeStatusHintVdusQPs
 * @dict
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs = class { }
/**
 * `AwaitingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee221 = /** @type {string} */ (void 0)
/**
 * `ConfirmingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee222 = /** @type {string} */ (void 0)
/**
 * `ExchangingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee223 = /** @type {string} */ (void 0)
/**
 * `SendingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee224 = /** @type {string} */ (void 0)
/**
 * `FinishedStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee225 = /** @type {string} */ (void 0)
/**
 * `FailedStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee226 = /** @type {string} */ (void 0)
/**
 * `RefundedStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee227 = /** @type {string} */ (void 0)
/**
 * `OverdueStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee228 = /** @type {string} */ (void 0)
/**
 * `HoldStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee229 = /** @type {string} */ (void 0)
/**
 * `ExpiredStatusLa`
 */
xyz.swapee.wc.ExchangeStatusHintVdusQPs.prototype.ee2210 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/12-IExchangeStatusHintHtmlComponent.xml}  a5ee8d1e46162863625f68acd89b8765 */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintController.Initialese&xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese&xyz.swapee.wc.IExchangeStatusHint.Initialese&xyz.swapee.wc.IExchangeStatusHintGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IExchangeStatusHintProcessor.Initialese&xyz.swapee.wc.IExchangeStatusHintComputer.Initialese} xyz.swapee.wc.IExchangeStatusHintHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintHtmlComponent)} xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent} xyz.swapee.wc.ExchangeStatusHintHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent
 */
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.constructor&xyz.swapee.wc.ExchangeStatusHintHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintHtmlComponent|typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent)|(!xyz.swapee.wc.back.IExchangeStatusHintController|typeof xyz.swapee.wc.back.ExchangeStatusHintController)|(!xyz.swapee.wc.back.IExchangeStatusHintScreen|typeof xyz.swapee.wc.back.ExchangeStatusHintScreen)|(!xyz.swapee.wc.IExchangeStatusHint|typeof xyz.swapee.wc.ExchangeStatusHint)|(!xyz.swapee.wc.IExchangeStatusHintGPU|typeof xyz.swapee.wc.ExchangeStatusHintGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintHtmlComponent|typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent)|(!xyz.swapee.wc.back.IExchangeStatusHintController|typeof xyz.swapee.wc.back.ExchangeStatusHintController)|(!xyz.swapee.wc.back.IExchangeStatusHintScreen|typeof xyz.swapee.wc.back.ExchangeStatusHintScreen)|(!xyz.swapee.wc.IExchangeStatusHint|typeof xyz.swapee.wc.ExchangeStatusHint)|(!xyz.swapee.wc.IExchangeStatusHintGPU|typeof xyz.swapee.wc.ExchangeStatusHintGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintHtmlComponent|typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent)|(!xyz.swapee.wc.back.IExchangeStatusHintController|typeof xyz.swapee.wc.back.ExchangeStatusHintController)|(!xyz.swapee.wc.back.IExchangeStatusHintScreen|typeof xyz.swapee.wc.back.ExchangeStatusHintScreen)|(!xyz.swapee.wc.IExchangeStatusHint|typeof xyz.swapee.wc.ExchangeStatusHint)|(!xyz.swapee.wc.IExchangeStatusHintGPU|typeof xyz.swapee.wc.ExchangeStatusHintGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeStatusHintProcessor|typeof xyz.swapee.wc.ExchangeStatusHintProcessor)|(!xyz.swapee.wc.IExchangeStatusHintComputer|typeof xyz.swapee.wc.ExchangeStatusHintComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusHintHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintHtmlComponent.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintHtmlComponent} xyz.swapee.wc.ExchangeStatusHintHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintHtmlComponentCaster&xyz.swapee.wc.back.IExchangeStatusHintController&xyz.swapee.wc.back.IExchangeStatusHintScreen&xyz.swapee.wc.IExchangeStatusHint&xyz.swapee.wc.IExchangeStatusHintGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.IExchangeStatusHintController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IExchangeStatusHintProcessor&xyz.swapee.wc.IExchangeStatusHintComputer)} xyz.swapee.wc.IExchangeStatusHintHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusHintController} xyz.swapee.wc.back.IExchangeStatusHintController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusHintScreen} xyz.swapee.wc.back.IExchangeStatusHintScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintGPU} xyz.swapee.wc.IExchangeStatusHintGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IExchangeStatusHint_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IExchangeStatusHintHtmlComponent
 */
xyz.swapee.wc.IExchangeStatusHintHtmlComponent = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeStatusHintController.typeof&xyz.swapee.wc.back.IExchangeStatusHintScreen.typeof&xyz.swapee.wc.IExchangeStatusHint.typeof&xyz.swapee.wc.IExchangeStatusHintGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IExchangeStatusHintProcessor.typeof&xyz.swapee.wc.IExchangeStatusHintComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintHtmlComponent.Initialese>)} xyz.swapee.wc.ExchangeStatusHintHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintHtmlComponent} xyz.swapee.wc.IExchangeStatusHintHtmlComponent.typeof */
/**
 * A concrete class of _IExchangeStatusHintHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintHtmlComponent
 * @implements {xyz.swapee.wc.IExchangeStatusHintHtmlComponent} The _IExchangeStatusHint_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintHtmlComponent = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintHtmlComponent.constructor&xyz.swapee.wc.IExchangeStatusHintHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintHtmlComponent}
 */
xyz.swapee.wc.ExchangeStatusHintHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusHintHtmlComponent} */
xyz.swapee.wc.RecordIExchangeStatusHintHtmlComponent

/** @typedef {xyz.swapee.wc.IExchangeStatusHintHtmlComponent} xyz.swapee.wc.BoundIExchangeStatusHintHtmlComponent */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintHtmlComponent} xyz.swapee.wc.BoundExchangeStatusHintHtmlComponent */

/**
 * Contains getters to cast the _IExchangeStatusHintHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintHtmlComponentCaster
 */
xyz.swapee.wc.IExchangeStatusHintHtmlComponentCaster = class { }
/**
 * Cast the _IExchangeStatusHintHtmlComponent_ instance into the _BoundIExchangeStatusHintHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintHtmlComponent}
 */
xyz.swapee.wc.IExchangeStatusHintHtmlComponentCaster.prototype.asIExchangeStatusHintHtmlComponent
/**
 * Access the _ExchangeStatusHintHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintHtmlComponent}
 */
xyz.swapee.wc.IExchangeStatusHintHtmlComponentCaster.prototype.superExchangeStatusHintHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/130-IExchangeStatusHintElement.xml}  036015bd2d5c915170878f56ede9400b */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.IExchangeStatusHintElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IExchangeStatusHintElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintElement)} xyz.swapee.wc.AbstractExchangeStatusHintElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintElement} xyz.swapee.wc.ExchangeStatusHintElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintElement` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintElement
 */
xyz.swapee.wc.AbstractExchangeStatusHintElement = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintElement.constructor&xyz.swapee.wc.ExchangeStatusHintElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintElement.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintElement.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintElement|typeof xyz.swapee.wc.ExchangeStatusHintElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintElement}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElement}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintElement|typeof xyz.swapee.wc.ExchangeStatusHintElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElement}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintElement|typeof xyz.swapee.wc.ExchangeStatusHintElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElement}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintElement.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintElement} xyz.swapee.wc.ExchangeStatusHintElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintElementFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.IExchangeStatusHintElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.IExchangeStatusHintElement.Inputs, null>)} xyz.swapee.wc.IExchangeStatusHintElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IExchangeStatusHint_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IExchangeStatusHintElement
 */
xyz.swapee.wc.IExchangeStatusHintElement = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusHintElement.solder} */
xyz.swapee.wc.IExchangeStatusHintElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusHintElement.render} */
xyz.swapee.wc.IExchangeStatusHintElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusHintElement.server} */
xyz.swapee.wc.IExchangeStatusHintElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusHintElement.inducer} */
xyz.swapee.wc.IExchangeStatusHintElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintElement&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintElement.Initialese>)} xyz.swapee.wc.ExchangeStatusHintElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElement} xyz.swapee.wc.IExchangeStatusHintElement.typeof */
/**
 * A concrete class of _IExchangeStatusHintElement_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintElement
 * @implements {xyz.swapee.wc.IExchangeStatusHintElement} A component description.
 *
 * The _IExchangeStatusHint_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintElement.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintElement = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintElement.constructor&xyz.swapee.wc.IExchangeStatusHintElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElement}
 */
xyz.swapee.wc.ExchangeStatusHintElement.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintElement.
 * @interface xyz.swapee.wc.IExchangeStatusHintElementFields
 */
xyz.swapee.wc.IExchangeStatusHintElementFields = class { }
/**
 * The element-specific inputs to the _IExchangeStatusHint_ component.
 */
xyz.swapee.wc.IExchangeStatusHintElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusHintElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintElement} */
xyz.swapee.wc.RecordIExchangeStatusHintElement

/** @typedef {xyz.swapee.wc.IExchangeStatusHintElement} xyz.swapee.wc.BoundIExchangeStatusHintElement */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintElement} xyz.swapee.wc.BoundExchangeStatusHintElement */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintPort.Inputs&xyz.swapee.wc.IExchangeStatusHintDisplay.Queries&xyz.swapee.wc.IExchangeStatusHintController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs} xyz.swapee.wc.IExchangeStatusHintElement.Inputs The element-specific inputs to the _IExchangeStatusHint_ component. */

/**
 * Contains getters to cast the _IExchangeStatusHintElement_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintElementCaster
 */
xyz.swapee.wc.IExchangeStatusHintElementCaster = class { }
/**
 * Cast the _IExchangeStatusHintElement_ instance into the _BoundIExchangeStatusHintElement_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintElement}
 */
xyz.swapee.wc.IExchangeStatusHintElementCaster.prototype.asIExchangeStatusHintElement
/**
 * Access the _ExchangeStatusHintElement_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintElement}
 */
xyz.swapee.wc.IExchangeStatusHintElementCaster.prototype.superExchangeStatusHintElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ExchangeStatusHintMemory, props: !xyz.swapee.wc.IExchangeStatusHintElement.Inputs) => Object<string, *>} xyz.swapee.wc.IExchangeStatusHintElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintElement.__solder<!xyz.swapee.wc.IExchangeStatusHintElement>} xyz.swapee.wc.IExchangeStatusHintElement._solder */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.ExchangeStatusHintMemory} model The model.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusHintElement.Inputs} props The element props.
 * - `[status=null]` _&#42;?_ The status. ⤴ *IExchangeStatusHintOuterCore.WeakModel.Status* ⤴ *IExchangeStatusHintOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeStatusHintElementPort.Inputs.NoSolder* Default `false`.
 * - `[awaitingStatusLaOpts]` _!Object?_ The options to pass to the _AwaitingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts* Default `{}`.
 * - `[confirmingStatusLaOpts]` _!Object?_ The options to pass to the _ConfirmingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts* Default `{}`.
 * - `[exchangingStatusLaOpts]` _!Object?_ The options to pass to the _ExchangingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts* Default `{}`.
 * - `[sendingStatusLaOpts]` _!Object?_ The options to pass to the _SendingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts* Default `{}`.
 * - `[finishedStatusLaOpts]` _!Object?_ The options to pass to the _FinishedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts* Default `{}`.
 * - `[failedStatusLaOpts]` _!Object?_ The options to pass to the _FailedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts* Default `{}`.
 * - `[refundedStatusLaOpts]` _!Object?_ The options to pass to the _RefundedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts* Default `{}`.
 * - `[overdueStatusLaOpts]` _!Object?_ The options to pass to the _OverdueStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts* Default `{}`.
 * - `[holdStatusLaOpts]` _!Object?_ The options to pass to the _HoldStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts* Default `{}`.
 * - `[expiredStatusLaOpts]` _!Object?_ The options to pass to the _ExpiredStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IExchangeStatusHintElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeStatusHintMemory, instance?: !xyz.swapee.wc.IExchangeStatusHintScreen&xyz.swapee.wc.IExchangeStatusHintController) => !engineering.type.VNode} xyz.swapee.wc.IExchangeStatusHintElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintElement.__render<!xyz.swapee.wc.IExchangeStatusHintElement>} xyz.swapee.wc.IExchangeStatusHintElement._render */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.ExchangeStatusHintMemory} [model] The model for the view.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusHintScreen&xyz.swapee.wc.IExchangeStatusHintController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeStatusHintElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeStatusHintMemory, inputs: !xyz.swapee.wc.IExchangeStatusHintElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IExchangeStatusHintElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintElement.__server<!xyz.swapee.wc.IExchangeStatusHintElement>} xyz.swapee.wc.IExchangeStatusHintElement._server */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.ExchangeStatusHintMemory} memory The memory registers.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusHintElement.Inputs} inputs The inputs to the port.
 * - `[status=null]` _&#42;?_ The status. ⤴ *IExchangeStatusHintOuterCore.WeakModel.Status* ⤴ *IExchangeStatusHintOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeStatusHintElementPort.Inputs.NoSolder* Default `false`.
 * - `[awaitingStatusLaOpts]` _!Object?_ The options to pass to the _AwaitingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts* Default `{}`.
 * - `[confirmingStatusLaOpts]` _!Object?_ The options to pass to the _ConfirmingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts* Default `{}`.
 * - `[exchangingStatusLaOpts]` _!Object?_ The options to pass to the _ExchangingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts* Default `{}`.
 * - `[sendingStatusLaOpts]` _!Object?_ The options to pass to the _SendingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts* Default `{}`.
 * - `[finishedStatusLaOpts]` _!Object?_ The options to pass to the _FinishedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts* Default `{}`.
 * - `[failedStatusLaOpts]` _!Object?_ The options to pass to the _FailedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts* Default `{}`.
 * - `[refundedStatusLaOpts]` _!Object?_ The options to pass to the _RefundedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts* Default `{}`.
 * - `[overdueStatusLaOpts]` _!Object?_ The options to pass to the _OverdueStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts* Default `{}`.
 * - `[holdStatusLaOpts]` _!Object?_ The options to pass to the _HoldStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts* Default `{}`.
 * - `[expiredStatusLaOpts]` _!Object?_ The options to pass to the _ExpiredStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeStatusHintElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeStatusHintMemory, port?: !xyz.swapee.wc.IExchangeStatusHintElement.Inputs) => ?} xyz.swapee.wc.IExchangeStatusHintElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintElement.__inducer<!xyz.swapee.wc.IExchangeStatusHintElement>} xyz.swapee.wc.IExchangeStatusHintElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.ExchangeStatusHintMemory} [model] The model of the component into which to induce the state.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusHintElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[status=null]` _&#42;?_ The status. ⤴ *IExchangeStatusHintOuterCore.WeakModel.Status* ⤴ *IExchangeStatusHintOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeStatusHintElementPort.Inputs.NoSolder* Default `false`.
 * - `[awaitingStatusLaOpts]` _!Object?_ The options to pass to the _AwaitingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts* Default `{}`.
 * - `[confirmingStatusLaOpts]` _!Object?_ The options to pass to the _ConfirmingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts* Default `{}`.
 * - `[exchangingStatusLaOpts]` _!Object?_ The options to pass to the _ExchangingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts* Default `{}`.
 * - `[sendingStatusLaOpts]` _!Object?_ The options to pass to the _SendingStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts* Default `{}`.
 * - `[finishedStatusLaOpts]` _!Object?_ The options to pass to the _FinishedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts* Default `{}`.
 * - `[failedStatusLaOpts]` _!Object?_ The options to pass to the _FailedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts* Default `{}`.
 * - `[refundedStatusLaOpts]` _!Object?_ The options to pass to the _RefundedStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts* Default `{}`.
 * - `[overdueStatusLaOpts]` _!Object?_ The options to pass to the _OverdueStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts* Default `{}`.
 * - `[holdStatusLaOpts]` _!Object?_ The options to pass to the _HoldStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts* Default `{}`.
 * - `[expiredStatusLaOpts]` _!Object?_ The options to pass to the _ExpiredStatusLa_ vdu. ⤴ *IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IExchangeStatusHintElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusHintElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/140-IExchangeStatusHintElementPort.xml}  3ef47f5be6c7a0e8238c0607b75f165b */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeStatusHintElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintElementPort)} xyz.swapee.wc.AbstractExchangeStatusHintElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintElementPort} xyz.swapee.wc.ExchangeStatusHintElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintElementPort
 */
xyz.swapee.wc.AbstractExchangeStatusHintElementPort = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintElementPort.constructor&xyz.swapee.wc.ExchangeStatusHintElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintElementPort.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintElementPort|typeof xyz.swapee.wc.ExchangeStatusHintElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintElementPort|typeof xyz.swapee.wc.ExchangeStatusHintElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintElementPort|typeof xyz.swapee.wc.ExchangeStatusHintElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusHintElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintElementPort.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintElementPort} xyz.swapee.wc.ExchangeStatusHintElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs>)} xyz.swapee.wc.IExchangeStatusHintElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IExchangeStatusHintElementPort
 */
xyz.swapee.wc.IExchangeStatusHintElementPort = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintElementPort.Initialese>)} xyz.swapee.wc.ExchangeStatusHintElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort} xyz.swapee.wc.IExchangeStatusHintElementPort.typeof */
/**
 * A concrete class of _IExchangeStatusHintElementPort_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintElementPort
 * @implements {xyz.swapee.wc.IExchangeStatusHintElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintElementPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintElementPort = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintElementPort.constructor&xyz.swapee.wc.IExchangeStatusHintElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintElementPort}
 */
xyz.swapee.wc.ExchangeStatusHintElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintElementPort.
 * @interface xyz.swapee.wc.IExchangeStatusHintElementPortFields
 */
xyz.swapee.wc.IExchangeStatusHintElementPortFields = class { }
/**
 * The inputs to the _IExchangeStatusHintElement_'s controller via its element port.
 */
xyz.swapee.wc.IExchangeStatusHintElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeStatusHintElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintElementPort} */
xyz.swapee.wc.RecordIExchangeStatusHintElementPort

/** @typedef {xyz.swapee.wc.IExchangeStatusHintElementPort} xyz.swapee.wc.BoundIExchangeStatusHintElementPort */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintElementPort} xyz.swapee.wc.BoundExchangeStatusHintElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _AwaitingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts.awaitingStatusLaOpts

/**
 * The options to pass to the _ConfirmingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts.confirmingStatusLaOpts

/**
 * The options to pass to the _ExchangingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts.exchangingStatusLaOpts

/**
 * The options to pass to the _SendingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts.sendingStatusLaOpts

/**
 * The options to pass to the _FinishedStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts.finishedStatusLaOpts

/**
 * The options to pass to the _FailedStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts.failedStatusLaOpts

/**
 * The options to pass to the _RefundedStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts.refundedStatusLaOpts

/**
 * The options to pass to the _OverdueStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts.overdueStatusLaOpts

/**
 * The options to pass to the _HoldStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts.holdStatusLaOpts

/**
 * The options to pass to the _ExpiredStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts.expiredStatusLaOpts

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts)} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts.typeof */
/**
 * The inputs to the _IExchangeStatusHintElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.constructor&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.AwaitingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ConfirmingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExchangingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.SendingStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FinishedStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FailedStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.RefundedStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.OverdueStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.HoldStatusLaOpts&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExpiredStatusLaOpts)} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.AwaitingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.AwaitingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ConfirmingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ConfirmingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExchangingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExchangingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.SendingStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.SendingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FinishedStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FinishedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FailedStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FailedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.RefundedStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.RefundedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.OverdueStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.OverdueStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.HoldStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.HoldStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExpiredStatusLaOpts} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExpiredStatusLaOpts.typeof */
/**
 * The inputs to the _IExchangeStatusHintElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs
 */
xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.constructor&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.AwaitingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ConfirmingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExchangingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.SendingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FinishedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FailedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.RefundedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.OverdueStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.HoldStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExpiredStatusLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs

/**
 * Contains getters to cast the _IExchangeStatusHintElementPort_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintElementPortCaster
 */
xyz.swapee.wc.IExchangeStatusHintElementPortCaster = class { }
/**
 * Cast the _IExchangeStatusHintElementPort_ instance into the _BoundIExchangeStatusHintElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintElementPort}
 */
xyz.swapee.wc.IExchangeStatusHintElementPortCaster.prototype.asIExchangeStatusHintElementPort
/**
 * Access the _ExchangeStatusHintElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintElementPort}
 */
xyz.swapee.wc.IExchangeStatusHintElementPortCaster.prototype.superExchangeStatusHintElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [awaitingStatusLaOpts] The options to pass to the _AwaitingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.AwaitingStatusLaOpts_Safe The options to pass to the _AwaitingStatusLa_ vdu (required overlay).
 * @prop {!Object} awaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [confirmingStatusLaOpts] The options to pass to the _ConfirmingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ConfirmingStatusLaOpts_Safe The options to pass to the _ConfirmingStatusLa_ vdu (required overlay).
 * @prop {!Object} confirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [exchangingStatusLaOpts] The options to pass to the _ExchangingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExchangingStatusLaOpts_Safe The options to pass to the _ExchangingStatusLa_ vdu (required overlay).
 * @prop {!Object} exchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [sendingStatusLaOpts] The options to pass to the _SendingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.SendingStatusLaOpts_Safe The options to pass to the _SendingStatusLa_ vdu (required overlay).
 * @prop {!Object} sendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu (optional overlay).
 * @prop {!Object} [finishedStatusLaOpts] The options to pass to the _FinishedStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FinishedStatusLaOpts_Safe The options to pass to the _FinishedStatusLa_ vdu (required overlay).
 * @prop {!Object} finishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu (optional overlay).
 * @prop {!Object} [failedStatusLaOpts] The options to pass to the _FailedStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.FailedStatusLaOpts_Safe The options to pass to the _FailedStatusLa_ vdu (required overlay).
 * @prop {!Object} failedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu (optional overlay).
 * @prop {!Object} [refundedStatusLaOpts] The options to pass to the _RefundedStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.RefundedStatusLaOpts_Safe The options to pass to the _RefundedStatusLa_ vdu (required overlay).
 * @prop {!Object} refundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu (optional overlay).
 * @prop {!Object} [overdueStatusLaOpts] The options to pass to the _OverdueStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.OverdueStatusLaOpts_Safe The options to pass to the _OverdueStatusLa_ vdu (required overlay).
 * @prop {!Object} overdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts The options to pass to the _HoldStatusLa_ vdu (optional overlay).
 * @prop {!Object} [holdStatusLaOpts] The options to pass to the _HoldStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.HoldStatusLaOpts_Safe The options to pass to the _HoldStatusLa_ vdu (required overlay).
 * @prop {!Object} holdStatusLaOpts The options to pass to the _HoldStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu (optional overlay).
 * @prop {!Object} [expiredStatusLaOpts] The options to pass to the _ExpiredStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.Inputs.ExpiredStatusLaOpts_Safe The options to pass to the _ExpiredStatusLa_ vdu (required overlay).
 * @prop {!Object} expiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.AwaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu (optional overlay).
 * @prop {*} [awaitingStatusLaOpts=null] The options to pass to the _AwaitingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.AwaitingStatusLaOpts_Safe The options to pass to the _AwaitingStatusLa_ vdu (required overlay).
 * @prop {*} awaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ConfirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu (optional overlay).
 * @prop {*} [confirmingStatusLaOpts=null] The options to pass to the _ConfirmingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ConfirmingStatusLaOpts_Safe The options to pass to the _ConfirmingStatusLa_ vdu (required overlay).
 * @prop {*} confirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu (optional overlay).
 * @prop {*} [exchangingStatusLaOpts=null] The options to pass to the _ExchangingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExchangingStatusLaOpts_Safe The options to pass to the _ExchangingStatusLa_ vdu (required overlay).
 * @prop {*} exchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.SendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu (optional overlay).
 * @prop {*} [sendingStatusLaOpts=null] The options to pass to the _SendingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.SendingStatusLaOpts_Safe The options to pass to the _SendingStatusLa_ vdu (required overlay).
 * @prop {*} sendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FinishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu (optional overlay).
 * @prop {*} [finishedStatusLaOpts=null] The options to pass to the _FinishedStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FinishedStatusLaOpts_Safe The options to pass to the _FinishedStatusLa_ vdu (required overlay).
 * @prop {*} finishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FailedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu (optional overlay).
 * @prop {*} [failedStatusLaOpts=null] The options to pass to the _FailedStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.FailedStatusLaOpts_Safe The options to pass to the _FailedStatusLa_ vdu (required overlay).
 * @prop {*} failedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.RefundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu (optional overlay).
 * @prop {*} [refundedStatusLaOpts=null] The options to pass to the _RefundedStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.RefundedStatusLaOpts_Safe The options to pass to the _RefundedStatusLa_ vdu (required overlay).
 * @prop {*} refundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.OverdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu (optional overlay).
 * @prop {*} [overdueStatusLaOpts=null] The options to pass to the _OverdueStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.OverdueStatusLaOpts_Safe The options to pass to the _OverdueStatusLa_ vdu (required overlay).
 * @prop {*} overdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.HoldStatusLaOpts The options to pass to the _HoldStatusLa_ vdu (optional overlay).
 * @prop {*} [holdStatusLaOpts=null] The options to pass to the _HoldStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.HoldStatusLaOpts_Safe The options to pass to the _HoldStatusLa_ vdu (required overlay).
 * @prop {*} holdStatusLaOpts The options to pass to the _HoldStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExpiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu (optional overlay).
 * @prop {*} [expiredStatusLaOpts=null] The options to pass to the _ExpiredStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintElementPort.WeakInputs.ExpiredStatusLaOpts_Safe The options to pass to the _ExpiredStatusLa_ vdu (required overlay).
 * @prop {*} expiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/170-IExchangeStatusHintDesigner.xml}  75efae3a93df0576a458b635f8522bd5 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IExchangeStatusHintDesigner
 */
xyz.swapee.wc.IExchangeStatusHintDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.ExchangeStatusHintClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeStatusHint />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeStatusHintClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeStatusHint />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeStatusHintClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeStatusHintDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeStatusHint` _typeof IExchangeStatusHintController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeStatusHintDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeStatusHint` _typeof IExchangeStatusHintController_
   * - `This` _typeof IExchangeStatusHintController_
   * @param {!xyz.swapee.wc.IExchangeStatusHintDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ExchangeStatusHint` _!ExchangeStatusHintMemory_
   * - `This` _!ExchangeStatusHintMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeStatusHintClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeStatusHintClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IExchangeStatusHintDesigner.prototype.constructor = xyz.swapee.wc.IExchangeStatusHintDesigner

/**
 * A concrete class of _IExchangeStatusHintDesigner_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintDesigner
 * @implements {xyz.swapee.wc.IExchangeStatusHintDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.ExchangeStatusHintDesigner = class extends xyz.swapee.wc.IExchangeStatusHintDesigner { }
xyz.swapee.wc.ExchangeStatusHintDesigner.prototype.constructor = xyz.swapee.wc.ExchangeStatusHintDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeStatusHintController} ExchangeStatusHint
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeStatusHintController} ExchangeStatusHint
 * @prop {typeof xyz.swapee.wc.IExchangeStatusHintController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusHintDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.ExchangeStatusHintMemory} ExchangeStatusHint
 * @prop {!xyz.swapee.wc.ExchangeStatusHintMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/40-IExchangeStatusHintDisplay.xml}  9d8808c767fc05eb121c8b2282657eca */
/**
 * @typedef {Object} $xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese
 * @prop {HTMLSpanElement} [AwaitingStatusLa]
 * @prop {HTMLSpanElement} [ConfirmingStatusLa]
 * @prop {HTMLSpanElement} [ExchangingStatusLa]
 * @prop {HTMLSpanElement} [SendingStatusLa]
 * @prop {HTMLSpanElement} [FinishedStatusLa]
 * @prop {HTMLSpanElement} [FailedStatusLa]
 * @prop {HTMLSpanElement} [RefundedStatusLa]
 * @prop {HTMLSpanElement} [OverdueStatusLa]
 * @prop {HTMLSpanElement} [HoldStatusLa]
 * @prop {HTMLSpanElement} [ExpiredStatusLa]
 */
/** @typedef {$xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IExchangeStatusHintDisplay.Settings>} xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintDisplay)} xyz.swapee.wc.AbstractExchangeStatusHintDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintDisplay} xyz.swapee.wc.ExchangeStatusHintDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintDisplay
 */
xyz.swapee.wc.AbstractExchangeStatusHintDisplay = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintDisplay.constructor&xyz.swapee.wc.ExchangeStatusHintDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintDisplay.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.ExchangeStatusHintDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusHintDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusHintDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.ExchangeStatusHintDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusHintDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.ExchangeStatusHintDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusHintDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintDisplay} xyz.swapee.wc.ExchangeStatusHintDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.ExchangeStatusHintMemory, !HTMLDivElement, !xyz.swapee.wc.IExchangeStatusHintDisplay.Settings, xyz.swapee.wc.IExchangeStatusHintDisplay.Queries, null>)} xyz.swapee.wc.IExchangeStatusHintDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IExchangeStatusHint_.
 * @interface xyz.swapee.wc.IExchangeStatusHintDisplay
 */
xyz.swapee.wc.IExchangeStatusHintDisplay = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusHintDisplay.paint} */
xyz.swapee.wc.IExchangeStatusHintDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese>)} xyz.swapee.wc.ExchangeStatusHintDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintDisplay} xyz.swapee.wc.IExchangeStatusHintDisplay.typeof */
/**
 * A concrete class of _IExchangeStatusHintDisplay_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintDisplay
 * @implements {xyz.swapee.wc.IExchangeStatusHintDisplay} Display for presenting information from the _IExchangeStatusHint_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintDisplay = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintDisplay.constructor&xyz.swapee.wc.IExchangeStatusHintDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.ExchangeStatusHintDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintDisplay.
 * @interface xyz.swapee.wc.IExchangeStatusHintDisplayFields
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IExchangeStatusHintDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IExchangeStatusHintDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.AwaitingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.ConfirmingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.ExchangingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.SendingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.FinishedStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.FailedStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.RefundedStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.OverdueStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.HoldStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusHintDisplayFields.prototype.ExpiredStatusLa = /** @type {HTMLSpanElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintDisplay} */
xyz.swapee.wc.RecordIExchangeStatusHintDisplay

/** @typedef {xyz.swapee.wc.IExchangeStatusHintDisplay} xyz.swapee.wc.BoundIExchangeStatusHintDisplay */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintDisplay} xyz.swapee.wc.BoundExchangeStatusHintDisplay */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintDisplay.Queries} xyz.swapee.wc.IExchangeStatusHintDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeStatusHintDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IExchangeStatusHintDisplay_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintDisplayCaster
 */
xyz.swapee.wc.IExchangeStatusHintDisplayCaster = class { }
/**
 * Cast the _IExchangeStatusHintDisplay_ instance into the _BoundIExchangeStatusHintDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintDisplay}
 */
xyz.swapee.wc.IExchangeStatusHintDisplayCaster.prototype.asIExchangeStatusHintDisplay
/**
 * Cast the _IExchangeStatusHintDisplay_ instance into the _BoundIExchangeStatusHintScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintScreen}
 */
xyz.swapee.wc.IExchangeStatusHintDisplayCaster.prototype.asIExchangeStatusHintScreen
/**
 * Access the _ExchangeStatusHintDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintDisplay}
 */
xyz.swapee.wc.IExchangeStatusHintDisplayCaster.prototype.superExchangeStatusHintDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeStatusHintMemory, land: null) => void} xyz.swapee.wc.IExchangeStatusHintDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintDisplay.__paint<!xyz.swapee.wc.IExchangeStatusHintDisplay>} xyz.swapee.wc.IExchangeStatusHintDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeStatusHintMemory} memory The display data.
 * - `status` _string_ The status. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusHintDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusHintDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/40-IExchangeStatusHintDisplayBack.xml}  e9fb557ef5249c54485ea16bb54e4d84 */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IExchangeStatusHintDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [AwaitingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ConfirmingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [SendingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [FinishedStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [FailedStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [RefundedStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [OverdueStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [HoldStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ExpiredStatusLa]
 */
/** @typedef {$xyz.swapee.wc.back.IExchangeStatusHintDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ExchangeStatusHintClasses>} xyz.swapee.wc.back.IExchangeStatusHintDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusHintDisplay)} xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay} xyz.swapee.wc.back.ExchangeStatusHintDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusHintDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.constructor&xyz.swapee.wc.back.ExchangeStatusHintDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusHintDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusHintDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.ExchangeStatusHintClasses, null>)} xyz.swapee.wc.back.IExchangeStatusHintDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintDisplay
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplay = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusHintDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IExchangeStatusHintDisplay.paint} */
xyz.swapee.wc.back.IExchangeStatusHintDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusHintDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintDisplay.Initialese>)} xyz.swapee.wc.back.ExchangeStatusHintDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusHintDisplay} xyz.swapee.wc.back.IExchangeStatusHintDisplay.typeof */
/**
 * A concrete class of _IExchangeStatusHintDisplay_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusHintDisplay
 * @implements {xyz.swapee.wc.back.IExchangeStatusHintDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusHintDisplay = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusHintDisplay.constructor&xyz.swapee.wc.back.IExchangeStatusHintDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.ExchangeStatusHintDisplay.prototype.constructor = xyz.swapee.wc.back.ExchangeStatusHintDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay}
 */
xyz.swapee.wc.back.ExchangeStatusHintDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintDisplay.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintDisplayFields
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.AwaitingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.ConfirmingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.ExchangingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.SendingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.FinishedStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.FailedStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.RefundedStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.OverdueStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.HoldStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayFields.prototype.ExpiredStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintDisplay} */
xyz.swapee.wc.back.RecordIExchangeStatusHintDisplay

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintDisplay} xyz.swapee.wc.back.BoundIExchangeStatusHintDisplay */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusHintDisplay} xyz.swapee.wc.back.BoundExchangeStatusHintDisplay */

/**
 * Contains getters to cast the _IExchangeStatusHintDisplay_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintDisplayCaster
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayCaster = class { }
/**
 * Cast the _IExchangeStatusHintDisplay_ instance into the _BoundIExchangeStatusHintDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusHintDisplay}
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayCaster.prototype.asIExchangeStatusHintDisplay
/**
 * Access the _ExchangeStatusHintDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusHintDisplay}
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplayCaster.prototype.superExchangeStatusHintDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.ExchangeStatusHintMemory, land?: null) => void} xyz.swapee.wc.back.IExchangeStatusHintDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintDisplay.__paint<!xyz.swapee.wc.back.IExchangeStatusHintDisplay>} xyz.swapee.wc.back.IExchangeStatusHintDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusHintDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeStatusHintMemory} [memory] The display data.
 * - `status` _string_ The status. Default empty string.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IExchangeStatusHintDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IExchangeStatusHintDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/41-ExchangeStatusHintClasses.xml}  acfdf562d4fc78b6ab49e332c449bbec */
/**
 * The classes of the _IExchangeStatusHintDisplay_.
 * @record xyz.swapee.wc.ExchangeStatusHintClasses
 */
xyz.swapee.wc.ExchangeStatusHintClasses = class { }
/**
 *
 */
xyz.swapee.wc.ExchangeStatusHintClasses.prototype.StatusWarn = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ExchangeStatusHintClasses.prototype.props = /** @type {xyz.swapee.wc.ExchangeStatusHintClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/50-IExchangeStatusHintController.xml}  9ac8a9e84e3466855e072c24a8f63bb8 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IExchangeStatusHintController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IExchangeStatusHintController.Inputs, !xyz.swapee.wc.IExchangeStatusHintOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel>} xyz.swapee.wc.IExchangeStatusHintController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintController)} xyz.swapee.wc.AbstractExchangeStatusHintController.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintController} xyz.swapee.wc.ExchangeStatusHintController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintController` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintController
 */
xyz.swapee.wc.AbstractExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintController.constructor&xyz.swapee.wc.ExchangeStatusHintController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintController.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintController.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintController}
 */
xyz.swapee.wc.AbstractExchangeStatusHintController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintController}
 */
xyz.swapee.wc.AbstractExchangeStatusHintController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintController}
 */
xyz.swapee.wc.AbstractExchangeStatusHintController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintController}
 */
xyz.swapee.wc.AbstractExchangeStatusHintController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintController.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintController} xyz.swapee.wc.ExchangeStatusHintControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IExchangeStatusHintController.Inputs, !xyz.swapee.wc.IExchangeStatusHintOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IExchangeStatusHintOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IExchangeStatusHintController.Inputs, !xyz.swapee.wc.IExchangeStatusHintController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IExchangeStatusHintController.Inputs, !xyz.swapee.wc.ExchangeStatusHintMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeStatusHintController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IExchangeStatusHintController.Inputs>)} xyz.swapee.wc.IExchangeStatusHintController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IExchangeStatusHintController
 */
xyz.swapee.wc.IExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusHintController.resetPort} */
xyz.swapee.wc.IExchangeStatusHintController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintController&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintController.Initialese>)} xyz.swapee.wc.ExchangeStatusHintController.constructor */
/**
 * A concrete class of _IExchangeStatusHintController_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintController
 * @implements {xyz.swapee.wc.IExchangeStatusHintController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintController.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintController.constructor&xyz.swapee.wc.IExchangeStatusHintController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintController}
 */
xyz.swapee.wc.ExchangeStatusHintController.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintController.
 * @interface xyz.swapee.wc.IExchangeStatusHintControllerFields
 */
xyz.swapee.wc.IExchangeStatusHintControllerFields = class { }
/**
 * The inputs to the _IExchangeStatusHint_'s controller.
 */
xyz.swapee.wc.IExchangeStatusHintControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusHintController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IExchangeStatusHintControllerFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeStatusHintController} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintController} */
xyz.swapee.wc.RecordIExchangeStatusHintController

/** @typedef {xyz.swapee.wc.IExchangeStatusHintController} xyz.swapee.wc.BoundIExchangeStatusHintController */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintController} xyz.swapee.wc.BoundExchangeStatusHintController */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintPort.Inputs} xyz.swapee.wc.IExchangeStatusHintController.Inputs The inputs to the _IExchangeStatusHint_'s controller. */

/** @typedef {xyz.swapee.wc.IExchangeStatusHintPort.WeakInputs} xyz.swapee.wc.IExchangeStatusHintController.WeakInputs The inputs to the _IExchangeStatusHint_'s controller. */

/**
 * Contains getters to cast the _IExchangeStatusHintController_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintControllerCaster
 */
xyz.swapee.wc.IExchangeStatusHintControllerCaster = class { }
/**
 * Cast the _IExchangeStatusHintController_ instance into the _BoundIExchangeStatusHintController_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintController}
 */
xyz.swapee.wc.IExchangeStatusHintControllerCaster.prototype.asIExchangeStatusHintController
/**
 * Cast the _IExchangeStatusHintController_ instance into the _BoundIExchangeStatusHintProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintProcessor}
 */
xyz.swapee.wc.IExchangeStatusHintControllerCaster.prototype.asIExchangeStatusHintProcessor
/**
 * Access the _ExchangeStatusHintController_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintController}
 */
xyz.swapee.wc.IExchangeStatusHintControllerCaster.prototype.superExchangeStatusHintController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusHintController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusHintController.__resetPort<!xyz.swapee.wc.IExchangeStatusHintController>} xyz.swapee.wc.IExchangeStatusHintController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusHintController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusHintController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/51-IExchangeStatusHintControllerFront.xml}  97c8c36ea448bf66c75f918c777953f5 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IExchangeStatusHintController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeStatusHintController)} xyz.swapee.wc.front.AbstractExchangeStatusHintController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeStatusHintController} xyz.swapee.wc.front.ExchangeStatusHintController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeStatusHintController` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeStatusHintController
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeStatusHintController.constructor&xyz.swapee.wc.front.ExchangeStatusHintController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeStatusHintController.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeStatusHintController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintController.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintController|typeof xyz.swapee.wc.front.ExchangeStatusHintController)|(!xyz.swapee.wc.front.IExchangeStatusHintControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintController|typeof xyz.swapee.wc.front.ExchangeStatusHintController)|(!xyz.swapee.wc.front.IExchangeStatusHintControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintController|typeof xyz.swapee.wc.front.ExchangeStatusHintController)|(!xyz.swapee.wc.front.IExchangeStatusHintControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeStatusHintController.Initialese[]) => xyz.swapee.wc.front.IExchangeStatusHintController} xyz.swapee.wc.front.ExchangeStatusHintControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeStatusHintControllerCaster&xyz.swapee.wc.front.IExchangeStatusHintControllerAT)} xyz.swapee.wc.front.IExchangeStatusHintController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeStatusHintControllerAT} xyz.swapee.wc.front.IExchangeStatusHintControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IExchangeStatusHintController
 */
xyz.swapee.wc.front.IExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.front.IExchangeStatusHintController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IExchangeStatusHintControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusHintController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeStatusHintController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeStatusHintController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusHintController.Initialese>)} xyz.swapee.wc.front.ExchangeStatusHintController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeStatusHintController} xyz.swapee.wc.front.IExchangeStatusHintController.typeof */
/**
 * A concrete class of _IExchangeStatusHintController_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeStatusHintController
 * @implements {xyz.swapee.wc.front.IExchangeStatusHintController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusHintController.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.front.ExchangeStatusHintController.constructor&xyz.swapee.wc.front.IExchangeStatusHintController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeStatusHintController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusHintController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeStatusHintController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintController}
 */
xyz.swapee.wc.front.ExchangeStatusHintController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeStatusHintController} */
xyz.swapee.wc.front.RecordIExchangeStatusHintController

/** @typedef {xyz.swapee.wc.front.IExchangeStatusHintController} xyz.swapee.wc.front.BoundIExchangeStatusHintController */

/** @typedef {xyz.swapee.wc.front.ExchangeStatusHintController} xyz.swapee.wc.front.BoundExchangeStatusHintController */

/**
 * Contains getters to cast the _IExchangeStatusHintController_ interface.
 * @interface xyz.swapee.wc.front.IExchangeStatusHintControllerCaster
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerCaster = class { }
/**
 * Cast the _IExchangeStatusHintController_ instance into the _BoundIExchangeStatusHintController_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeStatusHintController}
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerCaster.prototype.asIExchangeStatusHintController
/**
 * Access the _ExchangeStatusHintController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeStatusHintController}
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerCaster.prototype.superExchangeStatusHintController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/52-IExchangeStatusHintControllerBack.xml}  72305e1452b671adcba4933ae2e72040 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IExchangeStatusHintController.Inputs>&xyz.swapee.wc.IExchangeStatusHintController.Initialese} xyz.swapee.wc.back.IExchangeStatusHintController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusHintController)} xyz.swapee.wc.back.AbstractExchangeStatusHintController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusHintController} xyz.swapee.wc.back.ExchangeStatusHintController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusHintController` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusHintController
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusHintController.constructor&xyz.swapee.wc.back.ExchangeStatusHintController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusHintController.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusHintController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintController.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintController|typeof xyz.swapee.wc.back.ExchangeStatusHintController)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintController|typeof xyz.swapee.wc.back.ExchangeStatusHintController)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintController|typeof xyz.swapee.wc.back.ExchangeStatusHintController)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusHintController.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusHintController} xyz.swapee.wc.back.ExchangeStatusHintControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusHintControllerCaster&xyz.swapee.wc.IExchangeStatusHintController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IExchangeStatusHintController.Inputs>)} xyz.swapee.wc.back.IExchangeStatusHintController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintController
 */
xyz.swapee.wc.back.IExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusHintController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusHintController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusHintController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusHintController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintController.Initialese>)} xyz.swapee.wc.back.ExchangeStatusHintController.constructor */
/**
 * A concrete class of _IExchangeStatusHintController_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusHintController
 * @implements {xyz.swapee.wc.back.IExchangeStatusHintController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintController.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusHintController = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusHintController.constructor&xyz.swapee.wc.back.IExchangeStatusHintController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusHintController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusHintController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintController}
 */
xyz.swapee.wc.back.ExchangeStatusHintController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintController} */
xyz.swapee.wc.back.RecordIExchangeStatusHintController

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintController} xyz.swapee.wc.back.BoundIExchangeStatusHintController */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusHintController} xyz.swapee.wc.back.BoundExchangeStatusHintController */

/**
 * Contains getters to cast the _IExchangeStatusHintController_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintControllerCaster
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerCaster = class { }
/**
 * Cast the _IExchangeStatusHintController_ instance into the _BoundIExchangeStatusHintController_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusHintController}
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerCaster.prototype.asIExchangeStatusHintController
/**
 * Access the _ExchangeStatusHintController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusHintController}
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerCaster.prototype.superExchangeStatusHintController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/53-IExchangeStatusHintControllerAR.xml}  360d84a4f517d0186838056dbe2ba577 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeStatusHintController.Initialese} xyz.swapee.wc.back.IExchangeStatusHintControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusHintControllerAR)} xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR} xyz.swapee.wc.back.ExchangeStatusHintControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusHintControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.constructor&xyz.swapee.wc.back.ExchangeStatusHintControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintControllerAR|typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintControllerAR|typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintControllerAR|typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusHintController|typeof xyz.swapee.wc.ExchangeStatusHintController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusHintControllerAR.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusHintControllerAR} xyz.swapee.wc.back.ExchangeStatusHintControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusHintControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeStatusHintController)} xyz.swapee.wc.back.IExchangeStatusHintControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeStatusHintControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintControllerAR
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerAR = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusHintControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeStatusHintController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusHintControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintControllerAR.Initialese>)} xyz.swapee.wc.back.ExchangeStatusHintControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusHintControllerAR} xyz.swapee.wc.back.IExchangeStatusHintControllerAR.typeof */
/**
 * A concrete class of _IExchangeStatusHintControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusHintControllerAR
 * @implements {xyz.swapee.wc.back.IExchangeStatusHintControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeStatusHintControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusHintControllerAR = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusHintControllerAR.constructor&xyz.swapee.wc.back.IExchangeStatusHintControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusHintControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusHintControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintControllerAR}
 */
xyz.swapee.wc.back.ExchangeStatusHintControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintControllerAR} */
xyz.swapee.wc.back.RecordIExchangeStatusHintControllerAR

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintControllerAR} xyz.swapee.wc.back.BoundIExchangeStatusHintControllerAR */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusHintControllerAR} xyz.swapee.wc.back.BoundExchangeStatusHintControllerAR */

/**
 * Contains getters to cast the _IExchangeStatusHintControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintControllerARCaster
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerARCaster = class { }
/**
 * Cast the _IExchangeStatusHintControllerAR_ instance into the _BoundIExchangeStatusHintControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusHintControllerAR}
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerARCaster.prototype.asIExchangeStatusHintControllerAR
/**
 * Access the _ExchangeStatusHintControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusHintControllerAR}
 */
xyz.swapee.wc.back.IExchangeStatusHintControllerARCaster.prototype.superExchangeStatusHintControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/54-IExchangeStatusHintControllerAT.xml}  6b15588bd4f08562e429b2a0263b3012 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IExchangeStatusHintControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeStatusHintControllerAT)} xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT} xyz.swapee.wc.front.ExchangeStatusHintControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeStatusHintControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.constructor&xyz.swapee.wc.front.ExchangeStatusHintControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeStatusHintControllerAT.Initialese[]) => xyz.swapee.wc.front.IExchangeStatusHintControllerAT} xyz.swapee.wc.front.ExchangeStatusHintControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeStatusHintControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IExchangeStatusHintControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeStatusHintControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IExchangeStatusHintControllerAT
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerAT = class extends /** @type {xyz.swapee.wc.front.IExchangeStatusHintControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusHintControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeStatusHintControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusHintControllerAT.Initialese>)} xyz.swapee.wc.front.ExchangeStatusHintControllerAT.constructor */
/**
 * A concrete class of _IExchangeStatusHintControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeStatusHintControllerAT
 * @implements {xyz.swapee.wc.front.IExchangeStatusHintControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeStatusHintControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusHintControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeStatusHintControllerAT = class extends /** @type {xyz.swapee.wc.front.ExchangeStatusHintControllerAT.constructor&xyz.swapee.wc.front.IExchangeStatusHintControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeStatusHintControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusHintControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeStatusHintControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintControllerAT}
 */
xyz.swapee.wc.front.ExchangeStatusHintControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeStatusHintControllerAT} */
xyz.swapee.wc.front.RecordIExchangeStatusHintControllerAT

/** @typedef {xyz.swapee.wc.front.IExchangeStatusHintControllerAT} xyz.swapee.wc.front.BoundIExchangeStatusHintControllerAT */

/** @typedef {xyz.swapee.wc.front.ExchangeStatusHintControllerAT} xyz.swapee.wc.front.BoundExchangeStatusHintControllerAT */

/**
 * Contains getters to cast the _IExchangeStatusHintControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IExchangeStatusHintControllerATCaster
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerATCaster = class { }
/**
 * Cast the _IExchangeStatusHintControllerAT_ instance into the _BoundIExchangeStatusHintControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeStatusHintControllerAT}
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerATCaster.prototype.asIExchangeStatusHintControllerAT
/**
 * Access the _ExchangeStatusHintControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeStatusHintControllerAT}
 */
xyz.swapee.wc.front.IExchangeStatusHintControllerATCaster.prototype.superExchangeStatusHintControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/70-IExchangeStatusHintScreen.xml}  5ec98d3d1265af71ea0eccc29de3b73b */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.front.ExchangeStatusHintInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeStatusHintDisplay.Settings, !xyz.swapee.wc.IExchangeStatusHintDisplay.Queries, null>&xyz.swapee.wc.IExchangeStatusHintDisplay.Initialese} xyz.swapee.wc.IExchangeStatusHintScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintScreen)} xyz.swapee.wc.AbstractExchangeStatusHintScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintScreen} xyz.swapee.wc.ExchangeStatusHintScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintScreen` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintScreen
 */
xyz.swapee.wc.AbstractExchangeStatusHintScreen = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintScreen.constructor&xyz.swapee.wc.ExchangeStatusHintScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintScreen.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintScreen.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintScreen|typeof xyz.swapee.wc.ExchangeStatusHintScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeStatusHintController|typeof xyz.swapee.wc.front.ExchangeStatusHintController)|(!xyz.swapee.wc.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.ExchangeStatusHintDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusHintScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusHintScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintScreen|typeof xyz.swapee.wc.ExchangeStatusHintScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeStatusHintController|typeof xyz.swapee.wc.front.ExchangeStatusHintController)|(!xyz.swapee.wc.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.ExchangeStatusHintDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusHintScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintScreen|typeof xyz.swapee.wc.ExchangeStatusHintScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeStatusHintController|typeof xyz.swapee.wc.front.ExchangeStatusHintController)|(!xyz.swapee.wc.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.ExchangeStatusHintDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusHintScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintScreen.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintScreen} xyz.swapee.wc.ExchangeStatusHintScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.ExchangeStatusHintMemory, !xyz.swapee.wc.front.ExchangeStatusHintInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeStatusHintDisplay.Settings, !xyz.swapee.wc.IExchangeStatusHintDisplay.Queries, null, null>&xyz.swapee.wc.front.IExchangeStatusHintController&xyz.swapee.wc.IExchangeStatusHintDisplay)} xyz.swapee.wc.IExchangeStatusHintScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IExchangeStatusHintScreen
 */
xyz.swapee.wc.IExchangeStatusHintScreen = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IExchangeStatusHintController.typeof&xyz.swapee.wc.IExchangeStatusHintDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintScreen.Initialese>)} xyz.swapee.wc.ExchangeStatusHintScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusHintScreen} xyz.swapee.wc.IExchangeStatusHintScreen.typeof */
/**
 * A concrete class of _IExchangeStatusHintScreen_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintScreen
 * @implements {xyz.swapee.wc.IExchangeStatusHintScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintScreen.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintScreen = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintScreen.constructor&xyz.swapee.wc.IExchangeStatusHintScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.ExchangeStatusHintScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusHintScreen} */
xyz.swapee.wc.RecordIExchangeStatusHintScreen

/** @typedef {xyz.swapee.wc.IExchangeStatusHintScreen} xyz.swapee.wc.BoundIExchangeStatusHintScreen */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintScreen} xyz.swapee.wc.BoundExchangeStatusHintScreen */

/**
 * Contains getters to cast the _IExchangeStatusHintScreen_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintScreenCaster
 */
xyz.swapee.wc.IExchangeStatusHintScreenCaster = class { }
/**
 * Cast the _IExchangeStatusHintScreen_ instance into the _BoundIExchangeStatusHintScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintScreen}
 */
xyz.swapee.wc.IExchangeStatusHintScreenCaster.prototype.asIExchangeStatusHintScreen
/**
 * Access the _ExchangeStatusHintScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintScreen}
 */
xyz.swapee.wc.IExchangeStatusHintScreenCaster.prototype.superExchangeStatusHintScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/70-IExchangeStatusHintScreenBack.xml}  89fff5e8a399ce8ba2e1a0b23ed22edd */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese} xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusHintScreen)} xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusHintScreen} xyz.swapee.wc.back.ExchangeStatusHintScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusHintScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusHintScreen
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.constructor&xyz.swapee.wc.back.ExchangeStatusHintScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusHintScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintScreen|typeof xyz.swapee.wc.back.ExchangeStatusHintScreen)|(!xyz.swapee.wc.back.IExchangeStatusHintScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintScreen|typeof xyz.swapee.wc.back.ExchangeStatusHintScreen)|(!xyz.swapee.wc.back.IExchangeStatusHintScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintScreen|typeof xyz.swapee.wc.back.ExchangeStatusHintScreen)|(!xyz.swapee.wc.back.IExchangeStatusHintScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusHintScreen} xyz.swapee.wc.back.ExchangeStatusHintScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusHintScreenCaster&xyz.swapee.wc.back.IExchangeStatusHintScreenAT)} xyz.swapee.wc.back.IExchangeStatusHintScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusHintScreenAT} xyz.swapee.wc.back.IExchangeStatusHintScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintScreen
 */
xyz.swapee.wc.back.IExchangeStatusHintScreen = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusHintScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeStatusHintScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusHintScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusHintScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese>)} xyz.swapee.wc.back.ExchangeStatusHintScreen.constructor */
/**
 * A concrete class of _IExchangeStatusHintScreen_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusHintScreen
 * @implements {xyz.swapee.wc.back.IExchangeStatusHintScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusHintScreen = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusHintScreen.constructor&xyz.swapee.wc.back.IExchangeStatusHintScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusHintScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreen}
 */
xyz.swapee.wc.back.ExchangeStatusHintScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintScreen} */
xyz.swapee.wc.back.RecordIExchangeStatusHintScreen

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintScreen} xyz.swapee.wc.back.BoundIExchangeStatusHintScreen */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusHintScreen} xyz.swapee.wc.back.BoundExchangeStatusHintScreen */

/**
 * Contains getters to cast the _IExchangeStatusHintScreen_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintScreenCaster
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenCaster = class { }
/**
 * Cast the _IExchangeStatusHintScreen_ instance into the _BoundIExchangeStatusHintScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusHintScreen}
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenCaster.prototype.asIExchangeStatusHintScreen
/**
 * Access the _ExchangeStatusHintScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusHintScreen}
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenCaster.prototype.superExchangeStatusHintScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/73-IExchangeStatusHintScreenAR.xml}  7e909e8d4763c9746b4f18e4100b6258 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeStatusHintScreen.Initialese} xyz.swapee.wc.front.IExchangeStatusHintScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeStatusHintScreenAR)} xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR} xyz.swapee.wc.front.ExchangeStatusHintScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeStatusHintScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.constructor&xyz.swapee.wc.front.ExchangeStatusHintScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintScreenAR|typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusHintScreen|typeof xyz.swapee.wc.ExchangeStatusHintScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintScreenAR|typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusHintScreen|typeof xyz.swapee.wc.ExchangeStatusHintScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusHintScreenAR|typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusHintScreen|typeof xyz.swapee.wc.ExchangeStatusHintScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusHintScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeStatusHintScreenAR.Initialese[]) => xyz.swapee.wc.front.IExchangeStatusHintScreenAR} xyz.swapee.wc.front.ExchangeStatusHintScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeStatusHintScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeStatusHintScreen)} xyz.swapee.wc.front.IExchangeStatusHintScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeStatusHintScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IExchangeStatusHintScreenAR
 */
xyz.swapee.wc.front.IExchangeStatusHintScreenAR = class extends /** @type {xyz.swapee.wc.front.IExchangeStatusHintScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeStatusHintScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusHintScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeStatusHintScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeStatusHintScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusHintScreenAR.Initialese>)} xyz.swapee.wc.front.ExchangeStatusHintScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeStatusHintScreenAR} xyz.swapee.wc.front.IExchangeStatusHintScreenAR.typeof */
/**
 * A concrete class of _IExchangeStatusHintScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeStatusHintScreenAR
 * @implements {xyz.swapee.wc.front.IExchangeStatusHintScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeStatusHintScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusHintScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeStatusHintScreenAR = class extends /** @type {xyz.swapee.wc.front.ExchangeStatusHintScreenAR.constructor&xyz.swapee.wc.front.IExchangeStatusHintScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeStatusHintScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusHintScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeStatusHintScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusHintScreenAR}
 */
xyz.swapee.wc.front.ExchangeStatusHintScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeStatusHintScreenAR} */
xyz.swapee.wc.front.RecordIExchangeStatusHintScreenAR

/** @typedef {xyz.swapee.wc.front.IExchangeStatusHintScreenAR} xyz.swapee.wc.front.BoundIExchangeStatusHintScreenAR */

/** @typedef {xyz.swapee.wc.front.ExchangeStatusHintScreenAR} xyz.swapee.wc.front.BoundExchangeStatusHintScreenAR */

/**
 * Contains getters to cast the _IExchangeStatusHintScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IExchangeStatusHintScreenARCaster
 */
xyz.swapee.wc.front.IExchangeStatusHintScreenARCaster = class { }
/**
 * Cast the _IExchangeStatusHintScreenAR_ instance into the _BoundIExchangeStatusHintScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeStatusHintScreenAR}
 */
xyz.swapee.wc.front.IExchangeStatusHintScreenARCaster.prototype.asIExchangeStatusHintScreenAR
/**
 * Access the _ExchangeStatusHintScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeStatusHintScreenAR}
 */
xyz.swapee.wc.front.IExchangeStatusHintScreenARCaster.prototype.superExchangeStatusHintScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/74-IExchangeStatusHintScreenAT.xml}  2c88ec956b3a1071453e2db3438a8f43 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusHintScreenAT)} xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT} xyz.swapee.wc.back.ExchangeStatusHintScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusHintScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.constructor&xyz.swapee.wc.back.ExchangeStatusHintScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusHintScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusHintScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusHintScreenAT} xyz.swapee.wc.back.ExchangeStatusHintScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusHintScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IExchangeStatusHintScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeStatusHintScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintScreenAT
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenAT = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusHintScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusHintScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese>)} xyz.swapee.wc.back.ExchangeStatusHintScreenAT.constructor */
/**
 * A concrete class of _IExchangeStatusHintScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusHintScreenAT
 * @implements {xyz.swapee.wc.back.IExchangeStatusHintScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeStatusHintScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusHintScreenAT = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusHintScreenAT.constructor&xyz.swapee.wc.back.IExchangeStatusHintScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusHintScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusHintScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusHintScreenAT}
 */
xyz.swapee.wc.back.ExchangeStatusHintScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintScreenAT} */
xyz.swapee.wc.back.RecordIExchangeStatusHintScreenAT

/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintScreenAT} xyz.swapee.wc.back.BoundIExchangeStatusHintScreenAT */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusHintScreenAT} xyz.swapee.wc.back.BoundExchangeStatusHintScreenAT */

/**
 * Contains getters to cast the _IExchangeStatusHintScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusHintScreenATCaster
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenATCaster = class { }
/**
 * Cast the _IExchangeStatusHintScreenAT_ instance into the _BoundIExchangeStatusHintScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusHintScreenAT}
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenATCaster.prototype.asIExchangeStatusHintScreenAT
/**
 * Access the _ExchangeStatusHintScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusHintScreenAT}
 */
xyz.swapee.wc.back.IExchangeStatusHintScreenATCaster.prototype.superExchangeStatusHintScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-hint/ExchangeStatusHint.mvc/design/80-IExchangeStatusHintGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusHintDisplay.Initialese} xyz.swapee.wc.IExchangeStatusHintGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusHintGPU)} xyz.swapee.wc.AbstractExchangeStatusHintGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusHintGPU} xyz.swapee.wc.ExchangeStatusHintGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusHintGPU` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusHintGPU
 */
xyz.swapee.wc.AbstractExchangeStatusHintGPU = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusHintGPU.constructor&xyz.swapee.wc.ExchangeStatusHintGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusHintGPU.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusHintGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusHintGPU.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusHintGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintGPU|typeof xyz.swapee.wc.ExchangeStatusHintGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusHintGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusHintGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusHintGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusHintGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintGPU|typeof xyz.swapee.wc.ExchangeStatusHintGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusHintGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusHintGPU|typeof xyz.swapee.wc.ExchangeStatusHintGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeStatusHintDisplay|typeof xyz.swapee.wc.back.ExchangeStatusHintDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusHintGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusHintGPU.Initialese[]) => xyz.swapee.wc.IExchangeStatusHintGPU} xyz.swapee.wc.ExchangeStatusHintGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusHintGPUCaster&com.webcircuits.IBrowserView<.!ExchangeStatusHintMemory,>&xyz.swapee.wc.back.IExchangeStatusHintDisplay)} xyz.swapee.wc.IExchangeStatusHintGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!ExchangeStatusHintMemory,>} com.webcircuits.IBrowserView<.!ExchangeStatusHintMemory,>.typeof */
/**
 * Handles the periphery of the _IExchangeStatusHintDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IExchangeStatusHintGPU
 */
xyz.swapee.wc.IExchangeStatusHintGPU = class extends /** @type {xyz.swapee.wc.IExchangeStatusHintGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!ExchangeStatusHintMemory,>.typeof&xyz.swapee.wc.back.IExchangeStatusHintDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusHintGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusHintGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusHintGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintGPU.Initialese>)} xyz.swapee.wc.ExchangeStatusHintGPU.constructor */
/**
 * A concrete class of _IExchangeStatusHintGPU_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusHintGPU
 * @implements {xyz.swapee.wc.IExchangeStatusHintGPU} Handles the periphery of the _IExchangeStatusHintDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusHintGPU.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusHintGPU = class extends /** @type {xyz.swapee.wc.ExchangeStatusHintGPU.constructor&xyz.swapee.wc.IExchangeStatusHintGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusHintGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusHintGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusHintGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusHintGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusHintGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusHintGPU}
 */
xyz.swapee.wc.ExchangeStatusHintGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusHintGPU.
 * @interface xyz.swapee.wc.IExchangeStatusHintGPUFields
 */
xyz.swapee.wc.IExchangeStatusHintGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IExchangeStatusHintGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusHintGPU} */
xyz.swapee.wc.RecordIExchangeStatusHintGPU

/** @typedef {xyz.swapee.wc.IExchangeStatusHintGPU} xyz.swapee.wc.BoundIExchangeStatusHintGPU */

/** @typedef {xyz.swapee.wc.ExchangeStatusHintGPU} xyz.swapee.wc.BoundExchangeStatusHintGPU */

/**
 * Contains getters to cast the _IExchangeStatusHintGPU_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusHintGPUCaster
 */
xyz.swapee.wc.IExchangeStatusHintGPUCaster = class { }
/**
 * Cast the _IExchangeStatusHintGPU_ instance into the _BoundIExchangeStatusHintGPU_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusHintGPU}
 */
xyz.swapee.wc.IExchangeStatusHintGPUCaster.prototype.asIExchangeStatusHintGPU
/**
 * Access the _ExchangeStatusHintGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusHintGPU}
 */
xyz.swapee.wc.IExchangeStatusHintGPUCaster.prototype.superExchangeStatusHintGPU

// nss:xyz.swapee.wc
/* @typal-end */