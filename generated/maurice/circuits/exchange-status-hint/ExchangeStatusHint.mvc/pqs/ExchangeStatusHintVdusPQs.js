export const ExchangeStatusHintVdusPQs=/**@type {!xyz.swapee.wc.ExchangeStatusHintVdusPQs}*/({
 AwaitingStatusLa:'ee221',
 ConfirmingStatusLa:'ee222',
 ExchangingStatusLa:'ee223',
 SendingStatusLa:'ee224',
 FinishedStatusLa:'ee225',
 FailedStatusLa:'ee226',
 RefundedStatusLa:'ee227',
 OverdueStatusLa:'ee228',
 HoldStatusLa:'ee229',
 ExpiredStatusLa:'ee2210',
})