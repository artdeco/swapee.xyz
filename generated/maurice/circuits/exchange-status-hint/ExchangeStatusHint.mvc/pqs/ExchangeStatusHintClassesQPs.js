import ExchangeStatusHintClassesPQs from './ExchangeStatusHintClassesPQs'
export const ExchangeStatusHintClassesQPs=/**@type {!xyz.swapee.wc.ExchangeStatusHintClassesQPs}*/(Object.keys(ExchangeStatusHintClassesPQs)
 .reduce((a,k)=>{a[ExchangeStatusHintClassesPQs[k]]=k;return a},{}))