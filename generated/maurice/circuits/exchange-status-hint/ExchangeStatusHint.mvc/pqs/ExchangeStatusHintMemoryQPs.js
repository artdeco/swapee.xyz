import {ExchangeStatusHintMemoryPQs} from './ExchangeStatusHintMemoryPQs'
export const ExchangeStatusHintMemoryQPs=/**@type {!xyz.swapee.wc.ExchangeStatusHintMemoryQPs}*/(Object.keys(ExchangeStatusHintMemoryPQs)
 .reduce((a,k)=>{a[ExchangeStatusHintMemoryPQs[k]]=k;return a},{}))