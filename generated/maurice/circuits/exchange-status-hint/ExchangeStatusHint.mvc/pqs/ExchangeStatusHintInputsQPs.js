import {ExchangeStatusHintInputsPQs} from './ExchangeStatusHintInputsPQs'
export const ExchangeStatusHintInputsQPs=/**@type {!xyz.swapee.wc.ExchangeStatusHintInputsQPs}*/(Object.keys(ExchangeStatusHintInputsPQs)
 .reduce((a,k)=>{a[ExchangeStatusHintInputsPQs[k]]=k;return a},{}))