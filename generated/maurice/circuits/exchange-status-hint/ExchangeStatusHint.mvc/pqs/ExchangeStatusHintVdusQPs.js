import {ExchangeStatusHintVdusPQs} from './ExchangeStatusHintVdusPQs'
export const ExchangeStatusHintVdusQPs=/**@type {!xyz.swapee.wc.ExchangeStatusHintVdusQPs}*/(Object.keys(ExchangeStatusHintVdusPQs)
 .reduce((a,k)=>{a[ExchangeStatusHintVdusPQs[k]]=k;return a},{}))