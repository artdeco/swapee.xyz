import {ExchangeStatusHintMemoryPQs} from './ExchangeStatusHintMemoryPQs'
export const ExchangeStatusHintInputsPQs=/**@type {!xyz.swapee.wc.ExchangeStatusHintInputsQPs}*/({
 ...ExchangeStatusHintMemoryPQs,
})