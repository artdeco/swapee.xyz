import ExchangeStatusHintHtmlController from '../ExchangeStatusHintHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractExchangeStatusHintHtmlComponent} from '../../gen/AbstractExchangeStatusHintHtmlComponent'

/** @extends {xyz.swapee.wc.ExchangeStatusHintHtmlComponent} */
export default class extends AbstractExchangeStatusHintHtmlComponent.implements(
 ExchangeStatusHintHtmlController,
 IntegratedComponentInitialiser,
){}