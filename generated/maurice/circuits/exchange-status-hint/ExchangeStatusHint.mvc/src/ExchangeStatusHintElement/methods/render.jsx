/** @type {xyz.swapee.wc.IExchangeStatusHintElement._render} */
export default function ExchangeStatusHintRender({status:status},{}) {
 return (<div $id="ExchangeStatusHint">
  <span $id="AwaitingStatusLa" $reveal={status=='waiting'} />
  <span $id="ConfirmingStatusLa" $reveal={status=='confirming'} />
  <span $id="ExchangingStatusLa" $reveal={status=='exchanging'} />
  <span $id="SendingStatusLa" $reveal={status=='sending'} />
  <span $id="FinishedStatusLa" $reveal={status=='finished'} />
  <span $id="FailedStatusLa" $reveal={status=='failed'} />
  <span $id="RefundedStatusLa" $reveal={status=='refunded'} />
  <span $id="OverdueStatusLa" $reveal={status=='overdue'} />
  <span $id="HoldStatusLa" $reveal={status=='hold'} />
  <span $id="ExpiredStatusLa" $reveal={status=='expired'} />
 </div>)
}