import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import ExchangeStatusHintServerController from '../ExchangeStatusHintServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractExchangeStatusHintElement from '../../gen/AbstractExchangeStatusHintElement'

/** @extends {xyz.swapee.wc.ExchangeStatusHintElement} */
export default class ExchangeStatusHintElement extends AbstractExchangeStatusHintElement.implements(
 ExchangeStatusHintServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /**@type {!xyz.swapee.wc.ExchangeStatusHintElement}*/({
  allocator: function allocateSelectors(){
   const{
    attributes:{id:id},asIMaurice:{page:page},
    asIExchangeStatusHintElement:{
    },
   }=this
  },
 }),
 /** @type {!xyz.swapee.wc.IExchangeStatusHintElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IExchangeStatusHintElement}*/({
   classesMap: true,
   rootSelector:     `.ExchangeStatusHint`,
   stylesheet:       'html/styles/ExchangeStatusHint.css',
   blockName:        'html/ExchangeStatusHintBlock.html',
  }),
){}

// thank you for using web circuits
