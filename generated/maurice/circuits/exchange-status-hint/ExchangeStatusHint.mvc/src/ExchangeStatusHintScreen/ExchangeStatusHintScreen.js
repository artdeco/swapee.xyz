import deduceInputs from './methods/deduce-inputs'
import AbstractExchangeStatusHintControllerAT from '../../gen/AbstractExchangeStatusHintControllerAT'
import ExchangeStatusHintDisplay from '../ExchangeStatusHintDisplay'
import AbstractExchangeStatusHintScreen from '../../gen/AbstractExchangeStatusHintScreen'

/** @extends {xyz.swapee.wc.ExchangeStatusHintScreen} */
export default class extends AbstractExchangeStatusHintScreen.implements(
 AbstractExchangeStatusHintControllerAT,
 ExchangeStatusHintDisplay,
 /**@type {!xyz.swapee.wc.IExchangeStatusHintScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IExchangeStatusHintScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:2700153076,
 }),
){}