import AbstractHyperExchangeStatusHintInterruptLine from '../../../../gen/AbstractExchangeStatusHintInterruptLine/hyper/AbstractHyperExchangeStatusHintInterruptLine'
import ExchangeStatusHintInterruptLine from '../../ExchangeStatusHintInterruptLine'
import ExchangeStatusHintInterruptLineGeneralAspects from '../ExchangeStatusHintInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperExchangeStatusHintInterruptLine} */
export default class extends AbstractHyperExchangeStatusHintInterruptLine
 .consults(
  ExchangeStatusHintInterruptLineGeneralAspects,
 )
 .implements(
  ExchangeStatusHintInterruptLine,
 )
{}