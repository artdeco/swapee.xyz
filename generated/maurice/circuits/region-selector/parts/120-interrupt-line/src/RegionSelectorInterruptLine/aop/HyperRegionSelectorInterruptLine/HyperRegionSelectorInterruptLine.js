import AbstractHyperRegionSelectorInterruptLine from '../../../../gen/AbstractRegionSelectorInterruptLine/hyper/AbstractHyperRegionSelectorInterruptLine'
import RegionSelectorInterruptLine from '../../RegionSelectorInterruptLine'
import RegionSelectorInterruptLineGeneralAspects from '../RegionSelectorInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperRegionSelectorInterruptLine} */
export default class extends AbstractHyperRegionSelectorInterruptLine
 .consults(
  RegionSelectorInterruptLineGeneralAspects,
 )
 .implements(
  RegionSelectorInterruptLine,
 )
{}