/** @extends {xyz.swapee.wc.AbstractRegionSelector} */
export default class AbstractRegionSelector extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractRegionSelectorComputer} */
export class AbstractRegionSelectorComputer extends (<computer>
   <adapter name="adaptRegion">
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingEu" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingUs" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingLondon" diff2 />

    <xyz.swapee.wc.IRegionPingCore $id="RegionPingSaoPaulo" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingSydney" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingTaiwan" diff2 />

    <xyz.swapee.wc.IRegionPingCore $id="RegionPingBelgium" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingNetherlands" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingParis" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingFrankfurt" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingWarsaw" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingZurich" diff2 />

    <xyz.swapee.wc.IRegionPingCore $id="RegionPingMumbai" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingDelhi" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingSingapore" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingJakarta" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingHongKong" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingTokyo" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingOsaka" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingSeoul" diff2 />

    <xyz.swapee.wc.IRegionPingCore $id="RegionPingDoha" diff2 />
    <xyz.swapee.wc.IRegionPingCore $id="RegionPingTelAviv" diff2 />
    <outputs>
     <xyz.swapee.wc.IRegionSelectorCore region />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractRegionSelectorController} */
export class AbstractRegionSelectorController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractRegionSelectorPort} */
export class RegionSelectorPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractRegionSelectorView} */
export class AbstractRegionSelectorView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractRegionSelectorElement} */
export class AbstractRegionSelectorElement extends (<element v3 html mv>
 <block src="./RegionSelector.mvc/src/RegionSelectorElement/methods/render.jsx" />
 <inducer src="./RegionSelector.mvc/src/RegionSelectorElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractRegionSelectorHtmlComponent} */
export class AbstractRegionSelectorHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IRegionPing via="RegionPingEu" />
   <xyz.swapee.wc.IRegionPing via="RegionPingUs" />
   <xyz.swapee.wc.IRegionPing via="RegionPingLondon" />
   <xyz.swapee.wc.IRegionPing via="RegionPingSaoPaulo" />
   <xyz.swapee.wc.IRegionPing via="RegionPingSydney" />
   <xyz.swapee.wc.IRegionPing via="RegionPingTaiwan" />
   <xyz.swapee.wc.IRegionPing via="RegionPingBelgium" />
   <xyz.swapee.wc.IRegionPing via="RegionPingNetherlands" />
   <xyz.swapee.wc.IRegionPing via="RegionPingParis" />
   <xyz.swapee.wc.IRegionPing via="RegionPingFrankfurt" />
   <xyz.swapee.wc.IRegionPing via="RegionPingWarsaw" />
   <xyz.swapee.wc.IRegionPing via="RegionPingZurich" />
   <xyz.swapee.wc.IRegionPing via="RegionPingMumbai" />
   <xyz.swapee.wc.IRegionPing via="RegionPingDelhi" />
   <xyz.swapee.wc.IRegionPing via="RegionPingSingapore" />
   <xyz.swapee.wc.IRegionPing via="RegionPingJakarta" />
   <xyz.swapee.wc.IRegionPing via="RegionPingHongKong" />
   <xyz.swapee.wc.IRegionPing via="RegionPingTokyo" />
   <xyz.swapee.wc.IRegionPing via="RegionPingOsaka" />
   <xyz.swapee.wc.IRegionPing via="RegionPingSeoul" />
   <xyz.swapee.wc.IRegionPing via="RegionPingDoha" />
   <xyz.swapee.wc.IRegionPing via="RegionPingTelAviv" />
  </connectors>

</html-ic>) { }
// </class-end>