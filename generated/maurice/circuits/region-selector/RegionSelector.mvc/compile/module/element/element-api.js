import { AbstractRegionSelector, RegionSelectorPort, AbstractRegionSelectorController,
 RegionSelectorElement, RegionSelectorBuffer, AbstractRegionSelectorComputer,
 RegionSelectorController } from './element-exports'

/** @lazy @api {xyz.swapee.wc.AbstractRegionSelector} */
export { AbstractRegionSelector }
/** @lazy @api {xyz.swapee.wc.RegionSelectorPort} */
export { RegionSelectorPort }
/** @lazy @api {xyz.swapee.wc.AbstractRegionSelectorController} */
export { AbstractRegionSelectorController }
/** @lazy @api {xyz.swapee.wc.RegionSelectorElement} */
export { RegionSelectorElement }
/** @lazy @api {xyz.swapee.wc.RegionSelectorBuffer} */
export { RegionSelectorBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractRegionSelectorComputer} */
export { AbstractRegionSelectorComputer }
/** @lazy @api {xyz.swapee.wc.RegionSelectorController} */
export { RegionSelectorController }