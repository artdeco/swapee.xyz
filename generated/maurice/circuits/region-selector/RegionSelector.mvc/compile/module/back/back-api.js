import { AbstractRegionSelector, RegionSelectorPort, AbstractRegionSelectorController,
 RegionSelectorHtmlComponent, RegionSelectorBuffer,
 AbstractRegionSelectorComputer, RegionSelectorComputer,
 RegionSelectorController } from './back-exports'

/** @lazy @api {xyz.swapee.wc.AbstractRegionSelector} */
export { AbstractRegionSelector }
/** @lazy @api {xyz.swapee.wc.RegionSelectorPort} */
export { RegionSelectorPort }
/** @lazy @api {xyz.swapee.wc.AbstractRegionSelectorController} */
export { AbstractRegionSelectorController }
/** @lazy @api {xyz.swapee.wc.RegionSelectorHtmlComponent} */
export { RegionSelectorHtmlComponent }
/** @lazy @api {xyz.swapee.wc.RegionSelectorBuffer} */
export { RegionSelectorBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractRegionSelectorComputer} */
export { AbstractRegionSelectorComputer }
/** @lazy @api {xyz.swapee.wc.RegionSelectorComputer} */
export { RegionSelectorComputer }
/** @lazy @api {xyz.swapee.wc.back.RegionSelectorController} */
export { RegionSelectorController }