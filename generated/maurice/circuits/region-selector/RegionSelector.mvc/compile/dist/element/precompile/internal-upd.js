import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {9509928906} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const a=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=a["372700389811"];function e(b,d,f,h){return a["372700389812"](b,d,f,h,!1,void 0)};function g(){}g.prototype={};class k{}class l extends e(k,95099289069,null,{ga:1,Va:2}){}l[c]=[g];

const m=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const n=m["615055805212"],p=m["615055805218"],q=m["615055805233"],r=m["615055805235"];const t={ping:"df911",region:"960db",g:"030c5"};const v={j:"8e8e4",i:"21528"};function w(){}w.prototype={};function x(){this.model={j:null,i:null}}class aa{}class y extends e(aa,95099289068,x,{aa:1,Qa:2}){}function z(){}z.prototype={};function A(){this.model={region:null,g:[]}}class ba{}class B extends e(ba,95099289063,A,{ea:1,Ta:2}){}B[c]=[z,{constructor(){q(this.model,"",t);q(this.model,"",v)}}];y[c]=[{},w,B];
const C=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ca=C.IntegratedComponentInitialiser,D=C.IntegratedComponent,da=C["95173443851"];function E(){}E.prototype={};class ea{}class F extends e(ea,95099289061,null,{Z:1,Oa:2}){}F[c]=[E,p];const G={regulate:n({region:String,g:[String]})};
const H=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const fa=H.IntegratedController,ha=H.Parametric;const I={...t};function J(){}J.prototype={};function ia(){const b={model:null};A.call(b);this.inputs=b.model}class ja{}class K extends e(ja,95099289065,ia,{fa:1,Ua:2}){}function L(){}K[c]=[L.prototype={},J,ha,L.prototype={constructor(){q(this.inputs,"",I)}}];function M(){}M.prototype={};class ka{}class N extends e(ka,950992890621,null,{$:1,Pa:2}){}N[c]=[{},M,G,fa,{get Port(){return K}}];const la=n({j:Number,i:Number},{silent:!0});const ma=Object.keys(v).reduce((b,d)=>{b[v[d]]=d;return b},{});function O(){}O.prototype={};class na{}class P extends e(na,950992890610,null,{Y:1,Na:2}){}P[c]=[O,y,l,D,F,N,{regulateState:la,stateQPs:ma}];function oa(){return{}};function pa(b){const d=this.model.g;return b=b.replace(new RegExp(/\\s+(\\x3c!--)( \\$if: (.+?) --\\x3e[\\s\\S]+?\\x3c!-- \\/\\$if: (.+?) )(--\\x3e)/.source,"g"),(f,h,Q,u)=>d.includes(u)?f:"")};const qa=require(eval('"@type.engineering/web-computing"')).h;function ra(){return qa("div",{$id:"RegionSelector"})};const R=require(eval('"@type.engineering/web-computing"')).h;function sa({region:b}){return R("div",{$id:"RegionSelector"},R("div",{$id:"SuggestedRegionWr"},R("span",{$id:"SuggestedRegionLa"},b||"-")))};var S=class extends N.implements(){};require("https");require("http");const ta=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(b){}ta("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const ua=T.ElementBase,va=T.HTMLBlocker;const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function wa(){this.inputs={noSolder:!1,oa:{},ja:{},ka:{},na:{},la:{},ma:{},La:{},Ma:{},ia:{},ha:{},Wa:{},sa:{},Ia:{},wa:{},Ba:{},Ea:{},Fa:{},pa:{},ya:{},Aa:{},ta:{},Ja:{},Ka:{},xa:{},qa:{},Da:{},va:{},ua:{},Ha:{},za:{},Ca:{},ra:{},Ga:{}}}class xa{}class W extends e(xa,950992890615,wa,{da:1,Sa:2}){}
W[c]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"ping-to-us-la-opts":void 0,"ping-to-eu-la-opts":void 0,"ping-to-lnd-la-opts":void 0,"ping-to-taiwan-la-opts":void 0,"ping-to-sao-paulo-la-opts":void 0,"ping-to-sydney-la-opts":void 0,"suggested-region-la-opts":void 0,"suggested-region-wr-opts":void 0,"ldn-ping-wr-opts":void 0,"eu-ping-wr-opts":void 0,"us-ping-wr-opts":void 0,"region-ping-eu-opts":void 0,"region-ping-us-opts":void 0,"region-ping-london-opts":void 0,
"region-ping-sao-paulo-opts":void 0,"region-ping-sydney-opts":void 0,"region-ping-taiwan-opts":void 0,"region-ping-belgium-opts":void 0,"region-ping-netherlands-opts":void 0,"region-ping-paris-opts":void 0,"region-ping-frankfurt-opts":void 0,"region-ping-warsaw-opts":void 0,"region-ping-zurich-opts":void 0,"region-ping-mumbai-opts":void 0,"region-ping-delhi-opts":void 0,"region-ping-singapore-opts":void 0,"region-ping-jakarta-opts":void 0,"region-ping-hong-kong-opts":void 0,"region-ping-tokyo-opts":void 0,
"region-ping-osaka-opts":void 0,"region-ping-seoul-opts":void 0,"region-ping-doha-opts":void 0,"region-ping-tel-aviv-opts":void 0})}}];function X(){}X.prototype={};class ya{}class Y extends e(ya,950992890614,null,{ba:1,Ra:2}){}function Z(){}
Y[c]=[X,ua,Z.prototype={calibrate:function({":no-solder":b,":region":d,":regions":f}){const {attributes:{"no-solder":h,region:Q,regions:u}}=this;return{...(void 0===h?{"no-solder":b}:{}),...(void 0===Q?{region:d}:{}),...(void 0===u?{regions:f}:{})}}},Z.prototype={calibrate:({"no-solder":b,region:d,regions:f})=>({noSolder:b,region:d,g:f})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return U("div",{$id:"RegionSelector"},U("vdu",{$id:"SuggestedRegionWr"}),U("vdu",
{$id:"SuggestedRegionLa"}),U("vdu",{$id:"PingToUsLa"}),U("vdu",{$id:"PingToEuLa"}),U("vdu",{$id:"PingToLndLa"}),U("vdu",{$id:"PingToTaiwanLa"}),U("vdu",{$id:"PingToSaoPauloLa"}),U("vdu",{$id:"PingToSydneyLa"}),U("vdu",{$id:"LdnPingWr"}),U("vdu",{$id:"EuPingWr"}),U("vdu",{$id:"UsPingWr"}),U("vdu",{$id:"RegionPingEu"}),U("vdu",{$id:"RegionPingUs"}),U("vdu",{$id:"RegionPingLondon"}),U("vdu",{$id:"RegionPingSaoPaulo"}),U("vdu",{$id:"RegionPingSydney"}),U("vdu",{$id:"RegionPingTaiwan"}),U("vdu",{$id:"RegionPingBelgium"}),
U("vdu",{$id:"RegionPingNetherlands"}),U("vdu",{$id:"RegionPingParis"}),U("vdu",{$id:"RegionPingFrankfurt"}),U("vdu",{$id:"RegionPingWarsaw"}),U("vdu",{$id:"RegionPingZurich"}),U("vdu",{$id:"RegionPingMumbai"}),U("vdu",{$id:"RegionPingDelhi"}),U("vdu",{$id:"RegionPingSingapore"}),U("vdu",{$id:"RegionPingJakarta"}),U("vdu",{$id:"RegionPingHongKong"}),U("vdu",{$id:"RegionPingTokyo"}),U("vdu",{$id:"RegionPingOsaka"}),U("vdu",{$id:"RegionPingSeoul"}),U("vdu",{$id:"RegionPingDoha"}),U("vdu",{$id:"RegionPingTelAviv"}))}},
Z.prototype={inputsPQs:I,cachePQs:v,vdus:{PingToEuLa:"d1ea1",PingToUsLa:"d1ea2",RegionPingEu:"d1ea3",RegionPingUs:"d1ea4",EuPingWr:"d1ea5",UsPingWr:"d1ea6",RegionPingLondon:"d1ea7",PingToLndLa:"d1ea9",LdnPingWr:"d1ea10",SuggestedRegionLa:"d1ea11",SuggestedRegionWr:"d1ea12",PingToTaiwanLa:"d1ea13",PingToSaoPauloLa:"d1ea14",PingToSydneyLa:"d1ea15",RegionPingSaoPaulo:"d1ea16",RegionPingSydney:"d1ea17",RegionPingTaiwan:"d1ea18",RegionPingBelgium:"d1ea19",RegionPingNetherlands:"d1ea20",RegionPingParis:"d1ea21",
RegionPingFrankfurt:"d1ea22",RegionPingWarsaw:"d1ea23",RegionPingZurich:"d1ea24",RegionPingMumbai:"d1ea25",RegionPingDelhi:"d1ea26",RegionPingSingapore:"d1ea27",RegionPingJakarta:"d1ea28",RegionPingHongKong:"d1ea29",RegionPingTokyo:"d1ea30",RegionPingOsaka:"d1ea31",RegionPingSeoul:"d1ea32",RegionPingDoha:"d1ea33",RegionPingTelAviv:"d1ea35"}},D,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder region regions no-solder :no-solder :region :regions fe646 4fa17 9ee87 e4d5e deda0 3e63e 62750 41de1 7a599 94136 53b3d 82510 44f12 aa610 a828d 36e2a 531b9 54af8 a74b4 2e658 aaba6 909ad 2ce50 7c56f db0fc 53914 c720d aadf1 be8b4 bb0cf ccdc7 ceeee a733e 78a5e 960db 030c5 children".split(" "))})},
get Port(){return W}},r,Z.prototype={constructor(){this.land={s:null,V:null,D:null,J:null,P:null,R:null,l:null,G:null,I:null,u:null,W:null,X:null,F:null,m:null,M:null,A:null,v:null,U:null,H:null,K:null,o:null,T:null}}}];Y[c]=[P,{rootId:"RegionSelector",__$id:9509928906,fqn:"xyz.swapee.wc.IRegionSelector",maurice_element_v3:!0}];class za extends Y.implements(S,da,va,ca,{solder:oa,PostProcess:pa,server:ra,render:sa},{classesMap:!0,rootSelector:".RegionSelector",stylesheet:"html/styles/RegionSelector.css",blockName:"html/RegionSelectorBlock.html"}){};module.exports["95099289060"]=P;module.exports["95099289061"]=P;module.exports["95099289063"]=K;module.exports["95099289064"]=N;module.exports["95099289068"]=za;module.exports["950992890611"]=G;module.exports["950992890630"]=F;module.exports["950992890661"]=S;
/*! @embed-object-end {9509928906} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule