/**
 * An abstract class of `xyz.swapee.wc.IRegionSelector` interface.
 * @extends {xyz.swapee.wc.AbstractRegionSelector}
 */
class AbstractRegionSelector extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IRegionSelector_, providing input
 * pins.
 * @extends {xyz.swapee.wc.RegionSelectorPort}
 */
class RegionSelectorPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionSelectorController` interface.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorController}
 */
class AbstractRegionSelectorController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IRegionSelector_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.RegionSelectorElement}
 */
class RegionSelectorElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.RegionSelectorBuffer}
 */
class RegionSelectorBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionSelectorComputer` interface.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorComputer}
 */
class AbstractRegionSelectorComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.RegionSelectorController}
 */
class RegionSelectorController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractRegionSelector = AbstractRegionSelector
module.exports.RegionSelectorPort = RegionSelectorPort
module.exports.AbstractRegionSelectorController = AbstractRegionSelectorController
module.exports.RegionSelectorElement = RegionSelectorElement
module.exports.RegionSelectorBuffer = RegionSelectorBuffer
module.exports.AbstractRegionSelectorComputer = AbstractRegionSelectorComputer
module.exports.RegionSelectorController = RegionSelectorController

Object.defineProperties(module.exports, {
 'AbstractRegionSelector': {get: () => require('./precompile/internal')[95099289061]},
 [95099289061]: {get: () => module.exports['AbstractRegionSelector']},
 'RegionSelectorPort': {get: () => require('./precompile/internal')[95099289063]},
 [95099289063]: {get: () => module.exports['RegionSelectorPort']},
 'AbstractRegionSelectorController': {get: () => require('./precompile/internal')[95099289064]},
 [95099289064]: {get: () => module.exports['AbstractRegionSelectorController']},
 'RegionSelectorElement': {get: () => require('./precompile/internal')[95099289068]},
 [95099289068]: {get: () => module.exports['RegionSelectorElement']},
 'RegionSelectorBuffer': {get: () => require('./precompile/internal')[950992890611]},
 [950992890611]: {get: () => module.exports['RegionSelectorBuffer']},
 'AbstractRegionSelectorComputer': {get: () => require('./precompile/internal')[950992890630]},
 [950992890630]: {get: () => module.exports['AbstractRegionSelectorComputer']},
 'RegionSelectorController': {get: () => require('./precompile/internal')[950992890661]},
 [950992890661]: {get: () => module.exports['RegionSelectorController']},
})