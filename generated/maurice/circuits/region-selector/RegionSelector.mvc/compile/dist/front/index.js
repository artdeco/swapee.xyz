/**
 * Display for presenting information from the _IRegionSelector_.
 * @extends {xyz.swapee.wc.RegionSelectorDisplay}
 */
class RegionSelectorDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.RegionSelectorScreen}
 */
class RegionSelectorScreen extends (class {/* lazy-loaded */}) {}

module.exports.RegionSelectorDisplay = RegionSelectorDisplay
module.exports.RegionSelectorScreen = RegionSelectorScreen