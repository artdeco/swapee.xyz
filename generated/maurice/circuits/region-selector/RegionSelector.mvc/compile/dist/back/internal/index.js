import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractRegionSelector}*/
export class AbstractRegionSelector extends Module['95099289061'] {}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelector} */
AbstractRegionSelector.class=function(){}
/** @type {typeof xyz.swapee.wc.RegionSelectorPort} */
export const RegionSelectorPort=Module['95099289063']
/**@extends {xyz.swapee.wc.AbstractRegionSelectorController}*/
export class AbstractRegionSelectorController extends Module['95099289064'] {}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorController} */
AbstractRegionSelectorController.class=function(){}
/** @type {typeof xyz.swapee.wc.RegionSelectorHtmlComponent} */
export const RegionSelectorHtmlComponent=Module['950992890610']
/** @type {typeof xyz.swapee.wc.RegionSelectorBuffer} */
export const RegionSelectorBuffer=Module['950992890611']
/**@extends {xyz.swapee.wc.AbstractRegionSelectorComputer}*/
export class AbstractRegionSelectorComputer extends Module['950992890630'] {}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorComputer} */
AbstractRegionSelectorComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.RegionSelectorComputer} */
export const RegionSelectorComputer=Module['950992890631']
/** @type {typeof xyz.swapee.wc.back.RegionSelectorController} */
export const RegionSelectorController=Module['950992890661']