import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {9509928906} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=e["372700389810"],f=e["372700389811"];function h(a,c,d,g){return e["372700389812"](a,c,d,g,!1,void 0)};function E(){}E.prototype={};class ba{}class F extends h(ba,95099289069,null,{Oa:1,$a:2}){}F[f]=[E];
const G=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=G["61505580523"],H=G["615055805212"],da=G["615055805218"],ea=G["615055805221"],fa=G["615055805223"],I=G["615055805233"],ha=G["615055805235"];const J={ping:"df911",region:"960db",ca:"030c5"};const K={da:"8e8e4",ba:"21528"};function L(){}L.prototype={};function ia(){this.model={da:null,ba:null}}class ja{}class M extends h(ja,95099289068,ia,{Ja:1,Ua:2}){}function N(){}N.prototype={};function O(){this.model={region:null,ca:[]}}class ka{}class P extends h(ka,95099289063,O,{Ma:1,Ya:2}){}P[f]=[N,{constructor(){I(this.model,"",J);I(this.model,"",K)}}];M[f]=[{},L,P];

const Q=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=Q.IntegratedController,ma=Q.Parametric;
const R=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const na=R.IntegratedComponentInitialiser,ya=R.IntegratedComponent;function S(){}S.prototype={};class za{}class T extends h(za,95099289061,null,{Ha:1,Sa:2}){}T[f]=[S,da];const Aa={regulate:H({region:String,ca:[String]})};const U={...J};function Ba(){}Ba.prototype={};function Ca(){const a={model:null};O.call(a);this.inputs=a.model}class Da{}class V extends h(Da,95099289065,Ca,{Na:1,Za:2}){}function Ea(){}V[f]=[Ea.prototype={},Ba,ma,Ea.prototype={constructor(){I(this.inputs,"",U)}}];function Fa(){}Fa.prototype={};class Ga{}class W extends h(Ga,950992890621,null,{fa:1,Fa:2}){}W[f]=[{},Fa,Aa,la,{get Port(){return V}}];const Ha=H({da:Number,ba:Number},{silent:!0});const Ia=Object.keys(K).reduce((a,c)=>{a[K[c]]=c;return a},{});function Ja(){}Ja.prototype={};class Ka{}class X extends h(Ka,950992890610,null,{Ga:1,Ra:2}){}X[f]=[Ja,M,F,ya,T,W,{regulateState:Ha,stateQPs:Ia}];
const Y=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const La=Y["12817393923"],Ma=Y["12817393924"],Na=Y["12817393925"],Oa=Y["12817393926"];function Pa(){}Pa.prototype={};class Qa{}class Ra extends h(Qa,950992890624,null,{Ia:1,Ta:2}){}Ra[f]=[Pa,Oa,{allocator(){this.methods={}}}];function Sa(){}Sa.prototype={};class Ta{}class Ua extends h(Ta,950992890623,null,{fa:1,Fa:2}){}Ua[f]=[Sa,W,Ra,La];var Va=class extends Ua.implements(){};function Wa({ua:a,ya:c,za:d,ka:g,pa:k,Ca:l,ha:m,la:n,ra:p,Ea:q,ta:r,Da:t,ia:u,ma:v,oa:w,qa:x,sa:y,wa:z,xa:A,Ba:B,ja:C,Aa:D}){const b=Math.min(...[g,k,l,a,c,d,m,n,p,q,r,t,u,v,w,x,y,z,A,B,C,D].filter(Boolean));return b==D?{region:"te"}:b==C?{region:"do"}:b==B?{region:"to"}:b==A?{region:"si"}:b==z?{region:"se"}:b==y?{region:"os"}:b==x?{region:"mu"}:b==w?{region:"ja"}:b==v?{region:"ho"}:b==u?{region:"de"}:b==g?{region:"eu"}:b==k?{region:"ldn"}:b==l?{region:"us"}:b==a?{region:"sp"}:b==c?{region:"sy"}:
b==r?{region:"pa"}:b==t?{region:"wa"}:b==q?{region:"zu"}:b==m?{region:"be"}:b==n?{region:"fr"}:b==p?{region:"ne"}:b==d?{region:"ta"}:{region:""}};function Xa(a,c,d){a={ka:this.land.j?this.land.j.model.e559c:void 0,Ca:this.land.L?this.land.L.model.e559c:void 0,pa:this.land.s?this.land.s.model.e559c:void 0,ua:this.land.D?this.land.D.model.e559c:void 0,ya:this.land.H?this.land.H.model.e559c:void 0,za:this.land.I?this.land.I.model.e559c:void 0,ha:this.land.g?this.land.g.model.e559c:void 0,ra:this.land.v?this.land.v.model.e559c:void 0,ta:this.land.C?this.land.C.model.e559c:void 0,la:this.land.l?this.land.l.model.e559c:void 0,Da:this.land.M?this.land.M.model.e559c:
void 0,Ea:this.land.O?this.land.O.model.e559c:void 0,qa:this.land.u?this.land.u.model.e559c:void 0,ia:this.land.h?this.land.h.model.e559c:void 0,xa:this.land.G?this.land.G.model.e559c:void 0,oa:this.land.o?this.land.o.model.e559c:void 0,ma:this.land.m?this.land.m.model.e559c:void 0,Ba:this.land.K?this.land.K.model.e559c:void 0,sa:this.land.A?this.land.A.model.e559c:void 0,wa:this.land.F?this.land.F.model.e559c:void 0,ja:this.land.i?this.land.i.model.e559c:void 0,Aa:this.land.J?this.land.J.model.e559c:
void 0};a=d?d(a):a;c=d?d(c):c;return this.ea(a,c)};class Ya extends T.implements({ea:Wa,adapt:[Xa]}){};function Za(){}Za.prototype={};class $a{}class ab extends h($a,950992890620,null,{Ka:1,Va:2}){}
ab[f]=[Za,Ma,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{$:a,P:a,Z:a,U:a,V:a,Y:a,W:a,X:a,T:a,R:a,aa:a,j:a,L:a,s:a,D:a,H:a,I:a,g:a,v:a,C:a,l:a,M:a,O:a,u:a,h:a,G:a,o:a,m:a,K:a,A:a,F:a,i:a,J:a})}},{[aa]:{Z:1,U:1,V:1,Y:1,W:1,X:1,P:1,$:1,T:1,R:1,aa:1,j:1,L:1,s:1,D:1,H:1,I:1,g:1,v:1,C:1,l:1,M:1,O:1,u:1,h:1,G:1,o:1,m:1,K:1,A:1,F:1,i:1,J:1},initializer({Z:a,U:c,V:d,Y:g,W:k,X:l,P:m,$:n,T:p,R:q,aa:r,j:t,L:u,s:v,D:w,H:x,I:y,g:z,v:A,C:B,l:C,
M:D,O:b,u:oa,h:pa,G:qa,o:ra,m:sa,K:ta,A:ua,F:va,i:wa,J:xa}){void 0!==a&&(this.Z=a);void 0!==c&&(this.U=c);void 0!==d&&(this.V=d);void 0!==g&&(this.Y=g);void 0!==k&&(this.W=k);void 0!==l&&(this.X=l);void 0!==m&&(this.P=m);void 0!==n&&(this.$=n);void 0!==p&&(this.T=p);void 0!==q&&(this.R=q);void 0!==r&&(this.aa=r);void 0!==t&&(this.j=t);void 0!==u&&(this.L=u);void 0!==v&&(this.s=v);void 0!==w&&(this.D=w);void 0!==x&&(this.H=x);void 0!==y&&(this.I=y);void 0!==z&&(this.g=z);void 0!==A&&(this.v=A);void 0!==
B&&(this.C=B);void 0!==C&&(this.l=C);void 0!==D&&(this.M=D);void 0!==b&&(this.O=b);void 0!==oa&&(this.u=oa);void 0!==pa&&(this.h=pa);void 0!==qa&&(this.G=qa);void 0!==ra&&(this.o=ra);void 0!==sa&&(this.m=sa);void 0!==ta&&(this.K=ta);void 0!==ua&&(this.A=ua);void 0!==va&&(this.F=va);void 0!==wa&&(this.i=wa);void 0!==xa&&(this.J=xa)}}];const bb={U:"d1ea1",Z:"d1ea2",j:"d1ea3",L:"d1ea4",R:"d1ea5",aa:"d1ea6",s:"d1ea7",V:"d1ea9",T:"d1ea10",P:"d1ea11",$:"d1ea12",Y:"d1ea13",W:"d1ea14",X:"d1ea15",D:"d1ea16",H:"d1ea17",I:"d1ea18",g:"d1ea19",v:"d1ea20",C:"d1ea21",l:"d1ea22",M:"d1ea23",O:"d1ea24",u:"d1ea25",h:"d1ea26",G:"d1ea27",o:"d1ea28",m:"d1ea29",K:"d1ea30",A:"d1ea31",F:"d1ea32",i:"d1ea33",J:"d1ea35"};const cb=Object.keys(bb).reduce((a,c)=>{a[bb[c]]=c;return a},{});function db(){}db.prototype={};class eb{}class fb extends h(eb,950992890617,null,{ga:1,Wa:2}){}fb[f]=[db,{vdusQPs:cb,memoryPQs:J},ab,ca];function gb(){}gb.prototype={};class hb{}class ib extends h(hb,950992890629,null,{Qa:1,bb:2}){}ib[f]=[gb,Na];function jb(){}jb.prototype={};class kb{}class lb extends h(kb,950992890627,null,{Pa:1,ab:2}){}lb[f]=[jb,ib];const mb=Object.keys(U).reduce((a,c)=>{a[U[c]]=c;return a},{});function nb(){}nb.prototype={};class ob{static mvc(a,c,d){return fa(this,a,c,null,d)}}class pb extends h(ob,950992890613,null,{La:1,Xa:2}){}function Z(){}
pb[f]=[nb,ea,X,fb,lb,ha,Z.prototype={constructor(){this.land={j:null,L:null,s:null,D:null,H:null,I:null,g:null,v:null,C:null,l:null,M:null,O:null,u:null,h:null,G:null,o:null,m:null,K:null,A:null,F:null,i:null,J:null}}},Z.prototype={inputsQPs:mb},{paint:[function(){this.P.setText(this.model.region||"-")}]},Z.prototype={scheduleTwo:function(){const {asIHtmlComponent:{complete:a},ga:{j:c,L:d,s:g,D:k,H:l,I:m,g:n,v:p,C:q,l:r,M:t,O:u,u:v,h:w,G:x,o:y,m:z,K:A,A:B,F:C,i:D,J:b}}=this;a(1631224845,{j:c});a(1631224845,
{L:d});a(1631224845,{s:g});a(1631224845,{D:k});a(1631224845,{H:l});a(1631224845,{I:m});a(1631224845,{g:n});a(1631224845,{v:p});a(1631224845,{C:q});a(1631224845,{l:r});a(1631224845,{M:t});a(1631224845,{O:u});a(1631224845,{u:v});a(1631224845,{h:w});a(1631224845,{G:x});a(1631224845,{o:y});a(1631224845,{m:z});a(1631224845,{K:A});a(1631224845,{A:B});a(1631224845,{F:C});a(1631224845,{i:D});a(1631224845,{J:b})}}];var qb=class extends pb.implements(Va,Ya,na){};module.exports["95099289060"]=X;module.exports["95099289061"]=X;module.exports["95099289063"]=V;module.exports["95099289064"]=W;module.exports["950992890610"]=qb;module.exports["950992890611"]=Aa;module.exports["950992890630"]=T;module.exports["950992890631"]=Ya;module.exports["950992890661"]=Va;
/*! @embed-object-end {9509928906} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule