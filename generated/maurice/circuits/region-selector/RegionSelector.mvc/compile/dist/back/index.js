/**
 * An abstract class of `xyz.swapee.wc.IRegionSelector` interface.
 * @extends {xyz.swapee.wc.AbstractRegionSelector}
 */
class AbstractRegionSelector extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IRegionSelector_, providing input
 * pins.
 * @extends {xyz.swapee.wc.RegionSelectorPort}
 */
class RegionSelectorPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionSelectorController` interface.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorController}
 */
class AbstractRegionSelectorController extends (class {/* lazy-loaded */}) {}
/**
 * The _IRegionSelector_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.RegionSelectorHtmlComponent}
 */
class RegionSelectorHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.RegionSelectorBuffer}
 */
class RegionSelectorBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IRegionSelectorComputer` interface.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorComputer}
 */
class AbstractRegionSelectorComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.RegionSelectorComputer}
 */
class RegionSelectorComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.RegionSelectorController}
 */
class RegionSelectorController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractRegionSelector = AbstractRegionSelector
module.exports.RegionSelectorPort = RegionSelectorPort
module.exports.AbstractRegionSelectorController = AbstractRegionSelectorController
module.exports.RegionSelectorHtmlComponent = RegionSelectorHtmlComponent
module.exports.RegionSelectorBuffer = RegionSelectorBuffer
module.exports.AbstractRegionSelectorComputer = AbstractRegionSelectorComputer
module.exports.RegionSelectorComputer = RegionSelectorComputer
module.exports.RegionSelectorController = RegionSelectorController