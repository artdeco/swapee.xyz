/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IRegionSelectorComputer': {
  'id': 95099289061,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptRegion': 2
  }
 },
 'xyz.swapee.wc.RegionSelectorMemoryPQs': {
  'id': 95099289062,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorOuterCore': {
  'id': 95099289063,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.RegionSelectorInputsPQs': {
  'id': 95099289064,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorPort': {
  'id': 95099289065,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetRegionSelectorPort': 2
  }
 },
 'xyz.swapee.wc.IRegionSelectorPortInterface': {
  'id': 95099289066,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.RegionSelectorCachePQs': {
  'id': 95099289067,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorCore': {
  'id': 95099289068,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetRegionSelectorCore': 2
  }
 },
 'xyz.swapee.wc.IRegionSelectorProcessor': {
  'id': 95099289069,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelector': {
  'id': 950992890610,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorBuffer': {
  'id': 950992890611,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorHtmlComponentUtil': {
  'id': 950992890612,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorHtmlComponent': {
  'id': 950992890613,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorElement': {
  'id': 950992890614,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildRegionPingEu': 4,
   'buildRegionPingUs': 5,
   'short': 6,
   'server': 7,
   'inducer': 8,
   'buildRegionPingLondon': 9,
   'buildRegionPingSaoPaulo': 10,
   'buildRegionPingSydney': 11,
   'buildRegionPingTaiwan': 12,
   'buildRegionPingBelgium': 13,
   'buildRegionPingNetherlands': 14,
   'buildRegionPingParis': 15,
   'buildRegionPingFrankfurt': 16,
   'buildRegionPingWarsaw': 17,
   'buildRegionPingZurich': 18,
   'buildRegionPingMumbai': 19,
   'buildRegionPingDelhi': 20,
   'buildRegionPingSingapore': 21,
   'buildRegionPingJakarta': 22,
   'buildRegionPingHongKong': 23,
   'buildRegionPingTokyo': 24,
   'buildRegionPingOsaka': 25,
   'buildRegionPingSeoul': 26,
   'buildRegionPingDoha': 27,
   'buildRegionPingTelAviv': 29
  }
 },
 'xyz.swapee.wc.IRegionSelectorElementPort': {
  'id': 950992890615,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorDesigner': {
  'id': 950992890616,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IRegionSelectorGPU': {
  'id': 950992890617,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorDisplay': {
  'id': 950992890618,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.RegionSelectorVdusPQs': {
  'id': 950992890619,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IRegionSelectorDisplay': {
  'id': 950992890620,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IRegionSelectorController': {
  'id': 950992890621,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IRegionSelectorController': {
  'id': 950992890622,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorController': {
  'id': 950992890623,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorControllerAR': {
  'id': 950992890624,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IRegionSelectorControllerAT': {
  'id': 950992890625,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IRegionSelectorScreen': {
  'id': 950992890626,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorScreen': {
  'id': 950992890627,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IRegionSelectorScreenAR': {
  'id': 950992890628,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IRegionSelectorScreenAT': {
  'id': 950992890629,
  'symbols': {},
  'methods': {}
 }
})