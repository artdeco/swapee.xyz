import {RegionSelectorInputsPQs} from './RegionSelectorInputsPQs'
export const RegionSelectorInputsQPs=/**@type {!xyz.swapee.wc.RegionSelectorInputsQPs}*/(Object.keys(RegionSelectorInputsPQs)
 .reduce((a,k)=>{a[RegionSelectorInputsPQs[k]]=k;return a},{}))