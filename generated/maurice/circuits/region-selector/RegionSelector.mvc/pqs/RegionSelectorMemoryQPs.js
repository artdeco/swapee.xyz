import {RegionSelectorMemoryPQs} from './RegionSelectorMemoryPQs'
export const RegionSelectorMemoryQPs=/**@type {!xyz.swapee.wc.RegionSelectorMemoryQPs}*/(Object.keys(RegionSelectorMemoryPQs)
 .reduce((a,k)=>{a[RegionSelectorMemoryPQs[k]]=k;return a},{}))