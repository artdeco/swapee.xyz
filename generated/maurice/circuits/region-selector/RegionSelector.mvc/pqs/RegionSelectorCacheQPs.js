import {RegionSelectorCachePQs} from './RegionSelectorCachePQs'
export const RegionSelectorCacheQPs=/**@type {!xyz.swapee.wc.RegionSelectorCacheQPs}*/(Object.keys(RegionSelectorCachePQs)
 .reduce((a,k)=>{a[RegionSelectorCachePQs[k]]=k;return a},{}))