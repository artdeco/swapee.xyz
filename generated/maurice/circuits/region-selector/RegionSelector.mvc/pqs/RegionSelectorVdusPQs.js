export const RegionSelectorVdusPQs=/**@type {!xyz.swapee.wc.RegionSelectorVdusPQs}*/({
 PingToEuLa:'d1ea1',
 PingToUsLa:'d1ea2',
 RegionPingEu:'d1ea3',
 RegionPingUs:'d1ea4',
 EuPingWr:'d1ea5',
 UsPingWr:'d1ea6',
 RegionPingLondon:'d1ea7',
 PingToLndLa:'d1ea9',
 LdnPingWr:'d1ea10',
 SuggestedRegionLa:'d1ea11',
 SuggestedRegionWr:'d1ea12',
 PingToTaiwanLa:'d1ea13',
 PingToSaoPauloLa:'d1ea14',
 PingToSydneyLa:'d1ea15',
 RegionPingSaoPaulo:'d1ea16',
 RegionPingSydney:'d1ea17',
 RegionPingTaiwan:'d1ea18',
 RegionPingBelgium:'d1ea19',
 RegionPingNetherlands:'d1ea20',
 RegionPingParis:'d1ea21',
 RegionPingFrankfurt:'d1ea22',
 RegionPingWarsaw:'d1ea23',
 RegionPingZurich:'d1ea24',
 RegionPingMumbai:'d1ea25',
 RegionPingDelhi:'d1ea26',
 RegionPingSingapore:'d1ea27',
 RegionPingJakarta:'d1ea28',
 RegionPingHongKong:'d1ea29',
 RegionPingTokyo:'d1ea30',
 RegionPingOsaka:'d1ea31',
 RegionPingSeoul:'d1ea32',
 RegionPingDoha:'d1ea33',
 RegionPingTelAviv:'d1ea35',
})