import {RegionSelectorVdusPQs} from './RegionSelectorVdusPQs'
export const RegionSelectorVdusQPs=/**@type {!xyz.swapee.wc.RegionSelectorVdusQPs}*/(Object.keys(RegionSelectorVdusPQs)
 .reduce((a,k)=>{a[RegionSelectorVdusPQs[k]]=k;return a},{}))