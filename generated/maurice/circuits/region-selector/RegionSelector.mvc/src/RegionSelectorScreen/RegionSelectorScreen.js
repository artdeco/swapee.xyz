import deduceInputs from './methods/deduce-inputs'
import AbstractRegionSelectorControllerAT from '../../gen/AbstractRegionSelectorControllerAT'
import RegionSelectorDisplay from '../RegionSelectorDisplay'
import AbstractRegionSelectorScreen from '../../gen/AbstractRegionSelectorScreen'

/** @extends {xyz.swapee.wc.RegionSelectorScreen} */
export default class extends AbstractRegionSelectorScreen.implements(
 AbstractRegionSelectorControllerAT,
 RegionSelectorDisplay,
 /**@type {!xyz.swapee.wc.IRegionSelectorScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IRegionSelectorScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:9509928906,
 }),
){}