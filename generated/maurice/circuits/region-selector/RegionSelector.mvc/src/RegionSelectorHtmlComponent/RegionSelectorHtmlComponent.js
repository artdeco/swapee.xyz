import RegionSelectorHtmlController from '../RegionSelectorHtmlController'
import RegionSelectorHtmlComputer from '../RegionSelectorHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractRegionSelectorHtmlComponent} from '../../gen/AbstractRegionSelectorHtmlComponent'

/** @extends {xyz.swapee.wc.RegionSelectorHtmlComponent} */
export default class extends AbstractRegionSelectorHtmlComponent.implements(
 RegionSelectorHtmlController,
 RegionSelectorHtmlComputer,
 IntegratedComponentInitialiser,
){}