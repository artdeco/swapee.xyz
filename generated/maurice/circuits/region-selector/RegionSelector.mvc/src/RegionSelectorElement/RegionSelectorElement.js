import solder from './methods/solder'
import PostProcess from './methods/post-process'
import server from './methods/server'
import render from './methods/render'
import RegionSelectorServerController from '../RegionSelectorServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractRegionSelectorElement from '../../gen/AbstractRegionSelectorElement'

/** @extends {xyz.swapee.wc.RegionSelectorElement} */
export default class RegionSelectorElement extends AbstractRegionSelectorElement.implements(
 RegionSelectorServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IRegionSelectorElement} */ ({
  solder:solder,
  PostProcess:PostProcess,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
   classesMap: true,
   rootSelector:     `.RegionSelector`,
   stylesheet:       'html/styles/RegionSelector.css',
   blockName:        'html/RegionSelectorBlock.html',
   // todo: select shortest ping
   // buildRegionPingUs({region:region,diff2}){
   //  return (<div $id="RegionSelector">
   //   <span $id="PingToUsLa">{diff2}</span>
   //  </div>)
   // },
   // need short which is activated on component connect
   // can only build one?
   // buildRegionPingLondon({diff2}){
   //  return (<div $id="RegionSelector">
   //   <span $id="PingToEuLa">{diff2}</span>
   //  </div>)
   // },

   // is it possible to automate the number of added regions
   // buildRegionPingEu({diff2}) {
   //  return (<div $id="RegionSelector">
   //   <span $id="PingToLndLa">{diff2}</span>
   //  </div>)
   // },
  }),
){}

// thank you for using web circuits
