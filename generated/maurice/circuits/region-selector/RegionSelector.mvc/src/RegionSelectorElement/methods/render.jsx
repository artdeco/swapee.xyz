/** @type {xyz.swapee.wc.IRegionSelectorElement._render} */
export default function RegionSelectorRender({region:region},{}) {
 return (<div $id="RegionSelector">
  <div $id="SuggestedRegionWr">
   <span $id="SuggestedRegionLa">{region||'-'}</span>
  </div>
  {/* <span $id="PingToEuLa">{euPing}</span> */}
  {/* need a se */}
  {/* <span $id="PingToUsLa">{usPing}</span> */}
 </div>)
}