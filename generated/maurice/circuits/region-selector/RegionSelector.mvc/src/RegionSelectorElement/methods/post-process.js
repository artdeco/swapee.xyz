/** @type {xyz.swapee.wc.IRegionSelectorElement._PostProcess} */
export default function PostProcess(html) {
 const regions=this.model.regions //['iowa','finland']
 const re=/\s+(<!--)( \$if: (.+?) -->[\s\S]+?<!-- \/\$if: (.+?) )(-->)/
 const reg=new RegExp(re.source,'g')
 html=html.replace(reg,(m,cs,c,ce)=>{
  if(!regions.includes(ce)) {
   return ''
  }
  return m
 })
 return html
}