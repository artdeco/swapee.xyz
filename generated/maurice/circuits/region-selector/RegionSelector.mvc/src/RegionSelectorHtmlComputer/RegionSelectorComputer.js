import adaptRegion from './methods/adapt-region'
import {preadaptRegion} from '../../gen/AbstractRegionSelectorComputer/preadapters'
import AbstractRegionSelectorComputer from '../../gen/AbstractRegionSelectorComputer'

/** @extends {xyz.swapee.wc.RegionSelectorComputer} */
export default class RegionSelectorHtmlComputer extends AbstractRegionSelectorComputer.implements(
 /** @type {!xyz.swapee.wc.IRegionSelectorComputer} */ ({
  adaptRegion:adaptRegion,
  adapt:[preadaptRegion],
 }),
){}