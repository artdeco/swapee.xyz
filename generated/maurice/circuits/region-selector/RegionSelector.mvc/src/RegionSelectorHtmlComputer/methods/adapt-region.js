/** @type {xyz.swapee.wc.IRegionSelectorComputer._adaptRegion} */
export default function adaptRegion({
 regionPingSaoPauloDiff2,
 regionPingSydneyDiff2,
 regionPingTaiwanDiff2,
 regionPingEuDiff2,
 regionPingLondonDiff2,
 regionPingUsDiff2,
 regionPingBelgiumDiff2,regionPingFrankfurtDiff2,
 regionPingNetherlandsDiff2,regionPingZurichDiff2,
 regionPingParisDiff2,regionPingWarsawDiff2,

 regionPingDelhiDiff2,regionPingHongKongDiff2,
 regionPingJakartaDiff2,
 regionPingMumbaiDiff2,regionPingOsakaDiff2,
 regionPingSeoulDiff2,regionPingSingaporeDiff2,regionPingTokyoDiff2,
 // regionPingDammamDiff2,
 regionPingDohaDiff2,regionPingTelAvivDiff2,
}){
 // how to do max.min easily
 const lowestPing=Math.min(...[
  regionPingEuDiff2,regionPingLondonDiff2,regionPingUsDiff2,
  regionPingSaoPauloDiff2,regionPingSydneyDiff2,regionPingTaiwanDiff2,
  regionPingBelgiumDiff2,regionPingFrankfurtDiff2,
  regionPingNetherlandsDiff2,regionPingZurichDiff2,
  regionPingParisDiff2,regionPingWarsawDiff2,

  regionPingDelhiDiff2,regionPingHongKongDiff2,
  regionPingJakartaDiff2,
  regionPingMumbaiDiff2,regionPingOsakaDiff2,
  regionPingSeoulDiff2,regionPingSingaporeDiff2,regionPingTokyoDiff2,
  // regionPingDammamDiff2,
  regionPingDohaDiff2,
  regionPingTelAvivDiff2,
 ].filter(Boolean))
 // how to make map of those
 // these is error- prone

 if(lowestPing==regionPingTelAvivDiff2){
  return {
   region:'te',
  }
 }
 if(lowestPing==regionPingDohaDiff2){
  return {
   region:'do',
  }
 }

 // if(lowestPing==regionPingDammamDiff2){
 //  return {
 //   region:'da',
 //  }
 // }

 if(lowestPing==regionPingTokyoDiff2){
  return {
   region:'to',
  }
 }
 if(lowestPing==regionPingSingaporeDiff2){
  return {
   region:'si',
  }
 }
 if(lowestPing==regionPingSeoulDiff2){
  return {
   region:'se',
  }
 }
 if(lowestPing==regionPingOsakaDiff2){
  return {
   region:'os',
  }
 }
 if(lowestPing==regionPingMumbaiDiff2){
  return {
   region:'mu',
  }
 }
 if(lowestPing==regionPingJakartaDiff2){
  return {
   region:'ja',
  }
 }
 if(lowestPing==regionPingHongKongDiff2){
  return {
   region:'ho',
  }
 }
 if(lowestPing==regionPingDelhiDiff2){
  return {
   region:'de',
  }
 }

 if(lowestPing==regionPingEuDiff2){
  return {
   region:'eu', // finland
  }
 }
 if(lowestPing==regionPingLondonDiff2){
  return {
   region:'ldn',
  }
 }
 if(lowestPing==regionPingUsDiff2){
  return {
   region:'us',
  }
 }
 if(lowestPing==regionPingSaoPauloDiff2){
  return {
   region:'sp',
  }
 }
 if(lowestPing==regionPingSydneyDiff2){
  return {
   region:'sy',
  }
 }
 if(lowestPing==regionPingParisDiff2){
  return {
   region:'pa',
  }
 }
 if(lowestPing==regionPingWarsawDiff2){
  return {
   region:'wa',
  }
 }
 if(lowestPing==regionPingZurichDiff2){
  return {
   region:'zu',
  }
 }
 if(lowestPing==regionPingBelgiumDiff2){
  return {
   region:'be',
  }
 }
 if(lowestPing==regionPingFrankfurtDiff2){
  return {
   region:'fr',
  }
 }
 if(lowestPing==regionPingNetherlandsDiff2){
  return {
   region:'ne',
  }
 }
 if(lowestPing==regionPingTaiwanDiff2){
  return {
   region:'ta',
  }
 }
 return {
  region:'',
 }
}