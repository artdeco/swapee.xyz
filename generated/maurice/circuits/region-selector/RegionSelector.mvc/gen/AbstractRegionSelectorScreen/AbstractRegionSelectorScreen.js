import AbstractRegionSelectorScreenAR from '../AbstractRegionSelectorScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {RegionSelectorInputsPQs} from '../../pqs/RegionSelectorInputsPQs'
import {RegionSelectorMemoryQPs} from '../../pqs/RegionSelectorMemoryQPs'
import {RegionSelectorCacheQPs} from '../../pqs/RegionSelectorCacheQPs'
import {RegionSelectorVdusPQs} from '../../pqs/RegionSelectorVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorScreen}
 */
function __AbstractRegionSelectorScreen() {}
__AbstractRegionSelectorScreen.prototype = /** @type {!_AbstractRegionSelectorScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorScreen}
 */
class _AbstractRegionSelectorScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorScreen} ‎
 */
class AbstractRegionSelectorScreen extends newAbstract(
 _AbstractRegionSelectorScreen,950992890626,null,{
  asIRegionSelectorScreen:1,
  superRegionSelectorScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorScreen} */
AbstractRegionSelectorScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorScreen} */
function AbstractRegionSelectorScreenClass(){}

export default AbstractRegionSelectorScreen


AbstractRegionSelectorScreen[$implementations]=[
 __AbstractRegionSelectorScreen,
 AbstractRegionSelectorScreenClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorScreen}*/({
  inputsPQs:RegionSelectorInputsPQs,
  memoryQPs:RegionSelectorMemoryQPs,
  cacheQPs:RegionSelectorCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractRegionSelectorScreenAR,
 AbstractRegionSelectorScreenClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorScreen}*/({
  vdusPQs:RegionSelectorVdusPQs,
 }),
 AbstractRegionSelectorScreenClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorScreen}*/({
  deduceInputs(){
   const{asIRegionSelectorDisplay:{
    SuggestedRegionLa:SuggestedRegionLa,
   }}=this
   return{
    region:SuggestedRegionLa?.innerText,
   }
  },
 }),
]