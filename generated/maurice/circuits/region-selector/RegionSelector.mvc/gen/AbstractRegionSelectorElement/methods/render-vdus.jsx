export default function RegionSelectorRenderVdus(){
 return (<div $id="RegionSelector">
  <vdu $id="SuggestedRegionWr" />
  <vdu $id="SuggestedRegionLa" />
  <vdu $id="PingToUsLa" />
  <vdu $id="PingToEuLa" />
  <vdu $id="PingToLndLa" />
  <vdu $id="PingToTaiwanLa" />
  <vdu $id="PingToSaoPauloLa" />
  <vdu $id="PingToSydneyLa" />
  <vdu $id="LdnPingWr" />
  <vdu $id="EuPingWr" />
  <vdu $id="UsPingWr" />
  <vdu $id="RegionPingEu" />
  <vdu $id="RegionPingUs" />
  <vdu $id="RegionPingLondon" />
  <vdu $id="RegionPingSaoPaulo" />
  <vdu $id="RegionPingSydney" />
  <vdu $id="RegionPingTaiwan" />
  <vdu $id="RegionPingBelgium" />
  <vdu $id="RegionPingNetherlands" />
  <vdu $id="RegionPingParis" />
  <vdu $id="RegionPingFrankfurt" />
  <vdu $id="RegionPingWarsaw" />
  <vdu $id="RegionPingZurich" />
  <vdu $id="RegionPingMumbai" />
  <vdu $id="RegionPingDelhi" />
  <vdu $id="RegionPingSingapore" />
  <vdu $id="RegionPingJakarta" />
  <vdu $id="RegionPingHongKong" />
  <vdu $id="RegionPingTokyo" />
  <vdu $id="RegionPingOsaka" />
  <vdu $id="RegionPingSeoul" />
  <vdu $id="RegionPingDoha" />
  <vdu $id="RegionPingTelAviv" />
 </div>)
}