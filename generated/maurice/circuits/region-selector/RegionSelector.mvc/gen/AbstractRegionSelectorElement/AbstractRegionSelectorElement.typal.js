
import AbstractRegionSelector from '../AbstractRegionSelector'

/** @abstract {xyz.swapee.wc.IRegionSelectorElement} */
export default class AbstractRegionSelectorElement { }



AbstractRegionSelectorElement[$implementations]=[AbstractRegionSelector,
 /** @type {!AbstractRegionSelectorElement} */ ({
  rootId:'RegionSelector',
  __$id:9509928906,
  fqn:'xyz.swapee.wc.IRegionSelector',
  maurice_element_v3:true,
 }),
]