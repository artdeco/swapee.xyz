import RegionSelectorRenderVdus from './methods/render-vdus'
import RegionSelectorElementPort from '../RegionSelectorElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {RegionSelectorInputsPQs} from '../../pqs/RegionSelectorInputsPQs'
import {RegionSelectorCachePQs} from '../../pqs/RegionSelectorCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractRegionSelector from '../AbstractRegionSelector'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorElement}
 */
function __AbstractRegionSelectorElement() {}
__AbstractRegionSelectorElement.prototype = /** @type {!_AbstractRegionSelectorElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorElement}
 */
class _AbstractRegionSelectorElement { }
/**
 * A component description.
 *
 * The _IRegionSelector_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorElement} ‎
 */
class AbstractRegionSelectorElement extends newAbstract(
 _AbstractRegionSelectorElement,950992890614,null,{
  asIRegionSelectorElement:1,
  superRegionSelectorElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorElement} */
AbstractRegionSelectorElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorElement} */
function AbstractRegionSelectorElementClass(){}

export default AbstractRegionSelectorElement


AbstractRegionSelectorElement[$implementations]=[
 __AbstractRegionSelectorElement,
 ElementBase,
 AbstractRegionSelectorElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':region':regionColAttr,
   ':regions':regionsColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'region':regionAttr,
    'regions':regionsAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(regionAttr===undefined?{'region':regionColAttr}:{}),
    ...(regionsAttr===undefined?{'regions':regionsColAttr}:{}),
   }
  },
 }),
 AbstractRegionSelectorElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'region':regionAttr,
   'regions':regionsAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    region:regionAttr,
    regions:regionsAttr,
   }
  },
 }),
 AbstractRegionSelectorElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractRegionSelectorElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
  render:RegionSelectorRenderVdus,
 }),
 AbstractRegionSelectorElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
  inputsPQs:RegionSelectorInputsPQs,
  cachePQs:RegionSelectorCachePQs,
  vdus:{
   'PingToEuLa': 'd1ea1',
   'PingToUsLa': 'd1ea2',
   'RegionPingEu': 'd1ea3',
   'RegionPingUs': 'd1ea4',
   'EuPingWr': 'd1ea5',
   'UsPingWr': 'd1ea6',
   'RegionPingLondon': 'd1ea7',
   'PingToLndLa': 'd1ea9',
   'LdnPingWr': 'd1ea10',
   'SuggestedRegionLa': 'd1ea11',
   'SuggestedRegionWr': 'd1ea12',
   'PingToTaiwanLa': 'd1ea13',
   'PingToSaoPauloLa': 'd1ea14',
   'PingToSydneyLa': 'd1ea15',
   'RegionPingSaoPaulo': 'd1ea16',
   'RegionPingSydney': 'd1ea17',
   'RegionPingTaiwan': 'd1ea18',
   'RegionPingBelgium': 'd1ea19',
   'RegionPingNetherlands': 'd1ea20',
   'RegionPingParis': 'd1ea21',
   'RegionPingFrankfurt': 'd1ea22',
   'RegionPingWarsaw': 'd1ea23',
   'RegionPingZurich': 'd1ea24',
   'RegionPingMumbai': 'd1ea25',
   'RegionPingDelhi': 'd1ea26',
   'RegionPingSingapore': 'd1ea27',
   'RegionPingJakarta': 'd1ea28',
   'RegionPingHongKong': 'd1ea29',
   'RegionPingTokyo': 'd1ea30',
   'RegionPingOsaka': 'd1ea31',
   'RegionPingSeoul': 'd1ea32',
   'RegionPingDoha': 'd1ea33',
   'RegionPingTelAviv': 'd1ea35',
  },
 }),
 IntegratedComponent,
 AbstractRegionSelectorElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','region','regions','no-solder',':no-solder',':region',':regions','fe646','4fa17','9ee87','e4d5e','deda0','3e63e','62750','41de1','7a599','94136','53b3d','82510','44f12','aa610','a828d','36e2a','531b9','54af8','a74b4','2e658','aaba6','909ad','2ce50','7c56f','db0fc','53914','c720d','aadf1','be8b4','bb0cf','ccdc7','ceeee','a733e','78a5e','960db','030c5','children']),
   })
  },
  get Port(){
   return RegionSelectorElementPort
  },
 }),
 Landed,
 AbstractRegionSelectorElementClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElement}*/({
  constructor(){
   this.land={
    RegionPingEu:null,
    RegionPingUs:null,
    RegionPingLondon:null,
    RegionPingSaoPaulo:null,
    RegionPingSydney:null,
    RegionPingTaiwan:null,
    RegionPingBelgium:null,
    RegionPingNetherlands:null,
    RegionPingParis:null,
    RegionPingFrankfurt:null,
    RegionPingWarsaw:null,
    RegionPingZurich:null,
    RegionPingMumbai:null,
    RegionPingDelhi:null,
    RegionPingSingapore:null,
    RegionPingJakarta:null,
    RegionPingHongKong:null,
    RegionPingTokyo:null,
    RegionPingOsaka:null,
    RegionPingSeoul:null,
    RegionPingDoha:null,
    RegionPingTelAviv:null,
   }
  },
 }),
]



AbstractRegionSelectorElement[$implementations]=[AbstractRegionSelector,
 /** @type {!AbstractRegionSelectorElement} */ ({
  rootId:'RegionSelector',
  __$id:9509928906,
  fqn:'xyz.swapee.wc.IRegionSelector',
  maurice_element_v3:true,
 }),
]