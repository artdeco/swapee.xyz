import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorControllerAR}
 */
function __AbstractRegionSelectorControllerAR() {}
__AbstractRegionSelectorControllerAR.prototype = /** @type {!_AbstractRegionSelectorControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorControllerAR}
 */
class _AbstractRegionSelectorControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IRegionSelectorControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorControllerAR} ‎
 */
class AbstractRegionSelectorControllerAR extends newAbstract(
 _AbstractRegionSelectorControllerAR,950992890624,null,{
  asIRegionSelectorControllerAR:1,
  superRegionSelectorControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorControllerAR} */
AbstractRegionSelectorControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorControllerAR} */
function AbstractRegionSelectorControllerARClass(){}

export default AbstractRegionSelectorControllerAR


AbstractRegionSelectorControllerAR[$implementations]=[
 __AbstractRegionSelectorControllerAR,
 AR,
 AbstractRegionSelectorControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IRegionSelectorControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]