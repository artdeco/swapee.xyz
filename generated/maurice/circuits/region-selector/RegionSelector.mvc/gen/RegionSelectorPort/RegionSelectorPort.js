import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {RegionSelectorInputsPQs} from '../../pqs/RegionSelectorInputsPQs'
import {RegionSelectorOuterCoreConstructor} from '../RegionSelectorCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionSelectorPort}
 */
function __RegionSelectorPort() {}
__RegionSelectorPort.prototype = /** @type {!_RegionSelectorPort} */ ({ })
/** @this {xyz.swapee.wc.RegionSelectorPort} */ function RegionSelectorPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.RegionSelectorOuterCore} */ ({model:null})
  RegionSelectorOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorPort}
 */
class _RegionSelectorPort { }
/**
 * The port that serves as an interface to the _IRegionSelector_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorPort} ‎
 */
export class RegionSelectorPort extends newAbstract(
 _RegionSelectorPort,95099289065,RegionSelectorPortConstructor,{
  asIRegionSelectorPort:1,
  superRegionSelectorPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorPort} */
RegionSelectorPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorPort} */
function RegionSelectorPortClass(){}

export const RegionSelectorPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IRegionSelector.Pinout>}*/({
 get Port() { return RegionSelectorPort },
})

RegionSelectorPort[$implementations]=[
 RegionSelectorPortClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorPort}*/({
  resetPort(){
   this.resetRegionSelectorPort()
  },
  resetRegionSelectorPort(){
   RegionSelectorPortConstructor.call(this)
  },
 }),
 __RegionSelectorPort,
 Parametric,
 RegionSelectorPortClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorPort}*/({
  constructor(){
   mountPins(this.inputs,'',RegionSelectorInputsPQs)
  },
 }),
]


export default RegionSelectorPort