import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionSelectorElementPort}
 */
function __RegionSelectorElementPort() {}
__RegionSelectorElementPort.prototype = /** @type {!_RegionSelectorElementPort} */ ({ })
/** @this {xyz.swapee.wc.RegionSelectorElementPort} */ function RegionSelectorElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IRegionSelectorElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    pingToUsLaOpts: {},
    pingToEuLaOpts: {},
    pingToLndLaOpts: {},
    pingToTaiwanLaOpts: {},
    pingToSaoPauloLaOpts: {},
    pingToSydneyLaOpts: {},
    suggestedRegionLaOpts: {},
    suggestedRegionWrOpts: {},
    ldnPingWrOpts: {},
    euPingWrOpts: {},
    usPingWrOpts: {},
    regionPingEuOpts: {},
    regionPingUsOpts: {},
    regionPingLondonOpts: {},
    regionPingSaoPauloOpts: {},
    regionPingSydneyOpts: {},
    regionPingTaiwanOpts: {},
    regionPingBelgiumOpts: {},
    regionPingNetherlandsOpts: {},
    regionPingParisOpts: {},
    regionPingFrankfurtOpts: {},
    regionPingWarsawOpts: {},
    regionPingZurichOpts: {},
    regionPingMumbaiOpts: {},
    regionPingDelhiOpts: {},
    regionPingSingaporeOpts: {},
    regionPingJakartaOpts: {},
    regionPingHongKongOpts: {},
    regionPingTokyoOpts: {},
    regionPingOsakaOpts: {},
    regionPingSeoulOpts: {},
    regionPingDohaOpts: {},
    regionPingTelAvivOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorElementPort}
 */
class _RegionSelectorElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorElementPort} ‎
 */
class RegionSelectorElementPort extends newAbstract(
 _RegionSelectorElementPort,950992890615,RegionSelectorElementPortConstructor,{
  asIRegionSelectorElementPort:1,
  superRegionSelectorElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorElementPort} */
RegionSelectorElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorElementPort} */
function RegionSelectorElementPortClass(){}

export default RegionSelectorElementPort


RegionSelectorElementPort[$implementations]=[
 __RegionSelectorElementPort,
 RegionSelectorElementPortClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'ping-to-us-la-opts':undefined,
    'ping-to-eu-la-opts':undefined,
    'ping-to-lnd-la-opts':undefined,
    'ping-to-taiwan-la-opts':undefined,
    'ping-to-sao-paulo-la-opts':undefined,
    'ping-to-sydney-la-opts':undefined,
    'suggested-region-la-opts':undefined,
    'suggested-region-wr-opts':undefined,
    'ldn-ping-wr-opts':undefined,
    'eu-ping-wr-opts':undefined,
    'us-ping-wr-opts':undefined,
    'region-ping-eu-opts':undefined,
    'region-ping-us-opts':undefined,
    'region-ping-london-opts':undefined,
    'region-ping-sao-paulo-opts':undefined,
    'region-ping-sydney-opts':undefined,
    'region-ping-taiwan-opts':undefined,
    'region-ping-belgium-opts':undefined,
    'region-ping-netherlands-opts':undefined,
    'region-ping-paris-opts':undefined,
    'region-ping-frankfurt-opts':undefined,
    'region-ping-warsaw-opts':undefined,
    'region-ping-zurich-opts':undefined,
    'region-ping-mumbai-opts':undefined,
    'region-ping-delhi-opts':undefined,
    'region-ping-singapore-opts':undefined,
    'region-ping-jakarta-opts':undefined,
    'region-ping-hong-kong-opts':undefined,
    'region-ping-tokyo-opts':undefined,
    'region-ping-osaka-opts':undefined,
    'region-ping-seoul-opts':undefined,
    'region-ping-doha-opts':undefined,
    'region-ping-tel-aviv-opts':undefined,
   })
  },
 }),
]