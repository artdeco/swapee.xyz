import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorComputer}
 */
function __AbstractRegionSelectorComputer() {}
__AbstractRegionSelectorComputer.prototype = /** @type {!_AbstractRegionSelectorComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorComputer}
 */
class _AbstractRegionSelectorComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorComputer} ‎
 */
export class AbstractRegionSelectorComputer extends newAbstract(
 _AbstractRegionSelectorComputer,95099289061,null,{
  asIRegionSelectorComputer:1,
  superRegionSelectorComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorComputer} */
AbstractRegionSelectorComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorComputer} */
function AbstractRegionSelectorComputerClass(){}


AbstractRegionSelectorComputer[$implementations]=[
 __AbstractRegionSelectorComputer,
 Adapter,
]


export default AbstractRegionSelectorComputer