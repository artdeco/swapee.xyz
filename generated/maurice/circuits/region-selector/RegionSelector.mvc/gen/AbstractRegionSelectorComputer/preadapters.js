
/**@this {xyz.swapee.wc.IRegionSelectorComputer}*/
export function preadaptRegion(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IRegionSelectorComputer.adaptRegion.Form}*/
 const _inputs={
  regionPingEuDiff2:this.land.RegionPingEu?this.land.RegionPingEu.model['e559c']:void 0,
  regionPingUsDiff2:this.land.RegionPingUs?this.land.RegionPingUs.model['e559c']:void 0,
  regionPingLondonDiff2:this.land.RegionPingLondon?this.land.RegionPingLondon.model['e559c']:void 0,
  regionPingSaoPauloDiff2:this.land.RegionPingSaoPaulo?this.land.RegionPingSaoPaulo.model['e559c']:void 0,
  regionPingSydneyDiff2:this.land.RegionPingSydney?this.land.RegionPingSydney.model['e559c']:void 0,
  regionPingTaiwanDiff2:this.land.RegionPingTaiwan?this.land.RegionPingTaiwan.model['e559c']:void 0,
  regionPingBelgiumDiff2:this.land.RegionPingBelgium?this.land.RegionPingBelgium.model['e559c']:void 0,
  regionPingNetherlandsDiff2:this.land.RegionPingNetherlands?this.land.RegionPingNetherlands.model['e559c']:void 0,
  regionPingParisDiff2:this.land.RegionPingParis?this.land.RegionPingParis.model['e559c']:void 0,
  regionPingFrankfurtDiff2:this.land.RegionPingFrankfurt?this.land.RegionPingFrankfurt.model['e559c']:void 0,
  regionPingWarsawDiff2:this.land.RegionPingWarsaw?this.land.RegionPingWarsaw.model['e559c']:void 0,
  regionPingZurichDiff2:this.land.RegionPingZurich?this.land.RegionPingZurich.model['e559c']:void 0,
  regionPingMumbaiDiff2:this.land.RegionPingMumbai?this.land.RegionPingMumbai.model['e559c']:void 0,
  regionPingDelhiDiff2:this.land.RegionPingDelhi?this.land.RegionPingDelhi.model['e559c']:void 0,
  regionPingSingaporeDiff2:this.land.RegionPingSingapore?this.land.RegionPingSingapore.model['e559c']:void 0,
  regionPingJakartaDiff2:this.land.RegionPingJakarta?this.land.RegionPingJakarta.model['e559c']:void 0,
  regionPingHongKongDiff2:this.land.RegionPingHongKong?this.land.RegionPingHongKong.model['e559c']:void 0,
  regionPingTokyoDiff2:this.land.RegionPingTokyo?this.land.RegionPingTokyo.model['e559c']:void 0,
  regionPingOsakaDiff2:this.land.RegionPingOsaka?this.land.RegionPingOsaka.model['e559c']:void 0,
  regionPingSeoulDiff2:this.land.RegionPingSeoul?this.land.RegionPingSeoul.model['e559c']:void 0,
  regionPingDohaDiff2:this.land.RegionPingDoha?this.land.RegionPingDoha.model['e559c']:void 0,
  regionPingTelAvivDiff2:this.land.RegionPingTelAviv?this.land.RegionPingTelAviv.model['e559c']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptRegion(__inputs,__changes)
 return RET
}