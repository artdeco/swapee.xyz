import AbstractRegionSelectorGPU from '../AbstractRegionSelectorGPU'
import AbstractRegionSelectorScreenBack from '../AbstractRegionSelectorScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {access} from '@mauriceguest/guest2'
import {RegionSelectorInputsQPs} from '../../pqs/RegionSelectorInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractRegionSelector from '../AbstractRegionSelector'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorHtmlComponent}
 */
function __AbstractRegionSelectorHtmlComponent() {}
__AbstractRegionSelectorHtmlComponent.prototype = /** @type {!_AbstractRegionSelectorHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorHtmlComponent}
 */
class _AbstractRegionSelectorHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.RegionSelectorHtmlComponent} */ (res)
  }
}
/**
 * The _IRegionSelector_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorHtmlComponent} ‎
 */
export class AbstractRegionSelectorHtmlComponent extends newAbstract(
 _AbstractRegionSelectorHtmlComponent,950992890613,null,{
  asIRegionSelectorHtmlComponent:1,
  superRegionSelectorHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorHtmlComponent} */
AbstractRegionSelectorHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorHtmlComponent} */
function AbstractRegionSelectorHtmlComponentClass(){}


AbstractRegionSelectorHtmlComponent[$implementations]=[
 __AbstractRegionSelectorHtmlComponent,
 HtmlComponent,
 AbstractRegionSelector,
 AbstractRegionSelectorGPU,
 AbstractRegionSelectorScreenBack,
 Landed,
 AbstractRegionSelectorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorHtmlComponent}*/({
  constructor(){
   this.land={
    RegionPingEu:null,
    RegionPingUs:null,
    RegionPingLondon:null,
    RegionPingSaoPaulo:null,
    RegionPingSydney:null,
    RegionPingTaiwan:null,
    RegionPingBelgium:null,
    RegionPingNetherlands:null,
    RegionPingParis:null,
    RegionPingFrankfurt:null,
    RegionPingWarsaw:null,
    RegionPingZurich:null,
    RegionPingMumbai:null,
    RegionPingDelhi:null,
    RegionPingSingapore:null,
    RegionPingJakarta:null,
    RegionPingHongKong:null,
    RegionPingTokyo:null,
    RegionPingOsaka:null,
    RegionPingSeoul:null,
    RegionPingDoha:null,
    RegionPingTelAviv:null,
   }
  },
 }),
 AbstractRegionSelectorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorHtmlComponent}*/({
  inputsQPs:RegionSelectorInputsQPs,
 }),

/** @type {!AbstractRegionSelectorHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IRegionSelectorHtmlComponent}*/function paintSuggestedRegionLa() {
     this.SuggestedRegionLa.setText(this.model.region||'-')
   }
 ] }),
 AbstractRegionSelectorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIRegionSelectorGPU:{
     RegionPingEu:RegionPingEu,
     RegionPingUs:RegionPingUs,
     RegionPingLondon:RegionPingLondon,
     RegionPingSaoPaulo:RegionPingSaoPaulo,
     RegionPingSydney:RegionPingSydney,
     RegionPingTaiwan:RegionPingTaiwan,
     RegionPingBelgium:RegionPingBelgium,
     RegionPingNetherlands:RegionPingNetherlands,
     RegionPingParis:RegionPingParis,
     RegionPingFrankfurt:RegionPingFrankfurt,
     RegionPingWarsaw:RegionPingWarsaw,
     RegionPingZurich:RegionPingZurich,
     RegionPingMumbai:RegionPingMumbai,
     RegionPingDelhi:RegionPingDelhi,
     RegionPingSingapore:RegionPingSingapore,
     RegionPingJakarta:RegionPingJakarta,
     RegionPingHongKong:RegionPingHongKong,
     RegionPingTokyo:RegionPingTokyo,
     RegionPingOsaka:RegionPingOsaka,
     RegionPingSeoul:RegionPingSeoul,
     RegionPingDoha:RegionPingDoha,
     RegionPingTelAviv:RegionPingTelAviv,
    },
   }=this
   complete(1631224845,{RegionPingEu:RegionPingEu})
   complete(1631224845,{RegionPingUs:RegionPingUs})
   complete(1631224845,{RegionPingLondon:RegionPingLondon})
   complete(1631224845,{RegionPingSaoPaulo:RegionPingSaoPaulo})
   complete(1631224845,{RegionPingSydney:RegionPingSydney})
   complete(1631224845,{RegionPingTaiwan:RegionPingTaiwan})
   complete(1631224845,{RegionPingBelgium:RegionPingBelgium})
   complete(1631224845,{RegionPingNetherlands:RegionPingNetherlands})
   complete(1631224845,{RegionPingParis:RegionPingParis})
   complete(1631224845,{RegionPingFrankfurt:RegionPingFrankfurt})
   complete(1631224845,{RegionPingWarsaw:RegionPingWarsaw})
   complete(1631224845,{RegionPingZurich:RegionPingZurich})
   complete(1631224845,{RegionPingMumbai:RegionPingMumbai})
   complete(1631224845,{RegionPingDelhi:RegionPingDelhi})
   complete(1631224845,{RegionPingSingapore:RegionPingSingapore})
   complete(1631224845,{RegionPingJakarta:RegionPingJakarta})
   complete(1631224845,{RegionPingHongKong:RegionPingHongKong})
   complete(1631224845,{RegionPingTokyo:RegionPingTokyo})
   complete(1631224845,{RegionPingOsaka:RegionPingOsaka})
   complete(1631224845,{RegionPingSeoul:RegionPingSeoul})
   complete(1631224845,{RegionPingDoha:RegionPingDoha})
   complete(1631224845,{RegionPingTelAviv:RegionPingTelAviv})
  },
 }),
]