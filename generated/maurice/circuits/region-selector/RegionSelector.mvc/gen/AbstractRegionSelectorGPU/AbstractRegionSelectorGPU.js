import AbstractRegionSelectorDisplay from '../AbstractRegionSelectorDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {RegionSelectorVdusPQs} from '../../pqs/RegionSelectorVdusPQs'
import {RegionSelectorVdusQPs} from '../../pqs/RegionSelectorVdusQPs'
import {RegionSelectorMemoryPQs} from '../../pqs/RegionSelectorMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorGPU}
 */
function __AbstractRegionSelectorGPU() {}
__AbstractRegionSelectorGPU.prototype = /** @type {!_AbstractRegionSelectorGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorGPU}
 */
class _AbstractRegionSelectorGPU { }
/**
 * Handles the periphery of the _IRegionSelectorDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorGPU} ‎
 */
class AbstractRegionSelectorGPU extends newAbstract(
 _AbstractRegionSelectorGPU,950992890617,null,{
  asIRegionSelectorGPU:1,
  superRegionSelectorGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorGPU} */
AbstractRegionSelectorGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorGPU} */
function AbstractRegionSelectorGPUClass(){}

export default AbstractRegionSelectorGPU


AbstractRegionSelectorGPU[$implementations]=[
 __AbstractRegionSelectorGPU,
 AbstractRegionSelectorGPUClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorGPU}*/({
  vdusPQs:RegionSelectorVdusPQs,
  vdusQPs:RegionSelectorVdusQPs,
  memoryPQs:RegionSelectorMemoryPQs,
 }),
 AbstractRegionSelectorDisplay,
 BrowserView,
]