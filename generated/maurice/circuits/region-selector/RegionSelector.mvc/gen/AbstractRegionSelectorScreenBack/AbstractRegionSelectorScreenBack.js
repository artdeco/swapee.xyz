import AbstractRegionSelectorScreenAT from '../AbstractRegionSelectorScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorScreenBack}
 */
function __AbstractRegionSelectorScreenBack() {}
__AbstractRegionSelectorScreenBack.prototype = /** @type {!_AbstractRegionSelectorScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorScreen}
 */
class _AbstractRegionSelectorScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorScreen} ‎
 */
class AbstractRegionSelectorScreenBack extends newAbstract(
 _AbstractRegionSelectorScreenBack,950992890627,null,{
  asIRegionSelectorScreen:1,
  superRegionSelectorScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorScreen} */
AbstractRegionSelectorScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorScreen} */
function AbstractRegionSelectorScreenBackClass(){}

export default AbstractRegionSelectorScreenBack


AbstractRegionSelectorScreenBack[$implementations]=[
 __AbstractRegionSelectorScreenBack,
 AbstractRegionSelectorScreenAT,
]