import {mountPins} from '@webcircuits/webcircuits'
import {RegionSelectorMemoryPQs} from '../../pqs/RegionSelectorMemoryPQs'
import {RegionSelectorCachePQs} from '../../pqs/RegionSelectorCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionSelectorCore}
 */
function __RegionSelectorCore() {}
__RegionSelectorCore.prototype = /** @type {!_RegionSelectorCore} */ ({ })
/** @this {xyz.swapee.wc.RegionSelectorCore} */ function RegionSelectorCoreConstructor() {
  /**@type {!xyz.swapee.wc.IRegionSelectorCore.Model}*/
  this.model={
    usPing: null,
    euPing: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorCore}
 */
class _RegionSelectorCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorCore} ‎
 */
class RegionSelectorCore extends newAbstract(
 _RegionSelectorCore,95099289068,RegionSelectorCoreConstructor,{
  asIRegionSelectorCore:1,
  superRegionSelectorCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorCore} */
RegionSelectorCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorCore} */
function RegionSelectorCoreClass(){}

export default RegionSelectorCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_RegionSelectorOuterCore}
 */
function __RegionSelectorOuterCore() {}
__RegionSelectorOuterCore.prototype = /** @type {!_RegionSelectorOuterCore} */ ({ })
/** @this {xyz.swapee.wc.RegionSelectorOuterCore} */
export function RegionSelectorOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IRegionSelectorOuterCore.Model}*/
  this.model={
    region: null,
    regions: [],
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorOuterCore}
 */
class _RegionSelectorOuterCore { }
/**
 * The _IRegionSelector_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorOuterCore} ‎
 */
export class RegionSelectorOuterCore extends newAbstract(
 _RegionSelectorOuterCore,95099289063,RegionSelectorOuterCoreConstructor,{
  asIRegionSelectorOuterCore:1,
  superRegionSelectorOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorOuterCore} */
RegionSelectorOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorOuterCore} */
function RegionSelectorOuterCoreClass(){}


RegionSelectorOuterCore[$implementations]=[
 __RegionSelectorOuterCore,
 RegionSelectorOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorOuterCore}*/({
  constructor(){
   mountPins(this.model,'',RegionSelectorMemoryPQs)
   mountPins(this.model,'',RegionSelectorCachePQs)
  },
 }),
]

RegionSelectorCore[$implementations]=[
 RegionSelectorCoreClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorCore}*/({
  resetCore(){
   this.resetRegionSelectorCore()
  },
  resetRegionSelectorCore(){
   RegionSelectorCoreConstructor.call(
    /**@type {xyz.swapee.wc.RegionSelectorCore}*/(this),
   )
   RegionSelectorOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.RegionSelectorOuterCore}*/(
     /**@type {!xyz.swapee.wc.IRegionSelectorOuterCore}*/(this)),
   )
  },
 }),
 __RegionSelectorCore,
 RegionSelectorOuterCore,
]

export {RegionSelectorCore}