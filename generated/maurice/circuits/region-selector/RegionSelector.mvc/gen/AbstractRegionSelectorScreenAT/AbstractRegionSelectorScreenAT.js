import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorScreenAT}
 */
function __AbstractRegionSelectorScreenAT() {}
__AbstractRegionSelectorScreenAT.prototype = /** @type {!_AbstractRegionSelectorScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorScreenAT}
 */
class _AbstractRegionSelectorScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IRegionSelectorScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorScreenAT} ‎
 */
class AbstractRegionSelectorScreenAT extends newAbstract(
 _AbstractRegionSelectorScreenAT,950992890629,null,{
  asIRegionSelectorScreenAT:1,
  superRegionSelectorScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorScreenAT} */
AbstractRegionSelectorScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorScreenAT} */
function AbstractRegionSelectorScreenATClass(){}

export default AbstractRegionSelectorScreenAT


AbstractRegionSelectorScreenAT[$implementations]=[
 __AbstractRegionSelectorScreenAT,
 UartUniversal,
]