import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateRegionSelectorCache=makeBuffers({
 usPing:Number,
 euPing:Number,
},{silent:true})