import AbstractRegionSelectorProcessor from '../AbstractRegionSelectorProcessor'
import {RegionSelectorCore} from '../RegionSelectorCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractRegionSelectorComputer} from '../AbstractRegionSelectorComputer'
import {AbstractRegionSelectorController} from '../AbstractRegionSelectorController'
import {regulateRegionSelectorCache} from './methods/regulateRegionSelectorCache'
import {RegionSelectorCacheQPs} from '../../pqs/RegionSelectorCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelector}
 */
function __AbstractRegionSelector() {}
__AbstractRegionSelector.prototype = /** @type {!_AbstractRegionSelector} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelector}
 */
class _AbstractRegionSelector { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractRegionSelector} ‎
 */
class AbstractRegionSelector extends newAbstract(
 _AbstractRegionSelector,950992890610,null,{
  asIRegionSelector:1,
  superRegionSelector:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelector} */
AbstractRegionSelector.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelector} */
function AbstractRegionSelectorClass(){}

export default AbstractRegionSelector


AbstractRegionSelector[$implementations]=[
 __AbstractRegionSelector,
 RegionSelectorCore,
 AbstractRegionSelectorProcessor,
 IntegratedComponent,
 AbstractRegionSelectorComputer,
 AbstractRegionSelectorController,
 AbstractRegionSelectorClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelector}*/({
  regulateState:regulateRegionSelectorCache,
  stateQPs:RegionSelectorCacheQPs,
 }),
]


export {AbstractRegionSelector}