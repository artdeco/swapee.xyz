import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorControllerAT}
 */
function __AbstractRegionSelectorControllerAT() {}
__AbstractRegionSelectorControllerAT.prototype = /** @type {!_AbstractRegionSelectorControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractRegionSelectorControllerAT}
 */
class _AbstractRegionSelectorControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IRegionSelectorControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractRegionSelectorControllerAT} ‎
 */
class AbstractRegionSelectorControllerAT extends newAbstract(
 _AbstractRegionSelectorControllerAT,950992890625,null,{
  asIRegionSelectorControllerAT:1,
  superRegionSelectorControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractRegionSelectorControllerAT} */
AbstractRegionSelectorControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractRegionSelectorControllerAT} */
function AbstractRegionSelectorControllerATClass(){}

export default AbstractRegionSelectorControllerAT


AbstractRegionSelectorControllerAT[$implementations]=[
 __AbstractRegionSelectorControllerAT,
 UartUniversal,
 AbstractRegionSelectorControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IRegionSelectorControllerAT}*/({
  get asIRegionSelectorController(){
   return this
  },
 }),
]