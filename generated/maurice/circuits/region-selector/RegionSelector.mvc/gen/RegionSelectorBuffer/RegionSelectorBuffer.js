import {makeBuffers} from '@webcircuits/webcircuits'

export const RegionSelectorBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  region:String,
  regions:[String],
 }),
})

export default RegionSelectorBuffer