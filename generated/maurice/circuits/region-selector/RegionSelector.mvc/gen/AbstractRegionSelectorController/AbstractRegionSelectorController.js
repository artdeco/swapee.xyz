import RegionSelectorBuffer from '../RegionSelectorBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {RegionSelectorPortConnector} from '../RegionSelectorPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorController}
 */
function __AbstractRegionSelectorController() {}
__AbstractRegionSelectorController.prototype = /** @type {!_AbstractRegionSelectorController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorController}
 */
class _AbstractRegionSelectorController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorController} ‎
 */
export class AbstractRegionSelectorController extends newAbstract(
 _AbstractRegionSelectorController,950992890621,null,{
  asIRegionSelectorController:1,
  superRegionSelectorController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorController} */
AbstractRegionSelectorController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorController} */
function AbstractRegionSelectorControllerClass(){}


AbstractRegionSelectorController[$implementations]=[
 AbstractRegionSelectorControllerClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IRegionSelectorPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractRegionSelectorController,
 RegionSelectorBuffer,
 IntegratedController,
 /**@type {!AbstractRegionSelectorController}*/(RegionSelectorPortConnector),
]


export default AbstractRegionSelectorController