import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorDisplay}
 */
function __AbstractRegionSelectorDisplay() {}
__AbstractRegionSelectorDisplay.prototype = /** @type {!_AbstractRegionSelectorDisplay} */ ({ })
/** @this {xyz.swapee.wc.RegionSelectorDisplay} */ function RegionSelectorDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.PingToUsLa=null
  /** @type {HTMLDivElement} */ this.PingToEuLa=null
  /** @type {HTMLDivElement} */ this.PingToLndLa=null
  /** @type {HTMLDivElement} */ this.PingToTaiwanLa=null
  /** @type {HTMLDivElement} */ this.PingToSaoPauloLa=null
  /** @type {HTMLDivElement} */ this.PingToSydneyLa=null
  /** @type {HTMLSpanElement} */ this.SuggestedRegionLa=null
  /** @type {HTMLDivElement} */ this.SuggestedRegionWr=null
  /** @type {HTMLDivElement} */ this.LdnPingWr=null
  /** @type {HTMLDivElement} */ this.EuPingWr=null
  /** @type {HTMLDivElement} */ this.UsPingWr=null
  /** @type {HTMLElement} */ this.RegionPingEu=null
  /** @type {HTMLElement} */ this.RegionPingUs=null
  /** @type {HTMLElement} */ this.RegionPingLondon=null
  /** @type {HTMLElement} */ this.RegionPingSaoPaulo=null
  /** @type {HTMLElement} */ this.RegionPingSydney=null
  /** @type {HTMLElement} */ this.RegionPingTaiwan=null
  /** @type {HTMLElement} */ this.RegionPingBelgium=null
  /** @type {HTMLElement} */ this.RegionPingNetherlands=null
  /** @type {HTMLElement} */ this.RegionPingParis=null
  /** @type {HTMLElement} */ this.RegionPingFrankfurt=null
  /** @type {HTMLElement} */ this.RegionPingWarsaw=null
  /** @type {HTMLElement} */ this.RegionPingZurich=null
  /** @type {HTMLElement} */ this.RegionPingMumbai=null
  /** @type {HTMLElement} */ this.RegionPingDelhi=null
  /** @type {HTMLElement} */ this.RegionPingSingapore=null
  /** @type {HTMLElement} */ this.RegionPingJakarta=null
  /** @type {HTMLElement} */ this.RegionPingHongKong=null
  /** @type {HTMLElement} */ this.RegionPingTokyo=null
  /** @type {HTMLElement} */ this.RegionPingOsaka=null
  /** @type {HTMLElement} */ this.RegionPingSeoul=null
  /** @type {HTMLElement} */ this.RegionPingDoha=null
  /** @type {HTMLElement} */ this.RegionPingTelAviv=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorDisplay}
 */
class _AbstractRegionSelectorDisplay { }
/**
 * Display for presenting information from the _IRegionSelector_.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorDisplay} ‎
 */
class AbstractRegionSelectorDisplay extends newAbstract(
 _AbstractRegionSelectorDisplay,950992890618,RegionSelectorDisplayConstructor,{
  asIRegionSelectorDisplay:1,
  superRegionSelectorDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorDisplay} */
AbstractRegionSelectorDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorDisplay} */
function AbstractRegionSelectorDisplayClass(){}

export default AbstractRegionSelectorDisplay


AbstractRegionSelectorDisplay[$implementations]=[
 __AbstractRegionSelectorDisplay,
 Display,
 AbstractRegionSelectorDisplayClass.prototype=/**@type {!xyz.swapee.wc.IRegionSelectorDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIRegionSelectorScreen:{vdusPQs:{
    SuggestedRegionWr:SuggestedRegionWr,
    SuggestedRegionLa:SuggestedRegionLa,
    PingToUsLa:PingToUsLa,
    PingToEuLa:PingToEuLa,
    PingToLndLa:PingToLndLa,
    PingToTaiwanLa:PingToTaiwanLa,
    PingToSaoPauloLa:PingToSaoPauloLa,
    PingToSydneyLa:PingToSydneyLa,
    LdnPingWr:LdnPingWr,
    EuPingWr:EuPingWr,
    UsPingWr:UsPingWr,
    RegionPingEu:RegionPingEu,
    RegionPingUs:RegionPingUs,
    RegionPingLondon:RegionPingLondon,
    RegionPingSaoPaulo:RegionPingSaoPaulo,
    RegionPingSydney:RegionPingSydney,
    RegionPingTaiwan:RegionPingTaiwan,
    RegionPingBelgium:RegionPingBelgium,
    RegionPingNetherlands:RegionPingNetherlands,
    RegionPingParis:RegionPingParis,
    RegionPingFrankfurt:RegionPingFrankfurt,
    RegionPingWarsaw:RegionPingWarsaw,
    RegionPingZurich:RegionPingZurich,
    RegionPingMumbai:RegionPingMumbai,
    RegionPingDelhi:RegionPingDelhi,
    RegionPingSingapore:RegionPingSingapore,
    RegionPingJakarta:RegionPingJakarta,
    RegionPingHongKong:RegionPingHongKong,
    RegionPingTokyo:RegionPingTokyo,
    RegionPingOsaka:RegionPingOsaka,
    RegionPingSeoul:RegionPingSeoul,
    RegionPingDoha:RegionPingDoha,
    RegionPingTelAviv:RegionPingTelAviv,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    SuggestedRegionWr:/**@type {HTMLDivElement}*/(children[SuggestedRegionWr]),
    SuggestedRegionLa:/**@type {HTMLSpanElement}*/(children[SuggestedRegionLa]),
    PingToUsLa:/**@type {HTMLDivElement}*/(children[PingToUsLa]),
    PingToEuLa:/**@type {HTMLDivElement}*/(children[PingToEuLa]),
    PingToLndLa:/**@type {HTMLDivElement}*/(children[PingToLndLa]),
    PingToTaiwanLa:/**@type {HTMLDivElement}*/(children[PingToTaiwanLa]),
    PingToSaoPauloLa:/**@type {HTMLDivElement}*/(children[PingToSaoPauloLa]),
    PingToSydneyLa:/**@type {HTMLDivElement}*/(children[PingToSydneyLa]),
    LdnPingWr:/**@type {HTMLDivElement}*/(children[LdnPingWr]),
    EuPingWr:/**@type {HTMLDivElement}*/(children[EuPingWr]),
    UsPingWr:/**@type {HTMLDivElement}*/(children[UsPingWr]),
    RegionPingEu:/**@type {HTMLElement}*/(children[RegionPingEu]),
    RegionPingUs:/**@type {HTMLElement}*/(children[RegionPingUs]),
    RegionPingLondon:/**@type {HTMLElement}*/(children[RegionPingLondon]),
    RegionPingSaoPaulo:/**@type {HTMLElement}*/(children[RegionPingSaoPaulo]),
    RegionPingSydney:/**@type {HTMLElement}*/(children[RegionPingSydney]),
    RegionPingTaiwan:/**@type {HTMLElement}*/(children[RegionPingTaiwan]),
    RegionPingBelgium:/**@type {HTMLElement}*/(children[RegionPingBelgium]),
    RegionPingNetherlands:/**@type {HTMLElement}*/(children[RegionPingNetherlands]),
    RegionPingParis:/**@type {HTMLElement}*/(children[RegionPingParis]),
    RegionPingFrankfurt:/**@type {HTMLElement}*/(children[RegionPingFrankfurt]),
    RegionPingWarsaw:/**@type {HTMLElement}*/(children[RegionPingWarsaw]),
    RegionPingZurich:/**@type {HTMLElement}*/(children[RegionPingZurich]),
    RegionPingMumbai:/**@type {HTMLElement}*/(children[RegionPingMumbai]),
    RegionPingDelhi:/**@type {HTMLElement}*/(children[RegionPingDelhi]),
    RegionPingSingapore:/**@type {HTMLElement}*/(children[RegionPingSingapore]),
    RegionPingJakarta:/**@type {HTMLElement}*/(children[RegionPingJakarta]),
    RegionPingHongKong:/**@type {HTMLElement}*/(children[RegionPingHongKong]),
    RegionPingTokyo:/**@type {HTMLElement}*/(children[RegionPingTokyo]),
    RegionPingOsaka:/**@type {HTMLElement}*/(children[RegionPingOsaka]),
    RegionPingSeoul:/**@type {HTMLElement}*/(children[RegionPingSeoul]),
    RegionPingDoha:/**@type {HTMLElement}*/(children[RegionPingDoha]),
    RegionPingTelAviv:/**@type {HTMLElement}*/(children[RegionPingTelAviv]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.RegionSelectorDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IRegionSelectorDisplay.Initialese}*/({
   PingToUsLa:1,
   PingToEuLa:1,
   PingToLndLa:1,
   PingToTaiwanLa:1,
   PingToSaoPauloLa:1,
   PingToSydneyLa:1,
   SuggestedRegionLa:1,
   SuggestedRegionWr:1,
   LdnPingWr:1,
   EuPingWr:1,
   UsPingWr:1,
   RegionPingEu:1,
   RegionPingUs:1,
   RegionPingLondon:1,
   RegionPingSaoPaulo:1,
   RegionPingSydney:1,
   RegionPingTaiwan:1,
   RegionPingBelgium:1,
   RegionPingNetherlands:1,
   RegionPingParis:1,
   RegionPingFrankfurt:1,
   RegionPingWarsaw:1,
   RegionPingZurich:1,
   RegionPingMumbai:1,
   RegionPingDelhi:1,
   RegionPingSingapore:1,
   RegionPingJakarta:1,
   RegionPingHongKong:1,
   RegionPingTokyo:1,
   RegionPingOsaka:1,
   RegionPingSeoul:1,
   RegionPingDoha:1,
   RegionPingTelAviv:1,
  }),
  initializer({
   PingToUsLa:_PingToUsLa,
   PingToEuLa:_PingToEuLa,
   PingToLndLa:_PingToLndLa,
   PingToTaiwanLa:_PingToTaiwanLa,
   PingToSaoPauloLa:_PingToSaoPauloLa,
   PingToSydneyLa:_PingToSydneyLa,
   SuggestedRegionLa:_SuggestedRegionLa,
   SuggestedRegionWr:_SuggestedRegionWr,
   LdnPingWr:_LdnPingWr,
   EuPingWr:_EuPingWr,
   UsPingWr:_UsPingWr,
   RegionPingEu:_RegionPingEu,
   RegionPingUs:_RegionPingUs,
   RegionPingLondon:_RegionPingLondon,
   RegionPingSaoPaulo:_RegionPingSaoPaulo,
   RegionPingSydney:_RegionPingSydney,
   RegionPingTaiwan:_RegionPingTaiwan,
   RegionPingBelgium:_RegionPingBelgium,
   RegionPingNetherlands:_RegionPingNetherlands,
   RegionPingParis:_RegionPingParis,
   RegionPingFrankfurt:_RegionPingFrankfurt,
   RegionPingWarsaw:_RegionPingWarsaw,
   RegionPingZurich:_RegionPingZurich,
   RegionPingMumbai:_RegionPingMumbai,
   RegionPingDelhi:_RegionPingDelhi,
   RegionPingSingapore:_RegionPingSingapore,
   RegionPingJakarta:_RegionPingJakarta,
   RegionPingHongKong:_RegionPingHongKong,
   RegionPingTokyo:_RegionPingTokyo,
   RegionPingOsaka:_RegionPingOsaka,
   RegionPingSeoul:_RegionPingSeoul,
   RegionPingDoha:_RegionPingDoha,
   RegionPingTelAviv:_RegionPingTelAviv,
  }) {
   if(_PingToUsLa!==undefined) this.PingToUsLa=_PingToUsLa
   if(_PingToEuLa!==undefined) this.PingToEuLa=_PingToEuLa
   if(_PingToLndLa!==undefined) this.PingToLndLa=_PingToLndLa
   if(_PingToTaiwanLa!==undefined) this.PingToTaiwanLa=_PingToTaiwanLa
   if(_PingToSaoPauloLa!==undefined) this.PingToSaoPauloLa=_PingToSaoPauloLa
   if(_PingToSydneyLa!==undefined) this.PingToSydneyLa=_PingToSydneyLa
   if(_SuggestedRegionLa!==undefined) this.SuggestedRegionLa=_SuggestedRegionLa
   if(_SuggestedRegionWr!==undefined) this.SuggestedRegionWr=_SuggestedRegionWr
   if(_LdnPingWr!==undefined) this.LdnPingWr=_LdnPingWr
   if(_EuPingWr!==undefined) this.EuPingWr=_EuPingWr
   if(_UsPingWr!==undefined) this.UsPingWr=_UsPingWr
   if(_RegionPingEu!==undefined) this.RegionPingEu=_RegionPingEu
   if(_RegionPingUs!==undefined) this.RegionPingUs=_RegionPingUs
   if(_RegionPingLondon!==undefined) this.RegionPingLondon=_RegionPingLondon
   if(_RegionPingSaoPaulo!==undefined) this.RegionPingSaoPaulo=_RegionPingSaoPaulo
   if(_RegionPingSydney!==undefined) this.RegionPingSydney=_RegionPingSydney
   if(_RegionPingTaiwan!==undefined) this.RegionPingTaiwan=_RegionPingTaiwan
   if(_RegionPingBelgium!==undefined) this.RegionPingBelgium=_RegionPingBelgium
   if(_RegionPingNetherlands!==undefined) this.RegionPingNetherlands=_RegionPingNetherlands
   if(_RegionPingParis!==undefined) this.RegionPingParis=_RegionPingParis
   if(_RegionPingFrankfurt!==undefined) this.RegionPingFrankfurt=_RegionPingFrankfurt
   if(_RegionPingWarsaw!==undefined) this.RegionPingWarsaw=_RegionPingWarsaw
   if(_RegionPingZurich!==undefined) this.RegionPingZurich=_RegionPingZurich
   if(_RegionPingMumbai!==undefined) this.RegionPingMumbai=_RegionPingMumbai
   if(_RegionPingDelhi!==undefined) this.RegionPingDelhi=_RegionPingDelhi
   if(_RegionPingSingapore!==undefined) this.RegionPingSingapore=_RegionPingSingapore
   if(_RegionPingJakarta!==undefined) this.RegionPingJakarta=_RegionPingJakarta
   if(_RegionPingHongKong!==undefined) this.RegionPingHongKong=_RegionPingHongKong
   if(_RegionPingTokyo!==undefined) this.RegionPingTokyo=_RegionPingTokyo
   if(_RegionPingOsaka!==undefined) this.RegionPingOsaka=_RegionPingOsaka
   if(_RegionPingSeoul!==undefined) this.RegionPingSeoul=_RegionPingSeoul
   if(_RegionPingDoha!==undefined) this.RegionPingDoha=_RegionPingDoha
   if(_RegionPingTelAviv!==undefined) this.RegionPingTelAviv=_RegionPingTelAviv
  },
 }),
]