import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorDisplay}
 */
function __AbstractRegionSelectorDisplay() {}
__AbstractRegionSelectorDisplay.prototype = /** @type {!_AbstractRegionSelectorDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorDisplay}
 */
class _AbstractRegionSelectorDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorDisplay} ‎
 */
class AbstractRegionSelectorDisplay extends newAbstract(
 _AbstractRegionSelectorDisplay,950992890620,null,{
  asIRegionSelectorDisplay:1,
  superRegionSelectorDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorDisplay} */
AbstractRegionSelectorDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorDisplay} */
function AbstractRegionSelectorDisplayClass(){}

export default AbstractRegionSelectorDisplay


AbstractRegionSelectorDisplay[$implementations]=[
 __AbstractRegionSelectorDisplay,
 GraphicsDriverBack,
 AbstractRegionSelectorDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IRegionSelectorDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IRegionSelectorDisplay}*/({
    SuggestedRegionWr:twinMock,
    SuggestedRegionLa:twinMock,
    PingToUsLa:twinMock,
    PingToEuLa:twinMock,
    PingToLndLa:twinMock,
    PingToTaiwanLa:twinMock,
    PingToSaoPauloLa:twinMock,
    PingToSydneyLa:twinMock,
    LdnPingWr:twinMock,
    EuPingWr:twinMock,
    UsPingWr:twinMock,
    RegionPingEu:twinMock,
    RegionPingUs:twinMock,
    RegionPingLondon:twinMock,
    RegionPingSaoPaulo:twinMock,
    RegionPingSydney:twinMock,
    RegionPingTaiwan:twinMock,
    RegionPingBelgium:twinMock,
    RegionPingNetherlands:twinMock,
    RegionPingParis:twinMock,
    RegionPingFrankfurt:twinMock,
    RegionPingWarsaw:twinMock,
    RegionPingZurich:twinMock,
    RegionPingMumbai:twinMock,
    RegionPingDelhi:twinMock,
    RegionPingSingapore:twinMock,
    RegionPingJakarta:twinMock,
    RegionPingHongKong:twinMock,
    RegionPingTokyo:twinMock,
    RegionPingOsaka:twinMock,
    RegionPingSeoul:twinMock,
    RegionPingDoha:twinMock,
    RegionPingTelAviv:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.RegionSelectorDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IRegionSelectorDisplay.Initialese}*/({
   PingToUsLa:1,
   PingToEuLa:1,
   PingToLndLa:1,
   PingToTaiwanLa:1,
   PingToSaoPauloLa:1,
   PingToSydneyLa:1,
   SuggestedRegionLa:1,
   SuggestedRegionWr:1,
   LdnPingWr:1,
   EuPingWr:1,
   UsPingWr:1,
   RegionPingEu:1,
   RegionPingUs:1,
   RegionPingLondon:1,
   RegionPingSaoPaulo:1,
   RegionPingSydney:1,
   RegionPingTaiwan:1,
   RegionPingBelgium:1,
   RegionPingNetherlands:1,
   RegionPingParis:1,
   RegionPingFrankfurt:1,
   RegionPingWarsaw:1,
   RegionPingZurich:1,
   RegionPingMumbai:1,
   RegionPingDelhi:1,
   RegionPingSingapore:1,
   RegionPingJakarta:1,
   RegionPingHongKong:1,
   RegionPingTokyo:1,
   RegionPingOsaka:1,
   RegionPingSeoul:1,
   RegionPingDoha:1,
   RegionPingTelAviv:1,
  }),
  initializer({
   PingToUsLa:_PingToUsLa,
   PingToEuLa:_PingToEuLa,
   PingToLndLa:_PingToLndLa,
   PingToTaiwanLa:_PingToTaiwanLa,
   PingToSaoPauloLa:_PingToSaoPauloLa,
   PingToSydneyLa:_PingToSydneyLa,
   SuggestedRegionLa:_SuggestedRegionLa,
   SuggestedRegionWr:_SuggestedRegionWr,
   LdnPingWr:_LdnPingWr,
   EuPingWr:_EuPingWr,
   UsPingWr:_UsPingWr,
   RegionPingEu:_RegionPingEu,
   RegionPingUs:_RegionPingUs,
   RegionPingLondon:_RegionPingLondon,
   RegionPingSaoPaulo:_RegionPingSaoPaulo,
   RegionPingSydney:_RegionPingSydney,
   RegionPingTaiwan:_RegionPingTaiwan,
   RegionPingBelgium:_RegionPingBelgium,
   RegionPingNetherlands:_RegionPingNetherlands,
   RegionPingParis:_RegionPingParis,
   RegionPingFrankfurt:_RegionPingFrankfurt,
   RegionPingWarsaw:_RegionPingWarsaw,
   RegionPingZurich:_RegionPingZurich,
   RegionPingMumbai:_RegionPingMumbai,
   RegionPingDelhi:_RegionPingDelhi,
   RegionPingSingapore:_RegionPingSingapore,
   RegionPingJakarta:_RegionPingJakarta,
   RegionPingHongKong:_RegionPingHongKong,
   RegionPingTokyo:_RegionPingTokyo,
   RegionPingOsaka:_RegionPingOsaka,
   RegionPingSeoul:_RegionPingSeoul,
   RegionPingDoha:_RegionPingDoha,
   RegionPingTelAviv:_RegionPingTelAviv,
  }) {
   if(_PingToUsLa!==undefined) this.PingToUsLa=_PingToUsLa
   if(_PingToEuLa!==undefined) this.PingToEuLa=_PingToEuLa
   if(_PingToLndLa!==undefined) this.PingToLndLa=_PingToLndLa
   if(_PingToTaiwanLa!==undefined) this.PingToTaiwanLa=_PingToTaiwanLa
   if(_PingToSaoPauloLa!==undefined) this.PingToSaoPauloLa=_PingToSaoPauloLa
   if(_PingToSydneyLa!==undefined) this.PingToSydneyLa=_PingToSydneyLa
   if(_SuggestedRegionLa!==undefined) this.SuggestedRegionLa=_SuggestedRegionLa
   if(_SuggestedRegionWr!==undefined) this.SuggestedRegionWr=_SuggestedRegionWr
   if(_LdnPingWr!==undefined) this.LdnPingWr=_LdnPingWr
   if(_EuPingWr!==undefined) this.EuPingWr=_EuPingWr
   if(_UsPingWr!==undefined) this.UsPingWr=_UsPingWr
   if(_RegionPingEu!==undefined) this.RegionPingEu=_RegionPingEu
   if(_RegionPingUs!==undefined) this.RegionPingUs=_RegionPingUs
   if(_RegionPingLondon!==undefined) this.RegionPingLondon=_RegionPingLondon
   if(_RegionPingSaoPaulo!==undefined) this.RegionPingSaoPaulo=_RegionPingSaoPaulo
   if(_RegionPingSydney!==undefined) this.RegionPingSydney=_RegionPingSydney
   if(_RegionPingTaiwan!==undefined) this.RegionPingTaiwan=_RegionPingTaiwan
   if(_RegionPingBelgium!==undefined) this.RegionPingBelgium=_RegionPingBelgium
   if(_RegionPingNetherlands!==undefined) this.RegionPingNetherlands=_RegionPingNetherlands
   if(_RegionPingParis!==undefined) this.RegionPingParis=_RegionPingParis
   if(_RegionPingFrankfurt!==undefined) this.RegionPingFrankfurt=_RegionPingFrankfurt
   if(_RegionPingWarsaw!==undefined) this.RegionPingWarsaw=_RegionPingWarsaw
   if(_RegionPingZurich!==undefined) this.RegionPingZurich=_RegionPingZurich
   if(_RegionPingMumbai!==undefined) this.RegionPingMumbai=_RegionPingMumbai
   if(_RegionPingDelhi!==undefined) this.RegionPingDelhi=_RegionPingDelhi
   if(_RegionPingSingapore!==undefined) this.RegionPingSingapore=_RegionPingSingapore
   if(_RegionPingJakarta!==undefined) this.RegionPingJakarta=_RegionPingJakarta
   if(_RegionPingHongKong!==undefined) this.RegionPingHongKong=_RegionPingHongKong
   if(_RegionPingTokyo!==undefined) this.RegionPingTokyo=_RegionPingTokyo
   if(_RegionPingOsaka!==undefined) this.RegionPingOsaka=_RegionPingOsaka
   if(_RegionPingSeoul!==undefined) this.RegionPingSeoul=_RegionPingSeoul
   if(_RegionPingDoha!==undefined) this.RegionPingDoha=_RegionPingDoha
   if(_RegionPingTelAviv!==undefined) this.RegionPingTelAviv=_RegionPingTelAviv
  },
 }),
]