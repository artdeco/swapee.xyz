import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorProcessor}
 */
function __AbstractRegionSelectorProcessor() {}
__AbstractRegionSelectorProcessor.prototype = /** @type {!_AbstractRegionSelectorProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractRegionSelectorProcessor}
 */
class _AbstractRegionSelectorProcessor { }
/**
 * The processor to compute changes to the memory for the _IRegionSelector_.
 * @extends {xyz.swapee.wc.AbstractRegionSelectorProcessor} ‎
 */
class AbstractRegionSelectorProcessor extends newAbstract(
 _AbstractRegionSelectorProcessor,95099289069,null,{
  asIRegionSelectorProcessor:1,
  superRegionSelectorProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorProcessor} */
AbstractRegionSelectorProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractRegionSelectorProcessor} */
function AbstractRegionSelectorProcessorClass(){}

export default AbstractRegionSelectorProcessor


AbstractRegionSelectorProcessor[$implementations]=[
 __AbstractRegionSelectorProcessor,
]