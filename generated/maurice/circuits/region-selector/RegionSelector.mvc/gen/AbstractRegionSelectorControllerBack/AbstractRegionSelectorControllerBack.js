import AbstractRegionSelectorControllerAR from '../AbstractRegionSelectorControllerAR'
import {AbstractRegionSelectorController} from '../AbstractRegionSelectorController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorControllerBack}
 */
function __AbstractRegionSelectorControllerBack() {}
__AbstractRegionSelectorControllerBack.prototype = /** @type {!_AbstractRegionSelectorControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorController}
 */
class _AbstractRegionSelectorControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractRegionSelectorController} ‎
 */
class AbstractRegionSelectorControllerBack extends newAbstract(
 _AbstractRegionSelectorControllerBack,950992890623,null,{
  asIRegionSelectorController:1,
  superRegionSelectorController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorController} */
AbstractRegionSelectorControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractRegionSelectorController} */
function AbstractRegionSelectorControllerBackClass(){}

export default AbstractRegionSelectorControllerBack


AbstractRegionSelectorControllerBack[$implementations]=[
 __AbstractRegionSelectorControllerBack,
 AbstractRegionSelectorController,
 AbstractRegionSelectorControllerAR,
 DriverBack,
]