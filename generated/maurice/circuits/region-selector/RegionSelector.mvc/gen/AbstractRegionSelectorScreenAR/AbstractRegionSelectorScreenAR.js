import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractRegionSelectorScreenAR}
 */
function __AbstractRegionSelectorScreenAR() {}
__AbstractRegionSelectorScreenAR.prototype = /** @type {!_AbstractRegionSelectorScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractRegionSelectorScreenAR}
 */
class _AbstractRegionSelectorScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IRegionSelectorScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractRegionSelectorScreenAR} ‎
 */
class AbstractRegionSelectorScreenAR extends newAbstract(
 _AbstractRegionSelectorScreenAR,950992890628,null,{
  asIRegionSelectorScreenAR:1,
  superRegionSelectorScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractRegionSelectorScreenAR} */
AbstractRegionSelectorScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractRegionSelectorScreenAR} */
function AbstractRegionSelectorScreenARClass(){}

export default AbstractRegionSelectorScreenAR


AbstractRegionSelectorScreenAR[$implementations]=[
 __AbstractRegionSelectorScreenAR,
 AR,
 AbstractRegionSelectorScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IRegionSelectorScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractRegionSelectorScreenAR}