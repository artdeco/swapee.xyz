/** @extends {xyz.swapee.wc.AbstractExchangeStatusTitle} */
export default class AbstractExchangeStatusTitle extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractExchangeStatusTitleComputer} */
export class AbstractExchangeStatusTitleComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeStatusTitleController} */
export class AbstractExchangeStatusTitleController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeStatusTitlePort} */
export class ExchangeStatusTitlePort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeStatusTitleView} */
export class AbstractExchangeStatusTitleView extends (<view>
  <classes>
   <string opt name="Class" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeStatusTitleElement} */
export class AbstractExchangeStatusTitleElement extends (<element v3 html mv>
 <block src="./ExchangeStatusTitle.mvc/src/ExchangeStatusTitleElement/methods/render.jsx" />
 <inducer src="./ExchangeStatusTitle.mvc/src/ExchangeStatusTitleElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent} */
export class AbstractExchangeStatusTitleHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>