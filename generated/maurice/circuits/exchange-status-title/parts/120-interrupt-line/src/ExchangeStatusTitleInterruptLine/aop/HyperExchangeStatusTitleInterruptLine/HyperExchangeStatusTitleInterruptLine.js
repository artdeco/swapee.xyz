import AbstractHyperExchangeStatusTitleInterruptLine from '../../../../gen/AbstractExchangeStatusTitleInterruptLine/hyper/AbstractHyperExchangeStatusTitleInterruptLine'
import ExchangeStatusTitleInterruptLine from '../../ExchangeStatusTitleInterruptLine'
import ExchangeStatusTitleInterruptLineGeneralAspects from '../ExchangeStatusTitleInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperExchangeStatusTitleInterruptLine} */
export default class extends AbstractHyperExchangeStatusTitleInterruptLine
 .consults(
  ExchangeStatusTitleInterruptLineGeneralAspects,
 )
 .implements(
  ExchangeStatusTitleInterruptLine,
 )
{}