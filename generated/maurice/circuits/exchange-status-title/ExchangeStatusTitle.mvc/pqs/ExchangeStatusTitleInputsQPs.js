import {ExchangeStatusTitleInputsPQs} from './ExchangeStatusTitleInputsPQs'
export const ExchangeStatusTitleInputsQPs=/**@type {!xyz.swapee.wc.ExchangeStatusTitleInputsQPs}*/(Object.keys(ExchangeStatusTitleInputsPQs)
 .reduce((a,k)=>{a[ExchangeStatusTitleInputsPQs[k]]=k;return a},{}))