import {ExchangeStatusTitleVdusPQs} from './ExchangeStatusTitleVdusPQs'
export const ExchangeStatusTitleVdusQPs=/**@type {!xyz.swapee.wc.ExchangeStatusTitleVdusQPs}*/(Object.keys(ExchangeStatusTitleVdusPQs)
 .reduce((a,k)=>{a[ExchangeStatusTitleVdusPQs[k]]=k;return a},{}))