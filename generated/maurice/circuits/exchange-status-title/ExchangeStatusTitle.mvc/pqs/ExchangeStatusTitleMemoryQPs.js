import {ExchangeStatusTitleMemoryPQs} from './ExchangeStatusTitleMemoryPQs'
export const ExchangeStatusTitleMemoryQPs=/**@type {!xyz.swapee.wc.ExchangeStatusTitleMemoryQPs}*/(Object.keys(ExchangeStatusTitleMemoryPQs)
 .reduce((a,k)=>{a[ExchangeStatusTitleMemoryPQs[k]]=k;return a},{}))