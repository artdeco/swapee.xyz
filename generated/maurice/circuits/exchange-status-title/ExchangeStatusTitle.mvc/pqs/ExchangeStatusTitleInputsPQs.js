import {ExchangeStatusTitleMemoryPQs} from './ExchangeStatusTitleMemoryPQs'
export const ExchangeStatusTitleInputsPQs=/**@type {!xyz.swapee.wc.ExchangeStatusTitleInputsQPs}*/({
 ...ExchangeStatusTitleMemoryPQs,
})