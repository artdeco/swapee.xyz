import ExchangeStatusTitleClassesPQs from './ExchangeStatusTitleClassesPQs'
export const ExchangeStatusTitleClassesQPs=/**@type {!xyz.swapee.wc.ExchangeStatusTitleClassesQPs}*/(Object.keys(ExchangeStatusTitleClassesPQs)
 .reduce((a,k)=>{a[ExchangeStatusTitleClassesPQs[k]]=k;return a},{}))