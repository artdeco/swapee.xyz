export const ExchangeStatusTitleVdusPQs=/**@type {!xyz.swapee.wc.ExchangeStatusTitleVdusPQs}*/({
 AwaitingStatusLa:'a2741',
 ConfirmingStatusLa:'a2742',
 ExchangingStatusLa:'a2743',
 SendingStatusLa:'a2744',
 FinishedStatusLa:'a2745',
 FailedStatusLa:'a2746',
 RefundedStatusLa:'a2747',
 OverdueStatusLa:'a2748',
 HoldStatusLa:'a2749',
 ExpiredStatusLa:'a27410',
 NewStatusLa:'a27411',
})