/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IExchangeStatusTitleComputer={}
xyz.swapee.wc.IExchangeStatusTitleOuterCore={}
xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model={}
xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model.Status={}
xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel={}
xyz.swapee.wc.IExchangeStatusTitlePort={}
xyz.swapee.wc.IExchangeStatusTitlePort.Inputs={}
xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs={}
xyz.swapee.wc.IExchangeStatusTitleCore={}
xyz.swapee.wc.IExchangeStatusTitleCore.Model={}
xyz.swapee.wc.IExchangeStatusTitlePortInterface={}
xyz.swapee.wc.IExchangeStatusTitleProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IExchangeStatusTitleController={}
xyz.swapee.wc.front.IExchangeStatusTitleControllerAT={}
xyz.swapee.wc.front.IExchangeStatusTitleScreenAR={}
xyz.swapee.wc.IExchangeStatusTitle={}
xyz.swapee.wc.IExchangeStatusTitleHtmlComponent={}
xyz.swapee.wc.IExchangeStatusTitleElement={}
xyz.swapee.wc.IExchangeStatusTitleElementPort={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.LoadingStripeOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts={}
xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs={}
xyz.swapee.wc.IExchangeStatusTitleDesigner={}
xyz.swapee.wc.IExchangeStatusTitleDesigner.communicator={}
xyz.swapee.wc.IExchangeStatusTitleDesigner.relay={}
xyz.swapee.wc.IExchangeStatusTitleDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IExchangeStatusTitleDisplay={}
xyz.swapee.wc.back.IExchangeStatusTitleController={}
xyz.swapee.wc.back.IExchangeStatusTitleControllerAR={}
xyz.swapee.wc.back.IExchangeStatusTitleScreen={}
xyz.swapee.wc.back.IExchangeStatusTitleScreenAT={}
xyz.swapee.wc.IExchangeStatusTitleController={}
xyz.swapee.wc.IExchangeStatusTitleScreen={}
xyz.swapee.wc.IExchangeStatusTitleGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/02-IExchangeStatusTitleComputer.xml}  d4d4e08d9e29e758408ec3ec583c1035 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleComputer)} xyz.swapee.wc.AbstractExchangeStatusTitleComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleComputer} xyz.swapee.wc.ExchangeStatusTitleComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleComputer` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleComputer
 */
xyz.swapee.wc.AbstractExchangeStatusTitleComputer = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleComputer.constructor&xyz.swapee.wc.ExchangeStatusTitleComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleComputer.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleComputer.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleComputer}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleComputer} xyz.swapee.wc.ExchangeStatusTitleComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.ExchangeStatusTitleMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IExchangeStatusTitleComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IExchangeStatusTitleComputer
 */
xyz.swapee.wc.IExchangeStatusTitleComputer = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleComputer.compute} */
xyz.swapee.wc.IExchangeStatusTitleComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleComputer} xyz.swapee.wc.IExchangeStatusTitleComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeStatusTitleComputer_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleComputer
 * @implements {xyz.swapee.wc.IExchangeStatusTitleComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleComputer = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleComputer.constructor&xyz.swapee.wc.IExchangeStatusTitleComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleComputer}
 */
xyz.swapee.wc.ExchangeStatusTitleComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleComputer} */
xyz.swapee.wc.RecordIExchangeStatusTitleComputer

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleComputer} xyz.swapee.wc.BoundIExchangeStatusTitleComputer */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleComputer} xyz.swapee.wc.BoundExchangeStatusTitleComputer */

/**
 * Contains getters to cast the _IExchangeStatusTitleComputer_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleComputerCaster
 */
xyz.swapee.wc.IExchangeStatusTitleComputerCaster = class { }
/**
 * Cast the _IExchangeStatusTitleComputer_ instance into the _BoundIExchangeStatusTitleComputer_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleComputer}
 */
xyz.swapee.wc.IExchangeStatusTitleComputerCaster.prototype.asIExchangeStatusTitleComputer
/**
 * Access the _ExchangeStatusTitleComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleComputer}
 */
xyz.swapee.wc.IExchangeStatusTitleComputerCaster.prototype.superExchangeStatusTitleComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.ExchangeStatusTitleMemory) => void} xyz.swapee.wc.IExchangeStatusTitleComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleComputer.__compute<!xyz.swapee.wc.IExchangeStatusTitleComputer>} xyz.swapee.wc.IExchangeStatusTitleComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.ExchangeStatusTitleMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusTitleComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusTitleComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/03-IExchangeStatusTitleOuterCore.xml}  130904eeeb91a55e61a3940d5e6c0c0e */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeStatusTitleOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleOuterCore)} xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore} xyz.swapee.wc.ExchangeStatusTitleOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore
 */
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.constructor&xyz.swapee.wc.ExchangeStatusTitleOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleOuterCore|typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleOuterCore|typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleOuterCore|typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleOuterCoreCaster)} xyz.swapee.wc.IExchangeStatusTitleOuterCore.constructor */
/**
 * The _IExchangeStatusTitle_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IExchangeStatusTitleOuterCore
 */
xyz.swapee.wc.IExchangeStatusTitleOuterCore = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusTitleOuterCore.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitleOuterCore

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleOuterCore.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleOuterCore} xyz.swapee.wc.IExchangeStatusTitleOuterCore.typeof */
/**
 * A concrete class of _IExchangeStatusTitleOuterCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleOuterCore
 * @implements {xyz.swapee.wc.IExchangeStatusTitleOuterCore} The _IExchangeStatusTitle_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleOuterCore = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleOuterCore.constructor&xyz.swapee.wc.IExchangeStatusTitleOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusTitleOuterCore.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore}
 */
xyz.swapee.wc.ExchangeStatusTitleOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleOuterCore.
 * @interface xyz.swapee.wc.IExchangeStatusTitleOuterCoreFields
 */
xyz.swapee.wc.IExchangeStatusTitleOuterCoreFields = class { }
/**
 * The _IExchangeStatusTitle_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IExchangeStatusTitleOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore} */
xyz.swapee.wc.RecordIExchangeStatusTitleOuterCore

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore} xyz.swapee.wc.BoundIExchangeStatusTitleOuterCore */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleOuterCore} xyz.swapee.wc.BoundExchangeStatusTitleOuterCore */

/**
 * The status.
 * @typedef {string}
 */
xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model.Status.status

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model.Status} xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model The _IExchangeStatusTitle_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.Status} xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel The _IExchangeStatusTitle_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IExchangeStatusTitleOuterCore_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleOuterCoreCaster
 */
xyz.swapee.wc.IExchangeStatusTitleOuterCoreCaster = class { }
/**
 * Cast the _IExchangeStatusTitleOuterCore_ instance into the _BoundIExchangeStatusTitleOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleOuterCore}
 */
xyz.swapee.wc.IExchangeStatusTitleOuterCoreCaster.prototype.asIExchangeStatusTitleOuterCore
/**
 * Access the _ExchangeStatusTitleOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleOuterCore}
 */
xyz.swapee.wc.IExchangeStatusTitleOuterCoreCaster.prototype.superExchangeStatusTitleOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model.Status The status (optional overlay).
 * @prop {string} [status=""] The status. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model.Status_Safe The status (required overlay).
 * @prop {string} status The status.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.Status The status (optional overlay).
 * @prop {*} [status=null] The status. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.Status_Safe The status (required overlay).
 * @prop {*} status The status.
 */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.Status} xyz.swapee.wc.IExchangeStatusTitlePort.Inputs.Status The status (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.Status_Safe} xyz.swapee.wc.IExchangeStatusTitlePort.Inputs.Status_Safe The status (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.Status} xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs.Status The status (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.Status_Safe} xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs.Status_Safe The status (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model.Status} xyz.swapee.wc.IExchangeStatusTitleCore.Model.Status The status (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model.Status_Safe} xyz.swapee.wc.IExchangeStatusTitleCore.Model.Status_Safe The status (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/04-IExchangeStatusTitlePort.xml}  a67aa8193108d3ea72961bbded91c997 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeStatusTitlePort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitlePort)} xyz.swapee.wc.AbstractExchangeStatusTitlePort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitlePort} xyz.swapee.wc.ExchangeStatusTitlePort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitlePort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitlePort
 */
xyz.swapee.wc.AbstractExchangeStatusTitlePort = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitlePort.constructor&xyz.swapee.wc.ExchangeStatusTitlePort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitlePort.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitlePort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitlePort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitlePort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitlePort|typeof xyz.swapee.wc.ExchangeStatusTitlePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitlePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitlePort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitlePort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitlePort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitlePort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitlePort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitlePort|typeof xyz.swapee.wc.ExchangeStatusTitlePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitlePort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitlePort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitlePort|typeof xyz.swapee.wc.ExchangeStatusTitlePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitlePort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitlePort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitlePort.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitlePort} xyz.swapee.wc.ExchangeStatusTitlePortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitlePortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitlePortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeStatusTitlePort.Inputs>)} xyz.swapee.wc.IExchangeStatusTitlePort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IExchangeStatusTitle_, providing input
 * pins.
 * @interface xyz.swapee.wc.IExchangeStatusTitlePort
 */
xyz.swapee.wc.IExchangeStatusTitlePort = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitlePort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitlePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitlePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitlePort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusTitlePort.resetPort} */
xyz.swapee.wc.IExchangeStatusTitlePort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusTitlePort.resetExchangeStatusTitlePort} */
xyz.swapee.wc.IExchangeStatusTitlePort.prototype.resetExchangeStatusTitlePort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitlePort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitlePort.Initialese>)} xyz.swapee.wc.ExchangeStatusTitlePort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitlePort} xyz.swapee.wc.IExchangeStatusTitlePort.typeof */
/**
 * A concrete class of _IExchangeStatusTitlePort_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitlePort
 * @implements {xyz.swapee.wc.IExchangeStatusTitlePort} The port that serves as an interface to the _IExchangeStatusTitle_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitlePort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitlePort = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitlePort.constructor&xyz.swapee.wc.IExchangeStatusTitlePort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitlePort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitlePort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitlePort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitlePort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitlePort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitlePort}
 */
xyz.swapee.wc.ExchangeStatusTitlePort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitlePort.
 * @interface xyz.swapee.wc.IExchangeStatusTitlePortFields
 */
xyz.swapee.wc.IExchangeStatusTitlePortFields = class { }
/**
 * The inputs to the _IExchangeStatusTitle_'s controller via its port.
 */
xyz.swapee.wc.IExchangeStatusTitlePortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusTitlePort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeStatusTitlePortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeStatusTitlePort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitlePort} */
xyz.swapee.wc.RecordIExchangeStatusTitlePort

/** @typedef {xyz.swapee.wc.IExchangeStatusTitlePort} xyz.swapee.wc.BoundIExchangeStatusTitlePort */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitlePort} xyz.swapee.wc.BoundExchangeStatusTitlePort */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel)} xyz.swapee.wc.IExchangeStatusTitlePort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel} xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IExchangeStatusTitle_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeStatusTitlePort.Inputs
 */
xyz.swapee.wc.IExchangeStatusTitlePort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitlePort.Inputs.constructor&xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusTitlePort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitlePort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel)} xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs.constructor */
/**
 * The inputs to the _IExchangeStatusTitle_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs
 */
xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs.constructor&xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitlePortInterface
 */
xyz.swapee.wc.IExchangeStatusTitlePortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IExchangeStatusTitlePortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IExchangeStatusTitlePortInterface.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitlePortInterface

/**
 * A concrete class of _IExchangeStatusTitlePortInterface_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitlePortInterface
 * @implements {xyz.swapee.wc.IExchangeStatusTitlePortInterface} The port interface.
 */
xyz.swapee.wc.ExchangeStatusTitlePortInterface = class extends xyz.swapee.wc.IExchangeStatusTitlePortInterface { }
xyz.swapee.wc.ExchangeStatusTitlePortInterface.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitlePortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitlePortInterface.Props
 * @prop {string} status The status.
 */

/**
 * Contains getters to cast the _IExchangeStatusTitlePort_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitlePortCaster
 */
xyz.swapee.wc.IExchangeStatusTitlePortCaster = class { }
/**
 * Cast the _IExchangeStatusTitlePort_ instance into the _BoundIExchangeStatusTitlePort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitlePort}
 */
xyz.swapee.wc.IExchangeStatusTitlePortCaster.prototype.asIExchangeStatusTitlePort
/**
 * Access the _ExchangeStatusTitlePort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitlePort}
 */
xyz.swapee.wc.IExchangeStatusTitlePortCaster.prototype.superExchangeStatusTitlePort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusTitlePort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitlePort.__resetPort<!xyz.swapee.wc.IExchangeStatusTitlePort>} xyz.swapee.wc.IExchangeStatusTitlePort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitlePort.resetPort} */
/**
 * Resets the _IExchangeStatusTitle_ port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusTitlePort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusTitlePort.__resetExchangeStatusTitlePort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitlePort.__resetExchangeStatusTitlePort<!xyz.swapee.wc.IExchangeStatusTitlePort>} xyz.swapee.wc.IExchangeStatusTitlePort._resetExchangeStatusTitlePort */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitlePort.resetExchangeStatusTitlePort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusTitlePort.resetExchangeStatusTitlePort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusTitlePort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/09-IExchangeStatusTitleCore.xml}  ea88f73f9bb94740f57fde3d54c3a96f */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeStatusTitleCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleCore)} xyz.swapee.wc.AbstractExchangeStatusTitleCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleCore} xyz.swapee.wc.ExchangeStatusTitleCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleCore
 */
xyz.swapee.wc.AbstractExchangeStatusTitleCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleCore.constructor&xyz.swapee.wc.ExchangeStatusTitleCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleCore|typeof xyz.swapee.wc.ExchangeStatusTitleCore)|(!xyz.swapee.wc.IExchangeStatusTitleOuterCore|typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleCore|typeof xyz.swapee.wc.ExchangeStatusTitleCore)|(!xyz.swapee.wc.IExchangeStatusTitleOuterCore|typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleCore|typeof xyz.swapee.wc.ExchangeStatusTitleCore)|(!xyz.swapee.wc.IExchangeStatusTitleOuterCore|typeof xyz.swapee.wc.ExchangeStatusTitleOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleCore}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleCoreCaster&xyz.swapee.wc.IExchangeStatusTitleOuterCore)} xyz.swapee.wc.IExchangeStatusTitleCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IExchangeStatusTitleCore
 */
xyz.swapee.wc.IExchangeStatusTitleCore = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusTitleOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IExchangeStatusTitleCore.resetCore} */
xyz.swapee.wc.IExchangeStatusTitleCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleCore.resetExchangeStatusTitleCore} */
xyz.swapee.wc.IExchangeStatusTitleCore.prototype.resetExchangeStatusTitleCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleCore.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleCore} xyz.swapee.wc.IExchangeStatusTitleCore.typeof */
/**
 * A concrete class of _IExchangeStatusTitleCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleCore
 * @implements {xyz.swapee.wc.IExchangeStatusTitleCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleCore = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleCore.constructor&xyz.swapee.wc.IExchangeStatusTitleCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusTitleCore.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleCore}
 */
xyz.swapee.wc.ExchangeStatusTitleCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleCore.
 * @interface xyz.swapee.wc.IExchangeStatusTitleCoreFields
 */
xyz.swapee.wc.IExchangeStatusTitleCoreFields = class { }
/**
 * The _IExchangeStatusTitle_'s memory.
 */
xyz.swapee.wc.IExchangeStatusTitleCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeStatusTitleCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IExchangeStatusTitleCoreFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeStatusTitleCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleCore} */
xyz.swapee.wc.RecordIExchangeStatusTitleCore

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleCore} xyz.swapee.wc.BoundIExchangeStatusTitleCore */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleCore} xyz.swapee.wc.BoundExchangeStatusTitleCore */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model} xyz.swapee.wc.IExchangeStatusTitleCore.Model The _IExchangeStatusTitle_'s memory. */

/**
 * Contains getters to cast the _IExchangeStatusTitleCore_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleCoreCaster
 */
xyz.swapee.wc.IExchangeStatusTitleCoreCaster = class { }
/**
 * Cast the _IExchangeStatusTitleCore_ instance into the _BoundIExchangeStatusTitleCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleCore}
 */
xyz.swapee.wc.IExchangeStatusTitleCoreCaster.prototype.asIExchangeStatusTitleCore
/**
 * Access the _ExchangeStatusTitleCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleCore}
 */
xyz.swapee.wc.IExchangeStatusTitleCoreCaster.prototype.superExchangeStatusTitleCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusTitleCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleCore.__resetCore<!xyz.swapee.wc.IExchangeStatusTitleCore>} xyz.swapee.wc.IExchangeStatusTitleCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleCore.resetCore} */
/**
 * Resets the _IExchangeStatusTitle_ core.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusTitleCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusTitleCore.__resetExchangeStatusTitleCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleCore.__resetExchangeStatusTitleCore<!xyz.swapee.wc.IExchangeStatusTitleCore>} xyz.swapee.wc.IExchangeStatusTitleCore._resetExchangeStatusTitleCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleCore.resetExchangeStatusTitleCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusTitleCore.resetExchangeStatusTitleCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusTitleCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/10-IExchangeStatusTitleProcessor.xml}  75fb8e0541f4c284f053f612c73b75b5 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese&xyz.swapee.wc.IExchangeStatusTitleController.Initialese} xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleProcessor)} xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleProcessor} xyz.swapee.wc.ExchangeStatusTitleProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleProcessor
 */
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.constructor&xyz.swapee.wc.ExchangeStatusTitleProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!xyz.swapee.wc.IExchangeStatusTitleCore|typeof xyz.swapee.wc.ExchangeStatusTitleCore)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!xyz.swapee.wc.IExchangeStatusTitleCore|typeof xyz.swapee.wc.ExchangeStatusTitleCore)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!xyz.swapee.wc.IExchangeStatusTitleCore|typeof xyz.swapee.wc.ExchangeStatusTitleCore)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleProcessor} xyz.swapee.wc.ExchangeStatusTitleProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleProcessorCaster&xyz.swapee.wc.IExchangeStatusTitleComputer&xyz.swapee.wc.IExchangeStatusTitleCore&xyz.swapee.wc.IExchangeStatusTitleController)} xyz.swapee.wc.IExchangeStatusTitleProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleController} xyz.swapee.wc.IExchangeStatusTitleController.typeof */
/**
 * The processor to compute changes to the memory for the _IExchangeStatusTitle_.
 * @interface xyz.swapee.wc.IExchangeStatusTitleProcessor
 */
xyz.swapee.wc.IExchangeStatusTitleProcessor = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusTitleComputer.typeof&xyz.swapee.wc.IExchangeStatusTitleCore.typeof&xyz.swapee.wc.IExchangeStatusTitleController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleProcessor} xyz.swapee.wc.IExchangeStatusTitleProcessor.typeof */
/**
 * A concrete class of _IExchangeStatusTitleProcessor_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleProcessor
 * @implements {xyz.swapee.wc.IExchangeStatusTitleProcessor} The processor to compute changes to the memory for the _IExchangeStatusTitle_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleProcessor = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleProcessor.constructor&xyz.swapee.wc.IExchangeStatusTitleProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.ExchangeStatusTitleProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleProcessor} */
xyz.swapee.wc.RecordIExchangeStatusTitleProcessor

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleProcessor} xyz.swapee.wc.BoundIExchangeStatusTitleProcessor */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleProcessor} xyz.swapee.wc.BoundExchangeStatusTitleProcessor */

/**
 * Contains getters to cast the _IExchangeStatusTitleProcessor_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleProcessorCaster
 */
xyz.swapee.wc.IExchangeStatusTitleProcessorCaster = class { }
/**
 * Cast the _IExchangeStatusTitleProcessor_ instance into the _BoundIExchangeStatusTitleProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.IExchangeStatusTitleProcessorCaster.prototype.asIExchangeStatusTitleProcessor
/**
 * Access the _ExchangeStatusTitleProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.IExchangeStatusTitleProcessorCaster.prototype.superExchangeStatusTitleProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/100-ExchangeStatusTitleMemory.xml}  0fae0682356bd6ffddca939d30395abd */
/**
 * The memory of the _IExchangeStatusTitle_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.ExchangeStatusTitleMemory
 */
xyz.swapee.wc.ExchangeStatusTitleMemory = class { }
/**
 * The status. Default empty string.
 */
xyz.swapee.wc.ExchangeStatusTitleMemory.prototype.status = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/102-ExchangeStatusTitleInputs.xml}  575d0610bc598a01e328a8bdf88b21b1 */
/**
 * The inputs of the _IExchangeStatusTitle_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.ExchangeStatusTitleInputs
 */
xyz.swapee.wc.front.ExchangeStatusTitleInputs = class { }
/**
 * The status. Default empty string.
 */
xyz.swapee.wc.front.ExchangeStatusTitleInputs.prototype.status = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/11-IExchangeStatusTitle.xml}  4030cdf092fa806b6408684068a3b283 */
/**
 * An atomic wrapper for the _IExchangeStatusTitle_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.ExchangeStatusTitleEnv
 */
xyz.swapee.wc.ExchangeStatusTitleEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.ExchangeStatusTitleEnv.prototype.exchangeStatusTitle = /** @type {xyz.swapee.wc.IExchangeStatusTitle} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.IExchangeStatusTitleController.Inputs>&xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese&xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese&xyz.swapee.wc.IExchangeStatusTitleController.Initialese} xyz.swapee.wc.IExchangeStatusTitle.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitle)} xyz.swapee.wc.AbstractExchangeStatusTitle.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitle} xyz.swapee.wc.ExchangeStatusTitle.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitle` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitle
 */
xyz.swapee.wc.AbstractExchangeStatusTitle = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitle.constructor&xyz.swapee.wc.ExchangeStatusTitle.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitle.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitle
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitle.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitle} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitle|typeof xyz.swapee.wc.ExchangeStatusTitle)|(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitle}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitle.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitle}
 */
xyz.swapee.wc.AbstractExchangeStatusTitle.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitle}
 */
xyz.swapee.wc.AbstractExchangeStatusTitle.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitle|typeof xyz.swapee.wc.ExchangeStatusTitle)|(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitle}
 */
xyz.swapee.wc.AbstractExchangeStatusTitle.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitle|typeof xyz.swapee.wc.ExchangeStatusTitle)|(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitle}
 */
xyz.swapee.wc.AbstractExchangeStatusTitle.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitle.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitle} xyz.swapee.wc.ExchangeStatusTitleConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitle.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IExchangeStatusTitle.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IExchangeStatusTitle.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IExchangeStatusTitle.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.ExchangeStatusTitleMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.ExchangeStatusTitleClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleCaster&xyz.swapee.wc.IExchangeStatusTitleProcessor&xyz.swapee.wc.IExchangeStatusTitleComputer&xyz.swapee.wc.IExchangeStatusTitleController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.IExchangeStatusTitleController.Inputs, null>)} xyz.swapee.wc.IExchangeStatusTitle.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IExchangeStatusTitle
 */
xyz.swapee.wc.IExchangeStatusTitle = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitle.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusTitleProcessor.typeof&xyz.swapee.wc.IExchangeStatusTitleComputer.typeof&xyz.swapee.wc.IExchangeStatusTitleController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitle* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitle.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitle.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitle&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitle.Initialese>)} xyz.swapee.wc.ExchangeStatusTitle.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitle} xyz.swapee.wc.IExchangeStatusTitle.typeof */
/**
 * A concrete class of _IExchangeStatusTitle_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitle
 * @implements {xyz.swapee.wc.IExchangeStatusTitle} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitle.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitle = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitle.constructor&xyz.swapee.wc.IExchangeStatusTitle.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitle* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitle.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitle* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitle.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitle.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitle}
 */
xyz.swapee.wc.ExchangeStatusTitle.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitle.
 * @interface xyz.swapee.wc.IExchangeStatusTitleFields
 */
xyz.swapee.wc.IExchangeStatusTitleFields = class { }
/**
 * The input pins of the _IExchangeStatusTitle_ port.
 */
xyz.swapee.wc.IExchangeStatusTitleFields.prototype.pinout = /** @type {!xyz.swapee.wc.IExchangeStatusTitle.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitle} */
xyz.swapee.wc.RecordIExchangeStatusTitle

/** @typedef {xyz.swapee.wc.IExchangeStatusTitle} xyz.swapee.wc.BoundIExchangeStatusTitle */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitle} xyz.swapee.wc.BoundExchangeStatusTitle */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleController.Inputs} xyz.swapee.wc.IExchangeStatusTitle.Pinout The input pins of the _IExchangeStatusTitle_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs>)} xyz.swapee.wc.IExchangeStatusTitleBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IExchangeStatusTitleBuffer
 */
xyz.swapee.wc.IExchangeStatusTitleBuffer = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusTitleBuffer.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitleBuffer

/**
 * A concrete class of _IExchangeStatusTitleBuffer_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleBuffer
 * @implements {xyz.swapee.wc.IExchangeStatusTitleBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.ExchangeStatusTitleBuffer = class extends xyz.swapee.wc.IExchangeStatusTitleBuffer { }
xyz.swapee.wc.ExchangeStatusTitleBuffer.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleBuffer

/**
 * Contains getters to cast the _IExchangeStatusTitle_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleCaster
 */
xyz.swapee.wc.IExchangeStatusTitleCaster = class { }
/**
 * Cast the _IExchangeStatusTitle_ instance into the _BoundIExchangeStatusTitle_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitle}
 */
xyz.swapee.wc.IExchangeStatusTitleCaster.prototype.asIExchangeStatusTitle
/**
 * Access the _ExchangeStatusTitle_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitle}
 */
xyz.swapee.wc.IExchangeStatusTitleCaster.prototype.superExchangeStatusTitle

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/110-ExchangeStatusTitleSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeStatusTitleMemoryPQs
 */
xyz.swapee.wc.ExchangeStatusTitleMemoryPQs = class {
  constructor() {
    /**
     * `jacb4`
     */
    this.status=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeStatusTitleMemoryPQs.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleMemoryQPs
 * @dict
 */
xyz.swapee.wc.ExchangeStatusTitleMemoryQPs = class { }
/**
 * `status`
 */
xyz.swapee.wc.ExchangeStatusTitleMemoryQPs.prototype.jacb4 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleMemoryPQs)} xyz.swapee.wc.ExchangeStatusTitleInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleMemoryPQs} xyz.swapee.wc.ExchangeStatusTitleMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeStatusTitleInputsPQs
 */
xyz.swapee.wc.ExchangeStatusTitleInputsPQs = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleInputsPQs.constructor&xyz.swapee.wc.ExchangeStatusTitleMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusTitleInputsPQs.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleInputsPQs

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleMemoryPQs)} xyz.swapee.wc.ExchangeStatusTitleInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleInputsQPs
 * @dict
 */
xyz.swapee.wc.ExchangeStatusTitleInputsQPs = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleInputsQPs.constructor&xyz.swapee.wc.ExchangeStatusTitleMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeStatusTitleInputsQPs.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeStatusTitleVdusPQs
 */
xyz.swapee.wc.ExchangeStatusTitleVdusPQs = class {
  constructor() {
    /**
     * `a2741`
     */
    this.AwaitingStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2742`
     */
    this.ConfirmingStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2743`
     */
    this.ExchangingStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2744`
     */
    this.SendingStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2745`
     */
    this.FinishedStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2746`
     */
    this.FailedStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2747`
     */
    this.RefundedStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2748`
     */
    this.OverdueStatusLa=/** @type {string} */ (void 0)
    /**
     * `a2749`
     */
    this.HoldStatusLa=/** @type {string} */ (void 0)
    /**
     * `a27410`
     */
    this.ExpiredStatusLa=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeStatusTitleVdusPQs.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleVdusQPs
 * @dict
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs = class { }
/**
 * `AwaitingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2741 = /** @type {string} */ (void 0)
/**
 * `ConfirmingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2742 = /** @type {string} */ (void 0)
/**
 * `ExchangingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2743 = /** @type {string} */ (void 0)
/**
 * `SendingStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2744 = /** @type {string} */ (void 0)
/**
 * `FinishedStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2745 = /** @type {string} */ (void 0)
/**
 * `FailedStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2746 = /** @type {string} */ (void 0)
/**
 * `RefundedStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2747 = /** @type {string} */ (void 0)
/**
 * `OverdueStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2748 = /** @type {string} */ (void 0)
/**
 * `HoldStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a2749 = /** @type {string} */ (void 0)
/**
 * `ExpiredStatusLa`
 */
xyz.swapee.wc.ExchangeStatusTitleVdusQPs.prototype.a27410 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeStatusTitleClassesPQs
 */
xyz.swapee.wc.ExchangeStatusTitleClassesPQs = class {
  constructor() {
    /**
     * `jbd81`
     */
    this.Class=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeStatusTitleClassesPQs.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleClassesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleClassesQPs
 * @dict
 */
xyz.swapee.wc.ExchangeStatusTitleClassesQPs = class { }
/**
 * `Class`
 */
xyz.swapee.wc.ExchangeStatusTitleClassesQPs.prototype.jbd81 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/12-IExchangeStatusTitleHtmlComponent.xml}  f5f3d3327c6b854a20317fa04bbe6c37 */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese&xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese&xyz.swapee.wc.IExchangeStatusTitle.Initialese&xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IExchangeStatusTitleProcessor.Initialese&xyz.swapee.wc.IExchangeStatusTitleComputer.Initialese} xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleHtmlComponent)} xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent} xyz.swapee.wc.ExchangeStatusTitleHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent
 */
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.constructor&xyz.swapee.wc.ExchangeStatusTitleHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent|typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent)|(!xyz.swapee.wc.back.IExchangeStatusTitleController|typeof xyz.swapee.wc.back.ExchangeStatusTitleController)|(!xyz.swapee.wc.back.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen)|(!xyz.swapee.wc.IExchangeStatusTitle|typeof xyz.swapee.wc.ExchangeStatusTitle)|(!xyz.swapee.wc.IExchangeStatusTitleGPU|typeof xyz.swapee.wc.ExchangeStatusTitleGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent|typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent)|(!xyz.swapee.wc.back.IExchangeStatusTitleController|typeof xyz.swapee.wc.back.ExchangeStatusTitleController)|(!xyz.swapee.wc.back.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen)|(!xyz.swapee.wc.IExchangeStatusTitle|typeof xyz.swapee.wc.ExchangeStatusTitle)|(!xyz.swapee.wc.IExchangeStatusTitleGPU|typeof xyz.swapee.wc.ExchangeStatusTitleGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent|typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent)|(!xyz.swapee.wc.back.IExchangeStatusTitleController|typeof xyz.swapee.wc.back.ExchangeStatusTitleController)|(!xyz.swapee.wc.back.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen)|(!xyz.swapee.wc.IExchangeStatusTitle|typeof xyz.swapee.wc.ExchangeStatusTitle)|(!xyz.swapee.wc.IExchangeStatusTitleGPU|typeof xyz.swapee.wc.ExchangeStatusTitleGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeStatusTitleProcessor|typeof xyz.swapee.wc.ExchangeStatusTitleProcessor)|(!xyz.swapee.wc.IExchangeStatusTitleComputer|typeof xyz.swapee.wc.ExchangeStatusTitleComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleHtmlComponent} xyz.swapee.wc.ExchangeStatusTitleHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleHtmlComponentCaster&xyz.swapee.wc.back.IExchangeStatusTitleController&xyz.swapee.wc.back.IExchangeStatusTitleScreen&xyz.swapee.wc.IExchangeStatusTitle&xyz.swapee.wc.IExchangeStatusTitleGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.IExchangeStatusTitleController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IExchangeStatusTitleProcessor&xyz.swapee.wc.IExchangeStatusTitleComputer)} xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusTitleController} xyz.swapee.wc.back.IExchangeStatusTitleController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusTitleScreen} xyz.swapee.wc.back.IExchangeStatusTitleScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleGPU} xyz.swapee.wc.IExchangeStatusTitleGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IExchangeStatusTitle_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IExchangeStatusTitleHtmlComponent
 */
xyz.swapee.wc.IExchangeStatusTitleHtmlComponent = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeStatusTitleController.typeof&xyz.swapee.wc.back.IExchangeStatusTitleScreen.typeof&xyz.swapee.wc.IExchangeStatusTitle.typeof&xyz.swapee.wc.IExchangeStatusTitleGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IExchangeStatusTitleProcessor.typeof&xyz.swapee.wc.IExchangeStatusTitleComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleHtmlComponent} xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.typeof */
/**
 * A concrete class of _IExchangeStatusTitleHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleHtmlComponent
 * @implements {xyz.swapee.wc.IExchangeStatusTitleHtmlComponent} The _IExchangeStatusTitle_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleHtmlComponent = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleHtmlComponent.constructor&xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleHtmlComponent}
 */
xyz.swapee.wc.ExchangeStatusTitleHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleHtmlComponent} */
xyz.swapee.wc.RecordIExchangeStatusTitleHtmlComponent

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleHtmlComponent} xyz.swapee.wc.BoundIExchangeStatusTitleHtmlComponent */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleHtmlComponent} xyz.swapee.wc.BoundExchangeStatusTitleHtmlComponent */

/**
 * Contains getters to cast the _IExchangeStatusTitleHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleHtmlComponentCaster
 */
xyz.swapee.wc.IExchangeStatusTitleHtmlComponentCaster = class { }
/**
 * Cast the _IExchangeStatusTitleHtmlComponent_ instance into the _BoundIExchangeStatusTitleHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleHtmlComponent}
 */
xyz.swapee.wc.IExchangeStatusTitleHtmlComponentCaster.prototype.asIExchangeStatusTitleHtmlComponent
/**
 * Access the _ExchangeStatusTitleHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleHtmlComponent}
 */
xyz.swapee.wc.IExchangeStatusTitleHtmlComponentCaster.prototype.superExchangeStatusTitleHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/130-IExchangeStatusTitleElement.xml}  868a49b59e744e7ca09f8b3bb181b636 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.IExchangeStatusTitleElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IExchangeStatusTitleElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleElement)} xyz.swapee.wc.AbstractExchangeStatusTitleElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleElement} xyz.swapee.wc.ExchangeStatusTitleElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleElement` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleElement
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElement = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleElement.constructor&xyz.swapee.wc.ExchangeStatusTitleElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleElement.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElement.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleElement|typeof xyz.swapee.wc.ExchangeStatusTitleElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElement}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElement}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleElement|typeof xyz.swapee.wc.ExchangeStatusTitleElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElement}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleElement|typeof xyz.swapee.wc.ExchangeStatusTitleElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElement}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleElement.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleElement} xyz.swapee.wc.ExchangeStatusTitleElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleElementFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.IExchangeStatusTitleElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.IExchangeStatusTitleElement.Inputs, null>)} xyz.swapee.wc.IExchangeStatusTitleElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IExchangeStatusTitle_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IExchangeStatusTitleElement
 */
xyz.swapee.wc.IExchangeStatusTitleElement = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleElement.solder} */
xyz.swapee.wc.IExchangeStatusTitleElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleElement.render} */
xyz.swapee.wc.IExchangeStatusTitleElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleElement.server} */
xyz.swapee.wc.IExchangeStatusTitleElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleElement.inducer} */
xyz.swapee.wc.IExchangeStatusTitleElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleElement&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleElement.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElement} xyz.swapee.wc.IExchangeStatusTitleElement.typeof */
/**
 * A concrete class of _IExchangeStatusTitleElement_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleElement
 * @implements {xyz.swapee.wc.IExchangeStatusTitleElement} A component description.
 *
 * The _IExchangeStatusTitle_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleElement.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleElement = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleElement.constructor&xyz.swapee.wc.IExchangeStatusTitleElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElement}
 */
xyz.swapee.wc.ExchangeStatusTitleElement.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleElement.
 * @interface xyz.swapee.wc.IExchangeStatusTitleElementFields
 */
xyz.swapee.wc.IExchangeStatusTitleElementFields = class { }
/**
 * The element-specific inputs to the _IExchangeStatusTitle_ component.
 */
xyz.swapee.wc.IExchangeStatusTitleElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusTitleElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElement} */
xyz.swapee.wc.RecordIExchangeStatusTitleElement

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElement} xyz.swapee.wc.BoundIExchangeStatusTitleElement */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleElement} xyz.swapee.wc.BoundExchangeStatusTitleElement */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitlePort.Inputs&xyz.swapee.wc.IExchangeStatusTitleDisplay.Queries&xyz.swapee.wc.IExchangeStatusTitleController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs} xyz.swapee.wc.IExchangeStatusTitleElement.Inputs The element-specific inputs to the _IExchangeStatusTitle_ component. */

/**
 * Contains getters to cast the _IExchangeStatusTitleElement_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleElementCaster
 */
xyz.swapee.wc.IExchangeStatusTitleElementCaster = class { }
/**
 * Cast the _IExchangeStatusTitleElement_ instance into the _BoundIExchangeStatusTitleElement_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleElement}
 */
xyz.swapee.wc.IExchangeStatusTitleElementCaster.prototype.asIExchangeStatusTitleElement
/**
 * Access the _ExchangeStatusTitleElement_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleElement}
 */
xyz.swapee.wc.IExchangeStatusTitleElementCaster.prototype.superExchangeStatusTitleElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ExchangeStatusTitleMemory, props: !xyz.swapee.wc.IExchangeStatusTitleElement.Inputs) => Object<string, *>} xyz.swapee.wc.IExchangeStatusTitleElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElement.__solder<!xyz.swapee.wc.IExchangeStatusTitleElement>} xyz.swapee.wc.IExchangeStatusTitleElement._solder */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.ExchangeStatusTitleMemory} model The model.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusTitleElement.Inputs} props The element props.
 * - `[status=null]` _&#42;?_ The status. ⤴ *IExchangeStatusTitleOuterCore.WeakModel.Status* ⤴ *IExchangeStatusTitleOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeStatusTitleElementPort.Inputs.NoSolder* Default `false`.
 * - `[awaitingStatusLaOpts]` _!Object?_ The options to pass to the _AwaitingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts* Default `{}`.
 * - `[confirmingStatusLaOpts]` _!Object?_ The options to pass to the _ConfirmingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts* Default `{}`.
 * - `[exchangingStatusLaOpts]` _!Object?_ The options to pass to the _ExchangingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts* Default `{}`.
 * - `[sendingStatusLaOpts]` _!Object?_ The options to pass to the _SendingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts* Default `{}`.
 * - `[finishedStatusLaOpts]` _!Object?_ The options to pass to the _FinishedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts* Default `{}`.
 * - `[failedStatusLaOpts]` _!Object?_ The options to pass to the _FailedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts* Default `{}`.
 * - `[refundedStatusLaOpts]` _!Object?_ The options to pass to the _RefundedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts* Default `{}`.
 * - `[overdueStatusLaOpts]` _!Object?_ The options to pass to the _OverdueStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts* Default `{}`.
 * - `[holdStatusLaOpts]` _!Object?_ The options to pass to the _HoldStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts* Default `{}`.
 * - `[expiredStatusLaOpts]` _!Object?_ The options to pass to the _ExpiredStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IExchangeStatusTitleElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeStatusTitleMemory, instance?: !xyz.swapee.wc.IExchangeStatusTitleScreen&xyz.swapee.wc.IExchangeStatusTitleController) => !engineering.type.VNode} xyz.swapee.wc.IExchangeStatusTitleElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElement.__render<!xyz.swapee.wc.IExchangeStatusTitleElement>} xyz.swapee.wc.IExchangeStatusTitleElement._render */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.ExchangeStatusTitleMemory} [model] The model for the view.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusTitleScreen&xyz.swapee.wc.IExchangeStatusTitleController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeStatusTitleElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeStatusTitleMemory, inputs: !xyz.swapee.wc.IExchangeStatusTitleElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IExchangeStatusTitleElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElement.__server<!xyz.swapee.wc.IExchangeStatusTitleElement>} xyz.swapee.wc.IExchangeStatusTitleElement._server */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.ExchangeStatusTitleMemory} memory The memory registers.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusTitleElement.Inputs} inputs The inputs to the port.
 * - `[status=null]` _&#42;?_ The status. ⤴ *IExchangeStatusTitleOuterCore.WeakModel.Status* ⤴ *IExchangeStatusTitleOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeStatusTitleElementPort.Inputs.NoSolder* Default `false`.
 * - `[awaitingStatusLaOpts]` _!Object?_ The options to pass to the _AwaitingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts* Default `{}`.
 * - `[confirmingStatusLaOpts]` _!Object?_ The options to pass to the _ConfirmingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts* Default `{}`.
 * - `[exchangingStatusLaOpts]` _!Object?_ The options to pass to the _ExchangingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts* Default `{}`.
 * - `[sendingStatusLaOpts]` _!Object?_ The options to pass to the _SendingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts* Default `{}`.
 * - `[finishedStatusLaOpts]` _!Object?_ The options to pass to the _FinishedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts* Default `{}`.
 * - `[failedStatusLaOpts]` _!Object?_ The options to pass to the _FailedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts* Default `{}`.
 * - `[refundedStatusLaOpts]` _!Object?_ The options to pass to the _RefundedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts* Default `{}`.
 * - `[overdueStatusLaOpts]` _!Object?_ The options to pass to the _OverdueStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts* Default `{}`.
 * - `[holdStatusLaOpts]` _!Object?_ The options to pass to the _HoldStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts* Default `{}`.
 * - `[expiredStatusLaOpts]` _!Object?_ The options to pass to the _ExpiredStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeStatusTitleElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeStatusTitleMemory, port?: !xyz.swapee.wc.IExchangeStatusTitleElement.Inputs) => ?} xyz.swapee.wc.IExchangeStatusTitleElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElement.__inducer<!xyz.swapee.wc.IExchangeStatusTitleElement>} xyz.swapee.wc.IExchangeStatusTitleElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.ExchangeStatusTitleMemory} [model] The model of the component into which to induce the state.
 * - `status` _string_ The status. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeStatusTitleElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[status=null]` _&#42;?_ The status. ⤴ *IExchangeStatusTitleOuterCore.WeakModel.Status* ⤴ *IExchangeStatusTitleOuterCore.WeakModel.Status* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeStatusTitleElementPort.Inputs.NoSolder* Default `false`.
 * - `[awaitingStatusLaOpts]` _!Object?_ The options to pass to the _AwaitingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts* Default `{}`.
 * - `[confirmingStatusLaOpts]` _!Object?_ The options to pass to the _ConfirmingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts* Default `{}`.
 * - `[exchangingStatusLaOpts]` _!Object?_ The options to pass to the _ExchangingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts* Default `{}`.
 * - `[sendingStatusLaOpts]` _!Object?_ The options to pass to the _SendingStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts* Default `{}`.
 * - `[finishedStatusLaOpts]` _!Object?_ The options to pass to the _FinishedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts* Default `{}`.
 * - `[failedStatusLaOpts]` _!Object?_ The options to pass to the _FailedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts* Default `{}`.
 * - `[refundedStatusLaOpts]` _!Object?_ The options to pass to the _RefundedStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts* Default `{}`.
 * - `[overdueStatusLaOpts]` _!Object?_ The options to pass to the _OverdueStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts* Default `{}`.
 * - `[holdStatusLaOpts]` _!Object?_ The options to pass to the _HoldStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts* Default `{}`.
 * - `[expiredStatusLaOpts]` _!Object?_ The options to pass to the _ExpiredStatusLa_ vdu. ⤴ *IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IExchangeStatusTitleElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusTitleElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/140-IExchangeStatusTitleElementPort.xml}  e6fe58f148420da8d4fa36c4932e53d3 */
/**
 * The options to pass to the _LoadingStripe_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.LoadingStripeOpts.loadingStripeOpts

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.LoadingStripeOpts The options to pass to the _LoadingStripe_ vdu (optional overlay).
 * @prop {!Object} [loadingStripeOpts] The options to pass to the _LoadingStripe_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.LoadingStripeOpts_Safe The options to pass to the _LoadingStripe_ vdu (required overlay).
 * @prop {!Object} loadingStripeOpts The options to pass to the _LoadingStripe_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.LoadingStripeOpts The options to pass to the _LoadingStripe_ vdu (optional overlay).
 * @prop {*} [loadingStripeOpts=null] The options to pass to the _LoadingStripe_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.LoadingStripeOpts_Safe The options to pass to the _LoadingStripe_ vdu (required overlay).
 * @prop {*} loadingStripeOpts The options to pass to the _LoadingStripe_ vdu.
 */

/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeStatusTitleElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleElementPort)} xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleElementPort} xyz.swapee.wc.ExchangeStatusTitleElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleElementPort
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.constructor&xyz.swapee.wc.ExchangeStatusTitleElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleElementPort|typeof xyz.swapee.wc.ExchangeStatusTitleElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleElementPort|typeof xyz.swapee.wc.ExchangeStatusTitleElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleElementPort|typeof xyz.swapee.wc.ExchangeStatusTitleElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElementPort}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleElementPort.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleElementPort} xyz.swapee.wc.ExchangeStatusTitleElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs>)} xyz.swapee.wc.IExchangeStatusTitleElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IExchangeStatusTitleElementPort
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleElementPort.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort} xyz.swapee.wc.IExchangeStatusTitleElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeStatusTitleElementPort_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleElementPort
 * @implements {xyz.swapee.wc.IExchangeStatusTitleElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleElementPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleElementPort = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleElementPort.constructor&xyz.swapee.wc.IExchangeStatusTitleElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleElementPort}
 */
xyz.swapee.wc.ExchangeStatusTitleElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleElementPort.
 * @interface xyz.swapee.wc.IExchangeStatusTitleElementPortFields
 */
xyz.swapee.wc.IExchangeStatusTitleElementPortFields = class { }
/**
 * The inputs to the _IExchangeStatusTitleElement_'s controller via its element port.
 */
xyz.swapee.wc.IExchangeStatusTitleElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeStatusTitleElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElementPort} */
xyz.swapee.wc.RecordIExchangeStatusTitleElementPort

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleElementPort} xyz.swapee.wc.BoundIExchangeStatusTitleElementPort */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleElementPort} xyz.swapee.wc.BoundExchangeStatusTitleElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _NewStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts.newStatusLaOpts

/**
 * The options to pass to the _AwaitingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts.awaitingStatusLaOpts

/**
 * The options to pass to the _ConfirmingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts.confirmingStatusLaOpts

/**
 * The options to pass to the _ExchangingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts.exchangingStatusLaOpts

/**
 * The options to pass to the _SendingStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts.sendingStatusLaOpts

/**
 * The options to pass to the _FinishedStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts.finishedStatusLaOpts

/**
 * The options to pass to the _FailedStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts.failedStatusLaOpts

/**
 * The options to pass to the _RefundedStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts.refundedStatusLaOpts

/**
 * The options to pass to the _OverdueStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts.overdueStatusLaOpts

/**
 * The options to pass to the _HoldStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts.holdStatusLaOpts

/**
 * The options to pass to the _ExpiredStatusLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts.expiredStatusLaOpts

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts)} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts.typeof */
/**
 * The inputs to the _IExchangeStatusTitleElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.constructor&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NewStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.AwaitingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ConfirmingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExchangingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.SendingStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FinishedStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FailedStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.RefundedStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.OverdueStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.HoldStatusLaOpts&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExpiredStatusLaOpts)} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NewStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NewStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.AwaitingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.AwaitingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ConfirmingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ConfirmingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExchangingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExchangingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.SendingStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.SendingStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FinishedStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FinishedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FailedStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FailedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.RefundedStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.RefundedStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.OverdueStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.OverdueStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.HoldStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.HoldStatusLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExpiredStatusLaOpts} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExpiredStatusLaOpts.typeof */
/**
 * The inputs to the _IExchangeStatusTitleElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs
 */
xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.constructor&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NewStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.AwaitingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ConfirmingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExchangingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.SendingStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FinishedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FailedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.RefundedStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.OverdueStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.HoldStatusLaOpts.typeof&xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExpiredStatusLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs

/**
 * Contains getters to cast the _IExchangeStatusTitleElementPort_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleElementPortCaster
 */
xyz.swapee.wc.IExchangeStatusTitleElementPortCaster = class { }
/**
 * Cast the _IExchangeStatusTitleElementPort_ instance into the _BoundIExchangeStatusTitleElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleElementPort}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPortCaster.prototype.asIExchangeStatusTitleElementPort
/**
 * Access the _ExchangeStatusTitleElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleElementPort}
 */
xyz.swapee.wc.IExchangeStatusTitleElementPortCaster.prototype.superExchangeStatusTitleElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts The options to pass to the _NewStatusLa_ vdu (optional overlay).
 * @prop {!Object} [newStatusLaOpts] The options to pass to the _NewStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.NewStatusLaOpts_Safe The options to pass to the _NewStatusLa_ vdu (required overlay).
 * @prop {!Object} newStatusLaOpts The options to pass to the _NewStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [awaitingStatusLaOpts] The options to pass to the _AwaitingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.AwaitingStatusLaOpts_Safe The options to pass to the _AwaitingStatusLa_ vdu (required overlay).
 * @prop {!Object} awaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [confirmingStatusLaOpts] The options to pass to the _ConfirmingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ConfirmingStatusLaOpts_Safe The options to pass to the _ConfirmingStatusLa_ vdu (required overlay).
 * @prop {!Object} confirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [exchangingStatusLaOpts] The options to pass to the _ExchangingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExchangingStatusLaOpts_Safe The options to pass to the _ExchangingStatusLa_ vdu (required overlay).
 * @prop {!Object} exchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu (optional overlay).
 * @prop {!Object} [sendingStatusLaOpts] The options to pass to the _SendingStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.SendingStatusLaOpts_Safe The options to pass to the _SendingStatusLa_ vdu (required overlay).
 * @prop {!Object} sendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu (optional overlay).
 * @prop {!Object} [finishedStatusLaOpts] The options to pass to the _FinishedStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FinishedStatusLaOpts_Safe The options to pass to the _FinishedStatusLa_ vdu (required overlay).
 * @prop {!Object} finishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu (optional overlay).
 * @prop {!Object} [failedStatusLaOpts] The options to pass to the _FailedStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.FailedStatusLaOpts_Safe The options to pass to the _FailedStatusLa_ vdu (required overlay).
 * @prop {!Object} failedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu (optional overlay).
 * @prop {!Object} [refundedStatusLaOpts] The options to pass to the _RefundedStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.RefundedStatusLaOpts_Safe The options to pass to the _RefundedStatusLa_ vdu (required overlay).
 * @prop {!Object} refundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu (optional overlay).
 * @prop {!Object} [overdueStatusLaOpts] The options to pass to the _OverdueStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.OverdueStatusLaOpts_Safe The options to pass to the _OverdueStatusLa_ vdu (required overlay).
 * @prop {!Object} overdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts The options to pass to the _HoldStatusLa_ vdu (optional overlay).
 * @prop {!Object} [holdStatusLaOpts] The options to pass to the _HoldStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.HoldStatusLaOpts_Safe The options to pass to the _HoldStatusLa_ vdu (required overlay).
 * @prop {!Object} holdStatusLaOpts The options to pass to the _HoldStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu (optional overlay).
 * @prop {!Object} [expiredStatusLaOpts] The options to pass to the _ExpiredStatusLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs.ExpiredStatusLaOpts_Safe The options to pass to the _ExpiredStatusLa_ vdu (required overlay).
 * @prop {!Object} expiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NewStatusLaOpts The options to pass to the _NewStatusLa_ vdu (optional overlay).
 * @prop {*} [newStatusLaOpts=null] The options to pass to the _NewStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.NewStatusLaOpts_Safe The options to pass to the _NewStatusLa_ vdu (required overlay).
 * @prop {*} newStatusLaOpts The options to pass to the _NewStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.AwaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu (optional overlay).
 * @prop {*} [awaitingStatusLaOpts=null] The options to pass to the _AwaitingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.AwaitingStatusLaOpts_Safe The options to pass to the _AwaitingStatusLa_ vdu (required overlay).
 * @prop {*} awaitingStatusLaOpts The options to pass to the _AwaitingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ConfirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu (optional overlay).
 * @prop {*} [confirmingStatusLaOpts=null] The options to pass to the _ConfirmingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ConfirmingStatusLaOpts_Safe The options to pass to the _ConfirmingStatusLa_ vdu (required overlay).
 * @prop {*} confirmingStatusLaOpts The options to pass to the _ConfirmingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu (optional overlay).
 * @prop {*} [exchangingStatusLaOpts=null] The options to pass to the _ExchangingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExchangingStatusLaOpts_Safe The options to pass to the _ExchangingStatusLa_ vdu (required overlay).
 * @prop {*} exchangingStatusLaOpts The options to pass to the _ExchangingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.SendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu (optional overlay).
 * @prop {*} [sendingStatusLaOpts=null] The options to pass to the _SendingStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.SendingStatusLaOpts_Safe The options to pass to the _SendingStatusLa_ vdu (required overlay).
 * @prop {*} sendingStatusLaOpts The options to pass to the _SendingStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FinishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu (optional overlay).
 * @prop {*} [finishedStatusLaOpts=null] The options to pass to the _FinishedStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FinishedStatusLaOpts_Safe The options to pass to the _FinishedStatusLa_ vdu (required overlay).
 * @prop {*} finishedStatusLaOpts The options to pass to the _FinishedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FailedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu (optional overlay).
 * @prop {*} [failedStatusLaOpts=null] The options to pass to the _FailedStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.FailedStatusLaOpts_Safe The options to pass to the _FailedStatusLa_ vdu (required overlay).
 * @prop {*} failedStatusLaOpts The options to pass to the _FailedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.RefundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu (optional overlay).
 * @prop {*} [refundedStatusLaOpts=null] The options to pass to the _RefundedStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.RefundedStatusLaOpts_Safe The options to pass to the _RefundedStatusLa_ vdu (required overlay).
 * @prop {*} refundedStatusLaOpts The options to pass to the _RefundedStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.OverdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu (optional overlay).
 * @prop {*} [overdueStatusLaOpts=null] The options to pass to the _OverdueStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.OverdueStatusLaOpts_Safe The options to pass to the _OverdueStatusLa_ vdu (required overlay).
 * @prop {*} overdueStatusLaOpts The options to pass to the _OverdueStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.HoldStatusLaOpts The options to pass to the _HoldStatusLa_ vdu (optional overlay).
 * @prop {*} [holdStatusLaOpts=null] The options to pass to the _HoldStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.HoldStatusLaOpts_Safe The options to pass to the _HoldStatusLa_ vdu (required overlay).
 * @prop {*} holdStatusLaOpts The options to pass to the _HoldStatusLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExpiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu (optional overlay).
 * @prop {*} [expiredStatusLaOpts=null] The options to pass to the _ExpiredStatusLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleElementPort.WeakInputs.ExpiredStatusLaOpts_Safe The options to pass to the _ExpiredStatusLa_ vdu (required overlay).
 * @prop {*} expiredStatusLaOpts The options to pass to the _ExpiredStatusLa_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/170-IExchangeStatusTitleDesigner.xml}  3d8d0295c818b3dedc0e1a5031b097aa */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IExchangeStatusTitleDesigner
 */
xyz.swapee.wc.IExchangeStatusTitleDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.ExchangeStatusTitleClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeStatusTitle />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeStatusTitleClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeStatusTitle />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeStatusTitleClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeStatusTitleDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeStatusTitle` _typeof IExchangeStatusTitleController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeStatusTitleDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeStatusTitle` _typeof IExchangeStatusTitleController_
   * - `This` _typeof IExchangeStatusTitleController_
   * @param {!xyz.swapee.wc.IExchangeStatusTitleDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ExchangeStatusTitle` _!ExchangeStatusTitleMemory_
   * - `This` _!ExchangeStatusTitleMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeStatusTitleClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeStatusTitleClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IExchangeStatusTitleDesigner.prototype.constructor = xyz.swapee.wc.IExchangeStatusTitleDesigner

/**
 * A concrete class of _IExchangeStatusTitleDesigner_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleDesigner
 * @implements {xyz.swapee.wc.IExchangeStatusTitleDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.ExchangeStatusTitleDesigner = class extends xyz.swapee.wc.IExchangeStatusTitleDesigner { }
xyz.swapee.wc.ExchangeStatusTitleDesigner.prototype.constructor = xyz.swapee.wc.ExchangeStatusTitleDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeStatusTitleController} ExchangeStatusTitle
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeStatusTitleController} ExchangeStatusTitle
 * @prop {typeof xyz.swapee.wc.IExchangeStatusTitleController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeStatusTitleDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.ExchangeStatusTitleMemory} ExchangeStatusTitle
 * @prop {!xyz.swapee.wc.ExchangeStatusTitleMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/40-IExchangeStatusTitleDisplay.xml}  b84f12d4a1ada995038158e05477223e */
/**
 * @typedef {Object} $xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese
 * @prop {HTMLSpanElement} [NewStatusLa]
 * @prop {HTMLSpanElement} [AwaitingStatusLa]
 * @prop {HTMLSpanElement} [ConfirmingStatusLa]
 * @prop {HTMLSpanElement} [ExchangingStatusLa]
 * @prop {HTMLSpanElement} [SendingStatusLa]
 * @prop {HTMLSpanElement} [FinishedStatusLa]
 * @prop {HTMLSpanElement} [FailedStatusLa]
 * @prop {HTMLSpanElement} [RefundedStatusLa]
 * @prop {HTMLSpanElement} [OverdueStatusLa]
 * @prop {HTMLSpanElement} [HoldStatusLa]
 * @prop {HTMLSpanElement} [ExpiredStatusLa]
 */
/** @typedef {$xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IExchangeStatusTitleDisplay.Settings>} xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleDisplay)} xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleDisplay} xyz.swapee.wc.ExchangeStatusTitleDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleDisplay
 */
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.constructor&xyz.swapee.wc.ExchangeStatusTitleDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.ExchangeStatusTitleDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.ExchangeStatusTitleDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.ExchangeStatusTitleDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleDisplay} xyz.swapee.wc.ExchangeStatusTitleDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.ExchangeStatusTitleMemory, !HTMLDivElement, !xyz.swapee.wc.IExchangeStatusTitleDisplay.Settings, xyz.swapee.wc.IExchangeStatusTitleDisplay.Queries, null>)} xyz.swapee.wc.IExchangeStatusTitleDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IExchangeStatusTitle_.
 * @interface xyz.swapee.wc.IExchangeStatusTitleDisplay
 */
xyz.swapee.wc.IExchangeStatusTitleDisplay = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleDisplay.paint} */
xyz.swapee.wc.IExchangeStatusTitleDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleDisplay} xyz.swapee.wc.IExchangeStatusTitleDisplay.typeof */
/**
 * A concrete class of _IExchangeStatusTitleDisplay_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleDisplay
 * @implements {xyz.swapee.wc.IExchangeStatusTitleDisplay} Display for presenting information from the _IExchangeStatusTitle_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleDisplay = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleDisplay.constructor&xyz.swapee.wc.IExchangeStatusTitleDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.ExchangeStatusTitleDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleDisplay.
 * @interface xyz.swapee.wc.IExchangeStatusTitleDisplayFields
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IExchangeStatusTitleDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IExchangeStatusTitleDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.NewStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.AwaitingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.ConfirmingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.ExchangingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.SendingStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.FinishedStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.FailedStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.RefundedStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.OverdueStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.HoldStatusLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayFields.prototype.ExpiredStatusLa = /** @type {HTMLSpanElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleDisplay} */
xyz.swapee.wc.RecordIExchangeStatusTitleDisplay

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleDisplay} xyz.swapee.wc.BoundIExchangeStatusTitleDisplay */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleDisplay} xyz.swapee.wc.BoundExchangeStatusTitleDisplay */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleDisplay.Queries} xyz.swapee.wc.IExchangeStatusTitleDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeStatusTitleDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IExchangeStatusTitleDisplay_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleDisplayCaster
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayCaster = class { }
/**
 * Cast the _IExchangeStatusTitleDisplay_ instance into the _BoundIExchangeStatusTitleDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayCaster.prototype.asIExchangeStatusTitleDisplay
/**
 * Cast the _IExchangeStatusTitleDisplay_ instance into the _BoundIExchangeStatusTitleScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleScreen}
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayCaster.prototype.asIExchangeStatusTitleScreen
/**
 * Access the _ExchangeStatusTitleDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.IExchangeStatusTitleDisplayCaster.prototype.superExchangeStatusTitleDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeStatusTitleMemory, land: null) => void} xyz.swapee.wc.IExchangeStatusTitleDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleDisplay.__paint<!xyz.swapee.wc.IExchangeStatusTitleDisplay>} xyz.swapee.wc.IExchangeStatusTitleDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeStatusTitleMemory} memory The display data.
 * - `status` _string_ The status. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusTitleDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusTitleDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/40-IExchangeStatusTitleDisplayBack.xml}  3b8e8099a21df792c801a04e346d3d0c */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IExchangeStatusTitleDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [NewStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [AwaitingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ConfirmingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [SendingStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [FinishedStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [FailedStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [RefundedStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [OverdueStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [HoldStatusLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ExpiredStatusLa]
 */
/** @typedef {$xyz.swapee.wc.back.IExchangeStatusTitleDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ExchangeStatusTitleClasses>} xyz.swapee.wc.back.IExchangeStatusTitleDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusTitleDisplay)} xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay} xyz.swapee.wc.back.ExchangeStatusTitleDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusTitleDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.constructor&xyz.swapee.wc.back.ExchangeStatusTitleDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusTitleDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.ExchangeStatusTitleClasses, null>)} xyz.swapee.wc.back.IExchangeStatusTitleDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleDisplay
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplay = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusTitleDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IExchangeStatusTitleDisplay.paint} */
xyz.swapee.wc.back.IExchangeStatusTitleDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusTitleDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleDisplay.Initialese>)} xyz.swapee.wc.back.ExchangeStatusTitleDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusTitleDisplay} xyz.swapee.wc.back.IExchangeStatusTitleDisplay.typeof */
/**
 * A concrete class of _IExchangeStatusTitleDisplay_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusTitleDisplay
 * @implements {xyz.swapee.wc.back.IExchangeStatusTitleDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusTitleDisplay = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusTitleDisplay.constructor&xyz.swapee.wc.back.IExchangeStatusTitleDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.ExchangeStatusTitleDisplay.prototype.constructor = xyz.swapee.wc.back.ExchangeStatusTitleDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.back.ExchangeStatusTitleDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleDisplay.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.NewStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.AwaitingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.ConfirmingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.ExchangingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.SendingStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.FinishedStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.FailedStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.RefundedStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.OverdueStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.HoldStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayFields.prototype.ExpiredStatusLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleDisplay} */
xyz.swapee.wc.back.RecordIExchangeStatusTitleDisplay

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleDisplay} xyz.swapee.wc.back.BoundIExchangeStatusTitleDisplay */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusTitleDisplay} xyz.swapee.wc.back.BoundExchangeStatusTitleDisplay */

/**
 * Contains getters to cast the _IExchangeStatusTitleDisplay_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleDisplayCaster
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayCaster = class { }
/**
 * Cast the _IExchangeStatusTitleDisplay_ instance into the _BoundIExchangeStatusTitleDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayCaster.prototype.asIExchangeStatusTitleDisplay
/**
 * Access the _ExchangeStatusTitleDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusTitleDisplay}
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplayCaster.prototype.superExchangeStatusTitleDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.ExchangeStatusTitleMemory, land?: null) => void} xyz.swapee.wc.back.IExchangeStatusTitleDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleDisplay.__paint<!xyz.swapee.wc.back.IExchangeStatusTitleDisplay>} xyz.swapee.wc.back.IExchangeStatusTitleDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusTitleDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeStatusTitleMemory} [memory] The display data.
 * - `status` _string_ The status. Default empty string.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IExchangeStatusTitleDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IExchangeStatusTitleDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/41-ExchangeStatusTitleClasses.xml}  f5147a4250ba79ff8646516ac0b56c6c */
/**
 * The classes of the _IExchangeStatusTitleDisplay_.
 * @record xyz.swapee.wc.ExchangeStatusTitleClasses
 */
xyz.swapee.wc.ExchangeStatusTitleClasses = class { }
/**
 *
 */
xyz.swapee.wc.ExchangeStatusTitleClasses.prototype.Class = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ExchangeStatusTitleClasses.prototype.props = /** @type {xyz.swapee.wc.ExchangeStatusTitleClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/50-IExchangeStatusTitleController.xml}  4bb6f63dbaa26207d5468f47cd4577b6 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs, !xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel>} xyz.swapee.wc.IExchangeStatusTitleController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleController)} xyz.swapee.wc.AbstractExchangeStatusTitleController.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleController} xyz.swapee.wc.ExchangeStatusTitleController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleController` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleController
 */
xyz.swapee.wc.AbstractExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleController.constructor&xyz.swapee.wc.ExchangeStatusTitleController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleController.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleController.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleController}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleController}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleController}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleController}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleController.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleController} xyz.swapee.wc.ExchangeStatusTitleControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs, !xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IExchangeStatusTitleOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs, !xyz.swapee.wc.IExchangeStatusTitleController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs, !xyz.swapee.wc.ExchangeStatusTitleMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs>)} xyz.swapee.wc.IExchangeStatusTitleController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IExchangeStatusTitleController
 */
xyz.swapee.wc.IExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeStatusTitleController.resetPort} */
xyz.swapee.wc.IExchangeStatusTitleController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleController&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleController.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleController.constructor */
/**
 * A concrete class of _IExchangeStatusTitleController_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleController
 * @implements {xyz.swapee.wc.IExchangeStatusTitleController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleController.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleController.constructor&xyz.swapee.wc.IExchangeStatusTitleController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleController}
 */
xyz.swapee.wc.ExchangeStatusTitleController.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleController.
 * @interface xyz.swapee.wc.IExchangeStatusTitleControllerFields
 */
xyz.swapee.wc.IExchangeStatusTitleControllerFields = class { }
/**
 * The inputs to the _IExchangeStatusTitle_'s controller.
 */
xyz.swapee.wc.IExchangeStatusTitleControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeStatusTitleController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IExchangeStatusTitleControllerFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeStatusTitleController} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleController} */
xyz.swapee.wc.RecordIExchangeStatusTitleController

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleController} xyz.swapee.wc.BoundIExchangeStatusTitleController */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleController} xyz.swapee.wc.BoundExchangeStatusTitleController */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitlePort.Inputs} xyz.swapee.wc.IExchangeStatusTitleController.Inputs The inputs to the _IExchangeStatusTitle_'s controller. */

/** @typedef {xyz.swapee.wc.IExchangeStatusTitlePort.WeakInputs} xyz.swapee.wc.IExchangeStatusTitleController.WeakInputs The inputs to the _IExchangeStatusTitle_'s controller. */

/**
 * Contains getters to cast the _IExchangeStatusTitleController_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleControllerCaster
 */
xyz.swapee.wc.IExchangeStatusTitleControllerCaster = class { }
/**
 * Cast the _IExchangeStatusTitleController_ instance into the _BoundIExchangeStatusTitleController_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleController}
 */
xyz.swapee.wc.IExchangeStatusTitleControllerCaster.prototype.asIExchangeStatusTitleController
/**
 * Cast the _IExchangeStatusTitleController_ instance into the _BoundIExchangeStatusTitleProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleProcessor}
 */
xyz.swapee.wc.IExchangeStatusTitleControllerCaster.prototype.asIExchangeStatusTitleProcessor
/**
 * Access the _ExchangeStatusTitleController_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleController}
 */
xyz.swapee.wc.IExchangeStatusTitleControllerCaster.prototype.superExchangeStatusTitleController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeStatusTitleController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeStatusTitleController.__resetPort<!xyz.swapee.wc.IExchangeStatusTitleController>} xyz.swapee.wc.IExchangeStatusTitleController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeStatusTitleController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeStatusTitleController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/51-IExchangeStatusTitleControllerFront.xml}  6d868e1e800fee7b14d54a28f61789fb */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IExchangeStatusTitleController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeStatusTitleController)} xyz.swapee.wc.front.AbstractExchangeStatusTitleController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeStatusTitleController} xyz.swapee.wc.front.ExchangeStatusTitleController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeStatusTitleController` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeStatusTitleController
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeStatusTitleController.constructor&xyz.swapee.wc.front.ExchangeStatusTitleController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeStatusTitleController.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeStatusTitleController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleController.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleController|typeof xyz.swapee.wc.front.ExchangeStatusTitleController)|(!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleController|typeof xyz.swapee.wc.front.ExchangeStatusTitleController)|(!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleController|typeof xyz.swapee.wc.front.ExchangeStatusTitleController)|(!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleController}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeStatusTitleController.Initialese[]) => xyz.swapee.wc.front.IExchangeStatusTitleController} xyz.swapee.wc.front.ExchangeStatusTitleControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeStatusTitleControllerCaster&xyz.swapee.wc.front.IExchangeStatusTitleControllerAT)} xyz.swapee.wc.front.IExchangeStatusTitleController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeStatusTitleControllerAT} xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IExchangeStatusTitleController
 */
xyz.swapee.wc.front.IExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.front.IExchangeStatusTitleController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeStatusTitleController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeStatusTitleController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusTitleController.Initialese>)} xyz.swapee.wc.front.ExchangeStatusTitleController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeStatusTitleController} xyz.swapee.wc.front.IExchangeStatusTitleController.typeof */
/**
 * A concrete class of _IExchangeStatusTitleController_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeStatusTitleController
 * @implements {xyz.swapee.wc.front.IExchangeStatusTitleController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusTitleController.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.front.ExchangeStatusTitleController.constructor&xyz.swapee.wc.front.IExchangeStatusTitleController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeStatusTitleController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleController}
 */
xyz.swapee.wc.front.ExchangeStatusTitleController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeStatusTitleController} */
xyz.swapee.wc.front.RecordIExchangeStatusTitleController

/** @typedef {xyz.swapee.wc.front.IExchangeStatusTitleController} xyz.swapee.wc.front.BoundIExchangeStatusTitleController */

/** @typedef {xyz.swapee.wc.front.ExchangeStatusTitleController} xyz.swapee.wc.front.BoundExchangeStatusTitleController */

/**
 * Contains getters to cast the _IExchangeStatusTitleController_ interface.
 * @interface xyz.swapee.wc.front.IExchangeStatusTitleControllerCaster
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerCaster = class { }
/**
 * Cast the _IExchangeStatusTitleController_ instance into the _BoundIExchangeStatusTitleController_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeStatusTitleController}
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerCaster.prototype.asIExchangeStatusTitleController
/**
 * Access the _ExchangeStatusTitleController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeStatusTitleController}
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerCaster.prototype.superExchangeStatusTitleController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/52-IExchangeStatusTitleControllerBack.xml}  595f1bfdb355a9939d5b216935050d9e */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs>&xyz.swapee.wc.IExchangeStatusTitleController.Initialese} xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusTitleController)} xyz.swapee.wc.back.AbstractExchangeStatusTitleController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusTitleController} xyz.swapee.wc.back.ExchangeStatusTitleController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusTitleController` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusTitleController
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusTitleController.constructor&xyz.swapee.wc.back.ExchangeStatusTitleController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusTitleController.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusTitleController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleController.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleController|typeof xyz.swapee.wc.back.ExchangeStatusTitleController)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleController|typeof xyz.swapee.wc.back.ExchangeStatusTitleController)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleController|typeof xyz.swapee.wc.back.ExchangeStatusTitleController)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleController}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusTitleController} xyz.swapee.wc.back.ExchangeStatusTitleControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusTitleControllerCaster&xyz.swapee.wc.IExchangeStatusTitleController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IExchangeStatusTitleController.Inputs>)} xyz.swapee.wc.back.IExchangeStatusTitleController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleController
 */
xyz.swapee.wc.back.IExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusTitleController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeStatusTitleController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusTitleController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusTitleController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese>)} xyz.swapee.wc.back.ExchangeStatusTitleController.constructor */
/**
 * A concrete class of _IExchangeStatusTitleController_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusTitleController
 * @implements {xyz.swapee.wc.back.IExchangeStatusTitleController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusTitleController = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusTitleController.constructor&xyz.swapee.wc.back.IExchangeStatusTitleController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusTitleController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleController}
 */
xyz.swapee.wc.back.ExchangeStatusTitleController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleController} */
xyz.swapee.wc.back.RecordIExchangeStatusTitleController

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleController} xyz.swapee.wc.back.BoundIExchangeStatusTitleController */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusTitleController} xyz.swapee.wc.back.BoundExchangeStatusTitleController */

/**
 * Contains getters to cast the _IExchangeStatusTitleController_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleControllerCaster
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerCaster = class { }
/**
 * Cast the _IExchangeStatusTitleController_ instance into the _BoundIExchangeStatusTitleController_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusTitleController}
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerCaster.prototype.asIExchangeStatusTitleController
/**
 * Access the _ExchangeStatusTitleController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusTitleController}
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerCaster.prototype.superExchangeStatusTitleController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/53-IExchangeStatusTitleControllerAR.xml}  9f396b924b3a678cbc8e9d72ef7d7d0f */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeStatusTitleController.Initialese} xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusTitleControllerAR)} xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR} xyz.swapee.wc.back.ExchangeStatusTitleControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusTitleControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.constructor&xyz.swapee.wc.back.ExchangeStatusTitleControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR|typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR|typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR|typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusTitleController|typeof xyz.swapee.wc.ExchangeStatusTitleController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusTitleControllerAR} xyz.swapee.wc.back.ExchangeStatusTitleControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusTitleControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeStatusTitleController)} xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeStatusTitleControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleControllerAR
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerAR = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeStatusTitleController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusTitleControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.Initialese>)} xyz.swapee.wc.back.ExchangeStatusTitleControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusTitleControllerAR} xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.typeof */
/**
 * A concrete class of _IExchangeStatusTitleControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusTitleControllerAR
 * @implements {xyz.swapee.wc.back.IExchangeStatusTitleControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeStatusTitleControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusTitleControllerAR = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusTitleControllerAR.constructor&xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusTitleControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleControllerAR}
 */
xyz.swapee.wc.back.ExchangeStatusTitleControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleControllerAR} */
xyz.swapee.wc.back.RecordIExchangeStatusTitleControllerAR

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleControllerAR} xyz.swapee.wc.back.BoundIExchangeStatusTitleControllerAR */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusTitleControllerAR} xyz.swapee.wc.back.BoundExchangeStatusTitleControllerAR */

/**
 * Contains getters to cast the _IExchangeStatusTitleControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleControllerARCaster
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerARCaster = class { }
/**
 * Cast the _IExchangeStatusTitleControllerAR_ instance into the _BoundIExchangeStatusTitleControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusTitleControllerAR}
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerARCaster.prototype.asIExchangeStatusTitleControllerAR
/**
 * Access the _ExchangeStatusTitleControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusTitleControllerAR}
 */
xyz.swapee.wc.back.IExchangeStatusTitleControllerARCaster.prototype.superExchangeStatusTitleControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/54-IExchangeStatusTitleControllerAT.xml}  0eff22a53f3fa7f55245048055994a95 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeStatusTitleControllerAT)} xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT} xyz.swapee.wc.front.ExchangeStatusTitleControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeStatusTitleControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.constructor&xyz.swapee.wc.front.ExchangeStatusTitleControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT|typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.Initialese[]) => xyz.swapee.wc.front.IExchangeStatusTitleControllerAT} xyz.swapee.wc.front.ExchangeStatusTitleControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeStatusTitleControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeStatusTitleControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IExchangeStatusTitleControllerAT
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerAT = class extends /** @type {xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeStatusTitleControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.Initialese>)} xyz.swapee.wc.front.ExchangeStatusTitleControllerAT.constructor */
/**
 * A concrete class of _IExchangeStatusTitleControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeStatusTitleControllerAT
 * @implements {xyz.swapee.wc.front.IExchangeStatusTitleControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeStatusTitleControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeStatusTitleControllerAT = class extends /** @type {xyz.swapee.wc.front.ExchangeStatusTitleControllerAT.constructor&xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeStatusTitleControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleControllerAT}
 */
xyz.swapee.wc.front.ExchangeStatusTitleControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeStatusTitleControllerAT} */
xyz.swapee.wc.front.RecordIExchangeStatusTitleControllerAT

/** @typedef {xyz.swapee.wc.front.IExchangeStatusTitleControllerAT} xyz.swapee.wc.front.BoundIExchangeStatusTitleControllerAT */

/** @typedef {xyz.swapee.wc.front.ExchangeStatusTitleControllerAT} xyz.swapee.wc.front.BoundExchangeStatusTitleControllerAT */

/**
 * Contains getters to cast the _IExchangeStatusTitleControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IExchangeStatusTitleControllerATCaster
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerATCaster = class { }
/**
 * Cast the _IExchangeStatusTitleControllerAT_ instance into the _BoundIExchangeStatusTitleControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeStatusTitleControllerAT}
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerATCaster.prototype.asIExchangeStatusTitleControllerAT
/**
 * Access the _ExchangeStatusTitleControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeStatusTitleControllerAT}
 */
xyz.swapee.wc.front.IExchangeStatusTitleControllerATCaster.prototype.superExchangeStatusTitleControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/70-IExchangeStatusTitleScreen.xml}  c3cc0a0ba8e368a9e9d67ef732d4ecbf */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.front.ExchangeStatusTitleInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeStatusTitleDisplay.Settings, !xyz.swapee.wc.IExchangeStatusTitleDisplay.Queries, null>&xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese} xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleScreen)} xyz.swapee.wc.AbstractExchangeStatusTitleScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleScreen} xyz.swapee.wc.ExchangeStatusTitleScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleScreen` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleScreen
 */
xyz.swapee.wc.AbstractExchangeStatusTitleScreen = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleScreen.constructor&xyz.swapee.wc.ExchangeStatusTitleScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleScreen.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleScreen.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.ExchangeStatusTitleScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeStatusTitleController|typeof xyz.swapee.wc.front.ExchangeStatusTitleController)|(!xyz.swapee.wc.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.ExchangeStatusTitleDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.ExchangeStatusTitleScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeStatusTitleController|typeof xyz.swapee.wc.front.ExchangeStatusTitleController)|(!xyz.swapee.wc.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.ExchangeStatusTitleDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.ExchangeStatusTitleScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeStatusTitleController|typeof xyz.swapee.wc.front.ExchangeStatusTitleController)|(!xyz.swapee.wc.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.ExchangeStatusTitleDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleScreen} xyz.swapee.wc.ExchangeStatusTitleScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.ExchangeStatusTitleMemory, !xyz.swapee.wc.front.ExchangeStatusTitleInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeStatusTitleDisplay.Settings, !xyz.swapee.wc.IExchangeStatusTitleDisplay.Queries, null, null>&xyz.swapee.wc.front.IExchangeStatusTitleController&xyz.swapee.wc.IExchangeStatusTitleDisplay)} xyz.swapee.wc.IExchangeStatusTitleScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IExchangeStatusTitleScreen
 */
xyz.swapee.wc.IExchangeStatusTitleScreen = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IExchangeStatusTitleController.typeof&xyz.swapee.wc.IExchangeStatusTitleDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeStatusTitleScreen} xyz.swapee.wc.IExchangeStatusTitleScreen.typeof */
/**
 * A concrete class of _IExchangeStatusTitleScreen_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleScreen
 * @implements {xyz.swapee.wc.IExchangeStatusTitleScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleScreen = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleScreen.constructor&xyz.swapee.wc.IExchangeStatusTitleScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.ExchangeStatusTitleScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleScreen} */
xyz.swapee.wc.RecordIExchangeStatusTitleScreen

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleScreen} xyz.swapee.wc.BoundIExchangeStatusTitleScreen */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleScreen} xyz.swapee.wc.BoundExchangeStatusTitleScreen */

/**
 * Contains getters to cast the _IExchangeStatusTitleScreen_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleScreenCaster
 */
xyz.swapee.wc.IExchangeStatusTitleScreenCaster = class { }
/**
 * Cast the _IExchangeStatusTitleScreen_ instance into the _BoundIExchangeStatusTitleScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleScreen}
 */
xyz.swapee.wc.IExchangeStatusTitleScreenCaster.prototype.asIExchangeStatusTitleScreen
/**
 * Access the _ExchangeStatusTitleScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleScreen}
 */
xyz.swapee.wc.IExchangeStatusTitleScreenCaster.prototype.superExchangeStatusTitleScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/70-IExchangeStatusTitleScreenBack.xml}  f229bfc92a9694881912c2238b8fc02c */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese} xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusTitleScreen)} xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen} xyz.swapee.wc.back.ExchangeStatusTitleScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusTitleScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.constructor&xyz.swapee.wc.back.ExchangeStatusTitleScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen)|(!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen)|(!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen)|(!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusTitleScreen} xyz.swapee.wc.back.ExchangeStatusTitleScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusTitleScreenCaster&xyz.swapee.wc.back.IExchangeStatusTitleScreenAT)} xyz.swapee.wc.back.IExchangeStatusTitleScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeStatusTitleScreenAT} xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleScreen
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreen = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusTitleScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusTitleScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese>)} xyz.swapee.wc.back.ExchangeStatusTitleScreen.constructor */
/**
 * A concrete class of _IExchangeStatusTitleScreen_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusTitleScreen
 * @implements {xyz.swapee.wc.back.IExchangeStatusTitleScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusTitleScreen = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusTitleScreen.constructor&xyz.swapee.wc.back.IExchangeStatusTitleScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusTitleScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreen}
 */
xyz.swapee.wc.back.ExchangeStatusTitleScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleScreen} */
xyz.swapee.wc.back.RecordIExchangeStatusTitleScreen

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleScreen} xyz.swapee.wc.back.BoundIExchangeStatusTitleScreen */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusTitleScreen} xyz.swapee.wc.back.BoundExchangeStatusTitleScreen */

/**
 * Contains getters to cast the _IExchangeStatusTitleScreen_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleScreenCaster
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenCaster = class { }
/**
 * Cast the _IExchangeStatusTitleScreen_ instance into the _BoundIExchangeStatusTitleScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusTitleScreen}
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenCaster.prototype.asIExchangeStatusTitleScreen
/**
 * Access the _ExchangeStatusTitleScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusTitleScreen}
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenCaster.prototype.superExchangeStatusTitleScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/73-IExchangeStatusTitleScreenAR.xml}  efcf34047a4855894a6576593cfeac98 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeStatusTitleScreen.Initialese} xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeStatusTitleScreenAR)} xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR} xyz.swapee.wc.front.ExchangeStatusTitleScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeStatusTitleScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.constructor&xyz.swapee.wc.front.ExchangeStatusTitleScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR|typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.ExchangeStatusTitleScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR|typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.ExchangeStatusTitleScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR|typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeStatusTitleScreen|typeof xyz.swapee.wc.ExchangeStatusTitleScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.Initialese[]) => xyz.swapee.wc.front.IExchangeStatusTitleScreenAR} xyz.swapee.wc.front.ExchangeStatusTitleScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeStatusTitleScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeStatusTitleScreen)} xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeStatusTitleScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IExchangeStatusTitleScreenAR
 */
xyz.swapee.wc.front.IExchangeStatusTitleScreenAR = class extends /** @type {xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeStatusTitleScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeStatusTitleScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.Initialese>)} xyz.swapee.wc.front.ExchangeStatusTitleScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeStatusTitleScreenAR} xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.typeof */
/**
 * A concrete class of _IExchangeStatusTitleScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeStatusTitleScreenAR
 * @implements {xyz.swapee.wc.front.IExchangeStatusTitleScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeStatusTitleScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeStatusTitleScreenAR = class extends /** @type {xyz.swapee.wc.front.ExchangeStatusTitleScreenAR.constructor&xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeStatusTitleScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeStatusTitleScreenAR}
 */
xyz.swapee.wc.front.ExchangeStatusTitleScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeStatusTitleScreenAR} */
xyz.swapee.wc.front.RecordIExchangeStatusTitleScreenAR

/** @typedef {xyz.swapee.wc.front.IExchangeStatusTitleScreenAR} xyz.swapee.wc.front.BoundIExchangeStatusTitleScreenAR */

/** @typedef {xyz.swapee.wc.front.ExchangeStatusTitleScreenAR} xyz.swapee.wc.front.BoundExchangeStatusTitleScreenAR */

/**
 * Contains getters to cast the _IExchangeStatusTitleScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IExchangeStatusTitleScreenARCaster
 */
xyz.swapee.wc.front.IExchangeStatusTitleScreenARCaster = class { }
/**
 * Cast the _IExchangeStatusTitleScreenAR_ instance into the _BoundIExchangeStatusTitleScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeStatusTitleScreenAR}
 */
xyz.swapee.wc.front.IExchangeStatusTitleScreenARCaster.prototype.asIExchangeStatusTitleScreenAR
/**
 * Access the _ExchangeStatusTitleScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeStatusTitleScreenAR}
 */
xyz.swapee.wc.front.IExchangeStatusTitleScreenARCaster.prototype.superExchangeStatusTitleScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/74-IExchangeStatusTitleScreenAT.xml}  ccdc065b4a9406740920ffd631d028da */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeStatusTitleScreenAT)} xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT} xyz.swapee.wc.back.ExchangeStatusTitleScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeStatusTitleScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.constructor&xyz.swapee.wc.back.ExchangeStatusTitleScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT|typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese[]) => xyz.swapee.wc.back.IExchangeStatusTitleScreenAT} xyz.swapee.wc.back.ExchangeStatusTitleScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeStatusTitleScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeStatusTitleScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleScreenAT
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenAT = class extends /** @type {xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeStatusTitleScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese>)} xyz.swapee.wc.back.ExchangeStatusTitleScreenAT.constructor */
/**
 * A concrete class of _IExchangeStatusTitleScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeStatusTitleScreenAT
 * @implements {xyz.swapee.wc.back.IExchangeStatusTitleScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeStatusTitleScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeStatusTitleScreenAT = class extends /** @type {xyz.swapee.wc.back.ExchangeStatusTitleScreenAT.constructor&xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeStatusTitleScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeStatusTitleScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeStatusTitleScreenAT}
 */
xyz.swapee.wc.back.ExchangeStatusTitleScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleScreenAT} */
xyz.swapee.wc.back.RecordIExchangeStatusTitleScreenAT

/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleScreenAT} xyz.swapee.wc.back.BoundIExchangeStatusTitleScreenAT */

/** @typedef {xyz.swapee.wc.back.ExchangeStatusTitleScreenAT} xyz.swapee.wc.back.BoundExchangeStatusTitleScreenAT */

/**
 * Contains getters to cast the _IExchangeStatusTitleScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IExchangeStatusTitleScreenATCaster
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenATCaster = class { }
/**
 * Cast the _IExchangeStatusTitleScreenAT_ instance into the _BoundIExchangeStatusTitleScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeStatusTitleScreenAT}
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenATCaster.prototype.asIExchangeStatusTitleScreenAT
/**
 * Access the _ExchangeStatusTitleScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeStatusTitleScreenAT}
 */
xyz.swapee.wc.back.IExchangeStatusTitleScreenATCaster.prototype.superExchangeStatusTitleScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-status-title/ExchangeStatusTitle.mvc/design/80-IExchangeStatusTitleGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IExchangeStatusTitleDisplay.Initialese} xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeStatusTitleGPU)} xyz.swapee.wc.AbstractExchangeStatusTitleGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeStatusTitleGPU} xyz.swapee.wc.ExchangeStatusTitleGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleGPU` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeStatusTitleGPU
 */
xyz.swapee.wc.AbstractExchangeStatusTitleGPU = class extends /** @type {xyz.swapee.wc.AbstractExchangeStatusTitleGPU.constructor&xyz.swapee.wc.ExchangeStatusTitleGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeStatusTitleGPU.prototype.constructor = xyz.swapee.wc.AbstractExchangeStatusTitleGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeStatusTitleGPU.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleGPU|typeof xyz.swapee.wc.ExchangeStatusTitleGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeStatusTitleGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeStatusTitleGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleGPU|typeof xyz.swapee.wc.ExchangeStatusTitleGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeStatusTitleGPU|typeof xyz.swapee.wc.ExchangeStatusTitleGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeStatusTitleDisplay|typeof xyz.swapee.wc.back.ExchangeStatusTitleDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleGPU}
 */
xyz.swapee.wc.AbstractExchangeStatusTitleGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese[]) => xyz.swapee.wc.IExchangeStatusTitleGPU} xyz.swapee.wc.ExchangeStatusTitleGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeStatusTitleGPUCaster&com.webcircuits.IBrowserView<.!ExchangeStatusTitleMemory,>&xyz.swapee.wc.back.IExchangeStatusTitleDisplay)} xyz.swapee.wc.IExchangeStatusTitleGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!ExchangeStatusTitleMemory,>} com.webcircuits.IBrowserView<.!ExchangeStatusTitleMemory,>.typeof */
/**
 * Handles the periphery of the _IExchangeStatusTitleDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IExchangeStatusTitleGPU
 */
xyz.swapee.wc.IExchangeStatusTitleGPU = class extends /** @type {xyz.swapee.wc.IExchangeStatusTitleGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!ExchangeStatusTitleMemory,>.typeof&xyz.swapee.wc.back.IExchangeStatusTitleDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeStatusTitleGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeStatusTitleGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeStatusTitleGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese>)} xyz.swapee.wc.ExchangeStatusTitleGPU.constructor */
/**
 * A concrete class of _IExchangeStatusTitleGPU_ instances.
 * @constructor xyz.swapee.wc.ExchangeStatusTitleGPU
 * @implements {xyz.swapee.wc.IExchangeStatusTitleGPU} Handles the periphery of the _IExchangeStatusTitleDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeStatusTitleGPU = class extends /** @type {xyz.swapee.wc.ExchangeStatusTitleGPU.constructor&xyz.swapee.wc.IExchangeStatusTitleGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeStatusTitleGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeStatusTitleGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeStatusTitleGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeStatusTitleGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeStatusTitleGPU}
 */
xyz.swapee.wc.ExchangeStatusTitleGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeStatusTitleGPU.
 * @interface xyz.swapee.wc.IExchangeStatusTitleGPUFields
 */
xyz.swapee.wc.IExchangeStatusTitleGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IExchangeStatusTitleGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleGPU} */
xyz.swapee.wc.RecordIExchangeStatusTitleGPU

/** @typedef {xyz.swapee.wc.IExchangeStatusTitleGPU} xyz.swapee.wc.BoundIExchangeStatusTitleGPU */

/** @typedef {xyz.swapee.wc.ExchangeStatusTitleGPU} xyz.swapee.wc.BoundExchangeStatusTitleGPU */

/**
 * Contains getters to cast the _IExchangeStatusTitleGPU_ interface.
 * @interface xyz.swapee.wc.IExchangeStatusTitleGPUCaster
 */
xyz.swapee.wc.IExchangeStatusTitleGPUCaster = class { }
/**
 * Cast the _IExchangeStatusTitleGPU_ instance into the _BoundIExchangeStatusTitleGPU_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeStatusTitleGPU}
 */
xyz.swapee.wc.IExchangeStatusTitleGPUCaster.prototype.asIExchangeStatusTitleGPU
/**
 * Access the _ExchangeStatusTitleGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeStatusTitleGPU}
 */
xyz.swapee.wc.IExchangeStatusTitleGPUCaster.prototype.superExchangeStatusTitleGPU

// nss:xyz.swapee.wc
/* @typal-end */