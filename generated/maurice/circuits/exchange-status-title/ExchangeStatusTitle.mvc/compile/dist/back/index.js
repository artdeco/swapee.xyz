/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitle` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitle}
 */
class AbstractExchangeStatusTitle extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeStatusTitle_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeStatusTitlePort}
 */
class ExchangeStatusTitlePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleController}
 */
class AbstractExchangeStatusTitleController extends (class {/* lazy-loaded */}) {}
/**
 * The _IExchangeStatusTitle_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ExchangeStatusTitleHtmlComponent}
 */
class ExchangeStatusTitleHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeStatusTitleBuffer}
 */
class ExchangeStatusTitleBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleComputer}
 */
class AbstractExchangeStatusTitleComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ExchangeStatusTitleController}
 */
class ExchangeStatusTitleController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeStatusTitle = AbstractExchangeStatusTitle
module.exports.ExchangeStatusTitlePort = ExchangeStatusTitlePort
module.exports.AbstractExchangeStatusTitleController = AbstractExchangeStatusTitleController
module.exports.ExchangeStatusTitleHtmlComponent = ExchangeStatusTitleHtmlComponent
module.exports.ExchangeStatusTitleBuffer = ExchangeStatusTitleBuffer
module.exports.AbstractExchangeStatusTitleComputer = AbstractExchangeStatusTitleComputer
module.exports.ExchangeStatusTitleController = ExchangeStatusTitleController