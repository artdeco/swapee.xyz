/**
 * Display for presenting information from the _IExchangeStatusTitle_.
 * @extends {xyz.swapee.wc.ExchangeStatusTitleDisplay}
 */
class ExchangeStatusTitleDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ExchangeStatusTitleScreen}
 */
class ExchangeStatusTitleScreen extends (class {/* lazy-loaded */}) {}

module.exports.ExchangeStatusTitleDisplay = ExchangeStatusTitleDisplay
module.exports.ExchangeStatusTitleScreen = ExchangeStatusTitleScreen