var f="depack-remove-start",g=f;try{if(f)throw Error();Object.setPrototypeOf(g,g);g.L=new WeakMap;g.map=new Map;g.set=new Set;Object.getOwnPropertySymbols({});Object.getOwnPropertyDescriptors({});f.includes("");[].keys();Object.values({});Object.assign({},{})}catch(b){}f="depack-remove-end";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const k=h["37270038985"],l=h["372700389810"],m=h["372700389811"];function w(b,c,d,e){return h["372700389812"](b,c,d,e,!1,void 0)};/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const x=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();/*

@LICENSE @webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const y=x["61893096584"],z=x["61893096586"],A=x["618930965811"],B=x["618930965812"],C=x["618930965815"];function D(){}D.prototype={};function E(){this.j=this.o=this.u=this.v=this.l=this.m=this.A=this.i=this.h=this.g=this.s=null}class F{}class G extends w(F,783779980716,E,{F:1,I:2}){}
G[m]=[D,y,function(){}.prototype={constructor(){k(this,()=>{this.scan({})})},scan:function(){const {element:b,C:{vdusPQs:{s:c,g:d,h:e,i:n,A:p,m:q,l:r,v:t,u,o:v,j:Q}}}=this,a=C(b);Object.assign(this,{s:a[c],g:a[d],h:a[e],i:a[n],A:a[p],m:a[q],l:a[r],v:a[t],u:a[u],o:a[v],j:a[Q]})}},{[l]:{s:1,g:1,h:1,i:1,A:1,m:1,l:1,v:1,u:1,o:1,j:1},initializer({s:b,g:c,h:d,i:e,A:n,m:p,l:q,v:r,u:t,o:u,j:v}){void 0!==b&&(this.s=b);void 0!==c&&(this.g=c);void 0!==d&&(this.h=d);void 0!==e&&(this.i=e);void 0!==n&&(this.A=
n);void 0!==p&&(this.m=p);void 0!==q&&(this.l=q);void 0!==r&&(this.v=r);void 0!==t&&(this.u=t);void 0!==u&&(this.o=u);void 0!==v&&(this.j=v)}}];var H=class extends G.implements(){};function I(){};function J(){}J.prototype={};class K{}class L extends w(K,783779980724,null,{D:1,H:2}){}L[m]=[J,A,function(){}.prototype={}];function M(){}M.prototype={};class N{}class O extends w(N,783779980727,null,{G:1,K:2}){}O[m]=[M,B,function(){}.prototype={allocator(){this.methods={}}}];const P={status:"9acb4"};const R={...P};const S=Object.keys(P).reduce((b,c)=>{b[P[c]]=c;return b},{});function T(){}T.prototype={};class U{}class V extends w(U,783779980725,null,{C:1,J:2}){}function W(){}V[m]=[T,W.prototype={inputsPQs:R,memoryQPs:S},z,O,W.prototype={vdusPQs:{g:"a2741",h:"a2742",i:"a2743",A:"a2744",m:"a2745",l:"a2746",v:"a2747",u:"a2748",o:"a2749",j:"a27410",s:"a27411"}}];var X=class extends V.implements(L,H,{get queries(){return this.settings}},{deduceInputs:I,__$id:7837799807}){};module.exports["783779980741"]=H;module.exports["783779980771"]=X;

//# sourceMappingURL=internal.js.map