import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {7837799807} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const b=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=b["372700389811"];function d(a,g,l,m){return b["372700389812"](a,g,l,m,!1,void 0)};function e(){}e.prototype={};class f{}class h extends d(f,78377998078,null,{v:1,aa:2}){}h[c]=[e];

const k=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const n=k["615055805212"],p=k["615055805218"],q=k["615055805233"];const r={status:"9acb4"};function t(){}t.prototype={};class u{}class v extends d(u,78377998077,null,{l:1,W:2}){}function w(){}w.prototype={};function x(){this.model={status:""}}class y{}class z extends d(y,78377998073,x,{s:1,Z:2}){}z[c]=[w,{constructor(){q(this.model,"",r)}}];v[c]=[{},t,z];
const A=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const B=A.IntegratedComponentInitialiser,C=A.IntegratedComponent,D=A["95173443851"];function E(){}E.prototype={};class F{}class G extends d(F,78377998071,null,{i:1,U:2}){}G[c]=[E,p];const H={regulate:n({status:String})};
const I=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const aa=I.IntegratedController,ba=I.Parametric;const J={...r};function K(){}K.prototype={};function ca(){const a={model:null};x.call(a);this.inputs=a.model}class da{}class L extends d(da,78377998075,ca,{u:1,$:2}){}function M(){}L[c]=[M.prototype={},K,ba,M.prototype={constructor(){q(this.inputs,"",J)}}];function N(){}N.prototype={};class ea{}class O extends d(ea,783779980720,null,{j:1,V:2}){}O[c]=[{},N,H,aa,{get Port(){return L}}];function P(){}P.prototype={};class fa{}class Q extends d(fa,78377998079,null,{g:1,T:2}){}Q[c]=[P,v,h,C,G,O];function ha(){return{}};require(eval('"@type.engineering/web-computing"'));const ia=require(eval('"@type.engineering/web-computing"')).h;function ja(){return ia("div",{$id:"ExchangeStatusTitle"})};require(eval('"@type.engineering/web-computing"'));const R=require(eval('"@type.engineering/web-computing"')).h;
function ka({status:a}){return R("div",{$id:"ExchangeStatusTitle"},R("span",{$id:"NewStatusLa",$reveal:"new"==a}),R("span",{$id:"AwaitingStatusLa",$reveal:"waiting"==a}),R("span",{$id:"ConfirmingStatusLa",$reveal:"confirming"==a}),R("span",{$id:"ExchangingStatusLa",$reveal:"exchanging"==a}),R("span",{$id:"SendingStatusLa",$reveal:"sending"==a}),R("span",{$id:"FinishedStatusLa",$reveal:"finished"==a}),R("span",{$id:"FailedStatusLa",$reveal:"failed"==a}),R("span",{$id:"RefundedStatusLa",$reveal:"refunded"==
a}),R("span",{$id:"OverdueStatusLa",$reveal:"overdue"==a}),R("span",{$id:"HoldStatusLa",$reveal:"hold"==a}),R("span",{$id:"ExpiredStatusLa",$reveal:"expired"==a}))};var S=class extends O.implements(){};require("https");require("http");const la=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}la("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const ma=T.ElementBase,na=T.HTMLBlocker;require(eval('"@type.engineering/web-computing"'));const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function oa(){this.inputs={noSolder:!1,K:{},A:{},D:{},F:{},R:{},I:{},H:{},P:{},M:{},J:{},G:{}}}class pa{}class W extends d(pa,783779980713,oa,{o:1,Y:2}){}
W[c]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"new-status-la-opts":void 0,"awaiting-status-la-opts":void 0,"confirming-status-la-opts":void 0,"exchanging-status-la-opts":void 0,"sending-status-la-opts":void 0,"finished-status-la-opts":void 0,"failed-status-la-opts":void 0,"refunded-status-la-opts":void 0,"overdue-status-la-opts":void 0,"hold-status-la-opts":void 0,"expired-status-la-opts":void 0})}}];function X(){}X.prototype={};class qa{}class Y extends d(qa,783779980712,null,{m:1,X:2}){}function Z(){}
Y[c]=[X,ma,Z.prototype={calibrate:function({":no-solder":a,":status":g}){const {attributes:{"no-solder":l,status:m}}=this;return{...(void 0===l?{"no-solder":a}:{}),...(void 0===m?{status:g}:{})}}},Z.prototype={calibrate:({"no-solder":a,status:g})=>({noSolder:a,status:g})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return U("div",{$id:"ExchangeStatusTitle"},U("vdu",{$id:"NewStatusLa"}),U("vdu",{$id:"AwaitingStatusLa"}),U("vdu",{$id:"ConfirmingStatusLa"}),U("vdu",
{$id:"ExchangingStatusLa"}),U("vdu",{$id:"SendingStatusLa"}),U("vdu",{$id:"FinishedStatusLa"}),U("vdu",{$id:"FailedStatusLa"}),U("vdu",{$id:"RefundedStatusLa"}),U("vdu",{$id:"OverdueStatusLa"}),U("vdu",{$id:"HoldStatusLa"}),U("vdu",{$id:"ExpiredStatusLa"}))}},Z.prototype={classes:{Class:"9bd81"},inputsPQs:J,vdus:{AwaitingStatusLa:"a2741",ConfirmingStatusLa:"a2742",ExchangingStatusLa:"a2743",SendingStatusLa:"a2744",FinishedStatusLa:"a2745",FailedStatusLa:"a2746",RefundedStatusLa:"a2747",OverdueStatusLa:"a2748",
HoldStatusLa:"a2749",ExpiredStatusLa:"a27410",NewStatusLa:"a27411"}},C,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder status no-solder :no-solder :status fe646 992ee 54ab8 f0fe5 8544f e86ed d736e 860a0 697a8 0c832 e5575 e2820 9acb4 children".split(" "))})},get Port(){return W}}];Y[c]=[Q,{rootId:"ExchangeStatusTitle",__$id:7837799807,fqn:"xyz.swapee.wc.IExchangeStatusTitle",maurice_element_v3:!0}];class ra extends Y.implements(S,D,na,B,{solder:ha,server:ja,render:ka},{classesMap:!0,rootSelector:".ExchangeStatusTitle",stylesheet:"html/styles/ExchangeStatusTitle.css",blockName:"html/ExchangeStatusTitleBlock.html"}){};module.exports["78377998070"]=Q;module.exports["78377998071"]=Q;module.exports["78377998073"]=L;module.exports["78377998074"]=O;module.exports["78377998078"]=ra;module.exports["783779980711"]=H;module.exports["783779980730"]=G;module.exports["783779980761"]=S;
/*! @embed-object-end {7837799807} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule