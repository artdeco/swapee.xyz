import Module from './element'

/**@extends {xyz.swapee.wc.AbstractExchangeStatusTitle}*/
export class AbstractExchangeStatusTitle extends Module['78377998071'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitle} */
AbstractExchangeStatusTitle.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeStatusTitlePort} */
export const ExchangeStatusTitlePort=Module['78377998073']
/**@extends {xyz.swapee.wc.AbstractExchangeStatusTitleController}*/
export class AbstractExchangeStatusTitleController extends Module['78377998074'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleController} */
AbstractExchangeStatusTitleController.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeStatusTitleElement} */
export const ExchangeStatusTitleElement=Module['78377998078']
/** @type {typeof xyz.swapee.wc.ExchangeStatusTitleBuffer} */
export const ExchangeStatusTitleBuffer=Module['783779980711']
/**@extends {xyz.swapee.wc.AbstractExchangeStatusTitleComputer}*/
export class AbstractExchangeStatusTitleComputer extends Module['783779980730'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleComputer} */
AbstractExchangeStatusTitleComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeStatusTitleController} */
export const ExchangeStatusTitleController=Module['783779980761']