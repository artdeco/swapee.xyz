/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitle` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitle}
 */
class AbstractExchangeStatusTitle extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeStatusTitle_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeStatusTitlePort}
 */
class ExchangeStatusTitlePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleController}
 */
class AbstractExchangeStatusTitleController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IExchangeStatusTitle_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ExchangeStatusTitleElement}
 */
class ExchangeStatusTitleElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeStatusTitleBuffer}
 */
class ExchangeStatusTitleBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeStatusTitleComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleComputer}
 */
class AbstractExchangeStatusTitleComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ExchangeStatusTitleController}
 */
class ExchangeStatusTitleController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeStatusTitle = AbstractExchangeStatusTitle
module.exports.ExchangeStatusTitlePort = ExchangeStatusTitlePort
module.exports.AbstractExchangeStatusTitleController = AbstractExchangeStatusTitleController
module.exports.ExchangeStatusTitleElement = ExchangeStatusTitleElement
module.exports.ExchangeStatusTitleBuffer = ExchangeStatusTitleBuffer
module.exports.AbstractExchangeStatusTitleComputer = AbstractExchangeStatusTitleComputer
module.exports.ExchangeStatusTitleController = ExchangeStatusTitleController

Object.defineProperties(module.exports, {
 'AbstractExchangeStatusTitle': {get: () => require('./precompile/internal')[78377998071]},
 [78377998071]: {get: () => module.exports['AbstractExchangeStatusTitle']},
 'ExchangeStatusTitlePort': {get: () => require('./precompile/internal')[78377998073]},
 [78377998073]: {get: () => module.exports['ExchangeStatusTitlePort']},
 'AbstractExchangeStatusTitleController': {get: () => require('./precompile/internal')[78377998074]},
 [78377998074]: {get: () => module.exports['AbstractExchangeStatusTitleController']},
 'ExchangeStatusTitleElement': {get: () => require('./precompile/internal')[78377998078]},
 [78377998078]: {get: () => module.exports['ExchangeStatusTitleElement']},
 'ExchangeStatusTitleBuffer': {get: () => require('./precompile/internal')[783779980711]},
 [783779980711]: {get: () => module.exports['ExchangeStatusTitleBuffer']},
 'AbstractExchangeStatusTitleComputer': {get: () => require('./precompile/internal')[783779980730]},
 [783779980730]: {get: () => module.exports['AbstractExchangeStatusTitleComputer']},
 'ExchangeStatusTitleController': {get: () => require('./precompile/internal')[783779980761]},
 [783779980761]: {get: () => module.exports['ExchangeStatusTitleController']},
})