import { ExchangeStatusTitleDisplay, ExchangeStatusTitleScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.ExchangeStatusTitleDisplay} */
export { ExchangeStatusTitleDisplay }
/** @lazy @api {xyz.swapee.wc.ExchangeStatusTitleScreen} */
export { ExchangeStatusTitleScreen }