import deduceInputs from './methods/deduce-inputs'
import AbstractExchangeStatusTitleControllerAT from '../../gen/AbstractExchangeStatusTitleControllerAT'
import ExchangeStatusTitleDisplay from '../ExchangeStatusTitleDisplay'
import AbstractExchangeStatusTitleScreen from '../../gen/AbstractExchangeStatusTitleScreen'

/** @extends {xyz.swapee.wc.ExchangeStatusTitleScreen} */
export default class extends AbstractExchangeStatusTitleScreen.implements(
 AbstractExchangeStatusTitleControllerAT,
 ExchangeStatusTitleDisplay,
 /**@type {!xyz.swapee.wc.IExchangeStatusTitleScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IExchangeStatusTitleScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:7837799807,
 }),
){}