import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import ExchangeStatusTitleServerController from '../ExchangeStatusTitleServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractExchangeStatusTitleElement from '../../gen/AbstractExchangeStatusTitleElement'

/** @extends {xyz.swapee.wc.ExchangeStatusTitleElement} */
export default class ExchangeStatusTitleElement extends AbstractExchangeStatusTitleElement.implements(
 ExchangeStatusTitleServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IExchangeStatusTitleElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IExchangeStatusTitleElement}*/({
   classesMap: true,
   rootSelector:     `.ExchangeStatusTitle`,
   stylesheet:       'html/styles/ExchangeStatusTitle.css',
   blockName:        'html/ExchangeStatusTitleBlock.html',
  }),
){}

// thank you for using web circuits
