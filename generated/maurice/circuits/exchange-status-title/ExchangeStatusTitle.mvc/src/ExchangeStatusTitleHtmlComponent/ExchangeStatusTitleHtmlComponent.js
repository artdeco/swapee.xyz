import ExchangeStatusTitleHtmlController from '../ExchangeStatusTitleHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractExchangeStatusTitleHtmlComponent} from '../../gen/AbstractExchangeStatusTitleHtmlComponent'

/** @extends {xyz.swapee.wc.ExchangeStatusTitleHtmlComponent} */
export default class extends AbstractExchangeStatusTitleHtmlComponent.implements(
 ExchangeStatusTitleHtmlController,
 IntegratedComponentInitialiser,
){}