import AbstractExchangeStatusTitleScreenAT from '../AbstractExchangeStatusTitleScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleScreenBack}
 */
function __AbstractExchangeStatusTitleScreenBack() {}
__AbstractExchangeStatusTitleScreenBack.prototype = /** @type {!_AbstractExchangeStatusTitleScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen}
 */
class _AbstractExchangeStatusTitleScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen} ‎
 */
class AbstractExchangeStatusTitleScreenBack extends newAbstract(
 _AbstractExchangeStatusTitleScreenBack,783779980726,null,{
  asIExchangeStatusTitleScreen:1,
  superExchangeStatusTitleScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen} */
AbstractExchangeStatusTitleScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreen} */
function AbstractExchangeStatusTitleScreenBackClass(){}

export default AbstractExchangeStatusTitleScreenBack


AbstractExchangeStatusTitleScreenBack[$implementations]=[
 __AbstractExchangeStatusTitleScreenBack,
 AbstractExchangeStatusTitleScreenAT,
]