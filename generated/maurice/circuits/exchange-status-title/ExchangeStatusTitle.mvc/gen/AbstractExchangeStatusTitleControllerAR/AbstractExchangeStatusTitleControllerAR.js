import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleControllerAR}
 */
function __AbstractExchangeStatusTitleControllerAR() {}
__AbstractExchangeStatusTitleControllerAR.prototype = /** @type {!_AbstractExchangeStatusTitleControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR}
 */
class _AbstractExchangeStatusTitleControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeStatusTitleControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR} ‎
 */
class AbstractExchangeStatusTitleControllerAR extends newAbstract(
 _AbstractExchangeStatusTitleControllerAR,783779980723,null,{
  asIExchangeStatusTitleControllerAR:1,
  superExchangeStatusTitleControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR} */
AbstractExchangeStatusTitleControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleControllerAR} */
function AbstractExchangeStatusTitleControllerARClass(){}

export default AbstractExchangeStatusTitleControllerAR


AbstractExchangeStatusTitleControllerAR[$implementations]=[
 __AbstractExchangeStatusTitleControllerAR,
 AR,
 AbstractExchangeStatusTitleControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeStatusTitleControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]