import AbstractExchangeStatusTitleProcessor from '../AbstractExchangeStatusTitleProcessor'
import {ExchangeStatusTitleCore} from '../ExchangeStatusTitleCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractExchangeStatusTitleComputer} from '../AbstractExchangeStatusTitleComputer'
import {AbstractExchangeStatusTitleController} from '../AbstractExchangeStatusTitleController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitle}
 */
function __AbstractExchangeStatusTitle() {}
__AbstractExchangeStatusTitle.prototype = /** @type {!_AbstractExchangeStatusTitle} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitle}
 */
class _AbstractExchangeStatusTitle { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitle} ‎
 */
class AbstractExchangeStatusTitle extends newAbstract(
 _AbstractExchangeStatusTitle,78377998079,null,{
  asIExchangeStatusTitle:1,
  superExchangeStatusTitle:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitle} */
AbstractExchangeStatusTitle.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitle} */
function AbstractExchangeStatusTitleClass(){}

export default AbstractExchangeStatusTitle


AbstractExchangeStatusTitle[$implementations]=[
 __AbstractExchangeStatusTitle,
 ExchangeStatusTitleCore,
 AbstractExchangeStatusTitleProcessor,
 IntegratedComponent,
 AbstractExchangeStatusTitleComputer,
 AbstractExchangeStatusTitleController,
]


export {AbstractExchangeStatusTitle}