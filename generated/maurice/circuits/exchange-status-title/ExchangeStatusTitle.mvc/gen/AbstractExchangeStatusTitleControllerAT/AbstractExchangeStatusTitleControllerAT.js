import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleControllerAT}
 */
function __AbstractExchangeStatusTitleControllerAT() {}
__AbstractExchangeStatusTitleControllerAT.prototype = /** @type {!_AbstractExchangeStatusTitleControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT}
 */
class _AbstractExchangeStatusTitleControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeStatusTitleControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT} ‎
 */
class AbstractExchangeStatusTitleControllerAT extends newAbstract(
 _AbstractExchangeStatusTitleControllerAT,783779980724,null,{
  asIExchangeStatusTitleControllerAT:1,
  superExchangeStatusTitleControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT} */
AbstractExchangeStatusTitleControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleControllerAT} */
function AbstractExchangeStatusTitleControllerATClass(){}

export default AbstractExchangeStatusTitleControllerAT


AbstractExchangeStatusTitleControllerAT[$implementations]=[
 __AbstractExchangeStatusTitleControllerAT,
 UartUniversal,
 AbstractExchangeStatusTitleControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeStatusTitleControllerAT}*/({
  get asIExchangeStatusTitleController(){
   return this
  },
 }),
]