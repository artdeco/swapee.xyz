import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeStatusTitleMemoryPQs} from '../../pqs/ExchangeStatusTitleMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusTitleCore}
 */
function __ExchangeStatusTitleCore() {}
__ExchangeStatusTitleCore.prototype = /** @type {!_ExchangeStatusTitleCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleCore}
 */
class _ExchangeStatusTitleCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleCore} ‎
 */
class ExchangeStatusTitleCore extends newAbstract(
 _ExchangeStatusTitleCore,78377998077,null,{
  asIExchangeStatusTitleCore:1,
  superExchangeStatusTitleCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleCore} */
ExchangeStatusTitleCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleCore} */
function ExchangeStatusTitleCoreClass(){}

export default ExchangeStatusTitleCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusTitleOuterCore}
 */
function __ExchangeStatusTitleOuterCore() {}
__ExchangeStatusTitleOuterCore.prototype = /** @type {!_ExchangeStatusTitleOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusTitleOuterCore} */
export function ExchangeStatusTitleOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeStatusTitleOuterCore.Model}*/
  this.model={
    status: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore}
 */
class _ExchangeStatusTitleOuterCore { }
/**
 * The _IExchangeStatusTitle_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore} ‎
 */
export class ExchangeStatusTitleOuterCore extends newAbstract(
 _ExchangeStatusTitleOuterCore,78377998073,ExchangeStatusTitleOuterCoreConstructor,{
  asIExchangeStatusTitleOuterCore:1,
  superExchangeStatusTitleOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore} */
ExchangeStatusTitleOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleOuterCore} */
function ExchangeStatusTitleOuterCoreClass(){}


ExchangeStatusTitleOuterCore[$implementations]=[
 __ExchangeStatusTitleOuterCore,
 ExchangeStatusTitleOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleOuterCore}*/({
  constructor(){
   mountPins(this.model,'',ExchangeStatusTitleMemoryPQs)

  },
 }),
]

ExchangeStatusTitleCore[$implementations]=[
 ExchangeStatusTitleCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleCore}*/({
  resetCore(){
   this.resetExchangeStatusTitleCore()
  },
  resetExchangeStatusTitleCore(){
   ExchangeStatusTitleOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ExchangeStatusTitleOuterCore}*/(
     /**@type {!xyz.swapee.wc.IExchangeStatusTitleOuterCore}*/(this)),
   )
  },
 }),
 __ExchangeStatusTitleCore,
 ExchangeStatusTitleOuterCore,
]

export {ExchangeStatusTitleCore}