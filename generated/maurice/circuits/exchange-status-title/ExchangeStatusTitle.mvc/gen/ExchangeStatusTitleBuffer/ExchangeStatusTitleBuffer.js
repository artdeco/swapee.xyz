import {makeBuffers} from '@webcircuits/webcircuits'

export const ExchangeStatusTitleBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  status:String,
 }),
})

export default ExchangeStatusTitleBuffer