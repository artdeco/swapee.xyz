import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleScreenAT}
 */
function __AbstractExchangeStatusTitleScreenAT() {}
__AbstractExchangeStatusTitleScreenAT.prototype = /** @type {!_AbstractExchangeStatusTitleScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT}
 */
class _AbstractExchangeStatusTitleScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeStatusTitleScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT} ‎
 */
class AbstractExchangeStatusTitleScreenAT extends newAbstract(
 _AbstractExchangeStatusTitleScreenAT,783779980728,null,{
  asIExchangeStatusTitleScreenAT:1,
  superExchangeStatusTitleScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT} */
AbstractExchangeStatusTitleScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleScreenAT} */
function AbstractExchangeStatusTitleScreenATClass(){}

export default AbstractExchangeStatusTitleScreenAT


AbstractExchangeStatusTitleScreenAT[$implementations]=[
 __AbstractExchangeStatusTitleScreenAT,
 UartUniversal,
]