import ExchangeStatusTitleRenderVdus from './methods/render-vdus'
import ExchangeStatusTitleElementPort from '../ExchangeStatusTitleElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {ExchangeStatusTitleInputsPQs} from '../../pqs/ExchangeStatusTitleInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractExchangeStatusTitle from '../AbstractExchangeStatusTitle'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleElement}
 */
function __AbstractExchangeStatusTitleElement() {}
__AbstractExchangeStatusTitleElement.prototype = /** @type {!_AbstractExchangeStatusTitleElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleElement}
 */
class _AbstractExchangeStatusTitleElement { }
/**
 * A component description.
 *
 * The _IExchangeStatusTitle_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleElement} ‎
 */
class AbstractExchangeStatusTitleElement extends newAbstract(
 _AbstractExchangeStatusTitleElement,783779980712,null,{
  asIExchangeStatusTitleElement:1,
  superExchangeStatusTitleElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElement} */
AbstractExchangeStatusTitleElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElement} */
function AbstractExchangeStatusTitleElementClass(){}

export default AbstractExchangeStatusTitleElement


AbstractExchangeStatusTitleElement[$implementations]=[
 __AbstractExchangeStatusTitleElement,
 ElementBase,
 AbstractExchangeStatusTitleElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':status':statusColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'status':statusAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(statusAttr===undefined?{'status':statusColAttr}:{}),
   }
  },
 }),
 AbstractExchangeStatusTitleElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'status':statusAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    status:statusAttr,
   }
  },
 }),
 AbstractExchangeStatusTitleElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractExchangeStatusTitleElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleElement}*/({
  render:ExchangeStatusTitleRenderVdus,
 }),
 AbstractExchangeStatusTitleElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleElement}*/({
  classes:{
   'Class': '9bd81',
  },
  inputsPQs:ExchangeStatusTitleInputsPQs,
  vdus:{
   'AwaitingStatusLa': 'a2741',
   'ConfirmingStatusLa': 'a2742',
   'ExchangingStatusLa': 'a2743',
   'SendingStatusLa': 'a2744',
   'FinishedStatusLa': 'a2745',
   'FailedStatusLa': 'a2746',
   'RefundedStatusLa': 'a2747',
   'OverdueStatusLa': 'a2748',
   'HoldStatusLa': 'a2749',
   'ExpiredStatusLa': 'a27410',
   'NewStatusLa': 'a27411',
  },
 }),
 IntegratedComponent,
 AbstractExchangeStatusTitleElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','status','no-solder',':no-solder',':status','fe646','992ee','54ab8','f0fe5','8544f','e86ed','d736e','860a0','697a8','0c832','e5575','e2820','9acb4','children']),
   })
  },
  get Port(){
   return ExchangeStatusTitleElementPort
  },
 }),
]



AbstractExchangeStatusTitleElement[$implementations]=[AbstractExchangeStatusTitle,
 /** @type {!AbstractExchangeStatusTitleElement} */ ({
  rootId:'ExchangeStatusTitle',
  __$id:7837799807,
  fqn:'xyz.swapee.wc.IExchangeStatusTitle',
  maurice_element_v3:true,
 }),
]