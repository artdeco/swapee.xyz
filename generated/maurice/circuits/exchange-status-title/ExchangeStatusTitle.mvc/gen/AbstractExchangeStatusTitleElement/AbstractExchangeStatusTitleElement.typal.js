
import AbstractExchangeStatusTitle from '../AbstractExchangeStatusTitle'

/** @abstract {xyz.swapee.wc.IExchangeStatusTitleElement} */
export default class AbstractExchangeStatusTitleElement { }



AbstractExchangeStatusTitleElement[$implementations]=[AbstractExchangeStatusTitle,
 /** @type {!AbstractExchangeStatusTitleElement} */ ({
  rootId:'ExchangeStatusTitle',
  __$id:7837799807,
  fqn:'xyz.swapee.wc.IExchangeStatusTitle',
  maurice_element_v3:true,
 }),
]