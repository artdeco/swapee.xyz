import AbstractExchangeStatusTitleControllerAR from '../AbstractExchangeStatusTitleControllerAR'
import {AbstractExchangeStatusTitleController} from '../AbstractExchangeStatusTitleController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleControllerBack}
 */
function __AbstractExchangeStatusTitleControllerBack() {}
__AbstractExchangeStatusTitleControllerBack.prototype = /** @type {!_AbstractExchangeStatusTitleControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleController}
 */
class _AbstractExchangeStatusTitleControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleController} ‎
 */
class AbstractExchangeStatusTitleControllerBack extends newAbstract(
 _AbstractExchangeStatusTitleControllerBack,783779980722,null,{
  asIExchangeStatusTitleController:1,
  superExchangeStatusTitleController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleController} */
AbstractExchangeStatusTitleControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleController} */
function AbstractExchangeStatusTitleControllerBackClass(){}

export default AbstractExchangeStatusTitleControllerBack


AbstractExchangeStatusTitleControllerBack[$implementations]=[
 __AbstractExchangeStatusTitleControllerBack,
 AbstractExchangeStatusTitleController,
 AbstractExchangeStatusTitleControllerAR,
 DriverBack,
]