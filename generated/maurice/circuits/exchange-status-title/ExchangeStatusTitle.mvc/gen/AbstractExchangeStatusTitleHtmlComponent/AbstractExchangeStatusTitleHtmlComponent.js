import AbstractExchangeStatusTitleGPU from '../AbstractExchangeStatusTitleGPU'
import AbstractExchangeStatusTitleScreenBack from '../AbstractExchangeStatusTitleScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {ExchangeStatusTitleInputsQPs} from '../../pqs/ExchangeStatusTitleInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractExchangeStatusTitle from '../AbstractExchangeStatusTitle'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleHtmlComponent}
 */
function __AbstractExchangeStatusTitleHtmlComponent() {}
__AbstractExchangeStatusTitleHtmlComponent.prototype = /** @type {!_AbstractExchangeStatusTitleHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent}
 */
class _AbstractExchangeStatusTitleHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ExchangeStatusTitleHtmlComponent} */ (res)
  }
}
/**
 * The _IExchangeStatusTitle_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent} ‎
 */
export class AbstractExchangeStatusTitleHtmlComponent extends newAbstract(
 _AbstractExchangeStatusTitleHtmlComponent,783779980711,null,{
  asIExchangeStatusTitleHtmlComponent:1,
  superExchangeStatusTitleHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent} */
AbstractExchangeStatusTitleHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleHtmlComponent} */
function AbstractExchangeStatusTitleHtmlComponentClass(){}


AbstractExchangeStatusTitleHtmlComponent[$implementations]=[
 __AbstractExchangeStatusTitleHtmlComponent,
 HtmlComponent,
 AbstractExchangeStatusTitle,
 AbstractExchangeStatusTitleGPU,
 AbstractExchangeStatusTitleScreenBack,
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  inputsQPs:ExchangeStatusTitleInputsQPs,
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_NewStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{NewStatusLa:NewStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(NewStatusLa,status=='new')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_AwaitingStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{AwaitingStatusLa:AwaitingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(AwaitingStatusLa,status=='waiting')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_ConfirmingStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{ConfirmingStatusLa:ConfirmingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ConfirmingStatusLa,status=='confirming')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_ExchangingStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{ExchangingStatusLa:ExchangingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ExchangingStatusLa,status=='exchanging')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_SendingStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{SendingStatusLa:SendingStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(SendingStatusLa,status=='sending')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_FinishedStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{FinishedStatusLa:FinishedStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(FinishedStatusLa,status=='finished')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_FailedStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{FailedStatusLa:FailedStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(FailedStatusLa,status=='failed')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_RefundedStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{RefundedStatusLa:RefundedStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(RefundedStatusLa,status=='refunded')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_OverdueStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{OverdueStatusLa:OverdueStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(OverdueStatusLa,status=='overdue')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_HoldStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{HoldStatusLa:HoldStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(HoldStatusLa,status=='hold')
  },
 }),
 AbstractExchangeStatusTitleHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleHtmlComponent}*/({
  paint:function $reveal_ExpiredStatusLa({status:status}){
   const{
    asIExchangeStatusTitleGPU:{ExpiredStatusLa:ExpiredStatusLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ExpiredStatusLa,status=='expired')
  },
 }),
]