import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleDisplay}
 */
function __AbstractExchangeStatusTitleDisplay() {}
__AbstractExchangeStatusTitleDisplay.prototype = /** @type {!_AbstractExchangeStatusTitleDisplay} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusTitleDisplay} */ function ExchangeStatusTitleDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.NewStatusLa=null
  /** @type {HTMLSpanElement} */ this.AwaitingStatusLa=null
  /** @type {HTMLSpanElement} */ this.ConfirmingStatusLa=null
  /** @type {HTMLSpanElement} */ this.ExchangingStatusLa=null
  /** @type {HTMLSpanElement} */ this.SendingStatusLa=null
  /** @type {HTMLSpanElement} */ this.FinishedStatusLa=null
  /** @type {HTMLSpanElement} */ this.FailedStatusLa=null
  /** @type {HTMLSpanElement} */ this.RefundedStatusLa=null
  /** @type {HTMLSpanElement} */ this.OverdueStatusLa=null
  /** @type {HTMLSpanElement} */ this.HoldStatusLa=null
  /** @type {HTMLSpanElement} */ this.ExpiredStatusLa=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleDisplay}
 */
class _AbstractExchangeStatusTitleDisplay { }
/**
 * Display for presenting information from the _IExchangeStatusTitle_.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleDisplay} ‎
 */
class AbstractExchangeStatusTitleDisplay extends newAbstract(
 _AbstractExchangeStatusTitleDisplay,783779980716,ExchangeStatusTitleDisplayConstructor,{
  asIExchangeStatusTitleDisplay:1,
  superExchangeStatusTitleDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleDisplay} */
AbstractExchangeStatusTitleDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleDisplay} */
function AbstractExchangeStatusTitleDisplayClass(){}

export default AbstractExchangeStatusTitleDisplay


AbstractExchangeStatusTitleDisplay[$implementations]=[
 __AbstractExchangeStatusTitleDisplay,
 Display,
 AbstractExchangeStatusTitleDisplayClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIExchangeStatusTitleScreen:{vdusPQs:{
    NewStatusLa:NewStatusLa,
    AwaitingStatusLa:AwaitingStatusLa,
    ConfirmingStatusLa:ConfirmingStatusLa,
    ExchangingStatusLa:ExchangingStatusLa,
    SendingStatusLa:SendingStatusLa,
    FinishedStatusLa:FinishedStatusLa,
    FailedStatusLa:FailedStatusLa,
    RefundedStatusLa:RefundedStatusLa,
    OverdueStatusLa:OverdueStatusLa,
    HoldStatusLa:HoldStatusLa,
    ExpiredStatusLa:ExpiredStatusLa,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    NewStatusLa:/**@type {HTMLSpanElement}*/(children[NewStatusLa]),
    AwaitingStatusLa:/**@type {HTMLSpanElement}*/(children[AwaitingStatusLa]),
    ConfirmingStatusLa:/**@type {HTMLSpanElement}*/(children[ConfirmingStatusLa]),
    ExchangingStatusLa:/**@type {HTMLSpanElement}*/(children[ExchangingStatusLa]),
    SendingStatusLa:/**@type {HTMLSpanElement}*/(children[SendingStatusLa]),
    FinishedStatusLa:/**@type {HTMLSpanElement}*/(children[FinishedStatusLa]),
    FailedStatusLa:/**@type {HTMLSpanElement}*/(children[FailedStatusLa]),
    RefundedStatusLa:/**@type {HTMLSpanElement}*/(children[RefundedStatusLa]),
    OverdueStatusLa:/**@type {HTMLSpanElement}*/(children[OverdueStatusLa]),
    HoldStatusLa:/**@type {HTMLSpanElement}*/(children[HoldStatusLa]),
    ExpiredStatusLa:/**@type {HTMLSpanElement}*/(children[ExpiredStatusLa]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.ExchangeStatusTitleDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IExchangeStatusTitleDisplay.Initialese}*/({
   NewStatusLa:1,
   AwaitingStatusLa:1,
   ConfirmingStatusLa:1,
   ExchangingStatusLa:1,
   SendingStatusLa:1,
   FinishedStatusLa:1,
   FailedStatusLa:1,
   RefundedStatusLa:1,
   OverdueStatusLa:1,
   HoldStatusLa:1,
   ExpiredStatusLa:1,
  }),
  initializer({
   NewStatusLa:_NewStatusLa,
   AwaitingStatusLa:_AwaitingStatusLa,
   ConfirmingStatusLa:_ConfirmingStatusLa,
   ExchangingStatusLa:_ExchangingStatusLa,
   SendingStatusLa:_SendingStatusLa,
   FinishedStatusLa:_FinishedStatusLa,
   FailedStatusLa:_FailedStatusLa,
   RefundedStatusLa:_RefundedStatusLa,
   OverdueStatusLa:_OverdueStatusLa,
   HoldStatusLa:_HoldStatusLa,
   ExpiredStatusLa:_ExpiredStatusLa,
  }) {
   if(_NewStatusLa!==undefined) this.NewStatusLa=_NewStatusLa
   if(_AwaitingStatusLa!==undefined) this.AwaitingStatusLa=_AwaitingStatusLa
   if(_ConfirmingStatusLa!==undefined) this.ConfirmingStatusLa=_ConfirmingStatusLa
   if(_ExchangingStatusLa!==undefined) this.ExchangingStatusLa=_ExchangingStatusLa
   if(_SendingStatusLa!==undefined) this.SendingStatusLa=_SendingStatusLa
   if(_FinishedStatusLa!==undefined) this.FinishedStatusLa=_FinishedStatusLa
   if(_FailedStatusLa!==undefined) this.FailedStatusLa=_FailedStatusLa
   if(_RefundedStatusLa!==undefined) this.RefundedStatusLa=_RefundedStatusLa
   if(_OverdueStatusLa!==undefined) this.OverdueStatusLa=_OverdueStatusLa
   if(_HoldStatusLa!==undefined) this.HoldStatusLa=_HoldStatusLa
   if(_ExpiredStatusLa!==undefined) this.ExpiredStatusLa=_ExpiredStatusLa
  },
 }),
]