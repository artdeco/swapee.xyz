import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleScreenAR}
 */
function __AbstractExchangeStatusTitleScreenAR() {}
__AbstractExchangeStatusTitleScreenAR.prototype = /** @type {!_AbstractExchangeStatusTitleScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR}
 */
class _AbstractExchangeStatusTitleScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeStatusTitleScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR} ‎
 */
class AbstractExchangeStatusTitleScreenAR extends newAbstract(
 _AbstractExchangeStatusTitleScreenAR,783779980727,null,{
  asIExchangeStatusTitleScreenAR:1,
  superExchangeStatusTitleScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR} */
AbstractExchangeStatusTitleScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeStatusTitleScreenAR} */
function AbstractExchangeStatusTitleScreenARClass(){}

export default AbstractExchangeStatusTitleScreenAR


AbstractExchangeStatusTitleScreenAR[$implementations]=[
 __AbstractExchangeStatusTitleScreenAR,
 AR,
 AbstractExchangeStatusTitleScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeStatusTitleScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractExchangeStatusTitleScreenAR}