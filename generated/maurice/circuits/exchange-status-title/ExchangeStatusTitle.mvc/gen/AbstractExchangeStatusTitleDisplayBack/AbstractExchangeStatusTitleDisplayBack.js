import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleDisplay}
 */
function __AbstractExchangeStatusTitleDisplay() {}
__AbstractExchangeStatusTitleDisplay.prototype = /** @type {!_AbstractExchangeStatusTitleDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay}
 */
class _AbstractExchangeStatusTitleDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay} ‎
 */
class AbstractExchangeStatusTitleDisplay extends newAbstract(
 _AbstractExchangeStatusTitleDisplay,783779980718,null,{
  asIExchangeStatusTitleDisplay:1,
  superExchangeStatusTitleDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay} */
AbstractExchangeStatusTitleDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeStatusTitleDisplay} */
function AbstractExchangeStatusTitleDisplayClass(){}

export default AbstractExchangeStatusTitleDisplay


AbstractExchangeStatusTitleDisplay[$implementations]=[
 __AbstractExchangeStatusTitleDisplay,
 GraphicsDriverBack,
 AbstractExchangeStatusTitleDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeStatusTitleDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IExchangeStatusTitleDisplay}*/({
    NewStatusLa:twinMock,
    AwaitingStatusLa:twinMock,
    ConfirmingStatusLa:twinMock,
    ExchangingStatusLa:twinMock,
    SendingStatusLa:twinMock,
    FinishedStatusLa:twinMock,
    FailedStatusLa:twinMock,
    RefundedStatusLa:twinMock,
    OverdueStatusLa:twinMock,
    HoldStatusLa:twinMock,
    ExpiredStatusLa:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.ExchangeStatusTitleDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IExchangeStatusTitleDisplay.Initialese}*/({
   NewStatusLa:1,
   AwaitingStatusLa:1,
   ConfirmingStatusLa:1,
   ExchangingStatusLa:1,
   SendingStatusLa:1,
   FinishedStatusLa:1,
   FailedStatusLa:1,
   RefundedStatusLa:1,
   OverdueStatusLa:1,
   HoldStatusLa:1,
   ExpiredStatusLa:1,
  }),
  initializer({
   NewStatusLa:_NewStatusLa,
   AwaitingStatusLa:_AwaitingStatusLa,
   ConfirmingStatusLa:_ConfirmingStatusLa,
   ExchangingStatusLa:_ExchangingStatusLa,
   SendingStatusLa:_SendingStatusLa,
   FinishedStatusLa:_FinishedStatusLa,
   FailedStatusLa:_FailedStatusLa,
   RefundedStatusLa:_RefundedStatusLa,
   OverdueStatusLa:_OverdueStatusLa,
   HoldStatusLa:_HoldStatusLa,
   ExpiredStatusLa:_ExpiredStatusLa,
  }) {
   if(_NewStatusLa!==undefined) this.NewStatusLa=_NewStatusLa
   if(_AwaitingStatusLa!==undefined) this.AwaitingStatusLa=_AwaitingStatusLa
   if(_ConfirmingStatusLa!==undefined) this.ConfirmingStatusLa=_ConfirmingStatusLa
   if(_ExchangingStatusLa!==undefined) this.ExchangingStatusLa=_ExchangingStatusLa
   if(_SendingStatusLa!==undefined) this.SendingStatusLa=_SendingStatusLa
   if(_FinishedStatusLa!==undefined) this.FinishedStatusLa=_FinishedStatusLa
   if(_FailedStatusLa!==undefined) this.FailedStatusLa=_FailedStatusLa
   if(_RefundedStatusLa!==undefined) this.RefundedStatusLa=_RefundedStatusLa
   if(_OverdueStatusLa!==undefined) this.OverdueStatusLa=_OverdueStatusLa
   if(_HoldStatusLa!==undefined) this.HoldStatusLa=_HoldStatusLa
   if(_ExpiredStatusLa!==undefined) this.ExpiredStatusLa=_ExpiredStatusLa
  },
 }),
]