import AbstractExchangeStatusTitleDisplay from '../AbstractExchangeStatusTitleDisplayBack'
import ExchangeStatusTitleClassesPQs from '../../pqs/ExchangeStatusTitleClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {ExchangeStatusTitleClassesQPs} from '../../pqs/ExchangeStatusTitleClassesQPs'
import {ExchangeStatusTitleVdusPQs} from '../../pqs/ExchangeStatusTitleVdusPQs'
import {ExchangeStatusTitleVdusQPs} from '../../pqs/ExchangeStatusTitleVdusQPs'
import {ExchangeStatusTitleMemoryPQs} from '../../pqs/ExchangeStatusTitleMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleGPU}
 */
function __AbstractExchangeStatusTitleGPU() {}
__AbstractExchangeStatusTitleGPU.prototype = /** @type {!_AbstractExchangeStatusTitleGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleGPU}
 */
class _AbstractExchangeStatusTitleGPU { }
/**
 * Handles the periphery of the _IExchangeStatusTitleDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleGPU} ‎
 */
class AbstractExchangeStatusTitleGPU extends newAbstract(
 _AbstractExchangeStatusTitleGPU,783779980715,null,{
  asIExchangeStatusTitleGPU:1,
  superExchangeStatusTitleGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleGPU} */
AbstractExchangeStatusTitleGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleGPU} */
function AbstractExchangeStatusTitleGPUClass(){}

export default AbstractExchangeStatusTitleGPU


AbstractExchangeStatusTitleGPU[$implementations]=[
 __AbstractExchangeStatusTitleGPU,
 AbstractExchangeStatusTitleGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleGPU}*/({
  classesQPs:ExchangeStatusTitleClassesQPs,
  vdusPQs:ExchangeStatusTitleVdusPQs,
  vdusQPs:ExchangeStatusTitleVdusQPs,
  memoryPQs:ExchangeStatusTitleMemoryPQs,
 }),
 AbstractExchangeStatusTitleDisplay,
 BrowserView,
 AbstractExchangeStatusTitleGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleGPU}*/({
  allocator(){
   pressFit(this.classes,'',ExchangeStatusTitleClassesPQs)
  },
 }),
]