import ExchangeStatusTitleBuffer from '../ExchangeStatusTitleBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ExchangeStatusTitlePortConnector} from '../ExchangeStatusTitlePort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleController}
 */
function __AbstractExchangeStatusTitleController() {}
__AbstractExchangeStatusTitleController.prototype = /** @type {!_AbstractExchangeStatusTitleController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleController}
 */
class _AbstractExchangeStatusTitleController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleController} ‎
 */
export class AbstractExchangeStatusTitleController extends newAbstract(
 _AbstractExchangeStatusTitleController,783779980720,null,{
  asIExchangeStatusTitleController:1,
  superExchangeStatusTitleController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleController} */
AbstractExchangeStatusTitleController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleController} */
function AbstractExchangeStatusTitleControllerClass(){}


AbstractExchangeStatusTitleController[$implementations]=[
 AbstractExchangeStatusTitleControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IExchangeStatusTitlePort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractExchangeStatusTitleController,
 ExchangeStatusTitleBuffer,
 IntegratedController,
 /**@type {!AbstractExchangeStatusTitleController}*/(ExchangeStatusTitlePortConnector),
]


export default AbstractExchangeStatusTitleController