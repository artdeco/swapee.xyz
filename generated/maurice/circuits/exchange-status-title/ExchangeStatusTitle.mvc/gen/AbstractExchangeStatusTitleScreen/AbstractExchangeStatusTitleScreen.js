import ExchangeStatusTitleClassesPQs from '../../pqs/ExchangeStatusTitleClassesPQs'
import AbstractExchangeStatusTitleScreenAR from '../AbstractExchangeStatusTitleScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {ExchangeStatusTitleInputsPQs} from '../../pqs/ExchangeStatusTitleInputsPQs'
import {ExchangeStatusTitleMemoryQPs} from '../../pqs/ExchangeStatusTitleMemoryQPs'
import {ExchangeStatusTitleVdusPQs} from '../../pqs/ExchangeStatusTitleVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeStatusTitleScreen}
 */
function __AbstractExchangeStatusTitleScreen() {}
__AbstractExchangeStatusTitleScreen.prototype = /** @type {!_AbstractExchangeStatusTitleScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleScreen}
 */
class _AbstractExchangeStatusTitleScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleScreen} ‎
 */
class AbstractExchangeStatusTitleScreen extends newAbstract(
 _AbstractExchangeStatusTitleScreen,783779980725,null,{
  asIExchangeStatusTitleScreen:1,
  superExchangeStatusTitleScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleScreen} */
AbstractExchangeStatusTitleScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleScreen} */
function AbstractExchangeStatusTitleScreenClass(){}

export default AbstractExchangeStatusTitleScreen


AbstractExchangeStatusTitleScreen[$implementations]=[
 __AbstractExchangeStatusTitleScreen,
 AbstractExchangeStatusTitleScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleScreen}*/({
  inputsPQs:ExchangeStatusTitleInputsPQs,
  classesPQs:ExchangeStatusTitleClassesPQs,
  memoryQPs:ExchangeStatusTitleMemoryQPs,
 }),
 Screen,
 AbstractExchangeStatusTitleScreenAR,
 AbstractExchangeStatusTitleScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleScreen}*/({
  vdusPQs:ExchangeStatusTitleVdusPQs,
 }),
]