import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeStatusTitleInputsPQs} from '../../pqs/ExchangeStatusTitleInputsPQs'
import {ExchangeStatusTitleOuterCoreConstructor} from '../ExchangeStatusTitleCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusTitlePort}
 */
function __ExchangeStatusTitlePort() {}
__ExchangeStatusTitlePort.prototype = /** @type {!_ExchangeStatusTitlePort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusTitlePort} */ function ExchangeStatusTitlePortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ExchangeStatusTitleOuterCore} */ ({model:null})
  ExchangeStatusTitleOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitlePort}
 */
class _ExchangeStatusTitlePort { }
/**
 * The port that serves as an interface to the _IExchangeStatusTitle_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitlePort} ‎
 */
export class ExchangeStatusTitlePort extends newAbstract(
 _ExchangeStatusTitlePort,78377998075,ExchangeStatusTitlePortConstructor,{
  asIExchangeStatusTitlePort:1,
  superExchangeStatusTitlePort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitlePort} */
ExchangeStatusTitlePort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitlePort} */
function ExchangeStatusTitlePortClass(){}

export const ExchangeStatusTitlePortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IExchangeStatusTitle.Pinout>}*/({
 get Port() { return ExchangeStatusTitlePort },
})

ExchangeStatusTitlePort[$implementations]=[
 ExchangeStatusTitlePortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitlePort}*/({
  resetPort(){
   this.resetExchangeStatusTitlePort()
  },
  resetExchangeStatusTitlePort(){
   ExchangeStatusTitlePortConstructor.call(this)
  },
 }),
 __ExchangeStatusTitlePort,
 Parametric,
 ExchangeStatusTitlePortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitlePort}*/({
  constructor(){
   mountPins(this.inputs,'',ExchangeStatusTitleInputsPQs)
  },
 }),
]


export default ExchangeStatusTitlePort