import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeStatusTitleElementPort}
 */
function __ExchangeStatusTitleElementPort() {}
__ExchangeStatusTitleElementPort.prototype = /** @type {!_ExchangeStatusTitleElementPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeStatusTitleElementPort} */ function ExchangeStatusTitleElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeStatusTitleElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    newStatusLaOpts: {},
    awaitingStatusLaOpts: {},
    confirmingStatusLaOpts: {},
    exchangingStatusLaOpts: {},
    sendingStatusLaOpts: {},
    finishedStatusLaOpts: {},
    failedStatusLaOpts: {},
    refundedStatusLaOpts: {},
    overdueStatusLaOpts: {},
    holdStatusLaOpts: {},
    expiredStatusLaOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleElementPort}
 */
class _ExchangeStatusTitleElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractExchangeStatusTitleElementPort} ‎
 */
class ExchangeStatusTitleElementPort extends newAbstract(
 _ExchangeStatusTitleElementPort,783779980713,ExchangeStatusTitleElementPortConstructor,{
  asIExchangeStatusTitleElementPort:1,
  superExchangeStatusTitleElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElementPort} */
ExchangeStatusTitleElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeStatusTitleElementPort} */
function ExchangeStatusTitleElementPortClass(){}

export default ExchangeStatusTitleElementPort


ExchangeStatusTitleElementPort[$implementations]=[
 __ExchangeStatusTitleElementPort,
 ExchangeStatusTitleElementPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeStatusTitleElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'new-status-la-opts':undefined,
    'awaiting-status-la-opts':undefined,
    'confirming-status-la-opts':undefined,
    'exchanging-status-la-opts':undefined,
    'sending-status-la-opts':undefined,
    'finished-status-la-opts':undefined,
    'failed-status-la-opts':undefined,
    'refunded-status-la-opts':undefined,
    'overdue-status-la-opts':undefined,
    'hold-status-la-opts':undefined,
    'expired-status-la-opts':undefined,
   })
  },
 }),
]