import ExchangeIntentHtmlController from '../ExchangeIntentHtmlController'
import ExchangeIntentHtmlComputer from '../ExchangeIntentHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractExchangeIntentHtmlComponent} from '../../gen/AbstractExchangeIntentHtmlComponent'

/** @extends {xyz.swapee.wc.ExchangeIntentHtmlComponent} */
export default class extends AbstractExchangeIntentHtmlComponent.implements(
 ExchangeIntentHtmlController,
 ExchangeIntentHtmlComputer,
 IntegratedComponentInitialiser,
){}