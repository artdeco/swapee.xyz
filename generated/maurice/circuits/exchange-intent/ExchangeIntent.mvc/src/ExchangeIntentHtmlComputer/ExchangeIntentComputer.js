import adaptInterlocks from './methods/adapt-interlocks'
import {preadaptInterlocks} from '../../gen/AbstractExchangeIntentComputer/preadapters'
import AbstractExchangeIntentComputer from '../../gen/AbstractExchangeIntentComputer'

/** @extends {xyz.swapee.wc.ExchangeIntentComputer} */
export default class ExchangeIntentHtmlComputer extends AbstractExchangeIntentComputer.implements(
 /** @type {!xyz.swapee.wc.IExchangeIntentComputer} */ ({
  adaptInterlocks:adaptInterlocks,
  adapt:[preadaptInterlocks],
 }),
){}