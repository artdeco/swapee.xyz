/** @type {xyz.swapee.wc.IExchangeIntentComputer._adaptInterlocks} */
export default function adaptInterlocks({
 fixed:fixed,float:float,any:any,
},{fixed:fixedPrev,float:floatPrev,any:anyPrev}) {
 if(fixed&&fixedPrev!==undefined){
  return{float:false,any:false}
 }
 if(float&&floatPrev!==undefined){
  return{fixed:false,any:false}
 }
 if(any&&anyPrev!==undefined){
  return{fixed:false,float:false}
 }
}