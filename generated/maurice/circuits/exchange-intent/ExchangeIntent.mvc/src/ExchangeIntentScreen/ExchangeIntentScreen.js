import AbstractExchangeIntentControllerAT from '../../gen/AbstractExchangeIntentControllerAT'
import ExchangeIntentDisplay from '../ExchangeIntentDisplay'
import AbstractExchangeIntentScreen from '../../gen/AbstractExchangeIntentScreen'

/** @extends {xyz.swapee.wc.ExchangeIntentScreen} */
export default class extends AbstractExchangeIntentScreen.implements(
 AbstractExchangeIntentControllerAT,
 ExchangeIntentDisplay,
 /**@type {!xyz.swapee.wc.IExchangeIntentScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IExchangeIntentScreen} */ ({
  __$id:7833868048,
 }),
/**@type {!xyz.swapee.wc.IExchangeIntentScreen}*/({
   // deduceInputs(el) {},
  }),
){}