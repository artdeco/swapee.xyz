
/**@type {xyz.swapee.wc.IExchangeIntentDesigner._relay} */
export default function relay({This,ExchangeIntent:{
 unsetFloat:unsetFloat,unsetAny:unsetAny,unsetFixed:unsetFixed,
 change,intentChange,
}}) {
 return (h(Fragment,{},
  h(This,{
   onAmountFromEdge:intentChange(),
   onCurrencyFromEdge:intentChange(),
   onCurrencyToEdge:intentChange(),

   onIntentChange:change(),
   onFixedEdge:change(),
   onFloatEdge:change(),
   onAnyEdge:change(),

   onFloatHigh:[ // this is called an interlock
    unsetAny(),unsetFixed(),
   ], onFixedHigh:[
    unsetFloat(),unsetAny(),
   ], onAnyHigh:[
    unsetFloat(),unsetFixed(),
   ] })
 ))
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZXhjaGFuZ2UtaW50ZW50L2V4Y2hhbmdlLWludGVudC53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBNkhHLFNBQVMsS0FBSyxFQUFFLElBQUksQ0FBQztDQUNwQixxQkFBcUIsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUI7Q0FDN0QsTUFBTSxDQUFDLFlBQVk7QUFDcEIsRUFBRTtDQUNELE9BQU8sQ0FBQyxZQUFDO0VBQ1I7R0FDQyxpQkFBa0IsWUFBWSxDQUFDLENBQUU7R0FDakMsbUJBQW9CLFlBQVksQ0FBQyxDQUFFO0dBQ25DLGlCQUFrQixZQUFZLENBQUMsQ0FBRTs7R0FFakMsZUFBZ0IsTUFBTSxDQUFDLENBQUU7R0FDekIsWUFBYSxNQUFNLENBQUMsQ0FBRTtHQUN0QixZQUFhLE1BQU0sQ0FBQyxDQUFFO0dBQ3RCLFVBQVcsTUFBTSxDQUFDLENBQUU7O0dBRXBCLFlBQWEsRUFBRSxHQUFHLEtBQUssR0FBRyxPQUFPLEdBQUc7SUFDbkMsUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztHQUN4QixDQUFFLEVBQUMsWUFBYTtJQUNmLFVBQVUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7R0FDeEIsQ0FBRSxFQUFDLFVBQVc7SUFDYixVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0dBQzFCLENBQUU7Q0FDSixDQUFrQztBQUNuQyxDQUFGIn0=