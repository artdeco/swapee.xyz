/** @type {xyz.swapee.wc.IExchangeIntentElement._relay} */
export default function relay({This,ExchangeIntent:{
 unsetFloat:unsetFloat,unsetAny:unsetAny,unsetFixed:unsetFixed,
 change,intentChange,
}}) {
 return (<>
  <This
   onAmountFromEdge={intentChange()}
   onCurrencyFromEdge={intentChange()}
   onCurrencyToEdge={intentChange()}

   onIntentChange={change()}
   onFixedEdge={change()}
   onFloatEdge={change()}
   onAnyEdge={change()}

   onFloatHigh={[ // this is called an interlock
    unsetAny(),unsetFixed(),
   ]} onFixedHigh={[
    unsetFloat(),unsetAny(),
   ]} onAnyHigh={[
    unsetFloat(),unsetFixed(),
   ]} />
 </>)
}