import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import ExchangeIntentServerController from '../ExchangeIntentServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractExchangeIntentElement from '../../gen/AbstractExchangeIntentElement'

/** @extends {xyz.swapee.wc.ExchangeIntentElement} */
export default class ExchangeIntentElement extends AbstractExchangeIntentElement.implements(
 ExchangeIntentServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IExchangeIntentElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IExchangeIntentElement}*/({
   classesMap: true,
   rootSelector:     `.ExchangeIntent`,
   stylesheet:       'html/styles/ExchangeIntent.css',
   blockName:        'html/ExchangeIntentBlock.html',
  }),
  /** @type {xyz.swapee.wc.IExchangeIntentDesigner} */({
  }),
){}

// thank you for using web circuits
