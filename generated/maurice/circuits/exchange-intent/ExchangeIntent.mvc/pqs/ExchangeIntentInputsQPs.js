import {ExchangeIntentInputsPQs} from './ExchangeIntentInputsPQs'
export const ExchangeIntentInputsQPs=/**@type {!xyz.swapee.wc.ExchangeIntentInputsQPs}*/(Object.keys(ExchangeIntentInputsPQs)
 .reduce((a,k)=>{a[ExchangeIntentInputsPQs[k]]=k;return a},{}))