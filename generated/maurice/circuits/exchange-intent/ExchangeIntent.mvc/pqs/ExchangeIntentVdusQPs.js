import {ExchangeIntentVdusPQs} from './ExchangeIntentVdusPQs'
export const ExchangeIntentVdusQPs=/**@type {!xyz.swapee.wc.ExchangeIntentVdusQPs}*/(Object.keys(ExchangeIntentVdusPQs)
 .reduce((a,k)=>{a[ExchangeIntentVdusPQs[k]]=k;return a},{}))