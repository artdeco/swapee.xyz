import {ExchangeIntentMemoryPQs} from './ExchangeIntentMemoryPQs'
export const ExchangeIntentMemoryQPs=/**@type {!xyz.swapee.wc.ExchangeIntentMemoryQPs}*/(Object.keys(ExchangeIntentMemoryPQs)
 .reduce((a,k)=>{a[ExchangeIntentMemoryPQs[k]]=k;return a},{}))