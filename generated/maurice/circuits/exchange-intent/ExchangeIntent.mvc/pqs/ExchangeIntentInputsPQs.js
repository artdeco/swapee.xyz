import {ExchangeIntentMemoryPQs} from './ExchangeIntentMemoryPQs'
export const ExchangeIntentInputsPQs=/**@type {!xyz.swapee.wc.ExchangeIntentInputsQPs}*/({
 ...ExchangeIntentMemoryPQs,
})