export const ExchangeIntentMemoryPQs=/**@type {!xyz.swapee.wc.ExchangeIntentMemoryPQs}*/({
 currencyFrom:'96c88',
 amountFrom:'748e6',
 currencyTo:'c23cd',
 fixed:'cec31',
 ready:'b2fda',
 any:'100b8',
 float:'546ad',
})