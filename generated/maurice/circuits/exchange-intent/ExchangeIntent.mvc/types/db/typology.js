/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IExchangeIntentComputer': {
  'id': 78338680481,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptInterlocks': 2
  }
 },
 'xyz.swapee.wc.ExchangeIntentMemoryPQs': {
  'id': 78338680482,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIntentOuterCore': {
  'id': 78338680483,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeIntentInputsPQs': {
  'id': 78338680484,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIntentPort': {
  'id': 78338680485,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeIntentPort': 2
  }
 },
 'xyz.swapee.wc.IExchangeIntentPortInterface': {
  'id': 78338680486,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentCore': {
  'id': 78338680487,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeIntentCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeIntentProcessor': {
  'id': 78338680488,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntent': {
  'id': 78338680489,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentBuffer': {
  'id': 783386804810,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentHtmlComponent': {
  'id': 783386804811,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentElement': {
  'id': 783386804812,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IExchangeIntentElementPort': {
  'id': 783386804813,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentDesigner': {
  'id': 783386804814,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeIntentGPU': {
  'id': 783386804815,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentDisplay': {
  'id': 783386804816,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeIntentVdusPQs': {
  'id': 783386804817,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeIntentDisplay': {
  'id': 783386804818,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IExchangeIntentController': {
  'id': 783386804819,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setCurrencyFrom': 2,
   'unsetCurrencyFrom': 3,
   'setAmountFrom': 4,
   'unsetAmountFrom': 5,
   'setCurrencyTo': 6,
   'unsetCurrencyTo': 7,
   'flipFixed': 8,
   'setFixed': 9,
   'unsetFixed': 10,
   'flipAny': 11,
   'setAny': 12,
   'unsetAny': 13,
   'flipFloat': 14,
   'setFloat': 15,
   'unsetFloat': 16,
   'change': 17,
   'onChange': 18,
   'intentChange': 19,
   'onIntentChange': 20
  }
 },
 'xyz.swapee.wc.front.IExchangeIntentController': {
  'id': 783386804820,
  'symbols': {},
  'methods': {
   'setCurrencyFrom': 1,
   'unsetCurrencyFrom': 2,
   'setAmountFrom': 3,
   'unsetAmountFrom': 4,
   'setCurrencyTo': 5,
   'unsetCurrencyTo': 6,
   'flipFixed': 7,
   'setFixed': 8,
   'unsetFixed': 9,
   'flipAny': 10,
   'setAny': 11,
   'unsetAny': 12,
   'flipFloat': 13,
   'setFloat': 14,
   'unsetFloat': 15,
   'change': 16,
   'onChange': 17,
   'intentChange': 18,
   'onIntentChange': 19
  }
 },
 'xyz.swapee.wc.back.IExchangeIntentController': {
  'id': 783386804821,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIntentControllerAR': {
  'id': 783386804822,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIntentControllerAT': {
  'id': 783386804823,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIntentScreen': {
  'id': 783386804824,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIntentScreen': {
  'id': 783386804825,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIntentScreenAR': {
  'id': 783386804826,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIntentScreenAT': {
  'id': 783386804827,
  'symbols': {},
  'methods': {}
 }
})