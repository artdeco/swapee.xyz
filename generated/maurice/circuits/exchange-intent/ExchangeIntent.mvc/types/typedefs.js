/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IExchangeIntentComputer={}
xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks={}
xyz.swapee.wc.IExchangeIntentOuterCore={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyFrom={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model.AmountFrom={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyTo={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Fixed={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Float={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Any={}
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Ready={}
xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel={}
xyz.swapee.wc.IExchangeIntentPort={}
xyz.swapee.wc.IExchangeIntentPort.Inputs={}
xyz.swapee.wc.IExchangeIntentPort.WeakInputs={}
xyz.swapee.wc.IExchangeIntentCore={}
xyz.swapee.wc.IExchangeIntentCore.Model={}
xyz.swapee.wc.IExchangeIntentPortInterface={}
xyz.swapee.wc.IExchangeIntentProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IExchangeIntentController={}
xyz.swapee.wc.front.IExchangeIntentControllerAT={}
xyz.swapee.wc.front.IExchangeIntentScreenAR={}
xyz.swapee.wc.IExchangeIntent={}
xyz.swapee.wc.IExchangeIntentHtmlComponent={}
xyz.swapee.wc.IExchangeIntentElement={}
xyz.swapee.wc.IExchangeIntentElementPort={}
xyz.swapee.wc.IExchangeIntentElementPort.Inputs={}
xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs={}
xyz.swapee.wc.IExchangeIntentDesigner={}
xyz.swapee.wc.IExchangeIntentDesigner.communicator={}
xyz.swapee.wc.IExchangeIntentDesigner.relay={}
xyz.swapee.wc.IExchangeIntentDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IExchangeIntentDisplay={}
xyz.swapee.wc.back.IExchangeIntentController={}
xyz.swapee.wc.back.IExchangeIntentControllerAR={}
xyz.swapee.wc.back.IExchangeIntentScreen={}
xyz.swapee.wc.back.IExchangeIntentScreenAT={}
xyz.swapee.wc.IExchangeIntentController={}
xyz.swapee.wc.IExchangeIntentScreen={}
xyz.swapee.wc.IExchangeIntentGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/02-IExchangeIntentComputer.xml}  1dab6a8bacd319455734f56e99ffdf92 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IExchangeIntentComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentComputer)} xyz.swapee.wc.AbstractExchangeIntentComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentComputer} xyz.swapee.wc.ExchangeIntentComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentComputer` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentComputer
 */
xyz.swapee.wc.AbstractExchangeIntentComputer = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentComputer.constructor&xyz.swapee.wc.ExchangeIntentComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentComputer.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentComputer.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentComputer}
 */
xyz.swapee.wc.AbstractExchangeIntentComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentComputer}
 */
xyz.swapee.wc.AbstractExchangeIntentComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentComputer}
 */
xyz.swapee.wc.AbstractExchangeIntentComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentComputer}
 */
xyz.swapee.wc.AbstractExchangeIntentComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentComputer.Initialese[]) => xyz.swapee.wc.IExchangeIntentComputer} xyz.swapee.wc.ExchangeIntentComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.ExchangeIntentMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IExchangeIntentComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IExchangeIntentComputer
 */
xyz.swapee.wc.IExchangeIntentComputer = class extends /** @type {xyz.swapee.wc.IExchangeIntentComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks} */
xyz.swapee.wc.IExchangeIntentComputer.prototype.adaptInterlocks = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentComputer.compute} */
xyz.swapee.wc.IExchangeIntentComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentComputer.Initialese>)} xyz.swapee.wc.ExchangeIntentComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentComputer} xyz.swapee.wc.IExchangeIntentComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeIntentComputer_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentComputer
 * @implements {xyz.swapee.wc.IExchangeIntentComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentComputer.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentComputer = class extends /** @type {xyz.swapee.wc.ExchangeIntentComputer.constructor&xyz.swapee.wc.IExchangeIntentComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentComputer}
 */
xyz.swapee.wc.ExchangeIntentComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentComputer} */
xyz.swapee.wc.RecordIExchangeIntentComputer

/** @typedef {xyz.swapee.wc.IExchangeIntentComputer} xyz.swapee.wc.BoundIExchangeIntentComputer */

/** @typedef {xyz.swapee.wc.ExchangeIntentComputer} xyz.swapee.wc.BoundExchangeIntentComputer */

/**
 * Contains getters to cast the _IExchangeIntentComputer_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentComputerCaster
 */
xyz.swapee.wc.IExchangeIntentComputerCaster = class { }
/**
 * Cast the _IExchangeIntentComputer_ instance into the _BoundIExchangeIntentComputer_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentComputer}
 */
xyz.swapee.wc.IExchangeIntentComputerCaster.prototype.asIExchangeIntentComputer
/**
 * Access the _ExchangeIntentComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentComputer}
 */
xyz.swapee.wc.IExchangeIntentComputerCaster.prototype.superExchangeIntentComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Form, changes: xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Form) => (void|xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Return)} xyz.swapee.wc.IExchangeIntentComputer.__adaptInterlocks
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentComputer.__adaptInterlocks<!xyz.swapee.wc.IExchangeIntentComputer>} xyz.swapee.wc.IExchangeIntentComputer._adaptInterlocks */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks} */
/**
 * @param {!xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Form} form The form with inputs.
 * - `fixed` _boolean_ Whether fixed rate is needed. ⤴ *IExchangeIntentOuterCore.Model.Fixed_Safe*
 * - `float` _boolean_ Whether float rate is needed. ⤴ *IExchangeIntentOuterCore.Model.Float_Safe*
 * - `any` _boolean_ Whether any rate is needed. ⤴ *IExchangeIntentOuterCore.Model.Any_Safe*
 * @param {xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Form} changes The previous values of the form.
 * - `fixed` _boolean_ Whether fixed rate is needed. ⤴ *IExchangeIntentOuterCore.Model.Fixed_Safe*
 * - `float` _boolean_ Whether float rate is needed. ⤴ *IExchangeIntentOuterCore.Model.Float_Safe*
 * - `any` _boolean_ Whether any rate is needed. ⤴ *IExchangeIntentOuterCore.Model.Any_Safe*
 * @return {void|xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Return} The form with outputs.
 */
xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe&xyz.swapee.wc.IExchangeIntentCore.Model.Any_Safe} xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed&xyz.swapee.wc.IExchangeIntentCore.Model.Float&xyz.swapee.wc.IExchangeIntentCore.Model.Any} xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.ExchangeIntentMemory) => void} xyz.swapee.wc.IExchangeIntentComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentComputer.__compute<!xyz.swapee.wc.IExchangeIntentComputer>} xyz.swapee.wc.IExchangeIntentComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.ExchangeIntentMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIntentComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/03-IExchangeIntentOuterCore.xml}  fa9d53802f59bb16a04ccd0249966baa */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeIntentOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentOuterCore)} xyz.swapee.wc.AbstractExchangeIntentOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentOuterCore} xyz.swapee.wc.ExchangeIntentOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentOuterCore
 */
xyz.swapee.wc.AbstractExchangeIntentOuterCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentOuterCore.constructor&xyz.swapee.wc.ExchangeIntentOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentOuterCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IExchangeIntentOuterCore|typeof xyz.swapee.wc.ExchangeIntentOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIntentOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIntentOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IExchangeIntentOuterCore|typeof xyz.swapee.wc.ExchangeIntentOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIntentOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IExchangeIntentOuterCore|typeof xyz.swapee.wc.ExchangeIntentOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIntentOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentOuterCoreCaster)} xyz.swapee.wc.IExchangeIntentOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IExchangeIntent_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IExchangeIntentOuterCore
 */
xyz.swapee.wc.IExchangeIntentOuterCore = class extends /** @type {xyz.swapee.wc.IExchangeIntentOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIntentOuterCore.prototype.constructor = xyz.swapee.wc.IExchangeIntentOuterCore

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentOuterCore.Initialese>)} xyz.swapee.wc.ExchangeIntentOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentOuterCore} xyz.swapee.wc.IExchangeIntentOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeIntentOuterCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentOuterCore
 * @implements {xyz.swapee.wc.IExchangeIntentOuterCore} The _IExchangeIntent_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentOuterCore = class extends /** @type {xyz.swapee.wc.ExchangeIntentOuterCore.constructor&xyz.swapee.wc.IExchangeIntentOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIntentOuterCore.prototype.constructor = xyz.swapee.wc.ExchangeIntentOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentOuterCore}
 */
xyz.swapee.wc.ExchangeIntentOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentOuterCore.
 * @interface xyz.swapee.wc.IExchangeIntentOuterCoreFields
 */
xyz.swapee.wc.IExchangeIntentOuterCoreFields = class { }
/**
 * The _IExchangeIntent_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IExchangeIntentOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeIntentOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore} */
xyz.swapee.wc.RecordIExchangeIntentOuterCore

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore} xyz.swapee.wc.BoundIExchangeIntentOuterCore */

/** @typedef {xyz.swapee.wc.ExchangeIntentOuterCore} xyz.swapee.wc.BoundExchangeIntentOuterCore */

/**
 * Ticker of the payin currency.
 * @typedef {string}
 */
xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyFrom.currencyFrom

/**
 * Amount of currency the user is going to send, used to create a transaction.
 * @typedef {string}
 */
xyz.swapee.wc.IExchangeIntentOuterCore.Model.AmountFrom.amountFrom

/**
 * Ticker of the payout currency.
 * @typedef {string}
 */
xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyTo.currencyTo

/**
 * Whether fixed rate is needed.
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Fixed.fixed

/**
 * Whether float rate is needed.
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Float.float

/**
 * Whether any rate is needed.
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Any.any

/**
 * Whether the intent has been established.
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeIntentOuterCore.Model.Ready.ready

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyFrom&xyz.swapee.wc.IExchangeIntentOuterCore.Model.AmountFrom&xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyTo&xyz.swapee.wc.IExchangeIntentOuterCore.Model.Fixed&xyz.swapee.wc.IExchangeIntentOuterCore.Model.Float&xyz.swapee.wc.IExchangeIntentOuterCore.Model.Any&xyz.swapee.wc.IExchangeIntentOuterCore.Model.Ready} xyz.swapee.wc.IExchangeIntentOuterCore.Model The _IExchangeIntent_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyFrom&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.AmountFrom&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyTo&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Fixed&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Float&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Any&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Ready} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel The _IExchangeIntent_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IExchangeIntentOuterCore_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentOuterCoreCaster
 */
xyz.swapee.wc.IExchangeIntentOuterCoreCaster = class { }
/**
 * Cast the _IExchangeIntentOuterCore_ instance into the _BoundIExchangeIntentOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentOuterCore}
 */
xyz.swapee.wc.IExchangeIntentOuterCoreCaster.prototype.asIExchangeIntentOuterCore
/**
 * Access the _ExchangeIntentOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentOuterCore}
 */
xyz.swapee.wc.IExchangeIntentOuterCoreCaster.prototype.superExchangeIntentOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyFrom Ticker of the payin currency (optional overlay).
 * @prop {string} [currencyFrom="void 0"] Ticker of the payin currency. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyFrom_Safe Ticker of the payin currency (required overlay).
 * @prop {string} currencyFrom Ticker of the payin currency.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.AmountFrom Amount of currency the user is going to send, used to create a transaction (optional overlay).
 * @prop {string} [amountFrom="void 0"] Amount of currency the user is going to send, used to create a transaction. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.AmountFrom_Safe Amount of currency the user is going to send, used to create a transaction (required overlay).
 * @prop {string} amountFrom Amount of currency the user is going to send, used to create a transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyTo Ticker of the payout currency (optional overlay).
 * @prop {string} [currencyTo="void 0"] Ticker of the payout currency. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyTo_Safe Ticker of the payout currency (required overlay).
 * @prop {string} currencyTo Ticker of the payout currency.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Fixed Whether fixed rate is needed (optional overlay).
 * @prop {boolean} [fixed=void 0] Whether fixed rate is needed. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Fixed_Safe Whether fixed rate is needed (required overlay).
 * @prop {boolean} fixed Whether fixed rate is needed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Float Whether float rate is needed (optional overlay).
 * @prop {boolean} [float=void 0] Whether float rate is needed. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Float_Safe Whether float rate is needed (required overlay).
 * @prop {boolean} float Whether float rate is needed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Any Whether any rate is needed (optional overlay).
 * @prop {boolean} [any=void 0] Whether any rate is needed. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Any_Safe Whether any rate is needed (required overlay).
 * @prop {boolean} any Whether any rate is needed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Ready Whether the intent has been established (optional overlay).
 * @prop {boolean} [ready=false] Whether the intent has been established. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.Model.Ready_Safe Whether the intent has been established (required overlay).
 * @prop {boolean} ready Whether the intent has been established.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyFrom Ticker of the payin currency (optional overlay).
 * @prop {*} [currencyFrom="void 0"] Ticker of the payin currency. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyFrom_Safe Ticker of the payin currency (required overlay).
 * @prop {*} currencyFrom Ticker of the payin currency.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.AmountFrom Amount of currency the user is going to send, used to create a transaction (optional overlay).
 * @prop {*} [amountFrom="void 0"] Amount of currency the user is going to send, used to create a transaction. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.AmountFrom_Safe Amount of currency the user is going to send, used to create a transaction (required overlay).
 * @prop {*} amountFrom Amount of currency the user is going to send, used to create a transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyTo Ticker of the payout currency (optional overlay).
 * @prop {*} [currencyTo="void 0"] Ticker of the payout currency. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyTo_Safe Ticker of the payout currency (required overlay).
 * @prop {*} currencyTo Ticker of the payout currency.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Fixed Whether fixed rate is needed (optional overlay).
 * @prop {*} [fixed="void 0"] Whether fixed rate is needed. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Fixed_Safe Whether fixed rate is needed (required overlay).
 * @prop {*} fixed Whether fixed rate is needed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Float Whether float rate is needed (optional overlay).
 * @prop {*} [float="void 0"] Whether float rate is needed. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Float_Safe Whether float rate is needed (required overlay).
 * @prop {*} float Whether float rate is needed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Any Whether any rate is needed (optional overlay).
 * @prop {*} [any="void 0"] Whether any rate is needed. Default `void 0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Any_Safe Whether any rate is needed (required overlay).
 * @prop {*} any Whether any rate is needed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Ready Whether the intent has been established (optional overlay).
 * @prop {*} [ready=null] Whether the intent has been established. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Ready_Safe Whether the intent has been established (required overlay).
 * @prop {*} ready Whether the intent has been established.
 */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyFrom} xyz.swapee.wc.IExchangeIntentPort.Inputs.CurrencyFrom Ticker of the payin currency (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyFrom_Safe} xyz.swapee.wc.IExchangeIntentPort.Inputs.CurrencyFrom_Safe Ticker of the payin currency (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.IExchangeIntentPort.Inputs.AmountFrom Amount of currency the user is going to send, used to create a transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.IExchangeIntentPort.Inputs.AmountFrom_Safe Amount of currency the user is going to send, used to create a transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyTo} xyz.swapee.wc.IExchangeIntentPort.Inputs.CurrencyTo Ticker of the payout currency (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyTo_Safe} xyz.swapee.wc.IExchangeIntentPort.Inputs.CurrencyTo_Safe Ticker of the payout currency (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Fixed} xyz.swapee.wc.IExchangeIntentPort.Inputs.Fixed Whether fixed rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Fixed_Safe} xyz.swapee.wc.IExchangeIntentPort.Inputs.Fixed_Safe Whether fixed rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Float} xyz.swapee.wc.IExchangeIntentPort.Inputs.Float Whether float rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Float_Safe} xyz.swapee.wc.IExchangeIntentPort.Inputs.Float_Safe Whether float rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Any} xyz.swapee.wc.IExchangeIntentPort.Inputs.Any Whether any rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Any_Safe} xyz.swapee.wc.IExchangeIntentPort.Inputs.Any_Safe Whether any rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Ready} xyz.swapee.wc.IExchangeIntentPort.Inputs.Ready Whether the intent has been established (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Ready_Safe} xyz.swapee.wc.IExchangeIntentPort.Inputs.Ready_Safe Whether the intent has been established (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyFrom} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.CurrencyFrom Ticker of the payin currency (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyFrom_Safe} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.CurrencyFrom_Safe Ticker of the payin currency (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.AmountFrom} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.AmountFrom Amount of currency the user is going to send, used to create a transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.AmountFrom_Safe} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.AmountFrom_Safe Amount of currency the user is going to send, used to create a transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyTo} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.CurrencyTo Ticker of the payout currency (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.CurrencyTo_Safe} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.CurrencyTo_Safe Ticker of the payout currency (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Fixed} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Fixed Whether fixed rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Fixed_Safe} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Fixed_Safe Whether fixed rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Float} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Float Whether float rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Float_Safe} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Float_Safe Whether float rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Any} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Any Whether any rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Any_Safe} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Any_Safe Whether any rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Ready} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Ready Whether the intent has been established (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.Ready_Safe} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.Ready_Safe Whether the intent has been established (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyFrom} xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom Ticker of the payin currency (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyFrom_Safe} xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe Ticker of the payin currency (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.AmountFrom} xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom Amount of currency the user is going to send, used to create a transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.AmountFrom_Safe} xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe Amount of currency the user is going to send, used to create a transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyTo} xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo Ticker of the payout currency (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.CurrencyTo_Safe} xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe Ticker of the payout currency (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Fixed} xyz.swapee.wc.IExchangeIntentCore.Model.Fixed Whether fixed rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Fixed_Safe} xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe Whether fixed rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Float} xyz.swapee.wc.IExchangeIntentCore.Model.Float Whether float rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Float_Safe} xyz.swapee.wc.IExchangeIntentCore.Model.Float_Safe Whether float rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Any} xyz.swapee.wc.IExchangeIntentCore.Model.Any Whether any rate is needed (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Any_Safe} xyz.swapee.wc.IExchangeIntentCore.Model.Any_Safe Whether any rate is needed (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Ready} xyz.swapee.wc.IExchangeIntentCore.Model.Ready Whether the intent has been established (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model.Ready_Safe} xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe Whether the intent has been established (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/04-IExchangeIntentPort.xml}  fa24d10bcb4e0da3ac9fb4df3bb3959a */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeIntentPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentPort)} xyz.swapee.wc.AbstractExchangeIntentPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentPort} xyz.swapee.wc.ExchangeIntentPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentPort
 */
xyz.swapee.wc.AbstractExchangeIntentPort = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentPort.constructor&xyz.swapee.wc.ExchangeIntentPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentPort.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentPort|typeof xyz.swapee.wc.ExchangeIntentPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentPort}
 */
xyz.swapee.wc.AbstractExchangeIntentPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentPort}
 */
xyz.swapee.wc.AbstractExchangeIntentPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentPort|typeof xyz.swapee.wc.ExchangeIntentPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentPort}
 */
xyz.swapee.wc.AbstractExchangeIntentPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentPort|typeof xyz.swapee.wc.ExchangeIntentPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentPort}
 */
xyz.swapee.wc.AbstractExchangeIntentPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentPort.Initialese[]) => xyz.swapee.wc.IExchangeIntentPort} xyz.swapee.wc.ExchangeIntentPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeIntentPort.Inputs>)} xyz.swapee.wc.IExchangeIntentPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IExchangeIntent_, providing input
 * pins.
 * @interface xyz.swapee.wc.IExchangeIntentPort
 */
xyz.swapee.wc.IExchangeIntentPort = class extends /** @type {xyz.swapee.wc.IExchangeIntentPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIntentPort.resetPort} */
xyz.swapee.wc.IExchangeIntentPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentPort.resetExchangeIntentPort} */
xyz.swapee.wc.IExchangeIntentPort.prototype.resetExchangeIntentPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentPort.Initialese>)} xyz.swapee.wc.ExchangeIntentPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentPort} xyz.swapee.wc.IExchangeIntentPort.typeof */
/**
 * A concrete class of _IExchangeIntentPort_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentPort
 * @implements {xyz.swapee.wc.IExchangeIntentPort} The port that serves as an interface to the _IExchangeIntent_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentPort = class extends /** @type {xyz.swapee.wc.ExchangeIntentPort.constructor&xyz.swapee.wc.IExchangeIntentPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentPort}
 */
xyz.swapee.wc.ExchangeIntentPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentPort.
 * @interface xyz.swapee.wc.IExchangeIntentPortFields
 */
xyz.swapee.wc.IExchangeIntentPortFields = class { }
/**
 * The inputs to the _IExchangeIntent_'s controller via its port.
 */
xyz.swapee.wc.IExchangeIntentPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIntentPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeIntentPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeIntentPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentPort} */
xyz.swapee.wc.RecordIExchangeIntentPort

/** @typedef {xyz.swapee.wc.IExchangeIntentPort} xyz.swapee.wc.BoundIExchangeIntentPort */

/** @typedef {xyz.swapee.wc.ExchangeIntentPort} xyz.swapee.wc.BoundExchangeIntentPort */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel)} xyz.swapee.wc.IExchangeIntentPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel} xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IExchangeIntent_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeIntentPort.Inputs
 */
xyz.swapee.wc.IExchangeIntentPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeIntentPort.Inputs.constructor&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIntentPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeIntentPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel)} xyz.swapee.wc.IExchangeIntentPort.WeakInputs.constructor */
/**
 * The inputs to the _IExchangeIntent_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeIntentPort.WeakInputs
 */
xyz.swapee.wc.IExchangeIntentPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeIntentPort.WeakInputs.constructor&xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIntentPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeIntentPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IExchangeIntentPortInterface
 */
xyz.swapee.wc.IExchangeIntentPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IExchangeIntentPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IExchangeIntentPortInterface.prototype.constructor = xyz.swapee.wc.IExchangeIntentPortInterface

/**
 * A concrete class of _IExchangeIntentPortInterface_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentPortInterface
 * @implements {xyz.swapee.wc.IExchangeIntentPortInterface} The port interface.
 */
xyz.swapee.wc.ExchangeIntentPortInterface = class extends xyz.swapee.wc.IExchangeIntentPortInterface { }
xyz.swapee.wc.ExchangeIntentPortInterface.prototype.constructor = xyz.swapee.wc.ExchangeIntentPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentPortInterface.Props
 * @prop {boolean} ready Whether the intent has been established.
 * @prop {string} [currencyFrom="void 0"] Ticker of the payin currency. Default `void 0`.
 * @prop {string} [amountFrom="void 0"] Amount of currency the user is going to send, used to create a transaction. Default `void 0`.
 * @prop {string} [currencyTo="void 0"] Ticker of the payout currency. Default `void 0`.
 * @prop {boolean} [fixed=void 0] Whether fixed rate is needed. Default `void 0`.
 * @prop {boolean} [float=void 0] Whether float rate is needed. Default `void 0`.
 * @prop {boolean} [any=void 0] Whether any rate is needed. Default `void 0`.
 */

/**
 * Contains getters to cast the _IExchangeIntentPort_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentPortCaster
 */
xyz.swapee.wc.IExchangeIntentPortCaster = class { }
/**
 * Cast the _IExchangeIntentPort_ instance into the _BoundIExchangeIntentPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentPort}
 */
xyz.swapee.wc.IExchangeIntentPortCaster.prototype.asIExchangeIntentPort
/**
 * Access the _ExchangeIntentPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentPort}
 */
xyz.swapee.wc.IExchangeIntentPortCaster.prototype.superExchangeIntentPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentPort.__resetPort<!xyz.swapee.wc.IExchangeIntentPort>} xyz.swapee.wc.IExchangeIntentPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentPort.resetPort} */
/**
 * Resets the _IExchangeIntent_ port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentPort.__resetExchangeIntentPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentPort.__resetExchangeIntentPort<!xyz.swapee.wc.IExchangeIntentPort>} xyz.swapee.wc.IExchangeIntentPort._resetExchangeIntentPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentPort.resetExchangeIntentPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentPort.resetExchangeIntentPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIntentPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/09-IExchangeIntentCore.xml}  edd188a5094b8fd495b99ed27e227c9a */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeIntentCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentCore)} xyz.swapee.wc.AbstractExchangeIntentCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentCore} xyz.swapee.wc.ExchangeIntentCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentCore
 */
xyz.swapee.wc.AbstractExchangeIntentCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentCore.constructor&xyz.swapee.wc.ExchangeIntentCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentCore|typeof xyz.swapee.wc.ExchangeIntentCore)|(!xyz.swapee.wc.IExchangeIntentOuterCore|typeof xyz.swapee.wc.ExchangeIntentOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentCore}
 */
xyz.swapee.wc.AbstractExchangeIntentCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentCore}
 */
xyz.swapee.wc.AbstractExchangeIntentCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentCore|typeof xyz.swapee.wc.ExchangeIntentCore)|(!xyz.swapee.wc.IExchangeIntentOuterCore|typeof xyz.swapee.wc.ExchangeIntentOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentCore}
 */
xyz.swapee.wc.AbstractExchangeIntentCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentCore|typeof xyz.swapee.wc.ExchangeIntentCore)|(!xyz.swapee.wc.IExchangeIntentOuterCore|typeof xyz.swapee.wc.ExchangeIntentOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentCore}
 */
xyz.swapee.wc.AbstractExchangeIntentCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentCoreCaster&xyz.swapee.wc.IExchangeIntentOuterCore)} xyz.swapee.wc.IExchangeIntentCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IExchangeIntentCore
 */
xyz.swapee.wc.IExchangeIntentCore = class extends /** @type {xyz.swapee.wc.IExchangeIntentCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIntentOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IExchangeIntentCore.resetCore} */
xyz.swapee.wc.IExchangeIntentCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentCore.resetExchangeIntentCore} */
xyz.swapee.wc.IExchangeIntentCore.prototype.resetExchangeIntentCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentCore.Initialese>)} xyz.swapee.wc.ExchangeIntentCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentCore} xyz.swapee.wc.IExchangeIntentCore.typeof */
/**
 * A concrete class of _IExchangeIntentCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentCore
 * @implements {xyz.swapee.wc.IExchangeIntentCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentCore = class extends /** @type {xyz.swapee.wc.ExchangeIntentCore.constructor&xyz.swapee.wc.IExchangeIntentCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIntentCore.prototype.constructor = xyz.swapee.wc.ExchangeIntentCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentCore}
 */
xyz.swapee.wc.ExchangeIntentCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentCore.
 * @interface xyz.swapee.wc.IExchangeIntentCoreFields
 */
xyz.swapee.wc.IExchangeIntentCoreFields = class { }
/**
 * The _IExchangeIntent_'s memory.
 */
xyz.swapee.wc.IExchangeIntentCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeIntentCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IExchangeIntentCoreFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeIntentCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentCore} */
xyz.swapee.wc.RecordIExchangeIntentCore

/** @typedef {xyz.swapee.wc.IExchangeIntentCore} xyz.swapee.wc.BoundIExchangeIntentCore */

/** @typedef {xyz.swapee.wc.ExchangeIntentCore} xyz.swapee.wc.BoundExchangeIntentCore */

/** @typedef {xyz.swapee.wc.IExchangeIntentOuterCore.Model} xyz.swapee.wc.IExchangeIntentCore.Model The _IExchangeIntent_'s memory. */

/**
 * Contains getters to cast the _IExchangeIntentCore_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentCoreCaster
 */
xyz.swapee.wc.IExchangeIntentCoreCaster = class { }
/**
 * Cast the _IExchangeIntentCore_ instance into the _BoundIExchangeIntentCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentCore}
 */
xyz.swapee.wc.IExchangeIntentCoreCaster.prototype.asIExchangeIntentCore
/**
 * Access the _ExchangeIntentCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentCore}
 */
xyz.swapee.wc.IExchangeIntentCoreCaster.prototype.superExchangeIntentCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentCore.__resetCore<!xyz.swapee.wc.IExchangeIntentCore>} xyz.swapee.wc.IExchangeIntentCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentCore.resetCore} */
/**
 * Resets the _IExchangeIntent_ core.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentCore.__resetExchangeIntentCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentCore.__resetExchangeIntentCore<!xyz.swapee.wc.IExchangeIntentCore>} xyz.swapee.wc.IExchangeIntentCore._resetExchangeIntentCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentCore.resetExchangeIntentCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentCore.resetExchangeIntentCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIntentCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/10-IExchangeIntentProcessor.xml}  b5df3b9c6cee1f7d71c7492bd07bc3f6 */
/** @typedef {xyz.swapee.wc.IExchangeIntentComputer.Initialese&xyz.swapee.wc.IExchangeIntentController.Initialese} xyz.swapee.wc.IExchangeIntentProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentProcessor)} xyz.swapee.wc.AbstractExchangeIntentProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentProcessor} xyz.swapee.wc.ExchangeIntentProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentProcessor
 */
xyz.swapee.wc.AbstractExchangeIntentProcessor = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentProcessor.constructor&xyz.swapee.wc.ExchangeIntentProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentProcessor.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!xyz.swapee.wc.IExchangeIntentCore|typeof xyz.swapee.wc.ExchangeIntentCore)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentProcessor}
 */
xyz.swapee.wc.AbstractExchangeIntentProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentProcessor}
 */
xyz.swapee.wc.AbstractExchangeIntentProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!xyz.swapee.wc.IExchangeIntentCore|typeof xyz.swapee.wc.ExchangeIntentCore)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentProcessor}
 */
xyz.swapee.wc.AbstractExchangeIntentProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!xyz.swapee.wc.IExchangeIntentCore|typeof xyz.swapee.wc.ExchangeIntentCore)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentProcessor}
 */
xyz.swapee.wc.AbstractExchangeIntentProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentProcessor.Initialese[]) => xyz.swapee.wc.IExchangeIntentProcessor} xyz.swapee.wc.ExchangeIntentProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentProcessorCaster&xyz.swapee.wc.IExchangeIntentComputer&xyz.swapee.wc.IExchangeIntentCore&xyz.swapee.wc.IExchangeIntentController)} xyz.swapee.wc.IExchangeIntentProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController} xyz.swapee.wc.IExchangeIntentController.typeof */
/**
 * The processor to compute changes to the memory for the _IExchangeIntent_.
 * @interface xyz.swapee.wc.IExchangeIntentProcessor
 */
xyz.swapee.wc.IExchangeIntentProcessor = class extends /** @type {xyz.swapee.wc.IExchangeIntentProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIntentComputer.typeof&xyz.swapee.wc.IExchangeIntentCore.typeof&xyz.swapee.wc.IExchangeIntentController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentProcessor.Initialese>)} xyz.swapee.wc.ExchangeIntentProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentProcessor} xyz.swapee.wc.IExchangeIntentProcessor.typeof */
/**
 * A concrete class of _IExchangeIntentProcessor_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentProcessor
 * @implements {xyz.swapee.wc.IExchangeIntentProcessor} The processor to compute changes to the memory for the _IExchangeIntent_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentProcessor.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentProcessor = class extends /** @type {xyz.swapee.wc.ExchangeIntentProcessor.constructor&xyz.swapee.wc.IExchangeIntentProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentProcessor}
 */
xyz.swapee.wc.ExchangeIntentProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentProcessor} */
xyz.swapee.wc.RecordIExchangeIntentProcessor

/** @typedef {xyz.swapee.wc.IExchangeIntentProcessor} xyz.swapee.wc.BoundIExchangeIntentProcessor */

/** @typedef {xyz.swapee.wc.ExchangeIntentProcessor} xyz.swapee.wc.BoundExchangeIntentProcessor */

/**
 * Contains getters to cast the _IExchangeIntentProcessor_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentProcessorCaster
 */
xyz.swapee.wc.IExchangeIntentProcessorCaster = class { }
/**
 * Cast the _IExchangeIntentProcessor_ instance into the _BoundIExchangeIntentProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentProcessor}
 */
xyz.swapee.wc.IExchangeIntentProcessorCaster.prototype.asIExchangeIntentProcessor
/**
 * Access the _ExchangeIntentProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentProcessor}
 */
xyz.swapee.wc.IExchangeIntentProcessorCaster.prototype.superExchangeIntentProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/100-ExchangeIntentMemory.xml}  c64b4774aae21f68a4c9b9bdea309859 */
/**
 * The memory of the _IExchangeIntent_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.ExchangeIntentMemory
 */
xyz.swapee.wc.ExchangeIntentMemory = class { }
/**
 * Ticker of the payin currency. Default empty string.
 */
xyz.swapee.wc.ExchangeIntentMemory.prototype.currencyFrom = /** @type {string} */ (void 0)
/**
 * Amount of currency the user is going to send, used to create a transaction. Default empty string.
 */
xyz.swapee.wc.ExchangeIntentMemory.prototype.amountFrom = /** @type {string} */ (void 0)
/**
 * Ticker of the payout currency. Default empty string.
 */
xyz.swapee.wc.ExchangeIntentMemory.prototype.currencyTo = /** @type {string} */ (void 0)
/**
 * Whether fixed rate is needed. Default `false`.
 */
xyz.swapee.wc.ExchangeIntentMemory.prototype.fixed = /** @type {boolean} */ (void 0)
/**
 * Whether float rate is needed. Default `false`.
 */
xyz.swapee.wc.ExchangeIntentMemory.prototype.float = /** @type {boolean} */ (void 0)
/**
 * Whether any rate is needed. Default `false`.
 */
xyz.swapee.wc.ExchangeIntentMemory.prototype.any = /** @type {boolean} */ (void 0)
/**
 * Whether the intent has been established. Default `false`.
 */
xyz.swapee.wc.ExchangeIntentMemory.prototype.ready = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/102-ExchangeIntentInputs.xml}  de41064940c23f8f718fa8f70c4bffb6 */
/**
 * The inputs of the _IExchangeIntent_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.ExchangeIntentInputs
 */
xyz.swapee.wc.front.ExchangeIntentInputs = class { }
/**
 * Ticker of the payin currency. Default `void 0`.
 */
xyz.swapee.wc.front.ExchangeIntentInputs.prototype.currencyFrom = /** @type {string|undefined} */ (void 0)
/**
 * Amount of currency the user is going to send, used to create a transaction. Default `void 0`.
 */
xyz.swapee.wc.front.ExchangeIntentInputs.prototype.amountFrom = /** @type {string|undefined} */ (void 0)
/**
 * Ticker of the payout currency. Default `void 0`.
 */
xyz.swapee.wc.front.ExchangeIntentInputs.prototype.currencyTo = /** @type {string|undefined} */ (void 0)
/**
 * Whether fixed rate is needed. Default `void 0`.
 */
xyz.swapee.wc.front.ExchangeIntentInputs.prototype.fixed = /** @type {boolean|undefined} */ (void 0)
/**
 * Whether float rate is needed. Default `void 0`.
 */
xyz.swapee.wc.front.ExchangeIntentInputs.prototype.float = /** @type {boolean|undefined} */ (void 0)
/**
 * Whether any rate is needed. Default `void 0`.
 */
xyz.swapee.wc.front.ExchangeIntentInputs.prototype.any = /** @type {boolean|undefined} */ (void 0)
/**
 * Whether the intent has been established. Default `false`.
 */
xyz.swapee.wc.front.ExchangeIntentInputs.prototype.ready = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/11-IExchangeIntent.xml}  ae2702ac1f5431afe172fe625109fbbf */
/**
 * An atomic wrapper for the _IExchangeIntent_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.ExchangeIntentEnv
 */
xyz.swapee.wc.ExchangeIntentEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.ExchangeIntentEnv.prototype.exchangeIntent = /** @type {xyz.swapee.wc.IExchangeIntent} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.IExchangeIntentController.Inputs>&xyz.swapee.wc.IExchangeIntentProcessor.Initialese&xyz.swapee.wc.IExchangeIntentComputer.Initialese&xyz.swapee.wc.IExchangeIntentController.Initialese} xyz.swapee.wc.IExchangeIntent.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntent)} xyz.swapee.wc.AbstractExchangeIntent.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntent} xyz.swapee.wc.ExchangeIntent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntent` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntent
 */
xyz.swapee.wc.AbstractExchangeIntent = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntent.constructor&xyz.swapee.wc.ExchangeIntent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntent.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntent.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntent|typeof xyz.swapee.wc.ExchangeIntent)|(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntent}
 */
xyz.swapee.wc.AbstractExchangeIntent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntent}
 */
xyz.swapee.wc.AbstractExchangeIntent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntent|typeof xyz.swapee.wc.ExchangeIntent)|(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntent}
 */
xyz.swapee.wc.AbstractExchangeIntent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntent|typeof xyz.swapee.wc.ExchangeIntent)|(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntent}
 */
xyz.swapee.wc.AbstractExchangeIntent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntent.Initialese[]) => xyz.swapee.wc.IExchangeIntent} xyz.swapee.wc.ExchangeIntentConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntent.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.ExchangeIntentClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentCaster&xyz.swapee.wc.IExchangeIntentProcessor&xyz.swapee.wc.IExchangeIntentComputer&xyz.swapee.wc.IExchangeIntentController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.IExchangeIntentController.Inputs, null>)} xyz.swapee.wc.IExchangeIntent.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IExchangeIntent
 */
xyz.swapee.wc.IExchangeIntent = class extends /** @type {xyz.swapee.wc.IExchangeIntent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIntentProcessor.typeof&xyz.swapee.wc.IExchangeIntentComputer.typeof&xyz.swapee.wc.IExchangeIntentController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntent&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntent.Initialese>)} xyz.swapee.wc.ExchangeIntent.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntent} xyz.swapee.wc.IExchangeIntent.typeof */
/**
 * A concrete class of _IExchangeIntent_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntent
 * @implements {xyz.swapee.wc.IExchangeIntent} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntent.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntent = class extends /** @type {xyz.swapee.wc.ExchangeIntent.constructor&xyz.swapee.wc.IExchangeIntent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntent}
 */
xyz.swapee.wc.ExchangeIntent.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntent.
 * @interface xyz.swapee.wc.IExchangeIntentFields
 */
xyz.swapee.wc.IExchangeIntentFields = class { }
/**
 * The input pins of the _IExchangeIntent_ port.
 */
xyz.swapee.wc.IExchangeIntentFields.prototype.pinout = /** @type {!xyz.swapee.wc.IExchangeIntent.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntent} */
xyz.swapee.wc.RecordIExchangeIntent

/** @typedef {xyz.swapee.wc.IExchangeIntent} xyz.swapee.wc.BoundIExchangeIntent */

/** @typedef {xyz.swapee.wc.ExchangeIntent} xyz.swapee.wc.BoundExchangeIntent */

/** @typedef {xyz.swapee.wc.IExchangeIntentController.Inputs} xyz.swapee.wc.IExchangeIntent.Pinout The input pins of the _IExchangeIntent_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeIntentController.Inputs>)} xyz.swapee.wc.IExchangeIntentBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IExchangeIntentBuffer
 */
xyz.swapee.wc.IExchangeIntentBuffer = class extends /** @type {xyz.swapee.wc.IExchangeIntentBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIntentBuffer.prototype.constructor = xyz.swapee.wc.IExchangeIntentBuffer

/**
 * A concrete class of _IExchangeIntentBuffer_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentBuffer
 * @implements {xyz.swapee.wc.IExchangeIntentBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.ExchangeIntentBuffer = class extends xyz.swapee.wc.IExchangeIntentBuffer { }
xyz.swapee.wc.ExchangeIntentBuffer.prototype.constructor = xyz.swapee.wc.ExchangeIntentBuffer

/**
 * Contains getters to cast the _IExchangeIntent_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentCaster
 */
xyz.swapee.wc.IExchangeIntentCaster = class { }
/**
 * Cast the _IExchangeIntent_ instance into the _BoundIExchangeIntent_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntent}
 */
xyz.swapee.wc.IExchangeIntentCaster.prototype.asIExchangeIntent
/**
 * Access the _ExchangeIntent_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntent}
 */
xyz.swapee.wc.IExchangeIntentCaster.prototype.superExchangeIntent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/110-ExchangeIntentSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeIntentMemoryPQs
 */
xyz.swapee.wc.ExchangeIntentMemoryPQs = class {
  constructor() {
    /**
     * `j6c88`
     */
    this.currencyFrom=/** @type {string} */ (void 0)
    /**
     * `h48e6`
     */
    this.amountFrom=/** @type {string} */ (void 0)
    /**
     * `c23cd`
     */
    this.currencyTo=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeIntentMemoryPQs.prototype.constructor = xyz.swapee.wc.ExchangeIntentMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeIntentMemoryQPs
 * @dict
 */
xyz.swapee.wc.ExchangeIntentMemoryQPs = class { }
/**
 * `currencyFrom`
 */
xyz.swapee.wc.ExchangeIntentMemoryQPs.prototype.j6c88 = /** @type {string} */ (void 0)
/**
 * `amountFrom`
 */
xyz.swapee.wc.ExchangeIntentMemoryQPs.prototype.h48e6 = /** @type {string} */ (void 0)
/**
 * `currencyTo`
 */
xyz.swapee.wc.ExchangeIntentMemoryQPs.prototype.c23cd = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentMemoryPQs)} xyz.swapee.wc.ExchangeIntentInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentMemoryPQs} xyz.swapee.wc.ExchangeIntentMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeIntentInputsPQs
 */
xyz.swapee.wc.ExchangeIntentInputsPQs = class extends /** @type {xyz.swapee.wc.ExchangeIntentInputsPQs.constructor&xyz.swapee.wc.ExchangeIntentMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIntentInputsPQs.prototype.constructor = xyz.swapee.wc.ExchangeIntentInputsPQs

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentMemoryPQs)} xyz.swapee.wc.ExchangeIntentInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeIntentInputsQPs
 * @dict
 */
xyz.swapee.wc.ExchangeIntentInputsQPs = class extends /** @type {xyz.swapee.wc.ExchangeIntentInputsQPs.constructor&xyz.swapee.wc.ExchangeIntentMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIntentInputsQPs.prototype.constructor = xyz.swapee.wc.ExchangeIntentInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeIntentVdusPQs
 */
xyz.swapee.wc.ExchangeIntentVdusPQs = class { }
xyz.swapee.wc.ExchangeIntentVdusPQs.prototype.constructor = xyz.swapee.wc.ExchangeIntentVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeIntentVdusQPs
 * @dict
 */
xyz.swapee.wc.ExchangeIntentVdusQPs = class { }
xyz.swapee.wc.ExchangeIntentVdusQPs.prototype.constructor = xyz.swapee.wc.ExchangeIntentVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/12-IExchangeIntentHtmlComponent.xml}  31ba5b743e9a1d559c285567b4efaaaa */
/** @typedef {xyz.swapee.wc.back.IExchangeIntentController.Initialese&xyz.swapee.wc.back.IExchangeIntentScreen.Initialese&xyz.swapee.wc.IExchangeIntent.Initialese&xyz.swapee.wc.IExchangeIntentGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IExchangeIntentProcessor.Initialese&xyz.swapee.wc.IExchangeIntentComputer.Initialese} xyz.swapee.wc.IExchangeIntentHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentHtmlComponent)} xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentHtmlComponent} xyz.swapee.wc.ExchangeIntentHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentHtmlComponent
 */
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.constructor&xyz.swapee.wc.ExchangeIntentHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentHtmlComponent|typeof xyz.swapee.wc.ExchangeIntentHtmlComponent)|(!xyz.swapee.wc.back.IExchangeIntentController|typeof xyz.swapee.wc.back.ExchangeIntentController)|(!xyz.swapee.wc.back.IExchangeIntentScreen|typeof xyz.swapee.wc.back.ExchangeIntentScreen)|(!xyz.swapee.wc.IExchangeIntent|typeof xyz.swapee.wc.ExchangeIntent)|(!xyz.swapee.wc.IExchangeIntentGPU|typeof xyz.swapee.wc.ExchangeIntentGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentHtmlComponent|typeof xyz.swapee.wc.ExchangeIntentHtmlComponent)|(!xyz.swapee.wc.back.IExchangeIntentController|typeof xyz.swapee.wc.back.ExchangeIntentController)|(!xyz.swapee.wc.back.IExchangeIntentScreen|typeof xyz.swapee.wc.back.ExchangeIntentScreen)|(!xyz.swapee.wc.IExchangeIntent|typeof xyz.swapee.wc.ExchangeIntent)|(!xyz.swapee.wc.IExchangeIntentGPU|typeof xyz.swapee.wc.ExchangeIntentGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentHtmlComponent|typeof xyz.swapee.wc.ExchangeIntentHtmlComponent)|(!xyz.swapee.wc.back.IExchangeIntentController|typeof xyz.swapee.wc.back.ExchangeIntentController)|(!xyz.swapee.wc.back.IExchangeIntentScreen|typeof xyz.swapee.wc.back.ExchangeIntentScreen)|(!xyz.swapee.wc.IExchangeIntent|typeof xyz.swapee.wc.ExchangeIntent)|(!xyz.swapee.wc.IExchangeIntentGPU|typeof xyz.swapee.wc.ExchangeIntentGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeIntentProcessor|typeof xyz.swapee.wc.ExchangeIntentProcessor)|(!xyz.swapee.wc.IExchangeIntentComputer|typeof xyz.swapee.wc.ExchangeIntentComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIntentHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentHtmlComponent.Initialese[]) => xyz.swapee.wc.IExchangeIntentHtmlComponent} xyz.swapee.wc.ExchangeIntentHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentHtmlComponentCaster&xyz.swapee.wc.back.IExchangeIntentController&xyz.swapee.wc.back.IExchangeIntentScreen&xyz.swapee.wc.IExchangeIntent&xyz.swapee.wc.IExchangeIntentGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.IExchangeIntentController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IExchangeIntentProcessor&xyz.swapee.wc.IExchangeIntentComputer)} xyz.swapee.wc.IExchangeIntentHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIntentController} xyz.swapee.wc.back.IExchangeIntentController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIntentScreen} xyz.swapee.wc.back.IExchangeIntentScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentGPU} xyz.swapee.wc.IExchangeIntentGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IExchangeIntent_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IExchangeIntentHtmlComponent
 */
xyz.swapee.wc.IExchangeIntentHtmlComponent = class extends /** @type {xyz.swapee.wc.IExchangeIntentHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeIntentController.typeof&xyz.swapee.wc.back.IExchangeIntentScreen.typeof&xyz.swapee.wc.IExchangeIntent.typeof&xyz.swapee.wc.IExchangeIntentGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IExchangeIntentProcessor.typeof&xyz.swapee.wc.IExchangeIntentComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentHtmlComponent.Initialese>)} xyz.swapee.wc.ExchangeIntentHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentHtmlComponent} xyz.swapee.wc.IExchangeIntentHtmlComponent.typeof */
/**
 * A concrete class of _IExchangeIntentHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentHtmlComponent
 * @implements {xyz.swapee.wc.IExchangeIntentHtmlComponent} The _IExchangeIntent_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentHtmlComponent = class extends /** @type {xyz.swapee.wc.ExchangeIntentHtmlComponent.constructor&xyz.swapee.wc.IExchangeIntentHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentHtmlComponent}
 */
xyz.swapee.wc.ExchangeIntentHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentHtmlComponent} */
xyz.swapee.wc.RecordIExchangeIntentHtmlComponent

/** @typedef {xyz.swapee.wc.IExchangeIntentHtmlComponent} xyz.swapee.wc.BoundIExchangeIntentHtmlComponent */

/** @typedef {xyz.swapee.wc.ExchangeIntentHtmlComponent} xyz.swapee.wc.BoundExchangeIntentHtmlComponent */

/**
 * Contains getters to cast the _IExchangeIntentHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentHtmlComponentCaster
 */
xyz.swapee.wc.IExchangeIntentHtmlComponentCaster = class { }
/**
 * Cast the _IExchangeIntentHtmlComponent_ instance into the _BoundIExchangeIntentHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentHtmlComponent}
 */
xyz.swapee.wc.IExchangeIntentHtmlComponentCaster.prototype.asIExchangeIntentHtmlComponent
/**
 * Access the _ExchangeIntentHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentHtmlComponent}
 */
xyz.swapee.wc.IExchangeIntentHtmlComponentCaster.prototype.superExchangeIntentHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/130-IExchangeIntentElement.xml}  c674d3ca69390b2138db433eb16075cc */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.IExchangeIntentElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IExchangeIntentElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentElement)} xyz.swapee.wc.AbstractExchangeIntentElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentElement} xyz.swapee.wc.ExchangeIntentElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentElement` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentElement
 */
xyz.swapee.wc.AbstractExchangeIntentElement = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentElement.constructor&xyz.swapee.wc.ExchangeIntentElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentElement.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentElement.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentElement|typeof xyz.swapee.wc.ExchangeIntentElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentElement}
 */
xyz.swapee.wc.AbstractExchangeIntentElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElement}
 */
xyz.swapee.wc.AbstractExchangeIntentElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentElement|typeof xyz.swapee.wc.ExchangeIntentElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElement}
 */
xyz.swapee.wc.AbstractExchangeIntentElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentElement|typeof xyz.swapee.wc.ExchangeIntentElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElement}
 */
xyz.swapee.wc.AbstractExchangeIntentElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentElement.Initialese[]) => xyz.swapee.wc.IExchangeIntentElement} xyz.swapee.wc.ExchangeIntentElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentElementFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.IExchangeIntentElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.IExchangeIntentElement.Inputs, null>)} xyz.swapee.wc.IExchangeIntentElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IExchangeIntent_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IExchangeIntentElement
 */
xyz.swapee.wc.IExchangeIntentElement = class extends /** @type {xyz.swapee.wc.IExchangeIntentElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIntentElement.solder} */
xyz.swapee.wc.IExchangeIntentElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentElement.render} */
xyz.swapee.wc.IExchangeIntentElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentElement.server} */
xyz.swapee.wc.IExchangeIntentElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentElement.inducer} */
xyz.swapee.wc.IExchangeIntentElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentElement&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentElement.Initialese>)} xyz.swapee.wc.ExchangeIntentElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElement} xyz.swapee.wc.IExchangeIntentElement.typeof */
/**
 * A concrete class of _IExchangeIntentElement_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentElement
 * @implements {xyz.swapee.wc.IExchangeIntentElement} A component description.
 *
 * The _IExchangeIntent_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentElement.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentElement = class extends /** @type {xyz.swapee.wc.ExchangeIntentElement.constructor&xyz.swapee.wc.IExchangeIntentElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentElement}
 */
xyz.swapee.wc.ExchangeIntentElement.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentElement.
 * @interface xyz.swapee.wc.IExchangeIntentElementFields
 */
xyz.swapee.wc.IExchangeIntentElementFields = class { }
/**
 * The element-specific inputs to the _IExchangeIntent_ component.
 */
xyz.swapee.wc.IExchangeIntentElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIntentElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentElement} */
xyz.swapee.wc.RecordIExchangeIntentElement

/** @typedef {xyz.swapee.wc.IExchangeIntentElement} xyz.swapee.wc.BoundIExchangeIntentElement */

/** @typedef {xyz.swapee.wc.ExchangeIntentElement} xyz.swapee.wc.BoundExchangeIntentElement */

/** @typedef {xyz.swapee.wc.IExchangeIntentPort.Inputs&xyz.swapee.wc.IExchangeIntentDisplay.Queries&xyz.swapee.wc.IExchangeIntentController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IExchangeIntentElementPort.Inputs} xyz.swapee.wc.IExchangeIntentElement.Inputs The element-specific inputs to the _IExchangeIntent_ component. */

/**
 * Contains getters to cast the _IExchangeIntentElement_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentElementCaster
 */
xyz.swapee.wc.IExchangeIntentElementCaster = class { }
/**
 * Cast the _IExchangeIntentElement_ instance into the _BoundIExchangeIntentElement_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentElement}
 */
xyz.swapee.wc.IExchangeIntentElementCaster.prototype.asIExchangeIntentElement
/**
 * Access the _ExchangeIntentElement_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentElement}
 */
xyz.swapee.wc.IExchangeIntentElementCaster.prototype.superExchangeIntentElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ExchangeIntentMemory, props: !xyz.swapee.wc.IExchangeIntentElement.Inputs) => Object<string, *>} xyz.swapee.wc.IExchangeIntentElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentElement.__solder<!xyz.swapee.wc.IExchangeIntentElement>} xyz.swapee.wc.IExchangeIntentElement._solder */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.ExchangeIntentMemory} model The model.
 * - `currencyFrom` _string_ Ticker of the payin currency. Default empty string.
 * - `amountFrom` _string_ Amount of currency the user is going to send, used to create a transaction. Default empty string.
 * - `currencyTo` _string_ Ticker of the payout currency. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeIntentElement.Inputs} props The element props.
 * - `[currencyFrom=null]` _&#42;?_ Ticker of the payin currency. ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyFrom* ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyFrom* Default `null`.
 * - `[amountFrom=null]` _&#42;?_ Amount of currency the user is going to send, used to create a transaction. ⤴ *IExchangeIntentOuterCore.WeakModel.AmountFrom* ⤴ *IExchangeIntentOuterCore.WeakModel.AmountFrom* Default `null`.
 * - `[currencyTo=null]` _&#42;?_ Ticker of the payout currency. ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyTo* ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyTo* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeIntentElementPort.Inputs.NoSolder* Default `false`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IExchangeIntentElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeIntentMemory, instance?: !xyz.swapee.wc.IExchangeIntentScreen&xyz.swapee.wc.IExchangeIntentController) => !engineering.type.VNode} xyz.swapee.wc.IExchangeIntentElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentElement.__render<!xyz.swapee.wc.IExchangeIntentElement>} xyz.swapee.wc.IExchangeIntentElement._render */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.ExchangeIntentMemory} [model] The model for the view.
 * - `currencyFrom` _string_ Ticker of the payin currency. Default empty string.
 * - `amountFrom` _string_ Amount of currency the user is going to send, used to create a transaction. Default empty string.
 * - `currencyTo` _string_ Ticker of the payout currency. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeIntentScreen&xyz.swapee.wc.IExchangeIntentController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeIntentElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeIntentMemory, inputs: !xyz.swapee.wc.IExchangeIntentElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IExchangeIntentElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentElement.__server<!xyz.swapee.wc.IExchangeIntentElement>} xyz.swapee.wc.IExchangeIntentElement._server */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.ExchangeIntentMemory} memory The memory registers.
 * - `currencyFrom` _string_ Ticker of the payin currency. Default empty string.
 * - `amountFrom` _string_ Amount of currency the user is going to send, used to create a transaction. Default empty string.
 * - `currencyTo` _string_ Ticker of the payout currency. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeIntentElement.Inputs} inputs The inputs to the port.
 * - `[currencyFrom=null]` _&#42;?_ Ticker of the payin currency. ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyFrom* ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyFrom* Default `null`.
 * - `[amountFrom=null]` _&#42;?_ Amount of currency the user is going to send, used to create a transaction. ⤴ *IExchangeIntentOuterCore.WeakModel.AmountFrom* ⤴ *IExchangeIntentOuterCore.WeakModel.AmountFrom* Default `null`.
 * - `[currencyTo=null]` _&#42;?_ Ticker of the payout currency. ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyTo* ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyTo* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeIntentElementPort.Inputs.NoSolder* Default `false`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeIntentElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeIntentMemory, port?: !xyz.swapee.wc.IExchangeIntentElement.Inputs) => ?} xyz.swapee.wc.IExchangeIntentElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentElement.__inducer<!xyz.swapee.wc.IExchangeIntentElement>} xyz.swapee.wc.IExchangeIntentElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.ExchangeIntentMemory} [model] The model of the component into which to induce the state.
 * - `currencyFrom` _string_ Ticker of the payin currency. Default empty string.
 * - `amountFrom` _string_ Amount of currency the user is going to send, used to create a transaction. Default empty string.
 * - `currencyTo` _string_ Ticker of the payout currency. Default empty string.
 * @param {!xyz.swapee.wc.IExchangeIntentElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[currencyFrom=null]` _&#42;?_ Ticker of the payin currency. ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyFrom* ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyFrom* Default `null`.
 * - `[amountFrom=null]` _&#42;?_ Amount of currency the user is going to send, used to create a transaction. ⤴ *IExchangeIntentOuterCore.WeakModel.AmountFrom* ⤴ *IExchangeIntentOuterCore.WeakModel.AmountFrom* Default `null`.
 * - `[currencyTo=null]` _&#42;?_ Ticker of the payout currency. ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyTo* ⤴ *IExchangeIntentOuterCore.WeakModel.CurrencyTo* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeIntentElementPort.Inputs.NoSolder* Default `false`.
 * @return {?}
 */
xyz.swapee.wc.IExchangeIntentElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIntentElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/140-IExchangeIntentElementPort.xml}  736dfc3f8e501b29795c102ec22597ec */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeIntentElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentElementPort)} xyz.swapee.wc.AbstractExchangeIntentElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentElementPort} xyz.swapee.wc.ExchangeIntentElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentElementPort
 */
xyz.swapee.wc.AbstractExchangeIntentElementPort = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentElementPort.constructor&xyz.swapee.wc.ExchangeIntentElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentElementPort.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentElementPort|typeof xyz.swapee.wc.ExchangeIntentElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentElementPort}
 */
xyz.swapee.wc.AbstractExchangeIntentElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElementPort}
 */
xyz.swapee.wc.AbstractExchangeIntentElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentElementPort|typeof xyz.swapee.wc.ExchangeIntentElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElementPort}
 */
xyz.swapee.wc.AbstractExchangeIntentElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentElementPort|typeof xyz.swapee.wc.ExchangeIntentElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentElementPort}
 */
xyz.swapee.wc.AbstractExchangeIntentElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentElementPort.Initialese[]) => xyz.swapee.wc.IExchangeIntentElementPort} xyz.swapee.wc.ExchangeIntentElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeIntentElementPort.Inputs>)} xyz.swapee.wc.IExchangeIntentElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IExchangeIntentElementPort
 */
xyz.swapee.wc.IExchangeIntentElementPort = class extends /** @type {xyz.swapee.wc.IExchangeIntentElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentElementPort.Initialese>)} xyz.swapee.wc.ExchangeIntentElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElementPort} xyz.swapee.wc.IExchangeIntentElementPort.typeof */
/**
 * A concrete class of _IExchangeIntentElementPort_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentElementPort
 * @implements {xyz.swapee.wc.IExchangeIntentElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentElementPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentElementPort = class extends /** @type {xyz.swapee.wc.ExchangeIntentElementPort.constructor&xyz.swapee.wc.IExchangeIntentElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentElementPort}
 */
xyz.swapee.wc.ExchangeIntentElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentElementPort.
 * @interface xyz.swapee.wc.IExchangeIntentElementPortFields
 */
xyz.swapee.wc.IExchangeIntentElementPortFields = class { }
/**
 * The inputs to the _IExchangeIntentElement_'s controller via its element port.
 */
xyz.swapee.wc.IExchangeIntentElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIntentElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeIntentElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeIntentElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentElementPort} */
xyz.swapee.wc.RecordIExchangeIntentElementPort

/** @typedef {xyz.swapee.wc.IExchangeIntentElementPort} xyz.swapee.wc.BoundIExchangeIntentElementPort */

/** @typedef {xyz.swapee.wc.ExchangeIntentElementPort} xyz.swapee.wc.BoundExchangeIntentElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder)} xyz.swapee.wc.IExchangeIntentElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder} xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _IExchangeIntentElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeIntentElementPort.Inputs
 */
xyz.swapee.wc.IExchangeIntentElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeIntentElementPort.Inputs.constructor&xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIntentElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeIntentElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _IExchangeIntentElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs
 */
xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.constructor&xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs

/**
 * Contains getters to cast the _IExchangeIntentElementPort_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentElementPortCaster
 */
xyz.swapee.wc.IExchangeIntentElementPortCaster = class { }
/**
 * Cast the _IExchangeIntentElementPort_ instance into the _BoundIExchangeIntentElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentElementPort}
 */
xyz.swapee.wc.IExchangeIntentElementPortCaster.prototype.asIExchangeIntentElementPort
/**
 * Access the _ExchangeIntentElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentElementPort}
 */
xyz.swapee.wc.IExchangeIntentElementPortCaster.prototype.superExchangeIntentElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/170-IExchangeIntentDesigner.xml}  8fc0ad3dd01a68fa56fe97dca7658f4c */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IExchangeIntentDesigner
 */
xyz.swapee.wc.IExchangeIntentDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.ExchangeIntentClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeIntent />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeIntentClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeIntent />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeIntentClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeIntentDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof IExchangeIntentController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeIntentDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof IExchangeIntentController_
   * - `This` _typeof IExchangeIntentController_
   * @param {!xyz.swapee.wc.IExchangeIntentDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ExchangeIntent` _!ExchangeIntentMemory_
   * - `This` _!ExchangeIntentMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeIntentClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeIntentClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IExchangeIntentDesigner.prototype.constructor = xyz.swapee.wc.IExchangeIntentDesigner

/**
 * A concrete class of _IExchangeIntentDesigner_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentDesigner
 * @implements {xyz.swapee.wc.IExchangeIntentDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.ExchangeIntentDesigner = class extends xyz.swapee.wc.IExchangeIntentDesigner { }
xyz.swapee.wc.ExchangeIntentDesigner.prototype.constructor = xyz.swapee.wc.ExchangeIntentDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIntentDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/40-IExchangeIntentDisplay.xml}  729577d83e0e55e6f8b290fdba9df96a */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IExchangeIntentDisplay.Settings>} xyz.swapee.wc.IExchangeIntentDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentDisplay)} xyz.swapee.wc.AbstractExchangeIntentDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentDisplay} xyz.swapee.wc.ExchangeIntentDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentDisplay
 */
xyz.swapee.wc.AbstractExchangeIntentDisplay = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentDisplay.constructor&xyz.swapee.wc.ExchangeIntentDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentDisplay.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentDisplay|typeof xyz.swapee.wc.ExchangeIntentDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentDisplay}
 */
xyz.swapee.wc.AbstractExchangeIntentDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentDisplay}
 */
xyz.swapee.wc.AbstractExchangeIntentDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentDisplay|typeof xyz.swapee.wc.ExchangeIntentDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentDisplay}
 */
xyz.swapee.wc.AbstractExchangeIntentDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentDisplay|typeof xyz.swapee.wc.ExchangeIntentDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentDisplay}
 */
xyz.swapee.wc.AbstractExchangeIntentDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentDisplay.Initialese[]) => xyz.swapee.wc.IExchangeIntentDisplay} xyz.swapee.wc.ExchangeIntentDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.ExchangeIntentMemory, !HTMLDivElement, !xyz.swapee.wc.IExchangeIntentDisplay.Settings, xyz.swapee.wc.IExchangeIntentDisplay.Queries, null>)} xyz.swapee.wc.IExchangeIntentDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IExchangeIntent_.
 * @interface xyz.swapee.wc.IExchangeIntentDisplay
 */
xyz.swapee.wc.IExchangeIntentDisplay = class extends /** @type {xyz.swapee.wc.IExchangeIntentDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIntentDisplay.paint} */
xyz.swapee.wc.IExchangeIntentDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentDisplay.Initialese>)} xyz.swapee.wc.ExchangeIntentDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentDisplay} xyz.swapee.wc.IExchangeIntentDisplay.typeof */
/**
 * A concrete class of _IExchangeIntentDisplay_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentDisplay
 * @implements {xyz.swapee.wc.IExchangeIntentDisplay} Display for presenting information from the _IExchangeIntent_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentDisplay.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentDisplay = class extends /** @type {xyz.swapee.wc.ExchangeIntentDisplay.constructor&xyz.swapee.wc.IExchangeIntentDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentDisplay}
 */
xyz.swapee.wc.ExchangeIntentDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentDisplay.
 * @interface xyz.swapee.wc.IExchangeIntentDisplayFields
 */
xyz.swapee.wc.IExchangeIntentDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IExchangeIntentDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IExchangeIntentDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IExchangeIntentDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IExchangeIntentDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentDisplay} */
xyz.swapee.wc.RecordIExchangeIntentDisplay

/** @typedef {xyz.swapee.wc.IExchangeIntentDisplay} xyz.swapee.wc.BoundIExchangeIntentDisplay */

/** @typedef {xyz.swapee.wc.ExchangeIntentDisplay} xyz.swapee.wc.BoundExchangeIntentDisplay */

/** @typedef {xyz.swapee.wc.IExchangeIntentDisplay.Queries} xyz.swapee.wc.IExchangeIntentDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeIntentDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IExchangeIntentDisplay_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentDisplayCaster
 */
xyz.swapee.wc.IExchangeIntentDisplayCaster = class { }
/**
 * Cast the _IExchangeIntentDisplay_ instance into the _BoundIExchangeIntentDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentDisplay}
 */
xyz.swapee.wc.IExchangeIntentDisplayCaster.prototype.asIExchangeIntentDisplay
/**
 * Cast the _IExchangeIntentDisplay_ instance into the _BoundIExchangeIntentScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentScreen}
 */
xyz.swapee.wc.IExchangeIntentDisplayCaster.prototype.asIExchangeIntentScreen
/**
 * Access the _ExchangeIntentDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentDisplay}
 */
xyz.swapee.wc.IExchangeIntentDisplayCaster.prototype.superExchangeIntentDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeIntentMemory, land: null) => void} xyz.swapee.wc.IExchangeIntentDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentDisplay.__paint<!xyz.swapee.wc.IExchangeIntentDisplay>} xyz.swapee.wc.IExchangeIntentDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeIntentMemory} memory The display data.
 * - `currencyFrom` _string_ Ticker of the payin currency. Default empty string.
 * - `amountFrom` _string_ Amount of currency the user is going to send, used to create a transaction. Default empty string.
 * - `currencyTo` _string_ Ticker of the payout currency. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIntentDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/40-IExchangeIntentDisplayBack.xml}  90d76bc38e02a96055b44b2839e3f51b */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ExchangeIntentClasses>} xyz.swapee.wc.back.IExchangeIntentDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIntentDisplay)} xyz.swapee.wc.back.AbstractExchangeIntentDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIntentDisplay} xyz.swapee.wc.back.ExchangeIntentDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIntentDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIntentDisplay
 */
xyz.swapee.wc.back.AbstractExchangeIntentDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIntentDisplay.constructor&xyz.swapee.wc.back.ExchangeIntentDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIntentDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIntentDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIntentDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentDisplay|typeof xyz.swapee.wc.back.ExchangeIntentDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIntentDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIntentDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIntentDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIntentDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentDisplay|typeof xyz.swapee.wc.back.ExchangeIntentDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIntentDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentDisplay|typeof xyz.swapee.wc.back.ExchangeIntentDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIntentDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIntentDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.ExchangeIntentClasses, null>)} xyz.swapee.wc.back.IExchangeIntentDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IExchangeIntentDisplay
 */
xyz.swapee.wc.back.IExchangeIntentDisplay = class extends /** @type {xyz.swapee.wc.back.IExchangeIntentDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IExchangeIntentDisplay.paint} */
xyz.swapee.wc.back.IExchangeIntentDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIntentDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentDisplay.Initialese>)} xyz.swapee.wc.back.ExchangeIntentDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIntentDisplay} xyz.swapee.wc.back.IExchangeIntentDisplay.typeof */
/**
 * A concrete class of _IExchangeIntentDisplay_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIntentDisplay
 * @implements {xyz.swapee.wc.back.IExchangeIntentDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIntentDisplay = class extends /** @type {xyz.swapee.wc.back.ExchangeIntentDisplay.constructor&xyz.swapee.wc.back.IExchangeIntentDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.ExchangeIntentDisplay.prototype.constructor = xyz.swapee.wc.back.ExchangeIntentDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentDisplay}
 */
xyz.swapee.wc.back.ExchangeIntentDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIntentDisplay} */
xyz.swapee.wc.back.RecordIExchangeIntentDisplay

/** @typedef {xyz.swapee.wc.back.IExchangeIntentDisplay} xyz.swapee.wc.back.BoundIExchangeIntentDisplay */

/** @typedef {xyz.swapee.wc.back.ExchangeIntentDisplay} xyz.swapee.wc.back.BoundExchangeIntentDisplay */

/**
 * Contains getters to cast the _IExchangeIntentDisplay_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIntentDisplayCaster
 */
xyz.swapee.wc.back.IExchangeIntentDisplayCaster = class { }
/**
 * Cast the _IExchangeIntentDisplay_ instance into the _BoundIExchangeIntentDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIntentDisplay}
 */
xyz.swapee.wc.back.IExchangeIntentDisplayCaster.prototype.asIExchangeIntentDisplay
/**
 * Access the _ExchangeIntentDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIntentDisplay}
 */
xyz.swapee.wc.back.IExchangeIntentDisplayCaster.prototype.superExchangeIntentDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.ExchangeIntentMemory, land?: null) => void} xyz.swapee.wc.back.IExchangeIntentDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IExchangeIntentDisplay.__paint<!xyz.swapee.wc.back.IExchangeIntentDisplay>} xyz.swapee.wc.back.IExchangeIntentDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIntentDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeIntentMemory} [memory] The display data.
 * - `currencyFrom` _string_ Ticker of the payin currency. Default empty string.
 * - `amountFrom` _string_ Amount of currency the user is going to send, used to create a transaction. Default empty string.
 * - `currencyTo` _string_ Ticker of the payout currency. Default empty string.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IExchangeIntentDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IExchangeIntentDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/41-ExchangeIntentClasses.xml}  21b0b5e8b6195d9fd8f17f6e2b5860e0 */
/**
 * The classes of the _IExchangeIntentDisplay_.
 * @record xyz.swapee.wc.ExchangeIntentClasses
 */
xyz.swapee.wc.ExchangeIntentClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ExchangeIntentClasses.prototype.props = /** @type {xyz.swapee.wc.ExchangeIntentClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/50-IExchangeIntentController.xml}  2dc808311f1d0827824205be6741240f */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IExchangeIntentController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IExchangeIntentController.Inputs, !xyz.swapee.wc.IExchangeIntentOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel>} xyz.swapee.wc.IExchangeIntentController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentController)} xyz.swapee.wc.AbstractExchangeIntentController.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentController} xyz.swapee.wc.ExchangeIntentController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentController` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentController
 */
xyz.swapee.wc.AbstractExchangeIntentController = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentController.constructor&xyz.swapee.wc.ExchangeIntentController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentController.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentController.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentController}
 */
xyz.swapee.wc.AbstractExchangeIntentController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentController}
 */
xyz.swapee.wc.AbstractExchangeIntentController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentController}
 */
xyz.swapee.wc.AbstractExchangeIntentController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentController}
 */
xyz.swapee.wc.AbstractExchangeIntentController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentController.Initialese[]) => xyz.swapee.wc.IExchangeIntentController} xyz.swapee.wc.ExchangeIntentControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IExchangeIntentController.Inputs, !xyz.swapee.wc.IExchangeIntentOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IExchangeIntentOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IExchangeIntentController.Inputs, !xyz.swapee.wc.IExchangeIntentController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IExchangeIntentController.Inputs, !xyz.swapee.wc.ExchangeIntentMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeIntentController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IExchangeIntentController.Inputs>)} xyz.swapee.wc.IExchangeIntentController.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IExchangeIntentController
 */
xyz.swapee.wc.IExchangeIntentController = class extends /** @type {xyz.swapee.wc.IExchangeIntentController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIntentController.resetPort} */
xyz.swapee.wc.IExchangeIntentController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.change} */
xyz.swapee.wc.IExchangeIntentController.prototype.change = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.intentChange} */
xyz.swapee.wc.IExchangeIntentController.prototype.intentChange = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.onChange} */
xyz.swapee.wc.IExchangeIntentController.prototype.onChange = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.onIntentChange} */
xyz.swapee.wc.IExchangeIntentController.prototype.onIntentChange = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.setCurrencyFrom} */
xyz.swapee.wc.IExchangeIntentController.prototype.setCurrencyFrom = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.unsetCurrencyFrom} */
xyz.swapee.wc.IExchangeIntentController.prototype.unsetCurrencyFrom = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.setAmountFrom} */
xyz.swapee.wc.IExchangeIntentController.prototype.setAmountFrom = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.unsetAmountFrom} */
xyz.swapee.wc.IExchangeIntentController.prototype.unsetAmountFrom = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.setCurrencyTo} */
xyz.swapee.wc.IExchangeIntentController.prototype.setCurrencyTo = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.unsetCurrencyTo} */
xyz.swapee.wc.IExchangeIntentController.prototype.unsetCurrencyTo = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.flipFixed} */
xyz.swapee.wc.IExchangeIntentController.prototype.flipFixed = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.setFixed} */
xyz.swapee.wc.IExchangeIntentController.prototype.setFixed = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.unsetFixed} */
xyz.swapee.wc.IExchangeIntentController.prototype.unsetFixed = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.flipFloat} */
xyz.swapee.wc.IExchangeIntentController.prototype.flipFloat = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.setFloat} */
xyz.swapee.wc.IExchangeIntentController.prototype.setFloat = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.unsetFloat} */
xyz.swapee.wc.IExchangeIntentController.prototype.unsetFloat = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.flipAny} */
xyz.swapee.wc.IExchangeIntentController.prototype.flipAny = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.setAny} */
xyz.swapee.wc.IExchangeIntentController.prototype.setAny = function() {}
/** @type {xyz.swapee.wc.IExchangeIntentController.unsetAny} */
xyz.swapee.wc.IExchangeIntentController.prototype.unsetAny = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentController&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentController.Initialese>)} xyz.swapee.wc.ExchangeIntentController.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController} xyz.swapee.wc.IExchangeIntentController.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeIntentController_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentController
 * @implements {xyz.swapee.wc.IExchangeIntentController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentController.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentController = class extends /** @type {xyz.swapee.wc.ExchangeIntentController.constructor&xyz.swapee.wc.IExchangeIntentController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentController}
 */
xyz.swapee.wc.ExchangeIntentController.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentController.
 * @interface xyz.swapee.wc.IExchangeIntentControllerFields
 */
xyz.swapee.wc.IExchangeIntentControllerFields = class { }
/**
 * The inputs to the _IExchangeIntent_'s controller.
 */
xyz.swapee.wc.IExchangeIntentControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIntentController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IExchangeIntentControllerFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeIntentController} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentController} */
xyz.swapee.wc.RecordIExchangeIntentController

/** @typedef {xyz.swapee.wc.IExchangeIntentController} xyz.swapee.wc.BoundIExchangeIntentController */

/** @typedef {xyz.swapee.wc.ExchangeIntentController} xyz.swapee.wc.BoundExchangeIntentController */

/** @typedef {xyz.swapee.wc.IExchangeIntentPort.Inputs} xyz.swapee.wc.IExchangeIntentController.Inputs The inputs to the _IExchangeIntent_'s controller. */

/** @typedef {xyz.swapee.wc.IExchangeIntentPort.WeakInputs} xyz.swapee.wc.IExchangeIntentController.WeakInputs The inputs to the _IExchangeIntent_'s controller. */

/**
 * Contains getters to cast the _IExchangeIntentController_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentControllerCaster
 */
xyz.swapee.wc.IExchangeIntentControllerCaster = class { }
/**
 * Cast the _IExchangeIntentController_ instance into the _BoundIExchangeIntentController_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentController}
 */
xyz.swapee.wc.IExchangeIntentControllerCaster.prototype.asIExchangeIntentController
/**
 * Cast the _IExchangeIntentController_ instance into the _BoundIExchangeIntentProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentProcessor}
 */
xyz.swapee.wc.IExchangeIntentControllerCaster.prototype.asIExchangeIntentProcessor
/**
 * Access the _ExchangeIntentController_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentController}
 */
xyz.swapee.wc.IExchangeIntentControllerCaster.prototype.superExchangeIntentController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__resetPort<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.IExchangeIntentController.__change
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__change<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._change */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.change} */
/**
 * Fired when either of the inputs changed.
 * @return {?}
 */
xyz.swapee.wc.IExchangeIntentController.change = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.IExchangeIntentController.__intentChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__intentChange<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._intentChange */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.intentChange} */
/**
 * Fired when currency from or to, or amount changes (but not fixed/float/any
 * type).
 * @return {?}
 */
xyz.swapee.wc.IExchangeIntentController.intentChange = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.IExchangeIntentController.__onChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__onChange<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._onChange */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.onChange} */
/**
 * Fired when either of the inputs changed.
 *
 * Executed when `change` is called. Can be used in relays.
 * @return {?}
 */
xyz.swapee.wc.IExchangeIntentController.onChange = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.IExchangeIntentController.__onIntentChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__onIntentChange<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._onIntentChange */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.onIntentChange} */
/**
 * Fired when currency from or to, or amount changes (but not fixed/float/any
 * type).
 *
 * Executed when `intentChange` is called. Can be used in relays.
 * @return {?}
 */
xyz.swapee.wc.IExchangeIntentController.onIntentChange = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IExchangeIntentController.__setCurrencyFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__setCurrencyFrom<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._setCurrencyFrom */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.setCurrencyFrom} */
/**
 * Sets the `currencyFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.setCurrencyFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__unsetCurrencyFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__unsetCurrencyFrom<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._unsetCurrencyFrom */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.unsetCurrencyFrom} */
/**
 * Clears the `currencyFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.unsetCurrencyFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IExchangeIntentController.__setAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__setAmountFrom<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._setAmountFrom */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.setAmountFrom} */
/**
 * Sets the `amountFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.setAmountFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__unsetAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__unsetAmountFrom<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._unsetAmountFrom */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.unsetAmountFrom} */
/**
 * Clears the `amountFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.unsetAmountFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.IExchangeIntentController.__setCurrencyTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__setCurrencyTo<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._setCurrencyTo */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.setCurrencyTo} */
/**
 * Sets the `currencyTo` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.setCurrencyTo = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__unsetCurrencyTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__unsetCurrencyTo<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._unsetCurrencyTo */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.unsetCurrencyTo} */
/**
 * Clears the `currencyTo` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.unsetCurrencyTo = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__flipFixed
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__flipFixed<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._flipFixed */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.flipFixed} */
/**
 * Flips between the positive and negative values of the `fixed`.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.flipFixed = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__setFixed
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__setFixed<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._setFixed */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.setFixed} */
/**
 * Sets the `fixed` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.setFixed = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__unsetFixed
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__unsetFixed<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._unsetFixed */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.unsetFixed} */
/**
 * Clears the `fixed` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.unsetFixed = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__flipFloat
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__flipFloat<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._flipFloat */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.flipFloat} */
/**
 * Flips between the positive and negative values of the `float`.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.flipFloat = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__setFloat
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__setFloat<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._setFloat */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.setFloat} */
/**
 * Sets the `float` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.setFloat = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__unsetFloat
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__unsetFloat<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._unsetFloat */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.unsetFloat} */
/**
 * Clears the `float` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.unsetFloat = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__flipAny
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__flipAny<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._flipAny */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.flipAny} */
/**
 * Flips between the positive and negative values of the `any`.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.flipAny = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__setAny
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__setAny<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._setAny */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.setAny} */
/**
 * Sets the `any` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.setAny = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIntentController.__unsetAny
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIntentController.__unsetAny<!xyz.swapee.wc.IExchangeIntentController>} xyz.swapee.wc.IExchangeIntentController._unsetAny */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentController.unsetAny} */
/**
 * Clears the `any` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIntentController.unsetAny = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIntentController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/51-IExchangeIntentControllerFront.xml}  04ac9066b44ac2f30063acbae111a881 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IExchangeIntentController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeIntentController)} xyz.swapee.wc.front.AbstractExchangeIntentController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeIntentController} xyz.swapee.wc.front.ExchangeIntentController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeIntentController` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeIntentController
 */
xyz.swapee.wc.front.AbstractExchangeIntentController = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeIntentController.constructor&xyz.swapee.wc.front.ExchangeIntentController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeIntentController.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeIntentController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeIntentController.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeIntentController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentController|typeof xyz.swapee.wc.front.ExchangeIntentController)|(!xyz.swapee.wc.front.IExchangeIntentControllerAT|typeof xyz.swapee.wc.front.ExchangeIntentControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeIntentController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeIntentController}
 */
xyz.swapee.wc.front.AbstractExchangeIntentController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentController}
 */
xyz.swapee.wc.front.AbstractExchangeIntentController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentController|typeof xyz.swapee.wc.front.ExchangeIntentController)|(!xyz.swapee.wc.front.IExchangeIntentControllerAT|typeof xyz.swapee.wc.front.ExchangeIntentControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentController}
 */
xyz.swapee.wc.front.AbstractExchangeIntentController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentController|typeof xyz.swapee.wc.front.ExchangeIntentController)|(!xyz.swapee.wc.front.IExchangeIntentControllerAT|typeof xyz.swapee.wc.front.ExchangeIntentControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentController}
 */
xyz.swapee.wc.front.AbstractExchangeIntentController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeIntentController.Initialese[]) => xyz.swapee.wc.front.IExchangeIntentController} xyz.swapee.wc.front.ExchangeIntentControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeIntentControllerCaster&xyz.swapee.wc.front.IExchangeIntentControllerAT)} xyz.swapee.wc.front.IExchangeIntentController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentControllerAT} xyz.swapee.wc.front.IExchangeIntentControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IExchangeIntentController
 */
xyz.swapee.wc.front.IExchangeIntentController = class extends /** @type {xyz.swapee.wc.front.IExchangeIntentController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IExchangeIntentControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIntentController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeIntentController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.change} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.change = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.intentChange} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.intentChange = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.onChange} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.onChange = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.onIntentChange} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.onIntentChange = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.setCurrencyFrom} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.setCurrencyFrom = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.unsetCurrencyFrom} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.unsetCurrencyFrom = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.setAmountFrom} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.setAmountFrom = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.unsetAmountFrom} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.unsetAmountFrom = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.setCurrencyTo} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.setCurrencyTo = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.unsetCurrencyTo} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.unsetCurrencyTo = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.flipFixed} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.flipFixed = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.setFixed} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.setFixed = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.unsetFixed} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.unsetFixed = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.flipFloat} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.flipFloat = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.setFloat} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.setFloat = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.unsetFloat} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.unsetFloat = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.flipAny} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.flipAny = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.setAny} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.setAny = function() {}
/** @type {xyz.swapee.wc.front.IExchangeIntentController.unsetAny} */
xyz.swapee.wc.front.IExchangeIntentController.prototype.unsetAny = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeIntentController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIntentController.Initialese>)} xyz.swapee.wc.front.ExchangeIntentController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController} xyz.swapee.wc.front.IExchangeIntentController.typeof */
/**
 * A concrete class of _IExchangeIntentController_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeIntentController
 * @implements {xyz.swapee.wc.front.IExchangeIntentController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIntentController.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeIntentController = class extends /** @type {xyz.swapee.wc.front.ExchangeIntentController.constructor&xyz.swapee.wc.front.IExchangeIntentController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeIntentController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIntentController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeIntentController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentController}
 */
xyz.swapee.wc.front.ExchangeIntentController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeIntentController} */
xyz.swapee.wc.front.RecordIExchangeIntentController

/** @typedef {xyz.swapee.wc.front.IExchangeIntentController} xyz.swapee.wc.front.BoundIExchangeIntentController */

/** @typedef {xyz.swapee.wc.front.ExchangeIntentController} xyz.swapee.wc.front.BoundExchangeIntentController */

/**
 * Contains getters to cast the _IExchangeIntentController_ interface.
 * @interface xyz.swapee.wc.front.IExchangeIntentControllerCaster
 */
xyz.swapee.wc.front.IExchangeIntentControllerCaster = class { }
/**
 * Cast the _IExchangeIntentController_ instance into the _BoundIExchangeIntentController_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeIntentController}
 */
xyz.swapee.wc.front.IExchangeIntentControllerCaster.prototype.asIExchangeIntentController
/**
 * Access the _ExchangeIntentController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeIntentController}
 */
xyz.swapee.wc.front.IExchangeIntentControllerCaster.prototype.superExchangeIntentController

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.IExchangeIntentController.__change
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__change<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._change */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.change} */
/**
 * Fired when either of the inputs changed.
 * @return {?}
 */
xyz.swapee.wc.front.IExchangeIntentController.change = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.IExchangeIntentController.__intentChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__intentChange<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._intentChange */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.intentChange} */
/**
 * Fired when currency from or to, or amount changes (but not fixed/float/any
 * type).
 * @return {?}
 */
xyz.swapee.wc.front.IExchangeIntentController.intentChange = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.IExchangeIntentController.__onChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__onChange<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._onChange */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.onChange} */
/**
 * Fired when either of the inputs changed.
 *
 * Executed when `change` is called. Can be used in relays.
 * @return {?}
 */
xyz.swapee.wc.front.IExchangeIntentController.onChange = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.IExchangeIntentController.__onIntentChange
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__onIntentChange<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._onIntentChange */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.onIntentChange} */
/**
 * Fired when currency from or to, or amount changes (but not fixed/float/any
 * type).
 *
 * Executed when `intentChange` is called. Can be used in relays.
 * @return {?}
 */
xyz.swapee.wc.front.IExchangeIntentController.onIntentChange = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IExchangeIntentController.__setCurrencyFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__setCurrencyFrom<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._setCurrencyFrom */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.setCurrencyFrom} */
/**
 * Sets the `currencyFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.setCurrencyFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__unsetCurrencyFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__unsetCurrencyFrom<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._unsetCurrencyFrom */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.unsetCurrencyFrom} */
/**
 * Clears the `currencyFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.unsetCurrencyFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IExchangeIntentController.__setAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__setAmountFrom<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._setAmountFrom */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.setAmountFrom} */
/**
 * Sets the `amountFrom` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.setAmountFrom = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__unsetAmountFrom
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__unsetAmountFrom<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._unsetAmountFrom */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.unsetAmountFrom} */
/**
 * Clears the `amountFrom` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.unsetAmountFrom = function() {}

/**
 * @typedef {(this: THIS, val: string) => void} xyz.swapee.wc.front.IExchangeIntentController.__setCurrencyTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__setCurrencyTo<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._setCurrencyTo */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.setCurrencyTo} */
/**
 * Sets the `currencyTo` in the core via port.
 * @param {string} val The value to set.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.setCurrencyTo = function(val) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__unsetCurrencyTo
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__unsetCurrencyTo<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._unsetCurrencyTo */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.unsetCurrencyTo} */
/**
 * Clears the `currencyTo` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.unsetCurrencyTo = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__flipFixed
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__flipFixed<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._flipFixed */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.flipFixed} */
/**
 * Flips between the positive and negative values of the `fixed`.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.flipFixed = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__setFixed
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__setFixed<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._setFixed */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.setFixed} */
/**
 * Sets the `fixed` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.setFixed = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__unsetFixed
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__unsetFixed<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._unsetFixed */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.unsetFixed} */
/**
 * Clears the `fixed` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.unsetFixed = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__flipFloat
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__flipFloat<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._flipFloat */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.flipFloat} */
/**
 * Flips between the positive and negative values of the `float`.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.flipFloat = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__setFloat
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__setFloat<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._setFloat */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.setFloat} */
/**
 * Sets the `float` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.setFloat = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__unsetFloat
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__unsetFloat<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._unsetFloat */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.unsetFloat} */
/**
 * Clears the `float` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.unsetFloat = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__flipAny
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__flipAny<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._flipAny */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.flipAny} */
/**
 * Flips between the positive and negative values of the `any`.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.flipAny = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__setAny
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__setAny<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._setAny */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.setAny} */
/**
 * Sets the `any` in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.setAny = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IExchangeIntentController.__unsetAny
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IExchangeIntentController.__unsetAny<!xyz.swapee.wc.front.IExchangeIntentController>} xyz.swapee.wc.front.IExchangeIntentController._unsetAny */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentController.unsetAny} */
/**
 * Clears the `any` value in the core via port.
 * @return {void}
 */
xyz.swapee.wc.front.IExchangeIntentController.unsetAny = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IExchangeIntentController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/52-IExchangeIntentControllerBack.xml}  8804ddea1c9ffe4894c23cfefcd27dbb */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IExchangeIntentController.Inputs>&xyz.swapee.wc.IExchangeIntentController.Initialese} xyz.swapee.wc.back.IExchangeIntentController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIntentController)} xyz.swapee.wc.back.AbstractExchangeIntentController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIntentController} xyz.swapee.wc.back.ExchangeIntentController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIntentController` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIntentController
 */
xyz.swapee.wc.back.AbstractExchangeIntentController = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIntentController.constructor&xyz.swapee.wc.back.ExchangeIntentController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIntentController.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIntentController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIntentController.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentController|typeof xyz.swapee.wc.back.ExchangeIntentController)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIntentController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIntentController}
 */
xyz.swapee.wc.back.AbstractExchangeIntentController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentController}
 */
xyz.swapee.wc.back.AbstractExchangeIntentController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentController|typeof xyz.swapee.wc.back.ExchangeIntentController)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentController}
 */
xyz.swapee.wc.back.AbstractExchangeIntentController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentController|typeof xyz.swapee.wc.back.ExchangeIntentController)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentController}
 */
xyz.swapee.wc.back.AbstractExchangeIntentController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIntentController.Initialese[]) => xyz.swapee.wc.back.IExchangeIntentController} xyz.swapee.wc.back.ExchangeIntentControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIntentControllerCaster&xyz.swapee.wc.IExchangeIntentController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IExchangeIntentController.Inputs>)} xyz.swapee.wc.back.IExchangeIntentController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IExchangeIntentController
 */
xyz.swapee.wc.back.IExchangeIntentController = class extends /** @type {xyz.swapee.wc.back.IExchangeIntentController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIntentController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIntentController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIntentController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentController.Initialese>)} xyz.swapee.wc.back.ExchangeIntentController.constructor */
/**
 * A concrete class of _IExchangeIntentController_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIntentController
 * @implements {xyz.swapee.wc.back.IExchangeIntentController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentController.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIntentController = class extends /** @type {xyz.swapee.wc.back.ExchangeIntentController.constructor&xyz.swapee.wc.back.IExchangeIntentController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIntentController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIntentController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentController}
 */
xyz.swapee.wc.back.ExchangeIntentController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIntentController} */
xyz.swapee.wc.back.RecordIExchangeIntentController

/** @typedef {xyz.swapee.wc.back.IExchangeIntentController} xyz.swapee.wc.back.BoundIExchangeIntentController */

/** @typedef {xyz.swapee.wc.back.ExchangeIntentController} xyz.swapee.wc.back.BoundExchangeIntentController */

/**
 * Contains getters to cast the _IExchangeIntentController_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIntentControllerCaster
 */
xyz.swapee.wc.back.IExchangeIntentControllerCaster = class { }
/**
 * Cast the _IExchangeIntentController_ instance into the _BoundIExchangeIntentController_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIntentController}
 */
xyz.swapee.wc.back.IExchangeIntentControllerCaster.prototype.asIExchangeIntentController
/**
 * Access the _ExchangeIntentController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIntentController}
 */
xyz.swapee.wc.back.IExchangeIntentControllerCaster.prototype.superExchangeIntentController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/53-IExchangeIntentControllerAR.xml}  e1b3d88e8c419377cc76a21af6990606 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeIntentController.Initialese} xyz.swapee.wc.back.IExchangeIntentControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIntentControllerAR)} xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIntentControllerAR} xyz.swapee.wc.back.ExchangeIntentControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIntentControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIntentControllerAR
 */
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.constructor&xyz.swapee.wc.back.ExchangeIntentControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIntentControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentControllerAR|typeof xyz.swapee.wc.back.ExchangeIntentControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIntentControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentControllerAR|typeof xyz.swapee.wc.back.ExchangeIntentControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentControllerAR|typeof xyz.swapee.wc.back.ExchangeIntentControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIntentController|typeof xyz.swapee.wc.ExchangeIntentController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIntentControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIntentControllerAR.Initialese[]) => xyz.swapee.wc.back.IExchangeIntentControllerAR} xyz.swapee.wc.back.ExchangeIntentControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIntentControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeIntentController)} xyz.swapee.wc.back.IExchangeIntentControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeIntentControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IExchangeIntentControllerAR
 */
xyz.swapee.wc.back.IExchangeIntentControllerAR = class extends /** @type {xyz.swapee.wc.back.IExchangeIntentControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeIntentController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIntentControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIntentControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentControllerAR.Initialese>)} xyz.swapee.wc.back.ExchangeIntentControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIntentControllerAR} xyz.swapee.wc.back.IExchangeIntentControllerAR.typeof */
/**
 * A concrete class of _IExchangeIntentControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIntentControllerAR
 * @implements {xyz.swapee.wc.back.IExchangeIntentControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeIntentControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIntentControllerAR = class extends /** @type {xyz.swapee.wc.back.ExchangeIntentControllerAR.constructor&xyz.swapee.wc.back.IExchangeIntentControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIntentControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIntentControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentControllerAR}
 */
xyz.swapee.wc.back.ExchangeIntentControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIntentControllerAR} */
xyz.swapee.wc.back.RecordIExchangeIntentControllerAR

/** @typedef {xyz.swapee.wc.back.IExchangeIntentControllerAR} xyz.swapee.wc.back.BoundIExchangeIntentControllerAR */

/** @typedef {xyz.swapee.wc.back.ExchangeIntentControllerAR} xyz.swapee.wc.back.BoundExchangeIntentControllerAR */

/**
 * Contains getters to cast the _IExchangeIntentControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIntentControllerARCaster
 */
xyz.swapee.wc.back.IExchangeIntentControllerARCaster = class { }
/**
 * Cast the _IExchangeIntentControllerAR_ instance into the _BoundIExchangeIntentControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIntentControllerAR}
 */
xyz.swapee.wc.back.IExchangeIntentControllerARCaster.prototype.asIExchangeIntentControllerAR
/**
 * Access the _ExchangeIntentControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIntentControllerAR}
 */
xyz.swapee.wc.back.IExchangeIntentControllerARCaster.prototype.superExchangeIntentControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/54-IExchangeIntentControllerAT.xml}  af49e404c287362559ab35deff0e5ba0 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IExchangeIntentControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeIntentControllerAT)} xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeIntentControllerAT} xyz.swapee.wc.front.ExchangeIntentControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeIntentControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeIntentControllerAT
 */
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.constructor&xyz.swapee.wc.front.ExchangeIntentControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeIntentControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeIntentControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentControllerAT|typeof xyz.swapee.wc.front.ExchangeIntentControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeIntentControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentControllerAT|typeof xyz.swapee.wc.front.ExchangeIntentControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentControllerAT|typeof xyz.swapee.wc.front.ExchangeIntentControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIntentControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeIntentControllerAT.Initialese[]) => xyz.swapee.wc.front.IExchangeIntentControllerAT} xyz.swapee.wc.front.ExchangeIntentControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeIntentControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IExchangeIntentControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeIntentControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IExchangeIntentControllerAT
 */
xyz.swapee.wc.front.IExchangeIntentControllerAT = class extends /** @type {xyz.swapee.wc.front.IExchangeIntentControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIntentControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeIntentControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeIntentControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIntentControllerAT.Initialese>)} xyz.swapee.wc.front.ExchangeIntentControllerAT.constructor */
/**
 * A concrete class of _IExchangeIntentControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeIntentControllerAT
 * @implements {xyz.swapee.wc.front.IExchangeIntentControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeIntentControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIntentControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeIntentControllerAT = class extends /** @type {xyz.swapee.wc.front.ExchangeIntentControllerAT.constructor&xyz.swapee.wc.front.IExchangeIntentControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeIntentControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIntentControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeIntentControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentControllerAT}
 */
xyz.swapee.wc.front.ExchangeIntentControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeIntentControllerAT} */
xyz.swapee.wc.front.RecordIExchangeIntentControllerAT

/** @typedef {xyz.swapee.wc.front.IExchangeIntentControllerAT} xyz.swapee.wc.front.BoundIExchangeIntentControllerAT */

/** @typedef {xyz.swapee.wc.front.ExchangeIntentControllerAT} xyz.swapee.wc.front.BoundExchangeIntentControllerAT */

/**
 * Contains getters to cast the _IExchangeIntentControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IExchangeIntentControllerATCaster
 */
xyz.swapee.wc.front.IExchangeIntentControllerATCaster = class { }
/**
 * Cast the _IExchangeIntentControllerAT_ instance into the _BoundIExchangeIntentControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeIntentControllerAT}
 */
xyz.swapee.wc.front.IExchangeIntentControllerATCaster.prototype.asIExchangeIntentControllerAT
/**
 * Access the _ExchangeIntentControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeIntentControllerAT}
 */
xyz.swapee.wc.front.IExchangeIntentControllerATCaster.prototype.superExchangeIntentControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/70-IExchangeIntentScreen.xml}  03a7f5299222c75716a96c6a26cb6599 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.front.ExchangeIntentInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeIntentDisplay.Settings, !xyz.swapee.wc.IExchangeIntentDisplay.Queries, null>&xyz.swapee.wc.IExchangeIntentDisplay.Initialese} xyz.swapee.wc.IExchangeIntentScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentScreen)} xyz.swapee.wc.AbstractExchangeIntentScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentScreen} xyz.swapee.wc.ExchangeIntentScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentScreen` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentScreen
 */
xyz.swapee.wc.AbstractExchangeIntentScreen = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentScreen.constructor&xyz.swapee.wc.ExchangeIntentScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentScreen.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentScreen.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentScreen|typeof xyz.swapee.wc.ExchangeIntentScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeIntentController|typeof xyz.swapee.wc.front.ExchangeIntentController)|(!xyz.swapee.wc.IExchangeIntentDisplay|typeof xyz.swapee.wc.ExchangeIntentDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentScreen}
 */
xyz.swapee.wc.AbstractExchangeIntentScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentScreen}
 */
xyz.swapee.wc.AbstractExchangeIntentScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentScreen|typeof xyz.swapee.wc.ExchangeIntentScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeIntentController|typeof xyz.swapee.wc.front.ExchangeIntentController)|(!xyz.swapee.wc.IExchangeIntentDisplay|typeof xyz.swapee.wc.ExchangeIntentDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentScreen}
 */
xyz.swapee.wc.AbstractExchangeIntentScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentScreen|typeof xyz.swapee.wc.ExchangeIntentScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeIntentController|typeof xyz.swapee.wc.front.ExchangeIntentController)|(!xyz.swapee.wc.IExchangeIntentDisplay|typeof xyz.swapee.wc.ExchangeIntentDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentScreen}
 */
xyz.swapee.wc.AbstractExchangeIntentScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentScreen.Initialese[]) => xyz.swapee.wc.IExchangeIntentScreen} xyz.swapee.wc.ExchangeIntentScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.ExchangeIntentMemory, !xyz.swapee.wc.front.ExchangeIntentInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeIntentDisplay.Settings, !xyz.swapee.wc.IExchangeIntentDisplay.Queries, null, null>&xyz.swapee.wc.front.IExchangeIntentController&xyz.swapee.wc.IExchangeIntentDisplay)} xyz.swapee.wc.IExchangeIntentScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IExchangeIntentScreen
 */
xyz.swapee.wc.IExchangeIntentScreen = class extends /** @type {xyz.swapee.wc.IExchangeIntentScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IExchangeIntentController.typeof&xyz.swapee.wc.IExchangeIntentDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentScreen.Initialese>)} xyz.swapee.wc.ExchangeIntentScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIntentScreen} xyz.swapee.wc.IExchangeIntentScreen.typeof */
/**
 * A concrete class of _IExchangeIntentScreen_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentScreen
 * @implements {xyz.swapee.wc.IExchangeIntentScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentScreen.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentScreen = class extends /** @type {xyz.swapee.wc.ExchangeIntentScreen.constructor&xyz.swapee.wc.IExchangeIntentScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentScreen}
 */
xyz.swapee.wc.ExchangeIntentScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIntentScreen} */
xyz.swapee.wc.RecordIExchangeIntentScreen

/** @typedef {xyz.swapee.wc.IExchangeIntentScreen} xyz.swapee.wc.BoundIExchangeIntentScreen */

/** @typedef {xyz.swapee.wc.ExchangeIntentScreen} xyz.swapee.wc.BoundExchangeIntentScreen */

/**
 * Contains getters to cast the _IExchangeIntentScreen_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentScreenCaster
 */
xyz.swapee.wc.IExchangeIntentScreenCaster = class { }
/**
 * Cast the _IExchangeIntentScreen_ instance into the _BoundIExchangeIntentScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentScreen}
 */
xyz.swapee.wc.IExchangeIntentScreenCaster.prototype.asIExchangeIntentScreen
/**
 * Access the _ExchangeIntentScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentScreen}
 */
xyz.swapee.wc.IExchangeIntentScreenCaster.prototype.superExchangeIntentScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/70-IExchangeIntentScreenBack.xml}  e59660ae8b1aa7317094b437c112eb7a */
/** @typedef {xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese} xyz.swapee.wc.back.IExchangeIntentScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIntentScreen)} xyz.swapee.wc.back.AbstractExchangeIntentScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIntentScreen} xyz.swapee.wc.back.ExchangeIntentScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIntentScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIntentScreen
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreen = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIntentScreen.constructor&xyz.swapee.wc.back.ExchangeIntentScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIntentScreen.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIntentScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentScreen|typeof xyz.swapee.wc.back.ExchangeIntentScreen)|(!xyz.swapee.wc.back.IExchangeIntentScreenAT|typeof xyz.swapee.wc.back.ExchangeIntentScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentScreen|typeof xyz.swapee.wc.back.ExchangeIntentScreen)|(!xyz.swapee.wc.back.IExchangeIntentScreenAT|typeof xyz.swapee.wc.back.ExchangeIntentScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentScreen|typeof xyz.swapee.wc.back.ExchangeIntentScreen)|(!xyz.swapee.wc.back.IExchangeIntentScreenAT|typeof xyz.swapee.wc.back.ExchangeIntentScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIntentScreen.Initialese[]) => xyz.swapee.wc.back.IExchangeIntentScreen} xyz.swapee.wc.back.ExchangeIntentScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIntentScreenCaster&xyz.swapee.wc.back.IExchangeIntentScreenAT)} xyz.swapee.wc.back.IExchangeIntentScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIntentScreenAT} xyz.swapee.wc.back.IExchangeIntentScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IExchangeIntentScreen
 */
xyz.swapee.wc.back.IExchangeIntentScreen = class extends /** @type {xyz.swapee.wc.back.IExchangeIntentScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeIntentScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIntentScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIntentScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentScreen.Initialese>)} xyz.swapee.wc.back.ExchangeIntentScreen.constructor */
/**
 * A concrete class of _IExchangeIntentScreen_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIntentScreen
 * @implements {xyz.swapee.wc.back.IExchangeIntentScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIntentScreen = class extends /** @type {xyz.swapee.wc.back.ExchangeIntentScreen.constructor&xyz.swapee.wc.back.IExchangeIntentScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIntentScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIntentScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreen}
 */
xyz.swapee.wc.back.ExchangeIntentScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIntentScreen} */
xyz.swapee.wc.back.RecordIExchangeIntentScreen

/** @typedef {xyz.swapee.wc.back.IExchangeIntentScreen} xyz.swapee.wc.back.BoundIExchangeIntentScreen */

/** @typedef {xyz.swapee.wc.back.ExchangeIntentScreen} xyz.swapee.wc.back.BoundExchangeIntentScreen */

/**
 * Contains getters to cast the _IExchangeIntentScreen_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIntentScreenCaster
 */
xyz.swapee.wc.back.IExchangeIntentScreenCaster = class { }
/**
 * Cast the _IExchangeIntentScreen_ instance into the _BoundIExchangeIntentScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIntentScreen}
 */
xyz.swapee.wc.back.IExchangeIntentScreenCaster.prototype.asIExchangeIntentScreen
/**
 * Access the _ExchangeIntentScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIntentScreen}
 */
xyz.swapee.wc.back.IExchangeIntentScreenCaster.prototype.superExchangeIntentScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/73-IExchangeIntentScreenAR.xml}  4d708a5fc4202e26697b624e526c8025 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeIntentScreen.Initialese} xyz.swapee.wc.front.IExchangeIntentScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeIntentScreenAR)} xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeIntentScreenAR} xyz.swapee.wc.front.ExchangeIntentScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeIntentScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeIntentScreenAR
 */
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.constructor&xyz.swapee.wc.front.ExchangeIntentScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeIntentScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeIntentScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentScreenAR|typeof xyz.swapee.wc.front.ExchangeIntentScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIntentScreen|typeof xyz.swapee.wc.ExchangeIntentScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeIntentScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentScreenAR|typeof xyz.swapee.wc.front.ExchangeIntentScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIntentScreen|typeof xyz.swapee.wc.ExchangeIntentScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeIntentScreenAR|typeof xyz.swapee.wc.front.ExchangeIntentScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIntentScreen|typeof xyz.swapee.wc.ExchangeIntentScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIntentScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeIntentScreenAR.Initialese[]) => xyz.swapee.wc.front.IExchangeIntentScreenAR} xyz.swapee.wc.front.ExchangeIntentScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeIntentScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeIntentScreen)} xyz.swapee.wc.front.IExchangeIntentScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeIntentScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IExchangeIntentScreenAR
 */
xyz.swapee.wc.front.IExchangeIntentScreenAR = class extends /** @type {xyz.swapee.wc.front.IExchangeIntentScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeIntentScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIntentScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeIntentScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeIntentScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIntentScreenAR.Initialese>)} xyz.swapee.wc.front.ExchangeIntentScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIntentScreenAR} xyz.swapee.wc.front.IExchangeIntentScreenAR.typeof */
/**
 * A concrete class of _IExchangeIntentScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeIntentScreenAR
 * @implements {xyz.swapee.wc.front.IExchangeIntentScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeIntentScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIntentScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeIntentScreenAR = class extends /** @type {xyz.swapee.wc.front.ExchangeIntentScreenAR.constructor&xyz.swapee.wc.front.IExchangeIntentScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeIntentScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIntentScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeIntentScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeIntentScreenAR}
 */
xyz.swapee.wc.front.ExchangeIntentScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeIntentScreenAR} */
xyz.swapee.wc.front.RecordIExchangeIntentScreenAR

/** @typedef {xyz.swapee.wc.front.IExchangeIntentScreenAR} xyz.swapee.wc.front.BoundIExchangeIntentScreenAR */

/** @typedef {xyz.swapee.wc.front.ExchangeIntentScreenAR} xyz.swapee.wc.front.BoundExchangeIntentScreenAR */

/**
 * Contains getters to cast the _IExchangeIntentScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IExchangeIntentScreenARCaster
 */
xyz.swapee.wc.front.IExchangeIntentScreenARCaster = class { }
/**
 * Cast the _IExchangeIntentScreenAR_ instance into the _BoundIExchangeIntentScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeIntentScreenAR}
 */
xyz.swapee.wc.front.IExchangeIntentScreenARCaster.prototype.asIExchangeIntentScreenAR
/**
 * Access the _ExchangeIntentScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeIntentScreenAR}
 */
xyz.swapee.wc.front.IExchangeIntentScreenARCaster.prototype.superExchangeIntentScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/74-IExchangeIntentScreenAT.xml}  435ed47aff3c3e647e0db4385d4c4749 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIntentScreenAT)} xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIntentScreenAT} xyz.swapee.wc.back.ExchangeIntentScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIntentScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIntentScreenAT
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.constructor&xyz.swapee.wc.back.ExchangeIntentScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIntentScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentScreenAT|typeof xyz.swapee.wc.back.ExchangeIntentScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentScreenAT|typeof xyz.swapee.wc.back.ExchangeIntentScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIntentScreenAT|typeof xyz.swapee.wc.back.ExchangeIntentScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIntentScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese[]) => xyz.swapee.wc.back.IExchangeIntentScreenAT} xyz.swapee.wc.back.ExchangeIntentScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIntentScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IExchangeIntentScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeIntentScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IExchangeIntentScreenAT
 */
xyz.swapee.wc.back.IExchangeIntentScreenAT = class extends /** @type {xyz.swapee.wc.back.IExchangeIntentScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIntentScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIntentScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese>)} xyz.swapee.wc.back.ExchangeIntentScreenAT.constructor */
/**
 * A concrete class of _IExchangeIntentScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIntentScreenAT
 * @implements {xyz.swapee.wc.back.IExchangeIntentScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeIntentScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIntentScreenAT = class extends /** @type {xyz.swapee.wc.back.ExchangeIntentScreenAT.constructor&xyz.swapee.wc.back.IExchangeIntentScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIntentScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIntentScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIntentScreenAT}
 */
xyz.swapee.wc.back.ExchangeIntentScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIntentScreenAT} */
xyz.swapee.wc.back.RecordIExchangeIntentScreenAT

/** @typedef {xyz.swapee.wc.back.IExchangeIntentScreenAT} xyz.swapee.wc.back.BoundIExchangeIntentScreenAT */

/** @typedef {xyz.swapee.wc.back.ExchangeIntentScreenAT} xyz.swapee.wc.back.BoundExchangeIntentScreenAT */

/**
 * Contains getters to cast the _IExchangeIntentScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIntentScreenATCaster
 */
xyz.swapee.wc.back.IExchangeIntentScreenATCaster = class { }
/**
 * Cast the _IExchangeIntentScreenAT_ instance into the _BoundIExchangeIntentScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIntentScreenAT}
 */
xyz.swapee.wc.back.IExchangeIntentScreenATCaster.prototype.asIExchangeIntentScreenAT
/**
 * Access the _ExchangeIntentScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIntentScreenAT}
 */
xyz.swapee.wc.back.IExchangeIntentScreenATCaster.prototype.superExchangeIntentScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-intent/ExchangeIntent.mvc/design/80-IExchangeIntentGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IExchangeIntentDisplay.Initialese} xyz.swapee.wc.IExchangeIntentGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIntentGPU)} xyz.swapee.wc.AbstractExchangeIntentGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIntentGPU} xyz.swapee.wc.ExchangeIntentGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentGPU` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIntentGPU
 */
xyz.swapee.wc.AbstractExchangeIntentGPU = class extends /** @type {xyz.swapee.wc.AbstractExchangeIntentGPU.constructor&xyz.swapee.wc.ExchangeIntentGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIntentGPU.prototype.constructor = xyz.swapee.wc.AbstractExchangeIntentGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIntentGPU.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIntentGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIntentGPU|typeof xyz.swapee.wc.ExchangeIntentGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeIntentDisplay|typeof xyz.swapee.wc.back.ExchangeIntentDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIntentGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIntentGPU}
 */
xyz.swapee.wc.AbstractExchangeIntentGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentGPU}
 */
xyz.swapee.wc.AbstractExchangeIntentGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIntentGPU|typeof xyz.swapee.wc.ExchangeIntentGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeIntentDisplay|typeof xyz.swapee.wc.back.ExchangeIntentDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentGPU}
 */
xyz.swapee.wc.AbstractExchangeIntentGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIntentGPU|typeof xyz.swapee.wc.ExchangeIntentGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeIntentDisplay|typeof xyz.swapee.wc.back.ExchangeIntentDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIntentGPU}
 */
xyz.swapee.wc.AbstractExchangeIntentGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIntentGPU.Initialese[]) => xyz.swapee.wc.IExchangeIntentGPU} xyz.swapee.wc.ExchangeIntentGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIntentGPUCaster&com.webcircuits.IBrowserView<.!ExchangeIntentMemory,>&xyz.swapee.wc.back.IExchangeIntentDisplay)} xyz.swapee.wc.IExchangeIntentGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!ExchangeIntentMemory,>} com.webcircuits.IBrowserView<.!ExchangeIntentMemory,>.typeof */
/**
 * Handles the periphery of the _IExchangeIntentDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IExchangeIntentGPU
 */
xyz.swapee.wc.IExchangeIntentGPU = class extends /** @type {xyz.swapee.wc.IExchangeIntentGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!ExchangeIntentMemory,>.typeof&xyz.swapee.wc.back.IExchangeIntentDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIntentGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIntentGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIntentGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentGPU.Initialese>)} xyz.swapee.wc.ExchangeIntentGPU.constructor */
/**
 * A concrete class of _IExchangeIntentGPU_ instances.
 * @constructor xyz.swapee.wc.ExchangeIntentGPU
 * @implements {xyz.swapee.wc.IExchangeIntentGPU} Handles the periphery of the _IExchangeIntentDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIntentGPU.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIntentGPU = class extends /** @type {xyz.swapee.wc.ExchangeIntentGPU.constructor&xyz.swapee.wc.IExchangeIntentGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIntentGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIntentGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIntentGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIntentGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIntentGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIntentGPU}
 */
xyz.swapee.wc.ExchangeIntentGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIntentGPU.
 * @interface xyz.swapee.wc.IExchangeIntentGPUFields
 */
xyz.swapee.wc.IExchangeIntentGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IExchangeIntentGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIntentGPU} */
xyz.swapee.wc.RecordIExchangeIntentGPU

/** @typedef {xyz.swapee.wc.IExchangeIntentGPU} xyz.swapee.wc.BoundIExchangeIntentGPU */

/** @typedef {xyz.swapee.wc.ExchangeIntentGPU} xyz.swapee.wc.BoundExchangeIntentGPU */

/**
 * Contains getters to cast the _IExchangeIntentGPU_ interface.
 * @interface xyz.swapee.wc.IExchangeIntentGPUCaster
 */
xyz.swapee.wc.IExchangeIntentGPUCaster = class { }
/**
 * Cast the _IExchangeIntentGPU_ instance into the _BoundIExchangeIntentGPU_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIntentGPU}
 */
xyz.swapee.wc.IExchangeIntentGPUCaster.prototype.asIExchangeIntentGPU
/**
 * Access the _ExchangeIntentGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIntentGPU}
 */
xyz.swapee.wc.IExchangeIntentGPUCaster.prototype.superExchangeIntentGPU

// nss:xyz.swapee.wc
/* @typal-end */