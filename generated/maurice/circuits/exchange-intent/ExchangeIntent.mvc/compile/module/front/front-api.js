import { ExchangeIntentDisplay, ExchangeIntentScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.ExchangeIntentDisplay} */
export { ExchangeIntentDisplay }
/** @lazy @api {xyz.swapee.wc.ExchangeIntentScreen} */
export { ExchangeIntentScreen }