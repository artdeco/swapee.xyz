import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {7833868048} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const e=d["372700389811"];function g(b,f,x){return d["372700389812"](b,f,null,x,!1,void 0)};
const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const k=h["61893096584"],l=h["61893096586"],m=h["618930965811"],n=h["618930965812"];function p(){}p.prototype={};class q{}class r extends g(q,783386804816,{i:1,v:2}){}r[e]=[p,k];var t=class extends r.implements(){};function u(){}u.prototype={};class v{}class w extends g(v,783386804823,{h:1,u:2}){}w[e]=[u,m,{}];function y(){}y.prototype={};class z{}class A extends g(z,783386804826,{l:1,C:2}){}A[e]=[y,n,{allocator(){this.methods={}}}];const B={m:"96c88",g:"748e6",o:"c23cd",fixed:"cec31",ready:"b2fda",any:"100b8",s:"546ad"};const C={...B};const D=Object.keys(B).reduce((b,f)=>{b[B[f]]=f;return b},{});function E(){}E.prototype={};class F{}class G extends g(F,783386804824,{j:1,A:2}){}function H(){}G[e]=[E,H.prototype={inputsPQs:C,memoryQPs:D},l,A,H.prototype={vdusPQs:{}}];var I=class extends G.implements(w,t,{get queries(){return this.settings}},{__$id:7833868048},{}){};module.exports["783386804841"]=t;module.exports["783386804871"]=I;
/*! @embed-object-end {7833868048} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule