/**
 * Display for presenting information from the _IExchangeIntent_.
 * @extends {xyz.swapee.wc.ExchangeIntentDisplay}
 */
class ExchangeIntentDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ExchangeIntentScreen}
 */
class ExchangeIntentScreen extends (class {/* lazy-loaded */}) {}

module.exports.ExchangeIntentDisplay = ExchangeIntentDisplay
module.exports.ExchangeIntentScreen = ExchangeIntentScreen