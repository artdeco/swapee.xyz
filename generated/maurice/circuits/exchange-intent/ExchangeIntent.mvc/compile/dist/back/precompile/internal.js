/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const e=d["372700389811"];function g(a,b,c,f){return d["372700389812"](a,b,c,f,!1,void 0)};function h(){}h.prototype={};class aa{}class k extends g(aa,78338680488,null,{da:1,qa:2}){}k[e]=[h,function(){}.prototype={H(){const {s:{model:{fixed:a},setInfo:b}}=this;b({fixed:!a})},I(){const {s:{model:{h:a},setInfo:b}}=this;b({h:!a})},G(){const {s:{model:{any:a},setInfo:b}}=this;b({any:!a})}}];/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you'll see boundary comments like
@object-code-[start|end] around chunks), proprietary software from the package(s):
*/
/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE @type.engineering/seers (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const l=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];const m={l:"96c88",i:"748e6",m:"c23cd",fixed:"cec31",ready:"b2fda",any:"100b8",h:"546ad"};function n(){}n.prototype={};class ba{}class p extends g(ba,78338680487,null,{Y:1,ja:2}){}function q(){}q.prototype={};function r(){this.model={l:void 0,i:void 0,m:void 0,fixed:void 0,h:void 0,any:void 0,ready:!1}}class ca{}class t extends g(ca,78338680483,r,{ba:1,oa:2}){}t[e]=[q,function(){}.prototype={constructor(){l(this.model,m)}}];p[e]=[function(){}.prototype={},n,t];/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you'll see boundary comments like
@object-code-[start|end] around chunks), proprietary software from the package:
*/
const u=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();/*

@LICENSE @type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
*/
const da=u.IntegratedController,ea=u.Parametric;/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const fa=v["61505580523"],ha=v["615055805212"],ia=v["615055805218"],ja=v["615055805221"],ka=v["615055805223"];/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const la=w.defineEmitters,ma=w.IntegratedComponentInitialiser,na=w.IntegratedComponent;function x(){}x.prototype={};class oa{}class y extends g(oa,78338680481,null,{s:1,ha:2}){}y[e]=[x,ia];const z={regulate:ha({l:String,i:String,m:String,fixed:Boolean,h:Boolean,any:Boolean,ready:Boolean})};const A={...m};function B(){}B.prototype={};function C(){const a={model:null};r.call(a);this.inputs=a.model}class pa{}class D extends g(pa,78338680485,C,{ca:1,pa:2}){}function E(){}D[e]=[E.prototype={resetPort(){C.call(this)}},B,ea,E.prototype={constructor(){l(this.inputs,A)}}];function F(){}F.prototype={};class qa{}class G extends g(qa,783386804819,null,{g:1,R:2}){}function H(){}
G[e]=[H.prototype={resetPort(){this.port.resetPort()}},F,z,da,{get Port(){return D}},H.prototype={constructor(){la(this,{D:!0,u:!0})}},H.prototype={L(a){const {g:{setInputs:b}}=this;b({l:a})},J(a){const {g:{setInputs:b}}=this;b({i:a})},M(a){const {g:{setInputs:b}}=this;b({m:a})},O(){const {g:{setInputs:a}}=this;a({fixed:!0})},P(){const {g:{setInputs:a}}=this;a({h:!0})},K(){const {g:{setInputs:a}}=this;a({any:!0})},U(){const {g:{setInputs:a}}=this;a({l:""})},T(){const {g:{setInputs:a}}=this;a({i:""})},
V(){const {g:{setInputs:a}}=this;a({m:""})},A(){const {g:{setInputs:a}}=this;a({fixed:!1})},C(){const {g:{setInputs:a}}=this;a({h:!1})},v(){const {g:{setInputs:a}}=this;a({any:!1})}},H.prototype={j(){const {g:{D:a}}=this;a()},o(){const {g:{u:a}}=this;a()}}];function I(){}I.prototype={};class ra{}class J extends g(ra,78338680489,null,{W:1,ga:2}){}J[e]=[I,p,k,na,y,G];/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const K=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();/*

@LICENSE @webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const sa=K["12817393923"],ta=K["12817393924"],ua=K["12817393925"],va=K["12817393926"];function L(){}L.prototype={};class wa{}class M extends g(wa,783386804822,null,{X:1,ia:2}){}M[e]=[L,va,function(){}.prototype={allocator(){this.methods={j:"9fa01",o:"efe7d",D:"2f9c8",u:"bdddf",L:"77a3f",U:"2d6fb",J:"7a4f4",T:"5b606",M:"2b26e",V:"8d348",H:"ed4f2",O:"b86b7",A:"397f6",I:"ef181",P:"0f496",C:"b7a20",G:"4abea",K:"e2f68",v:"e6875"}}}];function N(){}N.prototype={};class xa{}class O extends g(xa,783386804821,null,{g:1,R:2}){}O[e]=[N,G,M,sa];var P=class extends O.implements(){};function ya({fixed:a,h:b,any:c},{fixed:f,h:Da,any:Ea}){if(a&&void 0!==f)return{h:!1,any:!1};if(b&&void 0!==Da)return{fixed:!1,any:!1};if(c&&void 0!==Ea)return{fixed:!1,h:!1}};function za(a,b,c){a={fixed:a.fixed,h:a.h,any:a.any};a=c?c(a):a;b=c?c(b):b;return this.F(a,b)};class Q extends y.implements({F:ya,adapt:[za]}){};function R(){}R.prototype={};class Aa{}class S extends g(Aa,783386804818,null,{Z:1,ka:2}){}S[e]=[R,ta];const T={};const Ba=Object.keys(T).reduce((a,b)=>{a[T[b]]=b;return a},{});function U(){}U.prototype={};class Ca{}class V extends g(Ca,783386804815,null,{$:1,la:2}){}V[e]=[U,function(){}.prototype={vdusQPs:Ba,memoryPQs:m},S,fa];function W(){}W.prototype={};class Fa{}class X extends g(Fa,783386804827,null,{fa:1,sa:2}){}X[e]=[W,ua];function Ga(){}Ga.prototype={};class Ha{}class Ia extends g(Ha,783386804825,null,{ea:1,ra:2}){}Ia[e]=[Ga,X];const Ja=Object.keys(A).reduce((a,b)=>{a[A[b]]=b;return a},{});function Ka(){}Ka.prototype={};class La{static mvc(a,b,c){return ka(this,a,b,null,c)}}class Ma extends g(La,783386804811,null,{aa:1,ma:2}){}function Y(){}function Z(a){a?a.value:void 0}
Ma[e]=[Ka,ja,J,V,Ia,Y.prototype={inputsQPs:Ja},Y.prototype={paint(){const {g:{j:a}}=this;this.u=()=>{a()}}},Y.prototype={paint({i:a}){const {g:{o:b}}=this;b();Z({value:a})}},Y.prototype={paint({l:a}){const {g:{o:b}}=this;b();Z({value:a})}},Y.prototype={paint({m:a}){const {g:{o:b}}=this;b();Z({value:a})}},Y.prototype={paint({fixed:a}){const {g:{j:b,C:c,v:f}}=this;a&&(c(),f());b();Z({value:a})}},Y.prototype={paint({h:a}){const {g:{j:b,v:c,A:f}}=this;a&&(c(),f());b();Z({value:a})}},Y.prototype={paint({any:a}){const {g:{j:b,
C:c,A:f}}=this;a&&(c(),f());b();Z({value:a})}},Y.prototype={}];var Na=class extends Ma.implements(P,Q,ma){};module.exports["78338680480"]=J;module.exports["78338680481"]=J;module.exports["78338680483"]=D;module.exports["78338680484"]=G;module.exports["783386804810"]=Na;module.exports["783386804811"]=z;module.exports["783386804830"]=y;module.exports["783386804831"]=Q;module.exports["783386804861"]=P;

//# sourceMappingURL=internal.js.map