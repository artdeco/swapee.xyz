/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntent` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIntent}
 */
class AbstractExchangeIntent extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeIntent_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeIntentPort}
 */
class ExchangeIntentPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentController}
 */
class AbstractExchangeIntentController extends (class {/* lazy-loaded */}) {}
/**
 * The _IExchangeIntent_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ExchangeIntentHtmlComponent}
 */
class ExchangeIntentHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeIntentBuffer}
 */
class ExchangeIntentBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentComputer}
 */
class AbstractExchangeIntentComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ExchangeIntentComputer}
 */
class ExchangeIntentComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ExchangeIntentController}
 */
class ExchangeIntentController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeIntent = AbstractExchangeIntent
module.exports.ExchangeIntentPort = ExchangeIntentPort
module.exports.AbstractExchangeIntentController = AbstractExchangeIntentController
module.exports.ExchangeIntentHtmlComponent = ExchangeIntentHtmlComponent
module.exports.ExchangeIntentBuffer = ExchangeIntentBuffer
module.exports.AbstractExchangeIntentComputer = AbstractExchangeIntentComputer
module.exports.ExchangeIntentComputer = ExchangeIntentComputer
module.exports.ExchangeIntentController = ExchangeIntentController