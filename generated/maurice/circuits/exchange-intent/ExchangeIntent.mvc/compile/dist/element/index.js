/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntent` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIntent}
 */
class AbstractExchangeIntent extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeIntent_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeIntentPort}
 */
class ExchangeIntentPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentController}
 */
class AbstractExchangeIntentController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IExchangeIntent_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ExchangeIntentElement}
 */
class ExchangeIntentElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeIntentBuffer}
 */
class ExchangeIntentBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIntentComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentComputer}
 */
class AbstractExchangeIntentComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ExchangeIntentController}
 */
class ExchangeIntentController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeIntent = AbstractExchangeIntent
module.exports.ExchangeIntentPort = ExchangeIntentPort
module.exports.AbstractExchangeIntentController = AbstractExchangeIntentController
module.exports.ExchangeIntentElement = ExchangeIntentElement
module.exports.ExchangeIntentBuffer = ExchangeIntentBuffer
module.exports.AbstractExchangeIntentComputer = AbstractExchangeIntentComputer
module.exports.ExchangeIntentController = ExchangeIntentController

Object.defineProperties(module.exports, {
 'AbstractExchangeIntent': {get: () => require('./precompile/internal')[78338680481]},
 [78338680481]: {get: () => module.exports['AbstractExchangeIntent']},
 'ExchangeIntentPort': {get: () => require('./precompile/internal')[78338680483]},
 [78338680483]: {get: () => module.exports['ExchangeIntentPort']},
 'AbstractExchangeIntentController': {get: () => require('./precompile/internal')[78338680484]},
 [78338680484]: {get: () => module.exports['AbstractExchangeIntentController']},
 'ExchangeIntentElement': {get: () => require('./precompile/internal')[78338680488]},
 [78338680488]: {get: () => module.exports['ExchangeIntentElement']},
 'ExchangeIntentBuffer': {get: () => require('./precompile/internal')[783386804811]},
 [783386804811]: {get: () => module.exports['ExchangeIntentBuffer']},
 'AbstractExchangeIntentComputer': {get: () => require('./precompile/internal')[783386804830]},
 [783386804830]: {get: () => module.exports['AbstractExchangeIntentComputer']},
 'ExchangeIntentController': {get: () => require('./precompile/internal')[783386804861]},
 [783386804861]: {get: () => module.exports['ExchangeIntentController']},
})