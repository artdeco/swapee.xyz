import Module from './element'

/**@extends {xyz.swapee.wc.AbstractExchangeIntent}*/
export class AbstractExchangeIntent extends Module['78338680481'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntent} */
AbstractExchangeIntent.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeIntentPort} */
export const ExchangeIntentPort=Module['78338680483']
/**@extends {xyz.swapee.wc.AbstractExchangeIntentController}*/
export class AbstractExchangeIntentController extends Module['78338680484'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentController} */
AbstractExchangeIntentController.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeIntentElement} */
export const ExchangeIntentElement=Module['78338680488']
/** @type {typeof xyz.swapee.wc.ExchangeIntentBuffer} */
export const ExchangeIntentBuffer=Module['783386804811']
/**@extends {xyz.swapee.wc.AbstractExchangeIntentComputer}*/
export class AbstractExchangeIntentComputer extends Module['783386804830'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentComputer} */
AbstractExchangeIntentComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeIntentController} */
export const ExchangeIntentController=Module['783386804861']