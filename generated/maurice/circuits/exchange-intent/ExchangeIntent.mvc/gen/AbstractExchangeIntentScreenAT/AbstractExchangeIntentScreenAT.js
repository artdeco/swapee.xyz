import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentScreenAT}
 */
function __AbstractExchangeIntentScreenAT() {}
__AbstractExchangeIntentScreenAT.prototype = /** @type {!_AbstractExchangeIntentScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentScreenAT}
 */
class _AbstractExchangeIntentScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeIntentScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentScreenAT} ‎
 */
class AbstractExchangeIntentScreenAT extends newAbstract(
 _AbstractExchangeIntentScreenAT,783386804827,null,{
  asIExchangeIntentScreenAT:1,
  superExchangeIntentScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreenAT} */
AbstractExchangeIntentScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreenAT} */
function AbstractExchangeIntentScreenATClass(){}

export default AbstractExchangeIntentScreenAT


AbstractExchangeIntentScreenAT[$implementations]=[
 __AbstractExchangeIntentScreenAT,
 UartUniversal,
]