import AbstractExchangeIntentScreenAT from '../AbstractExchangeIntentScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentScreenBack}
 */
function __AbstractExchangeIntentScreenBack() {}
__AbstractExchangeIntentScreenBack.prototype = /** @type {!_AbstractExchangeIntentScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentScreen}
 */
class _AbstractExchangeIntentScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentScreen} ‎
 */
class AbstractExchangeIntentScreenBack extends newAbstract(
 _AbstractExchangeIntentScreenBack,783386804825,null,{
  asIExchangeIntentScreen:1,
  superExchangeIntentScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreen} */
AbstractExchangeIntentScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentScreen} */
function AbstractExchangeIntentScreenBackClass(){}

export default AbstractExchangeIntentScreenBack


AbstractExchangeIntentScreenBack[$implementations]=[
 __AbstractExchangeIntentScreenBack,
 AbstractExchangeIntentScreenAT,
]