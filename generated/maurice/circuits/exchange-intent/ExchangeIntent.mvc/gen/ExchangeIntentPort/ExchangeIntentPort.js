import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {ExchangeIntentInputsPQs} from '../../pqs/ExchangeIntentInputsPQs'
import {ExchangeIntentOuterCoreConstructor} from '../ExchangeIntentCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIntentPort}
 */
function __ExchangeIntentPort() {}
__ExchangeIntentPort.prototype = /** @type {!_ExchangeIntentPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeIntentPort} */ function ExchangeIntentPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ExchangeIntentOuterCore} */ ({model:null})
  ExchangeIntentOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentPort}
 */
class _ExchangeIntentPort { }
/**
 * The port that serves as an interface to the _IExchangeIntent_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentPort} ‎
 */
export class ExchangeIntentPort extends newAbstract(
 _ExchangeIntentPort,78338680485,ExchangeIntentPortConstructor,{
  asIExchangeIntentPort:1,
  superExchangeIntentPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentPort} */
ExchangeIntentPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentPort} */
function ExchangeIntentPortClass(){}

export const ExchangeIntentPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IExchangeIntent.Pinout>}*/({
 get Port() { return ExchangeIntentPort },
})

ExchangeIntentPort[$implementations]=[
 ExchangeIntentPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentPort}*/({
  resetPort(){
   this.resetExchangeIntentPort()
  },
  resetExchangeIntentPort(){
   ExchangeIntentPortConstructor.call(this)
  },
 }),
 __ExchangeIntentPort,
 Parametric,
 ExchangeIntentPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentPort}*/({
  constructor(){
   mountPins(this.inputs,ExchangeIntentInputsPQs)
  },
 }),
]


export default ExchangeIntentPort