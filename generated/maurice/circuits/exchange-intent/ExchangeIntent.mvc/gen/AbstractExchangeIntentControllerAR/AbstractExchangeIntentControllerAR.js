import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentControllerAR}
 */
function __AbstractExchangeIntentControllerAR() {}
__AbstractExchangeIntentControllerAR.prototype = /** @type {!_AbstractExchangeIntentControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentControllerAR}
 */
class _AbstractExchangeIntentControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeIntentControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentControllerAR} ‎
 */
class AbstractExchangeIntentControllerAR extends newAbstract(
 _AbstractExchangeIntentControllerAR,783386804822,null,{
  asIExchangeIntentControllerAR:1,
  superExchangeIntentControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentControllerAR} */
AbstractExchangeIntentControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentControllerAR} */
function AbstractExchangeIntentControllerARClass(){}

export default AbstractExchangeIntentControllerAR


AbstractExchangeIntentControllerAR[$implementations]=[
 __AbstractExchangeIntentControllerAR,
 AR,
 AbstractExchangeIntentControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeIntentControllerAR}*/({
  allocator(){
   this.methods={
    change:'9fa01',
    intentChange:'efe7d',
    onChange:'2f9c8',
    onIntentChange:'bdddf',
    setCurrencyFrom:'77a3f',
    unsetCurrencyFrom:'2d6fb',
    setAmountFrom:'7a4f4',
    unsetAmountFrom:'5b606',
    setCurrencyTo:'2b26e',
    unsetCurrencyTo:'8d348',
    flipFixed:'ed4f2',
    setFixed:'b86b7',
    unsetFixed:'397f6',
    flipFloat:'ef181',
    setFloat:'0f496',
    unsetFloat:'b7a20',
    flipAny:'4abea',
    setAny:'e2f68',
    unsetAny:'e6875',
   }
  },
 }),
]