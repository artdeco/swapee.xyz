import ExchangeIntentBuffer from '../ExchangeIntentBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ExchangeIntentPortConnector} from '../ExchangeIntentPort'
import {defineEmitters} from '@mauriceguest/guest2'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentController}
 */
function __AbstractExchangeIntentController() {}
__AbstractExchangeIntentController.prototype = /** @type {!_AbstractExchangeIntentController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentController}
 */
class _AbstractExchangeIntentController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentController} ‎
 */
export class AbstractExchangeIntentController extends newAbstract(
 _AbstractExchangeIntentController,783386804819,null,{
  asIExchangeIntentController:1,
  superExchangeIntentController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentController} */
AbstractExchangeIntentController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentController} */
function AbstractExchangeIntentControllerClass(){}


AbstractExchangeIntentController[$implementations]=[
 AbstractExchangeIntentControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IExchangeIntentPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractExchangeIntentController,
 ExchangeIntentBuffer,
 IntegratedController,
 /**@type {!AbstractExchangeIntentController}*/(ExchangeIntentPortConnector),
 AbstractExchangeIntentControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentController}*/({
  constructor(){
   defineEmitters(this,{
    onChange:true,
    onIntentChange:true,
   })
  },
 }),
 AbstractExchangeIntentControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentController}*/({
  setCurrencyFrom(val){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({currencyFrom:val})
  },
  setAmountFrom(val){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({amountFrom:val})
  },
  setCurrencyTo(val){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({currencyTo:val})
  },
  setFixed(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({fixed:true})
  },
  setFloat(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({float:true})
  },
  setAny(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({any:true})
  },
  unsetCurrencyFrom(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({currencyFrom:''})
  },
  unsetAmountFrom(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({amountFrom:''})
  },
  unsetCurrencyTo(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({currencyTo:''})
  },
  unsetFixed(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({fixed:false})
  },
  unsetFloat(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({float:false})
  },
  unsetAny(){
   const{asIExchangeIntentController:{setInputs:setInputs}}=this
   setInputs({any:false})
  },
 }),
 AbstractExchangeIntentControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentController}*/({
  change(){
   const{asIExchangeIntentController:{onChange:onChange}}=this
   onChange()
  },
  intentChange(){
   const{asIExchangeIntentController:{onIntentChange:onIntentChange}}=this
   onIntentChange()
  },
 }),
]


export default AbstractExchangeIntentController