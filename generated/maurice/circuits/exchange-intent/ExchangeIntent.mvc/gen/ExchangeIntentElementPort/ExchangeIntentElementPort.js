import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIntentElementPort}
 */
function __ExchangeIntentElementPort() {}
__ExchangeIntentElementPort.prototype = /** @type {!_ExchangeIntentElementPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeIntentElementPort} */ function ExchangeIntentElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeIntentElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentElementPort}
 */
class _ExchangeIntentElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentElementPort} ‎
 */
class ExchangeIntentElementPort extends newAbstract(
 _ExchangeIntentElementPort,783386804813,ExchangeIntentElementPortConstructor,{
  asIExchangeIntentElementPort:1,
  superExchangeIntentElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentElementPort} */
ExchangeIntentElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentElementPort} */
function ExchangeIntentElementPortClass(){}

export default ExchangeIntentElementPort


ExchangeIntentElementPort[$implementations]=[
 __ExchangeIntentElementPort,
 ExchangeIntentElementPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'currency-from':undefined,
    'amount-from':undefined,
    'currency-to':undefined,
   })
  },
 }),
]