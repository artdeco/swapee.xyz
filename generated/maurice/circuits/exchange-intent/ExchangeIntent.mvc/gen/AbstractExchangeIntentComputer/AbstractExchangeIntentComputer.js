import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentComputer}
 */
function __AbstractExchangeIntentComputer() {}
__AbstractExchangeIntentComputer.prototype = /** @type {!_AbstractExchangeIntentComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentComputer}
 */
class _AbstractExchangeIntentComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentComputer} ‎
 */
export class AbstractExchangeIntentComputer extends newAbstract(
 _AbstractExchangeIntentComputer,78338680481,null,{
  asIExchangeIntentComputer:1,
  superExchangeIntentComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentComputer} */
AbstractExchangeIntentComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentComputer} */
function AbstractExchangeIntentComputerClass(){}


AbstractExchangeIntentComputer[$implementations]=[
 __AbstractExchangeIntentComputer,
 Adapter,
]


export default AbstractExchangeIntentComputer