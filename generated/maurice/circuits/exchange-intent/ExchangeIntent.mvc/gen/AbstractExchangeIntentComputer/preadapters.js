
/**@this {xyz.swapee.wc.IExchangeIntentComputer}*/
export function preadaptInterlocks(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeIntentComputer.adaptInterlocks.Form}*/
 const _inputs={
  fixed:inputs.fixed,
  float:inputs.float,
  any:inputs.any,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptInterlocks(__inputs,__changes)
 return RET
}