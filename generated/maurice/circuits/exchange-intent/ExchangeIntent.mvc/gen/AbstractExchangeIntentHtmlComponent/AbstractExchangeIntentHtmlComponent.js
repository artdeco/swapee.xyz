import AbstractExchangeIntentGPU from '../AbstractExchangeIntentGPU'
import AbstractExchangeIntentScreenBack from '../AbstractExchangeIntentScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {ExchangeIntentInputsQPs} from '../../pqs/ExchangeIntentInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractExchangeIntent from '../AbstractExchangeIntent'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentHtmlComponent}
 */
function __AbstractExchangeIntentHtmlComponent() {}
__AbstractExchangeIntentHtmlComponent.prototype = /** @type {!_AbstractExchangeIntentHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentHtmlComponent}
 */
class _AbstractExchangeIntentHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ExchangeIntentHtmlComponent} */ (res)
  }
}
/**
 * The _IExchangeIntent_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentHtmlComponent} ‎
 */
export class AbstractExchangeIntentHtmlComponent extends newAbstract(
 _AbstractExchangeIntentHtmlComponent,783386804811,null,{
  asIExchangeIntentHtmlComponent:1,
  superExchangeIntentHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentHtmlComponent} */
AbstractExchangeIntentHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentHtmlComponent} */
function AbstractExchangeIntentHtmlComponentClass(){}


AbstractExchangeIntentHtmlComponent[$implementations]=[
 __AbstractExchangeIntentHtmlComponent,
 HtmlComponent,
 AbstractExchangeIntent,
 AbstractExchangeIntentGPU,
 AbstractExchangeIntentScreenBack,
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  inputsQPs:ExchangeIntentInputsQPs,
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  paint(){
   const{asIExchangeIntentController:{
    change:change,
   }}=this
   this.onIntentChange=()=>{
    change()
   }
  },
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  paint({amountFrom:amountFrom}){
   const{asIExchangeIntentController:{
    intentChange:intentChange,
   }}=this
   intentChange()
   this.void({'value':amountFrom})
  },
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  paint({currencyFrom:currencyFrom}){
   const{asIExchangeIntentController:{
    intentChange:intentChange,
   }}=this
   intentChange()
   this.void({'value':currencyFrom})
  },
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  paint({currencyTo:currencyTo}){
   const{asIExchangeIntentController:{
    intentChange:intentChange,
   }}=this
   intentChange()
   this.void({'value':currencyTo})
  },
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  paint({fixed:fixed}){
   const{asIExchangeIntentController:{
    change:change,
    unsetFloat:unsetFloat,
    unsetAny:unsetAny,
   }}=this
   if(fixed) {
    unsetFloat()
    unsetAny()
   }
   change()
   this.void({'value':fixed})
  },
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  paint({float:float}){
   const{asIExchangeIntentController:{
    change:change,
    unsetAny:unsetAny,
    unsetFixed:unsetFixed,
   }}=this
   if(float) {
    unsetAny()
    unsetFixed()
   }
   change()
   this.void({'value':float})
  },
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  paint({any:any}){
   const{asIExchangeIntentController:{
    change:change,
    unsetFloat:unsetFloat,
    unsetFixed:unsetFixed,
   }}=this
   if(any) {
    unsetFloat()
    unsetFixed()
   }
   change()
   this.void({'value':any})
  },
 }),
 AbstractExchangeIntentHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentHtmlComponent}*/({
  void:function voidValue(value){
   value?value['value']:void 0
  },
 }),
]