import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentDisplay}
 */
function __AbstractExchangeIntentDisplay() {}
__AbstractExchangeIntentDisplay.prototype = /** @type {!_AbstractExchangeIntentDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentDisplay}
 */
class _AbstractExchangeIntentDisplay { }
/**
 * Display for presenting information from the _IExchangeIntent_.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentDisplay} ‎
 */
class AbstractExchangeIntentDisplay extends newAbstract(
 _AbstractExchangeIntentDisplay,783386804816,null,{
  asIExchangeIntentDisplay:1,
  superExchangeIntentDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentDisplay} */
AbstractExchangeIntentDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentDisplay} */
function AbstractExchangeIntentDisplayClass(){}

export default AbstractExchangeIntentDisplay


AbstractExchangeIntentDisplay[$implementations]=[
 __AbstractExchangeIntentDisplay,
 Display,
]