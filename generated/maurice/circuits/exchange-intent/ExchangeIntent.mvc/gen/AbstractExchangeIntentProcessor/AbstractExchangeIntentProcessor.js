import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentProcessor}
 */
function __AbstractExchangeIntentProcessor() {}
__AbstractExchangeIntentProcessor.prototype = /** @type {!_AbstractExchangeIntentProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentProcessor}
 */
class _AbstractExchangeIntentProcessor { }
/**
 * The processor to compute changes to the memory for the _IExchangeIntent_.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentProcessor} ‎
 */
class AbstractExchangeIntentProcessor extends newAbstract(
 _AbstractExchangeIntentProcessor,78338680488,null,{
  asIExchangeIntentProcessor:1,
  superExchangeIntentProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentProcessor} */
AbstractExchangeIntentProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentProcessor} */
function AbstractExchangeIntentProcessorClass(){}

export default AbstractExchangeIntentProcessor


AbstractExchangeIntentProcessor[$implementations]=[
 __AbstractExchangeIntentProcessor,
 AbstractExchangeIntentProcessorClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentProcessor}*/({
  flipFixed(){
   const{
    asIExchangeIntentComputer:{
     model:{fixed:fixed},
     setInfo:setInfo,
    },
   }=this
   setInfo({fixed:!fixed})
  },
  flipFloat(){
   const{
    asIExchangeIntentComputer:{
     model:{float:float},
     setInfo:setInfo,
    },
   }=this
   setInfo({float:!float})
  },
  flipAny(){
   const{
    asIExchangeIntentComputer:{
     model:{any:any},
     setInfo:setInfo,
    },
   }=this
   setInfo({any:!any})
  },
 }),
]