import AbstractExchangeIntentProcessor from '../AbstractExchangeIntentProcessor'
import {ExchangeIntentCore} from '../ExchangeIntentCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractExchangeIntentComputer} from '../AbstractExchangeIntentComputer'
import {AbstractExchangeIntentController} from '../AbstractExchangeIntentController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntent}
 */
function __AbstractExchangeIntent() {}
__AbstractExchangeIntent.prototype = /** @type {!_AbstractExchangeIntent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntent}
 */
class _AbstractExchangeIntent { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractExchangeIntent} ‎
 */
class AbstractExchangeIntent extends newAbstract(
 _AbstractExchangeIntent,78338680489,null,{
  asIExchangeIntent:1,
  superExchangeIntent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntent} */
AbstractExchangeIntent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntent} */
function AbstractExchangeIntentClass(){}

export default AbstractExchangeIntent


AbstractExchangeIntent[$implementations]=[
 __AbstractExchangeIntent,
 ExchangeIntentCore,
 AbstractExchangeIntentProcessor,
 IntegratedComponent,
 AbstractExchangeIntentComputer,
 AbstractExchangeIntentController,
]


export {AbstractExchangeIntent}