import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentScreenAR}
 */
function __AbstractExchangeIntentScreenAR() {}
__AbstractExchangeIntentScreenAR.prototype = /** @type {!_AbstractExchangeIntentScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeIntentScreenAR}
 */
class _AbstractExchangeIntentScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeIntentScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractExchangeIntentScreenAR} ‎
 */
class AbstractExchangeIntentScreenAR extends newAbstract(
 _AbstractExchangeIntentScreenAR,783386804826,null,{
  asIExchangeIntentScreenAR:1,
  superExchangeIntentScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIntentScreenAR} */
AbstractExchangeIntentScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIntentScreenAR} */
function AbstractExchangeIntentScreenARClass(){}

export default AbstractExchangeIntentScreenAR


AbstractExchangeIntentScreenAR[$implementations]=[
 __AbstractExchangeIntentScreenAR,
 AR,
 AbstractExchangeIntentScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeIntentScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractExchangeIntentScreenAR}