import AbstractExchangeIntentScreenAR from '../AbstractExchangeIntentScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {ExchangeIntentInputsPQs} from '../../pqs/ExchangeIntentInputsPQs'
import {ExchangeIntentMemoryQPs} from '../../pqs/ExchangeIntentMemoryQPs'
import {ExchangeIntentVdusPQs} from '../../pqs/ExchangeIntentVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentScreen}
 */
function __AbstractExchangeIntentScreen() {}
__AbstractExchangeIntentScreen.prototype = /** @type {!_AbstractExchangeIntentScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentScreen}
 */
class _AbstractExchangeIntentScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentScreen} ‎
 */
class AbstractExchangeIntentScreen extends newAbstract(
 _AbstractExchangeIntentScreen,783386804824,null,{
  asIExchangeIntentScreen:1,
  superExchangeIntentScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentScreen} */
AbstractExchangeIntentScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentScreen} */
function AbstractExchangeIntentScreenClass(){}

export default AbstractExchangeIntentScreen


AbstractExchangeIntentScreen[$implementations]=[
 __AbstractExchangeIntentScreen,
 AbstractExchangeIntentScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentScreen}*/({
  inputsPQs:ExchangeIntentInputsPQs,
  memoryQPs:ExchangeIntentMemoryQPs,
 }),
 Screen,
 AbstractExchangeIntentScreenAR,
 AbstractExchangeIntentScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentScreen}*/({
  vdusPQs:ExchangeIntentVdusPQs,
 }),
]