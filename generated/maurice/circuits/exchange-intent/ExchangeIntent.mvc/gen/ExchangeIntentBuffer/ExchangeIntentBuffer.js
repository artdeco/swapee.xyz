import {makeBuffers} from '@webcircuits/webcircuits'

export const ExchangeIntentBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  currencyFrom:String,
  amountFrom:String,
  currencyTo:String,
  fixed:Boolean,
  float:Boolean,
  any:Boolean,
  ready:Boolean,
 }),
})

export default ExchangeIntentBuffer