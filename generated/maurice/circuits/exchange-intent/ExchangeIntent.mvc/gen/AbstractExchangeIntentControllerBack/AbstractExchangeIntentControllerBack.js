import AbstractExchangeIntentControllerAR from '../AbstractExchangeIntentControllerAR'
import {AbstractExchangeIntentController} from '../AbstractExchangeIntentController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentControllerBack}
 */
function __AbstractExchangeIntentControllerBack() {}
__AbstractExchangeIntentControllerBack.prototype = /** @type {!_AbstractExchangeIntentControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentController}
 */
class _AbstractExchangeIntentControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentController} ‎
 */
class AbstractExchangeIntentControllerBack extends newAbstract(
 _AbstractExchangeIntentControllerBack,783386804821,null,{
  asIExchangeIntentController:1,
  superExchangeIntentController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentController} */
AbstractExchangeIntentControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentController} */
function AbstractExchangeIntentControllerBackClass(){}

export default AbstractExchangeIntentControllerBack


AbstractExchangeIntentControllerBack[$implementations]=[
 __AbstractExchangeIntentControllerBack,
 AbstractExchangeIntentController,
 AbstractExchangeIntentControllerAR,
 DriverBack,
]