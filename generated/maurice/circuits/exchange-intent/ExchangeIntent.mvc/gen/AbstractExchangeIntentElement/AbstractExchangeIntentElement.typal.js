
import AbstractExchangeIntent from '../AbstractExchangeIntent'

/** @abstract {xyz.swapee.wc.IExchangeIntentElement} */
export default class AbstractExchangeIntentElement { }



AbstractExchangeIntentElement[$implementations]=[AbstractExchangeIntent,
 /** @type {!AbstractExchangeIntentElement} */ ({
  rootId:'ExchangeIntent',
  __$id:7833868048,
  fqn:'xyz.swapee.wc.IExchangeIntent',
  maurice_element_v3:true,
 }),
]