import ExchangeIntentElementPort from '../ExchangeIntentElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {ExchangeIntentInputsPQs} from '../../pqs/ExchangeIntentInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractExchangeIntent from '../AbstractExchangeIntent'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentElement}
 */
function __AbstractExchangeIntentElement() {}
__AbstractExchangeIntentElement.prototype = /** @type {!_AbstractExchangeIntentElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentElement}
 */
class _AbstractExchangeIntentElement { }
/**
 * A component description.
 *
 * The _IExchangeIntent_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentElement} ‎
 */
class AbstractExchangeIntentElement extends newAbstract(
 _AbstractExchangeIntentElement,783386804812,null,{
  asIExchangeIntentElement:1,
  superExchangeIntentElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentElement} */
AbstractExchangeIntentElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentElement} */
function AbstractExchangeIntentElementClass(){}

export default AbstractExchangeIntentElement


AbstractExchangeIntentElement[$implementations]=[
 __AbstractExchangeIntentElement,
 ElementBase,
 AbstractExchangeIntentElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':currency-from':currencyFromColAttr,
   ':amount-from':amountFromColAttr,
   ':currency-to':currencyToColAttr,
   ':fixed':fixedColAttr,
   ':float':floatColAttr,
   ':any':anyColAttr,
   ':ready':readyColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'currency-from':currencyFromAttr,
    'amount-from':amountFromAttr,
    'currency-to':currencyToAttr,
    'fixed':fixedAttr,
    'float':floatAttr,
    'any':anyAttr,
    'ready':readyAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(currencyFromAttr===undefined?{'currency-from':currencyFromColAttr}:{}),
    ...(amountFromAttr===undefined?{'amount-from':amountFromColAttr}:{}),
    ...(currencyToAttr===undefined?{'currency-to':currencyToColAttr}:{}),
    ...(fixedAttr===undefined?{'fixed':fixedColAttr}:{}),
    ...(floatAttr===undefined?{'float':floatColAttr}:{}),
    ...(anyAttr===undefined?{'any':anyColAttr}:{}),
    ...(readyAttr===undefined?{'ready':readyColAttr}:{}),
   }
  },
 }),
 AbstractExchangeIntentElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'currency-from':currencyFromAttr,
   'amount-from':amountFromAttr,
   'currency-to':currencyToAttr,
   'fixed':fixedAttr,
   'float':floatAttr,
   'any':anyAttr,
   'ready':readyAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    currencyFrom:currencyFromAttr,
    amountFrom:amountFromAttr,
    currencyTo:currencyToAttr,
    fixed:fixedAttr,
    float:floatAttr,
    any:anyAttr,
    ready:readyAttr,
   }
  },
 }),
 AbstractExchangeIntentElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractExchangeIntentElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentElement}*/({
  inputsPQs:ExchangeIntentInputsPQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractExchangeIntentElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','currencyFrom','amountFrom','currencyTo','fixed','float','any','ready','no-solder',':no-solder','currency-from',':currency-from','amount-from',':amount-from','currency-to',':currency-to',':fixed',':float',':any',':ready','fe646','96c88','748e6','c23cd','cec31','546ad','100b8','b2fda','children']),
   })
  },
  get Port(){
   return ExchangeIntentElementPort
  },
 }),
]



AbstractExchangeIntentElement[$implementations]=[AbstractExchangeIntent,
 /** @type {!AbstractExchangeIntentElement} */ ({
  rootId:'ExchangeIntent',
  __$id:7833868048,
  fqn:'xyz.swapee.wc.IExchangeIntent',
  maurice_element_v3:true,
 }),
]