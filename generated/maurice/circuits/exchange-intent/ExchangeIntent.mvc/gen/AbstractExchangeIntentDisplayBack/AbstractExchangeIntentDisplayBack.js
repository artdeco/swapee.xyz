import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentDisplay}
 */
function __AbstractExchangeIntentDisplay() {}
__AbstractExchangeIntentDisplay.prototype = /** @type {!_AbstractExchangeIntentDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentDisplay}
 */
class _AbstractExchangeIntentDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIntentDisplay} ‎
 */
class AbstractExchangeIntentDisplay extends newAbstract(
 _AbstractExchangeIntentDisplay,783386804818,null,{
  asIExchangeIntentDisplay:1,
  superExchangeIntentDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentDisplay} */
AbstractExchangeIntentDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIntentDisplay} */
function AbstractExchangeIntentDisplayClass(){}

export default AbstractExchangeIntentDisplay


AbstractExchangeIntentDisplay[$implementations]=[
 __AbstractExchangeIntentDisplay,
 GraphicsDriverBack,
]