import {mountPins} from '@type.engineering/seers'
import {ExchangeIntentMemoryPQs} from '../../pqs/ExchangeIntentMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIntentCore}
 */
function __ExchangeIntentCore() {}
__ExchangeIntentCore.prototype = /** @type {!_ExchangeIntentCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentCore}
 */
class _ExchangeIntentCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentCore} ‎
 */
class ExchangeIntentCore extends newAbstract(
 _ExchangeIntentCore,78338680487,null,{
  asIExchangeIntentCore:1,
  superExchangeIntentCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentCore} */
ExchangeIntentCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentCore} */
function ExchangeIntentCoreClass(){}

export default ExchangeIntentCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIntentOuterCore}
 */
function __ExchangeIntentOuterCore() {}
__ExchangeIntentOuterCore.prototype = /** @type {!_ExchangeIntentOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ExchangeIntentOuterCore} */
export function ExchangeIntentOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeIntentOuterCore.Model}*/
  this.model={
    currencyFrom: undefined,
    amountFrom: undefined,
    currencyTo: undefined,
    fixed: undefined,
    float: undefined,
    any: undefined,
    ready: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentOuterCore}
 */
class _ExchangeIntentOuterCore { }
/**
 * The _IExchangeIntent_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentOuterCore} ‎
 */
export class ExchangeIntentOuterCore extends newAbstract(
 _ExchangeIntentOuterCore,78338680483,ExchangeIntentOuterCoreConstructor,{
  asIExchangeIntentOuterCore:1,
  superExchangeIntentOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentOuterCore} */
ExchangeIntentOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentOuterCore} */
function ExchangeIntentOuterCoreClass(){}


ExchangeIntentOuterCore[$implementations]=[
 __ExchangeIntentOuterCore,
 ExchangeIntentOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentOuterCore}*/({
  constructor(){
   mountPins(this.model,ExchangeIntentMemoryPQs)

  },
 }),
]

ExchangeIntentCore[$implementations]=[
 ExchangeIntentCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentCore}*/({
  resetCore(){
   this.resetExchangeIntentCore()
  },
  resetExchangeIntentCore(){
   ExchangeIntentOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ExchangeIntentOuterCore}*/(
     /**@type {!xyz.swapee.wc.IExchangeIntentOuterCore}*/(this)),
   )
  },
 }),
 __ExchangeIntentCore,
 ExchangeIntentOuterCore,
]

export {ExchangeIntentCore}