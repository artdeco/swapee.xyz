import AbstractExchangeIntentDisplay from '../AbstractExchangeIntentDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {ExchangeIntentVdusPQs} from '../../pqs/ExchangeIntentVdusPQs'
import {ExchangeIntentVdusQPs} from '../../pqs/ExchangeIntentVdusQPs'
import {ExchangeIntentMemoryPQs} from '../../pqs/ExchangeIntentMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentGPU}
 */
function __AbstractExchangeIntentGPU() {}
__AbstractExchangeIntentGPU.prototype = /** @type {!_AbstractExchangeIntentGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIntentGPU}
 */
class _AbstractExchangeIntentGPU { }
/**
 * Handles the periphery of the _IExchangeIntentDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractExchangeIntentGPU} ‎
 */
class AbstractExchangeIntentGPU extends newAbstract(
 _AbstractExchangeIntentGPU,783386804815,null,{
  asIExchangeIntentGPU:1,
  superExchangeIntentGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentGPU} */
AbstractExchangeIntentGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIntentGPU} */
function AbstractExchangeIntentGPUClass(){}

export default AbstractExchangeIntentGPU


AbstractExchangeIntentGPU[$implementations]=[
 __AbstractExchangeIntentGPU,
 AbstractExchangeIntentGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIntentGPU}*/({
  vdusPQs:ExchangeIntentVdusPQs,
  vdusQPs:ExchangeIntentVdusQPs,
  memoryPQs:ExchangeIntentMemoryPQs,
 }),
 AbstractExchangeIntentDisplay,
 BrowserView,
]