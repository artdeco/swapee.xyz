import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIntentControllerAT}
 */
function __AbstractExchangeIntentControllerAT() {}
__AbstractExchangeIntentControllerAT.prototype = /** @type {!_AbstractExchangeIntentControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeIntentControllerAT}
 */
class _AbstractExchangeIntentControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeIntentControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractExchangeIntentControllerAT} ‎
 */
class AbstractExchangeIntentControllerAT extends newAbstract(
 _AbstractExchangeIntentControllerAT,783386804823,null,{
  asIExchangeIntentControllerAT:1,
  superExchangeIntentControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIntentControllerAT} */
AbstractExchangeIntentControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIntentControllerAT} */
function AbstractExchangeIntentControllerATClass(){}

export default AbstractExchangeIntentControllerAT


AbstractExchangeIntentControllerAT[$implementations]=[
 __AbstractExchangeIntentControllerAT,
 UartUniversal,
 AbstractExchangeIntentControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeIntentControllerAT}*/({
  get asIExchangeIntentController(){
   return this
  },
  change(){
   this.uart.t("inv",{mid:'9fa01'})
  },
  intentChange(){
   this.uart.t("inv",{mid:'efe7d'})
  },
  onChange(){
   this.uart.t("inv",{mid:'2f9c8'})
  },
  onIntentChange(){
   this.uart.t("inv",{mid:'bdddf'})
  },
  setCurrencyFrom(){
   this.uart.t("inv",{mid:'77a3f',args:[...arguments]})
  },
  unsetCurrencyFrom(){
   this.uart.t("inv",{mid:'2d6fb'})
  },
  setAmountFrom(){
   this.uart.t("inv",{mid:'7a4f4',args:[...arguments]})
  },
  unsetAmountFrom(){
   this.uart.t("inv",{mid:'5b606'})
  },
  setCurrencyTo(){
   this.uart.t("inv",{mid:'2b26e',args:[...arguments]})
  },
  unsetCurrencyTo(){
   this.uart.t("inv",{mid:'8d348'})
  },
  flipFixed(){
   this.uart.t("inv",{mid:'ed4f2'})
  },
  setFixed(){
   this.uart.t("inv",{mid:'b86b7'})
  },
  unsetFixed(){
   this.uart.t("inv",{mid:'397f6'})
  },
  flipFloat(){
   this.uart.t("inv",{mid:'ef181'})
  },
  setFloat(){
   this.uart.t("inv",{mid:'0f496'})
  },
  unsetFloat(){
   this.uart.t("inv",{mid:'b7a20'})
  },
  flipAny(){
   this.uart.t("inv",{mid:'4abea'})
  },
  setAny(){
   this.uart.t("inv",{mid:'e2f68'})
  },
  unsetAny(){
   this.uart.t("inv",{mid:'e6875'})
  },
 }),
]