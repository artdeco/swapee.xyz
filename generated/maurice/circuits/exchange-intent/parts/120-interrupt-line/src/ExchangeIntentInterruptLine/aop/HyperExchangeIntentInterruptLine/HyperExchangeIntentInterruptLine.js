import AbstractHyperExchangeIntentInterruptLine from '../../../../gen/AbstractExchangeIntentInterruptLine/hyper/AbstractHyperExchangeIntentInterruptLine'
import ExchangeIntentInterruptLine from '../../ExchangeIntentInterruptLine'
import ExchangeIntentInterruptLineGeneralAspects from '../ExchangeIntentInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperExchangeIntentInterruptLine} */
export default class extends AbstractHyperExchangeIntentInterruptLine
 .consults(
  ExchangeIntentInterruptLineGeneralAspects,
 )
 .implements(
  ExchangeIntentInterruptLine,
 )
{}