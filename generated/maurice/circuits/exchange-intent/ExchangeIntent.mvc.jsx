/** @extends {xyz.swapee.wc.AbstractExchangeIntent} */
export default class AbstractExchangeIntent extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractExchangeIntentComputer} */
export class AbstractExchangeIntentComputer extends (<computer>
   <adapter name="adaptInterlocks">
    <xyz.swapee.wc.IExchangeIntentCore fixed float any />
    <outputs>
     <xyz.swapee.wc.IExchangeIntentCore fixed float any />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeIntentController} */
export class AbstractExchangeIntentController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeIntentPort} */
export class ExchangeIntentPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeIntentView} */
export class AbstractExchangeIntentView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeIntentElement} */
export class AbstractExchangeIntentElement extends (<element v3 html mv>
 <block src="./ExchangeIntent.mvc/src/ExchangeIntentElement/methods/render.jsx" />
 <inducer src="./ExchangeIntent.mvc/src/ExchangeIntentElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeIntentHtmlComponent} */
export class AbstractExchangeIntentHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>