import AbstractHyperOffersFilterInterruptLine from '../../../../gen/AbstractOffersFilterInterruptLine/hyper/AbstractHyperOffersFilterInterruptLine'
import OffersFilterInterruptLine from '../../OffersFilterInterruptLine'
import OffersFilterInterruptLineGeneralAspects from '../OffersFilterInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperOffersFilterInterruptLine} */
export default class extends AbstractHyperOffersFilterInterruptLine
 .consults(
  OffersFilterInterruptLineGeneralAspects,
 )
 .implements(
  OffersFilterInterruptLine,
 )
{}