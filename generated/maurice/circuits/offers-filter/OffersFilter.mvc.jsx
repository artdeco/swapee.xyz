/** @extends {xyz.swapee.wc.AbstractOffersFilter} */
export default class AbstractOffersFilter extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractOffersFilterComputer} */
export class AbstractOffersFilterComputer extends (<computer>
   <adapter name="adaptSelectorsIntent">
    <xyz.swapee.wc.ICryptoSelectCore $id="CryptoSelectIn" selected="real" />
    <xyz.swapee.wc.ICryptoSelectCore $id="CryptoSelectOut" selected="real" />
    <outputs>
     <xyz.swapee.wc.IExchangeIntentPort currencyTo currencyFrom />
    </outputs>
   </adapter>
   <adapter name="adaptIgnoreCryptosOut">
    <xyz.swapee.wc.ICryptoSelectCore $id="CryptoSelectIn" selected="real" />
    <outputs>
     <xyz.swapee.wc.ICryptoSelectPort $id="CryptoSelectOut" ignoreCryptos />
    </outputs>
   </adapter>
   <adapter name="adaptIgnoreCryptosIn">
    <xyz.swapee.wc.ICryptoSelectCore $id="CryptoSelectOut" selected="real" />
    <outputs>
     <xyz.swapee.wc.ICryptoSelectPort $id="CryptoSelectIn" ignoreCryptos />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersFilterController} */
export class AbstractOffersFilterController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOffersFilterPort} */
export class OffersFilterPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOffersFilterView} */
export class AbstractOffersFilterView extends (<view>
  <classes>
   <string opt name="SelectedRateType" />
   <string opt name="Ready" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersFilterElement} */
export class AbstractOffersFilterElement extends (<element v3 html mv>
 <block src="./OffersFilter.mvc/src/OffersFilterElement/methods/render.jsx" />
 <inducer src="./OffersFilter.mvc/src/OffersFilterElement/methods/inducer.jsx" />
 <element-port name="HTMLTags">
   <bool name="allowAll" />
  </element-port>
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOffersFilterHtmlComponent} */
export class AbstractOffersFilterHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.ICryptoSelect via="CryptoSelectIn" />
   <xyz.swapee.wc.ICryptoSelect via="CryptoSelectOut" />
   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
   <com.webcircuits.ui.ILoadingStripe via="ReadyLoadingStripe1" />
   <com.webcircuits.ui.ILoadingStripe via="ReadyLoadingStripe2" />
   <xyz.swapee.wc.IDealBroker via="DealBroker" />
  </connectors>

</html-ic>) { }
// </class-end>