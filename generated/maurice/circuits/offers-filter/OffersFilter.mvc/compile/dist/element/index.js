/**
 * An abstract class of `xyz.swapee.wc.IOffersFilter` interface.
 * @extends {xyz.swapee.wc.AbstractOffersFilter}
 */
class AbstractOffersFilter extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOffersFilter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OffersFilterPort}
 */
class OffersFilterPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterController` interface.
 * @extends {xyz.swapee.wc.AbstractOffersFilterController}
 */
class AbstractOffersFilterController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IOffersFilter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.OffersFilterElement}
 */
class OffersFilterElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OffersFilterBuffer}
 */
class OffersFilterBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOffersFilterComputer}
 */
class AbstractOffersFilterComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.OffersFilterController}
 */
class OffersFilterController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOffersFilter = AbstractOffersFilter
module.exports.OffersFilterPort = OffersFilterPort
module.exports.AbstractOffersFilterController = AbstractOffersFilterController
module.exports.OffersFilterElement = OffersFilterElement
module.exports.OffersFilterBuffer = OffersFilterBuffer
module.exports.AbstractOffersFilterComputer = AbstractOffersFilterComputer
module.exports.OffersFilterController = OffersFilterController

Object.defineProperties(module.exports, {
 'AbstractOffersFilter': {get: () => require('./precompile/internal')[80665300511]},
 [80665300511]: {get: () => module.exports['AbstractOffersFilter']},
 'OffersFilterPort': {get: () => require('./precompile/internal')[80665300513]},
 [80665300513]: {get: () => module.exports['OffersFilterPort']},
 'AbstractOffersFilterController': {get: () => require('./precompile/internal')[80665300514]},
 [80665300514]: {get: () => module.exports['AbstractOffersFilterController']},
 'OffersFilterElement': {get: () => require('./precompile/internal')[80665300518]},
 [80665300518]: {get: () => module.exports['OffersFilterElement']},
 'OffersFilterBuffer': {get: () => require('./precompile/internal')[806653005111]},
 [806653005111]: {get: () => module.exports['OffersFilterBuffer']},
 'AbstractOffersFilterComputer': {get: () => require('./precompile/internal')[806653005130]},
 [806653005130]: {get: () => module.exports['AbstractOffersFilterComputer']},
 'OffersFilterController': {get: () => require('./precompile/internal')[806653005161]},
 [806653005161]: {get: () => module.exports['OffersFilterController']},
})