import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractOffersFilter}*/
export class AbstractOffersFilter extends Module['80665300511'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilter} */
AbstractOffersFilter.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersFilterPort} */
export const OffersFilterPort=Module['80665300513']
/**@extends {xyz.swapee.wc.AbstractOffersFilterController}*/
export class AbstractOffersFilterController extends Module['80665300514'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterController} */
AbstractOffersFilterController.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersFilterHtmlComponent} */
export const OffersFilterHtmlComponent=Module['806653005110']
/** @type {typeof xyz.swapee.wc.OffersFilterBuffer} */
export const OffersFilterBuffer=Module['806653005111']
/**@extends {xyz.swapee.wc.AbstractOffersFilterComputer}*/
export class AbstractOffersFilterComputer extends Module['806653005130'] {}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterComputer} */
AbstractOffersFilterComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.OffersFilterComputer} */
export const OffersFilterComputer=Module['806653005131']
/** @type {typeof xyz.swapee.wc.back.OffersFilterController} */
export const OffersFilterController=Module['806653005161']