import getModule from '../precompile/internal-upd'

/**
@license
@LICENSE                      Warning!

This file links to (or embeds, in which case you'll see boundary comments like
@object-code-[start|end] around chunks), proprietary software from the package(s):
*/

const Module=getModule({},self['DEPACK_REQUIRE'])

export default Module