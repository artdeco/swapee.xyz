/**
 * An abstract class of `xyz.swapee.wc.IOffersFilter` interface.
 * @extends {xyz.swapee.wc.AbstractOffersFilter}
 */
class AbstractOffersFilter extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOffersFilter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OffersFilterPort}
 */
class OffersFilterPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterController` interface.
 * @extends {xyz.swapee.wc.AbstractOffersFilterController}
 */
class AbstractOffersFilterController extends (class {/* lazy-loaded */}) {}
/**
 * The _IOffersFilter_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.OffersFilterHtmlComponent}
 */
class OffersFilterHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OffersFilterBuffer}
 */
class OffersFilterBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOffersFilterComputer}
 */
class AbstractOffersFilterComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.OffersFilterComputer}
 */
class OffersFilterComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.OffersFilterController}
 */
class OffersFilterController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOffersFilter = AbstractOffersFilter
module.exports.OffersFilterPort = OffersFilterPort
module.exports.AbstractOffersFilterController = AbstractOffersFilterController
module.exports.OffersFilterHtmlComponent = OffersFilterHtmlComponent
module.exports.OffersFilterBuffer = OffersFilterBuffer
module.exports.AbstractOffersFilterComputer = AbstractOffersFilterComputer
module.exports.OffersFilterComputer = OffersFilterComputer
module.exports.OffersFilterController = OffersFilterController