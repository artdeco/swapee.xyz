/**
 * Display for presenting information from the _IOffersFilter_.
 * @extends {xyz.swapee.wc.OffersFilterDisplay}
 */
class OffersFilterDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.OffersFilterScreen}
 */
class OffersFilterScreen extends (class {/* lazy-loaded */}) {}

module.exports.OffersFilterDisplay = OffersFilterDisplay
module.exports.OffersFilterScreen = OffersFilterScreen