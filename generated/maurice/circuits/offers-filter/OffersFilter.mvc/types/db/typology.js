/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IOffersFilterComputer': {
  'id': 80665300511,
  'symbols': {},
  'methods': {
   'compute': 1,
   'adaptSelectorsIntent': 2,
   'adaptIgnoreCryptosOut': 3,
   'adaptIgnoreCryptosIn': 4
  }
 },
 'xyz.swapee.wc.OffersFilterMemoryPQs': {
  'id': 80665300512,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterOuterCore': {
  'id': 80665300513,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersFilterInputsPQs': {
  'id': 80665300514,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterPort': {
  'id': 80665300515,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOffersFilterPort': 2
  }
 },
 'xyz.swapee.wc.IOffersFilterPortInterface': {
  'id': 80665300516,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterCore': {
  'id': 80665300517,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOffersFilterCore': 2
  }
 },
 'xyz.swapee.wc.IOffersFilterProcessor': {
  'id': 80665300518,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilter': {
  'id': 80665300519,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterBuffer': {
  'id': 806653005110,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterHtmlComponent': {
  'id': 806653005111,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterElement': {
  'id': 806653005112,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4,
   'build': 5,
   'buildCryptoSelectIn': 6,
   'buildCryptoSelectOut': 7,
   'short': 8,
   'buildExchangeIntent': 9,
   'buildReadyLoadingStripe1': 10,
   'buildReadyLoadingStripe2': 11,
   'buildDealBroker': 12
  }
 },
 'xyz.swapee.wc.IOffersFilterElementPort': {
  'id': 806653005113,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterDesigner': {
  'id': 806653005114,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOffersFilterGPU': {
  'id': 806653005115,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterDisplay': {
  'id': 806653005116,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OffersFilterVdusPQs': {
  'id': 806653005117,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOffersFilterDisplay': {
  'id': 806653005118,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterController': {
  'id': 806653005119,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   '$ExchangeIntent_setAmountFrom_on_MinAmountLa_click': 7,
   '$ExchangeIntent_setAmountFrom_on_AmountIn_input': 8,
   '$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click': 9,
   'swapDirection': 14,
   'onSwapDirection': 15
  }
 },
 'xyz.swapee.wc.front.IOffersFilterController': {
  'id': 806653005120,
  'symbols': {},
  'methods': {
   '$ExchangeIntent_setAmountFrom_on_MinAmountLa_click': 6,
   '$ExchangeIntent_setAmountFrom_on_AmountIn_input': 7,
   '$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click': 8,
   'swapDirection': 13,
   'onSwapDirection': 14
  }
 },
 'xyz.swapee.wc.back.IOffersFilterController': {
  'id': 806653005121,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersFilterControllerAR': {
  'id': 806653005122,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterControllerAT': {
  'id': 806653005123,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterScreen': {
  'id': 806653005124,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersFilterScreen': {
  'id': 806653005125,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterScreenAR': {
  'id': 806653005126,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOffersFilterScreenAT': {
  'id': 806653005127,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterHtmlComponentUtil': {
  'id': 806653005128,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.OffersFilterClassesPQs': {
  'id': 806653005129,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.OffersFilterQueriesPQs': {
  'id': 806653005130,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOffersFilterControllerHyperslice': {
  'id': 806653005131,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOffersFilterControllerBindingHyperslice': {
  'id': 806653005132,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterControllerHyperslice': {
  'id': 806653005133,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice': {
  'id': 806653005134,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OffersFilterCachePQs': {
  'id': 806653005135,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})