/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IOffersFilterComputer={}
xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent={}
xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut={}
xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn={}
xyz.swapee.wc.IOffersFilterComputer.compute={}
xyz.swapee.wc.IOffersFilterOuterCore={}
xyz.swapee.wc.IOffersFilterOuterCore.Model={}
xyz.swapee.wc.IOffersFilterOuterCore.Model.DealBrokerCid={}
xyz.swapee.wc.IOffersFilterOuterCore.WeakModel={}
xyz.swapee.wc.IOffersFilterPort={}
xyz.swapee.wc.IOffersFilterPort.Inputs={}
xyz.swapee.wc.IOffersFilterPort.WeakInputs={}
xyz.swapee.wc.IOffersFilterCore={}
xyz.swapee.wc.IOffersFilterCore.Model={}
xyz.swapee.wc.IOffersFilterPortInterface={}
xyz.swapee.wc.IOffersFilterProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IOffersFilterController={}
xyz.swapee.wc.front.IOffersFilterControllerAT={}
xyz.swapee.wc.front.IOffersFilterScreenAR={}
xyz.swapee.wc.IOffersFilter={}
xyz.swapee.wc.IOffersFilterHtmlComponentUtil={}
xyz.swapee.wc.IOffersFilterHtmlComponent={}
xyz.swapee.wc.IOffersFilterElement={}
xyz.swapee.wc.IOffersFilterElement.build={}
xyz.swapee.wc.IOffersFilterElement.short={}
xyz.swapee.wc.IOffersFilterElementPort={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts={}
xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts={}
xyz.swapee.wc.IOffersFilterElementPort.WeakInputs={}
xyz.swapee.wc.IOffersFilterDesigner={}
xyz.swapee.wc.IOffersFilterDesigner.communicator={}
xyz.swapee.wc.IOffersFilterDesigner.relay={}
xyz.swapee.wc.IOffersFilterDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IOffersFilterDisplay={}
xyz.swapee.wc.back.IOffersFilterController={}
xyz.swapee.wc.back.IOffersFilterControllerAR={}
xyz.swapee.wc.back.IOffersFilterScreen={}
xyz.swapee.wc.back.IOffersFilterScreenAT={}
xyz.swapee.wc.IOffersFilterController={}
xyz.swapee.wc.IOffersFilterScreen={}
xyz.swapee.wc.IOffersFilterGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/02-IOffersFilterComputer.xml}  8cabbe31a5ce9d158aa70c5ff4768e77 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IOffersFilterComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterComputer)} xyz.swapee.wc.AbstractOffersFilterComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterComputer} xyz.swapee.wc.OffersFilterComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterComputer` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterComputer
 */
xyz.swapee.wc.AbstractOffersFilterComputer = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterComputer.constructor&xyz.swapee.wc.OffersFilterComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterComputer.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterComputer.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterComputer}
 */
xyz.swapee.wc.AbstractOffersFilterComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterComputer}
 */
xyz.swapee.wc.AbstractOffersFilterComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterComputer}
 */
xyz.swapee.wc.AbstractOffersFilterComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterComputer}
 */
xyz.swapee.wc.AbstractOffersFilterComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterComputer.Initialese[]) => xyz.swapee.wc.IOffersFilterComputer} xyz.swapee.wc.OffersFilterComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.OffersFilterMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.OffersFilterLand>)} xyz.swapee.wc.IOffersFilterComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IOffersFilterComputer
 */
xyz.swapee.wc.IOffersFilterComputer = class extends /** @type {xyz.swapee.wc.IOffersFilterComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent} */
xyz.swapee.wc.IOffersFilterComputer.prototype.adaptSelectorsIntent = function() {}
/** @type {xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut} */
xyz.swapee.wc.IOffersFilterComputer.prototype.adaptIgnoreCryptosOut = function() {}
/** @type {xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn} */
xyz.swapee.wc.IOffersFilterComputer.prototype.adaptIgnoreCryptosIn = function() {}
/** @type {xyz.swapee.wc.IOffersFilterComputer.compute} */
xyz.swapee.wc.IOffersFilterComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterComputer.Initialese>)} xyz.swapee.wc.OffersFilterComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterComputer} xyz.swapee.wc.IOffersFilterComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersFilterComputer_ instances.
 * @constructor xyz.swapee.wc.OffersFilterComputer
 * @implements {xyz.swapee.wc.IOffersFilterComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterComputer.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterComputer = class extends /** @type {xyz.swapee.wc.OffersFilterComputer.constructor&xyz.swapee.wc.IOffersFilterComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterComputer}
 */
xyz.swapee.wc.OffersFilterComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersFilterComputer} */
xyz.swapee.wc.RecordIOffersFilterComputer

/** @typedef {xyz.swapee.wc.IOffersFilterComputer} xyz.swapee.wc.BoundIOffersFilterComputer */

/** @typedef {xyz.swapee.wc.OffersFilterComputer} xyz.swapee.wc.BoundOffersFilterComputer */

/**
 * Contains getters to cast the _IOffersFilterComputer_ interface.
 * @interface xyz.swapee.wc.IOffersFilterComputerCaster
 */
xyz.swapee.wc.IOffersFilterComputerCaster = class { }
/**
 * Cast the _IOffersFilterComputer_ instance into the _BoundIOffersFilterComputer_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterComputer}
 */
xyz.swapee.wc.IOffersFilterComputerCaster.prototype.asIOffersFilterComputer
/**
 * Access the _OffersFilterComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterComputer}
 */
xyz.swapee.wc.IOffersFilterComputerCaster.prototype.superOffersFilterComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Form, changes: xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Form) => (void|xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Return)} xyz.swapee.wc.IOffersFilterComputer.__adaptSelectorsIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterComputer.__adaptSelectorsIntent<!xyz.swapee.wc.IOffersFilterComputer>} xyz.swapee.wc.IOffersFilterComputer._adaptSelectorsIntent */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent} */
/**
 * @param {!xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Form} form The form with inputs.
 * - `cryptoSelectInSelected` _?_ .
 * - `cryptoSelectOutSelected` _?_ .
 * @param {xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Form} changes The previous values of the form.
 * - `cryptoSelectInSelected` _?_ .
 * - `cryptoSelectOutSelected` _?_ .
 * @return {void|xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Return} The form with outputs.
 */
xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent = function(form, changes) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Form The form with inputs.
 * @prop {?} cryptoSelectInSelected .
 * @prop {?} cryptoSelectOutSelected .
 */

/** @typedef {xyz.swapee.wc.IExchangeIntentPort.Inputs.CurrencyTo&xyz.swapee.wc.IExchangeIntentPort.Inputs.CurrencyFrom} xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Form, changes: xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Form) => (void|xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Return)} xyz.swapee.wc.IOffersFilterComputer.__adaptIgnoreCryptosOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterComputer.__adaptIgnoreCryptosOut<!xyz.swapee.wc.IOffersFilterComputer>} xyz.swapee.wc.IOffersFilterComputer._adaptIgnoreCryptosOut */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut} */
/**
 * @param {!xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Form} form The form with inputs.
 * - `cryptoSelectInSelected` _?_ .
 * @param {xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Form} changes The previous values of the form.
 * - `cryptoSelectInSelected` _?_ .
 * @return {void|xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Return} The form with outputs.
 */
xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut = function(form, changes) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Form The form with inputs.
 * @prop {?} cryptoSelectInSelected .
 */

/** @typedef {xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos} xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Form, changes: xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Form) => (void|xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Return)} xyz.swapee.wc.IOffersFilterComputer.__adaptIgnoreCryptosIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterComputer.__adaptIgnoreCryptosIn<!xyz.swapee.wc.IOffersFilterComputer>} xyz.swapee.wc.IOffersFilterComputer._adaptIgnoreCryptosIn */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn} */
/**
 * @param {!xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Form} form The form with inputs.
 * - `cryptoSelectOutSelected` _?_ .
 * @param {xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Form} changes The previous values of the form.
 * - `cryptoSelectOutSelected` _?_ .
 * @return {void|xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Return} The form with outputs.
 */
xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn = function(form, changes) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Form The form with inputs.
 * @prop {?} cryptoSelectOutSelected .
 */

/** @typedef {xyz.swapee.wc.ICryptoSelectPort.Inputs.IgnoreCryptos} xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.OffersFilterMemory, land: !xyz.swapee.wc.IOffersFilterComputer.compute.Land) => void} xyz.swapee.wc.IOffersFilterComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterComputer.__compute<!xyz.swapee.wc.IOffersFilterComputer>} xyz.swapee.wc.IOffersFilterComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.OffersFilterMemory} mem The memory.
 * @param {!xyz.swapee.wc.IOffersFilterComputer.compute.Land} land The land.
 * - `CryptoSelectIn` _!xyz.swapee.wc.CryptoSelectMemory_ `.`
 * - `CryptoSelectOut` _!xyz.swapee.wc.CryptoSelectMemory_ `.`
 * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_ `.`
 * - `ReadyLoadingStripe1` _!com.webcircuits.ui.LoadingStripeMemory_ `.`
 * - `ReadyLoadingStripe2` _!com.webcircuits.ui.LoadingStripeMemory_ `.`
 * - `DealBroker` _!xyz.swapee.wc.DealBrokerMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectIn `.`
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut `.`
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent `.`
 * @prop {!com.webcircuits.ui.LoadingStripeMemory} ReadyLoadingStripe1 `.`
 * @prop {!com.webcircuits.ui.LoadingStripeMemory} ReadyLoadingStripe2 `.`
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersFilterComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/03-IOffersFilterOuterCore.xml}  d1994f43d98f96aaa45addef22500bb4 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersFilterOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterOuterCore)} xyz.swapee.wc.AbstractOffersFilterOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterOuterCore} xyz.swapee.wc.OffersFilterOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterOuterCore
 */
xyz.swapee.wc.AbstractOffersFilterOuterCore = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterOuterCore.constructor&xyz.swapee.wc.OffersFilterOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterOuterCore.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IOffersFilterOuterCore|typeof xyz.swapee.wc.OffersFilterOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterOuterCore}
 */
xyz.swapee.wc.AbstractOffersFilterOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterOuterCore}
 */
xyz.swapee.wc.AbstractOffersFilterOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IOffersFilterOuterCore|typeof xyz.swapee.wc.OffersFilterOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterOuterCore}
 */
xyz.swapee.wc.AbstractOffersFilterOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IOffersFilterOuterCore|typeof xyz.swapee.wc.OffersFilterOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterOuterCore}
 */
xyz.swapee.wc.AbstractOffersFilterOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterOuterCoreCaster)} xyz.swapee.wc.IOffersFilterOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IOffersFilter_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IOffersFilterOuterCore
 */
xyz.swapee.wc.IOffersFilterOuterCore = class extends /** @type {xyz.swapee.wc.IOffersFilterOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersFilterOuterCore.prototype.constructor = xyz.swapee.wc.IOffersFilterOuterCore

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterOuterCore.Initialese>)} xyz.swapee.wc.OffersFilterOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterOuterCore} xyz.swapee.wc.IOffersFilterOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersFilterOuterCore_ instances.
 * @constructor xyz.swapee.wc.OffersFilterOuterCore
 * @implements {xyz.swapee.wc.IOffersFilterOuterCore} The _IOffersFilter_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterOuterCore = class extends /** @type {xyz.swapee.wc.OffersFilterOuterCore.constructor&xyz.swapee.wc.IOffersFilterOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OffersFilterOuterCore.prototype.constructor = xyz.swapee.wc.OffersFilterOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterOuterCore}
 */
xyz.swapee.wc.OffersFilterOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterOuterCore.
 * @interface xyz.swapee.wc.IOffersFilterOuterCoreFields
 */
xyz.swapee.wc.IOffersFilterOuterCoreFields = class { }
/**
 * The _IOffersFilter_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IOffersFilterOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOffersFilterOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore} */
xyz.swapee.wc.RecordIOffersFilterOuterCore

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore} xyz.swapee.wc.BoundIOffersFilterOuterCore */

/** @typedef {xyz.swapee.wc.OffersFilterOuterCore} xyz.swapee.wc.BoundOffersFilterOuterCore */

/** @typedef {number} */
xyz.swapee.wc.IOffersFilterOuterCore.Model.DealBrokerCid.dealBrokerCid

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.Model.DealBrokerCid} xyz.swapee.wc.IOffersFilterOuterCore.Model The _IOffersFilter_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.DealBrokerCid} xyz.swapee.wc.IOffersFilterOuterCore.WeakModel The _IOffersFilter_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IOffersFilterOuterCore_ interface.
 * @interface xyz.swapee.wc.IOffersFilterOuterCoreCaster
 */
xyz.swapee.wc.IOffersFilterOuterCoreCaster = class { }
/**
 * Cast the _IOffersFilterOuterCore_ instance into the _BoundIOffersFilterOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterOuterCore}
 */
xyz.swapee.wc.IOffersFilterOuterCoreCaster.prototype.asIOffersFilterOuterCore
/**
 * Access the _OffersFilterOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterOuterCore}
 */
xyz.swapee.wc.IOffersFilterOuterCoreCaster.prototype.superOffersFilterOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterOuterCore.Model.DealBrokerCid  (optional overlay).
 * @prop {number} [dealBrokerCid=0] Default `0`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterOuterCore.Model.DealBrokerCid_Safe  (required overlay).
 * @prop {number} dealBrokerCid
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.DealBrokerCid  (optional overlay).
 * @prop {*} [dealBrokerCid=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.DealBrokerCid_Safe  (required overlay).
 * @prop {*} dealBrokerCid
 */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.DealBrokerCid} xyz.swapee.wc.IOffersFilterPort.Inputs.DealBrokerCid  (optional overlay). */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.DealBrokerCid_Safe} xyz.swapee.wc.IOffersFilterPort.Inputs.DealBrokerCid_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.DealBrokerCid} xyz.swapee.wc.IOffersFilterPort.WeakInputs.DealBrokerCid  (optional overlay). */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.DealBrokerCid_Safe} xyz.swapee.wc.IOffersFilterPort.WeakInputs.DealBrokerCid_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.Model.DealBrokerCid} xyz.swapee.wc.IOffersFilterCore.Model.DealBrokerCid  (optional overlay). */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.Model.DealBrokerCid_Safe} xyz.swapee.wc.IOffersFilterCore.Model.DealBrokerCid_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/04-IOffersFilterPort.xml}  f6c67604c217a4d179a17e9119e7cf0b */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOffersFilterPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterPort)} xyz.swapee.wc.AbstractOffersFilterPort.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterPort} xyz.swapee.wc.OffersFilterPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterPort` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterPort
 */
xyz.swapee.wc.AbstractOffersFilterPort = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterPort.constructor&xyz.swapee.wc.OffersFilterPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterPort.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterPort.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterPort|typeof xyz.swapee.wc.OffersFilterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterPort}
 */
xyz.swapee.wc.AbstractOffersFilterPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterPort}
 */
xyz.swapee.wc.AbstractOffersFilterPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterPort|typeof xyz.swapee.wc.OffersFilterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterPort}
 */
xyz.swapee.wc.AbstractOffersFilterPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterPort|typeof xyz.swapee.wc.OffersFilterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterPort}
 */
xyz.swapee.wc.AbstractOffersFilterPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterPort.Initialese[]) => xyz.swapee.wc.IOffersFilterPort} xyz.swapee.wc.OffersFilterPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterPortFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersFilterPort.Inputs>)} xyz.swapee.wc.IOffersFilterPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IOffersFilter_, providing input
 * pins.
 * @interface xyz.swapee.wc.IOffersFilterPort
 */
xyz.swapee.wc.IOffersFilterPort = class extends /** @type {xyz.swapee.wc.IOffersFilterPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersFilterPort.resetPort} */
xyz.swapee.wc.IOffersFilterPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IOffersFilterPort.resetOffersFilterPort} */
xyz.swapee.wc.IOffersFilterPort.prototype.resetOffersFilterPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterPort&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterPort.Initialese>)} xyz.swapee.wc.OffersFilterPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterPort} xyz.swapee.wc.IOffersFilterPort.typeof */
/**
 * A concrete class of _IOffersFilterPort_ instances.
 * @constructor xyz.swapee.wc.OffersFilterPort
 * @implements {xyz.swapee.wc.IOffersFilterPort} The port that serves as an interface to the _IOffersFilter_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterPort.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterPort = class extends /** @type {xyz.swapee.wc.OffersFilterPort.constructor&xyz.swapee.wc.IOffersFilterPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterPort}
 */
xyz.swapee.wc.OffersFilterPort.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterPort.
 * @interface xyz.swapee.wc.IOffersFilterPortFields
 */
xyz.swapee.wc.IOffersFilterPortFields = class { }
/**
 * The inputs to the _IOffersFilter_'s controller via its port.
 */
xyz.swapee.wc.IOffersFilterPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersFilterPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOffersFilterPortFields.prototype.props = /** @type {!xyz.swapee.wc.IOffersFilterPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterPort} */
xyz.swapee.wc.RecordIOffersFilterPort

/** @typedef {xyz.swapee.wc.IOffersFilterPort} xyz.swapee.wc.BoundIOffersFilterPort */

/** @typedef {xyz.swapee.wc.OffersFilterPort} xyz.swapee.wc.BoundOffersFilterPort */

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterOuterCore.WeakModel)} xyz.swapee.wc.IOffersFilterPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterOuterCore.WeakModel} xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IOffersFilter_'s controller via its port.
 * @record xyz.swapee.wc.IOffersFilterPort.Inputs
 */
xyz.swapee.wc.IOffersFilterPort.Inputs = class extends /** @type {xyz.swapee.wc.IOffersFilterPort.Inputs.constructor&xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersFilterPort.Inputs.prototype.constructor = xyz.swapee.wc.IOffersFilterPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterOuterCore.WeakModel)} xyz.swapee.wc.IOffersFilterPort.WeakInputs.constructor */
/**
 * The inputs to the _IOffersFilter_'s controller via its port.
 * @record xyz.swapee.wc.IOffersFilterPort.WeakInputs
 */
xyz.swapee.wc.IOffersFilterPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOffersFilterPort.WeakInputs.constructor&xyz.swapee.wc.IOffersFilterOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersFilterPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOffersFilterPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IOffersFilterPortInterface
 */
xyz.swapee.wc.IOffersFilterPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IOffersFilterPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IOffersFilterPortInterface.prototype.constructor = xyz.swapee.wc.IOffersFilterPortInterface

/**
 * A concrete class of _IOffersFilterPortInterface_ instances.
 * @constructor xyz.swapee.wc.OffersFilterPortInterface
 * @implements {xyz.swapee.wc.IOffersFilterPortInterface} The port interface.
 */
xyz.swapee.wc.OffersFilterPortInterface = class extends xyz.swapee.wc.IOffersFilterPortInterface { }
xyz.swapee.wc.OffersFilterPortInterface.prototype.constructor = xyz.swapee.wc.OffersFilterPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterPortInterface.Props
 * @prop {number} dealBrokerCid
 */

/**
 * Contains getters to cast the _IOffersFilterPort_ interface.
 * @interface xyz.swapee.wc.IOffersFilterPortCaster
 */
xyz.swapee.wc.IOffersFilterPortCaster = class { }
/**
 * Cast the _IOffersFilterPort_ instance into the _BoundIOffersFilterPort_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterPort}
 */
xyz.swapee.wc.IOffersFilterPortCaster.prototype.asIOffersFilterPort
/**
 * Access the _OffersFilterPort_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterPort}
 */
xyz.swapee.wc.IOffersFilterPortCaster.prototype.superOffersFilterPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterPort.__resetPort<!xyz.swapee.wc.IOffersFilterPort>} xyz.swapee.wc.IOffersFilterPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterPort.resetPort} */
/**
 * Resets the _IOffersFilter_ port.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterPort.__resetOffersFilterPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterPort.__resetOffersFilterPort<!xyz.swapee.wc.IOffersFilterPort>} xyz.swapee.wc.IOffersFilterPort._resetOffersFilterPort */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterPort.resetOffersFilterPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterPort.resetOffersFilterPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersFilterPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/09-IOffersFilterCore.xml}  218c4946ff64a05848d3d8f98dbc24c3 */
/** @typedef {number} */
xyz.swapee.wc.IOffersFilterCore.Model.DealBrokerCid.dealBrokerCid

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOffersFilterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterCore)} xyz.swapee.wc.AbstractOffersFilterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterCore} xyz.swapee.wc.OffersFilterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterCore` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterCore
 */
xyz.swapee.wc.AbstractOffersFilterCore = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterCore.constructor&xyz.swapee.wc.OffersFilterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterCore.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterCore.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterCore|typeof xyz.swapee.wc.OffersFilterCore)|(!xyz.swapee.wc.IOffersFilterOuterCore|typeof xyz.swapee.wc.OffersFilterOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterCore}
 */
xyz.swapee.wc.AbstractOffersFilterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterCore}
 */
xyz.swapee.wc.AbstractOffersFilterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterCore|typeof xyz.swapee.wc.OffersFilterCore)|(!xyz.swapee.wc.IOffersFilterOuterCore|typeof xyz.swapee.wc.OffersFilterOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterCore}
 */
xyz.swapee.wc.AbstractOffersFilterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterCore|typeof xyz.swapee.wc.OffersFilterCore)|(!xyz.swapee.wc.IOffersFilterOuterCore|typeof xyz.swapee.wc.OffersFilterOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterCore}
 */
xyz.swapee.wc.AbstractOffersFilterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterCoreCaster&xyz.swapee.wc.IOffersFilterOuterCore)} xyz.swapee.wc.IOffersFilterCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IOffersFilterCore
 */
xyz.swapee.wc.IOffersFilterCore = class extends /** @type {xyz.swapee.wc.IOffersFilterCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersFilterOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IOffersFilterCore.resetCore} */
xyz.swapee.wc.IOffersFilterCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IOffersFilterCore.resetOffersFilterCore} */
xyz.swapee.wc.IOffersFilterCore.prototype.resetOffersFilterCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterCore.Initialese>)} xyz.swapee.wc.OffersFilterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterCore} xyz.swapee.wc.IOffersFilterCore.typeof */
/**
 * A concrete class of _IOffersFilterCore_ instances.
 * @constructor xyz.swapee.wc.OffersFilterCore
 * @implements {xyz.swapee.wc.IOffersFilterCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterCore.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterCore = class extends /** @type {xyz.swapee.wc.OffersFilterCore.constructor&xyz.swapee.wc.IOffersFilterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OffersFilterCore.prototype.constructor = xyz.swapee.wc.OffersFilterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterCore}
 */
xyz.swapee.wc.OffersFilterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterCore.
 * @interface xyz.swapee.wc.IOffersFilterCoreFields
 */
xyz.swapee.wc.IOffersFilterCoreFields = class { }
/**
 * The _IOffersFilter_'s memory.
 */
xyz.swapee.wc.IOffersFilterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOffersFilterCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IOffersFilterCoreFields.prototype.props = /** @type {xyz.swapee.wc.IOffersFilterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterCore} */
xyz.swapee.wc.RecordIOffersFilterCore

/** @typedef {xyz.swapee.wc.IOffersFilterCore} xyz.swapee.wc.BoundIOffersFilterCore */

/** @typedef {xyz.swapee.wc.OffersFilterCore} xyz.swapee.wc.BoundOffersFilterCore */

/** @typedef {xyz.swapee.wc.IOffersFilterOuterCore.Model} xyz.swapee.wc.IOffersFilterCore.Model The _IOffersFilter_'s memory. */

/**
 * Contains getters to cast the _IOffersFilterCore_ interface.
 * @interface xyz.swapee.wc.IOffersFilterCoreCaster
 */
xyz.swapee.wc.IOffersFilterCoreCaster = class { }
/**
 * Cast the _IOffersFilterCore_ instance into the _BoundIOffersFilterCore_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterCore}
 */
xyz.swapee.wc.IOffersFilterCoreCaster.prototype.asIOffersFilterCore
/**
 * Access the _OffersFilterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterCore}
 */
xyz.swapee.wc.IOffersFilterCoreCaster.prototype.superOffersFilterCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterCore.__resetCore<!xyz.swapee.wc.IOffersFilterCore>} xyz.swapee.wc.IOffersFilterCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterCore.resetCore} */
/**
 * Resets the _IOffersFilter_ core.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterCore.__resetOffersFilterCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterCore.__resetOffersFilterCore<!xyz.swapee.wc.IOffersFilterCore>} xyz.swapee.wc.IOffersFilterCore._resetOffersFilterCore */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterCore.resetOffersFilterCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterCore.resetOffersFilterCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersFilterCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/10-IOffersFilterProcessor.xml}  fd5553f072624309748fdbb5566040c0 */
/** @typedef {xyz.swapee.wc.IOffersFilterComputer.Initialese&xyz.swapee.wc.IOffersFilterController.Initialese} xyz.swapee.wc.IOffersFilterProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterProcessor)} xyz.swapee.wc.AbstractOffersFilterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterProcessor} xyz.swapee.wc.OffersFilterProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterProcessor
 */
xyz.swapee.wc.AbstractOffersFilterProcessor = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterProcessor.constructor&xyz.swapee.wc.OffersFilterProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterProcessor.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!xyz.swapee.wc.IOffersFilterCore|typeof xyz.swapee.wc.OffersFilterCore)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterProcessor}
 */
xyz.swapee.wc.AbstractOffersFilterProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterProcessor}
 */
xyz.swapee.wc.AbstractOffersFilterProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!xyz.swapee.wc.IOffersFilterCore|typeof xyz.swapee.wc.OffersFilterCore)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterProcessor}
 */
xyz.swapee.wc.AbstractOffersFilterProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!xyz.swapee.wc.IOffersFilterCore|typeof xyz.swapee.wc.OffersFilterCore)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterProcessor}
 */
xyz.swapee.wc.AbstractOffersFilterProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterProcessor.Initialese[]) => xyz.swapee.wc.IOffersFilterProcessor} xyz.swapee.wc.OffersFilterProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterProcessorCaster&xyz.swapee.wc.IOffersFilterComputer&xyz.swapee.wc.IOffersFilterCore&xyz.swapee.wc.IOffersFilterController)} xyz.swapee.wc.IOffersFilterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController} xyz.swapee.wc.IOffersFilterController.typeof */
/**
 * The processor to compute changes to the memory for the _IOffersFilter_.
 * @interface xyz.swapee.wc.IOffersFilterProcessor
 */
xyz.swapee.wc.IOffersFilterProcessor = class extends /** @type {xyz.swapee.wc.IOffersFilterProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersFilterComputer.typeof&xyz.swapee.wc.IOffersFilterCore.typeof&xyz.swapee.wc.IOffersFilterController.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterProcessor.Initialese>)} xyz.swapee.wc.OffersFilterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterProcessor} xyz.swapee.wc.IOffersFilterProcessor.typeof */
/**
 * A concrete class of _IOffersFilterProcessor_ instances.
 * @constructor xyz.swapee.wc.OffersFilterProcessor
 * @implements {xyz.swapee.wc.IOffersFilterProcessor} The processor to compute changes to the memory for the _IOffersFilter_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterProcessor.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterProcessor = class extends /** @type {xyz.swapee.wc.OffersFilterProcessor.constructor&xyz.swapee.wc.IOffersFilterProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterProcessor}
 */
xyz.swapee.wc.OffersFilterProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersFilterProcessor} */
xyz.swapee.wc.RecordIOffersFilterProcessor

/** @typedef {xyz.swapee.wc.IOffersFilterProcessor} xyz.swapee.wc.BoundIOffersFilterProcessor */

/** @typedef {xyz.swapee.wc.OffersFilterProcessor} xyz.swapee.wc.BoundOffersFilterProcessor */

/**
 * Contains getters to cast the _IOffersFilterProcessor_ interface.
 * @interface xyz.swapee.wc.IOffersFilterProcessorCaster
 */
xyz.swapee.wc.IOffersFilterProcessorCaster = class { }
/**
 * Cast the _IOffersFilterProcessor_ instance into the _BoundIOffersFilterProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterProcessor}
 */
xyz.swapee.wc.IOffersFilterProcessorCaster.prototype.asIOffersFilterProcessor
/**
 * Access the _OffersFilterProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterProcessor}
 */
xyz.swapee.wc.IOffersFilterProcessorCaster.prototype.superOffersFilterProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/100-OffersFilterMemory.xml}  93458fcfef4d6177d0391dcaa936ea02 */
/**
 * The memory of the _IOffersFilter_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.OffersFilterMemory
 */
xyz.swapee.wc.OffersFilterMemory = class { }
/**
 * Default `0`.
 */
xyz.swapee.wc.OffersFilterMemory.prototype.dealBrokerCid = /** @type {number} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/102-OffersFilterInputs.xml}  a58221bcfe680222f58dfa8bc97b085b */
/**
 * The inputs of the _IOffersFilter_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.OffersFilterInputs
 */
xyz.swapee.wc.front.OffersFilterInputs = class { }
/**
 * Default `0`.
 */
xyz.swapee.wc.front.OffersFilterInputs.prototype.dealBrokerCid = /** @type {number|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/11-IOffersFilter.xml}  cbd943ebdecdc1c6972b02b4e0eaf3eb */
/**
 * An atomic wrapper for the _IOffersFilter_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.OffersFilterEnv
 */
xyz.swapee.wc.OffersFilterEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.OffersFilterEnv.prototype.offersFilter = /** @type {xyz.swapee.wc.IOffersFilter} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.IOffersFilterController.Inputs>&xyz.swapee.wc.IOffersFilterProcessor.Initialese&xyz.swapee.wc.IOffersFilterComputer.Initialese&xyz.swapee.wc.IOffersFilterController.Initialese} xyz.swapee.wc.IOffersFilter.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersFilter)} xyz.swapee.wc.AbstractOffersFilter.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilter} xyz.swapee.wc.OffersFilter.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilter` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilter
 */
xyz.swapee.wc.AbstractOffersFilter = class extends /** @type {xyz.swapee.wc.AbstractOffersFilter.constructor&xyz.swapee.wc.OffersFilter.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilter.prototype.constructor = xyz.swapee.wc.AbstractOffersFilter
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilter.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilter} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilter|typeof xyz.swapee.wc.OffersFilter)|(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilter}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilter.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilter}
 */
xyz.swapee.wc.AbstractOffersFilter.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilter}
 */
xyz.swapee.wc.AbstractOffersFilter.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilter|typeof xyz.swapee.wc.OffersFilter)|(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilter}
 */
xyz.swapee.wc.AbstractOffersFilter.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilter|typeof xyz.swapee.wc.OffersFilter)|(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilter}
 */
xyz.swapee.wc.AbstractOffersFilter.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilter.Initialese[]) => xyz.swapee.wc.IOffersFilter} xyz.swapee.wc.OffersFilterConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilter.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IOffersFilter.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IOffersFilter.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IOffersFilter.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.OffersFilterMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.OffersFilterClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterCaster&xyz.swapee.wc.IOffersFilterProcessor&xyz.swapee.wc.IOffersFilterComputer&xyz.swapee.wc.IOffersFilterController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.IOffersFilterController.Inputs, !xyz.swapee.wc.OffersFilterLand>)} xyz.swapee.wc.IOffersFilter.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterProcessor} xyz.swapee.wc.IOffersFilterProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController} xyz.swapee.wc.IOffersFilterController.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IOffersFilter
 */
xyz.swapee.wc.IOffersFilter = class extends /** @type {xyz.swapee.wc.IOffersFilter.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersFilterProcessor.typeof&xyz.swapee.wc.IOffersFilterComputer.typeof&xyz.swapee.wc.IOffersFilterController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilter* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilter.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilter.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilter&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilter.Initialese>)} xyz.swapee.wc.OffersFilter.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilter} xyz.swapee.wc.IOffersFilter.typeof */
/**
 * A concrete class of _IOffersFilter_ instances.
 * @constructor xyz.swapee.wc.OffersFilter
 * @implements {xyz.swapee.wc.IOffersFilter} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilter.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilter = class extends /** @type {xyz.swapee.wc.OffersFilter.constructor&xyz.swapee.wc.IOffersFilter.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilter* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilter.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilter* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilter.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilter.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilter}
 */
xyz.swapee.wc.OffersFilter.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilter.
 * @interface xyz.swapee.wc.IOffersFilterFields
 */
xyz.swapee.wc.IOffersFilterFields = class { }
/**
 * The input pins of the _IOffersFilter_ port.
 */
xyz.swapee.wc.IOffersFilterFields.prototype.pinout = /** @type {!xyz.swapee.wc.IOffersFilter.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilter} */
xyz.swapee.wc.RecordIOffersFilter

/** @typedef {xyz.swapee.wc.IOffersFilter} xyz.swapee.wc.BoundIOffersFilter */

/** @typedef {xyz.swapee.wc.OffersFilter} xyz.swapee.wc.BoundOffersFilter */

/** @typedef {xyz.swapee.wc.IOffersFilterController.Inputs} xyz.swapee.wc.IOffersFilter.Pinout The input pins of the _IOffersFilter_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersFilterController.Inputs>)} xyz.swapee.wc.IOffersFilterBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IOffersFilterBuffer
 */
xyz.swapee.wc.IOffersFilterBuffer = class extends /** @type {xyz.swapee.wc.IOffersFilterBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersFilterBuffer.prototype.constructor = xyz.swapee.wc.IOffersFilterBuffer

/**
 * A concrete class of _IOffersFilterBuffer_ instances.
 * @constructor xyz.swapee.wc.OffersFilterBuffer
 * @implements {xyz.swapee.wc.IOffersFilterBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.OffersFilterBuffer = class extends xyz.swapee.wc.IOffersFilterBuffer { }
xyz.swapee.wc.OffersFilterBuffer.prototype.constructor = xyz.swapee.wc.OffersFilterBuffer

/**
 * Contains getters to cast the _IOffersFilter_ interface.
 * @interface xyz.swapee.wc.IOffersFilterCaster
 */
xyz.swapee.wc.IOffersFilterCaster = class { }
/**
 * Cast the _IOffersFilter_ instance into the _BoundIOffersFilter_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilter}
 */
xyz.swapee.wc.IOffersFilterCaster.prototype.asIOffersFilter
/**
 * Access the _OffersFilter_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilter}
 */
xyz.swapee.wc.IOffersFilterCaster.prototype.superOffersFilter

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/110-OffersFilterSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersFilterMemoryPQs
 */
xyz.swapee.wc.OffersFilterMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OffersFilterMemoryPQs.prototype.constructor = xyz.swapee.wc.OffersFilterMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersFilterMemoryQPs
 * @dict
 */
xyz.swapee.wc.OffersFilterMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.OffersFilterMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.OffersFilterMemoryPQs)} xyz.swapee.wc.OffersFilterInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterMemoryPQs} xyz.swapee.wc.OffersFilterMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersFilterInputsPQs
 */
xyz.swapee.wc.OffersFilterInputsPQs = class extends /** @type {xyz.swapee.wc.OffersFilterInputsPQs.constructor&xyz.swapee.wc.OffersFilterMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OffersFilterInputsPQs.prototype.constructor = xyz.swapee.wc.OffersFilterInputsPQs

/** @typedef {function(new: xyz.swapee.wc.OffersFilterMemoryPQs)} xyz.swapee.wc.OffersFilterInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersFilterInputsQPs
 * @dict
 */
xyz.swapee.wc.OffersFilterInputsQPs = class extends /** @type {xyz.swapee.wc.OffersFilterInputsQPs.constructor&xyz.swapee.wc.OffersFilterMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OffersFilterInputsQPs.prototype.constructor = xyz.swapee.wc.OffersFilterInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OffersFilterVdusPQs
 */
xyz.swapee.wc.OffersFilterVdusPQs = class { }
xyz.swapee.wc.OffersFilterVdusPQs.prototype.constructor = xyz.swapee.wc.OffersFilterVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OffersFilterVdusQPs
 * @dict
 */
xyz.swapee.wc.OffersFilterVdusQPs = class { }
xyz.swapee.wc.OffersFilterVdusQPs.prototype.constructor = xyz.swapee.wc.OffersFilterVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/12-IOffersFilterHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IOffersFilterHtmlComponentUtilFields)} xyz.swapee.wc.IOffersFilterHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IOffersFilterHtmlComponentUtil */
xyz.swapee.wc.IOffersFilterHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IOffersFilterHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IOffersFilterHtmlComponentUtil.router} */
xyz.swapee.wc.IOffersFilterHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IOffersFilterHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.OffersFilterHtmlComponentUtil
 * @implements {xyz.swapee.wc.IOffersFilterHtmlComponentUtil} ‎
 */
xyz.swapee.wc.OffersFilterHtmlComponentUtil = class extends xyz.swapee.wc.IOffersFilterHtmlComponentUtil { }
xyz.swapee.wc.OffersFilterHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.OffersFilterHtmlComponentUtil

/**
 * Fields of the IOffersFilterHtmlComponentUtil.
 * @interface xyz.swapee.wc.IOffersFilterHtmlComponentUtilFields
 */
xyz.swapee.wc.IOffersFilterHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IOffersFilterHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IOffersFilterHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterHtmlComponentUtil} */
xyz.swapee.wc.RecordIOffersFilterHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IOffersFilterHtmlComponentUtil} xyz.swapee.wc.BoundIOffersFilterHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.OffersFilterHtmlComponentUtil} xyz.swapee.wc.BoundOffersFilterHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPort} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPort} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPort} ExchangeIntent
 * @prop {typeof com.webcircuits.ui.ILoadingStripePort} ReadyLoadingStripe1
 * @prop {typeof com.webcircuits.ui.ILoadingStripePort} ReadyLoadingStripe2
 * @prop {typeof xyz.swapee.wc.IDealBrokerPort} DealBroker
 * @prop {typeof xyz.swapee.wc.IOffersFilterPort} OffersFilter The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectIn
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!com.webcircuits.ui.LoadingStripeMemory} ReadyLoadingStripe1
 * @prop {!com.webcircuits.ui.LoadingStripeMemory} ReadyLoadingStripe2
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker
 * @prop {!xyz.swapee.wc.OffersFilterMemory} OffersFilter
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.ICryptoSelect.Pinout} CryptoSelectIn
 * @prop {!xyz.swapee.wc.ICryptoSelect.Pinout} CryptoSelectOut
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} ExchangeIntent
 * @prop {!com.webcircuits.ui.ILoadingStripe.Pinout} ReadyLoadingStripe1
 * @prop {!com.webcircuits.ui.ILoadingStripe.Pinout} ReadyLoadingStripe2
 * @prop {!xyz.swapee.wc.IDealBroker.Pinout} DealBroker
 * @prop {!xyz.swapee.wc.IOffersFilter.Pinout} OffersFilter
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IOffersFilterHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterHtmlComponentUtil.__router<!xyz.swapee.wc.IOffersFilterHtmlComponentUtil>} xyz.swapee.wc.IOffersFilterHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `CryptoSelectIn` _typeof ICryptoSelectPort_
 * - `CryptoSelectOut` _typeof ICryptoSelectPort_
 * - `ExchangeIntent` _typeof IExchangeIntentPort_
 * - `ReadyLoadingStripe1` _typeof com.webcircuits.ui.ILoadingStripePort_
 * - `ReadyLoadingStripe2` _typeof com.webcircuits.ui.ILoadingStripePort_
 * - `DealBroker` _typeof IDealBrokerPort_
 * - `OffersFilter` _typeof IOffersFilterPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `CryptoSelectIn` _!CryptoSelectMemory_
 * - `CryptoSelectOut` _!CryptoSelectMemory_
 * - `ExchangeIntent` _!ExchangeIntentMemory_
 * - `ReadyLoadingStripe1` _!com.webcircuits.ui.LoadingStripeMemory_
 * - `ReadyLoadingStripe2` _!com.webcircuits.ui.LoadingStripeMemory_
 * - `DealBroker` _!DealBrokerMemory_
 * - `OffersFilter` _!OffersFilterMemory_
 * @param {!xyz.swapee.wc.IOffersFilterHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `CryptoSelectIn` _!ICryptoSelect.Pinout_
 * - `CryptoSelectOut` _!ICryptoSelect.Pinout_
 * - `ExchangeIntent` _!IExchangeIntent.Pinout_
 * - `ReadyLoadingStripe1` _!com.webcircuits.ui.ILoadingStripe.Pinout_
 * - `ReadyLoadingStripe2` _!com.webcircuits.ui.ILoadingStripe.Pinout_
 * - `DealBroker` _!IDealBroker.Pinout_
 * - `OffersFilter` _!IOffersFilter.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersFilterHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/12-IOffersFilterHtmlComponent.xml}  cce18dcc485a09aeb8495a9858ceec2a */
/** @typedef {xyz.swapee.wc.back.IOffersFilterController.Initialese&xyz.swapee.wc.back.IOffersFilterScreen.Initialese&xyz.swapee.wc.IOffersFilter.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IOffersFilterGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IOffersFilterProcessor.Initialese&xyz.swapee.wc.IOffersFilterComputer.Initialese} xyz.swapee.wc.IOffersFilterHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterHtmlComponent)} xyz.swapee.wc.AbstractOffersFilterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterHtmlComponent} xyz.swapee.wc.OffersFilterHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterHtmlComponent
 */
xyz.swapee.wc.AbstractOffersFilterHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterHtmlComponent.constructor&xyz.swapee.wc.OffersFilterHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterHtmlComponent|typeof xyz.swapee.wc.OffersFilterHtmlComponent)|(!xyz.swapee.wc.back.IOffersFilterController|typeof xyz.swapee.wc.back.OffersFilterController)|(!xyz.swapee.wc.back.IOffersFilterScreen|typeof xyz.swapee.wc.back.OffersFilterScreen)|(!xyz.swapee.wc.IOffersFilter|typeof xyz.swapee.wc.OffersFilter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersFilterGPU|typeof xyz.swapee.wc.OffersFilterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersFilterHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersFilterHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterHtmlComponent|typeof xyz.swapee.wc.OffersFilterHtmlComponent)|(!xyz.swapee.wc.back.IOffersFilterController|typeof xyz.swapee.wc.back.OffersFilterController)|(!xyz.swapee.wc.back.IOffersFilterScreen|typeof xyz.swapee.wc.back.OffersFilterScreen)|(!xyz.swapee.wc.IOffersFilter|typeof xyz.swapee.wc.OffersFilter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersFilterGPU|typeof xyz.swapee.wc.OffersFilterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersFilterHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterHtmlComponent|typeof xyz.swapee.wc.OffersFilterHtmlComponent)|(!xyz.swapee.wc.back.IOffersFilterController|typeof xyz.swapee.wc.back.OffersFilterController)|(!xyz.swapee.wc.back.IOffersFilterScreen|typeof xyz.swapee.wc.back.OffersFilterScreen)|(!xyz.swapee.wc.IOffersFilter|typeof xyz.swapee.wc.OffersFilter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOffersFilterGPU|typeof xyz.swapee.wc.OffersFilterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOffersFilterProcessor|typeof xyz.swapee.wc.OffersFilterProcessor)|(!xyz.swapee.wc.IOffersFilterComputer|typeof xyz.swapee.wc.OffersFilterComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterHtmlComponent}
 */
xyz.swapee.wc.AbstractOffersFilterHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterHtmlComponent.Initialese[]) => xyz.swapee.wc.IOffersFilterHtmlComponent} xyz.swapee.wc.OffersFilterHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterHtmlComponentCaster&xyz.swapee.wc.back.IOffersFilterController&xyz.swapee.wc.back.IOffersFilterScreen&xyz.swapee.wc.IOffersFilter&com.webcircuits.ILanded<!xyz.swapee.wc.OffersFilterLand>&xyz.swapee.wc.IOffersFilterGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.IOffersFilterController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OffersFilterLand>&xyz.swapee.wc.IOffersFilterProcessor&xyz.swapee.wc.IOffersFilterComputer)} xyz.swapee.wc.IOffersFilterHtmlComponent.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IOffersFilterController} xyz.swapee.wc.back.IOffersFilterController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IOffersFilterScreen} xyz.swapee.wc.back.IOffersFilterScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilter} xyz.swapee.wc.IOffersFilter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterGPU} xyz.swapee.wc.IOffersFilterGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterProcessor} xyz.swapee.wc.IOffersFilterProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterComputer} xyz.swapee.wc.IOffersFilterComputer.typeof */
/**
 * The _IOffersFilter_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IOffersFilterHtmlComponent
 */
xyz.swapee.wc.IOffersFilterHtmlComponent = class extends /** @type {xyz.swapee.wc.IOffersFilterHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOffersFilterController.typeof&xyz.swapee.wc.back.IOffersFilterScreen.typeof&xyz.swapee.wc.IOffersFilter.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IOffersFilterGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IOffersFilterProcessor.typeof&xyz.swapee.wc.IOffersFilterComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterHtmlComponent.Initialese>)} xyz.swapee.wc.OffersFilterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterHtmlComponent} xyz.swapee.wc.IOffersFilterHtmlComponent.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersFilterHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.OffersFilterHtmlComponent
 * @implements {xyz.swapee.wc.IOffersFilterHtmlComponent} The _IOffersFilter_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterHtmlComponent = class extends /** @type {xyz.swapee.wc.OffersFilterHtmlComponent.constructor&xyz.swapee.wc.IOffersFilterHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterHtmlComponent}
 */
xyz.swapee.wc.OffersFilterHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersFilterHtmlComponent} */
xyz.swapee.wc.RecordIOffersFilterHtmlComponent

/** @typedef {xyz.swapee.wc.IOffersFilterHtmlComponent} xyz.swapee.wc.BoundIOffersFilterHtmlComponent */

/** @typedef {xyz.swapee.wc.OffersFilterHtmlComponent} xyz.swapee.wc.BoundOffersFilterHtmlComponent */

/**
 * Contains getters to cast the _IOffersFilterHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IOffersFilterHtmlComponentCaster
 */
xyz.swapee.wc.IOffersFilterHtmlComponentCaster = class { }
/**
 * Cast the _IOffersFilterHtmlComponent_ instance into the _BoundIOffersFilterHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterHtmlComponent}
 */
xyz.swapee.wc.IOffersFilterHtmlComponentCaster.prototype.asIOffersFilterHtmlComponent
/**
 * Access the _OffersFilterHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterHtmlComponent}
 */
xyz.swapee.wc.IOffersFilterHtmlComponentCaster.prototype.superOffersFilterHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/130-IOffersFilterElement.xml}  55d8ea6a21493e13797dcfb0a5da8650 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.OffersFilterLand>&guest.maurice.IMilleu.Initialese<!(xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IDealBroker)>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.IOffersFilterElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IOffersFilterElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterElement)} xyz.swapee.wc.AbstractOffersFilterElement.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterElement} xyz.swapee.wc.OffersFilterElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterElement` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterElement
 */
xyz.swapee.wc.AbstractOffersFilterElement = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterElement.constructor&xyz.swapee.wc.OffersFilterElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterElement.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterElement.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterElement|typeof xyz.swapee.wc.OffersFilterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterElement}
 */
xyz.swapee.wc.AbstractOffersFilterElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElement}
 */
xyz.swapee.wc.AbstractOffersFilterElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterElement|typeof xyz.swapee.wc.OffersFilterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElement}
 */
xyz.swapee.wc.AbstractOffersFilterElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterElement|typeof xyz.swapee.wc.OffersFilterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElement}
 */
xyz.swapee.wc.AbstractOffersFilterElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterElement.Initialese[]) => xyz.swapee.wc.IOffersFilterElement} xyz.swapee.wc.OffersFilterElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterElementFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.IOffersFilterElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.IOffersFilterElement.Inputs, !xyz.swapee.wc.OffersFilterLand>&com.webcircuits.ILanded<!xyz.swapee.wc.OffersFilterLand>&guest.maurice.IMilleu<!(xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.IDealBroker)>)} xyz.swapee.wc.IOffersFilterElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IOffersFilter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IOffersFilterElement
 */
xyz.swapee.wc.IOffersFilterElement = class extends /** @type {xyz.swapee.wc.IOffersFilterElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersFilterElement.solder} */
xyz.swapee.wc.IOffersFilterElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.render} */
xyz.swapee.wc.IOffersFilterElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.build} */
xyz.swapee.wc.IOffersFilterElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.buildCryptoSelectIn} */
xyz.swapee.wc.IOffersFilterElement.prototype.buildCryptoSelectIn = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.buildCryptoSelectOut} */
xyz.swapee.wc.IOffersFilterElement.prototype.buildCryptoSelectOut = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.buildExchangeIntent} */
xyz.swapee.wc.IOffersFilterElement.prototype.buildExchangeIntent = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.buildReadyLoadingStripe1} */
xyz.swapee.wc.IOffersFilterElement.prototype.buildReadyLoadingStripe1 = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.buildReadyLoadingStripe2} */
xyz.swapee.wc.IOffersFilterElement.prototype.buildReadyLoadingStripe2 = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.buildDealBroker} */
xyz.swapee.wc.IOffersFilterElement.prototype.buildDealBroker = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.short} */
xyz.swapee.wc.IOffersFilterElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.server} */
xyz.swapee.wc.IOffersFilterElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IOffersFilterElement.inducer} */
xyz.swapee.wc.IOffersFilterElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterElement&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterElement.Initialese>)} xyz.swapee.wc.OffersFilterElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement} xyz.swapee.wc.IOffersFilterElement.typeof */
/**
 * A concrete class of _IOffersFilterElement_ instances.
 * @constructor xyz.swapee.wc.OffersFilterElement
 * @implements {xyz.swapee.wc.IOffersFilterElement} A component description.
 *
 * The _IOffersFilter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterElement.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterElement = class extends /** @type {xyz.swapee.wc.OffersFilterElement.constructor&xyz.swapee.wc.IOffersFilterElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterElement}
 */
xyz.swapee.wc.OffersFilterElement.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterElement.
 * @interface xyz.swapee.wc.IOffersFilterElementFields
 */
xyz.swapee.wc.IOffersFilterElementFields = class { }
/**
 * The element-specific inputs to the _IOffersFilter_ component.
 */
xyz.swapee.wc.IOffersFilterElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersFilterElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IOffersFilterElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterElement} */
xyz.swapee.wc.RecordIOffersFilterElement

/** @typedef {xyz.swapee.wc.IOffersFilterElement} xyz.swapee.wc.BoundIOffersFilterElement */

/** @typedef {xyz.swapee.wc.OffersFilterElement} xyz.swapee.wc.BoundOffersFilterElement */

/** @typedef {xyz.swapee.wc.IOffersFilterPort.Inputs&xyz.swapee.wc.IOffersFilterDisplay.Queries&xyz.swapee.wc.IOffersFilterController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IOffersFilterElementPort.Inputs} xyz.swapee.wc.IOffersFilterElement.Inputs The element-specific inputs to the _IOffersFilter_ component. */

/**
 * Contains getters to cast the _IOffersFilterElement_ interface.
 * @interface xyz.swapee.wc.IOffersFilterElementCaster
 */
xyz.swapee.wc.IOffersFilterElementCaster = class { }
/**
 * Cast the _IOffersFilterElement_ instance into the _BoundIOffersFilterElement_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterElement}
 */
xyz.swapee.wc.IOffersFilterElementCaster.prototype.asIOffersFilterElement
/**
 * Access the _OffersFilterElement_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterElement}
 */
xyz.swapee.wc.IOffersFilterElementCaster.prototype.superOffersFilterElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.OffersFilterMemory, props: !xyz.swapee.wc.IOffersFilterElement.Inputs) => Object<string, *>} xyz.swapee.wc.IOffersFilterElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__solder<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._solder */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.OffersFilterMemory} model The model.
 * - `amountOut` _number_ Default `0`.
 * @param {!xyz.swapee.wc.IOffersFilterElement.Inputs} props The element props.
 * - `[amountOut=null]` _&#42;?_ ⤴ *IOffersFilterOuterCore.WeakModel.AmountOut* ⤴ *IOffersFilterOuterCore.WeakModel.AmountOut* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOffersFilterDisplay.Queries* Default empty string.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IOffersFilterDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOffersFilterElementPort.Inputs.NoSolder* Default `false`.
 * - `[showWhenReadyOpts]` _!Object?_ The options to pass to the _ShowWhenReady_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ShowWhenReadyOpts* Default `{}`.
 * - `[concealWhenReadyOpts]` _!Object?_ The options to pass to the _ConcealWhenReady_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[floatingLaOpts]` _!Object?_ The options to pass to the _FloatingLa_ vdu. ⤴ *IOffersFilterElementPort.Inputs.FloatingLaOpts* Default `{}`.
 * - `[fixedLaOpts]` _!Object?_ The options to pass to the _FixedLa_ vdu. ⤴ *IOffersFilterElementPort.Inputs.FixedLaOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[outWrOpts]` _!Object?_ The options to pass to the _OutWr_ vdu. ⤴ *IOffersFilterElementPort.Inputs.OutWrOpts* Default `{}`.
 * - `[inWrOpts]` _!Object?_ The options to pass to the _InWr_ vdu. ⤴ *IOffersFilterElementPort.Inputs.InWrOpts* Default `{}`.
 * - `[cryptoSelectInOpts]` _!Object?_ The options to pass to the _CryptoSelectIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.CryptoSelectInOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IOffersFilterElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[readyLoadingStripe1Opts]` _!Object?_ The options to pass to the _ReadyLoadingStripe1_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts* Default `{}`.
 * - `[readyLoadingStripe2Opts]` _!Object?_ The options to pass to the _ReadyLoadingStripe2_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IOffersFilterElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IOffersFilterElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OffersFilterMemory, instance?: !xyz.swapee.wc.IOffersFilterScreen&xyz.swapee.wc.IOffersFilterController) => !engineering.type.VNode} xyz.swapee.wc.IOffersFilterElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__render<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._render */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.OffersFilterMemory} [model] The model for the view.
 * - `amountOut` _number_ Default `0`.
 * @param {!xyz.swapee.wc.IOffersFilterScreen&xyz.swapee.wc.IOffersFilterController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOffersFilterElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IOffersFilterElement.build.Cores, instances: !xyz.swapee.wc.IOffersFilterElement.build.Instances) => ?} xyz.swapee.wc.IOffersFilterElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__build<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._build */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IOffersFilterElement.build.Cores} cores The models of components on the land.
 * - `CryptoSelectIn` _!xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `CryptoSelectOut` _!xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntentCore.Model_
 * - `ReadyLoadingStripe1` _!com.webcircuits.ui.ILoadingStripeCore.Model_
 * - `ReadyLoadingStripe2` _!com.webcircuits.ui.ILoadingStripeCore.Model_
 * - `DealBroker` _!xyz.swapee.wc.IDealBrokerCore.Model_
 * @param {!xyz.swapee.wc.IOffersFilterElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `CryptoSelectIn` _!xyz.swapee.wc.ICryptoSelect_
 * - `CryptoSelectOut` _!xyz.swapee.wc.ICryptoSelect_
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntent_
 * - `ReadyLoadingStripe1` _!com.webcircuits.ui.ILoadingStripe_
 * - `ReadyLoadingStripe2` _!com.webcircuits.ui.ILoadingStripe_
 * - `DealBroker` _!xyz.swapee.wc.IDealBroker_
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectIn
 * @prop {!xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectOut
 * @prop {!xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 * @prop {!com.webcircuits.ui.ILoadingStripeCore.Model} ReadyLoadingStripe1
 * @prop {!com.webcircuits.ui.ILoadingStripeCore.Model} ReadyLoadingStripe2
 * @prop {!xyz.swapee.wc.IDealBrokerCore.Model} DealBroker
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.ICryptoSelect} CryptoSelectIn
 * @prop {!xyz.swapee.wc.ICryptoSelect} CryptoSelectOut
 * @prop {!xyz.swapee.wc.IExchangeIntent} ExchangeIntent
 * @prop {!com.webcircuits.ui.ILoadingStripe} ReadyLoadingStripe1
 * @prop {!com.webcircuits.ui.ILoadingStripe} ReadyLoadingStripe2
 * @prop {!xyz.swapee.wc.IDealBroker} DealBroker
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ICryptoSelectCore.Model, instance: !xyz.swapee.wc.ICryptoSelect) => ?} xyz.swapee.wc.IOffersFilterElement.__buildCryptoSelectIn
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__buildCryptoSelectIn<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._buildCryptoSelectIn */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.buildCryptoSelectIn} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ICryptoSelect_ component.
 * @param {!xyz.swapee.wc.ICryptoSelectCore.Model} model
 * @param {!xyz.swapee.wc.ICryptoSelect} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.buildCryptoSelectIn = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ICryptoSelectCore.Model, instance: !xyz.swapee.wc.ICryptoSelect) => ?} xyz.swapee.wc.IOffersFilterElement.__buildCryptoSelectOut
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__buildCryptoSelectOut<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._buildCryptoSelectOut */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.buildCryptoSelectOut} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ICryptoSelect_ component.
 * @param {!xyz.swapee.wc.ICryptoSelectCore.Model} model
 * @param {!xyz.swapee.wc.ICryptoSelect} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.buildCryptoSelectOut = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeIntentCore.Model, instance: !xyz.swapee.wc.IExchangeIntent) => ?} xyz.swapee.wc.IOffersFilterElement.__buildExchangeIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__buildExchangeIntent<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._buildExchangeIntent */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.buildExchangeIntent} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeIntent_ component.
 * @param {!xyz.swapee.wc.IExchangeIntentCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeIntent} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.buildExchangeIntent = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !com.webcircuits.ui.ILoadingStripeCore.Model, instance: !com.webcircuits.ui.ILoadingStripe) => ?} xyz.swapee.wc.IOffersFilterElement.__buildReadyLoadingStripe1
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__buildReadyLoadingStripe1<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._buildReadyLoadingStripe1 */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.buildReadyLoadingStripe1} */
/**
 * Controls the VDUs using the model of the _com.webcircuits.ui.ILoadingStripe_ component.
 * @param {!com.webcircuits.ui.ILoadingStripeCore.Model} model
 * @param {!com.webcircuits.ui.ILoadingStripe} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.buildReadyLoadingStripe1 = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !com.webcircuits.ui.ILoadingStripeCore.Model, instance: !com.webcircuits.ui.ILoadingStripe) => ?} xyz.swapee.wc.IOffersFilterElement.__buildReadyLoadingStripe2
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__buildReadyLoadingStripe2<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._buildReadyLoadingStripe2 */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.buildReadyLoadingStripe2} */
/**
 * Controls the VDUs using the model of the _com.webcircuits.ui.ILoadingStripe_ component.
 * @param {!com.webcircuits.ui.ILoadingStripeCore.Model} model
 * @param {!com.webcircuits.ui.ILoadingStripe} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.buildReadyLoadingStripe2 = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IDealBrokerCore.Model, instance: !xyz.swapee.wc.IDealBroker) => ?} xyz.swapee.wc.IOffersFilterElement.__buildDealBroker
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__buildDealBroker<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._buildDealBroker */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.buildDealBroker} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IDealBroker_ component.
 * @param {!xyz.swapee.wc.IDealBrokerCore.Model} model
 * @param {!xyz.swapee.wc.IDealBroker} instance
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.buildDealBroker = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.OffersFilterMemory, ports: !xyz.swapee.wc.IOffersFilterElement.short.Ports, cores: !xyz.swapee.wc.IOffersFilterElement.short.Cores) => ?} xyz.swapee.wc.IOffersFilterElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__short<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._short */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.OffersFilterMemory} model The model from which to feed properties to peer's ports.
 * - `amountOut` _number_ Default `0`.
 * @param {!xyz.swapee.wc.IOffersFilterElement.short.Ports} ports The ports of the peers.
 * - `CryptoSelectIn` _typeof xyz.swapee.wc.ICryptoSelectPortInterface_
 * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectPortInterface_
 * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentPortInterface_
 * - `ReadyLoadingStripe1` _typeof com.webcircuits.ui.ILoadingStripePortInterface_
 * - `ReadyLoadingStripe2` _typeof com.webcircuits.ui.ILoadingStripePortInterface_
 * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerPortInterface_
 * @param {!xyz.swapee.wc.IOffersFilterElement.short.Cores} cores The cores of the peers.
 * - `CryptoSelectIn` _xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `CryptoSelectOut` _xyz.swapee.wc.ICryptoSelectCore.Model_
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntentCore.Model_
 * - `ReadyLoadingStripe1` _com.webcircuits.ui.ILoadingStripeCore.Model_
 * - `ReadyLoadingStripe2` _com.webcircuits.ui.ILoadingStripeCore.Model_
 * - `DealBroker` _xyz.swapee.wc.IDealBrokerCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPortInterface} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectPortInterface} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPortInterface} ExchangeIntent
 * @prop {typeof com.webcircuits.ui.ILoadingStripePortInterface} ReadyLoadingStripe1
 * @prop {typeof com.webcircuits.ui.ILoadingStripePortInterface} ReadyLoadingStripe2
 * @prop {typeof xyz.swapee.wc.IDealBrokerPortInterface} DealBroker
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectIn
 * @prop {xyz.swapee.wc.ICryptoSelectCore.Model} CryptoSelectOut
 * @prop {xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 * @prop {com.webcircuits.ui.ILoadingStripeCore.Model} ReadyLoadingStripe1
 * @prop {com.webcircuits.ui.ILoadingStripeCore.Model} ReadyLoadingStripe2
 * @prop {xyz.swapee.wc.IDealBrokerCore.Model} DealBroker
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OffersFilterMemory, inputs: !xyz.swapee.wc.IOffersFilterElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IOffersFilterElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__server<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._server */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.OffersFilterMemory} memory The memory registers.
 * - `amountOut` _number_ Default `0`.
 * @param {!xyz.swapee.wc.IOffersFilterElement.Inputs} inputs The inputs to the port.
 * - `[amountOut=null]` _&#42;?_ ⤴ *IOffersFilterOuterCore.WeakModel.AmountOut* ⤴ *IOffersFilterOuterCore.WeakModel.AmountOut* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOffersFilterDisplay.Queries* Default empty string.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IOffersFilterDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOffersFilterElementPort.Inputs.NoSolder* Default `false`.
 * - `[showWhenReadyOpts]` _!Object?_ The options to pass to the _ShowWhenReady_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ShowWhenReadyOpts* Default `{}`.
 * - `[concealWhenReadyOpts]` _!Object?_ The options to pass to the _ConcealWhenReady_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[floatingLaOpts]` _!Object?_ The options to pass to the _FloatingLa_ vdu. ⤴ *IOffersFilterElementPort.Inputs.FloatingLaOpts* Default `{}`.
 * - `[fixedLaOpts]` _!Object?_ The options to pass to the _FixedLa_ vdu. ⤴ *IOffersFilterElementPort.Inputs.FixedLaOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[outWrOpts]` _!Object?_ The options to pass to the _OutWr_ vdu. ⤴ *IOffersFilterElementPort.Inputs.OutWrOpts* Default `{}`.
 * - `[inWrOpts]` _!Object?_ The options to pass to the _InWr_ vdu. ⤴ *IOffersFilterElementPort.Inputs.InWrOpts* Default `{}`.
 * - `[cryptoSelectInOpts]` _!Object?_ The options to pass to the _CryptoSelectIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.CryptoSelectInOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IOffersFilterElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[readyLoadingStripe1Opts]` _!Object?_ The options to pass to the _ReadyLoadingStripe1_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts* Default `{}`.
 * - `[readyLoadingStripe2Opts]` _!Object?_ The options to pass to the _ReadyLoadingStripe2_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IOffersFilterElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOffersFilterElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OffersFilterMemory, port?: !xyz.swapee.wc.IOffersFilterElement.Inputs) => ?} xyz.swapee.wc.IOffersFilterElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterElement.__inducer<!xyz.swapee.wc.IOffersFilterElement>} xyz.swapee.wc.IOffersFilterElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.OffersFilterMemory} [model] The model of the component into which to induce the state.
 * - `amountOut` _number_ Default `0`.
 * @param {!xyz.swapee.wc.IOffersFilterElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[amountOut=null]` _&#42;?_ ⤴ *IOffersFilterOuterCore.WeakModel.AmountOut* ⤴ *IOffersFilterOuterCore.WeakModel.AmountOut* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOffersFilterDisplay.Queries* Default empty string.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IOffersFilterDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOffersFilterElementPort.Inputs.NoSolder* Default `false`.
 * - `[showWhenReadyOpts]` _!Object?_ The options to pass to the _ShowWhenReady_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ShowWhenReadyOpts* Default `{}`.
 * - `[concealWhenReadyOpts]` _!Object?_ The options to pass to the _ConcealWhenReady_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[floatingLaOpts]` _!Object?_ The options to pass to the _FloatingLa_ vdu. ⤴ *IOffersFilterElementPort.Inputs.FloatingLaOpts* Default `{}`.
 * - `[fixedLaOpts]` _!Object?_ The options to pass to the _FixedLa_ vdu. ⤴ *IOffersFilterElementPort.Inputs.FixedLaOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[outWrOpts]` _!Object?_ The options to pass to the _OutWr_ vdu. ⤴ *IOffersFilterElementPort.Inputs.OutWrOpts* Default `{}`.
 * - `[inWrOpts]` _!Object?_ The options to pass to the _InWr_ vdu. ⤴ *IOffersFilterElementPort.Inputs.InWrOpts* Default `{}`.
 * - `[cryptoSelectInOpts]` _!Object?_ The options to pass to the _CryptoSelectIn_ vdu. ⤴ *IOffersFilterElementPort.Inputs.CryptoSelectInOpts* Default `{}`.
 * - `[cryptoSelectOutOpts]` _!Object?_ The options to pass to the _CryptoSelectOut_ vdu. ⤴ *IOffersFilterElementPort.Inputs.CryptoSelectOutOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * - `[readyLoadingStripe1Opts]` _!Object?_ The options to pass to the _ReadyLoadingStripe1_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts* Default `{}`.
 * - `[readyLoadingStripe2Opts]` _!Object?_ The options to pass to the _ReadyLoadingStripe2_ vdu. ⤴ *IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IOffersFilterElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IOffersFilterElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersFilterElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/140-IOffersFilterElementPort.xml}  a1264c0d2596bcc0f506fb9500809193 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOffersFilterElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterElementPort)} xyz.swapee.wc.AbstractOffersFilterElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterElementPort} xyz.swapee.wc.OffersFilterElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterElementPort
 */
xyz.swapee.wc.AbstractOffersFilterElementPort = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterElementPort.constructor&xyz.swapee.wc.OffersFilterElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterElementPort.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterElementPort|typeof xyz.swapee.wc.OffersFilterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterElementPort}
 */
xyz.swapee.wc.AbstractOffersFilterElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElementPort}
 */
xyz.swapee.wc.AbstractOffersFilterElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterElementPort|typeof xyz.swapee.wc.OffersFilterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElementPort}
 */
xyz.swapee.wc.AbstractOffersFilterElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterElementPort|typeof xyz.swapee.wc.OffersFilterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterElementPort}
 */
xyz.swapee.wc.AbstractOffersFilterElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterElementPort.Initialese[]) => xyz.swapee.wc.IOffersFilterElementPort} xyz.swapee.wc.OffersFilterElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOffersFilterElementPort.Inputs>)} xyz.swapee.wc.IOffersFilterElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IOffersFilterElementPort
 */
xyz.swapee.wc.IOffersFilterElementPort = class extends /** @type {xyz.swapee.wc.IOffersFilterElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterElementPort.Initialese>)} xyz.swapee.wc.OffersFilterElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort} xyz.swapee.wc.IOffersFilterElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOffersFilterElementPort_ instances.
 * @constructor xyz.swapee.wc.OffersFilterElementPort
 * @implements {xyz.swapee.wc.IOffersFilterElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterElementPort.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterElementPort = class extends /** @type {xyz.swapee.wc.OffersFilterElementPort.constructor&xyz.swapee.wc.IOffersFilterElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterElementPort}
 */
xyz.swapee.wc.OffersFilterElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterElementPort.
 * @interface xyz.swapee.wc.IOffersFilterElementPortFields
 */
xyz.swapee.wc.IOffersFilterElementPortFields = class { }
/**
 * The inputs to the _IOffersFilterElement_'s controller via its element port.
 */
xyz.swapee.wc.IOffersFilterElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersFilterElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOffersFilterElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IOffersFilterElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterElementPort} */
xyz.swapee.wc.RecordIOffersFilterElementPort

/** @typedef {xyz.swapee.wc.IOffersFilterElementPort} xyz.swapee.wc.BoundIOffersFilterElementPort */

/** @typedef {xyz.swapee.wc.OffersFilterElementPort} xyz.swapee.wc.BoundOffersFilterElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder.noSolder

/** @typedef {boolean} */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll.allowAll

/**
 * The options to pass to the _AmountIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts.amountInOpts

/**
 * The options to pass to the _ShowWhenReady_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts.showWhenReadyOpts

/**
 * The options to pass to the _ConcealWhenReady_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts.concealWhenReadyOpts

/**
 * The options to pass to the _AmountOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts.amountOutOpts

/**
 * The options to pass to the _SwapBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts.swapBuOpts

/**
 * The options to pass to the _FloatingLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts.floatingLaOpts

/**
 * The options to pass to the _FixedLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts.fixedLaOpts

/**
 * The options to pass to the _AnyLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts.anyLaOpts

/**
 * The options to pass to the _AmountOutLoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts.amountOutLoInOpts

/**
 * The options to pass to the _ShowWhenSelectedIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts.showWhenSelectedInOpts

/**
 * The options to pass to the _ShowWhenSelectedOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts.showWhenSelectedOutOpts

/**
 * The options to pass to the _MinAmountWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts.minAmountWrOpts

/**
 * The options to pass to the _MinAmountLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts.minAmountLaOpts

/**
 * The options to pass to the _MaxAmountWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts.maxAmountWrOpts

/**
 * The options to pass to the _MaxAmountLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts.maxAmountLaOpts

/**
 * The options to pass to the _OutWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts.outWrOpts

/**
 * The options to pass to the _InWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts.inWrOpts

/**
 * The options to pass to the _CryptoSelectIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts.cryptoSelectInOpts

/**
 * The options to pass to the _CryptoSelectOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts.cryptoSelectOutOpts

/**
 * The options to pass to the _ExchangeIntent_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

/**
 * The options to pass to the _ReadyLoadingStripe1_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts.readyLoadingStripe1Opts

/**
 * The options to pass to the _ReadyLoadingStripe2_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts.readyLoadingStripe2Opts

/**
 * The options to pass to the _DealBroker_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts.dealBrokerOpts

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts&xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts)} xyz.swapee.wc.IOffersFilterElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder} xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts} xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts.typeof */
/**
 * The inputs to the _IOffersFilterElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOffersFilterElementPort.Inputs
 */
xyz.swapee.wc.IOffersFilterElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IOffersFilterElementPort.Inputs.constructor&xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts.typeof&xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersFilterElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IOffersFilterElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AllowAll&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountInOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenReadyOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ConcealWhenReadyOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.SwapBuOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FloatingLaOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FixedLaOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AnyLaOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutLoInOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedInOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedOutOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountWrOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountLaOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountWrOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountLaOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.OutWrOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.InWrOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectInOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectOutOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ExchangeIntentOpts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe1Opts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe2Opts&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.DealBrokerOpts)} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AllowAll} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AllowAll.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountInOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenReadyOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenReadyOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ConcealWhenReadyOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ConcealWhenReadyOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.SwapBuOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.SwapBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FloatingLaOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FloatingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FixedLaOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FixedLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AnyLaOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AnyLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutLoInOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedInOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedOutOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountWrOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountLaOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountWrOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountLaOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.OutWrOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.OutWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.InWrOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.InWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectInOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectOutOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ExchangeIntentOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ExchangeIntentOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe1Opts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe1Opts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe2Opts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe2Opts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.DealBrokerOpts} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.DealBrokerOpts.typeof */
/**
 * The inputs to the _IOffersFilterElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOffersFilterElementPort.WeakInputs
 */
xyz.swapee.wc.IOffersFilterElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.constructor&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AllowAll.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenReadyOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ConcealWhenReadyOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.SwapBuOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FloatingLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FixedLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AnyLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutLoInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedOutOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountLaOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.OutWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.InWrOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectInOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectOutOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ExchangeIntentOpts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe1Opts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe2Opts.typeof&xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.DealBrokerOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOffersFilterElementPort.WeakInputs

/**
 * Contains getters to cast the _IOffersFilterElementPort_ interface.
 * @interface xyz.swapee.wc.IOffersFilterElementPortCaster
 */
xyz.swapee.wc.IOffersFilterElementPortCaster = class { }
/**
 * Cast the _IOffersFilterElementPort_ instance into the _BoundIOffersFilterElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterElementPort}
 */
xyz.swapee.wc.IOffersFilterElementPortCaster.prototype.asIOffersFilterElementPort
/**
 * Access the _OffersFilterElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterElementPort}
 */
xyz.swapee.wc.IOffersFilterElementPortCaster.prototype.superOffersFilterElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll  (optional overlay).
 * @prop {boolean} [allowAll=false] Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AllowAll_Safe  (required overlay).
 * @prop {boolean} allowAll
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {!Object} [amountInOpts] The options to pass to the _AmountIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {!Object} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts The options to pass to the _ShowWhenReady_ vdu (optional overlay).
 * @prop {!Object} [showWhenReadyOpts] The options to pass to the _ShowWhenReady_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenReadyOpts_Safe The options to pass to the _ShowWhenReady_ vdu (required overlay).
 * @prop {!Object} showWhenReadyOpts The options to pass to the _ShowWhenReady_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts The options to pass to the _ConcealWhenReady_ vdu (optional overlay).
 * @prop {!Object} [concealWhenReadyOpts] The options to pass to the _ConcealWhenReady_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ConcealWhenReadyOpts_Safe The options to pass to the _ConcealWhenReady_ vdu (required overlay).
 * @prop {!Object} concealWhenReadyOpts The options to pass to the _ConcealWhenReady_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts The options to pass to the _AmountOut_ vdu (optional overlay).
 * @prop {!Object} [amountOutOpts] The options to pass to the _AmountOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutOpts_Safe The options to pass to the _AmountOut_ vdu (required overlay).
 * @prop {!Object} amountOutOpts The options to pass to the _AmountOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts The options to pass to the _SwapBu_ vdu (optional overlay).
 * @prop {!Object} [swapBuOpts] The options to pass to the _SwapBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.SwapBuOpts_Safe The options to pass to the _SwapBu_ vdu (required overlay).
 * @prop {!Object} swapBuOpts The options to pass to the _SwapBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts The options to pass to the _FloatingLa_ vdu (optional overlay).
 * @prop {!Object} [floatingLaOpts] The options to pass to the _FloatingLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.FloatingLaOpts_Safe The options to pass to the _FloatingLa_ vdu (required overlay).
 * @prop {!Object} floatingLaOpts The options to pass to the _FloatingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts The options to pass to the _FixedLa_ vdu (optional overlay).
 * @prop {!Object} [fixedLaOpts] The options to pass to the _FixedLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.FixedLaOpts_Safe The options to pass to the _FixedLa_ vdu (required overlay).
 * @prop {!Object} fixedLaOpts The options to pass to the _FixedLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts The options to pass to the _AnyLa_ vdu (optional overlay).
 * @prop {!Object} [anyLaOpts] The options to pass to the _AnyLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AnyLaOpts_Safe The options to pass to the _AnyLa_ vdu (required overlay).
 * @prop {!Object} anyLaOpts The options to pass to the _AnyLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu (optional overlay).
 * @prop {!Object} [amountOutLoInOpts] The options to pass to the _AmountOutLoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.AmountOutLoInOpts_Safe The options to pass to the _AmountOutLoIn_ vdu (required overlay).
 * @prop {!Object} amountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts The options to pass to the _ShowWhenSelectedIn_ vdu (optional overlay).
 * @prop {!Object} [showWhenSelectedInOpts] The options to pass to the _ShowWhenSelectedIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedInOpts_Safe The options to pass to the _ShowWhenSelectedIn_ vdu (required overlay).
 * @prop {!Object} showWhenSelectedInOpts The options to pass to the _ShowWhenSelectedIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts The options to pass to the _ShowWhenSelectedOut_ vdu (optional overlay).
 * @prop {!Object} [showWhenSelectedOutOpts] The options to pass to the _ShowWhenSelectedOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ShowWhenSelectedOutOpts_Safe The options to pass to the _ShowWhenSelectedOut_ vdu (required overlay).
 * @prop {!Object} showWhenSelectedOutOpts The options to pass to the _ShowWhenSelectedOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts The options to pass to the _MinAmountWr_ vdu (optional overlay).
 * @prop {!Object} [minAmountWrOpts] The options to pass to the _MinAmountWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountWrOpts_Safe The options to pass to the _MinAmountWr_ vdu (required overlay).
 * @prop {!Object} minAmountWrOpts The options to pass to the _MinAmountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts The options to pass to the _MinAmountLa_ vdu (optional overlay).
 * @prop {!Object} [minAmountLaOpts] The options to pass to the _MinAmountLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MinAmountLaOpts_Safe The options to pass to the _MinAmountLa_ vdu (required overlay).
 * @prop {!Object} minAmountLaOpts The options to pass to the _MinAmountLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts The options to pass to the _MaxAmountWr_ vdu (optional overlay).
 * @prop {!Object} [maxAmountWrOpts] The options to pass to the _MaxAmountWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountWrOpts_Safe The options to pass to the _MaxAmountWr_ vdu (required overlay).
 * @prop {!Object} maxAmountWrOpts The options to pass to the _MaxAmountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts The options to pass to the _MaxAmountLa_ vdu (optional overlay).
 * @prop {!Object} [maxAmountLaOpts] The options to pass to the _MaxAmountLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.MaxAmountLaOpts_Safe The options to pass to the _MaxAmountLa_ vdu (required overlay).
 * @prop {!Object} maxAmountLaOpts The options to pass to the _MaxAmountLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts The options to pass to the _OutWr_ vdu (optional overlay).
 * @prop {!Object} [outWrOpts] The options to pass to the _OutWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.OutWrOpts_Safe The options to pass to the _OutWr_ vdu (required overlay).
 * @prop {!Object} outWrOpts The options to pass to the _OutWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts The options to pass to the _InWr_ vdu (optional overlay).
 * @prop {!Object} [inWrOpts] The options to pass to the _InWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.InWrOpts_Safe The options to pass to the _InWr_ vdu (required overlay).
 * @prop {!Object} inWrOpts The options to pass to the _InWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu (optional overlay).
 * @prop {!Object} [cryptoSelectInOpts] The options to pass to the _CryptoSelectIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectInOpts_Safe The options to pass to the _CryptoSelectIn_ vdu (required overlay).
 * @prop {!Object} cryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu (optional overlay).
 * @prop {!Object} [cryptoSelectOutOpts] The options to pass to the _CryptoSelectOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.CryptoSelectOutOpts_Safe The options to pass to the _CryptoSelectOut_ vdu (required overlay).
 * @prop {!Object} cryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {!Object} [exchangeIntentOpts] The options to pass to the _ExchangeIntent_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {!Object} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts The options to pass to the _ReadyLoadingStripe1_ vdu (optional overlay).
 * @prop {!Object} [readyLoadingStripe1Opts] The options to pass to the _ReadyLoadingStripe1_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe1Opts_Safe The options to pass to the _ReadyLoadingStripe1_ vdu (required overlay).
 * @prop {!Object} readyLoadingStripe1Opts The options to pass to the _ReadyLoadingStripe1_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts The options to pass to the _ReadyLoadingStripe2_ vdu (optional overlay).
 * @prop {!Object} [readyLoadingStripe2Opts] The options to pass to the _ReadyLoadingStripe2_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.ReadyLoadingStripe2Opts_Safe The options to pass to the _ReadyLoadingStripe2_ vdu (required overlay).
 * @prop {!Object} readyLoadingStripe2Opts The options to pass to the _ReadyLoadingStripe2_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts The options to pass to the _DealBroker_ vdu (optional overlay).
 * @prop {!Object} [dealBrokerOpts] The options to pass to the _DealBroker_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.Inputs.DealBrokerOpts_Safe The options to pass to the _DealBroker_ vdu (required overlay).
 * @prop {!Object} dealBrokerOpts The options to pass to the _DealBroker_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AllowAll  (optional overlay).
 * @prop {*} [allowAll=null] Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AllowAll_Safe  (required overlay).
 * @prop {*} allowAll
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {*} [amountInOpts=null] The options to pass to the _AmountIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {*} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenReadyOpts The options to pass to the _ShowWhenReady_ vdu (optional overlay).
 * @prop {*} [showWhenReadyOpts=null] The options to pass to the _ShowWhenReady_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenReadyOpts_Safe The options to pass to the _ShowWhenReady_ vdu (required overlay).
 * @prop {*} showWhenReadyOpts The options to pass to the _ShowWhenReady_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ConcealWhenReadyOpts The options to pass to the _ConcealWhenReady_ vdu (optional overlay).
 * @prop {*} [concealWhenReadyOpts=null] The options to pass to the _ConcealWhenReady_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ConcealWhenReadyOpts_Safe The options to pass to the _ConcealWhenReady_ vdu (required overlay).
 * @prop {*} concealWhenReadyOpts The options to pass to the _ConcealWhenReady_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutOpts The options to pass to the _AmountOut_ vdu (optional overlay).
 * @prop {*} [amountOutOpts=null] The options to pass to the _AmountOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutOpts_Safe The options to pass to the _AmountOut_ vdu (required overlay).
 * @prop {*} amountOutOpts The options to pass to the _AmountOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.SwapBuOpts The options to pass to the _SwapBu_ vdu (optional overlay).
 * @prop {*} [swapBuOpts=null] The options to pass to the _SwapBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.SwapBuOpts_Safe The options to pass to the _SwapBu_ vdu (required overlay).
 * @prop {*} swapBuOpts The options to pass to the _SwapBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FloatingLaOpts The options to pass to the _FloatingLa_ vdu (optional overlay).
 * @prop {*} [floatingLaOpts=null] The options to pass to the _FloatingLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FloatingLaOpts_Safe The options to pass to the _FloatingLa_ vdu (required overlay).
 * @prop {*} floatingLaOpts The options to pass to the _FloatingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FixedLaOpts The options to pass to the _FixedLa_ vdu (optional overlay).
 * @prop {*} [fixedLaOpts=null] The options to pass to the _FixedLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.FixedLaOpts_Safe The options to pass to the _FixedLa_ vdu (required overlay).
 * @prop {*} fixedLaOpts The options to pass to the _FixedLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AnyLaOpts The options to pass to the _AnyLa_ vdu (optional overlay).
 * @prop {*} [anyLaOpts=null] The options to pass to the _AnyLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AnyLaOpts_Safe The options to pass to the _AnyLa_ vdu (required overlay).
 * @prop {*} anyLaOpts The options to pass to the _AnyLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu (optional overlay).
 * @prop {*} [amountOutLoInOpts=null] The options to pass to the _AmountOutLoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.AmountOutLoInOpts_Safe The options to pass to the _AmountOutLoIn_ vdu (required overlay).
 * @prop {*} amountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedInOpts The options to pass to the _ShowWhenSelectedIn_ vdu (optional overlay).
 * @prop {*} [showWhenSelectedInOpts=null] The options to pass to the _ShowWhenSelectedIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedInOpts_Safe The options to pass to the _ShowWhenSelectedIn_ vdu (required overlay).
 * @prop {*} showWhenSelectedInOpts The options to pass to the _ShowWhenSelectedIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedOutOpts The options to pass to the _ShowWhenSelectedOut_ vdu (optional overlay).
 * @prop {*} [showWhenSelectedOutOpts=null] The options to pass to the _ShowWhenSelectedOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ShowWhenSelectedOutOpts_Safe The options to pass to the _ShowWhenSelectedOut_ vdu (required overlay).
 * @prop {*} showWhenSelectedOutOpts The options to pass to the _ShowWhenSelectedOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountWrOpts The options to pass to the _MinAmountWr_ vdu (optional overlay).
 * @prop {*} [minAmountWrOpts=null] The options to pass to the _MinAmountWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountWrOpts_Safe The options to pass to the _MinAmountWr_ vdu (required overlay).
 * @prop {*} minAmountWrOpts The options to pass to the _MinAmountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountLaOpts The options to pass to the _MinAmountLa_ vdu (optional overlay).
 * @prop {*} [minAmountLaOpts=null] The options to pass to the _MinAmountLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MinAmountLaOpts_Safe The options to pass to the _MinAmountLa_ vdu (required overlay).
 * @prop {*} minAmountLaOpts The options to pass to the _MinAmountLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountWrOpts The options to pass to the _MaxAmountWr_ vdu (optional overlay).
 * @prop {*} [maxAmountWrOpts=null] The options to pass to the _MaxAmountWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountWrOpts_Safe The options to pass to the _MaxAmountWr_ vdu (required overlay).
 * @prop {*} maxAmountWrOpts The options to pass to the _MaxAmountWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountLaOpts The options to pass to the _MaxAmountLa_ vdu (optional overlay).
 * @prop {*} [maxAmountLaOpts=null] The options to pass to the _MaxAmountLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.MaxAmountLaOpts_Safe The options to pass to the _MaxAmountLa_ vdu (required overlay).
 * @prop {*} maxAmountLaOpts The options to pass to the _MaxAmountLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.OutWrOpts The options to pass to the _OutWr_ vdu (optional overlay).
 * @prop {*} [outWrOpts=null] The options to pass to the _OutWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.OutWrOpts_Safe The options to pass to the _OutWr_ vdu (required overlay).
 * @prop {*} outWrOpts The options to pass to the _OutWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.InWrOpts The options to pass to the _InWr_ vdu (optional overlay).
 * @prop {*} [inWrOpts=null] The options to pass to the _InWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.InWrOpts_Safe The options to pass to the _InWr_ vdu (required overlay).
 * @prop {*} inWrOpts The options to pass to the _InWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu (optional overlay).
 * @prop {*} [cryptoSelectInOpts=null] The options to pass to the _CryptoSelectIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectInOpts_Safe The options to pass to the _CryptoSelectIn_ vdu (required overlay).
 * @prop {*} cryptoSelectInOpts The options to pass to the _CryptoSelectIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu (optional overlay).
 * @prop {*} [cryptoSelectOutOpts=null] The options to pass to the _CryptoSelectOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.CryptoSelectOutOpts_Safe The options to pass to the _CryptoSelectOut_ vdu (required overlay).
 * @prop {*} cryptoSelectOutOpts The options to pass to the _CryptoSelectOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {*} [exchangeIntentOpts=null] The options to pass to the _ExchangeIntent_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {*} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe1Opts The options to pass to the _ReadyLoadingStripe1_ vdu (optional overlay).
 * @prop {*} [readyLoadingStripe1Opts=null] The options to pass to the _ReadyLoadingStripe1_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe1Opts_Safe The options to pass to the _ReadyLoadingStripe1_ vdu (required overlay).
 * @prop {*} readyLoadingStripe1Opts The options to pass to the _ReadyLoadingStripe1_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe2Opts The options to pass to the _ReadyLoadingStripe2_ vdu (optional overlay).
 * @prop {*} [readyLoadingStripe2Opts=null] The options to pass to the _ReadyLoadingStripe2_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.ReadyLoadingStripe2Opts_Safe The options to pass to the _ReadyLoadingStripe2_ vdu (required overlay).
 * @prop {*} readyLoadingStripe2Opts The options to pass to the _ReadyLoadingStripe2_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.DealBrokerOpts The options to pass to the _DealBroker_ vdu (optional overlay).
 * @prop {*} [dealBrokerOpts=null] The options to pass to the _DealBroker_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterElementPort.WeakInputs.DealBrokerOpts_Safe The options to pass to the _DealBroker_ vdu (required overlay).
 * @prop {*} dealBrokerOpts The options to pass to the _DealBroker_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/170-IOffersFilterDesigner.xml}  b9c1f0a192b1e050b4f997f1b9cc40d3 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IOffersFilterDesigner
 */
xyz.swapee.wc.IOffersFilterDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.OffersFilterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOffersFilter />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OffersFilterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOffersFilter />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OffersFilterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOffersFilterDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `CryptoSelectIn` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `ReadyLoadingStripe1` _typeof com.webcircuits.ui.ILoadingStripeController_
   * - `ReadyLoadingStripe2` _typeof com.webcircuits.ui.ILoadingStripeController_
   * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerController_
   * - `OffersFilter` _typeof IOffersFilterController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOffersFilterDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `CryptoSelectIn` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `CryptoSelectOut` _typeof xyz.swapee.wc.ICryptoSelectController_
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `ReadyLoadingStripe1` _typeof com.webcircuits.ui.ILoadingStripeController_
   * - `ReadyLoadingStripe2` _typeof com.webcircuits.ui.ILoadingStripeController_
   * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerController_
   * - `OffersFilter` _typeof IOffersFilterController_
   * - `This` _typeof IOffersFilterController_
   * @param {!xyz.swapee.wc.IOffersFilterDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `CryptoSelectIn` _!xyz.swapee.wc.CryptoSelectMemory_
   * - `CryptoSelectOut` _!xyz.swapee.wc.CryptoSelectMemory_
   * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_
   * - `ReadyLoadingStripe1` _!com.webcircuits.ui.LoadingStripeMemory_
   * - `ReadyLoadingStripe2` _!com.webcircuits.ui.LoadingStripeMemory_
   * - `DealBroker` _!xyz.swapee.wc.DealBrokerMemory_
   * - `OffersFilter` _!OffersFilterMemory_
   * - `This` _!OffersFilterMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OffersFilterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OffersFilterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IOffersFilterDesigner.prototype.constructor = xyz.swapee.wc.IOffersFilterDesigner

/**
 * A concrete class of _IOffersFilterDesigner_ instances.
 * @constructor xyz.swapee.wc.OffersFilterDesigner
 * @implements {xyz.swapee.wc.IOffersFilterDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.OffersFilterDesigner = class extends xyz.swapee.wc.IOffersFilterDesigner { }
xyz.swapee.wc.OffersFilterDesigner.prototype.constructor = xyz.swapee.wc.OffersFilterDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof com.webcircuits.ui.ILoadingStripeController} ReadyLoadingStripe1
 * @prop {typeof com.webcircuits.ui.ILoadingStripeController} ReadyLoadingStripe2
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 * @prop {typeof xyz.swapee.wc.IOffersFilterController} OffersFilter
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectIn
 * @prop {typeof xyz.swapee.wc.ICryptoSelectController} CryptoSelectOut
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof com.webcircuits.ui.ILoadingStripeController} ReadyLoadingStripe1
 * @prop {typeof com.webcircuits.ui.ILoadingStripeController} ReadyLoadingStripe2
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 * @prop {typeof xyz.swapee.wc.IOffersFilterController} OffersFilter
 * @prop {typeof xyz.swapee.wc.IOffersFilterController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectIn
 * @prop {!xyz.swapee.wc.CryptoSelectMemory} CryptoSelectOut
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!com.webcircuits.ui.LoadingStripeMemory} ReadyLoadingStripe1
 * @prop {!com.webcircuits.ui.LoadingStripeMemory} ReadyLoadingStripe2
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker
 * @prop {!xyz.swapee.wc.OffersFilterMemory} OffersFilter
 * @prop {!xyz.swapee.wc.OffersFilterMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/200-OffersFilterLand.xml}  0715a5e76bb120d6c3cfd507634985eb */
/**
 * The surrounding of the _IOffersFilter_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.OffersFilterLand
 */
xyz.swapee.wc.OffersFilterLand = class { }
/**
 *
 */
xyz.swapee.wc.OffersFilterLand.prototype.CryptoSelectIn = /** @type {xyz.swapee.wc.ICryptoSelect} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersFilterLand.prototype.CryptoSelectOut = /** @type {xyz.swapee.wc.ICryptoSelect} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersFilterLand.prototype.ExchangeIntent = /** @type {xyz.swapee.wc.IExchangeIntent} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersFilterLand.prototype.ReadyLoadingStripe1 = /** @type {com.webcircuits.ui.ILoadingStripe} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersFilterLand.prototype.ReadyLoadingStripe2 = /** @type {com.webcircuits.ui.ILoadingStripe} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersFilterLand.prototype.DealBroker = /** @type {xyz.swapee.wc.IDealBroker} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/40-IOffersFilterDisplay.xml}  9282e5b901bd2ab22f873f8d13380dc3 */
/**
 * @typedef {Object} $xyz.swapee.wc.IOffersFilterDisplay.Initialese
 * @prop {HTMLInputElement} [AmountIn] Allows to enter desired amount.
 * @prop {!Array<!HTMLSpanElement>} [ShowWhenReadies] These elements are hidden before the intent is established.
 * @prop {!Array<!HTMLSpanElement>} [HideWhenReadies] These elements are shown before the intent is established.
 * @prop {HTMLDivElement} [AmountOut]
 * @prop {!Array<!HTMLButtonElement>} [SwapBus]
 * @prop {HTMLSpanElement} [FloatingLa]
 * @prop {HTMLSpanElement} [FixedLa]
 * @prop {HTMLSpanElement} [AnyLa]
 * @prop {HTMLSpanElement} [AmountOutLoIn]
 * @prop {HTMLSpanElement} [ShowWhenSelectedIn]
 * @prop {HTMLSpanElement} [ShowWhenSelectedOut]
 * @prop {HTMLParagraphElement} [MinAmountWr]
 * @prop {HTMLAnchorElement} [MinAmountLa]
 * @prop {HTMLParagraphElement} [MaxAmountWr]
 * @prop {HTMLAnchorElement} [MaxAmountLa]
 * @prop {HTMLSpanElement} [OutWr]
 * @prop {HTMLSpanElement} [InWr]
 * @prop {HTMLElement} [CryptoSelectIn] The via for the _CryptoSelectIn_ peer.
 * @prop {HTMLElement} [CryptoSelectOut] The via for the _CryptoSelectOut_ peer.
 * @prop {HTMLElement} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 * @prop {HTMLElement} [ReadyLoadingStripe1] The via for the _ReadyLoadingStripe1_ peer.
 * @prop {HTMLElement} [ReadyLoadingStripe2] The via for the _ReadyLoadingStripe2_ peer.
 * @prop {HTMLElement} [DealBroker] The via for the _DealBroker_ peer.
 */
/** @typedef {$xyz.swapee.wc.IOffersFilterDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOffersFilterDisplay.Settings>} xyz.swapee.wc.IOffersFilterDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterDisplay)} xyz.swapee.wc.AbstractOffersFilterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterDisplay} xyz.swapee.wc.OffersFilterDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterDisplay
 */
xyz.swapee.wc.AbstractOffersFilterDisplay = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterDisplay.constructor&xyz.swapee.wc.OffersFilterDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterDisplay.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterDisplay|typeof xyz.swapee.wc.OffersFilterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterDisplay}
 */
xyz.swapee.wc.AbstractOffersFilterDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterDisplay}
 */
xyz.swapee.wc.AbstractOffersFilterDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterDisplay|typeof xyz.swapee.wc.OffersFilterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterDisplay}
 */
xyz.swapee.wc.AbstractOffersFilterDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterDisplay|typeof xyz.swapee.wc.OffersFilterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterDisplay}
 */
xyz.swapee.wc.AbstractOffersFilterDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterDisplay.Initialese[]) => xyz.swapee.wc.IOffersFilterDisplay} xyz.swapee.wc.OffersFilterDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.OffersFilterMemory, !HTMLDivElement, !xyz.swapee.wc.IOffersFilterDisplay.Settings, xyz.swapee.wc.IOffersFilterDisplay.Queries, null>)} xyz.swapee.wc.IOffersFilterDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IOffersFilter_.
 * @interface xyz.swapee.wc.IOffersFilterDisplay
 */
xyz.swapee.wc.IOffersFilterDisplay = class extends /** @type {xyz.swapee.wc.IOffersFilterDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersFilterDisplay.paint} */
xyz.swapee.wc.IOffersFilterDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterDisplay.Initialese>)} xyz.swapee.wc.OffersFilterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterDisplay} xyz.swapee.wc.IOffersFilterDisplay.typeof */
/**
 * A concrete class of _IOffersFilterDisplay_ instances.
 * @constructor xyz.swapee.wc.OffersFilterDisplay
 * @implements {xyz.swapee.wc.IOffersFilterDisplay} Display for presenting information from the _IOffersFilter_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterDisplay.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterDisplay = class extends /** @type {xyz.swapee.wc.OffersFilterDisplay.constructor&xyz.swapee.wc.IOffersFilterDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterDisplay}
 */
xyz.swapee.wc.OffersFilterDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterDisplay.
 * @interface xyz.swapee.wc.IOffersFilterDisplayFields
 */
xyz.swapee.wc.IOffersFilterDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IOffersFilterDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IOffersFilterDisplay.Queries} */ (void 0)
/**
 * Allows to enter desired amount. Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.AmountIn = /** @type {HTMLInputElement} */ (void 0)
/**
 * These elements are hidden before the intent is established. Default `[]`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.ShowWhenReadies = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * These elements are shown before the intent is established. Default `[]`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.HideWhenReadies = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.AmountOut = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.SwapBus = /** @type {!Array<!HTMLButtonElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.FloatingLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.FixedLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.AnyLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.AmountOutLoIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.ShowWhenSelectedIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.ShowWhenSelectedOut = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.MinAmountWr = /** @type {HTMLParagraphElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.MinAmountLa = /** @type {HTMLAnchorElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.MaxAmountWr = /** @type {HTMLParagraphElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.MaxAmountLa = /** @type {HTMLAnchorElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.OutWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.InWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * The via for the _CryptoSelectIn_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.CryptoSelectIn = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _CryptoSelectOut_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.CryptoSelectOut = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.ExchangeIntent = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _ReadyLoadingStripe1_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.ReadyLoadingStripe1 = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _ReadyLoadingStripe2_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.ReadyLoadingStripe2 = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _DealBroker_ peer. Default `null`.
 */
xyz.swapee.wc.IOffersFilterDisplayFields.prototype.DealBroker = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterDisplay} */
xyz.swapee.wc.RecordIOffersFilterDisplay

/** @typedef {xyz.swapee.wc.IOffersFilterDisplay} xyz.swapee.wc.BoundIOffersFilterDisplay */

/** @typedef {xyz.swapee.wc.OffersFilterDisplay} xyz.swapee.wc.BoundOffersFilterDisplay */

/** @typedef {xyz.swapee.wc.IOffersFilterDisplay.Queries} xyz.swapee.wc.IOffersFilterDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IOffersFilterDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [exchangeIntentSel=""] The query to discover the _ExchangeIntent_ VDU. Default empty string.
 * @prop {string} [dealBrokerSel=""] The query to discover the _DealBroker_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IOffersFilterDisplay_ interface.
 * @interface xyz.swapee.wc.IOffersFilterDisplayCaster
 */
xyz.swapee.wc.IOffersFilterDisplayCaster = class { }
/**
 * Cast the _IOffersFilterDisplay_ instance into the _BoundIOffersFilterDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterDisplay}
 */
xyz.swapee.wc.IOffersFilterDisplayCaster.prototype.asIOffersFilterDisplay
/**
 * Cast the _IOffersFilterDisplay_ instance into the _BoundIOffersFilterScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterScreen}
 */
xyz.swapee.wc.IOffersFilterDisplayCaster.prototype.asIOffersFilterScreen
/**
 * Access the _OffersFilterDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterDisplay}
 */
xyz.swapee.wc.IOffersFilterDisplayCaster.prototype.superOffersFilterDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OffersFilterMemory, land: null) => void} xyz.swapee.wc.IOffersFilterDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterDisplay.__paint<!xyz.swapee.wc.IOffersFilterDisplay>} xyz.swapee.wc.IOffersFilterDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OffersFilterMemory} memory The display data.
 * - `dealBrokerCid` _number_ Default `0`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersFilterDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/40-IOffersFilterDisplayBack.xml}  85054c3b209390fc4f7936b0cec09836 */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IOffersFilterDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [AmountIn] Allows to enter desired amount.
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [ShowWhenReadies] These elements are hidden before the intent is established.
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [HideWhenReadies] These elements are shown before the intent is established.
 * @prop {!com.webcircuits.IHtmlTwin} [AmountOut]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [SwapBus]
 * @prop {!com.webcircuits.IHtmlTwin} [FloatingLa]
 * @prop {!com.webcircuits.IHtmlTwin} [FixedLa]
 * @prop {!com.webcircuits.IHtmlTwin} [AnyLa]
 * @prop {!com.webcircuits.IHtmlTwin} [AmountOutLoIn]
 * @prop {!com.webcircuits.IHtmlTwin} [ShowWhenSelectedIn]
 * @prop {!com.webcircuits.IHtmlTwin} [ShowWhenSelectedOut]
 * @prop {!com.webcircuits.IHtmlTwin} [MinAmountWr]
 * @prop {!com.webcircuits.IHtmlTwin} [MinAmountLa]
 * @prop {!com.webcircuits.IHtmlTwin} [MaxAmountWr]
 * @prop {!com.webcircuits.IHtmlTwin} [MaxAmountLa]
 * @prop {!com.webcircuits.IHtmlTwin} [OutWr]
 * @prop {!com.webcircuits.IHtmlTwin} [InWr]
 * @prop {!com.webcircuits.IHtmlTwin} [CryptoSelectIn] The via for the _CryptoSelectIn_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [CryptoSelectOut] The via for the _CryptoSelectOut_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [ReadyLoadingStripe1] The via for the _ReadyLoadingStripe1_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [ReadyLoadingStripe2] The via for the _ReadyLoadingStripe2_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [DealBroker] The via for the _DealBroker_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IOffersFilterDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OffersFilterClasses>} xyz.swapee.wc.back.IOffersFilterDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OffersFilterDisplay)} xyz.swapee.wc.back.AbstractOffersFilterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersFilterDisplay} xyz.swapee.wc.back.OffersFilterDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersFilterDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersFilterDisplay
 */
xyz.swapee.wc.back.AbstractOffersFilterDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractOffersFilterDisplay.constructor&xyz.swapee.wc.back.OffersFilterDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersFilterDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractOffersFilterDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersFilterDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterDisplay|typeof xyz.swapee.wc.back.OffersFilterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersFilterDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersFilterDisplay}
 */
xyz.swapee.wc.back.AbstractOffersFilterDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterDisplay}
 */
xyz.swapee.wc.back.AbstractOffersFilterDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterDisplay|typeof xyz.swapee.wc.back.OffersFilterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterDisplay}
 */
xyz.swapee.wc.back.AbstractOffersFilterDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterDisplay|typeof xyz.swapee.wc.back.OffersFilterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterDisplay}
 */
xyz.swapee.wc.back.AbstractOffersFilterDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersFilterDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IOffersFilterDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.OffersFilterClasses, !xyz.swapee.wc.OffersFilterLand>)} xyz.swapee.wc.back.IOffersFilterDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IOffersFilterDisplay
 */
xyz.swapee.wc.back.IOffersFilterDisplay = class extends /** @type {xyz.swapee.wc.back.IOffersFilterDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IOffersFilterDisplay.paint} */
xyz.swapee.wc.back.IOffersFilterDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersFilterDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterDisplay.Initialese>)} xyz.swapee.wc.back.OffersFilterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOffersFilterDisplay} xyz.swapee.wc.back.IOffersFilterDisplay.typeof */
/**
 * A concrete class of _IOffersFilterDisplay_ instances.
 * @constructor xyz.swapee.wc.back.OffersFilterDisplay
 * @implements {xyz.swapee.wc.back.IOffersFilterDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersFilterDisplay = class extends /** @type {xyz.swapee.wc.back.OffersFilterDisplay.constructor&xyz.swapee.wc.back.IOffersFilterDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.OffersFilterDisplay.prototype.constructor = xyz.swapee.wc.back.OffersFilterDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersFilterDisplay}
 */
xyz.swapee.wc.back.OffersFilterDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterDisplay.
 * @interface xyz.swapee.wc.back.IOffersFilterDisplayFields
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields = class { }
/**
 * Allows to enter desired amount.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.AmountIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * These elements are hidden before the intent is established. Default `[]`.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.ShowWhenReadies = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 * These elements are shown before the intent is established. Default `[]`.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.HideWhenReadies = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.AmountOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.SwapBus = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.FloatingLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.FixedLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.AnyLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.AmountOutLoIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.ShowWhenSelectedIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.ShowWhenSelectedOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.MinAmountWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.MinAmountLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.MaxAmountWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.MaxAmountLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.OutWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.InWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _CryptoSelectIn_ peer.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.CryptoSelectIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _CryptoSelectOut_ peer.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.CryptoSelectOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.ExchangeIntent = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ReadyLoadingStripe1_ peer.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.ReadyLoadingStripe1 = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ReadyLoadingStripe2_ peer.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.ReadyLoadingStripe2 = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _DealBroker_ peer.
 */
xyz.swapee.wc.back.IOffersFilterDisplayFields.prototype.DealBroker = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IOffersFilterDisplay} */
xyz.swapee.wc.back.RecordIOffersFilterDisplay

/** @typedef {xyz.swapee.wc.back.IOffersFilterDisplay} xyz.swapee.wc.back.BoundIOffersFilterDisplay */

/** @typedef {xyz.swapee.wc.back.OffersFilterDisplay} xyz.swapee.wc.back.BoundOffersFilterDisplay */

/**
 * Contains getters to cast the _IOffersFilterDisplay_ interface.
 * @interface xyz.swapee.wc.back.IOffersFilterDisplayCaster
 */
xyz.swapee.wc.back.IOffersFilterDisplayCaster = class { }
/**
 * Cast the _IOffersFilterDisplay_ instance into the _BoundIOffersFilterDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersFilterDisplay}
 */
xyz.swapee.wc.back.IOffersFilterDisplayCaster.prototype.asIOffersFilterDisplay
/**
 * Access the _OffersFilterDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersFilterDisplay}
 */
xyz.swapee.wc.back.IOffersFilterDisplayCaster.prototype.superOffersFilterDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.OffersFilterMemory, land?: !xyz.swapee.wc.OffersFilterLand) => void} xyz.swapee.wc.back.IOffersFilterDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IOffersFilterDisplay.__paint<!xyz.swapee.wc.back.IOffersFilterDisplay>} xyz.swapee.wc.back.IOffersFilterDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IOffersFilterDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OffersFilterMemory} [memory] The display data.
 * - `dealBrokerCid` _number_ Default `0`.
 * @param {!xyz.swapee.wc.OffersFilterLand} [land] The land data.
 * - `CryptoSelectIn` _xyz.swapee.wc.ICryptoSelect_
 * - `CryptoSelectOut` _xyz.swapee.wc.ICryptoSelect_
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntent_
 * - `ReadyLoadingStripe1` _com.webcircuits.ui.ILoadingStripe_
 * - `ReadyLoadingStripe2` _com.webcircuits.ui.ILoadingStripe_
 * - `DealBroker` _xyz.swapee.wc.IDealBroker_
 * @return {void}
 */
xyz.swapee.wc.back.IOffersFilterDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IOffersFilterDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/41-OffersFilterClasses.xml}  75f40df32f0233cdbc9283444a7c69d3 */
/**
 * The classes of the _IOffersFilterDisplay_.
 * @record xyz.swapee.wc.OffersFilterClasses
 */
xyz.swapee.wc.OffersFilterClasses = class { }
/**
 *
 */
xyz.swapee.wc.OffersFilterClasses.prototype.SelectedRateType = /** @type {string|undefined} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OffersFilterClasses.prototype.Ready = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.OffersFilterClasses.prototype.props = /** @type {xyz.swapee.wc.OffersFilterClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/50-IOffersFilterController.xml}  4fd2af72376d379d8e898cac4617ea3f */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOffersFilterController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOffersFilterController.Inputs, !xyz.swapee.wc.IOffersFilterOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOffersFilterOuterCore.WeakModel>} xyz.swapee.wc.IOffersFilterController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterController)} xyz.swapee.wc.AbstractOffersFilterController.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterController} xyz.swapee.wc.OffersFilterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterController` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterController
 */
xyz.swapee.wc.AbstractOffersFilterController = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterController.constructor&xyz.swapee.wc.OffersFilterController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterController.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterController.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersFilterControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterController}
 */
xyz.swapee.wc.AbstractOffersFilterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterController}
 */
xyz.swapee.wc.AbstractOffersFilterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersFilterControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterController}
 */
xyz.swapee.wc.AbstractOffersFilterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)|!xyz.swapee.wc.IOffersFilterControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterController}
 */
xyz.swapee.wc.AbstractOffersFilterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterController.Initialese[]) => xyz.swapee.wc.IOffersFilterController} xyz.swapee.wc.OffersFilterControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOffersFilterControllerHyperslice
 */
xyz.swapee.wc.IOffersFilterControllerHyperslice = class {
  constructor() {
    this.swapDirection=/** @type {!xyz.swapee.wc.IOffersFilterController._swapDirection|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersFilterController._swapDirection>} */ (void 0)
  }
}
xyz.swapee.wc.IOffersFilterControllerHyperslice.prototype.constructor = xyz.swapee.wc.IOffersFilterControllerHyperslice

/**
 * A concrete class of _IOffersFilterControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.OffersFilterControllerHyperslice
 * @implements {xyz.swapee.wc.IOffersFilterControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.OffersFilterControllerHyperslice = class extends xyz.swapee.wc.IOffersFilterControllerHyperslice { }
xyz.swapee.wc.OffersFilterControllerHyperslice.prototype.constructor = xyz.swapee.wc.OffersFilterControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IOffersFilterControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IOffersFilterControllerBindingHyperslice = class {
  constructor() {
    this.swapDirection=/** @type {!xyz.swapee.wc.IOffersFilterController.__swapDirection<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IOffersFilterController.__swapDirection<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IOffersFilterControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.IOffersFilterControllerBindingHyperslice

/**
 * A concrete class of _IOffersFilterControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.OffersFilterControllerBindingHyperslice
 * @implements {xyz.swapee.wc.IOffersFilterControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IOffersFilterControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.OffersFilterControllerBindingHyperslice = class extends xyz.swapee.wc.IOffersFilterControllerBindingHyperslice { }
xyz.swapee.wc.OffersFilterControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.OffersFilterControllerBindingHyperslice

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IOffersFilterController.Inputs, !xyz.swapee.wc.IOffersFilterOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IOffersFilterOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOffersFilterController.Inputs, !xyz.swapee.wc.IOffersFilterController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOffersFilterController.Inputs, !xyz.swapee.wc.OffersFilterMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOffersFilterController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOffersFilterController.Inputs>)} xyz.swapee.wc.IOffersFilterController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IOffersFilterController
 */
xyz.swapee.wc.IOffersFilterController = class extends /** @type {xyz.swapee.wc.IOffersFilterController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOffersFilterController.resetPort} */
xyz.swapee.wc.IOffersFilterController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IOffersFilterController.swapDirection} */
xyz.swapee.wc.IOffersFilterController.prototype.swapDirection = function() {}
/** @type {xyz.swapee.wc.IOffersFilterController.onSwapDirection} */
xyz.swapee.wc.IOffersFilterController.prototype.onSwapDirection = function() {}
/** @type {xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_AmountIn_input} */
xyz.swapee.wc.IOffersFilterController.prototype.$ExchangeIntent_setAmountFrom_on_AmountIn_input = function() {}
/** @type {xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click} */
xyz.swapee.wc.IOffersFilterController.prototype.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click = function() {}
/** @type {xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click} */
xyz.swapee.wc.IOffersFilterController.prototype.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterController&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterController.Initialese>)} xyz.swapee.wc.OffersFilterController.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController} xyz.swapee.wc.IOffersFilterController.typeof */
/**
 * A concrete class of _IOffersFilterController_ instances.
 * @constructor xyz.swapee.wc.OffersFilterController
 * @implements {xyz.swapee.wc.IOffersFilterController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterController.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterController = class extends /** @type {xyz.swapee.wc.OffersFilterController.constructor&xyz.swapee.wc.IOffersFilterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterController}
 */
xyz.swapee.wc.OffersFilterController.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterController.
 * @interface xyz.swapee.wc.IOffersFilterControllerFields
 */
xyz.swapee.wc.IOffersFilterControllerFields = class { }
/**
 * The inputs to the _IOffersFilter_'s controller.
 */
xyz.swapee.wc.IOffersFilterControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOffersFilterController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IOffersFilterControllerFields.prototype.props = /** @type {xyz.swapee.wc.IOffersFilterController} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterController} */
xyz.swapee.wc.RecordIOffersFilterController

/** @typedef {xyz.swapee.wc.IOffersFilterController} xyz.swapee.wc.BoundIOffersFilterController */

/** @typedef {xyz.swapee.wc.OffersFilterController} xyz.swapee.wc.BoundOffersFilterController */

/** @typedef {xyz.swapee.wc.IOffersFilterPort.Inputs} xyz.swapee.wc.IOffersFilterController.Inputs The inputs to the _IOffersFilter_'s controller. */

/** @typedef {xyz.swapee.wc.IOffersFilterPort.WeakInputs} xyz.swapee.wc.IOffersFilterController.WeakInputs The inputs to the _IOffersFilter_'s controller. */

/**
 * Contains getters to cast the _IOffersFilterController_ interface.
 * @interface xyz.swapee.wc.IOffersFilterControllerCaster
 */
xyz.swapee.wc.IOffersFilterControllerCaster = class { }
/**
 * Cast the _IOffersFilterController_ instance into the _BoundIOffersFilterController_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterController}
 */
xyz.swapee.wc.IOffersFilterControllerCaster.prototype.asIOffersFilterController
/**
 * Cast the _IOffersFilterController_ instance into the _BoundIOffersFilterProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterProcessor}
 */
xyz.swapee.wc.IOffersFilterControllerCaster.prototype.asIOffersFilterProcessor
/**
 * Access the _OffersFilterController_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterController}
 */
xyz.swapee.wc.IOffersFilterControllerCaster.prototype.superOffersFilterController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterController.__resetPort<!xyz.swapee.wc.IOffersFilterController>} xyz.swapee.wc.IOffersFilterController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterController.__swapDirection
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterController.__swapDirection<!xyz.swapee.wc.IOffersFilterController>} xyz.swapee.wc.IOffersFilterController._swapDirection */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController.swapDirection} */
/**
 *  `🔗 $combine`
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterController.swapDirection = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterController.__onSwapDirection
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterController.__onSwapDirection<!xyz.swapee.wc.IOffersFilterController>} xyz.swapee.wc.IOffersFilterController._onSwapDirection */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController.onSwapDirection} */
/**
 * Executed when `swapDirection` is called. Can be used in relays.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterController.onSwapDirection = function() {}

/**
 * @typedef {(this: THIS, value: *) => void} xyz.swapee.wc.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_AmountIn_input
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_AmountIn_input<!xyz.swapee.wc.IOffersFilterController>} xyz.swapee.wc.IOffersFilterController._$ExchangeIntent_setAmountFrom_on_AmountIn_input */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_AmountIn_input} */
/**
 * Invoked in response to the `input` event on _AmountIn_.
 * Triggers `setAmountFrom` on _ExchangeIntent_.
 * @param {*} value
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_AmountIn_input = function(value) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MinAmountLa_click
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MinAmountLa_click<!xyz.swapee.wc.IOffersFilterController>} xyz.swapee.wc.IOffersFilterController._$ExchangeIntent_setAmountFrom_on_MinAmountLa_click */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click} */
/**
 * Invoked in response to the `click` event on _MinAmountLa_.
 * Triggers `setAmountFrom` on _ExchangeIntent_.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click<!xyz.swapee.wc.IOffersFilterController>} xyz.swapee.wc.IOffersFilterController._$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click} */
/**
 * Invoked in response to the `click` event on _MaxAmountLa_.
 * Triggers `setAmountFrom` on _ExchangeIntent_.
 * @return {void}
 */
xyz.swapee.wc.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOffersFilterController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/51-IOffersFilterControllerFront.xml}  b18d4357dedf4c3188dc833d6b2be6b6 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IOffersFilterController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OffersFilterController)} xyz.swapee.wc.front.AbstractOffersFilterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OffersFilterController} xyz.swapee.wc.front.OffersFilterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOffersFilterController` interface.
 * @constructor xyz.swapee.wc.front.AbstractOffersFilterController
 */
xyz.swapee.wc.front.AbstractOffersFilterController = class extends /** @type {xyz.swapee.wc.front.AbstractOffersFilterController.constructor&xyz.swapee.wc.front.OffersFilterController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOffersFilterController.prototype.constructor = xyz.swapee.wc.front.AbstractOffersFilterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOffersFilterController.class = /** @type {typeof xyz.swapee.wc.front.AbstractOffersFilterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterController|typeof xyz.swapee.wc.front.OffersFilterController)|(!xyz.swapee.wc.front.IOffersFilterControllerAT|typeof xyz.swapee.wc.front.OffersFilterControllerAT)|!xyz.swapee.wc.front.IOffersFilterControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersFilterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOffersFilterController}
 */
xyz.swapee.wc.front.AbstractOffersFilterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterController}
 */
xyz.swapee.wc.front.AbstractOffersFilterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterController|typeof xyz.swapee.wc.front.OffersFilterController)|(!xyz.swapee.wc.front.IOffersFilterControllerAT|typeof xyz.swapee.wc.front.OffersFilterControllerAT)|!xyz.swapee.wc.front.IOffersFilterControllerHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterController}
 */
xyz.swapee.wc.front.AbstractOffersFilterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterController|typeof xyz.swapee.wc.front.OffersFilterController)|(!xyz.swapee.wc.front.IOffersFilterControllerAT|typeof xyz.swapee.wc.front.OffersFilterControllerAT)|!xyz.swapee.wc.front.IOffersFilterControllerHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterController}
 */
xyz.swapee.wc.front.AbstractOffersFilterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOffersFilterController.Initialese[]) => xyz.swapee.wc.front.IOffersFilterController} xyz.swapee.wc.front.OffersFilterControllerConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.IOffersFilterControllerHyperslice
 */
xyz.swapee.wc.front.IOffersFilterControllerHyperslice = class {
  constructor() {
    this.swapDirection=/** @type {!xyz.swapee.wc.front.IOffersFilterController._swapDirection|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersFilterController._swapDirection>} */ (void 0)
  }
}
xyz.swapee.wc.front.IOffersFilterControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.IOffersFilterControllerHyperslice

/**
 * A concrete class of _IOffersFilterControllerHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.OffersFilterControllerHyperslice
 * @implements {xyz.swapee.wc.front.IOffersFilterControllerHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.front.OffersFilterControllerHyperslice = class extends xyz.swapee.wc.front.IOffersFilterControllerHyperslice { }
xyz.swapee.wc.front.OffersFilterControllerHyperslice.prototype.constructor = xyz.swapee.wc.front.OffersFilterControllerHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice = class {
  constructor() {
    this.swapDirection=/** @type {!xyz.swapee.wc.front.IOffersFilterController.__swapDirection<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.front.IOffersFilterController.__swapDirection<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice

/**
 * A concrete class of _IOffersFilterControllerBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.front.OffersFilterControllerBindingHyperslice
 * @implements {xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice<THIS>}
 */
xyz.swapee.wc.front.OffersFilterControllerBindingHyperslice = class extends xyz.swapee.wc.front.IOffersFilterControllerBindingHyperslice { }
xyz.swapee.wc.front.OffersFilterControllerBindingHyperslice.prototype.constructor = xyz.swapee.wc.front.OffersFilterControllerBindingHyperslice

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOffersFilterControllerCaster&xyz.swapee.wc.front.IOffersFilterControllerAT)} xyz.swapee.wc.front.IOffersFilterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterControllerAT} xyz.swapee.wc.front.IOffersFilterControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IOffersFilterController
 */
xyz.swapee.wc.front.IOffersFilterController = class extends /** @type {xyz.swapee.wc.front.IOffersFilterController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IOffersFilterControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersFilterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOffersFilterController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IOffersFilterController.swapDirection} */
xyz.swapee.wc.front.IOffersFilterController.prototype.swapDirection = function() {}
/** @type {xyz.swapee.wc.front.IOffersFilterController.onSwapDirection} */
xyz.swapee.wc.front.IOffersFilterController.prototype.onSwapDirection = function() {}
/** @type {xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_AmountIn_input} */
xyz.swapee.wc.front.IOffersFilterController.prototype.$ExchangeIntent_setAmountFrom_on_AmountIn_input = function() {}
/** @type {xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click} */
xyz.swapee.wc.front.IOffersFilterController.prototype.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click = function() {}
/** @type {xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click} */
xyz.swapee.wc.front.IOffersFilterController.prototype.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IOffersFilterController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersFilterController.Initialese>)} xyz.swapee.wc.front.OffersFilterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterController} xyz.swapee.wc.front.IOffersFilterController.typeof */
/**
 * A concrete class of _IOffersFilterController_ instances.
 * @constructor xyz.swapee.wc.front.OffersFilterController
 * @implements {xyz.swapee.wc.front.IOffersFilterController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersFilterController.Initialese>} ‎
 */
xyz.swapee.wc.front.OffersFilterController = class extends /** @type {xyz.swapee.wc.front.OffersFilterController.constructor&xyz.swapee.wc.front.IOffersFilterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOffersFilterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersFilterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OffersFilterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersFilterController}
 */
xyz.swapee.wc.front.OffersFilterController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOffersFilterController} */
xyz.swapee.wc.front.RecordIOffersFilterController

/** @typedef {xyz.swapee.wc.front.IOffersFilterController} xyz.swapee.wc.front.BoundIOffersFilterController */

/** @typedef {xyz.swapee.wc.front.OffersFilterController} xyz.swapee.wc.front.BoundOffersFilterController */

/**
 * Contains getters to cast the _IOffersFilterController_ interface.
 * @interface xyz.swapee.wc.front.IOffersFilterControllerCaster
 */
xyz.swapee.wc.front.IOffersFilterControllerCaster = class { }
/**
 * Cast the _IOffersFilterController_ instance into the _BoundIOffersFilterController_ type.
 * @type {!xyz.swapee.wc.front.BoundIOffersFilterController}
 */
xyz.swapee.wc.front.IOffersFilterControllerCaster.prototype.asIOffersFilterController
/**
 * Access the _OffersFilterController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOffersFilterController}
 */
xyz.swapee.wc.front.IOffersFilterControllerCaster.prototype.superOffersFilterController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IOffersFilterController.__swapDirection
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersFilterController.__swapDirection<!xyz.swapee.wc.front.IOffersFilterController>} xyz.swapee.wc.front.IOffersFilterController._swapDirection */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterController.swapDirection} */
/**
 *  `🔗 $combine` `🔗 $combine`
 * @return {void}
 */
xyz.swapee.wc.front.IOffersFilterController.swapDirection = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IOffersFilterController.__onSwapDirection
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersFilterController.__onSwapDirection<!xyz.swapee.wc.front.IOffersFilterController>} xyz.swapee.wc.front.IOffersFilterController._onSwapDirection */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterController.onSwapDirection} */
/**
 * Executed when `swapDirection` is called. Can be used in relays.
 * @return {void}
 */
xyz.swapee.wc.front.IOffersFilterController.onSwapDirection = function() {}

/**
 * @typedef {(this: THIS, value: *) => void} xyz.swapee.wc.front.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_AmountIn_input
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_AmountIn_input<!xyz.swapee.wc.front.IOffersFilterController>} xyz.swapee.wc.front.IOffersFilterController._$ExchangeIntent_setAmountFrom_on_AmountIn_input */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_AmountIn_input} */
/**
 * Invoked in response to the `input` event on _AmountIn_.
 * Triggers `setAmountFrom` on _ExchangeIntent_.
 * @param {*} value
 * @return {void}
 */
xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_AmountIn_input = function(value) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MinAmountLa_click
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MinAmountLa_click<!xyz.swapee.wc.front.IOffersFilterController>} xyz.swapee.wc.front.IOffersFilterController._$ExchangeIntent_setAmountFrom_on_MinAmountLa_click */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click} */
/**
 * Invoked in response to the `click` event on _MinAmountLa_.
 * Triggers `setAmountFrom` on _ExchangeIntent_.
 * @return {void}
 */
xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MinAmountLa_click = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IOffersFilterController.__$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click<!xyz.swapee.wc.front.IOffersFilterController>} xyz.swapee.wc.front.IOffersFilterController._$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click} */
/**
 * Invoked in response to the `click` event on _MaxAmountLa_.
 * Triggers `setAmountFrom` on _ExchangeIntent_.
 * @return {void}
 */
xyz.swapee.wc.front.IOffersFilterController.$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IOffersFilterController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/52-IOffersFilterControllerBack.xml}  09e7b3975dc1d3b56a26418843136a5f */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOffersFilterController.Inputs>&xyz.swapee.wc.IOffersFilterController.Initialese} xyz.swapee.wc.back.IOffersFilterController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OffersFilterController)} xyz.swapee.wc.back.AbstractOffersFilterController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersFilterController} xyz.swapee.wc.back.OffersFilterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersFilterController` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersFilterController
 */
xyz.swapee.wc.back.AbstractOffersFilterController = class extends /** @type {xyz.swapee.wc.back.AbstractOffersFilterController.constructor&xyz.swapee.wc.back.OffersFilterController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersFilterController.prototype.constructor = xyz.swapee.wc.back.AbstractOffersFilterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersFilterController.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterController|typeof xyz.swapee.wc.back.OffersFilterController)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersFilterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersFilterController}
 */
xyz.swapee.wc.back.AbstractOffersFilterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterController}
 */
xyz.swapee.wc.back.AbstractOffersFilterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterController|typeof xyz.swapee.wc.back.OffersFilterController)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterController}
 */
xyz.swapee.wc.back.AbstractOffersFilterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterController|typeof xyz.swapee.wc.back.OffersFilterController)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterController}
 */
xyz.swapee.wc.back.AbstractOffersFilterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersFilterController.Initialese[]) => xyz.swapee.wc.back.IOffersFilterController} xyz.swapee.wc.back.OffersFilterControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersFilterControllerCaster&xyz.swapee.wc.IOffersFilterController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IOffersFilterController.Inputs>)} xyz.swapee.wc.back.IOffersFilterController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IOffersFilterController
 */
xyz.swapee.wc.back.IOffersFilterController = class extends /** @type {xyz.swapee.wc.back.IOffersFilterController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOffersFilterController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersFilterController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersFilterController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterController.Initialese>)} xyz.swapee.wc.back.OffersFilterController.constructor */
/**
 * A concrete class of _IOffersFilterController_ instances.
 * @constructor xyz.swapee.wc.back.OffersFilterController
 * @implements {xyz.swapee.wc.back.IOffersFilterController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterController.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersFilterController = class extends /** @type {xyz.swapee.wc.back.OffersFilterController.constructor&xyz.swapee.wc.back.IOffersFilterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersFilterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersFilterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersFilterController}
 */
xyz.swapee.wc.back.OffersFilterController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersFilterController} */
xyz.swapee.wc.back.RecordIOffersFilterController

/** @typedef {xyz.swapee.wc.back.IOffersFilterController} xyz.swapee.wc.back.BoundIOffersFilterController */

/** @typedef {xyz.swapee.wc.back.OffersFilterController} xyz.swapee.wc.back.BoundOffersFilterController */

/**
 * Contains getters to cast the _IOffersFilterController_ interface.
 * @interface xyz.swapee.wc.back.IOffersFilterControllerCaster
 */
xyz.swapee.wc.back.IOffersFilterControllerCaster = class { }
/**
 * Cast the _IOffersFilterController_ instance into the _BoundIOffersFilterController_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersFilterController}
 */
xyz.swapee.wc.back.IOffersFilterControllerCaster.prototype.asIOffersFilterController
/**
 * Access the _OffersFilterController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersFilterController}
 */
xyz.swapee.wc.back.IOffersFilterControllerCaster.prototype.superOffersFilterController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/53-IOffersFilterControllerAR.xml}  b13f8c00876383f4d097c3c65352f31f */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOffersFilterController.Initialese} xyz.swapee.wc.back.IOffersFilterControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OffersFilterControllerAR)} xyz.swapee.wc.back.AbstractOffersFilterControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersFilterControllerAR} xyz.swapee.wc.back.OffersFilterControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersFilterControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersFilterControllerAR
 */
xyz.swapee.wc.back.AbstractOffersFilterControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractOffersFilterControllerAR.constructor&xyz.swapee.wc.back.OffersFilterControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersFilterControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractOffersFilterControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersFilterControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterControllerAR|typeof xyz.swapee.wc.back.OffersFilterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersFilterControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersFilterControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersFilterControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersFilterControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterControllerAR|typeof xyz.swapee.wc.back.OffersFilterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersFilterControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterControllerAR|typeof xyz.swapee.wc.back.OffersFilterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersFilterController|typeof xyz.swapee.wc.OffersFilterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterControllerAR}
 */
xyz.swapee.wc.back.AbstractOffersFilterControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersFilterControllerAR.Initialese[]) => xyz.swapee.wc.back.IOffersFilterControllerAR} xyz.swapee.wc.back.OffersFilterControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersFilterControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOffersFilterController)} xyz.swapee.wc.back.IOffersFilterControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOffersFilterControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IOffersFilterControllerAR
 */
xyz.swapee.wc.back.IOffersFilterControllerAR = class extends /** @type {xyz.swapee.wc.back.IOffersFilterControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOffersFilterController.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersFilterControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersFilterControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterControllerAR.Initialese>)} xyz.swapee.wc.back.OffersFilterControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOffersFilterControllerAR} xyz.swapee.wc.back.IOffersFilterControllerAR.typeof */
/**
 * A concrete class of _IOffersFilterControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.OffersFilterControllerAR
 * @implements {xyz.swapee.wc.back.IOffersFilterControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IOffersFilterControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersFilterControllerAR = class extends /** @type {xyz.swapee.wc.back.OffersFilterControllerAR.constructor&xyz.swapee.wc.back.IOffersFilterControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersFilterControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersFilterControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersFilterControllerAR}
 */
xyz.swapee.wc.back.OffersFilterControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersFilterControllerAR} */
xyz.swapee.wc.back.RecordIOffersFilterControllerAR

/** @typedef {xyz.swapee.wc.back.IOffersFilterControllerAR} xyz.swapee.wc.back.BoundIOffersFilterControllerAR */

/** @typedef {xyz.swapee.wc.back.OffersFilterControllerAR} xyz.swapee.wc.back.BoundOffersFilterControllerAR */

/**
 * Contains getters to cast the _IOffersFilterControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IOffersFilterControllerARCaster
 */
xyz.swapee.wc.back.IOffersFilterControllerARCaster = class { }
/**
 * Cast the _IOffersFilterControllerAR_ instance into the _BoundIOffersFilterControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersFilterControllerAR}
 */
xyz.swapee.wc.back.IOffersFilterControllerARCaster.prototype.asIOffersFilterControllerAR
/**
 * Access the _OffersFilterControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersFilterControllerAR}
 */
xyz.swapee.wc.back.IOffersFilterControllerARCaster.prototype.superOffersFilterControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/54-IOffersFilterControllerAT.xml}  146aaca57e5863c56424d32b876ee8e2 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IOffersFilterControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OffersFilterControllerAT)} xyz.swapee.wc.front.AbstractOffersFilterControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OffersFilterControllerAT} xyz.swapee.wc.front.OffersFilterControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOffersFilterControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractOffersFilterControllerAT
 */
xyz.swapee.wc.front.AbstractOffersFilterControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractOffersFilterControllerAT.constructor&xyz.swapee.wc.front.OffersFilterControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOffersFilterControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractOffersFilterControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOffersFilterControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractOffersFilterControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterControllerAT|typeof xyz.swapee.wc.front.OffersFilterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersFilterControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOffersFilterControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersFilterControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersFilterControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterControllerAT|typeof xyz.swapee.wc.front.OffersFilterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersFilterControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterControllerAT|typeof xyz.swapee.wc.front.OffersFilterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterControllerAT}
 */
xyz.swapee.wc.front.AbstractOffersFilterControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOffersFilterControllerAT.Initialese[]) => xyz.swapee.wc.front.IOffersFilterControllerAT} xyz.swapee.wc.front.OffersFilterControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOffersFilterControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IOffersFilterControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOffersFilterControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IOffersFilterControllerAT
 */
xyz.swapee.wc.front.IOffersFilterControllerAT = class extends /** @type {xyz.swapee.wc.front.IOffersFilterControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersFilterControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOffersFilterControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOffersFilterControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersFilterControllerAT.Initialese>)} xyz.swapee.wc.front.OffersFilterControllerAT.constructor */
/**
 * A concrete class of _IOffersFilterControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.OffersFilterControllerAT
 * @implements {xyz.swapee.wc.front.IOffersFilterControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOffersFilterControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersFilterControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.OffersFilterControllerAT = class extends /** @type {xyz.swapee.wc.front.OffersFilterControllerAT.constructor&xyz.swapee.wc.front.IOffersFilterControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOffersFilterControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersFilterControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OffersFilterControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersFilterControllerAT}
 */
xyz.swapee.wc.front.OffersFilterControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOffersFilterControllerAT} */
xyz.swapee.wc.front.RecordIOffersFilterControllerAT

/** @typedef {xyz.swapee.wc.front.IOffersFilterControllerAT} xyz.swapee.wc.front.BoundIOffersFilterControllerAT */

/** @typedef {xyz.swapee.wc.front.OffersFilterControllerAT} xyz.swapee.wc.front.BoundOffersFilterControllerAT */

/**
 * Contains getters to cast the _IOffersFilterControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IOffersFilterControllerATCaster
 */
xyz.swapee.wc.front.IOffersFilterControllerATCaster = class { }
/**
 * Cast the _IOffersFilterControllerAT_ instance into the _BoundIOffersFilterControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIOffersFilterControllerAT}
 */
xyz.swapee.wc.front.IOffersFilterControllerATCaster.prototype.asIOffersFilterControllerAT
/**
 * Access the _OffersFilterControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOffersFilterControllerAT}
 */
xyz.swapee.wc.front.IOffersFilterControllerATCaster.prototype.superOffersFilterControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/70-IOffersFilterScreen.xml}  50ef1102d975a81636533dbc8814d15e */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.front.OffersFilterInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersFilterDisplay.Settings, !xyz.swapee.wc.IOffersFilterDisplay.Queries, null>&xyz.swapee.wc.IOffersFilterDisplay.Initialese} xyz.swapee.wc.IOffersFilterScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterScreen)} xyz.swapee.wc.AbstractOffersFilterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterScreen} xyz.swapee.wc.OffersFilterScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterScreen` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterScreen
 */
xyz.swapee.wc.AbstractOffersFilterScreen = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterScreen.constructor&xyz.swapee.wc.OffersFilterScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterScreen.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterScreen.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterScreen|typeof xyz.swapee.wc.OffersFilterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersFilterController|typeof xyz.swapee.wc.front.OffersFilterController)|(!xyz.swapee.wc.IOffersFilterDisplay|typeof xyz.swapee.wc.OffersFilterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterScreen}
 */
xyz.swapee.wc.AbstractOffersFilterScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterScreen}
 */
xyz.swapee.wc.AbstractOffersFilterScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterScreen|typeof xyz.swapee.wc.OffersFilterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersFilterController|typeof xyz.swapee.wc.front.OffersFilterController)|(!xyz.swapee.wc.IOffersFilterDisplay|typeof xyz.swapee.wc.OffersFilterDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterScreen}
 */
xyz.swapee.wc.AbstractOffersFilterScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterScreen|typeof xyz.swapee.wc.OffersFilterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOffersFilterController|typeof xyz.swapee.wc.front.OffersFilterController)|(!xyz.swapee.wc.IOffersFilterDisplay|typeof xyz.swapee.wc.OffersFilterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterScreen}
 */
xyz.swapee.wc.AbstractOffersFilterScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterScreen.Initialese[]) => xyz.swapee.wc.IOffersFilterScreen} xyz.swapee.wc.OffersFilterScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.OffersFilterMemory, !xyz.swapee.wc.front.OffersFilterInputs, !HTMLDivElement, !xyz.swapee.wc.IOffersFilterDisplay.Settings, !xyz.swapee.wc.IOffersFilterDisplay.Queries, null, null>&xyz.swapee.wc.front.IOffersFilterController&xyz.swapee.wc.IOffersFilterDisplay)} xyz.swapee.wc.IOffersFilterScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IOffersFilterScreen
 */
xyz.swapee.wc.IOffersFilterScreen = class extends /** @type {xyz.swapee.wc.IOffersFilterScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IOffersFilterController.typeof&xyz.swapee.wc.IOffersFilterDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterScreen.Initialese>)} xyz.swapee.wc.OffersFilterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IOffersFilterScreen} xyz.swapee.wc.IOffersFilterScreen.typeof */
/**
 * A concrete class of _IOffersFilterScreen_ instances.
 * @constructor xyz.swapee.wc.OffersFilterScreen
 * @implements {xyz.swapee.wc.IOffersFilterScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterScreen.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterScreen = class extends /** @type {xyz.swapee.wc.OffersFilterScreen.constructor&xyz.swapee.wc.IOffersFilterScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterScreen}
 */
xyz.swapee.wc.OffersFilterScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOffersFilterScreen} */
xyz.swapee.wc.RecordIOffersFilterScreen

/** @typedef {xyz.swapee.wc.IOffersFilterScreen} xyz.swapee.wc.BoundIOffersFilterScreen */

/** @typedef {xyz.swapee.wc.OffersFilterScreen} xyz.swapee.wc.BoundOffersFilterScreen */

/**
 * Contains getters to cast the _IOffersFilterScreen_ interface.
 * @interface xyz.swapee.wc.IOffersFilterScreenCaster
 */
xyz.swapee.wc.IOffersFilterScreenCaster = class { }
/**
 * Cast the _IOffersFilterScreen_ instance into the _BoundIOffersFilterScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterScreen}
 */
xyz.swapee.wc.IOffersFilterScreenCaster.prototype.asIOffersFilterScreen
/**
 * Access the _OffersFilterScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterScreen}
 */
xyz.swapee.wc.IOffersFilterScreenCaster.prototype.superOffersFilterScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/70-IOffersFilterScreenBack.xml}  7abeb04f27a31fb8e5060456090ded84 */
/** @typedef {xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese} xyz.swapee.wc.back.IOffersFilterScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OffersFilterScreen)} xyz.swapee.wc.back.AbstractOffersFilterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersFilterScreen} xyz.swapee.wc.back.OffersFilterScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersFilterScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersFilterScreen
 */
xyz.swapee.wc.back.AbstractOffersFilterScreen = class extends /** @type {xyz.swapee.wc.back.AbstractOffersFilterScreen.constructor&xyz.swapee.wc.back.OffersFilterScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersFilterScreen.prototype.constructor = xyz.swapee.wc.back.AbstractOffersFilterScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersFilterScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterScreen|typeof xyz.swapee.wc.back.OffersFilterScreen)|(!xyz.swapee.wc.back.IOffersFilterScreenAT|typeof xyz.swapee.wc.back.OffersFilterScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersFilterScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersFilterScreen}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreen}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterScreen|typeof xyz.swapee.wc.back.OffersFilterScreen)|(!xyz.swapee.wc.back.IOffersFilterScreenAT|typeof xyz.swapee.wc.back.OffersFilterScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreen}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterScreen|typeof xyz.swapee.wc.back.OffersFilterScreen)|(!xyz.swapee.wc.back.IOffersFilterScreenAT|typeof xyz.swapee.wc.back.OffersFilterScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreen}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersFilterScreen.Initialese[]) => xyz.swapee.wc.back.IOffersFilterScreen} xyz.swapee.wc.back.OffersFilterScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersFilterScreenCaster&xyz.swapee.wc.back.IOffersFilterScreenAT)} xyz.swapee.wc.back.IOffersFilterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOffersFilterScreenAT} xyz.swapee.wc.back.IOffersFilterScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IOffersFilterScreen
 */
xyz.swapee.wc.back.IOffersFilterScreen = class extends /** @type {xyz.swapee.wc.back.IOffersFilterScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOffersFilterScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersFilterScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersFilterScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterScreen.Initialese>)} xyz.swapee.wc.back.OffersFilterScreen.constructor */
/**
 * A concrete class of _IOffersFilterScreen_ instances.
 * @constructor xyz.swapee.wc.back.OffersFilterScreen
 * @implements {xyz.swapee.wc.back.IOffersFilterScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersFilterScreen = class extends /** @type {xyz.swapee.wc.back.OffersFilterScreen.constructor&xyz.swapee.wc.back.IOffersFilterScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersFilterScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersFilterScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreen}
 */
xyz.swapee.wc.back.OffersFilterScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersFilterScreen} */
xyz.swapee.wc.back.RecordIOffersFilterScreen

/** @typedef {xyz.swapee.wc.back.IOffersFilterScreen} xyz.swapee.wc.back.BoundIOffersFilterScreen */

/** @typedef {xyz.swapee.wc.back.OffersFilterScreen} xyz.swapee.wc.back.BoundOffersFilterScreen */

/**
 * Contains getters to cast the _IOffersFilterScreen_ interface.
 * @interface xyz.swapee.wc.back.IOffersFilterScreenCaster
 */
xyz.swapee.wc.back.IOffersFilterScreenCaster = class { }
/**
 * Cast the _IOffersFilterScreen_ instance into the _BoundIOffersFilterScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersFilterScreen}
 */
xyz.swapee.wc.back.IOffersFilterScreenCaster.prototype.asIOffersFilterScreen
/**
 * Access the _OffersFilterScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersFilterScreen}
 */
xyz.swapee.wc.back.IOffersFilterScreenCaster.prototype.superOffersFilterScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/73-IOffersFilterScreenAR.xml}  8e54e5d42cdf90ec8f4a8ff6aea36ce1 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOffersFilterScreen.Initialese} xyz.swapee.wc.front.IOffersFilterScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OffersFilterScreenAR)} xyz.swapee.wc.front.AbstractOffersFilterScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OffersFilterScreenAR} xyz.swapee.wc.front.OffersFilterScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOffersFilterScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractOffersFilterScreenAR
 */
xyz.swapee.wc.front.AbstractOffersFilterScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractOffersFilterScreenAR.constructor&xyz.swapee.wc.front.OffersFilterScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOffersFilterScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractOffersFilterScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOffersFilterScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractOffersFilterScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterScreenAR|typeof xyz.swapee.wc.front.OffersFilterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersFilterScreen|typeof xyz.swapee.wc.OffersFilterScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOffersFilterScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOffersFilterScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersFilterScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersFilterScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterScreenAR|typeof xyz.swapee.wc.front.OffersFilterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersFilterScreen|typeof xyz.swapee.wc.OffersFilterScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersFilterScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOffersFilterScreenAR|typeof xyz.swapee.wc.front.OffersFilterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOffersFilterScreen|typeof xyz.swapee.wc.OffersFilterScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OffersFilterScreenAR}
 */
xyz.swapee.wc.front.AbstractOffersFilterScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOffersFilterScreenAR.Initialese[]) => xyz.swapee.wc.front.IOffersFilterScreenAR} xyz.swapee.wc.front.OffersFilterScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOffersFilterScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOffersFilterScreen)} xyz.swapee.wc.front.IOffersFilterScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOffersFilterScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IOffersFilterScreenAR
 */
xyz.swapee.wc.front.IOffersFilterScreenAR = class extends /** @type {xyz.swapee.wc.front.IOffersFilterScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOffersFilterScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersFilterScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOffersFilterScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOffersFilterScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersFilterScreenAR.Initialese>)} xyz.swapee.wc.front.OffersFilterScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOffersFilterScreenAR} xyz.swapee.wc.front.IOffersFilterScreenAR.typeof */
/**
 * A concrete class of _IOffersFilterScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.OffersFilterScreenAR
 * @implements {xyz.swapee.wc.front.IOffersFilterScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IOffersFilterScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOffersFilterScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.OffersFilterScreenAR = class extends /** @type {xyz.swapee.wc.front.OffersFilterScreenAR.constructor&xyz.swapee.wc.front.IOffersFilterScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOffersFilterScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOffersFilterScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OffersFilterScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OffersFilterScreenAR}
 */
xyz.swapee.wc.front.OffersFilterScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOffersFilterScreenAR} */
xyz.swapee.wc.front.RecordIOffersFilterScreenAR

/** @typedef {xyz.swapee.wc.front.IOffersFilterScreenAR} xyz.swapee.wc.front.BoundIOffersFilterScreenAR */

/** @typedef {xyz.swapee.wc.front.OffersFilterScreenAR} xyz.swapee.wc.front.BoundOffersFilterScreenAR */

/**
 * Contains getters to cast the _IOffersFilterScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IOffersFilterScreenARCaster
 */
xyz.swapee.wc.front.IOffersFilterScreenARCaster = class { }
/**
 * Cast the _IOffersFilterScreenAR_ instance into the _BoundIOffersFilterScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIOffersFilterScreenAR}
 */
xyz.swapee.wc.front.IOffersFilterScreenARCaster.prototype.asIOffersFilterScreenAR
/**
 * Access the _OffersFilterScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOffersFilterScreenAR}
 */
xyz.swapee.wc.front.IOffersFilterScreenARCaster.prototype.superOffersFilterScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/74-IOffersFilterScreenAT.xml}  940c635c9f5e6147f2c5fa520327eae1 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OffersFilterScreenAT)} xyz.swapee.wc.back.AbstractOffersFilterScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OffersFilterScreenAT} xyz.swapee.wc.back.OffersFilterScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOffersFilterScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractOffersFilterScreenAT
 */
xyz.swapee.wc.back.AbstractOffersFilterScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractOffersFilterScreenAT.constructor&xyz.swapee.wc.back.OffersFilterScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOffersFilterScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractOffersFilterScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOffersFilterScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterScreenAT|typeof xyz.swapee.wc.back.OffersFilterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOffersFilterScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOffersFilterScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterScreenAT|typeof xyz.swapee.wc.back.OffersFilterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOffersFilterScreenAT|typeof xyz.swapee.wc.back.OffersFilterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreenAT}
 */
xyz.swapee.wc.back.AbstractOffersFilterScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese[]) => xyz.swapee.wc.back.IOffersFilterScreenAT} xyz.swapee.wc.back.OffersFilterScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOffersFilterScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IOffersFilterScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOffersFilterScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IOffersFilterScreenAT
 */
xyz.swapee.wc.back.IOffersFilterScreenAT = class extends /** @type {xyz.swapee.wc.back.IOffersFilterScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOffersFilterScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOffersFilterScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese>)} xyz.swapee.wc.back.OffersFilterScreenAT.constructor */
/**
 * A concrete class of _IOffersFilterScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.OffersFilterScreenAT
 * @implements {xyz.swapee.wc.back.IOffersFilterScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOffersFilterScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.OffersFilterScreenAT = class extends /** @type {xyz.swapee.wc.back.OffersFilterScreenAT.constructor&xyz.swapee.wc.back.IOffersFilterScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOffersFilterScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OffersFilterScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OffersFilterScreenAT}
 */
xyz.swapee.wc.back.OffersFilterScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOffersFilterScreenAT} */
xyz.swapee.wc.back.RecordIOffersFilterScreenAT

/** @typedef {xyz.swapee.wc.back.IOffersFilterScreenAT} xyz.swapee.wc.back.BoundIOffersFilterScreenAT */

/** @typedef {xyz.swapee.wc.back.OffersFilterScreenAT} xyz.swapee.wc.back.BoundOffersFilterScreenAT */

/**
 * Contains getters to cast the _IOffersFilterScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IOffersFilterScreenATCaster
 */
xyz.swapee.wc.back.IOffersFilterScreenATCaster = class { }
/**
 * Cast the _IOffersFilterScreenAT_ instance into the _BoundIOffersFilterScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIOffersFilterScreenAT}
 */
xyz.swapee.wc.back.IOffersFilterScreenATCaster.prototype.asIOffersFilterScreenAT
/**
 * Access the _OffersFilterScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOffersFilterScreenAT}
 */
xyz.swapee.wc.back.IOffersFilterScreenATCaster.prototype.superOffersFilterScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offers-filter/OffersFilter.mvc/design/80-IOffersFilterGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IOffersFilterDisplay.Initialese} xyz.swapee.wc.IOffersFilterGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OffersFilterGPU)} xyz.swapee.wc.AbstractOffersFilterGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.OffersFilterGPU} xyz.swapee.wc.OffersFilterGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOffersFilterGPU` interface.
 * @constructor xyz.swapee.wc.AbstractOffersFilterGPU
 */
xyz.swapee.wc.AbstractOffersFilterGPU = class extends /** @type {xyz.swapee.wc.AbstractOffersFilterGPU.constructor&xyz.swapee.wc.OffersFilterGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOffersFilterGPU.prototype.constructor = xyz.swapee.wc.AbstractOffersFilterGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOffersFilterGPU.class = /** @type {typeof xyz.swapee.wc.AbstractOffersFilterGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOffersFilterGPU|typeof xyz.swapee.wc.OffersFilterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersFilterDisplay|typeof xyz.swapee.wc.back.OffersFilterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOffersFilterGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOffersFilterGPU}
 */
xyz.swapee.wc.AbstractOffersFilterGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterGPU}
 */
xyz.swapee.wc.AbstractOffersFilterGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOffersFilterGPU|typeof xyz.swapee.wc.OffersFilterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersFilterDisplay|typeof xyz.swapee.wc.back.OffersFilterDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterGPU}
 */
xyz.swapee.wc.AbstractOffersFilterGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOffersFilterGPU|typeof xyz.swapee.wc.OffersFilterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOffersFilterDisplay|typeof xyz.swapee.wc.back.OffersFilterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OffersFilterGPU}
 */
xyz.swapee.wc.AbstractOffersFilterGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOffersFilterGPU.Initialese[]) => xyz.swapee.wc.IOffersFilterGPU} xyz.swapee.wc.OffersFilterGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IOffersFilterGPUCaster&com.webcircuits.IBrowserView<.!OffersFilterMemory,>&xyz.swapee.wc.back.IOffersFilterDisplay)} xyz.swapee.wc.IOffersFilterGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!OffersFilterMemory,>} com.webcircuits.IBrowserView<.!OffersFilterMemory,>.typeof */
/**
 * Handles the periphery of the _IOffersFilterDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IOffersFilterGPU
 */
xyz.swapee.wc.IOffersFilterGPU = class extends /** @type {xyz.swapee.wc.IOffersFilterGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!OffersFilterMemory,>.typeof&xyz.swapee.wc.back.IOffersFilterDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOffersFilterGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOffersFilterGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOffersFilterGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterGPU.Initialese>)} xyz.swapee.wc.OffersFilterGPU.constructor */
/**
 * A concrete class of _IOffersFilterGPU_ instances.
 * @constructor xyz.swapee.wc.OffersFilterGPU
 * @implements {xyz.swapee.wc.IOffersFilterGPU} Handles the periphery of the _IOffersFilterDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOffersFilterGPU.Initialese>} ‎
 */
xyz.swapee.wc.OffersFilterGPU = class extends /** @type {xyz.swapee.wc.OffersFilterGPU.constructor&xyz.swapee.wc.IOffersFilterGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOffersFilterGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOffersFilterGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOffersFilterGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOffersFilterGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OffersFilterGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OffersFilterGPU}
 */
xyz.swapee.wc.OffersFilterGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IOffersFilterGPU.
 * @interface xyz.swapee.wc.IOffersFilterGPUFields
 */
xyz.swapee.wc.IOffersFilterGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IOffersFilterGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IOffersFilterGPU} */
xyz.swapee.wc.RecordIOffersFilterGPU

/** @typedef {xyz.swapee.wc.IOffersFilterGPU} xyz.swapee.wc.BoundIOffersFilterGPU */

/** @typedef {xyz.swapee.wc.OffersFilterGPU} xyz.swapee.wc.BoundOffersFilterGPU */

/**
 * Contains getters to cast the _IOffersFilterGPU_ interface.
 * @interface xyz.swapee.wc.IOffersFilterGPUCaster
 */
xyz.swapee.wc.IOffersFilterGPUCaster = class { }
/**
 * Cast the _IOffersFilterGPU_ instance into the _BoundIOffersFilterGPU_ type.
 * @type {!xyz.swapee.wc.BoundIOffersFilterGPU}
 */
xyz.swapee.wc.IOffersFilterGPUCaster.prototype.asIOffersFilterGPU
/**
 * Access the _OffersFilterGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundOffersFilterGPU}
 */
xyz.swapee.wc.IOffersFilterGPUCaster.prototype.superOffersFilterGPU

// nss:xyz.swapee.wc
/* @typal-end */