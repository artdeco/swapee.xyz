import {makeBuffers} from '@webcircuits/webcircuits'

export const OffersFilterBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  dealBrokerCid:Number,
 }),
})

export default OffersFilterBuffer