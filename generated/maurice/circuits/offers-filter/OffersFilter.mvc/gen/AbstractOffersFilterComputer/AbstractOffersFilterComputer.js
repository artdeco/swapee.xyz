import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterComputer}
 */
function __AbstractOffersFilterComputer() {}
__AbstractOffersFilterComputer.prototype = /** @type {!_AbstractOffersFilterComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterComputer}
 */
class _AbstractOffersFilterComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractOffersFilterComputer} ‎
 */
export class AbstractOffersFilterComputer extends newAbstract(
 _AbstractOffersFilterComputer,80665300511,null,{
  asIOffersFilterComputer:1,
  superOffersFilterComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterComputer} */
AbstractOffersFilterComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterComputer} */
function AbstractOffersFilterComputerClass(){}


AbstractOffersFilterComputer[$implementations]=[
 __AbstractOffersFilterComputer,
 Adapter,
]


export default AbstractOffersFilterComputer