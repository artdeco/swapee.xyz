
/**@this {xyz.swapee.wc.IOffersFilterComputer}*/
export function preadaptSelectorsIntent(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersFilterComputer.adaptSelectorsIntent.Form}*/
 const _inputs={
  cryptoSelectInSelected:this.land.CryptoSelectIn?this.land.CryptoSelectIn.model['ef7de']:void 0,
  cryptoSelectOutSelected:this.land.CryptoSelectOut?this.land.CryptoSelectOut.model['ef7de']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(([null,void 0].includes(__inputs.cryptoSelectInSelected))||([null,void 0].includes(__inputs.cryptoSelectOutSelected))) return
 changes.cryptoSelectInSelected=changes['f088d']
 changes.cryptoSelectOutSelected=changes['ca21c']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptSelectorsIntent(__inputs,__changes)
 const{currencyTo:currencyTo,currencyFrom:currencyFrom,...REST}=RET
 const{land:{ExchangeIntent:ExchangeIntent}}=this
 if(ExchangeIntent) ExchangeIntent.port.inputs['c23cd']=currencyTo
 if(ExchangeIntent) ExchangeIntent.port.inputs['96c88']=currencyFrom
 return REST
}

/**@this {xyz.swapee.wc.IOffersFilterComputer}*/
export function preadaptIgnoreCryptosOut(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosOut.Form}*/
 const _inputs={
  cryptoSelectInSelected:this.land.CryptoSelectIn?this.land.CryptoSelectIn.model['ef7de']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.cryptoSelectInSelected)) return
 changes.cryptoSelectInSelected=changes['f088d']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptIgnoreCryptosOut(__inputs,__changes)
 const{cryptoSelectOutIgnoreCryptos:cryptoSelectOutIgnoreCryptos,...REST}=RET
 const{land:{CryptoSelectOut:CryptoSelectOut}}=this
 if(CryptoSelectOut) CryptoSelectOut.port.inputs['a35e0']=cryptoSelectOutIgnoreCryptos
 return REST
}

/**@this {xyz.swapee.wc.IOffersFilterComputer}*/
export function preadaptIgnoreCryptosIn(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IOffersFilterComputer.adaptIgnoreCryptosIn.Form}*/
 const _inputs={
  cryptoSelectOutSelected:this.land.CryptoSelectOut?this.land.CryptoSelectOut.model['ef7de']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.cryptoSelectOutSelected)) return
 changes.cryptoSelectOutSelected=changes['ca21c']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptIgnoreCryptosIn(__inputs,__changes)
 const{cryptoSelectInIgnoreCryptos:cryptoSelectInIgnoreCryptos,...REST}=RET
 const{land:{CryptoSelectIn:CryptoSelectIn}}=this
 if(CryptoSelectIn) CryptoSelectIn.port.inputs['a35e0']=cryptoSelectInIgnoreCryptos
 return REST
}