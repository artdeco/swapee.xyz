import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterScreenAT}
 */
function __AbstractOffersFilterScreenAT() {}
__AbstractOffersFilterScreenAT.prototype = /** @type {!_AbstractOffersFilterScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterScreenAT}
 */
class _AbstractOffersFilterScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOffersFilterScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterScreenAT} ‎
 */
class AbstractOffersFilterScreenAT extends newAbstract(
 _AbstractOffersFilterScreenAT,806653005127,null,{
  asIOffersFilterScreenAT:1,
  superOffersFilterScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterScreenAT} */
AbstractOffersFilterScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterScreenAT} */
function AbstractOffersFilterScreenATClass(){}

export default AbstractOffersFilterScreenAT


AbstractOffersFilterScreenAT[$implementations]=[
 __AbstractOffersFilterScreenAT,
 UartUniversal,
]