export default function OffersFilterRenderVdus(){
 return (<div $id="OffersFilter">
  <vdu $id="SwapBu" />
  <vdu $id="AmountIn" />
  <vdu $id="ShowWhenReady" />
  <vdu $id="ConcealWhenReady" />
  <vdu $id="AmountOut" />
  <vdu $id="FloatingLa" />
  <vdu $id="FixedLa" />
  <vdu $id="AnyLa" />
  <vdu $id="AmountOutLoIn" />
  <vdu $id="ShowWhenSelectedIn" />
  <vdu $id="ShowWhenSelectedOut" />
  <vdu $id="MinAmountWr" />
  <vdu $id="MinAmountLa" />
  <vdu $id="MaxAmountWr" />
  <vdu $id="MaxAmountLa" />
  <vdu $id="OutWr" />
  <vdu $id="InWr" />
  <vdu $id="CryptoSelectIn" />
  <vdu $id="CryptoSelectOut" />
  <vdu $id="ReadyLoadingStripe1" />
  <vdu $id="ReadyLoadingStripe2" />
 </div>)
}