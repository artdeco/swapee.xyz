
import AbstractOffersFilter from '../AbstractOffersFilter'

/** @abstract {xyz.swapee.wc.IOffersFilterElement} */
export default class AbstractOffersFilterElement { }



AbstractOffersFilterElement[$implementations]=[AbstractOffersFilter,
 /** @type {!AbstractOffersFilterElement} */ ({
  rootId:'OffersFilter',
  __$id:8066530051,
  fqn:'xyz.swapee.wc.IOffersFilter',
  maurice_element_v3:true,
 }),
]