import OffersFilterRenderVdus from './methods/render-vdus'
import OffersFilterElementPort from '../OffersFilterElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {OffersFilterInputsPQs} from '../../pqs/OffersFilterInputsPQs'
import {OffersFilterQueriesPQs} from '../../pqs/OffersFilterQueriesPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractOffersFilter from '../AbstractOffersFilter'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterElement}
 */
function __AbstractOffersFilterElement() {}
__AbstractOffersFilterElement.prototype = /** @type {!_AbstractOffersFilterElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterElement}
 */
class _AbstractOffersFilterElement { }
/**
 * A component description.
 *
 * The _IOffersFilter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractOffersFilterElement} ‎
 */
class AbstractOffersFilterElement extends newAbstract(
 _AbstractOffersFilterElement,806653005112,null,{
  asIOffersFilterElement:1,
  superOffersFilterElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterElement} */
AbstractOffersFilterElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterElement} */
function AbstractOffersFilterElementClass(){}

export default AbstractOffersFilterElement


AbstractOffersFilterElement[$implementations]=[
 __AbstractOffersFilterElement,
 ElementBase,
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':allow-all':allowAllColAttr,
   ':deal-broker-cid':dealBrokerCidColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'allow-all':allowAllAttr,
    'deal-broker-cid':dealBrokerCidAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(allowAllAttr===undefined?{'allow-all':allowAllColAttr}:{}),
    ...(dealBrokerCidAttr===undefined?{'deal-broker-cid':dealBrokerCidColAttr}:{}),
   }
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'allow-all':allowAllAttr,
   'deal-broker-cid':dealBrokerCidAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    allowAll:allowAllAttr,
    dealBrokerCid:dealBrokerCidAttr,
   }
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 Landed,
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  render:function renderCryptoSelectIn(){
   const{
    asILanded:{
     land:{
      CryptoSelectIn:CryptoSelectIn,
     },
    },
    asIOffersFilterElement:{
     buildCryptoSelectIn:buildCryptoSelectIn,
    },
   }=this
   if(!CryptoSelectIn) return
   const{model:CryptoSelectInModel}=CryptoSelectIn
   const{
    '11a11':selectedCrypto,
   }=CryptoSelectInModel
   const res=buildCryptoSelectIn({
    selectedCrypto:selectedCrypto,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  render:function renderCryptoSelectOut(){
   const{
    asILanded:{
     land:{
      CryptoSelectOut:CryptoSelectOut,
     },
    },
    asIOffersFilterElement:{
     buildCryptoSelectOut:buildCryptoSelectOut,
    },
   }=this
   if(!CryptoSelectOut) return
   const{model:CryptoSelectOutModel}=CryptoSelectOut
   const{
    '11a11':selectedCrypto,
   }=CryptoSelectOutModel
   const res=buildCryptoSelectOut({
    selectedCrypto:selectedCrypto,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  render:function renderExchangeIntent(){
   const{
    asILanded:{
     land:{
      ExchangeIntent:ExchangeIntent,
     },
    },
    asIOffersFilterElement:{
     buildExchangeIntent:buildExchangeIntent,
    },
   }=this
   if(!ExchangeIntent) return
   const{model:ExchangeIntentModel}=ExchangeIntent
   const{
    '748e6':amountFrom,
    'cec31':fixed,
    '546ad':float,
    '100b8':any,
    'b2fda':ready,
   }=ExchangeIntentModel
   const res=buildExchangeIntent({
    amountFrom:amountFrom,
    fixed:fixed,
    float:float,
    any:any,
    ready:ready,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  render:function renderDealBroker(){
   const{
    asILanded:{
     land:{
      DealBroker:DealBroker,
     },
    },
    asIOffersFilterElement:{
     buildDealBroker:buildDealBroker,
    },
   }=this
   if(!DealBroker) return
   const{model:DealBrokerModel}=DealBroker
   const{
    '341da':gettingOffer,
    'c63f7':estimatedAmountTo,
    'ae0d3':minAmount,
    'd0b0c':maxAmount,
   }=DealBrokerModel
   const res=buildDealBroker({
    gettingOffer:gettingOffer,
    estimatedAmountTo:estimatedAmountTo,
    minAmount:minAmount,
    maxAmount:maxAmount,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.IOffersFilterElement}*/({land:{
    CryptoSelectIn:null,
    CryptoSelectOut:null,
    ExchangeIntent:null,
    DealBroker:null,
   }}))
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  render:OffersFilterRenderVdus,
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  classes:{
   'SelectedRateType': '0aa87',
   'Ready': 'e7d31',
  },
  inputsPQs:OffersFilterInputsPQs,
  queriesPQs:OffersFilterQueriesPQs,
  vdus:{
   'CryptoSelectIn': 'd31f1',
   'CryptoSelectOut': 'd31f2',
   'AmountIn': 'd31f3',
   'AmountOut': 'd31f4',
   'FixedLa': 'd31f5',
   'FloatingLa': 'd31f6',
   'OutWr': 'd31f7',
   'InWr': 'd31f8',
   'ExchangeIntent': 'd31f9',
   'ReadyLoadingStripe1': 'd31f11',
   'ReadyLoadingStripe2': 'd31f12',
   'AmountOutLoIn': 'd31f16',
   'DealBroker': 'd31f17',
   'MinAmountWr': 'd31f18',
   'MinAmountLa': 'd31f19',
   'MaxAmountWr': 'd31f20',
   'MaxAmountLa': 'd31f21',
   'AnyLa': 'd31f23',
   'ShowWhenSelectedIn': 'd31f24',
   'ShowWhenSelectedOut': 'd31f25',
   'ShowWhenReady': 'd31f14',
   'ConcealWhenReady': 'd31f15',
   'SwapBu': 'd31f22',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','allowAll','dealBrokerCid','query:exchange-intent','query:deal-broker','no-solder',':no-solder','allow-all',':allow-all','deal-broker-cid',':deal-broker-cid','fe646','ebca0','b88f2','6c6f9','c2d8b','cd1af','0c1ca','062ec','da65e','5099d','365b9','19802','e8ca3','b5ad9','1b75f','fb6e9','fa89d','c2bba','3e076','5ba9a','700bd','c3b6b','8c4dc','84e4f','c9e36','f4e71','children']),
   })
  },
  get Port(){
   return OffersFilterElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:exchange-intent':exchangeIntentSel,'query:deal-broker':dealBrokerSel}){
   const _ret={}
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   if(dealBrokerSel) _ret.dealBrokerSel=dealBrokerSel
   return _ret
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  constructor(){
   Object.assign(this.buildees,{
    'CryptoSelectIn':{CryptoSelectIn:3545350742},
    'CryptoSelectOut':{CryptoSelectOut:3545350742},
   })
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  constructor(){
   this.land={
    CryptoSelectIn:null,
    CryptoSelectOut:null,
    ExchangeIntent:null,
    ReadyLoadingStripe1:null,
    ReadyLoadingStripe2:null,
    DealBroker:null,
   }
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel,true)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  calibrate:async function awaitOnDealBroker({dealBrokerSel:dealBrokerSel}){
   if(!dealBrokerSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const DealBroker=await milleu(dealBrokerSel,true)
   if(!DealBroker) {
    console.warn('❗️ dealBrokerSel %s must be present on the page for %s to work',dealBrokerSel,fqn)
    return{}
   }
   land.DealBroker=DealBroker
  },
 }),
 AbstractOffersFilterElementClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
  solder:(_,{
   exchangeIntentSel:exchangeIntentSel,
   dealBrokerSel:dealBrokerSel,
  })=>{
   return{
    exchangeIntentSel:exchangeIntentSel,
    dealBrokerSel:dealBrokerSel,
   }
  },
 }),
]



AbstractOffersFilterElement[$implementations]=[AbstractOffersFilter,
 /** @type {!AbstractOffersFilterElement} */ ({
  rootId:'OffersFilter',
  __$id:8066530051,
  fqn:'xyz.swapee.wc.IOffersFilter',
  maurice_element_v3:true,
 }),
]