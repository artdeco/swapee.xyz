import AbstractOffersFilterGPU from '../AbstractOffersFilterGPU'
import AbstractOffersFilterScreenBack from '../AbstractOffersFilterScreenBack'
import {HtmlComponent,Landed,Shorter,mvc} from '@webcircuits/webcircuits'
import {OffersFilterInputsQPs} from '../../pqs/OffersFilterInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractOffersFilter from '../AbstractOffersFilter'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterHtmlComponent}
 */
function __AbstractOffersFilterHtmlComponent() {}
__AbstractOffersFilterHtmlComponent.prototype = /** @type {!_AbstractOffersFilterHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterHtmlComponent}
 */
class _AbstractOffersFilterHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.OffersFilterHtmlComponent} */ (res)
  }
}
/**
 * The _IOffersFilter_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractOffersFilterHtmlComponent} ‎
 */
export class AbstractOffersFilterHtmlComponent extends newAbstract(
 _AbstractOffersFilterHtmlComponent,806653005111,null,{
  asIOffersFilterHtmlComponent:1,
  superOffersFilterHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterHtmlComponent} */
AbstractOffersFilterHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterHtmlComponent} */
function AbstractOffersFilterHtmlComponentClass(){}


AbstractOffersFilterHtmlComponent[$implementations]=[
 __AbstractOffersFilterHtmlComponent,
 HtmlComponent,
 AbstractOffersFilter,
 AbstractOffersFilterGPU,
 AbstractOffersFilterScreenBack,
 Landed,
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  constructor(){
   this.land={
    CryptoSelectIn:null,
    CryptoSelectOut:null,
    ExchangeIntent:null,
    ReadyLoadingStripe1:null,
    ReadyLoadingStripe2:null,
    DealBroker:null,
   }
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  inputsQPs:OffersFilterInputsQPs,
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $show_ShowWhenSelectedIn(_,{CryptoSelectIn:CryptoSelectIn}){
   if(!CryptoSelectIn) return
   let{
    '11a11':selectedCrypto,
   }=CryptoSelectIn.model
   const{
    asIOffersFilterGPU:{ShowWhenSelectedIn:ShowWhenSelectedIn},
    asIBrowserView:{style:style},
   }=this
   style(ShowWhenSelectedIn,'opacity',selectedCrypto?null:0,true)
   style(ShowWhenSelectedIn,'pointer-events',selectedCrypto?null:'none',true)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $show_ShowWhenSelectedOut(_,{CryptoSelectOut:CryptoSelectOut}){
   if(!CryptoSelectOut) return
   let{
    '11a11':selectedCrypto,
   }=CryptoSelectOut.model
   const{
    asIOffersFilterGPU:{ShowWhenSelectedOut:ShowWhenSelectedOut},
    asIBrowserView:{style:style},
   }=this
   style(ShowWhenSelectedOut,'opacity',selectedCrypto?null:0,true)
   style(ShowWhenSelectedOut,'pointer-events',selectedCrypto?null:'none',true)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function paint_value_on_AmountIn(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    '748e6':amountFrom,
   }=ExchangeIntent.model
   const{asIOffersFilterGPU:{AmountIn:AmountIn}}=this
   AmountIn.val(amountFrom)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $show_AmountIn(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
   }=ExchangeIntent.model
   const{
    asIOffersFilterGPU:{AmountIn:AmountIn},
    asIBrowserView:{style:style},
   }=this
   style(AmountIn,'opacity',ready?null:0,true)
   style(AmountIn,'pointer-events',ready?null:'none',true)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function paint_SelectedRateType_on_AnyLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
    '100b8':any,
   }=ExchangeIntent.model
   const{
    asIOffersFilterGPU:{
     AnyLa:AnyLa,
    },
    classes:{SelectedRateType:SelectedRateType},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(ready&&any) addClass(AnyLa,SelectedRateType)
   else removeClass(AnyLa,SelectedRateType)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function interrupt_click_on_AnyLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   const{asIOffersFilterGPU:{AnyLa:AnyLa}}=this
   AnyLa.listen('click',(ev)=>{
    ExchangeIntent['e2f68']()
   })
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function paint_SelectedRateType_on_FixedLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
    'cec31':fixed,
   }=ExchangeIntent.model
   const{
    asIOffersFilterGPU:{
     FixedLa:FixedLa,
    },
    classes:{SelectedRateType:SelectedRateType},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(ready&&fixed) addClass(FixedLa,SelectedRateType)
   else removeClass(FixedLa,SelectedRateType)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function interrupt_click_on_FixedLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   const{asIOffersFilterGPU:{FixedLa:FixedLa}}=this
   FixedLa.listen('click',(ev)=>{
    ExchangeIntent['b86b7']()
   })
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function paint_SelectedRateType_on_FloatingLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
    '546ad':float,
   }=ExchangeIntent.model
   const{
    asIOffersFilterGPU:{
     FloatingLa:FloatingLa,
    },
    classes:{SelectedRateType:SelectedRateType},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(ready&&float) addClass(FloatingLa,SelectedRateType)
   else removeClass(FloatingLa,SelectedRateType)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function interrupt_click_on_FloatingLa(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   const{asIOffersFilterGPU:{FloatingLa:FloatingLa}}=this
   FloatingLa.listen('click',(ev)=>{
    ExchangeIntent['0f496']()
   })
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $show_ShowWhenReady(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
   }=ExchangeIntent.model
   const{
    asIOffersFilterGPU:{ShowWhenReadies:ShowWhenReadies},
    asIBrowserView:{style:style},
   }=this
   style(ShowWhenReadies,'opacity',ready?null:0,true)
   style(ShowWhenReadies,'pointer-events',ready?null:'none',true)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $conceal_ConcealWhenReady(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
   }=ExchangeIntent.model
   const{
    asIOffersFilterGPU:{HideWhenReadies:HideWhenReadies},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(HideWhenReadies,ready)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $reveal_AmountOutLoIn(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '341da':gettingOffer,
   }=DealBroker.model
   const{
    asIOffersFilterGPU:{AmountOutLoIn:AmountOutLoIn},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(AmountOutLoIn,gettingOffer)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function paint_AmountOut_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    'c63f7':estimatedAmountTo,
   }=DealBroker.model
   const{asIOffersFilterGPU:{AmountOut:AmountOut}}=this
   AmountOut.setText(estimatedAmountTo||'')
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $reveal_MinAmountWr(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    'ae0d3':minAmount,
   }=DealBroker.model
   const{
    asIOffersFilterGPU:{MinAmountWr:MinAmountWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(MinAmountWr,minAmount)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function paint_MinAmountLa_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    'ae0d3':minAmount,
   }=DealBroker.model
   const{asIOffersFilterGPU:{MinAmountLa:MinAmountLa}}=this
   MinAmountLa.setText(minAmount)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function $reveal_MaxAmountWr(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    'd0b0c':maxAmount,
   }=DealBroker.model
   const{
    asIOffersFilterGPU:{MaxAmountWr:MaxAmountWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(MaxAmountWr,maxAmount)
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint:function paint_MaxAmountLa_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    'd0b0c':maxAmount,
   }=DealBroker.model
   const{asIOffersFilterGPU:{MaxAmountLa:MaxAmountLa}}=this
   MaxAmountLa.setText(maxAmount)
  },
 }),
 Shorter,
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  short:function shortCryptoSelectIn({ExchangeIntent:_ExchangeIntent}){
   if(!_ExchangeIntent) return
   if([undefined,null].includes(_ExchangeIntent.model['96c88'])) return // currencyFrom
   const{
    asIHtmlComponent:{build:build},
    asIOffersFilterGPU:{
     CryptoSelectIn:CryptoSelectIn,
    },
   }=this
   build(3545350742,{CryptoSelectIn:CryptoSelectIn},{
    /**@this {xyz.swapee.wc.IOffersFilterHtmlComponent}*/
    get['ef7de'](){ // -> selected
     const currencyFrom=_ExchangeIntent.model['96c88'] // <- currencyFrom
     return currencyFrom
    },
   })
   return true
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  short:function shortCryptoSelectOut({ExchangeIntent:_ExchangeIntent}){
   if(!_ExchangeIntent) return
   if([undefined,null].includes(_ExchangeIntent.model['c23cd'])) return // currencyTo
   const{
    asIHtmlComponent:{build:build},
    asIOffersFilterGPU:{
     CryptoSelectOut:CryptoSelectOut,
    },
   }=this
   build(3545350742,{CryptoSelectOut:CryptoSelectOut},{
    /**@this {xyz.swapee.wc.IOffersFilterHtmlComponent}*/
    get['ef7de'](){ // -> selected
     const currencyTo=_ExchangeIntent.model['c23cd'] // <- currencyTo
     return currencyTo
    },
   })
   return true
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIOffersFilterGPU:{
     ExchangeIntent:ExchangeIntent,
     ReadyLoadingStripe1:ReadyLoadingStripe1,
     ReadyLoadingStripe2:ReadyLoadingStripe2,
     DealBroker:DealBroker,
    },
   }=this
   complete(7833868048,{ExchangeIntent:ExchangeIntent})
   complete(7097755498,{ReadyLoadingStripe1:ReadyLoadingStripe1})
   complete(7097755498,{ReadyLoadingStripe2:ReadyLoadingStripe2})
   complete(this.model.dealBrokerCid||3427226314,{DealBroker:DealBroker})
  },
 }),
 AbstractOffersFilterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   this.onSwapDirection=()=>{
    const setCurrencyTo=/**@type {xyz.swapee.wc.IExchangeIntentController.setCurrencyTo}*/(ExchangeIntent['2b26e'])
    setCurrencyTo(this.land.ExchangeIntent?this.land.ExchangeIntent.model['96c88']:void 0)
   }
   this.onSwapDirection=()=>{
    const setCurrencyFrom=/**@type {xyz.swapee.wc.IExchangeIntentController.setCurrencyFrom}*/(ExchangeIntent['77a3f'])
    setCurrencyFrom(this.land.ExchangeIntent?this.land.ExchangeIntent.model['c23cd']:void 0)
   }
   this.onSwapDirection=()=>{
    const setAmountFrom=/**@type {xyz.swapee.wc.IExchangeIntentController.setAmountFrom}*/(ExchangeIntent['7a4f4'])
    setAmountFrom(this.land.DealBroker?this.land.DealBroker.model['c63f7']:void 0)
   }
  },
 }),
]