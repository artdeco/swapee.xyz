import OffersFilterBuffer from '../OffersFilterBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {OffersFilterPortConnector} from '../OffersFilterPort'
import {defineEmitters} from '@mauriceguest/guest2'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterController}
 */
function __AbstractOffersFilterController() {}
__AbstractOffersFilterController.prototype = /** @type {!_AbstractOffersFilterController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterController}
 */
class _AbstractOffersFilterController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractOffersFilterController} ‎
 */
export class AbstractOffersFilterController extends newAbstract(
 _AbstractOffersFilterController,806653005119,null,{
  asIOffersFilterController:1,
  superOffersFilterController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterController} */
AbstractOffersFilterController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterController} */
function AbstractOffersFilterControllerClass(){}


AbstractOffersFilterController[$implementations]=[
 AbstractOffersFilterControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IOffersFilterPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractOffersFilterController,
 AbstractOffersFilterControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterController}*/({
  swapDirection:precombined,
 }),
 OffersFilterBuffer,
 IntegratedController,
 /**@type {!AbstractOffersFilterController}*/(OffersFilterPortConnector),
 AbstractOffersFilterControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterController}*/({
  constructor(){
   defineEmitters(this,{
    onSwapDirection:true,
   })
  },
 }),
 AbstractOffersFilterControllerClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterController}*/({
  swapDirection(){
   const{asIOffersFilterController:{onSwapDirection:onSwapDirection}}=this
   onSwapDirection()
  },
 }),
]


export default AbstractOffersFilterController