import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterControllerAR}
 */
function __AbstractOffersFilterControllerAR() {}
__AbstractOffersFilterControllerAR.prototype = /** @type {!_AbstractOffersFilterControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterControllerAR}
 */
class _AbstractOffersFilterControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOffersFilterControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterControllerAR} ‎
 */
class AbstractOffersFilterControllerAR extends newAbstract(
 _AbstractOffersFilterControllerAR,806653005122,null,{
  asIOffersFilterControllerAR:1,
  superOffersFilterControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterControllerAR} */
AbstractOffersFilterControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterControllerAR} */
function AbstractOffersFilterControllerARClass(){}

export default AbstractOffersFilterControllerAR


AbstractOffersFilterControllerAR[$implementations]=[
 __AbstractOffersFilterControllerAR,
 AR,
 AbstractOffersFilterControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IOffersFilterControllerAR}*/({
  allocator(){
   this.methods={
    swapDirection:'268b3',
    onSwapDirection:'d7159',
    $ExchangeIntent_setAmountFrom_on_AmountIn_input:'51156',
    $ExchangeIntent_setAmountFrom_on_MinAmountLa_click:'b724e',
    $ExchangeIntent_setAmountFrom_on_MaxAmountLa_click:'42b02',
   }
  },
 }),
]