import OffersFilterClassesPQs from '../../pqs/OffersFilterClassesPQs'
import AbstractOffersFilterScreenAR from '../AbstractOffersFilterScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {OffersFilterInputsPQs} from '../../pqs/OffersFilterInputsPQs'
import {OffersFilterQueriesPQs} from '../../pqs/OffersFilterQueriesPQs'
import {OffersFilterMemoryQPs} from '../../pqs/OffersFilterMemoryQPs'
import {OffersFilterVdusPQs} from '../../pqs/OffersFilterVdusPQs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterScreen}
 */
function __AbstractOffersFilterScreen() {}
__AbstractOffersFilterScreen.prototype = /** @type {!_AbstractOffersFilterScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterScreen}
 */
class _AbstractOffersFilterScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractOffersFilterScreen} ‎
 */
class AbstractOffersFilterScreen extends newAbstract(
 _AbstractOffersFilterScreen,806653005124,null,{
  asIOffersFilterScreen:1,
  superOffersFilterScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterScreen} */
AbstractOffersFilterScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterScreen} */
function AbstractOffersFilterScreenClass(){}

export default AbstractOffersFilterScreen


AbstractOffersFilterScreen[$implementations]=[
 __AbstractOffersFilterScreen,
 AbstractOffersFilterScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterScreen}*/({
  inputsPQs:OffersFilterInputsPQs,
  classesPQs:OffersFilterClassesPQs,
  queriesPQs:OffersFilterQueriesPQs,
  memoryQPs:OffersFilterMemoryQPs,
 }),
 Screen,
 AbstractOffersFilterScreenAR,
 AbstractOffersFilterScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterScreen}*/({
  vdusPQs:OffersFilterVdusPQs,
 }),
 AbstractOffersFilterScreenClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterScreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const SwapBus=this.SwapBus
    for(const SwapBu of SwapBus){
      SwapBu.addEventListener('click',(ev)=>{
      ev.preventDefault()
       this.swapDirection()
      return false
      })
    }
   })
  },
 }),
]