import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {OffersFilterInputsPQs} from '../../pqs/OffersFilterInputsPQs'
import {OffersFilterOuterCoreConstructor} from '../OffersFilterCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersFilterPort}
 */
function __OffersFilterPort() {}
__OffersFilterPort.prototype = /** @type {!_OffersFilterPort} */ ({ })
/** @this {xyz.swapee.wc.OffersFilterPort} */ function OffersFilterPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.OffersFilterOuterCore} */ ({model:null})
  OffersFilterOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterPort}
 */
class _OffersFilterPort { }
/**
 * The port that serves as an interface to the _IOffersFilter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractOffersFilterPort} ‎
 */
export class OffersFilterPort extends newAbstract(
 _OffersFilterPort,80665300515,OffersFilterPortConstructor,{
  asIOffersFilterPort:1,
  superOffersFilterPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterPort} */
OffersFilterPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterPort} */
function OffersFilterPortClass(){}

export const OffersFilterPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IOffersFilter.Pinout>}*/({
 get Port() { return OffersFilterPort },
})

OffersFilterPort[$implementations]=[
 OffersFilterPortClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterPort}*/({
  resetPort(){
   this.resetOffersFilterPort()
  },
  resetOffersFilterPort(){
   OffersFilterPortConstructor.call(this)
  },
 }),
 __OffersFilterPort,
 Parametric,
 OffersFilterPortClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterPort}*/({
  constructor(){
   mountPins(this.inputs,OffersFilterInputsPQs)
  },
 }),
]


export default OffersFilterPort