import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterDisplay}
 */
function __AbstractOffersFilterDisplay() {}
__AbstractOffersFilterDisplay.prototype = /** @type {!_AbstractOffersFilterDisplay} */ ({ })
/** @this {xyz.swapee.wc.back.OffersFilterDisplay} */ function OffersFilterDisplayConstructor() {
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.ShowWhenReadies=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.HideWhenReadies=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.SwapBus=[]
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterDisplay}
 */
class _AbstractOffersFilterDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterDisplay} ‎
 */
class AbstractOffersFilterDisplay extends newAbstract(
 _AbstractOffersFilterDisplay,806653005118,OffersFilterDisplayConstructor,{
  asIOffersFilterDisplay:1,
  superOffersFilterDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterDisplay} */
AbstractOffersFilterDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterDisplay} */
function AbstractOffersFilterDisplayClass(){}

export default AbstractOffersFilterDisplay


AbstractOffersFilterDisplay[$implementations]=[
 __AbstractOffersFilterDisplay,
 GraphicsDriverBack,
 AbstractOffersFilterDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IOffersFilterDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IOffersFilterDisplay}*/({
    AmountIn:twinMock,
    AmountOut:twinMock,
    FloatingLa:twinMock,
    FixedLa:twinMock,
    AnyLa:twinMock,
    AmountOutLoIn:twinMock,
    ShowWhenSelectedIn:twinMock,
    ShowWhenSelectedOut:twinMock,
    MinAmountWr:twinMock,
    MinAmountLa:twinMock,
    MaxAmountWr:twinMock,
    MaxAmountLa:twinMock,
    OutWr:twinMock,
    InWr:twinMock,
    CryptoSelectIn:twinMock,
    CryptoSelectOut:twinMock,
    ExchangeIntent:twinMock,
    ReadyLoadingStripe1:twinMock,
    ReadyLoadingStripe2:twinMock,
    DealBroker:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.OffersFilterDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IOffersFilterDisplay.Initialese}*/({
   AmountIn:1,
   ShowWhenReadies:1,
   HideWhenReadies:1,
   AmountOut:1,
   SwapBus:1,
   FloatingLa:1,
   FixedLa:1,
   AnyLa:1,
   AmountOutLoIn:1,
   ShowWhenSelectedIn:1,
   ShowWhenSelectedOut:1,
   MinAmountWr:1,
   MinAmountLa:1,
   MaxAmountWr:1,
   MaxAmountLa:1,
   OutWr:1,
   InWr:1,
   CryptoSelectIn:1,
   CryptoSelectOut:1,
   ExchangeIntent:1,
   ReadyLoadingStripe1:1,
   ReadyLoadingStripe2:1,
   DealBroker:1,
  }),
  initializer({
   AmountIn:_AmountIn,
   ShowWhenReadies:_ShowWhenReadies,
   HideWhenReadies:_HideWhenReadies,
   AmountOut:_AmountOut,
   SwapBus:_SwapBus,
   FloatingLa:_FloatingLa,
   FixedLa:_FixedLa,
   AnyLa:_AnyLa,
   AmountOutLoIn:_AmountOutLoIn,
   ShowWhenSelectedIn:_ShowWhenSelectedIn,
   ShowWhenSelectedOut:_ShowWhenSelectedOut,
   MinAmountWr:_MinAmountWr,
   MinAmountLa:_MinAmountLa,
   MaxAmountWr:_MaxAmountWr,
   MaxAmountLa:_MaxAmountLa,
   OutWr:_OutWr,
   InWr:_InWr,
   CryptoSelectIn:_CryptoSelectIn,
   CryptoSelectOut:_CryptoSelectOut,
   ExchangeIntent:_ExchangeIntent,
   ReadyLoadingStripe1:_ReadyLoadingStripe1,
   ReadyLoadingStripe2:_ReadyLoadingStripe2,
   DealBroker:_DealBroker,
  }) {
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_ShowWhenReadies!==undefined) this.ShowWhenReadies=_ShowWhenReadies
   if(_HideWhenReadies!==undefined) this.HideWhenReadies=_HideWhenReadies
   if(_AmountOut!==undefined) this.AmountOut=_AmountOut
   if(_SwapBus!==undefined) this.SwapBus=_SwapBus
   if(_FloatingLa!==undefined) this.FloatingLa=_FloatingLa
   if(_FixedLa!==undefined) this.FixedLa=_FixedLa
   if(_AnyLa!==undefined) this.AnyLa=_AnyLa
   if(_AmountOutLoIn!==undefined) this.AmountOutLoIn=_AmountOutLoIn
   if(_ShowWhenSelectedIn!==undefined) this.ShowWhenSelectedIn=_ShowWhenSelectedIn
   if(_ShowWhenSelectedOut!==undefined) this.ShowWhenSelectedOut=_ShowWhenSelectedOut
   if(_MinAmountWr!==undefined) this.MinAmountWr=_MinAmountWr
   if(_MinAmountLa!==undefined) this.MinAmountLa=_MinAmountLa
   if(_MaxAmountWr!==undefined) this.MaxAmountWr=_MaxAmountWr
   if(_MaxAmountLa!==undefined) this.MaxAmountLa=_MaxAmountLa
   if(_OutWr!==undefined) this.OutWr=_OutWr
   if(_InWr!==undefined) this.InWr=_InWr
   if(_CryptoSelectIn!==undefined) this.CryptoSelectIn=_CryptoSelectIn
   if(_CryptoSelectOut!==undefined) this.CryptoSelectOut=_CryptoSelectOut
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_ReadyLoadingStripe1!==undefined) this.ReadyLoadingStripe1=_ReadyLoadingStripe1
   if(_ReadyLoadingStripe2!==undefined) this.ReadyLoadingStripe2=_ReadyLoadingStripe2
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
  },
 }),
]