import AbstractOffersFilterDisplay from '../AbstractOffersFilterDisplayBack'
import OffersFilterClassesPQs from '../../pqs/OffersFilterClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {OffersFilterClassesQPs} from '../../pqs/OffersFilterClassesQPs'
import {OffersFilterVdusPQs} from '../../pqs/OffersFilterVdusPQs'
import {OffersFilterVdusQPs} from '../../pqs/OffersFilterVdusQPs'
import {OffersFilterMemoryPQs} from '../../pqs/OffersFilterMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterGPU}
 */
function __AbstractOffersFilterGPU() {}
__AbstractOffersFilterGPU.prototype = /** @type {!_AbstractOffersFilterGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterGPU}
 */
class _AbstractOffersFilterGPU { }
/**
 * Handles the periphery of the _IOffersFilterDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractOffersFilterGPU} ‎
 */
class AbstractOffersFilterGPU extends newAbstract(
 _AbstractOffersFilterGPU,806653005115,null,{
  asIOffersFilterGPU:1,
  superOffersFilterGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterGPU} */
AbstractOffersFilterGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterGPU} */
function AbstractOffersFilterGPUClass(){}

export default AbstractOffersFilterGPU


AbstractOffersFilterGPU[$implementations]=[
 __AbstractOffersFilterGPU,
 AbstractOffersFilterGPUClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterGPU}*/({
  classesQPs:OffersFilterClassesQPs,
  vdusPQs:OffersFilterVdusPQs,
  vdusQPs:OffersFilterVdusQPs,
  memoryPQs:OffersFilterMemoryPQs,
 }),
 AbstractOffersFilterDisplay,
 BrowserView,
 AbstractOffersFilterGPUClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterGPU}*/({
  allocator(){
   pressFit(this.classes,'',OffersFilterClassesPQs)
  },
 }),
]