import AbstractOffersFilterControllerAR from '../AbstractOffersFilterControllerAR'
import {AbstractOffersFilterController} from '../AbstractOffersFilterController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterControllerBack}
 */
function __AbstractOffersFilterControllerBack() {}
__AbstractOffersFilterControllerBack.prototype = /** @type {!_AbstractOffersFilterControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterController}
 */
class _AbstractOffersFilterControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterController} ‎
 */
class AbstractOffersFilterControllerBack extends newAbstract(
 _AbstractOffersFilterControllerBack,806653005121,null,{
  asIOffersFilterController:1,
  superOffersFilterController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterController} */
AbstractOffersFilterControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterController} */
function AbstractOffersFilterControllerBackClass(){}

export default AbstractOffersFilterControllerBack


AbstractOffersFilterControllerBack[$implementations]=[
 __AbstractOffersFilterControllerBack,
 AbstractOffersFilterController,
 AbstractOffersFilterControllerAR,
 DriverBack,
]