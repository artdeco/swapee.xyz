import {mountPins} from '@type.engineering/seers'
import {OffersFilterMemoryPQs} from '../../pqs/OffersFilterMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersFilterCore}
 */
function __OffersFilterCore() {}
__OffersFilterCore.prototype = /** @type {!_OffersFilterCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterCore}
 */
class _OffersFilterCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractOffersFilterCore} ‎
 */
class OffersFilterCore extends newAbstract(
 _OffersFilterCore,80665300517,null,{
  asIOffersFilterCore:1,
  superOffersFilterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterCore} */
OffersFilterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterCore} */
function OffersFilterCoreClass(){}

export default OffersFilterCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersFilterOuterCore}
 */
function __OffersFilterOuterCore() {}
__OffersFilterOuterCore.prototype = /** @type {!_OffersFilterOuterCore} */ ({ })
/** @this {xyz.swapee.wc.OffersFilterOuterCore} */
export function OffersFilterOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOffersFilterOuterCore.Model}*/
  this.model={
    dealBrokerCid: 0,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterOuterCore}
 */
class _OffersFilterOuterCore { }
/**
 * The _IOffersFilter_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractOffersFilterOuterCore} ‎
 */
export class OffersFilterOuterCore extends newAbstract(
 _OffersFilterOuterCore,80665300513,OffersFilterOuterCoreConstructor,{
  asIOffersFilterOuterCore:1,
  superOffersFilterOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterOuterCore} */
OffersFilterOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterOuterCore} */
function OffersFilterOuterCoreClass(){}


OffersFilterOuterCore[$implementations]=[
 __OffersFilterOuterCore,
 OffersFilterOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterOuterCore}*/({
  constructor(){
   mountPins(this.model,OffersFilterMemoryPQs)

  },
 }),
]

OffersFilterCore[$implementations]=[
 OffersFilterCoreClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterCore}*/({
  resetCore(){
   this.resetOffersFilterCore()
  },
  resetOffersFilterCore(){
   OffersFilterOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.OffersFilterOuterCore}*/(
     /**@type {!xyz.swapee.wc.IOffersFilterOuterCore}*/(this)),
   )
  },
 }),
 __OffersFilterCore,
 OffersFilterOuterCore,
]

export {OffersFilterCore}