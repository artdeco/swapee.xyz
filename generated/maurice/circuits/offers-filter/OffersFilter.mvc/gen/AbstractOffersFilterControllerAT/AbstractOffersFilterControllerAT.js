import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterControllerAT}
 */
function __AbstractOffersFilterControllerAT() {}
__AbstractOffersFilterControllerAT.prototype = /** @type {!_AbstractOffersFilterControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOffersFilterControllerAT}
 */
class _AbstractOffersFilterControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOffersFilterControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractOffersFilterControllerAT} ‎
 */
class AbstractOffersFilterControllerAT extends newAbstract(
 _AbstractOffersFilterControllerAT,806653005123,null,{
  asIOffersFilterControllerAT:1,
  superOffersFilterControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOffersFilterControllerAT} */
AbstractOffersFilterControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOffersFilterControllerAT} */
function AbstractOffersFilterControllerATClass(){}

export default AbstractOffersFilterControllerAT


AbstractOffersFilterControllerAT[$implementations]=[
 __AbstractOffersFilterControllerAT,
 UartUniversal,
 AbstractOffersFilterControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IOffersFilterControllerAT}*/({
  get asIOffersFilterController(){
   return this
  },
  swapDirection(){
   this.uart.t("inv",{mid:'268b3'})
  },
  onSwapDirection(){
   this.uart.t("inv",{mid:'d7159'})
  },
  $ExchangeIntent_setAmountFrom_on_AmountIn_input(){
   this.uart.t("inv",{mid:'51156',args:[...arguments]})
  },
  $ExchangeIntent_setAmountFrom_on_MinAmountLa_click(){
   this.uart.t("inv",{mid:'b724e'})
  },
  $ExchangeIntent_setAmountFrom_on_MaxAmountLa_click(){
   this.uart.t("inv",{mid:'42b02'})
  },
 }),
]