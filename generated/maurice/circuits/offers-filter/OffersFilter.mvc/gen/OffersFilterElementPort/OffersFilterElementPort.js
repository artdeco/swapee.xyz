import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OffersFilterElementPort}
 */
function __OffersFilterElementPort() {}
__OffersFilterElementPort.prototype = /** @type {!_OffersFilterElementPort} */ ({ })
/** @this {xyz.swapee.wc.OffersFilterElementPort} */ function OffersFilterElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IOffersFilterElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    allowAll: false,
    amountInOpts: {},
    showWhenReadyOpts: {},
    concealWhenReadyOpts: {},
    amountOutOpts: {},
    swapBuOpts: {},
    floatingLaOpts: {},
    fixedLaOpts: {},
    anyLaOpts: {},
    amountOutLoInOpts: {},
    showWhenSelectedInOpts: {},
    showWhenSelectedOutOpts: {},
    minAmountWrOpts: {},
    minAmountLaOpts: {},
    maxAmountWrOpts: {},
    maxAmountLaOpts: {},
    outWrOpts: {},
    inWrOpts: {},
    cryptoSelectInOpts: {},
    cryptoSelectOutOpts: {},
    exchangeIntentOpts: {},
    readyLoadingStripe1Opts: {},
    readyLoadingStripe2Opts: {},
    dealBrokerOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterElementPort}
 */
class _OffersFilterElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractOffersFilterElementPort} ‎
 */
class OffersFilterElementPort extends newAbstract(
 _OffersFilterElementPort,806653005113,OffersFilterElementPortConstructor,{
  asIOffersFilterElementPort:1,
  superOffersFilterElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterElementPort} */
OffersFilterElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterElementPort} */
function OffersFilterElementPortClass(){}

export default OffersFilterElementPort


OffersFilterElementPort[$implementations]=[
 __OffersFilterElementPort,
 OffersFilterElementPortClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'allow-all':undefined,
    'amount-in-opts':undefined,
    'show-when-ready-opts':undefined,
    'conceal-when-ready-opts':undefined,
    'amount-out-opts':undefined,
    'swap-bu-opts':undefined,
    'floating-la-opts':undefined,
    'fixed-la-opts':undefined,
    'any-la-opts':undefined,
    'amount-out-lo-in-opts':undefined,
    'show-when-selected-in-opts':undefined,
    'show-when-selected-out-opts':undefined,
    'min-amount-wr-opts':undefined,
    'min-amount-la-opts':undefined,
    'max-amount-wr-opts':undefined,
    'max-amount-la-opts':undefined,
    'out-wr-opts':undefined,
    'in-wr-opts':undefined,
    'crypto-select-in-opts':undefined,
    'crypto-select-out-opts':undefined,
    'exchange-intent-opts':undefined,
    'ready-loading-stripe1-opts':undefined,
    'ready-loading-stripe2-opts':undefined,
    'deal-broker-opts':undefined,
    'deal-broker-cid':undefined,
   })
  },
 }),
]