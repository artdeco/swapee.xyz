import AbstractOffersFilterProcessor from '../AbstractOffersFilterProcessor'
import {OffersFilterCore} from '../OffersFilterCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractOffersFilterComputer} from '../AbstractOffersFilterComputer'
import {AbstractOffersFilterController} from '../AbstractOffersFilterController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilter}
 */
function __AbstractOffersFilter() {}
__AbstractOffersFilter.prototype = /** @type {!_AbstractOffersFilter} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilter}
 */
class _AbstractOffersFilter { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractOffersFilter} ‎
 */
class AbstractOffersFilter extends newAbstract(
 _AbstractOffersFilter,80665300519,null,{
  asIOffersFilter:1,
  superOffersFilter:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilter} */
AbstractOffersFilter.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilter} */
function AbstractOffersFilterClass(){}

export default AbstractOffersFilter


AbstractOffersFilter[$implementations]=[
 __AbstractOffersFilter,
 OffersFilterCore,
 AbstractOffersFilterProcessor,
 IntegratedComponent,
 AbstractOffersFilterComputer,
 AbstractOffersFilterController,
]


export {AbstractOffersFilter}