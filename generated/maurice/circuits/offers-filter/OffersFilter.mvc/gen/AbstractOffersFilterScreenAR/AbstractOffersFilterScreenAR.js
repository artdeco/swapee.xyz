import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterScreenAR}
 */
function __AbstractOffersFilterScreenAR() {}
__AbstractOffersFilterScreenAR.prototype = /** @type {!_AbstractOffersFilterScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOffersFilterScreenAR}
 */
class _AbstractOffersFilterScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOffersFilterScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractOffersFilterScreenAR} ‎
 */
class AbstractOffersFilterScreenAR extends newAbstract(
 _AbstractOffersFilterScreenAR,806653005126,null,{
  asIOffersFilterScreenAR:1,
  superOffersFilterScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOffersFilterScreenAR} */
AbstractOffersFilterScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOffersFilterScreenAR} */
function AbstractOffersFilterScreenARClass(){}

export default AbstractOffersFilterScreenAR


AbstractOffersFilterScreenAR[$implementations]=[
 __AbstractOffersFilterScreenAR,
 AR,
 AbstractOffersFilterScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IOffersFilterScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractOffersFilterScreenAR}