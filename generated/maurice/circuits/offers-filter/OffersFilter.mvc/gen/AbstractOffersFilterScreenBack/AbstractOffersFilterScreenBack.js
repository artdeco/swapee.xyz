import AbstractOffersFilterScreenAT from '../AbstractOffersFilterScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterScreenBack}
 */
function __AbstractOffersFilterScreenBack() {}
__AbstractOffersFilterScreenBack.prototype = /** @type {!_AbstractOffersFilterScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterScreen}
 */
class _AbstractOffersFilterScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractOffersFilterScreen} ‎
 */
class AbstractOffersFilterScreenBack extends newAbstract(
 _AbstractOffersFilterScreenBack,806653005125,null,{
  asIOffersFilterScreen:1,
  superOffersFilterScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterScreen} */
AbstractOffersFilterScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOffersFilterScreen} */
function AbstractOffersFilterScreenBackClass(){}

export default AbstractOffersFilterScreenBack


AbstractOffersFilterScreenBack[$implementations]=[
 __AbstractOffersFilterScreenBack,
 AbstractOffersFilterScreenAT,
]