import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterProcessor}
 */
function __AbstractOffersFilterProcessor() {}
__AbstractOffersFilterProcessor.prototype = /** @type {!_AbstractOffersFilterProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterProcessor}
 */
class _AbstractOffersFilterProcessor { }
/**
 * The processor to compute changes to the memory for the _IOffersFilter_.
 * @extends {xyz.swapee.wc.AbstractOffersFilterProcessor} ‎
 */
class AbstractOffersFilterProcessor extends newAbstract(
 _AbstractOffersFilterProcessor,80665300518,null,{
  asIOffersFilterProcessor:1,
  superOffersFilterProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterProcessor} */
AbstractOffersFilterProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterProcessor} */
function AbstractOffersFilterProcessorClass(){}

export default AbstractOffersFilterProcessor


AbstractOffersFilterProcessor[$implementations]=[
 __AbstractOffersFilterProcessor,
]