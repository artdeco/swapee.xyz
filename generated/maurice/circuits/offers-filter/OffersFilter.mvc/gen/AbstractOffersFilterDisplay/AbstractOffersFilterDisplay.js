import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOffersFilterDisplay}
 */
function __AbstractOffersFilterDisplay() {}
__AbstractOffersFilterDisplay.prototype = /** @type {!_AbstractOffersFilterDisplay} */ ({ })
/** @this {xyz.swapee.wc.OffersFilterDisplay} */ function OffersFilterDisplayConstructor() {
  /** @type {HTMLInputElement} */ this.AmountIn=null
  /** @type {!Array<!HTMLSpanElement>} */ this.ShowWhenReadies=[]
  /** @type {!Array<!HTMLSpanElement>} */ this.HideWhenReadies=[]
  /** @type {HTMLDivElement} */ this.AmountOut=null
  /** @type {!Array<!HTMLButtonElement>} */ this.SwapBus=[]
  /** @type {HTMLSpanElement} */ this.FloatingLa=null
  /** @type {HTMLSpanElement} */ this.FixedLa=null
  /** @type {HTMLSpanElement} */ this.AnyLa=null
  /** @type {HTMLSpanElement} */ this.AmountOutLoIn=null
  /** @type {HTMLSpanElement} */ this.ShowWhenSelectedIn=null
  /** @type {HTMLSpanElement} */ this.ShowWhenSelectedOut=null
  /** @type {HTMLParagraphElement} */ this.MinAmountWr=null
  /** @type {HTMLAnchorElement} */ this.MinAmountLa=null
  /** @type {HTMLParagraphElement} */ this.MaxAmountWr=null
  /** @type {HTMLAnchorElement} */ this.MaxAmountLa=null
  /** @type {HTMLSpanElement} */ this.OutWr=null
  /** @type {HTMLSpanElement} */ this.InWr=null
  /** @type {HTMLElement} */ this.CryptoSelectIn=null
  /** @type {HTMLElement} */ this.CryptoSelectOut=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
  /** @type {HTMLElement} */ this.ReadyLoadingStripe1=null
  /** @type {HTMLElement} */ this.ReadyLoadingStripe2=null
  /** @type {HTMLElement} */ this.DealBroker=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOffersFilterDisplay}
 */
class _AbstractOffersFilterDisplay { }
/**
 * Display for presenting information from the _IOffersFilter_.
 * @extends {xyz.swapee.wc.AbstractOffersFilterDisplay} ‎
 */
class AbstractOffersFilterDisplay extends newAbstract(
 _AbstractOffersFilterDisplay,806653005116,OffersFilterDisplayConstructor,{
  asIOffersFilterDisplay:1,
  superOffersFilterDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOffersFilterDisplay} */
AbstractOffersFilterDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOffersFilterDisplay} */
function AbstractOffersFilterDisplayClass(){}

export default AbstractOffersFilterDisplay


AbstractOffersFilterDisplay[$implementations]=[
 __AbstractOffersFilterDisplay,
 Display,
 AbstractOffersFilterDisplayClass.prototype=/**@type {!xyz.swapee.wc.IOffersFilterDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{exchangeIntentScopeSel:exchangeIntentScopeSel,dealBrokerScopeSel:dealBrokerScopeSel}}=this
    this.scan({
     exchangeIntentSel:exchangeIntentScopeSel,
     dealBrokerSel:dealBrokerScopeSel,
    })
   })
  },
  scan:function vduScan({exchangeIntentSel:exchangeIntentSelScope,dealBrokerSel:dealBrokerSelScope}){
   const{element:element,asIOffersFilterScreen:{vdusPQs:{
    AmountIn:AmountIn,
    AmountOut:AmountOut,
    FloatingLa:FloatingLa,
    FixedLa:FixedLa,
    AnyLa:AnyLa,
    AmountOutLoIn:AmountOutLoIn,
    ShowWhenSelectedIn:ShowWhenSelectedIn,
    ShowWhenSelectedOut:ShowWhenSelectedOut,
    MinAmountWr:MinAmountWr,
    MinAmountLa:MinAmountLa,
    MaxAmountWr:MaxAmountWr,
    MaxAmountLa:MaxAmountLa,
    OutWr:OutWr,
    InWr:InWr,
    CryptoSelectIn:CryptoSelectIn,
    CryptoSelectOut:CryptoSelectOut,
    ReadyLoadingStripe1:ReadyLoadingStripe1,
    ReadyLoadingStripe2:ReadyLoadingStripe2,
   }},queries:{exchangeIntentSel,dealBrokerSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   /**@type {HTMLElement}*/
   const DealBroker=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _DealBroker=dealBrokerSel?root.querySelector(dealBrokerSel):void 0
    return _DealBroker
   })(dealBrokerSelScope)
   Object.assign(this,{
    SwapBus:element?/**@type {!Array<!HTMLButtonElement>}*/([...element.querySelectorAll('[data-id~=d31f22]')]):[],
    AmountIn:/**@type {HTMLInputElement}*/(children[AmountIn]),
    ShowWhenReadies:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=d31f14]')]):[],
    HideWhenReadies:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=d31f15]')]):[],
    AmountOut:/**@type {HTMLDivElement}*/(children[AmountOut]),
    FloatingLa:/**@type {HTMLSpanElement}*/(children[FloatingLa]),
    FixedLa:/**@type {HTMLSpanElement}*/(children[FixedLa]),
    AnyLa:/**@type {HTMLSpanElement}*/(children[AnyLa]),
    AmountOutLoIn:/**@type {HTMLSpanElement}*/(children[AmountOutLoIn]),
    ShowWhenSelectedIn:/**@type {HTMLSpanElement}*/(children[ShowWhenSelectedIn]),
    ShowWhenSelectedOut:/**@type {HTMLSpanElement}*/(children[ShowWhenSelectedOut]),
    MinAmountWr:/**@type {HTMLParagraphElement}*/(children[MinAmountWr]),
    MinAmountLa:/**@type {HTMLAnchorElement}*/(children[MinAmountLa]),
    MaxAmountWr:/**@type {HTMLParagraphElement}*/(children[MaxAmountWr]),
    MaxAmountLa:/**@type {HTMLAnchorElement}*/(children[MaxAmountLa]),
    OutWr:/**@type {HTMLSpanElement}*/(children[OutWr]),
    InWr:/**@type {HTMLSpanElement}*/(children[InWr]),
    CryptoSelectIn:/**@type {HTMLElement}*/(children[CryptoSelectIn]),
    CryptoSelectOut:/**@type {HTMLElement}*/(children[CryptoSelectOut]),
    ExchangeIntent:ExchangeIntent,
    ReadyLoadingStripe1:/**@type {HTMLElement}*/(children[ReadyLoadingStripe1]),
    ReadyLoadingStripe2:/**@type {HTMLElement}*/(children[ReadyLoadingStripe2]),
    DealBroker:DealBroker,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.OffersFilterDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IOffersFilterDisplay.Initialese}*/({
   AmountIn:1,
   ShowWhenReadies:1,
   HideWhenReadies:1,
   AmountOut:1,
   SwapBus:1,
   FloatingLa:1,
   FixedLa:1,
   AnyLa:1,
   AmountOutLoIn:1,
   ShowWhenSelectedIn:1,
   ShowWhenSelectedOut:1,
   MinAmountWr:1,
   MinAmountLa:1,
   MaxAmountWr:1,
   MaxAmountLa:1,
   OutWr:1,
   InWr:1,
   CryptoSelectIn:1,
   CryptoSelectOut:1,
   ExchangeIntent:1,
   ReadyLoadingStripe1:1,
   ReadyLoadingStripe2:1,
   DealBroker:1,
  }),
  initializer({
   AmountIn:_AmountIn,
   ShowWhenReadies:_ShowWhenReadies,
   HideWhenReadies:_HideWhenReadies,
   AmountOut:_AmountOut,
   SwapBus:_SwapBus,
   FloatingLa:_FloatingLa,
   FixedLa:_FixedLa,
   AnyLa:_AnyLa,
   AmountOutLoIn:_AmountOutLoIn,
   ShowWhenSelectedIn:_ShowWhenSelectedIn,
   ShowWhenSelectedOut:_ShowWhenSelectedOut,
   MinAmountWr:_MinAmountWr,
   MinAmountLa:_MinAmountLa,
   MaxAmountWr:_MaxAmountWr,
   MaxAmountLa:_MaxAmountLa,
   OutWr:_OutWr,
   InWr:_InWr,
   CryptoSelectIn:_CryptoSelectIn,
   CryptoSelectOut:_CryptoSelectOut,
   ExchangeIntent:_ExchangeIntent,
   ReadyLoadingStripe1:_ReadyLoadingStripe1,
   ReadyLoadingStripe2:_ReadyLoadingStripe2,
   DealBroker:_DealBroker,
  }) {
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_ShowWhenReadies!==undefined) this.ShowWhenReadies=_ShowWhenReadies
   if(_HideWhenReadies!==undefined) this.HideWhenReadies=_HideWhenReadies
   if(_AmountOut!==undefined) this.AmountOut=_AmountOut
   if(_SwapBus!==undefined) this.SwapBus=_SwapBus
   if(_FloatingLa!==undefined) this.FloatingLa=_FloatingLa
   if(_FixedLa!==undefined) this.FixedLa=_FixedLa
   if(_AnyLa!==undefined) this.AnyLa=_AnyLa
   if(_AmountOutLoIn!==undefined) this.AmountOutLoIn=_AmountOutLoIn
   if(_ShowWhenSelectedIn!==undefined) this.ShowWhenSelectedIn=_ShowWhenSelectedIn
   if(_ShowWhenSelectedOut!==undefined) this.ShowWhenSelectedOut=_ShowWhenSelectedOut
   if(_MinAmountWr!==undefined) this.MinAmountWr=_MinAmountWr
   if(_MinAmountLa!==undefined) this.MinAmountLa=_MinAmountLa
   if(_MaxAmountWr!==undefined) this.MaxAmountWr=_MaxAmountWr
   if(_MaxAmountLa!==undefined) this.MaxAmountLa=_MaxAmountLa
   if(_OutWr!==undefined) this.OutWr=_OutWr
   if(_InWr!==undefined) this.InWr=_InWr
   if(_CryptoSelectIn!==undefined) this.CryptoSelectIn=_CryptoSelectIn
   if(_CryptoSelectOut!==undefined) this.CryptoSelectOut=_CryptoSelectOut
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_ReadyLoadingStripe1!==undefined) this.ReadyLoadingStripe1=_ReadyLoadingStripe1
   if(_ReadyLoadingStripe2!==undefined) this.ReadyLoadingStripe2=_ReadyLoadingStripe2
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
  },
 }),
]