import {OffersFilterVdusPQs} from './OffersFilterVdusPQs'
export const OffersFilterVdusQPs=/**@type {!xyz.swapee.wc.OffersFilterVdusQPs}*/(Object.keys(OffersFilterVdusPQs)
 .reduce((a,k)=>{a[OffersFilterVdusPQs[k]]=k;return a},{}))