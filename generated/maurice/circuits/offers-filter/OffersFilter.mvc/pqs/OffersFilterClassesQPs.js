import OffersFilterClassesPQs from './OffersFilterClassesPQs'
export const OffersFilterClassesQPs=/**@type {!xyz.swapee.wc.OffersFilterClassesQPs}*/(Object.keys(OffersFilterClassesPQs)
 .reduce((a,k)=>{a[OffersFilterClassesPQs[k]]=k;return a},{}))