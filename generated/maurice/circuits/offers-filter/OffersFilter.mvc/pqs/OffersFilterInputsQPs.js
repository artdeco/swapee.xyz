import {OffersFilterInputsPQs} from './OffersFilterInputsPQs'
export const OffersFilterInputsQPs=/**@type {!xyz.swapee.wc.OffersFilterInputsQPs}*/(Object.keys(OffersFilterInputsPQs)
 .reduce((a,k)=>{a[OffersFilterInputsPQs[k]]=k;return a},{}))