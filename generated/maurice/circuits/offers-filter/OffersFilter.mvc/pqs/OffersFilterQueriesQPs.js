import {OffersFilterQueriesPQs} from './OffersFilterQueriesPQs'
export const OffersFilterQueriesQPs=/**@type {!xyz.swapee.wc.OffersFilterQueriesQPs}*/(Object.keys(OffersFilterQueriesPQs)
 .reduce((a,k)=>{a[OffersFilterQueriesPQs[k]]=k;return a},{}))