import {OffersFilterMemoryPQs} from './OffersFilterMemoryPQs'
export const OffersFilterInputsPQs=/**@type {!xyz.swapee.wc.OffersFilterInputsQPs}*/({
 ...OffersFilterMemoryPQs,
})