export const OffersFilterMemoryPQs=/**@type {!xyz.swapee.wc.OffersFilterMemoryPQs}*/({
 core:'a74ad',
 amountFrom:'748e6',
 cryptoIn:'4212d',
 cryptoOut:'c60b5',
 amountIn:'88da7',
 amountOut:'c125f',
 fixedRate:'7d0bd',
 floatingRate:'57342',
 ready:'b2fda',
 dealBrokerCid:'f4e71',
})