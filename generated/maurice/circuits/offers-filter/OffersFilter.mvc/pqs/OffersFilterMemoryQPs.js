import {OffersFilterMemoryPQs} from './OffersFilterMemoryPQs'
export const OffersFilterMemoryQPs=/**@type {!xyz.swapee.wc.OffersFilterMemoryQPs}*/(Object.keys(OffersFilterMemoryPQs)
 .reduce((a,k)=>{a[OffersFilterMemoryPQs[k]]=k;return a},{}))