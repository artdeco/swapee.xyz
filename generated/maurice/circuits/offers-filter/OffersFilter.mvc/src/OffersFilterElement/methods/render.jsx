/** @type {xyz.swapee.wc.IOffersFilterElement._render} */
export default function OffersFilterRender(_,{swapDirection:swapDirection}) {
 return (<div $id="OffersFilter">
  <button $id="SwapBu" onClick={swapDirection} />
 </div>)
}