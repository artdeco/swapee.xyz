/** @type {xyz.swapee.wc.IOffersFilterElement._buildCryptoSelectOut} */
export default function buildCryptoSelectOut({selectedCrypto:selectedCrypto}) { // todo: instead of creating vdus, can we build directly to components
 return (<div $id="OffersFilter">
  <span $id="ShowWhenSelectedOut" $show={selectedCrypto} />
 </div>)
}