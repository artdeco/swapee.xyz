/** @type {xyz.swapee.wc.IOffersFilterElement._wire} */
export default function wire({
 DealBroker:{minAmount:minAmount,maxAmount:maxAmount},
 ExchangeIntent:{
  setAmountFrom:setAmountFrom,
 },
}) {
 return (<div $id="OffersFilter">
  <input $id="AmountIn" onInput={(ev)=>{
   ev.setThreshold(500)
   setAmountFrom(ev.target.value)
  }} />
  <a $id="MinAmountLa" onClick={setAmountFrom(minAmount)} />
  <a $id="MaxAmountLa" onClick={setAmountFrom(maxAmount)} />
 </div>)
}