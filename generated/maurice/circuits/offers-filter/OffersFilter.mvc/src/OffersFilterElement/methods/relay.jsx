/** @type {xyz.swapee.wc.IOffersFilterElement._relay} */
export default function relay({
 This:This,
 ExchangeIntent:{
  setAmountFrom:setAmountFrom,
  setCurrencyTo:setCurrencyTo,
  setCurrencyFrom:setCurrencyFrom,
 }},{
 DealBroker:{estimatedAmountTo:estimatedAmountTo},
 ExchangeIntent:{
  currencyTo:currencyTo,currencyFrom:currencyFrom,
 }}) {
 return <>
  <This onSwapDirection={[
   setCurrencyTo(currencyFrom),
   setCurrencyFrom(currencyTo),
   setAmountFrom(estimatedAmountTo),
  ]} />
 </>
}