/** @type {xyz.swapee.wc.IOffersFilterElement._buildExchangeIntent} */
export default function buildExchangeIntent({
 amountFrom:amountFrom,fixed:fixed,float:float,any:any,ready:ready,
},{
 setFixed:setFixed,setAny:setAny,setFloat:setFloat,
}) {
 return (<div $id="OffersFilter">
  <input $id="AmountIn" $show={ready} value={amountFrom} />
  <span $id="AnyLa" SelectedRateType={ready&&any} onClick={setAny} />
  <span $id="FixedLa" SelectedRateType={ready&&fixed} onClick={setFixed} />
  <span $id="FloatingLa" SelectedRateType={ready&&float} onClick={setFloat} />
  <span $id="ShowWhenReady" $show={ready} />
  <span $id="ConcealWhenReady" $conceal={ready} />
 </div>)
}