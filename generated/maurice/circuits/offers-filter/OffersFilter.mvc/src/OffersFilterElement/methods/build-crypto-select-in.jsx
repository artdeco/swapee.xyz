/** @type {xyz.swapee.wc.IOffersFilterElement._buildCryptoSelectIn} */
export default function buildCryptoSelectIn({selectedCrypto:selectedCrypto}) {
 return (<div $id="OffersFilter">
  <span $id="ShowWhenSelectedIn" $show={selectedCrypto} />
 </div>)
}