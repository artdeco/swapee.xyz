/** @type {xyz.swapee.wc.IOffersFilterElement._PostProcess} */
export default function PostProcess(html) {
 const{asIProper:{props:{allowAll:allowAll}}}=/**@type {!xyz.swapee.wc.IOffersFilterElement}*/(this)
 if(!allowAll) {
  html=html.replace(/(<!--)( \$if: allow-all -->[\s\S]+?<!-- \/\$if )(-->)/g,(m,cs,c,ce)=>{
   return `${cs}${c.replace(/\S/g,' ')}${ce}`
  })
 }

 return html
}