/** @type {xyz.swapee.wc.IOffersFilterElement._short} */
export default function short(_,{
 CryptoSelectIn:CryptoSelectIn,CryptoSelectOut:CryptoSelectOut,
},{
 ExchangeIntent:{currencyTo:currencyTo,currencyFrom:currencyFrom},
}){
 // this should be part of render?
 // need 2-way binding between components, when crypto select in changes,
 // this field changes here too -> binder.
 return <>
  <CryptoSelectIn selected={currencyFrom} />
  <CryptoSelectOut selected={currencyTo} />
 </>
}