/** @type {xyz.swapee.wc.IOffersFilterElement._buildDealBroker} */
export default function buildDealBroker({
 gettingOffer:gettingOffer,estimatedAmountTo:estimatedAmountTo,
 minAmount:minAmount,maxAmount:maxAmount,
}) {
 return (<div $id="OffersFilter">
  <span $id="AmountOutLoIn" $reveal={gettingOffer} />
  <span $id="AmountOut">{estimatedAmountTo||''}</span>
  <p $id="MinAmountWr" $reveal={minAmount}>
   <a $id="MinAmountLa">{minAmount}</a>
  </p>
  <p $id="MaxAmountWr" $reveal={maxAmount}>
   <a $id="MaxAmountLa">{maxAmount}</a>
  </p>
 </div>)
}