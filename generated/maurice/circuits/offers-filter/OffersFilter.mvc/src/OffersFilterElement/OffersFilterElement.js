import solder from './methods/solder'
import PostProcess from './methods/post-process'
import server from './methods/server'
import buildDealBroker from './methods/build-deal-broker'
import wire from './methods/wire'
import buildCryptoSelectIn from './methods/build-crypto-select-in'
import buildCryptoSelectOut from './methods/build-crypto-select-out'
import buildExchangeIntent from './methods/build-exchange-intent'
import render from './methods/render'
import OffersFilterServerController from '../OffersFilterServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractOffersFilterElement from '../../gen/AbstractOffersFilterElement'

/** @extends {xyz.swapee.wc.OffersFilterElement} */
export default class OffersFilterElement extends AbstractOffersFilterElement.implements(
 OffersFilterServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /**@type {!xyz.swapee.wc.OffersFilterElement}*/({
  constructor(){
   Object.assign(this.buildees,{
   })
  },
  render: function renderWire() {
   const{
    asIOffersFilterElement:{
     wire:$wire,
    },
   }=this
   const res=$wire({
    ExchangeIntent:{
     setAmountFrom(){/**/},
    },
    DealBroker:{
     minAmount:void 0,
     maxAmount:void 0,
    },
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 /** @type {!xyz.swapee.wc.IOffersFilterElement} */ ({
  solder:solder,
  PostProcess:PostProcess,
  server:server,
  buildDealBroker:buildDealBroker,
  wire:wire,
  buildCryptoSelectIn:buildCryptoSelectIn,
  buildCryptoSelectOut:buildCryptoSelectOut,
  buildExchangeIntent:buildExchangeIntent,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IOffersFilterElement}*/({
   classesMap: true,
   rootSelector:     `.OffersFilter`,
   stylesheet:       'html/styles/OffersFilter.css',
   blockName:        'html/OffersFilterBlock.html',
  }),
  /**@type {xyz.swapee.wc.IOffersFilterDesigner}*/({
  }),
){}

// thank you for using web circuits
