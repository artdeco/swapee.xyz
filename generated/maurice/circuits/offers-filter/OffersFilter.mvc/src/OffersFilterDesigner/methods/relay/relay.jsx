
/**@type {xyz.swapee.wc.IOffersFilterDesigner._relay} */
export default function relay({
 This:This,
 ExchangeIntent:{
  setAmountFrom:setAmountFrom,
  setCurrencyTo:setCurrencyTo,
  setCurrencyFrom:setCurrencyFrom,
 }},{
 DealBroker:{estimatedAmountTo:estimatedAmountTo},
 ExchangeIntent:{
  currencyTo:currencyTo,currencyFrom:currencyFrom,
 }}) {
 return h(Fragment,{},
  h(This,{ onSwapDirection:[
   setCurrencyTo(currencyFrom),
   setCurrencyFrom(currencyTo),
   setAmountFrom(estimatedAmountTo),
  ] })
 )
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvb2ZmZXJzLWZpbHRlci9vZmZlcnMtZmlsdGVyLndja3QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFvUUcsU0FBUyxLQUFLO0NBQ2IsU0FBUztDQUNUO0VBQ0MsMkJBQTJCO0VBQzNCLDJCQUEyQjtFQUMzQiwrQkFBK0I7QUFDakM7Q0FDQyxZQUFZLG1DQUFtQztDQUMvQztFQUNDLHFCQUFxQixDQUFDLHlCQUF5QjtBQUNqRCxHQUFHO0NBQ0YsT0FBTyxZQUFDO0VBQ1AsU0FBTSxnQkFBaUI7R0FDdEIsYUFBYSxDQUFDLGFBQWE7R0FDM0IsZUFBZSxDQUFDLFdBQVc7R0FDM0IsYUFBYSxDQUFDLGtCQUFrQjtFQUNqQyxDQUFFO0NBQ0g7QUFDRCxDQUFGIn0=