import AbstractOffersFilterControllerAT from '../../gen/AbstractOffersFilterControllerAT'
import {scheduleLast} from '@type.engineering/type-engineer'
import OffersFilterDisplay from '../OffersFilterDisplay'
import AbstractOffersFilterScreen from '../../gen/AbstractOffersFilterScreen'

/** @extends {xyz.swapee.wc.OffersFilterScreen} */
export default class extends AbstractOffersFilterScreen.implements(
 AbstractOffersFilterControllerAT,
 /**@type {!xyz.swapee.wc.IOffersFilterScreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const{asIOffersFilterDisplay:{
     AmountIn:AmountIn,
    }}=this
    let threshold
    AmountIn.addEventListener('input',(ev)=>{
     const{asIOffersFilterController:{
      $ExchangeIntent_setAmountFrom_on_AmountIn_input:$ExchangeIntent_setAmountFrom_on_AmountIn_input,
     }}=this
     clearTimeout(threshold)
     threshold=setTimeout(()=>{
      $ExchangeIntent_setAmountFrom_on_AmountIn_input(ev.target.value)
     },500)
    })
    const{asIOffersFilterDisplay:{
     MinAmountLa:MinAmountLa,
    }}=this
    MinAmountLa.addEventListener('click',(ev)=>{
     const{asIOffersFilterController:{
      $ExchangeIntent_setAmountFrom_on_MinAmountLa_click:$ExchangeIntent_setAmountFrom_on_MinAmountLa_click,
     }}=this
     $ExchangeIntent_setAmountFrom_on_MinAmountLa_click()
     ev.preventDefault()
     return false
    })
    const{asIOffersFilterDisplay:{
     MaxAmountLa:MaxAmountLa,
    }}=this
    MaxAmountLa.addEventListener('click',(ev)=>{
     const{asIOffersFilterController:{
      $ExchangeIntent_setAmountFrom_on_MaxAmountLa_click:$ExchangeIntent_setAmountFrom_on_MaxAmountLa_click,
     }}=this
     $ExchangeIntent_setAmountFrom_on_MaxAmountLa_click()
     ev.preventDefault()
     return false
    })
   })
  },
 }),
 OffersFilterDisplay,
 /**@type {!xyz.swapee.wc.IOffersFilterScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IOffersFilterScreen} */ ({
  __$id:8066530051,
 }),
/**@type {!xyz.swapee.wc.IOffersFilterScreen}*/({
   // deduceInputs(el) {},
  }),
){}