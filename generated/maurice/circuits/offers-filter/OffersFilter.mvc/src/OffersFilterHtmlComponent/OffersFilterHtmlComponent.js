import OffersFilterHtmlController from '../OffersFilterHtmlController'
import OffersFilterHtmlComputer from '../OffersFilterHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractOffersFilterHtmlComponent} from '../../gen/AbstractOffersFilterHtmlComponent'

/** @extends {xyz.swapee.wc.OffersFilterHtmlComponent} */
export default class extends AbstractOffersFilterHtmlComponent.implements(
 OffersFilterHtmlController,
 OffersFilterHtmlComputer,
 /**@type {!xyz.swapee.wc.IOffersFilterHtmlComponent}*/({
  $ExchangeIntent_setAmountFrom_on_AmountIn_input(value){
   const{asILanded:{land:{
    ExchangeIntent:ExchangeIntent,
   }}}=this
   if(!ExchangeIntent) return
   ExchangeIntent['7a4f4'](value)
  },
  $ExchangeIntent_setAmountFrom_on_MinAmountLa_click(){
   const{asILanded:{land:{
    DealBroker:DealBroker,
    ExchangeIntent:ExchangeIntent,
   }}}=this
   if(!DealBroker) return
   if(!ExchangeIntent) return
   if(DealBroker.model['ae0d3']===undefined) return
   ExchangeIntent['7a4f4'](DealBroker.model['ae0d3'])
  },
  $ExchangeIntent_setAmountFrom_on_MaxAmountLa_click(){
   const{asILanded:{land:{
    DealBroker:DealBroker,
    ExchangeIntent:ExchangeIntent,
   }}}=this
   if(!DealBroker) return
   if(!ExchangeIntent) return
   if(DealBroker.model['d0b0c']===undefined) return
   ExchangeIntent['7a4f4'](DealBroker.model['d0b0c'])
  },
 }),
 IntegratedComponentInitialiser,
){}