import adaptIgnoreCryptosIn from './methods/adapt-ignore-cryptos-in'
import adaptIgnoreCryptosOut from './methods/adapt-ignore-cryptos-out'
import adaptSelectorsIntent from './methods/adapt-selectors-intent'
import {preadaptIgnoreCryptosIn,preadaptIgnoreCryptosOut,preadaptSelectorsIntent} from '../../gen/AbstractOffersFilterComputer/preadapters'
import AbstractOffersFilterComputer from '../../gen/AbstractOffersFilterComputer'

/** @extends {xyz.swapee.wc.OffersFilterComputer} */
export default class OffersFilterHtmlComputer extends AbstractOffersFilterComputer.implements(
 /** @type {!xyz.swapee.wc.IOffersFilterComputer} */ ({
  adaptIgnoreCryptosIn:adaptIgnoreCryptosIn,
  adaptIgnoreCryptosOut:adaptIgnoreCryptosOut,
  adaptSelectorsIntent:adaptSelectorsIntent,
  adapt:[preadaptIgnoreCryptosIn,preadaptIgnoreCryptosOut,preadaptSelectorsIntent],
 }),
/**@type {!xyz.swapee.wc.IOffersFilterComputer}*/({
   // async adaptReady({ready:ready}) {
   //  await new Promise(r=>setTimeout(r))
   //  return{ready:ready}
   // },
  }),
){}