/** @type {xyz.swapee.wc.IOffersFilterComputer._adaptIgnoreCryptosIn} */
export default function adaptIgnoreCryptosIn({cryptoSelectOutSelected:selected}) {
 return{
  cryptoSelectInIgnoreCryptos:new Set([selected]),
 }
}