/** @type {xyz.swapee.wc.IOffersFilterComputer._adaptIgnoreCryptosOut} */
export default function adaptIgnoreCryptosOut({cryptoSelectInSelected:selected}) {
 return{
  cryptoSelectOutIgnoreCryptos:new Set([selected]),
 }
}