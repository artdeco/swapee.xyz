/** @type {xyz.swapee.wc.IOffersFilterComputer._adaptSelectorsIntent} */
export default function adaptSelectorsIntent({
 cryptoSelectInSelected:cryptoSelectInSelected,
 cryptoSelectOutSelected:cryptoSelectOutSelected,
}) {
 return {
  currencyFrom:cryptoSelectInSelected,
  currencyTo:cryptoSelectOutSelected,
 }
}