/** @extends {xyz.swapee.wc.AbstractExchangeBroker} */
export default class AbstractExchangeBroker extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractExchangeBrokerComputer} */
export class AbstractExchangeBrokerComputer extends (<computer>
   <adapter name="adaptPaymentReceived">
    <xyz.swapee.wc.IExchangeBrokerCore paymentReceived="empty" />
    <xyz.swapee.wc.IExchangeBrokerCore paymentStatus="required" />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentReceived />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentCompleted">
    <xyz.swapee.wc.IExchangeBrokerCore paymentCompleted="empty" />
    <xyz.swapee.wc.IExchangeBrokerCore paymentStatus="required" />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentCompleted />
    </outputs>
   </adapter>
   <adapter name="adaptCheckingPaymentStatus">
    <xyz.swapee.wc.IExchangeBrokerCore checkingStatus status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore checkingPaymentStatus />
    </outputs>
   </adapter>
   <adapter name="adaptCheckingExchangeStatus">
    <xyz.swapee.wc.IExchangeBrokerCore checkingStatus status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore checkingExchangeStatus />
    </outputs>
   </adapter>

   <adapter name="adaptFinishedDate">
    <xyz.swapee.wc.IExchangeBrokerCore exchangeFinished />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore finishedDate />
    </outputs>
    When the status changes to "finished", records the date.
   </adapter>
   <adapter name="adaptWaitingForPayment">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore waitingForPayment />
    </outputs>
   </adapter>
   <adapter name="adaptExchangeFinished">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore exchangeFinished />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentStatus">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentStatus />
    </outputs>
   </adapter>
   <adapter name="adaptProcessingPayment">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore processingPayment />
    </outputs>
   </adapter>
   <adapter name="adaptCreatedDate">
    <xyz.swapee.wc.IExchangeBrokerCore createdAt />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore createdDate />
    </outputs>
   </adapter>
   <adapter name="adaptPaymentExpiredOrOverdue">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore paymentExpiredOrOverdue />
    </outputs>
   </adapter>
   <adapter name="adaptExchangeComplete">
    <xyz.swapee.wc.IExchangeBrokerCore status />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore exchangeComplete />
    </outputs>
   </adapter>

   <adapter name="adaptNotId">
    <xyz.swapee.wc.IExchangeBrokerCore id />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore notId Id />
    </outputs>
   </adapter>

   <adapter name="adaptStatusLastChecked">
    <xyz.swapee.wc.IExchangeBrokerCore checkStatusError="empty" checkingStatus="empty" />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerCore statusLastChecked />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeBrokerController} */
export class AbstractExchangeBrokerController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeBrokerPort} */
export class ExchangeBrokerPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeBrokerView} */
export class AbstractExchangeBrokerView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeBrokerElement} */
export class AbstractExchangeBrokerElement extends (<element v3 html mv>
 <block src="./ExchangeBroker.mvc/src/ExchangeBrokerElement/methods/render.jsx" />
 <inducer src="./ExchangeBroker.mvc/src/ExchangeBrokerElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeBrokerHtmlComponent} */
export class AbstractExchangeBrokerHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>