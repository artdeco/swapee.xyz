/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IExchangeBrokerComputer': {
  'id': 16830596651,
  'symbols': {},
  'methods': {
   'adaptNotId': 1,
   'compute': 2,
   'adaptPaymentExpiredOrOverdue': 4,
   'adaptExchangeComplete': 5,
   'adaptCreatedDate': 6,
   'adaptWaitingForPayment': 7,
   'adaptPaymentStatus': 8,
   'adaptProcessingPayment': 9,
   'adaptExchangeFinished': 10,
   'adaptFinishedDate': 11,
   'adaptCheckingPaymentStatus': 12,
   'adaptCheckingExchangeStatus': 13,
   'adaptStatusLastChecked': 14,
   'adaptPaymentCompleted': 15,
   'adaptPaymentReceived': 16
  }
 },
 'xyz.swapee.wc.ExchangeBrokerMemoryPQs': {
  'id': 16830596652,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeBrokerOuterCore': {
  'id': 16830596653,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeBrokerInputsPQs': {
  'id': 16830596654,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeBrokerPort': {
  'id': 16830596655,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeBrokerPort': 2
  }
 },
 'xyz.swapee.wc.IExchangeBrokerPortInterface': {
  'id': 16830596656,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerCore': {
  'id': 16830596657,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeBrokerCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeBrokerProcessor': {
  'id': 16830596658,
  'symbols': {},
  'methods': {
   'pulseCreateTransaction': 1,
   'pulseCheckPayment': 2
  }
 },
 'xyz.swapee.wc.IExchangeBroker': {
  'id': 16830596659,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerBuffer': {
  'id': 168305966510,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerHtmlComponent': {
  'id': 168305966511,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerElement': {
  'id': 168305966512,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IExchangeBrokerElementPort': {
  'id': 168305966513,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerDesigner': {
  'id': 168305966514,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeBrokerGPU': {
  'id': 168305966515,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerDisplay': {
  'id': 168305966516,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeBrokerVdusPQs': {
  'id': 168305966517,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeBrokerDisplay': {
  'id': 168305966518,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IExchangeBrokerController': {
  'id': 168305966519,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'setAddress': 8,
   'unsetAddress': 9,
   'setExtraId': 10,
   'unsetExtraId': 11,
   'setRefundAddress': 12,
   'unsetRefundAddress': 13,
   'setRefundExtraId': 14,
   'unsetRefundExtraId': 15,
   'setCreateTransactionError': 16,
   'unsetCreateTransactionError': 17,
   'pulseCreateTransaction': 18,
   'setCheckPaymentError': 19,
   'unsetCheckPaymentError': 20,
   'pulseCheckPayment': 21,
   'reset': 24,
   'onReset': 25,
   'setStatusLastChecked': 26,
   'unsetStatusLastChecked': 27
  }
 },
 'xyz.swapee.wc.front.IExchangeBrokerController': {
  'id': 168305966520,
  'symbols': {},
  'methods': {
   'setAddress': 7,
   'unsetAddress': 8,
   'setExtraId': 9,
   'unsetExtraId': 10,
   'setRefundAddress': 11,
   'unsetRefundAddress': 12,
   'setRefundExtraId': 13,
   'unsetRefundExtraId': 14,
   'setCreateTransactionError': 15,
   'unsetCreateTransactionError': 16,
   'pulseCreateTransaction': 17,
   'setCheckPaymentError': 18,
   'unsetCheckPaymentError': 19,
   'pulseCheckPayment': 20,
   'reset': 23,
   'onReset': 24,
   'setStatusLastChecked': 25,
   'unsetStatusLastChecked': 26
  }
 },
 'xyz.swapee.wc.back.IExchangeBrokerController': {
  'id': 168305966521,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeBrokerControllerAR': {
  'id': 168305966522,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeBrokerControllerAT': {
  'id': 168305966523,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeBrokerScreen': {
  'id': 168305966524,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeBrokerScreen': {
  'id': 168305966525,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeBrokerScreenAR': {
  'id': 168305966526,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeBrokerScreenAT': {
  'id': 168305966527,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeBrokerCachePQs': {
  'id': 168305966528,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})