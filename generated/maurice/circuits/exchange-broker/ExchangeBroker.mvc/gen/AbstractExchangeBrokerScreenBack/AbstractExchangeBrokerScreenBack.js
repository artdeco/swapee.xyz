import AbstractExchangeBrokerScreenAT from '../AbstractExchangeBrokerScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerScreenBack}
 */
function __AbstractExchangeBrokerScreenBack() {}
__AbstractExchangeBrokerScreenBack.prototype = /** @type {!_AbstractExchangeBrokerScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerScreen}
 */
class _AbstractExchangeBrokerScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerScreen} ‎
 */
class AbstractExchangeBrokerScreenBack extends newAbstract(
 _AbstractExchangeBrokerScreenBack,168305966525,null,{
  asIExchangeBrokerScreen:1,
  superExchangeBrokerScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerScreen} */
AbstractExchangeBrokerScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerScreen} */
function AbstractExchangeBrokerScreenBackClass(){}

export default AbstractExchangeBrokerScreenBack


AbstractExchangeBrokerScreenBack[$implementations]=[
 __AbstractExchangeBrokerScreenBack,
 AbstractExchangeBrokerScreenAT,
]