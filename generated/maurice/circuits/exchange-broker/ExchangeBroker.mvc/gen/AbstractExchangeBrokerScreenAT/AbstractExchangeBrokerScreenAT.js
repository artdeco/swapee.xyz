import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerScreenAT}
 */
function __AbstractExchangeBrokerScreenAT() {}
__AbstractExchangeBrokerScreenAT.prototype = /** @type {!_AbstractExchangeBrokerScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerScreenAT}
 */
class _AbstractExchangeBrokerScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeBrokerScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerScreenAT} ‎
 */
class AbstractExchangeBrokerScreenAT extends newAbstract(
 _AbstractExchangeBrokerScreenAT,168305966527,null,{
  asIExchangeBrokerScreenAT:1,
  superExchangeBrokerScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerScreenAT} */
AbstractExchangeBrokerScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerScreenAT} */
function AbstractExchangeBrokerScreenATClass(){}

export default AbstractExchangeBrokerScreenAT


AbstractExchangeBrokerScreenAT[$implementations]=[
 __AbstractExchangeBrokerScreenAT,
 UartUniversal,
]