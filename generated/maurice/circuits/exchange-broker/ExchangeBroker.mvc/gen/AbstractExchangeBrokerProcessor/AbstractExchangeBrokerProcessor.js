import { create, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerProcessor}
 */
function __AbstractExchangeBrokerProcessor() {}
__AbstractExchangeBrokerProcessor.prototype = /** @type {!_AbstractExchangeBrokerProcessor} */ ({
  /** @return {void} */
  pulseCreateTransaction() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     createTransaction:true,
    })
  },
  /** @return {void} */
  pulseCheckPayment() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     checkPayment:true,
    })
  },
})
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerProcessor}
 */
class _AbstractExchangeBrokerProcessor { }
/**
 * The processor to compute changes to the memory for the _IExchangeBroker_.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerProcessor} ‎
 */
class AbstractExchangeBrokerProcessor extends newAbstract(
 _AbstractExchangeBrokerProcessor,16830596658,null,{
  asIExchangeBrokerProcessor:1,
  superExchangeBrokerProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerProcessor} */
AbstractExchangeBrokerProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerProcessor} */
function AbstractExchangeBrokerProcessorClass(){}

export default AbstractExchangeBrokerProcessor


AbstractExchangeBrokerProcessor[$implementations]=[
 __AbstractExchangeBrokerProcessor,
]