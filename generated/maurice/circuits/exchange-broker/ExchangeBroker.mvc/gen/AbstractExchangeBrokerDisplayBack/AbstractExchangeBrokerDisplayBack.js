import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerDisplay}
 */
function __AbstractExchangeBrokerDisplay() {}
__AbstractExchangeBrokerDisplay.prototype = /** @type {!_AbstractExchangeBrokerDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerDisplay}
 */
class _AbstractExchangeBrokerDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerDisplay} ‎
 */
class AbstractExchangeBrokerDisplay extends newAbstract(
 _AbstractExchangeBrokerDisplay,168305966518,null,{
  asIExchangeBrokerDisplay:1,
  superExchangeBrokerDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerDisplay} */
AbstractExchangeBrokerDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerDisplay} */
function AbstractExchangeBrokerDisplayClass(){}

export default AbstractExchangeBrokerDisplay


AbstractExchangeBrokerDisplay[$implementations]=[
 __AbstractExchangeBrokerDisplay,
 GraphicsDriverBack,
 AbstractExchangeBrokerDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeBrokerDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IExchangeBrokerDisplay}*/({
    Debug:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.ExchangeBrokerDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IExchangeBrokerDisplay.Initialese}*/({
   Debug:1,
  }),
  initializer({
   Debug:_Debug,
  }) {
   if(_Debug!==undefined) this.Debug=_Debug
  },
 }),
]