import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerControllerAR}
 */
function __AbstractExchangeBrokerControllerAR() {}
__AbstractExchangeBrokerControllerAR.prototype = /** @type {!_AbstractExchangeBrokerControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerControllerAR}
 */
class _AbstractExchangeBrokerControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeBrokerControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerControllerAR} ‎
 */
class AbstractExchangeBrokerControllerAR extends newAbstract(
 _AbstractExchangeBrokerControllerAR,168305966522,null,{
  asIExchangeBrokerControllerAR:1,
  superExchangeBrokerControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerControllerAR} */
AbstractExchangeBrokerControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerControllerAR} */
function AbstractExchangeBrokerControllerARClass(){}

export default AbstractExchangeBrokerControllerAR


AbstractExchangeBrokerControllerAR[$implementations]=[
 __AbstractExchangeBrokerControllerAR,
 AR,
 AbstractExchangeBrokerControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeBrokerControllerAR}*/({
  allocator(){
   this.methods={
    reset:'70605',
    onReset:'37334',
    setCheckPaymentError:'250b2',
    unsetCheckPaymentError:'d553e',
    setAddress:'c73ca',
    unsetAddress:'3f1d3',
    setExtraId:'8a942',
    unsetExtraId:'cc115',
    setRefundAddress:'3e295',
    unsetRefundAddress:'ff85d',
    setRefundExtraId:'3567c',
    unsetRefundExtraId:'317cb',
    setCreateTransactionError:'6a351',
    unsetCreateTransactionError:'ed2de',
    setStatusLastChecked:'dc77f',
    unsetStatusLastChecked:'9d643',
    pulseCreateTransaction:'2e29f',
    pulseCheckPayment:'b71b0',
   }
  },
 }),
]