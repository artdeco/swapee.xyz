import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeBrokerMemoryPQs} from '../../pqs/ExchangeBrokerMemoryPQs'
import {ExchangeBrokerCachePQs} from '../../pqs/ExchangeBrokerCachePQs'
import { newAbstract, create, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeBrokerCore}
 */
function __ExchangeBrokerCore() {}
__ExchangeBrokerCore.prototype = /** @type {!_ExchangeBrokerCore} */ ({ })
/** @this {xyz.swapee.wc.ExchangeBrokerCore} */ function ExchangeBrokerCoreConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeBrokerCore.Model}*/
  this.model={
    checkingPaymentStatus: false,
    checkingExchangeStatus: false,
    finishedDate: null,
    paymentStatus: '',
    waitingForPayment: false,
    paymentExpiredOrOverdue: false,
    exchangeComplete: false,
    exchangeFinished: false,
    paymentReceived: false,
    paymentCompleted: false,
    createdDate: null,
    Id: '',
    notId: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerCore}
 */
class _ExchangeBrokerCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerCore} ‎
 */
class ExchangeBrokerCore extends newAbstract(
 _ExchangeBrokerCore,16830596657,ExchangeBrokerCoreConstructor,{
  asIExchangeBrokerCore:1,
  superExchangeBrokerCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerCore} */
ExchangeBrokerCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerCore} */
function ExchangeBrokerCoreClass(){}

export default ExchangeBrokerCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeBrokerOuterCore}
 */
function __ExchangeBrokerOuterCore() {}
__ExchangeBrokerOuterCore.prototype = /** @type {!_ExchangeBrokerOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ExchangeBrokerOuterCore} */
export function ExchangeBrokerOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeBrokerOuterCore.Model}*/
  this.model={
    creatingTransaction: false,
    checkingStatus: false,
    checkStatusError: false,
    createTransaction: false,
    checkPayment: false,
    checkPaymentError: '',
    address: '',
    extraId: '',
    refundAddress: '',
    refundExtraId: '',
    id: '',
    kycRequired: null,
    confirmedAmountFrom: '',
    status: '',
    processingPayment: false,
    createTransactionError: '',
    statusLastChecked: null,
    trackUrl: '',
    type: '',
    payinAddress: '',
    payinExtraId: '',
    payoutAddress: '',
    payoutExtraId: '',
    createdAt: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerOuterCore}
 */
class _ExchangeBrokerOuterCore { }
/**
 * The _IExchangeBroker_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerOuterCore} ‎
 */
export class ExchangeBrokerOuterCore extends newAbstract(
 _ExchangeBrokerOuterCore,16830596653,ExchangeBrokerOuterCoreConstructor,{
  asIExchangeBrokerOuterCore:1,
  superExchangeBrokerOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerOuterCore} */
ExchangeBrokerOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerOuterCore} */
function ExchangeBrokerOuterCoreClass(){}


ExchangeBrokerOuterCore[$implementations]=[
 __ExchangeBrokerOuterCore,
 ExchangeBrokerOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerOuterCore}*/({
  constructor(){
   mountPins(this.model,'',ExchangeBrokerMemoryPQs)
   mountPins(this.model,'',ExchangeBrokerCachePQs)
  },
 }),
]

ExchangeBrokerCore[$implementations]=[
 ExchangeBrokerCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerCore}*/({
  resetCore(){
   this.resetExchangeBrokerCore()
  },
  resetExchangeBrokerCore(){
   ExchangeBrokerCoreConstructor.call(
    /**@type {xyz.swapee.wc.ExchangeBrokerCore}*/(this),
   )
   ExchangeBrokerOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ExchangeBrokerOuterCore}*/(
     /**@type {!xyz.swapee.wc.IExchangeBrokerOuterCore}*/(this)),
   )
  },
 }),
 __ExchangeBrokerCore,
 ExchangeBrokerOuterCore,
]

export {ExchangeBrokerCore}