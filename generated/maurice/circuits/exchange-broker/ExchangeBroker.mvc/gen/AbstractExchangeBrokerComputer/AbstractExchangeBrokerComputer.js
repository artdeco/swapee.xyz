import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerComputer}
 */
function __AbstractExchangeBrokerComputer() {}
__AbstractExchangeBrokerComputer.prototype = /** @type {!_AbstractExchangeBrokerComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerComputer}
 */
class _AbstractExchangeBrokerComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerComputer} ‎
 */
export class AbstractExchangeBrokerComputer extends newAbstract(
 _AbstractExchangeBrokerComputer,16830596651,null,{
  asIExchangeBrokerComputer:1,
  superExchangeBrokerComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerComputer} */
AbstractExchangeBrokerComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerComputer} */
function AbstractExchangeBrokerComputerClass(){}


AbstractExchangeBrokerComputer[$implementations]=[
 __AbstractExchangeBrokerComputer,
 Adapter,
]


export default AbstractExchangeBrokerComputer