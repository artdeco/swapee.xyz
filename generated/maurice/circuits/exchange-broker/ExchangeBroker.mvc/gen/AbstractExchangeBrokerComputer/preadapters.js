
/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptPaymentReceived(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptPaymentReceived.Form}*/
 const _inputs={
  paymentReceived:inputs.paymentReceived,
  paymentStatus:inputs.paymentStatus,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.paymentStatus)||(__inputs.paymentReceived)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPaymentReceived(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptPaymentCompleted(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptPaymentCompleted.Form}*/
 const _inputs={
  paymentCompleted:inputs.paymentCompleted,
  paymentStatus:inputs.paymentStatus,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.paymentStatus)||(__inputs.paymentCompleted)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPaymentCompleted(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptCheckingPaymentStatus(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptCheckingPaymentStatus.Form}*/
 const _inputs={
  checkingStatus:inputs.checkingStatus,
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCheckingPaymentStatus(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptCheckingExchangeStatus(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptCheckingExchangeStatus.Form}*/
 const _inputs={
  checkingStatus:inputs.checkingStatus,
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCheckingExchangeStatus(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptFinishedDate(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptFinishedDate.Form}*/
 const _inputs={
  exchangeFinished:inputs.exchangeFinished,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptFinishedDate(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptWaitingForPayment(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptWaitingForPayment.Form}*/
 const _inputs={
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptWaitingForPayment(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptExchangeFinished(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptExchangeFinished.Form}*/
 const _inputs={
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptExchangeFinished(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptPaymentStatus(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptPaymentStatus.Form}*/
 const _inputs={
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPaymentStatus(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptProcessingPayment(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptProcessingPayment.Form}*/
 const _inputs={
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptProcessingPayment(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptCreatedDate(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptCreatedDate.Form}*/
 const _inputs={
  createdAt:inputs.createdAt,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCreatedDate(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptPaymentExpiredOrOverdue(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptPaymentExpiredOrOverdue.Form}*/
 const _inputs={
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptPaymentExpiredOrOverdue(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptExchangeComplete(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptExchangeComplete.Form}*/
 const _inputs={
  status:inputs.status,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptExchangeComplete(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptNotId(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptNotId.Form}*/
 const _inputs={
  id:inputs.id,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptNotId(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IExchangeBrokerComputer}*/
export function preadaptStatusLastChecked(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IExchangeBrokerComputer.adaptStatusLastChecked.Form}*/
 const _inputs={
  checkStatusError:inputs.checkStatusError,
  checkingStatus:inputs.checkingStatus,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((__inputs.checkStatusError)||(__inputs.checkingStatus)) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptStatusLastChecked(__inputs,__changes)
 return RET
}