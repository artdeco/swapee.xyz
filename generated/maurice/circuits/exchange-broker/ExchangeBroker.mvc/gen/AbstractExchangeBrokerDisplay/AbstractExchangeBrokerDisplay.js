import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerDisplay}
 */
function __AbstractExchangeBrokerDisplay() {}
__AbstractExchangeBrokerDisplay.prototype = /** @type {!_AbstractExchangeBrokerDisplay} */ ({ })
/** @this {xyz.swapee.wc.ExchangeBrokerDisplay} */ function ExchangeBrokerDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.Debug=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerDisplay}
 */
class _AbstractExchangeBrokerDisplay { }
/**
 * Display for presenting information from the _IExchangeBroker_.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerDisplay} ‎
 */
class AbstractExchangeBrokerDisplay extends newAbstract(
 _AbstractExchangeBrokerDisplay,168305966516,ExchangeBrokerDisplayConstructor,{
  asIExchangeBrokerDisplay:1,
  superExchangeBrokerDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerDisplay} */
AbstractExchangeBrokerDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerDisplay} */
function AbstractExchangeBrokerDisplayClass(){}

export default AbstractExchangeBrokerDisplay


AbstractExchangeBrokerDisplay[$implementations]=[
 __AbstractExchangeBrokerDisplay,
 Display,
 AbstractExchangeBrokerDisplayClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIExchangeBrokerScreen:{vdusPQs:{
    Debug:Debug,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    Debug:/**@type {HTMLDivElement}*/(children[Debug]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.ExchangeBrokerDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IExchangeBrokerDisplay.Initialese}*/({
   Debug:1,
  }),
  initializer({
   Debug:_Debug,
  }) {
   if(_Debug!==undefined) this.Debug=_Debug
  },
 }),
]