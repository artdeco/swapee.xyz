import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateExchangeBrokerCache=makeBuffers({
 checkingPaymentStatus:Boolean,
 checkingExchangeStatus:Boolean,
 finishedDate:Date,
 paymentStatus:String,
 waitingForPayment:Boolean,
 paymentExpiredOrOverdue:Boolean,
 exchangeComplete:Boolean,
 exchangeFinished:Boolean,
 paymentReceived:Boolean,
 paymentCompleted:Boolean,
 createdDate:Date,
 Id:String,
 notId:String,
},{silent:true})