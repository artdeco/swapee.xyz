import AbstractExchangeBrokerProcessor from '../AbstractExchangeBrokerProcessor'
import {ExchangeBrokerCore} from '../ExchangeBrokerCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractExchangeBrokerComputer} from '../AbstractExchangeBrokerComputer'
import {AbstractExchangeBrokerController} from '../AbstractExchangeBrokerController'
import {regulateExchangeBrokerCache} from './methods/regulateExchangeBrokerCache'
import {ExchangeBrokerCacheQPs} from '../../pqs/ExchangeBrokerCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBroker}
 */
function __AbstractExchangeBroker() {}
__AbstractExchangeBroker.prototype = /** @type {!_AbstractExchangeBroker} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBroker}
 */
class _AbstractExchangeBroker { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractExchangeBroker} ‎
 */
class AbstractExchangeBroker extends newAbstract(
 _AbstractExchangeBroker,16830596659,null,{
  asIExchangeBroker:1,
  superExchangeBroker:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBroker} */
AbstractExchangeBroker.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBroker} */
function AbstractExchangeBrokerClass(){}

export default AbstractExchangeBroker


AbstractExchangeBroker[$implementations]=[
 __AbstractExchangeBroker,
 ExchangeBrokerCore,
 AbstractExchangeBrokerProcessor,
 IntegratedComponent,
 AbstractExchangeBrokerComputer,
 AbstractExchangeBrokerController,
 AbstractExchangeBrokerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBroker}*/({
  regulateState:regulateExchangeBrokerCache,
  stateQPs:ExchangeBrokerCacheQPs,
 }),
]


export {AbstractExchangeBroker}