import AbstractExchangeBrokerGPU from '../AbstractExchangeBrokerGPU'
import AbstractExchangeBrokerScreenBack from '../AbstractExchangeBrokerScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {access} from '@mauriceguest/guest2'
import {ExchangeBrokerInputsQPs} from '../../pqs/ExchangeBrokerInputsQPs'
import { newAbstract, $implementations, create } from '@type.engineering/type-engineer'
import AbstractExchangeBroker from '../AbstractExchangeBroker'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerHtmlComponent}
 */
function __AbstractExchangeBrokerHtmlComponent() {}
__AbstractExchangeBrokerHtmlComponent.prototype = /** @type {!_AbstractExchangeBrokerHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerHtmlComponent}
 */
class _AbstractExchangeBrokerHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ExchangeBrokerHtmlComponent} */ (res)
  }
}
/**
 * The _IExchangeBroker_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerHtmlComponent} ‎
 */
export class AbstractExchangeBrokerHtmlComponent extends newAbstract(
 _AbstractExchangeBrokerHtmlComponent,168305966511,null,{
  asIExchangeBrokerHtmlComponent:1,
  superExchangeBrokerHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerHtmlComponent} */
AbstractExchangeBrokerHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerHtmlComponent} */
function AbstractExchangeBrokerHtmlComponentClass(){}


AbstractExchangeBrokerHtmlComponent[$implementations]=[
 __AbstractExchangeBrokerHtmlComponent,
 HtmlComponent,
 AbstractExchangeBroker,
 AbstractExchangeBrokerGPU,
 AbstractExchangeBrokerScreenBack,
 AbstractExchangeBrokerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerHtmlComponent}*/({
  inputsQPs:ExchangeBrokerInputsQPs,
 }),

/** @type {!AbstractExchangeBrokerHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IExchangeBrokerHtmlComponent}*/function paintDebug() {
   this.Debug.setText(this.model.id)
  }
 ] }),
 AbstractExchangeBrokerHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerHtmlComponent}*/({
  paint({createTransaction:createTransaction}){
   const{asIExchangeBrokerController:{
    unsetCreateTransactionError:unsetCreateTransactionError,
   }}=this
   if(createTransaction) {
    unsetCreateTransactionError()
   }
  },
 }),
]