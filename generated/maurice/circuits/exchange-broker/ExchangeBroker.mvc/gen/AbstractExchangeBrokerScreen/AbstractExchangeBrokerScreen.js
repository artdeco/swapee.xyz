import AbstractExchangeBrokerScreenAR from '../AbstractExchangeBrokerScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {ExchangeBrokerInputsPQs} from '../../pqs/ExchangeBrokerInputsPQs'
import {ExchangeBrokerMemoryQPs} from '../../pqs/ExchangeBrokerMemoryQPs'
import {ExchangeBrokerCacheQPs} from '../../pqs/ExchangeBrokerCacheQPs'
import {ExchangeBrokerVdusPQs} from '../../pqs/ExchangeBrokerVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerScreen}
 */
function __AbstractExchangeBrokerScreen() {}
__AbstractExchangeBrokerScreen.prototype = /** @type {!_AbstractExchangeBrokerScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerScreen}
 */
class _AbstractExchangeBrokerScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerScreen} ‎
 */
class AbstractExchangeBrokerScreen extends newAbstract(
 _AbstractExchangeBrokerScreen,168305966524,null,{
  asIExchangeBrokerScreen:1,
  superExchangeBrokerScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerScreen} */
AbstractExchangeBrokerScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerScreen} */
function AbstractExchangeBrokerScreenClass(){}

export default AbstractExchangeBrokerScreen


AbstractExchangeBrokerScreen[$implementations]=[
 __AbstractExchangeBrokerScreen,
 AbstractExchangeBrokerScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerScreen}*/({
  deduceInputs(){
   const{asIExchangeBrokerDisplay:{
    Debug:Debug,
   }}=this
   return{
    id:Debug?.innerText,
   }
  },
 }),
 AbstractExchangeBrokerScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerScreen}*/({
  inputsPQs:ExchangeBrokerInputsPQs,
  memoryQPs:ExchangeBrokerMemoryQPs,
  cacheQPs:ExchangeBrokerCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractExchangeBrokerScreenAR,
 AbstractExchangeBrokerScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerScreen}*/({
  vdusPQs:ExchangeBrokerVdusPQs,
 }),
]