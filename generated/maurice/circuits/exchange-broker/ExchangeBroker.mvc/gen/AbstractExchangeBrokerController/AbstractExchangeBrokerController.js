import ExchangeBrokerBuffer from '../ExchangeBrokerBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ExchangeBrokerPortConnector} from '../ExchangeBrokerPort'
import {defineEmitters} from '@mauriceguest/guest2'
import { newAbstract, $implementations, create } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerController}
 */
function __AbstractExchangeBrokerController() {}
__AbstractExchangeBrokerController.prototype = /** @type {!_AbstractExchangeBrokerController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerController}
 */
class _AbstractExchangeBrokerController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerController} ‎
 */
export class AbstractExchangeBrokerController extends newAbstract(
 _AbstractExchangeBrokerController,168305966519,null,{
  asIExchangeBrokerController:1,
  superExchangeBrokerController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerController} */
AbstractExchangeBrokerController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerController} */
function AbstractExchangeBrokerControllerClass(){}


AbstractExchangeBrokerController[$implementations]=[
 AbstractExchangeBrokerControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IExchangeBrokerPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractExchangeBrokerController,
 /**@type {!xyz.swapee.wc.IExchangeBrokerController}*/({
  calibrate:[
    /**@this {!xyz.swapee.wc.IExchangeBrokerController}*/ function calibrateCreateTransaction({createTransaction:createTransaction}){
     if(!createTransaction) return
     setTimeout(()=>{
      const{asIExchangeBrokerController:{setInputs:setInputs}}=this
      setInputs({
       createTransaction:false,
      })
     },1)
    },
    /**@this {!xyz.swapee.wc.IExchangeBrokerController}*/ function calibrateCheckPayment({checkPayment:checkPayment}){
     if(!checkPayment) return
     setTimeout(()=>{
      const{asIExchangeBrokerController:{setInputs:setInputs}}=this
      setInputs({
       checkPayment:false,
      })
     },1)
    },
  ],
 }),
 ExchangeBrokerBuffer,
 IntegratedController,
 /**@type {!AbstractExchangeBrokerController}*/(ExchangeBrokerPortConnector),
 AbstractExchangeBrokerControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerController}*/({
  constructor(){
   defineEmitters(this,{
    onReset:true,
   })
  },
 }),
 AbstractExchangeBrokerControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerController}*/({
  setCheckPaymentError(val){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({checkPaymentError:val})
  },
  setAddress(val){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({address:val})
  },
  setExtraId(val){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({extraId:val})
  },
  setRefundAddress(val){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({refundAddress:val})
  },
  setRefundExtraId(val){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({refundExtraId:val})
  },
  setCreateTransactionError(val){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({createTransactionError:val})
  },
  setStatusLastChecked(val){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({statusLastChecked:val})
  },
  unsetCheckPaymentError(){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({checkPaymentError:''})
  },
  unsetAddress(){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({address:''})
  },
  unsetExtraId(){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({extraId:''})
  },
  unsetRefundAddress(){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({refundAddress:''})
  },
  unsetRefundExtraId(){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({refundExtraId:''})
  },
  unsetCreateTransactionError(){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({createTransactionError:''})
  },
  unsetStatusLastChecked(){
   const{asIExchangeBrokerController:{setInputs:setInputs}}=this
   setInputs({statusLastChecked:null})
  },
 }),
 AbstractExchangeBrokerControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerController}*/({
  reset(){
   const{asIExchangeBrokerController:{onReset:onReset}}=this
   onReset()
  },
 }),
]


export default AbstractExchangeBrokerController