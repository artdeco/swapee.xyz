import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerScreenAR}
 */
function __AbstractExchangeBrokerScreenAR() {}
__AbstractExchangeBrokerScreenAR.prototype = /** @type {!_AbstractExchangeBrokerScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeBrokerScreenAR}
 */
class _AbstractExchangeBrokerScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeBrokerScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractExchangeBrokerScreenAR} ‎
 */
class AbstractExchangeBrokerScreenAR extends newAbstract(
 _AbstractExchangeBrokerScreenAR,168305966526,null,{
  asIExchangeBrokerScreenAR:1,
  superExchangeBrokerScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeBrokerScreenAR} */
AbstractExchangeBrokerScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeBrokerScreenAR} */
function AbstractExchangeBrokerScreenARClass(){}

export default AbstractExchangeBrokerScreenAR


AbstractExchangeBrokerScreenAR[$implementations]=[
 __AbstractExchangeBrokerScreenAR,
 AR,
 AbstractExchangeBrokerScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeBrokerScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractExchangeBrokerScreenAR}