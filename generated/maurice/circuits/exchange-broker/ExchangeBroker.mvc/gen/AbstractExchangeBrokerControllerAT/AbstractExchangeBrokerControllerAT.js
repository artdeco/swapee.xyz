import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerControllerAT}
 */
function __AbstractExchangeBrokerControllerAT() {}
__AbstractExchangeBrokerControllerAT.prototype = /** @type {!_AbstractExchangeBrokerControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeBrokerControllerAT}
 */
class _AbstractExchangeBrokerControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeBrokerControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractExchangeBrokerControllerAT} ‎
 */
class AbstractExchangeBrokerControllerAT extends newAbstract(
 _AbstractExchangeBrokerControllerAT,168305966523,null,{
  asIExchangeBrokerControllerAT:1,
  superExchangeBrokerControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeBrokerControllerAT} */
AbstractExchangeBrokerControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeBrokerControllerAT} */
function AbstractExchangeBrokerControllerATClass(){}

export default AbstractExchangeBrokerControllerAT


AbstractExchangeBrokerControllerAT[$implementations]=[
 __AbstractExchangeBrokerControllerAT,
 UartUniversal,
 AbstractExchangeBrokerControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeBrokerControllerAT}*/({
  get asIExchangeBrokerController(){
   return this
  },
  reset(){
   this.uart.t("inv",{mid:'70605'})
  },
  onReset(){
   this.uart.t("inv",{mid:'37334'})
  },
  setCheckPaymentError(){
   this.uart.t("inv",{mid:'250b2',args:[...arguments]})
  },
  unsetCheckPaymentError(){
   this.uart.t("inv",{mid:'d553e'})
  },
  setAddress(){
   this.uart.t("inv",{mid:'c73ca',args:[...arguments]})
  },
  unsetAddress(){
   this.uart.t("inv",{mid:'3f1d3'})
  },
  setExtraId(){
   this.uart.t("inv",{mid:'8a942',args:[...arguments]})
  },
  unsetExtraId(){
   this.uart.t("inv",{mid:'cc115'})
  },
  setRefundAddress(){
   this.uart.t("inv",{mid:'3e295',args:[...arguments]})
  },
  unsetRefundAddress(){
   this.uart.t("inv",{mid:'ff85d'})
  },
  setRefundExtraId(){
   this.uart.t("inv",{mid:'3567c',args:[...arguments]})
  },
  unsetRefundExtraId(){
   this.uart.t("inv",{mid:'317cb'})
  },
  setCreateTransactionError(){
   this.uart.t("inv",{mid:'6a351',args:[...arguments]})
  },
  unsetCreateTransactionError(){
   this.uart.t("inv",{mid:'ed2de'})
  },
  setStatusLastChecked(){
   this.uart.t("inv",{mid:'dc77f',args:[...arguments]})
  },
  unsetStatusLastChecked(){
   this.uart.t("inv",{mid:'9d643'})
  },
  pulseCreateTransaction(){
   this.uart.t("inv",{mid:'2e29f'})
  },
  pulseCheckPayment(){
   this.uart.t("inv",{mid:'b71b0'})
  },
 }),
]