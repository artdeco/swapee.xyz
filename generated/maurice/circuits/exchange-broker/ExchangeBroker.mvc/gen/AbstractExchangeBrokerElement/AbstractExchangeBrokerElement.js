import ExchangeBrokerRenderVdus from './methods/render-vdus'
import ExchangeBrokerElementPort from '../ExchangeBrokerElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {ExchangeBrokerInputsPQs} from '../../pqs/ExchangeBrokerInputsPQs'
import {ExchangeBrokerCachePQs} from '../../pqs/ExchangeBrokerCachePQs'
import { newAbstract, $implementations, create } from '@type.engineering/type-engineer'
import AbstractExchangeBroker from '../AbstractExchangeBroker'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerElement}
 */
function __AbstractExchangeBrokerElement() {}
__AbstractExchangeBrokerElement.prototype = /** @type {!_AbstractExchangeBrokerElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerElement}
 */
class _AbstractExchangeBrokerElement { }
/**
 * A component description.
 *
 * The _IExchangeBroker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerElement} ‎
 */
class AbstractExchangeBrokerElement extends newAbstract(
 _AbstractExchangeBrokerElement,168305966512,null,{
  asIExchangeBrokerElement:1,
  superExchangeBrokerElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerElement} */
AbstractExchangeBrokerElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerElement} */
function AbstractExchangeBrokerElementClass(){}

export default AbstractExchangeBrokerElement


AbstractExchangeBrokerElement[$implementations]=[
 __AbstractExchangeBrokerElement,
 ElementBase,
 AbstractExchangeBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':creating-transaction':creatingTransactionColAttr,
   ':checking-status':checkingStatusColAttr,
   ':check-status-error':checkStatusErrorColAttr,
   ':create-transaction':createTransactionColAttr,
   ':check-payment':checkPaymentColAttr,
   ':check-payment-error':checkPaymentErrorColAttr,
   ':address':addressColAttr,
   ':extra-id':extraIdColAttr,
   ':refund-address':refundAddressColAttr,
   ':refund-extra-id':refundExtraIdColAttr,
   ':id':idColAttr,
   ':kyc-required':kycRequiredColAttr,
   ':confirmed-amount-from':confirmedAmountFromColAttr,
   ':status':statusColAttr,
   ':processing-payment':processingPaymentColAttr,
   ':create-transaction-error':createTransactionErrorColAttr,
   ':status-last-checked':statusLastCheckedColAttr,
   ':track-url':trackUrlColAttr,
   ':type':typeColAttr,
   ':payin-address':payinAddressColAttr,
   ':payin-extra-id':payinExtraIdColAttr,
   ':payout-address':payoutAddressColAttr,
   ':payout-extra-id':payoutExtraIdColAttr,
   ':created-at':createdAtColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'creating-transaction':creatingTransactionAttr,
    'checking-status':checkingStatusAttr,
    'check-status-error':checkStatusErrorAttr,
    'create-transaction':createTransactionAttr,
    'check-payment':checkPaymentAttr,
    'check-payment-error':checkPaymentErrorAttr,
    'address':addressAttr,
    'extra-id':extraIdAttr,
    'refund-address':refundAddressAttr,
    'refund-extra-id':refundExtraIdAttr,
    'id':idAttr,
    'kyc-required':kycRequiredAttr,
    'confirmed-amount-from':confirmedAmountFromAttr,
    'status':statusAttr,
    'processing-payment':processingPaymentAttr,
    'create-transaction-error':createTransactionErrorAttr,
    'status-last-checked':statusLastCheckedAttr,
    'track-url':trackUrlAttr,
    'type':typeAttr,
    'payin-address':payinAddressAttr,
    'payin-extra-id':payinExtraIdAttr,
    'payout-address':payoutAddressAttr,
    'payout-extra-id':payoutExtraIdAttr,
    'created-at':createdAtAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(creatingTransactionAttr===undefined?{'creating-transaction':creatingTransactionColAttr}:{}),
    ...(checkingStatusAttr===undefined?{'checking-status':checkingStatusColAttr}:{}),
    ...(checkStatusErrorAttr===undefined?{'check-status-error':checkStatusErrorColAttr}:{}),
    ...(createTransactionAttr===undefined?{'create-transaction':createTransactionColAttr}:{}),
    ...(checkPaymentAttr===undefined?{'check-payment':checkPaymentColAttr}:{}),
    ...(checkPaymentErrorAttr===undefined?{'check-payment-error':checkPaymentErrorColAttr}:{}),
    ...(addressAttr===undefined?{'address':addressColAttr}:{}),
    ...(extraIdAttr===undefined?{'extra-id':extraIdColAttr}:{}),
    ...(refundAddressAttr===undefined?{'refund-address':refundAddressColAttr}:{}),
    ...(refundExtraIdAttr===undefined?{'refund-extra-id':refundExtraIdColAttr}:{}),
    ...(idAttr===undefined?{'id':idColAttr}:{}),
    ...(kycRequiredAttr===undefined?{'kyc-required':kycRequiredColAttr}:{}),
    ...(confirmedAmountFromAttr===undefined?{'confirmed-amount-from':confirmedAmountFromColAttr}:{}),
    ...(statusAttr===undefined?{'status':statusColAttr}:{}),
    ...(processingPaymentAttr===undefined?{'processing-payment':processingPaymentColAttr}:{}),
    ...(createTransactionErrorAttr===undefined?{'create-transaction-error':createTransactionErrorColAttr}:{}),
    ...(statusLastCheckedAttr===undefined?{'status-last-checked':statusLastCheckedColAttr}:{}),
    ...(trackUrlAttr===undefined?{'track-url':trackUrlColAttr}:{}),
    ...(typeAttr===undefined?{'type':typeColAttr}:{}),
    ...(payinAddressAttr===undefined?{'payin-address':payinAddressColAttr}:{}),
    ...(payinExtraIdAttr===undefined?{'payin-extra-id':payinExtraIdColAttr}:{}),
    ...(payoutAddressAttr===undefined?{'payout-address':payoutAddressColAttr}:{}),
    ...(payoutExtraIdAttr===undefined?{'payout-extra-id':payoutExtraIdColAttr}:{}),
    ...(createdAtAttr===undefined?{'created-at':createdAtColAttr}:{}),
   }
  },
 }),
 AbstractExchangeBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'creating-transaction':creatingTransactionAttr,
   'checking-status':checkingStatusAttr,
   'check-status-error':checkStatusErrorAttr,
   'create-transaction':createTransactionAttr,
   'check-payment':checkPaymentAttr,
   'check-payment-error':checkPaymentErrorAttr,
   'address':addressAttr,
   'extra-id':extraIdAttr,
   'refund-address':refundAddressAttr,
   'refund-extra-id':refundExtraIdAttr,
   'id':idAttr,
   'kyc-required':kycRequiredAttr,
   'confirmed-amount-from':confirmedAmountFromAttr,
   'status':statusAttr,
   'processing-payment':processingPaymentAttr,
   'create-transaction-error':createTransactionErrorAttr,
   'status-last-checked':statusLastCheckedAttr,
   'track-url':trackUrlAttr,
   'type':typeAttr,
   'payin-address':payinAddressAttr,
   'payin-extra-id':payinExtraIdAttr,
   'payout-address':payoutAddressAttr,
   'payout-extra-id':payoutExtraIdAttr,
   'created-at':createdAtAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    creatingTransaction:creatingTransactionAttr,
    checkingStatus:checkingStatusAttr,
    checkStatusError:checkStatusErrorAttr,
    createTransaction:createTransactionAttr,
    checkPayment:checkPaymentAttr,
    checkPaymentError:checkPaymentErrorAttr,
    address:addressAttr,
    extraId:extraIdAttr,
    refundAddress:refundAddressAttr,
    refundExtraId:refundExtraIdAttr,
    id:idAttr,
    kycRequired:kycRequiredAttr,
    confirmedAmountFrom:confirmedAmountFromAttr,
    status:statusAttr,
    processingPayment:processingPaymentAttr,
    createTransactionError:createTransactionErrorAttr,
    statusLastChecked:statusLastCheckedAttr,
    trackUrl:trackUrlAttr,
    type:typeAttr,
    payinAddress:payinAddressAttr,
    payinExtraId:payinExtraIdAttr,
    payoutAddress:payoutAddressAttr,
    payoutExtraId:payoutExtraIdAttr,
    createdAt:createdAtAttr,
   }
  },
 }),
 AbstractExchangeBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractExchangeBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerElement}*/({
  render:ExchangeBrokerRenderVdus,
 }),
 AbstractExchangeBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerElement}*/({
  inputsPQs:ExchangeBrokerInputsPQs,
  cachePQs:ExchangeBrokerCachePQs,
  vdus:{
   'Debug': 'b34f1',
  },
 }),
 IntegratedComponent,
 AbstractExchangeBrokerElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','creatingTransaction','checkingStatus','checkStatusError','createTransaction','checkPayment','checkPaymentError','address','extraId','refundAddress','refundExtraId','id','kycRequired','confirmedAmountFrom','status','processingPayment','createTransactionError','statusLastChecked','trackUrl','type','payinAddress','payinExtraId','payoutAddress','payoutExtraId','createdAt','no-solder',':no-solder','creating-transaction',':creating-transaction','checking-status',':checking-status','check-status-error',':check-status-error','create-transaction',':create-transaction','check-payment',':check-payment','check-payment-error',':check-payment-error',':address','extra-id',':extra-id','refund-address',':refund-address','refund-extra-id',':refund-extra-id',':id','kyc-required',':kyc-required','confirmed-amount-from',':confirmed-amount-from',':status','processing-payment',':processing-payment','create-transaction-error',':create-transaction-error','status-last-checked',':status-last-checked','track-url',':track-url',':type','payin-address',':payin-address','payin-extra-id',':payin-extra-id','payout-address',':payout-address','payout-extra-id',':payout-extra-id','created-at',':created-at','fe646','05bd2','89fc6','adfbe','64a41','cbfb1','c9266','afbad','884d9','79b82','be03a','dab97','b80bb','2b803','6103f','9acb4','355f3','601f8','43b98','079da','599dc','59af8','7a19b','16821','fbd75','97def','children']),
   })
  },
  get Port(){
   return ExchangeBrokerElementPort
  },
 }),
]



AbstractExchangeBrokerElement[$implementations]=[AbstractExchangeBroker,
 /** @type {!AbstractExchangeBrokerElement} */ ({
  rootId:'ExchangeBroker',
  __$id:1683059665,
  fqn:'xyz.swapee.wc.IExchangeBroker',
  maurice_element_v3:true,
 }),
]