
import AbstractExchangeBroker from '../AbstractExchangeBroker'

/** @abstract {xyz.swapee.wc.IExchangeBrokerElement} */
export default class AbstractExchangeBrokerElement { }



AbstractExchangeBrokerElement[$implementations]=[AbstractExchangeBroker,
 /** @type {!AbstractExchangeBrokerElement} */ ({
  rootId:'ExchangeBroker',
  __$id:1683059665,
  fqn:'xyz.swapee.wc.IExchangeBroker',
  maurice_element_v3:true,
 }),
]