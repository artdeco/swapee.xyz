import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeBrokerInputsPQs} from '../../pqs/ExchangeBrokerInputsPQs'
import {ExchangeBrokerOuterCoreConstructor} from '../ExchangeBrokerCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeBrokerPort}
 */
function __ExchangeBrokerPort() {}
__ExchangeBrokerPort.prototype = /** @type {!_ExchangeBrokerPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeBrokerPort} */ function ExchangeBrokerPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ExchangeBrokerOuterCore} */ ({model:null})
  ExchangeBrokerOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerPort}
 */
class _ExchangeBrokerPort { }
/**
 * The port that serves as an interface to the _IExchangeBroker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerPort} ‎
 */
export class ExchangeBrokerPort extends newAbstract(
 _ExchangeBrokerPort,16830596655,ExchangeBrokerPortConstructor,{
  asIExchangeBrokerPort:1,
  superExchangeBrokerPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerPort} */
ExchangeBrokerPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerPort} */
function ExchangeBrokerPortClass(){}

export const ExchangeBrokerPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IExchangeBroker.Pinout>}*/({
 get Port() { return ExchangeBrokerPort },
})

ExchangeBrokerPort[$implementations]=[
 ExchangeBrokerPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerPort}*/({
  resetPort(){
   this.resetExchangeBrokerPort()
  },
  resetExchangeBrokerPort(){
   ExchangeBrokerPortConstructor.call(this)
  },
 }),
 __ExchangeBrokerPort,
 Parametric,
 ExchangeBrokerPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerPort}*/({
  constructor(){
   mountPins(this.inputs,'',ExchangeBrokerInputsPQs)
  },
 }),
]


export default ExchangeBrokerPort