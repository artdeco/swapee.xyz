import {makeBuffers} from '@webcircuits/webcircuits'

export const ExchangeBrokerBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  creatingTransaction:Boolean,
  checkingStatus:Boolean,
  checkStatusError:Boolean,
  createTransaction:Boolean,
  checkPayment:Boolean,
  checkPaymentError:String,
  address:String,
  extraId:String,
  refundAddress:String,
  refundExtraId:String,
  id:String,
  kycRequired:Boolean,
  confirmedAmountFrom:String,
  status:String,
  processingPayment:Boolean,
  createTransactionError:String,
  statusLastChecked:Date,
  trackUrl:String,
  type:String,
  payinAddress:String,
  payinExtraId:String,
  payoutAddress:String,
  payoutExtraId:String,
  createdAt:String,
 }),
})

export default ExchangeBrokerBuffer