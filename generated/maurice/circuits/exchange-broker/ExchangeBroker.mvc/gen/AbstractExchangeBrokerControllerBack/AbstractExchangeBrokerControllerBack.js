import AbstractExchangeBrokerControllerAR from '../AbstractExchangeBrokerControllerAR'
import {AbstractExchangeBrokerController} from '../AbstractExchangeBrokerController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerControllerBack}
 */
function __AbstractExchangeBrokerControllerBack() {}
__AbstractExchangeBrokerControllerBack.prototype = /** @type {!_AbstractExchangeBrokerControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerController}
 */
class _AbstractExchangeBrokerControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeBrokerController} ‎
 */
class AbstractExchangeBrokerControllerBack extends newAbstract(
 _AbstractExchangeBrokerControllerBack,168305966521,null,{
  asIExchangeBrokerController:1,
  superExchangeBrokerController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerController} */
AbstractExchangeBrokerControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeBrokerController} */
function AbstractExchangeBrokerControllerBackClass(){}

export default AbstractExchangeBrokerControllerBack


AbstractExchangeBrokerControllerBack[$implementations]=[
 __AbstractExchangeBrokerControllerBack,
 AbstractExchangeBrokerController,
 AbstractExchangeBrokerControllerAR,
 DriverBack,
]