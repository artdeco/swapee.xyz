import AbstractExchangeBrokerDisplay from '../AbstractExchangeBrokerDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {ExchangeBrokerVdusPQs} from '../../pqs/ExchangeBrokerVdusPQs'
import {ExchangeBrokerVdusQPs} from '../../pqs/ExchangeBrokerVdusQPs'
import {ExchangeBrokerMemoryPQs} from '../../pqs/ExchangeBrokerMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeBrokerGPU}
 */
function __AbstractExchangeBrokerGPU() {}
__AbstractExchangeBrokerGPU.prototype = /** @type {!_AbstractExchangeBrokerGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerGPU}
 */
class _AbstractExchangeBrokerGPU { }
/**
 * Handles the periphery of the _IExchangeBrokerDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerGPU} ‎
 */
class AbstractExchangeBrokerGPU extends newAbstract(
 _AbstractExchangeBrokerGPU,168305966515,null,{
  asIExchangeBrokerGPU:1,
  superExchangeBrokerGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerGPU} */
AbstractExchangeBrokerGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerGPU} */
function AbstractExchangeBrokerGPUClass(){}

export default AbstractExchangeBrokerGPU


AbstractExchangeBrokerGPU[$implementations]=[
 __AbstractExchangeBrokerGPU,
 AbstractExchangeBrokerGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerGPU}*/({
  vdusPQs:ExchangeBrokerVdusPQs,
  vdusQPs:ExchangeBrokerVdusQPs,
  memoryPQs:ExchangeBrokerMemoryPQs,
 }),
 AbstractExchangeBrokerDisplay,
 BrowserView,
]