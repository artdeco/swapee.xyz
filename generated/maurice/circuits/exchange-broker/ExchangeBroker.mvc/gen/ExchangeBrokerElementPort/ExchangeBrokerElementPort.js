import { newAbstract, $implementations, create } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeBrokerElementPort}
 */
function __ExchangeBrokerElementPort() {}
__ExchangeBrokerElementPort.prototype = /** @type {!_ExchangeBrokerElementPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeBrokerElementPort} */ function ExchangeBrokerElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeBrokerElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    debugOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerElementPort}
 */
class _ExchangeBrokerElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerElementPort} ‎
 */
class ExchangeBrokerElementPort extends newAbstract(
 _ExchangeBrokerElementPort,168305966513,ExchangeBrokerElementPortConstructor,{
  asIExchangeBrokerElementPort:1,
  superExchangeBrokerElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerElementPort} */
ExchangeBrokerElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerElementPort} */
function ExchangeBrokerElementPortClass(){}

export default ExchangeBrokerElementPort


ExchangeBrokerElementPort[$implementations]=[
 __ExchangeBrokerElementPort,
 ExchangeBrokerElementPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeBrokerElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'debug-opts':undefined,
    'creating-transaction':undefined,
    'checking-status':undefined,
    'check-status-error':undefined,
    'create-transaction':undefined,
    'check-payment':undefined,
    'check-payment-error':undefined,
    'extra-id':undefined,
    'refund-address':undefined,
    'refund-extra-id':undefined,
    'kyc-required':undefined,
    'confirmed-amount-from':undefined,
    'processing-payment':undefined,
    'create-transaction-error':undefined,
    'status-last-checked':undefined,
    'track-url':undefined,
    'payin-address':undefined,
    'payin-extra-id':undefined,
    'payout-address':undefined,
    'payout-extra-id':undefined,
    'created-at':undefined,
   })
  },
 }),
]