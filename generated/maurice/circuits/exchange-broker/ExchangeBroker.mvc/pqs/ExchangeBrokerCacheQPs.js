import {ExchangeBrokerCachePQs} from './ExchangeBrokerCachePQs'
export const ExchangeBrokerCacheQPs=/**@type {!xyz.swapee.wc.ExchangeBrokerCacheQPs}*/(Object.keys(ExchangeBrokerCachePQs)
 .reduce((a,k)=>{a[ExchangeBrokerCachePQs[k]]=k;return a},{}))