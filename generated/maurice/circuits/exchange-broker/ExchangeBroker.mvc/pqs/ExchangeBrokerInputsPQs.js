import {ExchangeBrokerMemoryPQs} from './ExchangeBrokerMemoryPQs'
export const ExchangeBrokerInputsPQs=/**@type {!xyz.swapee.wc.ExchangeBrokerInputsQPs}*/({
 ...ExchangeBrokerMemoryPQs,
})