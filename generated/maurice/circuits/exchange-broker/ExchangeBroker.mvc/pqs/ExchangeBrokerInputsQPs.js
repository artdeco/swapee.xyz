import {ExchangeBrokerInputsPQs} from './ExchangeBrokerInputsPQs'
export const ExchangeBrokerInputsQPs=/**@type {!xyz.swapee.wc.ExchangeBrokerInputsQPs}*/(Object.keys(ExchangeBrokerInputsPQs)
 .reduce((a,k)=>{a[ExchangeBrokerInputsPQs[k]]=k;return a},{}))