import {ExchangeBrokerVdusPQs} from './ExchangeBrokerVdusPQs'
export const ExchangeBrokerVdusQPs=/**@type {!xyz.swapee.wc.ExchangeBrokerVdusQPs}*/(Object.keys(ExchangeBrokerVdusPQs)
 .reduce((a,k)=>{a[ExchangeBrokerVdusPQs[k]]=k;return a},{}))