import {ExchangeBrokerMemoryPQs} from './ExchangeBrokerMemoryPQs'
export const ExchangeBrokerMemoryQPs=/**@type {!xyz.swapee.wc.ExchangeBrokerMemoryQPs}*/(Object.keys(ExchangeBrokerMemoryPQs)
 .reduce((a,k)=>{a[ExchangeBrokerMemoryPQs[k]]=k;return a},{}))