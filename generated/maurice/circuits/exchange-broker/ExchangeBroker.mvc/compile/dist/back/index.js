/**
 * An abstract class of `xyz.swapee.wc.IExchangeBroker` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeBroker}
 */
class AbstractExchangeBroker extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeBroker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeBrokerPort}
 */
class ExchangeBrokerPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeBrokerController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerController}
 */
class AbstractExchangeBrokerController extends (class {/* lazy-loaded */}) {}
/**
 * The _IExchangeBroker_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ExchangeBrokerHtmlComponent}
 */
class ExchangeBrokerHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeBrokerBuffer}
 */
class ExchangeBrokerBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeBrokerComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerComputer}
 */
class AbstractExchangeBrokerComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ExchangeBrokerComputer}
 */
class ExchangeBrokerComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _IExchangeBroker_.
 * @extends {xyz.swapee.wc.ExchangeBrokerProcessor}
 */
class ExchangeBrokerProcessor extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ExchangeBrokerController}
 */
class ExchangeBrokerController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeBroker = AbstractExchangeBroker
module.exports.ExchangeBrokerPort = ExchangeBrokerPort
module.exports.AbstractExchangeBrokerController = AbstractExchangeBrokerController
module.exports.ExchangeBrokerHtmlComponent = ExchangeBrokerHtmlComponent
module.exports.ExchangeBrokerBuffer = ExchangeBrokerBuffer
module.exports.AbstractExchangeBrokerComputer = AbstractExchangeBrokerComputer
module.exports.ExchangeBrokerComputer = ExchangeBrokerComputer
module.exports.ExchangeBrokerProcessor = ExchangeBrokerProcessor
module.exports.ExchangeBrokerController = ExchangeBrokerController