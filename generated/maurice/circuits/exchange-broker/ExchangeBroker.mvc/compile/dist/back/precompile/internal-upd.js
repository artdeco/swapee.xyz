import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1683059665} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const d=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=d["372700389810"],e=d["372700389811"];function f(a,b,c,ha){return d["372700389812"](a,b,c,ha,!1,void 0)};function g(){}g.prototype={xa(){const {asIIntegratedController:{setInputs:a}}=this;a({j:!0})},wa(){const {asIIntegratedController:{setInputs:a}}=this;a({v:!0})}};class ba{}class h extends f(ba,16830596658,null,{ua:1,xb:2}){}h[e]=[g];
const k=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=k["61505580523"],l=k["615055805212"],da=k["615055805218"],ea=k["615055805221"],fa=k["615055805223"],m=k["615055805233"];const n={Y:"89fc6",j:"cbfb1",$a:"a38a0",v:"c9266",D:"afbad",ab:"96c88",Oa:"748e6",bb:"c23cd",address:"884d9",J:"79b82",L:"be03a",M:"dab97",id:"b80bb",Z:"2b803",X:"6103f",cb:"86cc4",status:"9acb4",V:"355f3",H:"601f8",nb:"f3dff",da:"079da",type:"599dc",T:"59af8",aa:"7a19b",ba:"16821",ca:"fbd75",Ma:"187ac",Na:"3648c",l:"97def",i:"adfbe",C:"43b98",A:"64a41",hb:"b370a",ib:"5f4a3",fixed:"cec31",gb:"2568a",jb:"341da",fb:"10a9a",kb:"5fd9c",mb:"82540",eb:"565af",lb:"96f44",Ab:"4685c"};const p={O:"490aa",R:"0b951",I:"272bc",m:"bb39a",K:"bd1a8",h:"b5666",W:"37b54",U:"b978a",P:"a99c4",G:"b0696",F:"6832e",o:"497c0",s:"39957"};function q(){}q.prototype={};function ia(){this.model={G:!1,F:!1,K:null,h:"",W:!1,U:!1,P:!1,m:!1,s:!1,o:!1,I:null,O:"",R:""}}class ja{}class r extends f(ja,16830596657,ia,{Sa:1,rb:2}){}function t(){}t.prototype={};function u(){this.model={Y:!1,i:!1,A:!1,j:!1,v:!1,D:"",address:"",J:"",L:"",M:"",id:"",Z:null,X:"",status:"",V:!1,H:"",C:null,da:"",type:"",T:"",aa:"",ba:"",ca:"",l:""}}class ka{}class v extends f(ka,16830596653,u,{Wa:1,vb:2}){}
v[e]=[t,{constructor(){m(this.model,"",n);m(this.model,"",p)}}];r[e]=[{},q,v];

const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=w.IntegratedController,ma=w.Parametric;
const x=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const na=x.defineEmitters,oa=x.IntegratedComponentInitialiser,pa=x.IntegratedComponent;function y(){}y.prototype={};class qa{}class z extends f(qa,16830596651,null,{Qa:1,pb:2}){}z[e]=[y,da];const A={regulate:l({Y:Boolean,i:Boolean,A:Boolean,j:Boolean,v:Boolean,D:String,address:String,J:String,L:String,M:String,id:String,Z:Boolean,X:String,status:String,V:Boolean,H:String,C:Date,da:String,type:String,T:String,aa:String,ba:String,ca:String,l:String})};const B={...n};function C(){}C.prototype={};function D(){const a={model:null};u.call(a);this.inputs=a.model}class ra{}class E extends f(ra,16830596655,D,{Xa:1,wb:2}){}function F(){}E[e]=[F.prototype={resetPort(){D.call(this)}},C,ma,F.prototype={constructor(){m(this.inputs,"",B)}}];function G(){}G.prototype={};class sa{}class H extends f(sa,168305966519,null,{g:1,Fa:2}){}function I(){}
H[e]=[I.prototype={resetPort(){this.port.resetPort()}},G,{calibrate:[function({j:a}){a&&setTimeout(()=>{const {g:{setInputs:b}}=this;b({j:!1})},1)},function({v:a}){a&&setTimeout(()=>{const {g:{setInputs:b}}=this;b({v:!1})},1)}]},A,la,{get Port(){return E}},I.prototype={constructor(){na(this,{$:!0})}},I.prototype={za(a){const {g:{setInputs:b}}=this;b({D:a})},ya(a){const {g:{setInputs:b}}=this;b({address:a})},Ba(a){const {g:{setInputs:b}}=this;b({J:a})},Ca(a){const {g:{setInputs:b}}=this;b({L:a})},
Da(a){const {g:{setInputs:b}}=this;b({M:a})},Aa(a){const {g:{setInputs:b}}=this;b({H:a})},Ea(a){const {g:{setInputs:b}}=this;b({C:a})},Ha(){const {g:{setInputs:a}}=this;a({D:""})},Ga(){const {g:{setInputs:a}}=this;a({address:""})},Ia(){const {g:{setInputs:a}}=this;a({J:""})},Ja(){const {g:{setInputs:a}}=this;a({L:""})},Ka(){const {g:{setInputs:a}}=this;a({M:""})},ea(){const {g:{setInputs:a}}=this;a({H:""})},La(){const {g:{setInputs:a}}=this;a({C:null})}},I.prototype={reset(){const {g:{$:a}}=this;
a()}}];const ta=l({G:Boolean,F:Boolean,K:Date,h:String,W:Boolean,U:Boolean,P:Boolean,m:Boolean,s:Boolean,o:Boolean,I:Date,O:String,R:String},{silent:!0});const ua=Object.keys(p).reduce((a,b)=>{a[p[b]]=b;return a},{});function J(){}J.prototype={};class va{}class K extends f(va,16830596659,null,{Pa:1,ob:2}){}K[e]=[J,r,h,pa,z,H,{regulateState:ta,stateQPs:ua}];
const L=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const wa=L["12817393923"],xa=L["12817393924"],ya=L["12817393925"],za=L["12817393926"];function M(){}M.prototype={};class Aa{}class N extends f(Aa,168305966522,null,{Ra:1,qb:2}){}N[e]=[M,za,{allocator(){this.methods={reset:"70605",$:"37334",za:"250b2",Ha:"d553e",ya:"c73ca",Ga:"3f1d3",Ba:"8a942",Ia:"cc115",Ca:"3e295",Ja:"ff85d",Da:"3567c",Ka:"317cb",Aa:"6a351",ea:"ed2de",Ea:"dc77f",La:"9d643",xa:"2e29f",wa:"b71b0"}}}];function O(){}O.prototype={};class Ba{}class P extends f(Ba,168305966521,null,{g:1,Fa:2}){}P[e]=[O,H,N,wa];var Q=class extends P.implements(){};function Ca({i:a,status:b}){return a?{G:["new","waiting","confirming"].includes(b)}:{G:!1}};function Da({i:a,status:b}){return a?{F:!["new","waiting","confirming"].includes(b)}:{F:!1}};function Ea({m:a}){return a?{K:new Date}:{K:null}};function Fa({status:a}){return{m:"finished"==a}};function Ga({status:a}){return["new","waiting"].includes(a)?{h:"waiting"}:"confirming"==a?{h:"confirming"}:"exchanging"==a||"sending"==a||"finished"==a?{h:"confirmed"}:"overdue"==a?{h:"overdue"}:"expired"==a?{h:"expired"}:{h:""}};function Ha({h:a}){return{s:["confirming","confirmed"].includes(a)}};function Ia({h:a}){return{o:"confirmed"==a}};function Ja({status:a}){return{W:["new","waiting"].includes(a)}};function Ka({status:a}){return{V:["confirming","exchanging","sending"].includes(a)}};function La({l:a}){return{I:new Date(a)}};function Ma({status:a}){return{P:["finished","failed","refunded","overdue","expired"].includes(a)}};function Na({status:a}){return{U:["overdue","expired"].includes(a)}};function Oa(a,{i:b}){if(b)return{C:new Date}};function Pa(a,b,c){a={s:a.s,h:a.h};a=c?c(a):a;if(a.h&&!a.s)return b=c?c(b):b,this.pa(a,b)}function Qa(a,b,c){a={o:a.o,h:a.h};a=c?c(a):a;if(a.h&&!a.o)return b=c?c(b):b,this.ma(a,b)}function Ra(a,b,c){a={i:a.i,status:a.status};a=c?c(a):a;b=c?c(b):b;return this.ga(a,b)}function Sa(a,b,c){a={i:a.i,status:a.status};a=c?c(a):a;b=c?c(b):b;return this.fa(a,b)}function Ta(a,b,c){a={m:a.m};a=c?c(a):a;b=c?c(b):b;return this.ka(a,b)}
function Ua(a,b,c){a={status:a.status};a=c?c(a):a;b=c?c(b):b;return this.ta(a,b)}function Va(a,b,c){a={status:a.status};a=c?c(a):a;b=c?c(b):b;return this.ja(a,b)}function Wa(a,b,c){a={status:a.status};a=c?c(a):a;b=c?c(b):b;return this.qa(a,b)}function Xa(a,b,c){a={status:a.status};a=c?c(a):a;b=c?c(b):b;return this.ra(a,b)}function Ya(a,b,c){a={l:a.l};a=c?c(a):a;b=c?c(b):b;return this.ha(a,b)}function Za(a,b,c){a={status:a.status};a=c?c(a):a;b=c?c(b):b;return this.oa(a,b)}
function $a(a,b,c){a={status:a.status};a=c?c(a):a;b=c?c(b):b;return this.ia(a,b)}function ab(a,b,c){a={A:a.A,i:a.i};a=c?c(a):a;if(!a.A&&!a.i)return b=c?c(b):b,this.sa(a,b)};const bb=z.__trait({la:function({id:a}){return{O:!!a,R:!a}},adapt:[function(a,b,c){a={id:a.id};a=c?c(a):a;b=c?c(b):b;return this.la(a,b)}]});class R extends z.implements(bb,{ga:Ca,fa:Da,ka:Ea,ja:Fa,qa:Ga,pa:Ha,ma:Ia,ta:Ja,ra:Ka,ha:La,ia:Ma,oa:Na,sa:Oa,adapt:[Ra,Sa,Ta,Va,Wa,Pa,Qa,Ua,Xa,Ya,$a,Za,ab]}){};const cb=h.__trait({reset:function(){const {g:{setInputs:a,resetPort:b},ua:{setInfo:c}}=this;c({I:null});a({status:"",id:"",l:"",T:""});b()}});var S=class extends h.implements(cb){};function T(){}T.prototype={};class db{}class U extends f(db,168305966518,null,{Ta:1,sb:2}){}U[e]=[T,xa,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{u:a})}},{[aa]:{u:1},initializer({u:a}){void 0!==a&&(this.u=a)}}];const V={u:"b34f1"};const eb=Object.keys(V).reduce((a,b)=>{a[V[b]]=b;return a},{});function W(){}W.prototype={};class fb{}class X extends f(fb,168305966515,null,{Ua:1,tb:2}){}X[e]=[W,{vdusQPs:eb,memoryPQs:n},U,ca];function Y(){}Y.prototype={};class gb{}class Z extends f(gb,168305966527,null,{Za:1,zb:2}){}Z[e]=[Y,ya];function hb(){}hb.prototype={};class ib{}class jb extends f(ib,168305966525,null,{Ya:1,yb:2}){}jb[e]=[hb,Z];const kb=Object.keys(B).reduce((a,b)=>{a[B[b]]=b;return a},{});function lb(){}lb.prototype={};class mb{static mvc(a,b,c){return fa(this,a,b,null,c)}}class nb extends f(mb,168305966511,null,{Va:1,ub:2}){}function ob(){}nb[e]=[lb,ea,K,X,jb,ob.prototype={inputsQPs:kb},{paint:[function(){this.u.setText(this.model.id)}]},ob.prototype={paint({j:a}){const {g:{ea:b}}=this;a&&b()}}];var pb=class extends nb.implements(Q,R,S,oa){};module.exports["16830596650"]=K;module.exports["16830596651"]=K;module.exports["16830596653"]=E;module.exports["16830596654"]=H;module.exports["168305966510"]=pb;module.exports["168305966511"]=A;module.exports["168305966530"]=z;module.exports["168305966531"]=R;module.exports["168305966551"]=S;module.exports["168305966561"]=Q;
/*! @embed-object-end {1683059665} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule