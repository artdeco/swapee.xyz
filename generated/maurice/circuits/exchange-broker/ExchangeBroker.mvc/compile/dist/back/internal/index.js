import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractExchangeBroker}*/
export class AbstractExchangeBroker extends Module['16830596651'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBroker} */
AbstractExchangeBroker.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeBrokerPort} */
export const ExchangeBrokerPort=Module['16830596653']
/**@extends {xyz.swapee.wc.AbstractExchangeBrokerController}*/
export class AbstractExchangeBrokerController extends Module['16830596654'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerController} */
AbstractExchangeBrokerController.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeBrokerHtmlComponent} */
export const ExchangeBrokerHtmlComponent=Module['168305966510']
/** @type {typeof xyz.swapee.wc.ExchangeBrokerBuffer} */
export const ExchangeBrokerBuffer=Module['168305966511']
/**@extends {xyz.swapee.wc.AbstractExchangeBrokerComputer}*/
export class AbstractExchangeBrokerComputer extends Module['168305966530'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeBrokerComputer} */
AbstractExchangeBrokerComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeBrokerComputer} */
export const ExchangeBrokerComputer=Module['168305966531']
/** @type {typeof xyz.swapee.wc.ExchangeBrokerProcessor} */
export const ExchangeBrokerProcessor=Module['168305966551']
/** @type {typeof xyz.swapee.wc.back.ExchangeBrokerController} */
export const ExchangeBrokerController=Module['168305966561']