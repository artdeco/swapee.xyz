/**
 * An abstract class of `xyz.swapee.wc.IExchangeBroker` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeBroker}
 */
class AbstractExchangeBroker extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeBroker_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeBrokerPort}
 */
class ExchangeBrokerPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeBrokerController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerController}
 */
class AbstractExchangeBrokerController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IExchangeBroker_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ExchangeBrokerElement}
 */
class ExchangeBrokerElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeBrokerBuffer}
 */
class ExchangeBrokerBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeBrokerComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeBrokerComputer}
 */
class AbstractExchangeBrokerComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ExchangeBrokerComputer}
 */
class ExchangeBrokerComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _IExchangeBroker_.
 * @extends {xyz.swapee.wc.ExchangeBrokerProcessor}
 */
class ExchangeBrokerProcessor extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ExchangeBrokerController}
 */
class ExchangeBrokerController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeBroker = AbstractExchangeBroker
module.exports.ExchangeBrokerPort = ExchangeBrokerPort
module.exports.AbstractExchangeBrokerController = AbstractExchangeBrokerController
module.exports.ExchangeBrokerElement = ExchangeBrokerElement
module.exports.ExchangeBrokerBuffer = ExchangeBrokerBuffer
module.exports.AbstractExchangeBrokerComputer = AbstractExchangeBrokerComputer
module.exports.ExchangeBrokerComputer = ExchangeBrokerComputer
module.exports.ExchangeBrokerProcessor = ExchangeBrokerProcessor
module.exports.ExchangeBrokerController = ExchangeBrokerController

Object.defineProperties(module.exports, {
 'AbstractExchangeBroker': {get: () => require('./precompile/internal')[16830596651]},
 [16830596651]: {get: () => module.exports['AbstractExchangeBroker']},
 'ExchangeBrokerPort': {get: () => require('./precompile/internal')[16830596653]},
 [16830596653]: {get: () => module.exports['ExchangeBrokerPort']},
 'AbstractExchangeBrokerController': {get: () => require('./precompile/internal')[16830596654]},
 [16830596654]: {get: () => module.exports['AbstractExchangeBrokerController']},
 'ExchangeBrokerElement': {get: () => require('./precompile/internal')[16830596658]},
 [16830596658]: {get: () => module.exports['ExchangeBrokerElement']},
 'ExchangeBrokerBuffer': {get: () => require('./precompile/internal')[168305966511]},
 [168305966511]: {get: () => module.exports['ExchangeBrokerBuffer']},
 'AbstractExchangeBrokerComputer': {get: () => require('./precompile/internal')[168305966530]},
 [168305966530]: {get: () => module.exports['AbstractExchangeBrokerComputer']},
 'ExchangeBrokerComputer': {get: () => require('./precompile/internal')[168305966531]},
 [168305966531]: {get: () => module.exports['ExchangeBrokerComputer']},
 'ExchangeBrokerProcessor': {get: () => require('./precompile/internal')[168305966551]},
 [168305966551]: {get: () => module.exports['ExchangeBrokerProcessor']},
 'ExchangeBrokerController': {get: () => require('./precompile/internal')[168305966561]},
 [168305966561]: {get: () => module.exports['ExchangeBrokerController']},
})