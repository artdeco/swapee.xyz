import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1683059665} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const e=c["372700389811"];function f(a,b,d,g){return c["372700389812"](a,b,d,g,!1,void 0)};function h(){}h.prototype={};class aa{}class k extends f(aa,16830596658,null,{ia:1,Ra:2}){}k[e]=[h];

const l=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const m=l["615055805212"],ba=l["615055805218"],n=l["615055805233"];const p={G:"89fc6",i:"cbfb1",ua:"a38a0",g:"c9266",s:"afbad",va:"96c88",ma:"748e6",wa:"c23cd",address:"884d9",H:"79b82",T:"be03a",U:"dab97",id:"b80bb",I:"2b803",A:"6103f",ya:"86cc4",status:"9acb4",R:"355f3",D:"601f8",Ia:"f3dff",W:"079da",type:"599dc",m:"59af8",K:"7a19b",M:"16821",P:"fbd75",ka:"187ac",la:"3648c",l:"97def",v:"adfbe",V:"43b98",u:"64a41",Ca:"b370a",Da:"5f4a3",fixed:"cec31",Ba:"2568a",Ea:"341da",Aa:"10a9a",Fa:"5fd9c",Ha:"82540",za:"565af",Ga:"96f44",Sa:"4685c"};const q={o:"490aa",J:"0b951",F:"272bc",$:"bb39a",aa:"bd1a8",fa:"b5666",ga:"37b54",da:"b978a",Z:"a99c4",Y:"b0696",X:"6832e",ba:"497c0",ea:"39957"};function r(){}r.prototype={};function ca(){this.model={Y:!1,X:!1,aa:null,fa:"",ga:!1,da:!1,Z:!1,$:!1,ea:!1,ba:!1,F:null,o:"",J:""}}class da{}class t extends f(da,16830596657,ca,{pa:1,Ma:2}){}function u(){}u.prototype={};function v(){this.model={G:!1,v:!1,u:!1,i:!1,g:!1,s:"",address:"",H:"",T:"",U:"",id:"",I:null,A:"",status:"",R:!1,D:"",V:null,W:"",type:"",m:"",K:"",M:"",P:"",l:""}}class ea{}class R extends f(ea,16830596653,v,{sa:1,Pa:2}){}
R[e]=[u,{constructor(){n(this.model,"",p);n(this.model,"",q)}}];t[e]=[{},r,R];
const S=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const fa=S.defineEmitters,ha=S.IntegratedComponentInitialiser,ia=S.IntegratedComponent,ja=S["95173443851"];function ka(){}ka.prototype={};class la{}class T extends f(la,16830596651,null,{oa:1,Ka:2}){}T[e]=[ka,ba];const ma={regulate:m({G:Boolean,v:Boolean,u:Boolean,i:Boolean,g:Boolean,s:String,address:String,H:String,T:String,U:String,id:String,I:Boolean,A:String,status:String,R:Boolean,D:String,V:Date,W:String,type:String,m:String,K:String,M:String,P:String,l:String})};
const na=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const oa=na.IntegratedController,pa=na.Parametric;const qa={...p};function ra(){}ra.prototype={};function sa(){const a={model:null};v.call(a);this.inputs=a.model}class ta{}class U extends f(ta,16830596655,sa,{ta:1,Qa:2}){}function ua(){}U[e]=[ua.prototype={},ra,pa,ua.prototype={constructor(){n(this.inputs,"",qa)}}];function va(){}va.prototype={};class wa{}class V extends f(wa,168305966519,null,{j:1,La:2}){}function W(){}V[e]=[W.prototype={},va,{calibrate:[function({i:a}){a&&setTimeout(()=>{const {j:{setInputs:b}}=this;b({i:!1})},1)},function({g:a}){a&&setTimeout(()=>{const {j:{setInputs:b}}=this;b({g:!1})},1)}]},ma,oa,{get Port(){return U}},W.prototype={constructor(){fa(this,{ja:!0})}},W.prototype={},W.prototype={reset(){const {j:{ja:a}}=this;a()}}];const xa=m({Y:Boolean,X:Boolean,aa:Date,fa:String,ga:Boolean,da:Boolean,Z:Boolean,$:Boolean,ea:Boolean,ba:Boolean,F:Date,o:String,J:String},{silent:!0});const ya=Object.keys(q).reduce((a,b)=>{a[q[b]]=b;return a},{});function za(){}za.prototype={};class Aa{}class X extends f(Aa,16830596659,null,{na:1,Ja:2}){}X[e]=[za,t,k,ia,T,V,{regulateState:xa,stateQPs:ya}];function Ba({address:a}){return{address:a}};const Ca=require(eval('"@type.engineering/web-computing"')).h;function Da(){return Ca("div",{$id:"ExchangeBroker"})};const Ea=require(eval('"@type.engineering/web-computing"')).h;function Fa({id:a}){return Ea("div",{$id:"ExchangeBroker"},Ea("pre",{$id:"Debug"},a))};var Ga=class extends V.implements(){};const Ha=T.__trait({ha:function({id:a}){return{o:!!a,J:!a}},adapt:[function(a,b,d){a={id:a.id};a=d?d(a):a;b=d?d(b):b;return this.ha(a,b)}]});class Ia extends T.implements(Ha){};const Ja=k.__trait({reset:function(){const {j:{setInputs:a},ia:{setInfo:b}}=this;b({F:null});a({status:"",id:"",l:"",m:""})}});var Ka=class extends k.implements(Ja){};require("https");require("http");const La=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}La("aqt");require("fs");require("child_process");
const Ma=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const Na=Ma.ElementBase,Oa=Ma.HTMLBlocker;const Pa=require(eval('"@type.engineering/web-computing"')).h;function Qa(){}Qa.prototype={};function Ra(){this.inputs={noSolder:!1,xa:{}}}class Sa{}class Ta extends f(Sa,168305966513,Ra,{ra:1,Oa:2}){}
Ta[e]=[Qa,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"debug-opts":void 0,"creating-transaction":void 0,"checking-status":void 0,"check-status-error":void 0,"create-transaction":void 0,"check-payment":void 0,"check-payment-error":void 0,"extra-id":void 0,"refund-address":void 0,"refund-extra-id":void 0,"kyc-required":void 0,"confirmed-amount-from":void 0,"processing-payment":void 0,"create-transaction-error":void 0,"status-last-checked":void 0,"track-url":void 0,
"payin-address":void 0,"payin-extra-id":void 0,"payout-address":void 0,"payout-extra-id":void 0,"created-at":void 0})}}];function Ua(){}Ua.prototype={};class Va{}class Y extends f(Va,168305966512,null,{qa:1,Na:2}){}function Z(){}
Y[e]=[Ua,Na,Z.prototype={calibrate:function({":no-solder":a,":creating-transaction":b,":checking-status":d,":check-status-error":g,":create-transaction":w,":check-payment":x,":check-payment-error":y,":address":z,":extra-id":A,":refund-address":B,":refund-extra-id":C,":id":D,":kyc-required":E,":confirmed-amount-from":F,":status":G,":processing-payment":H,":create-transaction-error":I,":status-last-checked":J,":track-url":K,":type":L,":payin-address":M,":payin-extra-id":N,":payout-address":O,":payout-extra-id":P,
":created-at":Q}){const {attributes:{"no-solder":Wa,"creating-transaction":Xa,"checking-status":Ya,"check-status-error":Za,"create-transaction":$a,"check-payment":ab,"check-payment-error":bb,address:cb,"extra-id":db,"refund-address":eb,"refund-extra-id":fb,id:gb,"kyc-required":hb,"confirmed-amount-from":ib,status:jb,"processing-payment":kb,"create-transaction-error":lb,"status-last-checked":mb,"track-url":nb,type:ob,"payin-address":pb,"payin-extra-id":qb,"payout-address":rb,"payout-extra-id":sb,"created-at":tb}}=
this;return{...(void 0===Wa?{"no-solder":a}:{}),...(void 0===Xa?{"creating-transaction":b}:{}),...(void 0===Ya?{"checking-status":d}:{}),...(void 0===Za?{"check-status-error":g}:{}),...(void 0===$a?{"create-transaction":w}:{}),...(void 0===ab?{"check-payment":x}:{}),...(void 0===bb?{"check-payment-error":y}:{}),...(void 0===cb?{address:z}:{}),...(void 0===db?{"extra-id":A}:{}),...(void 0===eb?{"refund-address":B}:{}),...(void 0===fb?{"refund-extra-id":C}:{}),...(void 0===gb?{id:D}:{}),...(void 0===
hb?{"kyc-required":E}:{}),...(void 0===ib?{"confirmed-amount-from":F}:{}),...(void 0===jb?{status:G}:{}),...(void 0===kb?{"processing-payment":H}:{}),...(void 0===lb?{"create-transaction-error":I}:{}),...(void 0===mb?{"status-last-checked":J}:{}),...(void 0===nb?{"track-url":K}:{}),...(void 0===ob?{type:L}:{}),...(void 0===pb?{"payin-address":M}:{}),...(void 0===qb?{"payin-extra-id":N}:{}),...(void 0===rb?{"payout-address":O}:{}),...(void 0===sb?{"payout-extra-id":P}:{}),...(void 0===tb?{"created-at":Q}:
{})}}},Z.prototype={calibrate:({"no-solder":a,"creating-transaction":b,"checking-status":d,"check-status-error":g,"create-transaction":w,"check-payment":x,"check-payment-error":y,address:z,"extra-id":A,"refund-address":B,"refund-extra-id":C,id:D,"kyc-required":E,"confirmed-amount-from":F,status:G,"processing-payment":H,"create-transaction-error":I,"status-last-checked":J,"track-url":K,type:L,"payin-address":M,"payin-extra-id":N,"payout-address":O,"payout-extra-id":P,"created-at":Q})=>({noSolder:a,
G:b,v:d,u:g,i:w,g:x,s:y,address:z,H:A,T:B,U:C,id:D,I:E,A:F,status:G,R:H,D:I,V:J,W:K,type:L,m:M,K:N,M:O,P,l:Q})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return Pa("div",{$id:"ExchangeBroker"},Pa("vdu",{$id:"Debug"}))}},Z.prototype={inputsPQs:qa,cachePQs:q,vdus:{Debug:"b34f1"}},ia,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder creatingTransaction checkingStatus checkStatusError createTransaction checkPayment checkPaymentError address extraId refundAddress refundExtraId id kycRequired confirmedAmountFrom status processingPayment createTransactionError statusLastChecked trackUrl type payinAddress payinExtraId payoutAddress payoutExtraId createdAt no-solder :no-solder creating-transaction :creating-transaction checking-status :checking-status check-status-error :check-status-error create-transaction :create-transaction check-payment :check-payment check-payment-error :check-payment-error :address extra-id :extra-id refund-address :refund-address refund-extra-id :refund-extra-id :id kyc-required :kyc-required confirmed-amount-from :confirmed-amount-from :status processing-payment :processing-payment create-transaction-error :create-transaction-error status-last-checked :status-last-checked track-url :track-url :type payin-address :payin-address payin-extra-id :payin-extra-id payout-address :payout-address payout-extra-id :payout-extra-id created-at :created-at fe646 05bd2 89fc6 adfbe 64a41 cbfb1 c9266 afbad 884d9 79b82 be03a dab97 b80bb 2b803 6103f 9acb4 355f3 601f8 43b98 079da 599dc 59af8 7a19b 16821 fbd75 97def children".split(" "))})},
get Port(){return Ta}}];Y[e]=[X,{rootId:"ExchangeBroker",__$id:1683059665,fqn:"xyz.swapee.wc.IExchangeBroker",maurice_element_v3:!0}];class ub extends Y.implements(Ga,Ia,Ka,ja,Oa,ha,{solder:Ba,server:Da,render:Fa},{classesMap:!0,rootSelector:".ExchangeBroker",stylesheet:"html/styles/ExchangeBroker.css",blockName:"html/ExchangeBrokerBlock.html"},{}){};module.exports["16830596650"]=X;module.exports["16830596651"]=X;module.exports["16830596653"]=U;module.exports["16830596654"]=V;module.exports["16830596658"]=ub;module.exports["168305966511"]=ma;module.exports["168305966530"]=T;module.exports["168305966531"]=Ia;module.exports["168305966551"]=Ka;module.exports["168305966561"]=Ga;
/*! @embed-object-end {1683059665} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule