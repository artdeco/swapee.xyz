/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const f=e["37270038985"],g=e["372700389810"],h=e["372700389811"];function l(a,b,k,B){return e["372700389812"](a,b,k,B,!1,void 0)};
const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const n=m["61893096584"],p=m["61893096586"],q=m["618930965811"],r=m["618930965812"],t=m["618930965815"],u=m["618930965819"];function v(){}v.prototype={};function w(){this.g=null}class x{}class y extends l(x,168305966516,w,{h:1,ya:2}){}y[h]=[v,n,{constructor(){f(this,()=>{this.scan({})})},scan:function(){const {element:a,i:{vdusPQs:{g:b}}}=this,k=t(a);Object.assign(this,{g:k[b]})}},{[g]:{g:1},initializer({g:a}){void 0!==a&&(this.g=a)}}];var z=class extends y.implements(){};function A(){}A.prototype={};class C{}class D extends l(C,168305966523,null,{s:1,xa:2}){}D[h]=[A,q,{reset(){this.uart.t("inv",{mid:"70605"})}}];function E(){}E.prototype={};class F{}class G extends l(F,168305966526,null,{u:1,Aa:2}){}G[h]=[E,r,{allocator(){this.methods={}}}];const H={O:"89fc6",J:"cbfb1",F:"a38a0",v:"c9266",A:"afbad",P:"96c88",o:"748e6",R:"c23cd",address:"884d9",X:"79b82",ta:"be03a",ua:"dab97",id:"b80bb",da:"2b803",I:"6103f",V:"86cc4",status:"9acb4",sa:"355f3",K:"601f8",ma:"f3dff",Ba:"079da",type:"599dc",ia:"59af8",ja:"7a19b",qa:"16821",ra:"fbd75",l:"187ac",m:"3648c",L:"97def",H:"adfbe",wa:"43b98",C:"64a41",aa:"b370a",ba:"5f4a3",fixed:"cec31",$:"2568a",ca:"341da",Z:"10a9a",ea:"5fd9c",ha:"82540",W:"565af",ga:"96f44",Ca:"4685c"};const I={...H};const J=Object.keys(H).reduce((a,b)=>{a[H[b]]=b;return a},{});const K={j:"490aa",fa:"0b951",M:"272bc",U:"bb39a",Y:"bd1a8",pa:"b5666",Da:"37b54",la:"b978a",T:"a99c4",G:"b0696",D:"6832e",ka:"497c0",oa:"39957"};const L=Object.keys(K).reduce((a,b)=>{a[K[b]]=b;return a},{});function M(){}M.prototype={};class N{}class O extends l(N,168305966524,null,{i:1,za:2}){}function P(){}O[h]=[M,P.prototype={deduceInputs(){const {h:{g:a}}=this;return{id:null==a?void 0:a.innerText}}},P.prototype={inputsPQs:I,memoryQPs:J,cacheQPs:L},p,u,G,P.prototype={vdusPQs:{g:"b34f1"}}];var Q=class extends O.implements(D,z,{get queries(){return this.settings}},{__$id:1683059665},{}){};module.exports["168305966541"]=z;module.exports["168305966571"]=Q;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1683059665']=module.exports