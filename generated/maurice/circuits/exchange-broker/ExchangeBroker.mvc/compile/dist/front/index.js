/**
 * Display for presenting information from the _IExchangeBroker_.
 * @extends {xyz.swapee.wc.ExchangeBrokerDisplay}
 */
class ExchangeBrokerDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ExchangeBrokerScreen}
 */
class ExchangeBrokerScreen extends (class {/* lazy-loaded */}) {}

module.exports.ExchangeBrokerDisplay = ExchangeBrokerDisplay
module.exports.ExchangeBrokerScreen = ExchangeBrokerScreen