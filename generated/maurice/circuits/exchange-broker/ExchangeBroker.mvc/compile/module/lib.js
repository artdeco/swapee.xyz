/**
 * Assigs lazy requires to the module.
 * @param {string} P The serial number of the package.
 * @param {!Iterable<number|Array<number,number>>} MODULES
 */
export default function assignModule(P,MODULES) {
 for(let n of MODULES) {
  let M,C
  if(Array.isArray(n)) {
   const[_,__]=n
   M=P+__
   C=_
  } else {
   M=P+n
   C=n
  }
  Object.defineProperty(module.exports,M,{
   get() {
    return require(eval(`'../${P}/c${C}'`))[M]
   },
  })
 }
}