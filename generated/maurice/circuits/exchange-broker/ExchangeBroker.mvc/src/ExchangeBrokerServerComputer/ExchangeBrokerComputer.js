import ExchangeBrokerSharedComputer from '../ExchangeBrokerSharedComputer'
import AbstractExchangeBrokerComputer from '../../gen/AbstractExchangeBrokerComputer'

/** @extends {xyz.swapee.wc.ExchangeBrokerComputer} */
export default class ExchangeBrokerServerComputer extends AbstractExchangeBrokerComputer.implements(
 ExchangeBrokerSharedComputer,
){}