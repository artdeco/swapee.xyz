import adaptNotId from './methods/adapt-not-id'
import {preadaptNotId} from '../../gen/AbstractExchangeBrokerComputer/preadapters'
import AbstractExchangeBrokerComputer from '../../gen/AbstractExchangeBrokerComputer'

const ExchangeBrokerSharedComputer=AbstractExchangeBrokerComputer.__trait(
 /** @type {!xyz.swapee.wc.IExchangeBrokerComputer} */ ({
  adaptNotId:adaptNotId,
  adapt:[preadaptNotId],
 }),
)
export default ExchangeBrokerSharedComputer