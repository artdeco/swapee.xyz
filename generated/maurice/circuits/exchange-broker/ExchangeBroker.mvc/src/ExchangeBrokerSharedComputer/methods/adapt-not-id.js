/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptNotId} */
export default function adaptNotId({id:id}) {
 return{
  Id:!!id,
  notId:!id,
 }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZXhjaGFuZ2UtYnJva2VyL2V4Y2hhbmdlLWJyb2tlci53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUF5VUcsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFO0NBQ25CLE1BQU07RUFDTCxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7RUFDUCxLQUFLLENBQUMsQ0FBQyxFQUFFO0NBQ1Y7QUFDRCJ9