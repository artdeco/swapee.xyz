/** @type {xyz.swapee.wc.IExchangeBrokerController._reset} */
export default function reset() { // todo: reset to specify which field to reset
 const{asIExchangeBrokerController:{setInputs:setInputs}}=/**@type {!xyz.swapee.wc.IExchangeBrokerController}*/(this)
 setInputs({
  status:'',id:'',createdAt:'',payinAddress:'',
  paymentStatus:'',
 })
}