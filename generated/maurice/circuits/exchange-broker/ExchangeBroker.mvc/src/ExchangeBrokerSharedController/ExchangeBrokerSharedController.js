import reset from './methods/reset'
import AbstractExchangeBrokerController from '../../gen/AbstractExchangeBrokerController'

const ExchangeBrokerSharedController=AbstractExchangeBrokerController.__trait(
 /** @type {!xyz.swapee.wc.IExchangeBrokerController} */ ({
  reset:reset,
 }),
)
export default ExchangeBrokerSharedController