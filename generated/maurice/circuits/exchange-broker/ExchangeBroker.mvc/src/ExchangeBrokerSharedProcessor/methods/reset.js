/** @type {xyz.swapee.wc.IExchangeBrokerProcessor._reset} */
export default function reset() { // todo: reset to specify which field to reset
 const{
  asIExchangeBrokerController:{setInputs:setInputs,resetPort},
  asIExchangeBrokerProcessor:{setInfo:setInfo},
 }=/**@type {!xyz.swapee.wc.IExchangeBrokerProcessor}*/(this)
 setInfo({createdDate:null})
 setInputs({
  status:'',id:'',createdAt:'',payinAddress:'',
 })
 resetPort()
}