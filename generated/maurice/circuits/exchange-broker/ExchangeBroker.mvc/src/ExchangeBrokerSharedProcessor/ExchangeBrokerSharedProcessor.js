import reset from './methods/reset'
import AbstractExchangeBrokerProcessor from '../../gen/AbstractExchangeBrokerProcessor'

const ExchangeBrokerSharedProcessor=AbstractExchangeBrokerProcessor.__trait(
 /** @type {!xyz.swapee.wc.IExchangeBrokerProcessor} */ ({
  reset:reset,
 }),
)
export default ExchangeBrokerSharedProcessor