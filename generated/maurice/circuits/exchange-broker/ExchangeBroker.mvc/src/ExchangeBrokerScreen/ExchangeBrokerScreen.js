import AbstractExchangeBrokerControllerAT from '../../gen/AbstractExchangeBrokerControllerAT'
import ExchangeBrokerDisplay from '../ExchangeBrokerDisplay'
import AbstractExchangeBrokerScreen from '../../gen/AbstractExchangeBrokerScreen'

/** @extends {xyz.swapee.wc.ExchangeBrokerScreen} */
export default class extends AbstractExchangeBrokerScreen.implements(
 AbstractExchangeBrokerControllerAT,
 ExchangeBrokerDisplay,
 /**@type {!xyz.swapee.wc.IExchangeBrokerScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IExchangeBrokerScreen} */ ({
  __$id:1683059665,
 }),
/**@type {!xyz.swapee.wc.IExchangeBrokerScreen}*/({
   // deduceInputs(el) {},
  }),
){}