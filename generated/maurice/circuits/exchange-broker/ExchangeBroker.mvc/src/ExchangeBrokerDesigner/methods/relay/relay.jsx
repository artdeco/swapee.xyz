
/**@type {xyz.swapee.wc.IExchangeBrokerDesigner._relay} */
export default function relay({This,ExchangeBroker:{unsetCreateTransactionError}}) {
 return (h(Fragment,{},
  h(This,{ onCreateTransactionHigh:
   unsetCreateTransactionError()
   })
 ))
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZXhjaGFuZ2UtYnJva2VyL2V4Y2hhbmdlLWJyb2tlci53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBMlhHLFNBQVMsS0FBSyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsMkJBQTJCLEVBQUU7Q0FDakUsT0FBTyxDQUFDLFlBQUM7RUFDUixTQUFNO0dBQ0wsMkJBQTJCLENBQUM7RUFDNUI7Q0FDRixDQUFrQztBQUNuQyxDQUFGIn0=