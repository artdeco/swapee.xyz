import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import ExchangeBrokerServerController from '../ExchangeBrokerServerController'
import ExchangeBrokerServerComputer from '../ExchangeBrokerServerComputer'
import ExchangeBrokerServerProcessor from '../ExchangeBrokerServerProcessor'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractExchangeBrokerElement from '../../gen/AbstractExchangeBrokerElement'

/** @extends {xyz.swapee.wc.ExchangeBrokerElement} */
export default class ExchangeBrokerElement extends AbstractExchangeBrokerElement.implements(
 ExchangeBrokerServerController,
 ExchangeBrokerServerComputer,
 ExchangeBrokerServerProcessor,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IExchangeBrokerElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IExchangeBrokerElement}*/({
   classesMap: true,
   rootSelector:     `.ExchangeBroker`,
   stylesheet:       'html/styles/ExchangeBroker.css',
   blockName:        'html/ExchangeBrokerBlock.html',
  }),
  /** @type {xyz.swapee.wc.IExchangeBrokerDesigner} */({
  }),
){}

// thank you for using web circuits
