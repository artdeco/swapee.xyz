/** @type {xyz.swapee.wc.IExchangeBrokerElement._relay} */
export default function relay({This,ExchangeBroker:{unsetCreateTransactionError}}) {
 return (<>
  <This onCreateTransactionHigh={
   unsetCreateTransactionError()
  } />
 </>)
}