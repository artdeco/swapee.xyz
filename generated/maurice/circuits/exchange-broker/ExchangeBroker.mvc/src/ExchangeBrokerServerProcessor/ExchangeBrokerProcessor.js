import ExchangeBrokerSharedProcessor from '../ExchangeBrokerSharedProcessor'
import AbstractExchangeBrokerProcessor from '../../gen/AbstractExchangeBrokerProcessor'

/** @extends {xyz.swapee.wc.ExchangeBrokerProcessor} */
export default class extends AbstractExchangeBrokerProcessor.implements(
 ExchangeBrokerSharedProcessor,
){}