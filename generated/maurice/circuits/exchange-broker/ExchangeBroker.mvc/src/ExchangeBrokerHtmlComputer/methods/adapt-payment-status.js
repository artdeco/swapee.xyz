/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptPaymentStatus} */
export default function adaptPaymentStatus({status:status}) {
 if(['new','waiting'].includes(status)) return {paymentStatus:'waiting'}
 if(status=='confirming') return {paymentStatus:'confirming'}

 if(status=='exchanging') return {paymentStatus:'confirmed'}
 if(status=='sending') return {paymentStatus:'confirmed'}
 if(status=='finished') return {paymentStatus:'confirmed'}

 if(status=='overdue') return {paymentStatus:'overdue'} // final state
 if(status=='expired') return {paymentStatus:'expired'} // final state

 return{paymentStatus:''}
}