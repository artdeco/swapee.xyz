/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptPaymentExpiredOrOverdue} */
export default function adaptPaymentExpiredOrOverdue({status:status}) {
 return{
  paymentExpiredOrOverdue:[
   'overdue','expired',
  ].includes(status),
 }
}