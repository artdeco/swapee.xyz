/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptCheckingPaymentStatus} */
export default function adaptCheckingPaymentStatus({checkingStatus:checkingStatus,status:status}) {
 if(!checkingStatus) return{checkingPaymentStatus:false}
 return{checkingPaymentStatus:['new','waiting','confirming'].includes(status)}
}