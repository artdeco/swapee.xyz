/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptProcessingPayment} */
export default function adaptProcessingPayment({status}) {
 return{
  processingPayment:[
   'confirming','exchanging','sending',
  ].includes(status),
 }
}