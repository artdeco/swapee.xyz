/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptWaitingForPayment} */
export default function adaptWaitingForPayment({status}) {
 return{
  waitingForPayment:[
   'new','waiting',
  ].includes(status),
 }
}