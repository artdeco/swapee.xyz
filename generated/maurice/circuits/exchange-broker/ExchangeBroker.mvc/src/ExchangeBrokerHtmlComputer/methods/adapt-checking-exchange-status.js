/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptCheckingExchangeStatus} */
export default function adaptCheckingExchangeStatus({checkingStatus:checkingStatus,status:status}) {
 if(!checkingStatus) return{checkingExchangeStatus:false}
 return{checkingExchangeStatus:!['new','waiting','confirming'].includes(status)}
}