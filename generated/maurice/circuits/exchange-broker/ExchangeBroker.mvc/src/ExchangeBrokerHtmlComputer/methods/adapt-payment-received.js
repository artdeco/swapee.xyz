/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptPaymentReceived} */
export default function adaptPaymentReceived({paymentStatus:paymentStatus}) {
 return{paymentReceived:['confirming','confirmed'].includes(paymentStatus)}
}