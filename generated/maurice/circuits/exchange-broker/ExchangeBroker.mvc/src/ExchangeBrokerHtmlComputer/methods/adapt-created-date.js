/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptCreatedDate} */
export default function adaptCreatedDate({createdAt:createdAt}) { // todo: automatic conversion between lambda functions
 return{
  createdDate:new Date(createdAt),
 }
}