/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptPaymentCompleted} */
export default function adaptPaymentCompleted({paymentStatus:paymentStatus}) {
 return{paymentCompleted:paymentStatus=='confirmed'}
}