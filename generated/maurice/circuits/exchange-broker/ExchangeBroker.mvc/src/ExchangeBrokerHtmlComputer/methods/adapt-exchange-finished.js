/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptExchangeFinished} */
export default function adaptExchangeFinished({status:status}) {
 return{exchangeFinished:status=='finished'}
}