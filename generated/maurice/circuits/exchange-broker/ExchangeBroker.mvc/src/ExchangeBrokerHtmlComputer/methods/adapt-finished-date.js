/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptFinishedDate} */
export default function adaptFinishedDate({exchangeFinished:exchangeFinished}) {
 if(!exchangeFinished)return{finishedDate:null}
 return{
  finishedDate:new Date,
 }
}