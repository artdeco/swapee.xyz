/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptExchangeComplete} */
export default function adaptExchangeComplete({status:status}) {
 return{
  exchangeComplete:[
   'finished','failed','refunded','overdue','expired',
  ].includes(status),
 }
}