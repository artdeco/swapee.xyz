/** @type {xyz.swapee.wc.IExchangeBrokerComputer._adaptStatusLastChecked} */
export default function adaptStatusLastChecked(_,{checkingStatus:prevCheckingPayment}) {
 if(!prevCheckingPayment) return
 // if(prevCheckStatusError) return
 return{
  statusLastChecked:new Date,
 }
}