import adaptCheckingPaymentStatus from './methods/adapt-checking-payment-status'
import adaptCheckingExchangeStatus from './methods/adapt-checking-exchange-status'
import adaptFinishedDate from './methods/adapt-finished-date'
import adaptExchangeFinished from './methods/adapt-exchange-finished'
import adaptPaymentStatus from './methods/adapt-payment-status'
import adaptPaymentReceived from './methods/adapt-payment-received'
import adaptPaymentCompleted from './methods/adapt-payment-completed'
import adaptWaitingForPayment from './methods/adapt-waiting-for-payment'
import adaptProcessingPayment from './methods/adapt-processing-payment'
import adaptCreatedDate from './methods/adapt-created-date'
import adaptExchangeComplete from './methods/adapt-exchange-complete'
import adaptPaymentExpiredOrOverdue from './methods/adapt-payment-expired-or-overdue'
import adaptStatusLastChecked from './methods/adapt-status-last-checked'
import ExchangeBrokerSharedComputer from '../ExchangeBrokerSharedComputer'
import {preadaptCheckingPaymentStatus,preadaptCheckingExchangeStatus,preadaptFinishedDate,preadaptExchangeFinished,preadaptPaymentStatus,preadaptPaymentReceived,preadaptPaymentCompleted,preadaptWaitingForPayment,preadaptProcessingPayment,preadaptCreatedDate,preadaptExchangeComplete,preadaptPaymentExpiredOrOverdue,preadaptStatusLastChecked} from '../../gen/AbstractExchangeBrokerComputer/preadapters'
import AbstractExchangeBrokerComputer from '../../gen/AbstractExchangeBrokerComputer'

/** @extends {xyz.swapee.wc.ExchangeBrokerComputer} */
export default class ExchangeBrokerHtmlComputer extends AbstractExchangeBrokerComputer.implements(
 ExchangeBrokerSharedComputer,
 /** @type {!xyz.swapee.wc.IExchangeBrokerComputer} */ ({
  adaptCheckingPaymentStatus:adaptCheckingPaymentStatus,
  adaptCheckingExchangeStatus:adaptCheckingExchangeStatus,
  adaptFinishedDate:adaptFinishedDate,
  adaptExchangeFinished:adaptExchangeFinished,
  adaptPaymentStatus:adaptPaymentStatus,
  adaptPaymentReceived:adaptPaymentReceived,
  adaptPaymentCompleted:adaptPaymentCompleted,
  adaptWaitingForPayment:adaptWaitingForPayment,
  adaptProcessingPayment:adaptProcessingPayment,
  adaptCreatedDate:adaptCreatedDate,
  adaptExchangeComplete:adaptExchangeComplete,
  adaptPaymentExpiredOrOverdue:adaptPaymentExpiredOrOverdue,
  adaptStatusLastChecked:adaptStatusLastChecked,
  adapt:[preadaptCheckingPaymentStatus,preadaptCheckingExchangeStatus,preadaptFinishedDate,preadaptExchangeFinished,preadaptPaymentStatus,preadaptPaymentReceived,preadaptPaymentCompleted,preadaptWaitingForPayment,preadaptProcessingPayment,preadaptCreatedDate,preadaptExchangeComplete,preadaptPaymentExpiredOrOverdue,preadaptStatusLastChecked],
 }),
){}