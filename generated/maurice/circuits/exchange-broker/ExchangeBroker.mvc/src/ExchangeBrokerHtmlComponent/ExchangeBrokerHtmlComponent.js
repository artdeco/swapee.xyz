import ExchangeBrokerHtmlController from '../ExchangeBrokerHtmlController'
import ExchangeBrokerHtmlComputer from '../ExchangeBrokerHtmlComputer'
import ExchangeBrokerHtmlProcessor from '../ExchangeBrokerHtmlProcessor'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractExchangeBrokerHtmlComponent} from '../../gen/AbstractExchangeBrokerHtmlComponent'

/** @extends {xyz.swapee.wc.ExchangeBrokerHtmlComponent} */
export default class extends AbstractExchangeBrokerHtmlComponent.implements(
 ExchangeBrokerHtmlController,
 ExchangeBrokerHtmlComputer,
 ExchangeBrokerHtmlProcessor,
 IntegratedComponentInitialiser,
){}