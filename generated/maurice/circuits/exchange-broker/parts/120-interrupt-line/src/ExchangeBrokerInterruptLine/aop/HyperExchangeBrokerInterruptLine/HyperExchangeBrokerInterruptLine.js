import AbstractHyperExchangeBrokerInterruptLine from '../../../../gen/AbstractExchangeBrokerInterruptLine/hyper/AbstractHyperExchangeBrokerInterruptLine'
import ExchangeBrokerInterruptLine from '../../ExchangeBrokerInterruptLine'
import ExchangeBrokerInterruptLineGeneralAspects from '../ExchangeBrokerInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperExchangeBrokerInterruptLine} */
export default class extends AbstractHyperExchangeBrokerInterruptLine
 .consults(
  ExchangeBrokerInterruptLineGeneralAspects,
 )
 .implements(
  ExchangeBrokerInterruptLine,
 )
{}