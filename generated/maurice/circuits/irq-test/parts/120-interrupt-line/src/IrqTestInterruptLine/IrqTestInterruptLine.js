import AbstractIrqTestInterruptLine from '../../gen/AbstractIrqTestInterruptLine'
import test from './methods/test'

/** @extends {xyz.swapee.wc.IrqTestInterruptLine} */
export default class extends AbstractIrqTestInterruptLine.implements(
 AbstractIrqTestInterruptLine.class.prototype=/**@type {xyz.swapee.wc.IrqTestInterruptLine}*/({
  test:test,
 }),
){}