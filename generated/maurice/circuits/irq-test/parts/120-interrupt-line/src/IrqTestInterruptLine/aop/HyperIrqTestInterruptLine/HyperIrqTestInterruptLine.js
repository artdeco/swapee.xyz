import AbstractHyperIrqTestInterruptLine from '../../../../gen/AbstractIrqTestInterruptLine/hyper/AbstractHyperIrqTestInterruptLine'
import IrqTestInterruptLine from '../../IrqTestInterruptLine'
import IrqTestInterruptLineGeneralAspects from '../IrqTestInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperIrqTestInterruptLine} */
export default class extends AbstractHyperIrqTestInterruptLine
 .consults(
  IrqTestInterruptLineGeneralAspects,
 )
 .implements(
  IrqTestInterruptLine,
 )
{}