import AbstractIrqTestInterruptLineAspects from '../../../../gen/AbstractIrqTestInterruptLine/aspects/AbstractIrqTestInterruptLineAspects'
import {SetInputs} from '@webcircuits/front'

/**@extends {xyz.swapee.wc.IrqTestInterruptLineAspects} */
export default class extends AbstractIrqTestInterruptLineAspects.continues(
 AbstractIrqTestInterruptLineAspects.class.prototype=/**@type {xyz.swapee.wc.IrqTestInterruptLineAspects}*/({
  afterTestReturns:SetInputs,
 }),
){}