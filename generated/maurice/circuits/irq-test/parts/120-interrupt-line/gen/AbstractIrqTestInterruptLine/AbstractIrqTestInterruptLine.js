import IrqTestInterruptLineAspectsInstaller from './aspects-installers/IrqTestInterruptLineAspectsInstaller'
import {SetInputs} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestInterruptLine}
 */
function __AbstractIrqTestInterruptLine() {}
__AbstractIrqTestInterruptLine.prototype = /** @type {!_AbstractIrqTestInterruptLine} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestInterruptLine}
 */
class _AbstractIrqTestInterruptLine { }
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @extends {xyz.swapee.wc.AbstractIrqTestInterruptLine} ‎
 */
class AbstractIrqTestInterruptLine extends newAbstract(
 _AbstractIrqTestInterruptLine,8,null,{
  asIIrqTestInterruptLine:1,
  superIrqTestInterruptLine:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLine} */
AbstractIrqTestInterruptLine.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLine} */
function AbstractIrqTestInterruptLineClass(){}

export default AbstractIrqTestInterruptLine


/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperIrqTestInterruptLine}
 */
function __AbstractHyperIrqTestInterruptLine() {}
__AbstractHyperIrqTestInterruptLine.prototype = /** @type {!_AbstractHyperIrqTestInterruptLine} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractHyperIrqTestInterruptLine}
 */
class _AbstractHyperIrqTestInterruptLine {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperIrqTestInterruptLine
    .clone({aspectsInstaller:IrqTestInterruptLineAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.wc.AbstractHyperIrqTestInterruptLine} ‎ */
export class AbstractHyperIrqTestInterruptLine extends newAbstract(
 _AbstractHyperIrqTestInterruptLine,7,null,{
  asIHyperIrqTestInterruptLine:1,
  superHyperIrqTestInterruptLine:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine} */
AbstractHyperIrqTestInterruptLine.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine} */
function AbstractHyperIrqTestInterruptLineClass(){}


AbstractHyperIrqTestInterruptLine[$implementations]=[
 __AbstractHyperIrqTestInterruptLine,
]

AbstractHyperIrqTestInterruptLine[$implementations]=[
 IrqTestInterruptLineAspectsInstaller,
 /** @type {!xyz.swapee.wc.IIrqTestInterruptLineAspects} */ (/** @type {!xyz.swapee.wc.IIrqTestInterruptLineAspectsPointcuts} */({
  afterTestReturns:SetInputs,
 })),
 AbstractIrqTestInterruptLine,
]

AbstractIrqTestInterruptLine[$implementations]=[
 __AbstractIrqTestInterruptLine,
]

export {AbstractIrqTestInterruptLine}