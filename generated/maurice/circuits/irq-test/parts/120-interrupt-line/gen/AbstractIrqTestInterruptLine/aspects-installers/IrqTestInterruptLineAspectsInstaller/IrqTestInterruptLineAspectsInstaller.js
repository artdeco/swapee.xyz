import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_IrqTestInterruptLineAspectsInstaller}
 */
function __IrqTestInterruptLineAspectsInstaller() {}
__IrqTestInterruptLineAspectsInstaller.prototype = /** @type {!_IrqTestInterruptLineAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller}
 */
class _IrqTestInterruptLineAspectsInstaller { }

_IrqTestInterruptLineAspectsInstaller.prototype[$advice]=__IrqTestInterruptLineAspectsInstaller

/** @extends {xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller} ‎ */
class IrqTestInterruptLineAspectsInstaller extends newAbstract(
 _IrqTestInterruptLineAspectsInstaller,4,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller} */
IrqTestInterruptLineAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller} */
function IrqTestInterruptLineAspectsInstallerClass(){}

export default IrqTestInterruptLineAspectsInstaller


IrqTestInterruptLineAspectsInstaller[$implementations]=[
 IrqTestInterruptLineAspectsInstallerClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller}*/({
  test(){
   this.beforeTest=1
   this.afterTest=2
   this.aroundTest=3
   this.afterTestThrows=4
   this.afterTestReturns=5
   this.afterTestCancels=7
  },
 }),
 __IrqTestInterruptLineAspectsInstaller,
]