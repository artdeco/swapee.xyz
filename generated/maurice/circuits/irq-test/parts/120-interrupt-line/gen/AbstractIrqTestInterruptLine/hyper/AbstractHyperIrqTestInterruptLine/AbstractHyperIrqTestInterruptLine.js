import IrqTestInterruptLineAspectsInstaller from '../../aspects-installers/IrqTestInterruptLineAspectsInstaller'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperIrqTestInterruptLine}
 */
function __AbstractHyperIrqTestInterruptLine() {}
__AbstractHyperIrqTestInterruptLine.prototype = /** @type {!_AbstractHyperIrqTestInterruptLine} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractHyperIrqTestInterruptLine}
 */
class _AbstractHyperIrqTestInterruptLine {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperIrqTestInterruptLine
    .clone({aspectsInstaller:IrqTestInterruptLineAspectsInstaller})
    .consults(...args)
  }
}
/** @extends {xyz.swapee.wc.AbstractHyperIrqTestInterruptLine} ‎ */
class AbstractHyperIrqTestInterruptLine extends newAbstract(
 _AbstractHyperIrqTestInterruptLine,7,null,{
  asIHyperIrqTestInterruptLine:1,
  superHyperIrqTestInterruptLine:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine} */
AbstractHyperIrqTestInterruptLine.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine} */
function AbstractHyperIrqTestInterruptLineClass(){}

export default AbstractHyperIrqTestInterruptLine


AbstractHyperIrqTestInterruptLine[$implementations]=[
 __AbstractHyperIrqTestInterruptLine,
]