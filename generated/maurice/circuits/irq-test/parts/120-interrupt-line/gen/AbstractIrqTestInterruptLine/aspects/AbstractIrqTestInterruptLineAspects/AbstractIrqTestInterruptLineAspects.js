import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestInterruptLineAspects}
 */
function __AbstractIrqTestInterruptLineAspects() {}
__AbstractIrqTestInterruptLineAspects.prototype = /** @type {!_AbstractIrqTestInterruptLineAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestInterruptLineAspects}
 */
class _AbstractIrqTestInterruptLineAspects { }
/**
 * The aspects of the *IIrqTestInterruptLine*.
 * @extends {xyz.swapee.wc.AbstractIrqTestInterruptLineAspects} ‎
 */
class AbstractIrqTestInterruptLineAspects extends newAspects(
 _AbstractIrqTestInterruptLineAspects,6,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspects} */
AbstractIrqTestInterruptLineAspects.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspects} */
function AbstractIrqTestInterruptLineAspectsClass(){}

export default AbstractIrqTestInterruptLineAspects


AbstractIrqTestInterruptLineAspects[$implementations]=[
 __AbstractIrqTestInterruptLineAspects,
]