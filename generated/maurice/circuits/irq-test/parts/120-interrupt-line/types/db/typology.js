/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice': {
  'id': 1,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice': {
  'id': 2,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel': {
  'id': 3,
  'symbols': {},
  'methods': {
   'beforeTest': 1,
   'afterTest': 2,
   'afterTestThrows': 3,
   'afterTestReturns': 4,
   'afterTestCancels': 5
  }
 },
 'xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller': {
  'id': 4,
  'symbols': {},
  'methods': {
   'test': 1
  }
 },
 'xyz.swapee.wc.BIrqTestInterruptLineAspects': {
  'id': 5,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IIrqTestInterruptLineAspects': {
  'id': 6,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IHyperIrqTestInterruptLine': {
  'id': 7,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IIrqTestInterruptLine': {
  'id': 8,
  'symbols': {},
  'methods': {
   'test': 1
  }
 }
})