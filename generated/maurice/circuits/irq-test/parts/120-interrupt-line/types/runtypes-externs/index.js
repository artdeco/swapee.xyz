/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLine.Initialese filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @record */
$xyz.swapee.wc.IIrqTestInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLine.Initialese} */
xyz.swapee.wc.IIrqTestInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineCaster filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @interface */
$xyz.swapee.wc.IIrqTestInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestInterruptLine} */
$xyz.swapee.wc.IIrqTestInterruptLineCaster.prototype.asIIrqTestInterruptLine
/** @type {!BoundIIrqTestTouchscreen} */
$xyz.swapee.wc.IIrqTestInterruptLineCaster.prototype.asIIrqTestTouchscreen
/** @type {!xyz.swapee.wc.BoundIrqTestInterruptLine} */
$xyz.swapee.wc.IIrqTestInterruptLineCaster.prototype.superIrqTestInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLineCaster}
 */
xyz.swapee.wc.IIrqTestInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.IIrqTestController}
 */
$xyz.swapee.wc.IIrqTestInterruptLine = function() {}
/** @return {(undefined|!xyz.swapee.wc.front.IrqTestInputs)} */
$xyz.swapee.wc.IIrqTestInterruptLine.prototype.test = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLine}
 */
xyz.swapee.wc.IIrqTestInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLine.Initialese>}
 */
$xyz.swapee.wc.IrqTestInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.IrqTestInterruptLine
/** @type {function(new: xyz.swapee.wc.IIrqTestInterruptLine)} */
xyz.swapee.wc.IrqTestInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.IrqTestInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.AbstractIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestInterruptLine}
 */
$xyz.swapee.wc.AbstractIrqTestInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestInterruptLine)} */
xyz.swapee.wc.AbstractIrqTestInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @interface */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._beforeTest|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._beforeTest>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice.prototype.beforeTest
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTest|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTest>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice.prototype.afterTest
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestThrows>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice.prototype.afterTestThrows
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestReturns>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice.prototype.afterTestReturns
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestCancels>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice.prototype.afterTestCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice}
 */
$xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice
/** @type {function(new: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice)} */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest<THIS>>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.beforeTest
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest<THIS>>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.afterTest
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows<THIS>>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.afterTestThrows
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns<THIS>>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.afterTestReturns
/** @type {(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels<THIS>>)} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.afterTestCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @template THIS
 * @extends {$xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice
/** @type {function(new: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>)} */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @interface */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.beforeTest = function(data) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTest = function(data) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTestThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTestReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTestCancels = function(data) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLineJoinpointModel filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.IrqTestInterruptLineJoinpointModel = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestInterruptLineJoinpointModel}
 */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModel
/** @type {function(new: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel)} */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModel.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.RecordIIrqTestInterruptLineJoinpointModel filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {{ beforeTest: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.beforeTest, afterTest: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTest, afterTestThrows: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestThrows, afterTestReturns: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestReturns, afterTestCancels: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestCancels }} */
xyz.swapee.wc.RecordIIrqTestInterruptLineJoinpointModel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BoundIIrqTestInterruptLineJoinpointModel filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.BoundIIrqTestInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundIIrqTestInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BoundIrqTestInterruptLineJoinpointModel filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestInterruptLineJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundIrqTestInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.beforeTest filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.beforeTest
/** @typedef {function(this: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel, !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._beforeTest
/** @typedef {typeof $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest

// nss:xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTest filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTest
/** @typedef {function(this: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel, !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTest
/** @typedef {typeof $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest

// nss:xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestThrows filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestThrows
/** @typedef {function(this: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel, !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestThrows
/** @typedef {typeof $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows

// nss:xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestReturns filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestReturns
/** @typedef {function(this: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel, !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestReturns
/** @typedef {typeof $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns

// nss:xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestCancels filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestCancels
/** @typedef {function(this: xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel, !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData=): void} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestCancels
/** @typedef {typeof $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels

// nss:xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @record */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese} */
xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller = function() {}
/** @type {number} */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.prototype.beforeTest
/** @type {number} */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.prototype.afterTest
/** @type {number} */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.prototype.afterTestThrows
/** @type {number} */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.prototype.afterTestReturns
/** @type {number} */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.prototype.afterTestCancels
/** @return {void} */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.prototype.test = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese>}
 */
$xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller, ...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
$xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller)} */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller|typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.__extend
/**
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller|typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.continues
/**
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller|typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLineAspectsInstallerConstructor filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller, ...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.IrqTestInterruptLineAspectsInstallerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.IrqTestInputs} value
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.IrqTestInputs} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.IrqTestInputs} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.IrqTestInputs} value
 * @return {?}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData}
 */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestInterruptLine} */
$xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster.prototype.asIIrqTestInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster<THIS>}
 */
xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BIrqTestInterruptLineAspects filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @interface
 * @extends {xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster<THIS>}
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.BIrqTestInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BIrqTestInterruptLineAspects<THIS>}
 */
xyz.swapee.wc.BIrqTestInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @record */
$xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese} */
xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestInterruptLineAspects
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @interface */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestInterruptLine} */
$xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster.prototype.asIIrqTestInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster}
 */
xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLineAspects filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster}
 * @extends {xyz.swapee.wc.BIrqTestInterruptLineAspects<!xyz.swapee.wc.IIrqTestInterruptLineAspects>}
 */
$xyz.swapee.wc.IIrqTestInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestInterruptLineAspects}
 */
xyz.swapee.wc.IIrqTestInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLineAspects filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese>}
 */
$xyz.swapee.wc.IrqTestInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.IrqTestInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.IIrqTestInterruptLineAspects, ...!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese)} */
xyz.swapee.wc.IrqTestInterruptLineAspects.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.IrqTestInterruptLineAspects.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.AbstractIrqTestInterruptLineAspects filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
$xyz.swapee.wc.AbstractIrqTestInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractIrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestInterruptLineAspects)} */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestInterruptLineAspects|typeof xyz.swapee.wc.IrqTestInterruptLineAspects)|(!xyz.swapee.wc.BIrqTestInterruptLineAspects|typeof xyz.swapee.wc.BIrqTestInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestInterruptLineAspects|typeof xyz.swapee.wc.IrqTestInterruptLineAspects)|(!xyz.swapee.wc.BIrqTestInterruptLineAspects|typeof xyz.swapee.wc.BIrqTestInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestInterruptLineAspects|typeof xyz.swapee.wc.IrqTestInterruptLineAspects)|(!xyz.swapee.wc.BIrqTestInterruptLineAspects|typeof xyz.swapee.wc.BIrqTestInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IrqTestInterruptLineAspectsConstructor filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestInterruptLineAspects, ...!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese)} */
xyz.swapee.wc.IrqTestInterruptLineAspectsConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestInterruptLine.Initialese}
 */
$xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese} */
xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IHyperIrqTestInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IHyperIrqTestInterruptLineCaster filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @interface */
$xyz.swapee.wc.IHyperIrqTestInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIHyperIrqTestInterruptLine} */
$xyz.swapee.wc.IHyperIrqTestInterruptLineCaster.prototype.asIHyperIrqTestInterruptLine
/** @type {!xyz.swapee.wc.BoundHyperIrqTestInterruptLine} */
$xyz.swapee.wc.IHyperIrqTestInterruptLineCaster.prototype.superHyperIrqTestInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperIrqTestInterruptLineCaster}
 */
xyz.swapee.wc.IHyperIrqTestInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IHyperIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IHyperIrqTestInterruptLineCaster}
 * @extends {xyz.swapee.wc.IIrqTestInterruptLine}
 */
$xyz.swapee.wc.IHyperIrqTestInterruptLine = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperIrqTestInterruptLine}
 */
xyz.swapee.wc.IHyperIrqTestInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.HyperIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese} init
 * @implements {xyz.swapee.wc.IHyperIrqTestInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese>}
 */
$xyz.swapee.wc.HyperIrqTestInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.HyperIrqTestInterruptLine
/** @type {function(new: xyz.swapee.wc.IHyperIrqTestInterruptLine, ...!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese)} */
xyz.swapee.wc.HyperIrqTestInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.HyperIrqTestInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.AbstractHyperIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
$xyz.swapee.wc.AbstractHyperIrqTestInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractHyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractHyperIrqTestInterruptLine)} */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IHyperIrqTestInterruptLine|typeof xyz.swapee.wc.HyperIrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.IHyperIrqTestInterruptLine|typeof xyz.swapee.wc.HyperIrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.IHyperIrqTestInterruptLine|typeof xyz.swapee.wc.HyperIrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.__trait
/**
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLineAspects|function(new: xyz.swapee.wc.IIrqTestInterruptLineAspects))} aides
 * @return {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.consults

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.HyperIrqTestInterruptLineConstructor filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {function(new: xyz.swapee.wc.IHyperIrqTestInterruptLine, ...!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese)} */
xyz.swapee.wc.HyperIrqTestInterruptLineConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.RecordIHyperIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIHyperIrqTestInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BoundIHyperIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIHyperIrqTestInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IHyperIrqTestInterruptLineCaster}
 */
$xyz.swapee.wc.BoundIHyperIrqTestInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIHyperIrqTestInterruptLine} */
xyz.swapee.wc.BoundIHyperIrqTestInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BoundHyperIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIHyperIrqTestInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundHyperIrqTestInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundHyperIrqTestInterruptLine} */
xyz.swapee.wc.BoundHyperIrqTestInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.RecordIIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {{ test: xyz.swapee.wc.IIrqTestInterruptLine.test }} */
xyz.swapee.wc.RecordIIrqTestInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BoundIIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.BoundIIrqTestController}
 */
$xyz.swapee.wc.BoundIIrqTestInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestInterruptLine} */
xyz.swapee.wc.BoundIIrqTestInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.BoundIrqTestInterruptLine filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestInterruptLine} */
xyz.swapee.wc.BoundIrqTestInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} xyz.swapee.wc.IIrqTestInterruptLine.test filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.IrqTestInputs)}
 */
$xyz.swapee.wc.IIrqTestInterruptLine.__test = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.IrqTestInputs)} */
xyz.swapee.wc.IIrqTestInterruptLine.test
/** @typedef {function(this: xyz.swapee.wc.IIrqTestInterruptLine): (undefined|!xyz.swapee.wc.front.IrqTestInputs)} */
xyz.swapee.wc.IIrqTestInterruptLine._test
/** @typedef {typeof $xyz.swapee.wc.IIrqTestInterruptLine.__test} */
xyz.swapee.wc.IIrqTestInterruptLine.__test

// nss:xyz.swapee.wc.IIrqTestInterruptLine,$xyz.swapee.wc.IIrqTestInterruptLine,xyz.swapee.wc
/* @typal-end */