/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IIrqTestInterruptLine={}
xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller={}
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel={}
xyz.swapee.wc.IIrqTestInterruptLineAspects={}
xyz.swapee.wc.IHyperIrqTestInterruptLine={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/parts/120-interrupt-line/types/design/120-IIrqTestInterruptLine.xml} filter:!ControllerPlugin~props 3c26a3cd02bd0cde0ee1a7d1041a847e */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IIrqTestInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestInterruptLine)} xyz.swapee.wc.AbstractIrqTestInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestInterruptLine} xyz.swapee.wc.IrqTestInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestInterruptLine
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractIrqTestInterruptLine.constructor&xyz.swapee.wc.IrqTestInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractIrqTestInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLine.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeTest=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._beforeTest|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._beforeTest>} */ (void 0)
    /**
     * After the method.
     */
    this.afterTest=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTest|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTest>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterTestThrows=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterTestReturns=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterTestCancels=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestCancels>} */ (void 0)
  }
}
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice

/**
 * A concrete class of _IIrqTestInterruptLineJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice = class extends xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelHyperslice { }
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.IrqTestInterruptLineJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeTest=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterTest=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterTestThrows=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterTestReturns=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterTestCancels=/** @type {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice

/**
 * A concrete class of _IIrqTestInterruptLineJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice = class extends xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice { }
xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.IrqTestInterruptLineJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IIrqTestInterruptLine`'s methods.
 * @interface xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel = class { }
/** @type {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.beforeTest} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.beforeTest = function() {}
/** @type {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTest} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTest = function() {}
/** @type {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestThrows} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTestThrows = function() {}
/** @type {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestReturns} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTestReturns = function() {}
/** @type {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestCancels} */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.prototype.afterTestCancels = function() {}

/**
 * A concrete class of _IIrqTestInterruptLineJoinpointModel_ instances.
 * @constructor xyz.swapee.wc.IrqTestInterruptLineJoinpointModel
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel} An interface that enumerates the joinpoints of `IIrqTestInterruptLine`'s methods.
 */
xyz.swapee.wc.IrqTestInterruptLineJoinpointModel = class extends xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel { }
xyz.swapee.wc.IrqTestInterruptLineJoinpointModel.prototype.constructor = xyz.swapee.wc.IrqTestInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel} */
xyz.swapee.wc.RecordIIrqTestInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel} xyz.swapee.wc.BoundIIrqTestInterruptLineJoinpointModel */

/** @typedef {xyz.swapee.wc.IrqTestInterruptLineJoinpointModel} xyz.swapee.wc.BoundIrqTestInterruptLineJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller)} xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller} xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.prototype.constructor = xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller|typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller|typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller|typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese[]) => xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller} xyz.swapee.wc.IrqTestInterruptLineAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller */
xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeTest=/** @type {number} */ (void 0)
    this.afterTest=/** @type {number} */ (void 0)
    this.afterTestThrows=/** @type {number} */ (void 0)
    this.afterTestReturns=/** @type {number} */ (void 0)
    this.afterTestCancels=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {void}
   */
  test() { }
}
/**
 * Create a new *IIrqTestInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese>)} xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller} xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IIrqTestInterruptLineAspectsInstaller_ instances.
 * @constructor xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestInterruptLineAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.IrqTestInterruptLineAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `test` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.IrqTestInputs) => void} sub Cancels a call to `test` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData&xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData
 * @prop {!xyz.swapee.wc.front.IrqTestInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData&xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `test` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData&xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData
 * @prop {!xyz.swapee.wc.front.IrqTestInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.IrqTestInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData&xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData&xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.TestPointcutData} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {function(new: xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster<THIS>&xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.wc.BIrqTestInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IIrqTestInterruptLine* that bind to an instance.
 * @interface xyz.swapee.wc.BIrqTestInterruptLineAspects
 * @template THIS
 */
xyz.swapee.wc.BIrqTestInterruptLineAspects = class extends /** @type {xyz.swapee.wc.BIrqTestInterruptLineAspects.constructor&xyz.swapee.wc.IIrqTestInterruptLineJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.wc.BIrqTestInterruptLineAspects.prototype.constructor = xyz.swapee.wc.BIrqTestInterruptLineAspects

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestInterruptLineAspects)} xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestInterruptLineAspects} xyz.swapee.wc.IrqTestInterruptLineAspects.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestInterruptLineAspects` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestInterruptLineAspects
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects = class extends /** @type {xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.constructor&xyz.swapee.wc.IrqTestInterruptLineAspects.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.prototype.constructor = xyz.swapee.wc.AbstractIrqTestInterruptLineAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLineAspects|typeof xyz.swapee.wc.IrqTestInterruptLineAspects)|(!xyz.swapee.wc.BIrqTestInterruptLineAspects|typeof xyz.swapee.wc.BIrqTestInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLineAspects|typeof xyz.swapee.wc.IrqTestInterruptLineAspects)|(!xyz.swapee.wc.BIrqTestInterruptLineAspects|typeof xyz.swapee.wc.BIrqTestInterruptLineAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestInterruptLineAspects|typeof xyz.swapee.wc.IrqTestInterruptLineAspects)|(!xyz.swapee.wc.BIrqTestInterruptLineAspects|typeof xyz.swapee.wc.BIrqTestInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.AbstractIrqTestInterruptLineAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese[]) => xyz.swapee.wc.IIrqTestInterruptLineAspects} xyz.swapee.wc.IrqTestInterruptLineAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster&xyz.swapee.wc.BIrqTestInterruptLineAspects<!xyz.swapee.wc.IIrqTestInterruptLineAspects>)} xyz.swapee.wc.IIrqTestInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.BIrqTestInterruptLineAspects} xyz.swapee.wc.BIrqTestInterruptLineAspects.typeof */
/**
 * The aspects of the *IIrqTestInterruptLine*.
 * @interface xyz.swapee.wc.IIrqTestInterruptLineAspects
 */
xyz.swapee.wc.IIrqTestInterruptLineAspects = class extends /** @type {xyz.swapee.wc.IIrqTestInterruptLineAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.BIrqTestInterruptLineAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestInterruptLineAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestInterruptLineAspects&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese>)} xyz.swapee.wc.IrqTestInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineAspects} xyz.swapee.wc.IIrqTestInterruptLineAspects.typeof */
/**
 * A concrete class of _IIrqTestInterruptLineAspects_ instances.
 * @constructor xyz.swapee.wc.IrqTestInterruptLineAspects
 * @implements {xyz.swapee.wc.IIrqTestInterruptLineAspects} The aspects of the *IIrqTestInterruptLine*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestInterruptLineAspects = class extends /** @type {xyz.swapee.wc.IrqTestInterruptLineAspects.constructor&xyz.swapee.wc.IIrqTestInterruptLineAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestInterruptLineAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestInterruptLineAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLineAspects}
 */
xyz.swapee.wc.IrqTestInterruptLineAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BIrqTestInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster
 * @template THIS
 */
xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster = class { }
/**
 * Cast the _BIrqTestInterruptLineAspects_ instance into the _BoundIIrqTestInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestInterruptLine}
 */
xyz.swapee.wc.BIrqTestInterruptLineAspectsCaster.prototype.asIIrqTestInterruptLine

/**
 * Contains getters to cast the _IIrqTestInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster
 */
xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster = class { }
/**
 * Cast the _IIrqTestInterruptLineAspects_ instance into the _BoundIIrqTestInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestInterruptLine}
 */
xyz.swapee.wc.IIrqTestInterruptLineAspectsCaster.prototype.asIIrqTestInterruptLine

/** @typedef {xyz.swapee.wc.IIrqTestInterruptLine.Initialese} xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.HyperIrqTestInterruptLine)} xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.HyperIrqTestInterruptLine} xyz.swapee.wc.HyperIrqTestInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IHyperIrqTestInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractHyperIrqTestInterruptLine
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.constructor&xyz.swapee.wc.HyperIrqTestInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractHyperIrqTestInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IHyperIrqTestInterruptLine|typeof xyz.swapee.wc.HyperIrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IHyperIrqTestInterruptLine|typeof xyz.swapee.wc.HyperIrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IHyperIrqTestInterruptLine|typeof xyz.swapee.wc.HyperIrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestInterruptLine|typeof xyz.swapee.wc.IrqTestInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.wc.IIrqTestInterruptLineAspects|function(new: xyz.swapee.wc.IIrqTestInterruptLineAspects)} aides The list of aides that advise the IIrqTestInterruptLine to implement aspects.
 * @return {typeof xyz.swapee.wc.AbstractHyperIrqTestInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperIrqTestInterruptLine.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese[]) => xyz.swapee.wc.IHyperIrqTestInterruptLine} xyz.swapee.wc.HyperIrqTestInterruptLineConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IHyperIrqTestInterruptLineCaster&xyz.swapee.wc.IIrqTestInterruptLine)} xyz.swapee.wc.IHyperIrqTestInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLine} xyz.swapee.wc.IIrqTestInterruptLine.typeof */
/** @interface xyz.swapee.wc.IHyperIrqTestInterruptLine */
xyz.swapee.wc.IHyperIrqTestInterruptLine = class extends /** @type {xyz.swapee.wc.IHyperIrqTestInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IIrqTestInterruptLine.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperIrqTestInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IHyperIrqTestInterruptLine.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IHyperIrqTestInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese>)} xyz.swapee.wc.HyperIrqTestInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IHyperIrqTestInterruptLine} xyz.swapee.wc.IHyperIrqTestInterruptLine.typeof */
/**
 * A concrete class of _IHyperIrqTestInterruptLine_ instances.
 * @constructor xyz.swapee.wc.HyperIrqTestInterruptLine
 * @implements {xyz.swapee.wc.IHyperIrqTestInterruptLine} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.HyperIrqTestInterruptLine = class extends /** @type {xyz.swapee.wc.HyperIrqTestInterruptLine.constructor&xyz.swapee.wc.IHyperIrqTestInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperIrqTestInterruptLine* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperIrqTestInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperIrqTestInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.HyperIrqTestInterruptLine.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperIrqTestInterruptLine}
 */
xyz.swapee.wc.HyperIrqTestInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IHyperIrqTestInterruptLine} */
xyz.swapee.wc.RecordIHyperIrqTestInterruptLine

/** @typedef {xyz.swapee.wc.IHyperIrqTestInterruptLine} xyz.swapee.wc.BoundIHyperIrqTestInterruptLine */

/** @typedef {xyz.swapee.wc.HyperIrqTestInterruptLine} xyz.swapee.wc.BoundHyperIrqTestInterruptLine */

/**
 * Contains getters to cast the _IHyperIrqTestInterruptLine_ interface.
 * @interface xyz.swapee.wc.IHyperIrqTestInterruptLineCaster
 */
xyz.swapee.wc.IHyperIrqTestInterruptLineCaster = class { }
/**
 * Cast the _IHyperIrqTestInterruptLine_ instance into the _BoundIHyperIrqTestInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIHyperIrqTestInterruptLine}
 */
xyz.swapee.wc.IHyperIrqTestInterruptLineCaster.prototype.asIHyperIrqTestInterruptLine
/**
 * Access the _HyperIrqTestInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundHyperIrqTestInterruptLine}
 */
xyz.swapee.wc.IHyperIrqTestInterruptLineCaster.prototype.superHyperIrqTestInterruptLine

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IIrqTestInterruptLineCaster&xyz.swapee.wc.front.IIrqTestController)} xyz.swapee.wc.IIrqTestInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IIrqTestController} xyz.swapee.wc.front.IIrqTestController.typeof */
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @interface xyz.swapee.wc.IIrqTestInterruptLine
 */
xyz.swapee.wc.IIrqTestInterruptLine = class extends /** @type {xyz.swapee.wc.IIrqTestInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IIrqTestController.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IIrqTestInterruptLine.test} */
xyz.swapee.wc.IIrqTestInterruptLine.prototype.test = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLine.Initialese>)} xyz.swapee.wc.IrqTestInterruptLine.constructor */
/**
 * A concrete class of _IIrqTestInterruptLine_ instances.
 * @constructor xyz.swapee.wc.IrqTestInterruptLine
 * @implements {xyz.swapee.wc.IIrqTestInterruptLine} Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestInterruptLine = class extends /** @type {xyz.swapee.wc.IrqTestInterruptLine.constructor&xyz.swapee.wc.IIrqTestInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.IrqTestInterruptLine.prototype.constructor = xyz.swapee.wc.IrqTestInterruptLine
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestInterruptLine}
 */
xyz.swapee.wc.IrqTestInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IIrqTestInterruptLine} */
xyz.swapee.wc.RecordIIrqTestInterruptLine

/** @typedef {xyz.swapee.wc.IIrqTestInterruptLine} xyz.swapee.wc.BoundIIrqTestInterruptLine */

/** @typedef {xyz.swapee.wc.IrqTestInterruptLine} xyz.swapee.wc.BoundIrqTestInterruptLine */

/**
 * Contains getters to cast the _IIrqTestInterruptLine_ interface.
 * @interface xyz.swapee.wc.IIrqTestInterruptLineCaster
 */
xyz.swapee.wc.IIrqTestInterruptLineCaster = class { }
/**
 * Cast the _IIrqTestInterruptLine_ instance into the _BoundIIrqTestInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestInterruptLine}
 */
xyz.swapee.wc.IIrqTestInterruptLineCaster.prototype.asIIrqTestInterruptLine
/**
 * Cast the _IIrqTestInterruptLine_ instance into the _BoundIIrqTestTouchscreen_ type.
 * @type {!BoundIIrqTestTouchscreen}
 */
xyz.swapee.wc.IIrqTestInterruptLineCaster.prototype.asIIrqTestTouchscreen
/**
 * Access the _IrqTestInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestInterruptLine}
 */
xyz.swapee.wc.IIrqTestInterruptLineCaster.prototype.superIrqTestInterruptLine

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData) => void} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__beforeTest<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel>} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._beforeTest */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.beforeTest} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.BeforeTestPointcutData} [data] Metadata passed to the pointcuts of _test_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `test` method from being executed.
 * - `sub` _(value: !front.IrqTestInputs) =&gt; void_ Cancels a call to `test` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.beforeTest = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData) => void} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTest<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel>} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTest */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTest} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterTestPointcutData} [data] Metadata passed to the pointcuts of _test_ at the `after` joinpoint.
 * - `res` _!front.IrqTestInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTest = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData) => void} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestThrows<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel>} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestThrows */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterThrowsTestPointcutData} [data] Metadata passed to the pointcuts of _test_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `test` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData) => void} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestReturns<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel>} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestReturns */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterReturnsTestPointcutData} [data] Metadata passed to the pointcuts of _test_ at the `afterReturns` joinpoint.
 * - `res` _!front.IrqTestInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.IrqTestInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData) => void} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.__afterTestCancels<!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel>} xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel._afterTestCancels */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.AfterCancelsTestPointcutData} [data] Metadata passed to the pointcuts of _test_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IIrqTestInterruptLineJoinpointModel.TestPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel.afterTestCancels = function(data) {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.IrqTestInputs)} xyz.swapee.wc.IIrqTestInterruptLine.__test
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestInterruptLine.__test<!xyz.swapee.wc.IIrqTestInterruptLine>} xyz.swapee.wc.IIrqTestInterruptLine._test */
/** @typedef {typeof xyz.swapee.wc.IIrqTestInterruptLine.test} */
/** @return {void|!xyz.swapee.wc.front.IrqTestInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IIrqTestInterruptLine.test = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestInterruptLineJoinpointModel,xyz.swapee.wc.IIrqTestInterruptLine
/* @typal-end */