/** @extends {xyz.swapee.wc.AbstractIrqTest} */
export default class AbstractIrqTest extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractIrqTestComputer} */
export class AbstractIrqTestComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractIrqTestController} */
export class AbstractIrqTestController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractIrqTestPort} */
export class IrqTestPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractIrqTestView} */
export class AbstractIrqTestView extends (<view>
  <classes>

  </classes>

  <children>

  </children>
</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractIrqTestElement} */
export class AbstractIrqTestElement extends (<element v3 html mv>
 <block src="./IrqTest.mvc/src/IrqTestElement/methods/render.jsx" />
 <inducer src="./IrqTest.mvc/src/IrqTestElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractIrqTestHtmlComponent} */
export class AbstractIrqTestHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>