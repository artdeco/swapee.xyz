import { IrqTestDisplay, IrqTestTouchscreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.IrqTestDisplay} */
export { IrqTestDisplay }
/** @lazy @api {xyz.swapee.wc.IrqTestTouchscreen} */
export { IrqTestTouchscreen }