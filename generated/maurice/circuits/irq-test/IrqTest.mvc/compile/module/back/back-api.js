import { AbstractIrqTest, IrqTestPort, AbstractIrqTestController, IrqTestHtmlComponent,
 IrqTestBuffer, AbstractIrqTestComputer, IrqTestController } from './back-exports'

/** @lazy @api {xyz.swapee.wc.AbstractIrqTest} */
export { AbstractIrqTest }
/** @lazy @api {xyz.swapee.wc.IrqTestPort} */
export { IrqTestPort }
/** @lazy @api {xyz.swapee.wc.AbstractIrqTestController} */
export { AbstractIrqTestController }
/** @lazy @api {xyz.swapee.wc.IrqTestHtmlComponent} */
export { IrqTestHtmlComponent }
/** @lazy @api {xyz.swapee.wc.IrqTestBuffer} */
export { IrqTestBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractIrqTestComputer} */
export { AbstractIrqTestComputer }
/** @lazy @api {xyz.swapee.wc.back.IrqTestController} */
export { IrqTestController }