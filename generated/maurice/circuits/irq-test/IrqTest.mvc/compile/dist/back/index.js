/**
 * An abstract class of `xyz.swapee.wc.IIrqTest` interface.
 * @extends {xyz.swapee.wc.AbstractIrqTest}
 */
class AbstractIrqTest extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IIrqTest_, providing input
 * pins.
 * @extends {xyz.swapee.wc.IrqTestPort}
 */
class IrqTestPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestController` interface.
 * @extends {xyz.swapee.wc.AbstractIrqTestController}
 */
class AbstractIrqTestController extends (class {/* lazy-loaded */}) {}
/**
 * The _IIrqTest_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.IrqTestHtmlComponent}
 */
class IrqTestHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.IrqTestBuffer}
 */
class IrqTestBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestComputer` interface.
 * @extends {xyz.swapee.wc.AbstractIrqTestComputer}
 */
class AbstractIrqTestComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.IrqTestController}
 */
class IrqTestController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractIrqTest = AbstractIrqTest
module.exports.IrqTestPort = IrqTestPort
module.exports.AbstractIrqTestController = AbstractIrqTestController
module.exports.IrqTestHtmlComponent = IrqTestHtmlComponent
module.exports.IrqTestBuffer = IrqTestBuffer
module.exports.AbstractIrqTestComputer = AbstractIrqTestComputer
module.exports.IrqTestController = IrqTestController