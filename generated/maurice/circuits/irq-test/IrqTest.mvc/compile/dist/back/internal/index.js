import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractIrqTest}*/
export class AbstractIrqTest extends Module['29406750421'] {}
/** @type {typeof xyz.swapee.wc.AbstractIrqTest} */
AbstractIrqTest.class=function(){}
/** @type {typeof xyz.swapee.wc.IrqTestPort} */
export const IrqTestPort=Module['29406750423']
/**@extends {xyz.swapee.wc.AbstractIrqTestController}*/
export class AbstractIrqTestController extends Module['29406750424'] {}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestController} */
AbstractIrqTestController.class=function(){}
/** @type {typeof xyz.swapee.wc.IrqTestHtmlComponent} */
export const IrqTestHtmlComponent=Module['294067504210']
/** @type {typeof xyz.swapee.wc.IrqTestBuffer} */
export const IrqTestBuffer=Module['294067504211']
/**@extends {xyz.swapee.wc.AbstractIrqTestComputer}*/
export class AbstractIrqTestComputer extends Module['294067504230'] {}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestComputer} */
AbstractIrqTestComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.back.IrqTestController} */
export const IrqTestController=Module['294067504261']