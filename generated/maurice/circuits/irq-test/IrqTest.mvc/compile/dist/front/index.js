/**
 * Display for presenting information from the _IIrqTest_.
 * @extends {xyz.swapee.wc.IrqTestDisplay}
 */
class IrqTestDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @extends {xyz.swapee.wc.IrqTestTouchscreen}
 */
class IrqTestTouchscreen extends (class {/* lazy-loaded */}) {}

module.exports.IrqTestDisplay = IrqTestDisplay
module.exports.IrqTestTouchscreen = IrqTestTouchscreen