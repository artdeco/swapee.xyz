import Module from './browser'

/** @type {typeof xyz.swapee.wc.IrqTestDisplay} */
export const IrqTestDisplay=Module['294067504241']
/** @type {typeof xyz.swapee.wc.IrqTestTouchscreen} */
export const IrqTestTouchscreen=Module['294067504271']