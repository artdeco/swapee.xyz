/**
 * An abstract class of `xyz.swapee.wc.IIrqTest` interface.
 * @extends {xyz.swapee.wc.AbstractIrqTest}
 */
class AbstractIrqTest extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IIrqTest_, providing input
 * pins.
 * @extends {xyz.swapee.wc.IrqTestPort}
 */
class IrqTestPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestController` interface.
 * @extends {xyz.swapee.wc.AbstractIrqTestController}
 */
class AbstractIrqTestController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IIrqTest_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.IrqTestElement}
 */
class IrqTestElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.IrqTestBuffer}
 */
class IrqTestBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestComputer` interface.
 * @extends {xyz.swapee.wc.AbstractIrqTestComputer}
 */
class AbstractIrqTestComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.IrqTestController}
 */
class IrqTestController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractIrqTest = AbstractIrqTest
module.exports.IrqTestPort = IrqTestPort
module.exports.AbstractIrqTestController = AbstractIrqTestController
module.exports.IrqTestElement = IrqTestElement
module.exports.IrqTestBuffer = IrqTestBuffer
module.exports.AbstractIrqTestComputer = AbstractIrqTestComputer
module.exports.IrqTestController = IrqTestController

Object.defineProperties(module.exports, {
 'AbstractIrqTest': {get: () => require('./precompile/internal')[29406750421]},
 [29406750421]: {get: () => module.exports['AbstractIrqTest']},
 'IrqTestPort': {get: () => require('./precompile/internal')[29406750423]},
 [29406750423]: {get: () => module.exports['IrqTestPort']},
 'AbstractIrqTestController': {get: () => require('./precompile/internal')[29406750424]},
 [29406750424]: {get: () => module.exports['AbstractIrqTestController']},
 'IrqTestElement': {get: () => require('./precompile/internal')[29406750428]},
 [29406750428]: {get: () => module.exports['IrqTestElement']},
 'IrqTestBuffer': {get: () => require('./precompile/internal')[294067504211]},
 [294067504211]: {get: () => module.exports['IrqTestBuffer']},
 'AbstractIrqTestComputer': {get: () => require('./precompile/internal')[294067504230]},
 [294067504230]: {get: () => module.exports['AbstractIrqTestComputer']},
 'IrqTestController': {get: () => require('./precompile/internal')[294067504261]},
 [294067504261]: {get: () => module.exports['IrqTestController']},
})