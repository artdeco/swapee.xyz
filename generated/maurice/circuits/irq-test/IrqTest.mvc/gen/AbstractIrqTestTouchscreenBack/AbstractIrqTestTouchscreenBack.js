import AbstractIrqTestTouchscreenAT from '../AbstractIrqTestTouchscreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestTouchscreenBack}
 */
function __AbstractIrqTestTouchscreenBack() {}
__AbstractIrqTestTouchscreenBack.prototype = /** @type {!_AbstractIrqTestTouchscreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractIrqTestTouchscreen}
 */
class _AbstractIrqTestTouchscreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractIrqTestTouchscreen} ‎
 */
class AbstractIrqTestTouchscreenBack extends newAbstract(
 _AbstractIrqTestTouchscreenBack,294067504224,null,{
  asIIrqTestTouchscreen:1,
  superIrqTestTouchscreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreen} */
AbstractIrqTestTouchscreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreen} */
function AbstractIrqTestTouchscreenBackClass(){}

export default AbstractIrqTestTouchscreenBack


AbstractIrqTestTouchscreenBack[$implementations]=[
 __AbstractIrqTestTouchscreenBack,
 AbstractIrqTestTouchscreenAT,
]