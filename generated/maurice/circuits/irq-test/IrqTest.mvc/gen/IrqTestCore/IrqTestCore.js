import {mountPins} from '@webcircuits/webcircuits'
import {IrqTestMemoryPQs} from '../../pqs/IrqTestMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_IrqTestCore}
 */
function __IrqTestCore() {}
__IrqTestCore.prototype = /** @type {!_IrqTestCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestCore}
 */
class _IrqTestCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractIrqTestCore} ‎
 */
class IrqTestCore extends newAbstract(
 _IrqTestCore,29406750426,null,{
  asIIrqTestCore:1,
  superIrqTestCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestCore} */
IrqTestCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestCore} */
function IrqTestCoreClass(){}

export default IrqTestCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_IrqTestOuterCore}
 */
function __IrqTestOuterCore() {}
__IrqTestOuterCore.prototype = /** @type {!_IrqTestOuterCore} */ ({ })
/** @this {xyz.swapee.wc.IrqTestOuterCore} */
export function IrqTestOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IIrqTestOuterCore.Model}*/
  this.model={
    core: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestOuterCore}
 */
class _IrqTestOuterCore { }
/**
 * The _IIrqTest_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractIrqTestOuterCore} ‎
 */
export class IrqTestOuterCore extends newAbstract(
 _IrqTestOuterCore,29406750423,IrqTestOuterCoreConstructor,{
  asIIrqTestOuterCore:1,
  superIrqTestOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestOuterCore} */
IrqTestOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestOuterCore} */
function IrqTestOuterCoreClass(){}


IrqTestOuterCore[$implementations]=[
 __IrqTestOuterCore,
 IrqTestOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestOuterCore}*/({
  constructor(){
   mountPins(this.model,'',IrqTestMemoryPQs)

  },
 }),
]

IrqTestCore[$implementations]=[
 IrqTestCoreClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestCore}*/({
  resetCore(){
   this.resetIrqTestCore()
  },
  resetIrqTestCore(){
   IrqTestOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.IrqTestOuterCore}*/(
     /**@type {!xyz.swapee.wc.IIrqTestOuterCore}*/(this)),
   )
  },
 }),
 __IrqTestCore,
 IrqTestOuterCore,
]

export {IrqTestCore}