import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestTouchscreenAT}
 */
function __AbstractIrqTestTouchscreenAT() {}
__AbstractIrqTestTouchscreenAT.prototype = /** @type {!_AbstractIrqTestTouchscreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT}
 */
class _AbstractIrqTestTouchscreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IIrqTestTouchscreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT} ‎
 */
class AbstractIrqTestTouchscreenAT extends newAbstract(
 _AbstractIrqTestTouchscreenAT,294067504226,null,{
  asIIrqTestTouchscreenAT:1,
  superIrqTestTouchscreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT} */
AbstractIrqTestTouchscreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT} */
function AbstractIrqTestTouchscreenATClass(){}

export default AbstractIrqTestTouchscreenAT


AbstractIrqTestTouchscreenAT[$implementations]=[
 __AbstractIrqTestTouchscreenAT,
 UartUniversal,
 AbstractIrqTestTouchscreenATClass.prototype=/**@type {!xyz.swapee.wc.back.IIrqTestTouchscreenAT}*/({
  stashKeyboardItemAfterMenuExpanded(){
   const{asIIrqTestTouchscreenAT:{uart:uart}}=this
   uart.t('inv',{mid:'12f8d',args:[...arguments]})
  },
 }),
]