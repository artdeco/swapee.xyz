import IrqTestBuffer from '../IrqTestBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {IrqTestPortConnector} from '../IrqTestPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestController}
 */
function __AbstractIrqTestController() {}
__AbstractIrqTestController.prototype = /** @type {!_AbstractIrqTestController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestController}
 */
class _AbstractIrqTestController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractIrqTestController} ‎
 */
export class AbstractIrqTestController extends newAbstract(
 _AbstractIrqTestController,294067504218,null,{
  asIIrqTestController:1,
  superIrqTestController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestController} */
AbstractIrqTestController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestController} */
function AbstractIrqTestControllerClass(){}


AbstractIrqTestController[$implementations]=[
 AbstractIrqTestControllerClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IIrqTestPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractIrqTestController,
 IrqTestBuffer,
 IntegratedController,
 /**@type {!AbstractIrqTestController}*/(IrqTestPortConnector),
]


export default AbstractIrqTestController