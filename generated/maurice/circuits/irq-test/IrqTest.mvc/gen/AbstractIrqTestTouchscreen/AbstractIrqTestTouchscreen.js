import AbstractIrqTestTouchscreenAR from '../AbstractIrqTestTouchscreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {IrqTestInputsPQs} from '../../pqs/IrqTestInputsPQs'
import {IrqTestMemoryQPs} from '../../pqs/IrqTestMemoryQPs'
import {IrqTestVdusPQs} from '../../pqs/IrqTestVdusPQs'
import { newAbstract, $implementations, scheduleLast } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestTouchscreen}
 */
function __AbstractIrqTestTouchscreen() {}
__AbstractIrqTestTouchscreen.prototype = /** @type {!_AbstractIrqTestTouchscreen} */ ({ })
/** @this {xyz.swapee.wc.IrqTestTouchscreen} */ function IrqTestTouchscreenConstructor() {
  /** @type {HTMLElement} */ this.keyboardItem=null
  /** @type {boolean} */ this.menuExpanded=false
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestTouchscreen}
 */
class _AbstractIrqTestTouchscreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @extends {xyz.swapee.wc.AbstractIrqTestTouchscreen} ‎
 */
class AbstractIrqTestTouchscreen extends newAbstract(
 _AbstractIrqTestTouchscreen,294067504223,IrqTestTouchscreenConstructor,{
  asIIrqTestTouchscreen:1,
  superIrqTestTouchscreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestTouchscreen} */
AbstractIrqTestTouchscreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestTouchscreen} */
function AbstractIrqTestTouchscreenClass(){}

export default AbstractIrqTestTouchscreen


AbstractIrqTestTouchscreen[$implementations]=[
 __AbstractIrqTestTouchscreen,
 AbstractIrqTestTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestTouchscreen}*/({
  inputsPQs:IrqTestInputsPQs,
  memoryQPs:IrqTestMemoryQPs,
 }),
 Screen,
 AbstractIrqTestTouchscreenAR,
 AbstractIrqTestTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestTouchscreen}*/({
  vdusPQs:IrqTestVdusPQs,
 }),
 AbstractIrqTestTouchscreenClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestTouchscreen}*/({
  constructor(){
   scheduleLast(this,()=>{
    const element=this.element
    if(element){
     element.addEventListener('click',(ev)=>{
      this.test()
     })
    }
   })
  },
 }),
]