import AbstractIrqTestDisplay from '../AbstractIrqTestDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {IrqTestVdusPQs} from '../../pqs/IrqTestVdusPQs'
import {IrqTestVdusQPs} from '../../pqs/IrqTestVdusQPs'
import {IrqTestMemoryPQs} from '../../pqs/IrqTestMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestGPU}
 */
function __AbstractIrqTestGPU() {}
__AbstractIrqTestGPU.prototype = /** @type {!_AbstractIrqTestGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestGPU}
 */
class _AbstractIrqTestGPU { }
/**
 * Handles the periphery of the _IIrqTestDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractIrqTestGPU} ‎
 */
class AbstractIrqTestGPU extends newAbstract(
 _AbstractIrqTestGPU,294067504214,null,{
  asIIrqTestGPU:1,
  superIrqTestGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestGPU} */
AbstractIrqTestGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestGPU} */
function AbstractIrqTestGPUClass(){}

export default AbstractIrqTestGPU


AbstractIrqTestGPU[$implementations]=[
 __AbstractIrqTestGPU,
 AbstractIrqTestGPUClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestGPU}*/({
  vdusPQs:IrqTestVdusPQs,
  vdusQPs:IrqTestVdusQPs,
  memoryPQs:IrqTestMemoryPQs,
 }),
 AbstractIrqTestDisplay,
 BrowserView,
]