import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestTouchscreenAR}
 */
function __AbstractIrqTestTouchscreenAR() {}
__AbstractIrqTestTouchscreenAR.prototype = /** @type {!_AbstractIrqTestTouchscreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR}
 */
class _AbstractIrqTestTouchscreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IIrqTestTouchscreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR} ‎
 */
class AbstractIrqTestTouchscreenAR extends newAbstract(
 _AbstractIrqTestTouchscreenAR,294067504225,null,{
  asIIrqTestTouchscreenAR:1,
  superIrqTestTouchscreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR} */
AbstractIrqTestTouchscreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR} */
function AbstractIrqTestTouchscreenARClass(){}

export default AbstractIrqTestTouchscreenAR


AbstractIrqTestTouchscreenAR[$implementations]=[
 __AbstractIrqTestTouchscreenAR,
 AR,
 AbstractIrqTestTouchscreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IIrqTestTouchscreenAR}*/({
  allocator(){
   this.methods={
    stashKeyboardItemAfterMenuExpanded:'12f8d',
   }
  },
 }),
]
export {AbstractIrqTestTouchscreenAR}