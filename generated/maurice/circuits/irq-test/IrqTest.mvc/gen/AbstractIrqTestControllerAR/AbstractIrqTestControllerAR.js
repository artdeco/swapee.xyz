import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestControllerAR}
 */
function __AbstractIrqTestControllerAR() {}
__AbstractIrqTestControllerAR.prototype = /** @type {!_AbstractIrqTestControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractIrqTestControllerAR}
 */
class _AbstractIrqTestControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IIrqTestControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractIrqTestControllerAR} ‎
 */
class AbstractIrqTestControllerAR extends newAbstract(
 _AbstractIrqTestControllerAR,294067504221,null,{
  asIIrqTestControllerAR:1,
  superIrqTestControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestControllerAR} */
AbstractIrqTestControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestControllerAR} */
function AbstractIrqTestControllerARClass(){}

export default AbstractIrqTestControllerAR


AbstractIrqTestControllerAR[$implementations]=[
 __AbstractIrqTestControllerAR,
 AR,
 AbstractIrqTestControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IIrqTestControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]