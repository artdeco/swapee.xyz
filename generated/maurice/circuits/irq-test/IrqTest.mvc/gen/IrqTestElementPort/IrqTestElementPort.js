import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_IrqTestElementPort}
 */
function __IrqTestElementPort() {}
__IrqTestElementPort.prototype = /** @type {!_IrqTestElementPort} */ ({ })
/** @this {xyz.swapee.wc.IrqTestElementPort} */ function IrqTestElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IIrqTestElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestElementPort}
 */
class _IrqTestElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractIrqTestElementPort} ‎
 */
class IrqTestElementPort extends newAbstract(
 _IrqTestElementPort,294067504212,IrqTestElementPortConstructor,{
  asIIrqTestElementPort:1,
  superIrqTestElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestElementPort} */
IrqTestElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestElementPort} */
function IrqTestElementPortClass(){}

export default IrqTestElementPort


IrqTestElementPort[$implementations]=[
 __IrqTestElementPort,
 IrqTestElementPortClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
   })
  },
 }),
]