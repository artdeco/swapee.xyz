import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestProcessor}
 */
function __AbstractIrqTestProcessor() {}
__AbstractIrqTestProcessor.prototype = /** @type {!_AbstractIrqTestProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestProcessor}
 */
class _AbstractIrqTestProcessor { }
/**
 * The processor to compute changes to the memory for the _IIrqTest_.
 * @extends {xyz.swapee.wc.AbstractIrqTestProcessor} ‎
 */
class AbstractIrqTestProcessor extends newAbstract(
 _AbstractIrqTestProcessor,29406750427,null,{
  asIIrqTestProcessor:1,
  superIrqTestProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestProcessor} */
AbstractIrqTestProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestProcessor} */
function AbstractIrqTestProcessorClass(){}

export default AbstractIrqTestProcessor


AbstractIrqTestProcessor[$implementations]=[
 __AbstractIrqTestProcessor,
]