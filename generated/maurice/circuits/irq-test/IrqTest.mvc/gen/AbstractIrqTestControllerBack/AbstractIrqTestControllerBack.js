import AbstractIrqTestControllerAR from '../AbstractIrqTestControllerAR'
import {AbstractIrqTestController} from '../AbstractIrqTestController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestControllerBack}
 */
function __AbstractIrqTestControllerBack() {}
__AbstractIrqTestControllerBack.prototype = /** @type {!_AbstractIrqTestControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractIrqTestController}
 */
class _AbstractIrqTestControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractIrqTestController} ‎
 */
class AbstractIrqTestControllerBack extends newAbstract(
 _AbstractIrqTestControllerBack,294067504220,null,{
  asIIrqTestController:1,
  superIrqTestController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestController} */
AbstractIrqTestControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestController} */
function AbstractIrqTestControllerBackClass(){}

export default AbstractIrqTestControllerBack


AbstractIrqTestControllerBack[$implementations]=[
 __AbstractIrqTestControllerBack,
 AbstractIrqTestController,
 AbstractIrqTestControllerAR,
 DriverBack,
]