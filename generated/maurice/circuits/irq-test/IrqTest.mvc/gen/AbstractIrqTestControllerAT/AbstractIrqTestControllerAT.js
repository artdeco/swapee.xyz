import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestControllerAT}
 */
function __AbstractIrqTestControllerAT() {}
__AbstractIrqTestControllerAT.prototype = /** @type {!_AbstractIrqTestControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractIrqTestControllerAT}
 */
class _AbstractIrqTestControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IIrqTestControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractIrqTestControllerAT} ‎
 */
class AbstractIrqTestControllerAT extends newAbstract(
 _AbstractIrqTestControllerAT,294067504222,null,{
  asIIrqTestControllerAT:1,
  superIrqTestControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractIrqTestControllerAT} */
AbstractIrqTestControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractIrqTestControllerAT} */
function AbstractIrqTestControllerATClass(){}

export default AbstractIrqTestControllerAT


AbstractIrqTestControllerAT[$implementations]=[
 __AbstractIrqTestControllerAT,
 UartUniversal,
 AbstractIrqTestControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IIrqTestControllerAT}*/({
  get asIIrqTestController(){
   return this
  },
 }),
]