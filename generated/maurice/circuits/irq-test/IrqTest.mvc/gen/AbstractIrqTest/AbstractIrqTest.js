import AbstractIrqTestProcessor from '../AbstractIrqTestProcessor'
import {IrqTestCore} from '../IrqTestCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractIrqTestComputer} from '../AbstractIrqTestComputer'
import {AbstractIrqTestController} from '../AbstractIrqTestController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTest}
 */
function __AbstractIrqTest() {}
__AbstractIrqTest.prototype = /** @type {!_AbstractIrqTest} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTest}
 */
class _AbstractIrqTest { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractIrqTest} ‎
 */
class AbstractIrqTest extends newAbstract(
 _AbstractIrqTest,29406750428,null,{
  asIIrqTest:1,
  superIrqTest:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTest} */
AbstractIrqTest.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTest} */
function AbstractIrqTestClass(){}

export default AbstractIrqTest


AbstractIrqTest[$implementations]=[
 __AbstractIrqTest,
 IrqTestCore,
 AbstractIrqTestProcessor,
 IntegratedComponent,
 AbstractIrqTestComputer,
 AbstractIrqTestController,
]


export {AbstractIrqTest}