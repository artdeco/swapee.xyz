import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestDisplay}
 */
function __AbstractIrqTestDisplay() {}
__AbstractIrqTestDisplay.prototype = /** @type {!_AbstractIrqTestDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestDisplay}
 */
class _AbstractIrqTestDisplay { }
/**
 * Display for presenting information from the _IIrqTest_.
 * @extends {xyz.swapee.wc.AbstractIrqTestDisplay} ‎
 */
class AbstractIrqTestDisplay extends newAbstract(
 _AbstractIrqTestDisplay,294067504215,null,{
  asIIrqTestDisplay:1,
  superIrqTestDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestDisplay} */
AbstractIrqTestDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestDisplay} */
function AbstractIrqTestDisplayClass(){}

export default AbstractIrqTestDisplay


AbstractIrqTestDisplay[$implementations]=[
 __AbstractIrqTestDisplay,
 Display,
]