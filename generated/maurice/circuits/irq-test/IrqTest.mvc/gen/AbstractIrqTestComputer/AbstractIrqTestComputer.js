import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestComputer}
 */
function __AbstractIrqTestComputer() {}
__AbstractIrqTestComputer.prototype = /** @type {!_AbstractIrqTestComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestComputer}
 */
class _AbstractIrqTestComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractIrqTestComputer} ‎
 */
export class AbstractIrqTestComputer extends newAbstract(
 _AbstractIrqTestComputer,29406750421,null,{
  asIIrqTestComputer:1,
  superIrqTestComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestComputer} */
AbstractIrqTestComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestComputer} */
function AbstractIrqTestComputerClass(){}


AbstractIrqTestComputer[$implementations]=[
 __AbstractIrqTestComputer,
 Adapter,
]


export default AbstractIrqTestComputer