import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {IrqTestInputsPQs} from '../../pqs/IrqTestInputsPQs'
import {IrqTestOuterCoreConstructor} from '../IrqTestCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_IrqTestPort}
 */
function __IrqTestPort() {}
__IrqTestPort.prototype = /** @type {!_IrqTestPort} */ ({ })
/** @this {xyz.swapee.wc.IrqTestPort} */ function IrqTestPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.IrqTestOuterCore} */ ({model:null})
  IrqTestOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestPort}
 */
class _IrqTestPort { }
/**
 * The port that serves as an interface to the _IIrqTest_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractIrqTestPort} ‎
 */
export class IrqTestPort extends newAbstract(
 _IrqTestPort,29406750425,IrqTestPortConstructor,{
  asIIrqTestPort:1,
  superIrqTestPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestPort} */
IrqTestPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestPort} */
function IrqTestPortClass(){}

export const IrqTestPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IIrqTest.Pinout>}*/({
 get Port() { return IrqTestPort },
})

IrqTestPort[$implementations]=[
 IrqTestPortClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestPort}*/({
  resetPort(){
   this.resetIrqTestPort()
  },
  resetIrqTestPort(){
   IrqTestPortConstructor.call(this)
  },
 }),
 __IrqTestPort,
 Parametric,
 IrqTestPortClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestPort}*/({
  constructor(){
   mountPins(this.inputs,'',IrqTestInputsPQs)
  },
 }),
]


export default IrqTestPort