import {makeBuffers} from '@webcircuits/webcircuits'

export const IrqTestBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  core:String,
 }),
})

export default IrqTestBuffer