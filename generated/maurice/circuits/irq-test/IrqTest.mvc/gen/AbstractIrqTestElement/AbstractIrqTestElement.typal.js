
import AbstractIrqTest from '../AbstractIrqTest'

/** @abstract {xyz.swapee.wc.IIrqTestElement} */
export default class AbstractIrqTestElement { }



AbstractIrqTestElement[$implementations]=[AbstractIrqTest,
 /** @type {!AbstractIrqTestElement} */ ({
  rootId:'IrqTest',
  __$id:2940675042,
  fqn:'xyz.swapee.wc.IIrqTest',
  maurice_element_v3:true,
 }),
]