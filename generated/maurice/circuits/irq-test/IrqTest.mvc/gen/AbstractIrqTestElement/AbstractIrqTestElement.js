import IrqTestElementPort from '../IrqTestElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {IrqTestInputsPQs} from '../../pqs/IrqTestInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractIrqTest from '../AbstractIrqTest'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestElement}
 */
function __AbstractIrqTestElement() {}
__AbstractIrqTestElement.prototype = /** @type {!_AbstractIrqTestElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestElement}
 */
class _AbstractIrqTestElement { }
/**
 * A component description.
 *
 * The _IIrqTest_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractIrqTestElement} ‎
 */
class AbstractIrqTestElement extends newAbstract(
 _AbstractIrqTestElement,294067504211,null,{
  asIIrqTestElement:1,
  superIrqTestElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestElement} */
AbstractIrqTestElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestElement} */
function AbstractIrqTestElementClass(){}

export default AbstractIrqTestElement


AbstractIrqTestElement[$implementations]=[
 __AbstractIrqTestElement,
 ElementBase,
 AbstractIrqTestElementClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':core':coreColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'core':coreAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(coreAttr===undefined?{'core':coreColAttr}:{}),
   }
  },
 }),
 AbstractIrqTestElementClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'core':coreAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    core:coreAttr,
   }
  },
 }),
 AbstractIrqTestElementClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractIrqTestElementClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestElement}*/({
  inputsPQs:IrqTestInputsPQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractIrqTestElementClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','core','no-solder',':no-solder',':core','fe646','a74ad','children']),
   })
  },
  get Port(){
   return IrqTestElementPort
  },
 }),
]



AbstractIrqTestElement[$implementations]=[AbstractIrqTest,
 /** @type {!AbstractIrqTestElement} */ ({
  rootId:'IrqTest',
  __$id:2940675042,
  fqn:'xyz.swapee.wc.IIrqTest',
  maurice_element_v3:true,
 }),
]