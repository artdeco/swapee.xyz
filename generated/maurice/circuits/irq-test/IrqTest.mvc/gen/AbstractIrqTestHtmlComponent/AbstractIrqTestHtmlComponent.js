import AbstractIrqTestGPU from '../AbstractIrqTestGPU'
import AbstractIrqTestTouchscreenBack from '../AbstractIrqTestTouchscreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {IrqTestInputsQPs} from '../../pqs/IrqTestInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractIrqTest from '../AbstractIrqTest'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestHtmlComponent}
 */
function __AbstractIrqTestHtmlComponent() {}
__AbstractIrqTestHtmlComponent.prototype = /** @type {!_AbstractIrqTestHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractIrqTestHtmlComponent}
 */
class _AbstractIrqTestHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.IrqTestHtmlComponent} */ (res)
  }
}
/**
 * The _IIrqTest_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractIrqTestHtmlComponent} ‎
 */
export class AbstractIrqTestHtmlComponent extends newAbstract(
 _AbstractIrqTestHtmlComponent,294067504210,null,{
  asIIrqTestHtmlComponent:1,
  superIrqTestHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractIrqTestHtmlComponent} */
AbstractIrqTestHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractIrqTestHtmlComponent} */
function AbstractIrqTestHtmlComponentClass(){}


AbstractIrqTestHtmlComponent[$implementations]=[
 __AbstractIrqTestHtmlComponent,
 HtmlComponent,
 AbstractIrqTest,
 AbstractIrqTestGPU,
 AbstractIrqTestTouchscreenBack,
 AbstractIrqTestHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IIrqTestHtmlComponent}*/({
  inputsQPs:IrqTestInputsQPs,
 }),
]