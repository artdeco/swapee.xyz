import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractIrqTestDisplay}
 */
function __AbstractIrqTestDisplay() {}
__AbstractIrqTestDisplay.prototype = /** @type {!_AbstractIrqTestDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractIrqTestDisplay}
 */
class _AbstractIrqTestDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractIrqTestDisplay} ‎
 */
class AbstractIrqTestDisplay extends newAbstract(
 _AbstractIrqTestDisplay,294067504217,null,{
  asIIrqTestDisplay:1,
  superIrqTestDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestDisplay} */
AbstractIrqTestDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractIrqTestDisplay} */
function AbstractIrqTestDisplayClass(){}

export default AbstractIrqTestDisplay


AbstractIrqTestDisplay[$implementations]=[
 __AbstractIrqTestDisplay,
 GraphicsDriverBack,
]