/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IIrqTestComputer={}
xyz.swapee.wc.IIrqTestOuterCore={}
xyz.swapee.wc.IIrqTestOuterCore.Model={}
xyz.swapee.wc.IIrqTestOuterCore.Model.Core={}
xyz.swapee.wc.IIrqTestOuterCore.WeakModel={}
xyz.swapee.wc.IIrqTestPort={}
xyz.swapee.wc.IIrqTestPort.Inputs={}
xyz.swapee.wc.IIrqTestPort.WeakInputs={}
xyz.swapee.wc.IIrqTestCore={}
xyz.swapee.wc.IIrqTestCore.Model={}
xyz.swapee.wc.IIrqTestProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IIrqTestController={}
xyz.swapee.wc.front.IIrqTestControllerAT={}
xyz.swapee.wc.front.IIrqTestTouchscreenAR={}
xyz.swapee.wc.IIrqTest={}
xyz.swapee.wc.IIrqTestHtmlComponent={}
xyz.swapee.wc.IIrqTestElement={}
xyz.swapee.wc.IIrqTestElementPort={}
xyz.swapee.wc.IIrqTestElementPort.Inputs={}
xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IIrqTestElementPort.WeakInputs={}
xyz.swapee.wc.IIrqTestDesigner={}
xyz.swapee.wc.IIrqTestDesigner.communicator={}
xyz.swapee.wc.IIrqTestDesigner.relay={}
xyz.swapee.wc.IIrqTestDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IIrqTestDisplay={}
xyz.swapee.wc.back.IIrqTestController={}
xyz.swapee.wc.back.IIrqTestControllerAR={}
xyz.swapee.wc.back.IIrqTestTouchscreen={}
xyz.swapee.wc.back.IIrqTestTouchscreenAT={}
xyz.swapee.wc.IIrqTestController={}
xyz.swapee.wc.IIrqTestTouchscreen={}
xyz.swapee.wc.IIrqTestGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml}  aade9056d51880e07671c7d35f1d1e6f */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IIrqTestComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestComputer)} xyz.swapee.wc.AbstractIrqTestComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestComputer} xyz.swapee.wc.IrqTestComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestComputer` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestComputer
 */
xyz.swapee.wc.AbstractIrqTestComputer = class extends /** @type {xyz.swapee.wc.AbstractIrqTestComputer.constructor&xyz.swapee.wc.IrqTestComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestComputer.prototype.constructor = xyz.swapee.wc.AbstractIrqTestComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestComputer.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestComputer.Initialese[]) => xyz.swapee.wc.IIrqTestComputer} xyz.swapee.wc.IrqTestComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IIrqTestComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.IrqTestMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IIrqTestComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IIrqTestComputer
 */
xyz.swapee.wc.IIrqTestComputer = class extends /** @type {xyz.swapee.wc.IIrqTestComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IIrqTestComputer.compute} */
xyz.swapee.wc.IIrqTestComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestComputer.Initialese>)} xyz.swapee.wc.IrqTestComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestComputer} xyz.swapee.wc.IIrqTestComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IIrqTestComputer_ instances.
 * @constructor xyz.swapee.wc.IrqTestComputer
 * @implements {xyz.swapee.wc.IIrqTestComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestComputer.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestComputer = class extends /** @type {xyz.swapee.wc.IrqTestComputer.constructor&xyz.swapee.wc.IIrqTestComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.IrqTestComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IIrqTestComputer} */
xyz.swapee.wc.RecordIIrqTestComputer

/** @typedef {xyz.swapee.wc.IIrqTestComputer} xyz.swapee.wc.BoundIIrqTestComputer */

/** @typedef {xyz.swapee.wc.IrqTestComputer} xyz.swapee.wc.BoundIrqTestComputer */

/**
 * Contains getters to cast the _IIrqTestComputer_ interface.
 * @interface xyz.swapee.wc.IIrqTestComputerCaster
 */
xyz.swapee.wc.IIrqTestComputerCaster = class { }
/**
 * Cast the _IIrqTestComputer_ instance into the _BoundIIrqTestComputer_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestComputer}
 */
xyz.swapee.wc.IIrqTestComputerCaster.prototype.asIIrqTestComputer
/**
 * Access the _IrqTestComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestComputer}
 */
xyz.swapee.wc.IIrqTestComputerCaster.prototype.superIrqTestComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.IrqTestMemory) => void} xyz.swapee.wc.IIrqTestComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestComputer.__compute<!xyz.swapee.wc.IIrqTestComputer>} xyz.swapee.wc.IIrqTestComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IIrqTestComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.IrqTestMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IIrqTestComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml}  0d419d16cb0da9ef4cac8d8609b34187 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IIrqTestOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestOuterCore)} xyz.swapee.wc.AbstractIrqTestOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestOuterCore} xyz.swapee.wc.IrqTestOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestOuterCore
 */
xyz.swapee.wc.AbstractIrqTestOuterCore = class extends /** @type {xyz.swapee.wc.AbstractIrqTestOuterCore.constructor&xyz.swapee.wc.IrqTestOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestOuterCore.prototype.constructor = xyz.swapee.wc.AbstractIrqTestOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestOuterCoreCaster)} xyz.swapee.wc.IIrqTestOuterCore.constructor */
/**
 * The _IIrqTest_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IIrqTestOuterCore
 */
xyz.swapee.wc.IIrqTestOuterCore = class extends /** @type {xyz.swapee.wc.IIrqTestOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IIrqTestOuterCore.prototype.constructor = xyz.swapee.wc.IIrqTestOuterCore

/** @typedef {function(new: xyz.swapee.wc.IIrqTestOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestOuterCore.Initialese>)} xyz.swapee.wc.IrqTestOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestOuterCore} xyz.swapee.wc.IIrqTestOuterCore.typeof */
/**
 * A concrete class of _IIrqTestOuterCore_ instances.
 * @constructor xyz.swapee.wc.IrqTestOuterCore
 * @implements {xyz.swapee.wc.IIrqTestOuterCore} The _IIrqTest_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestOuterCore = class extends /** @type {xyz.swapee.wc.IrqTestOuterCore.constructor&xyz.swapee.wc.IIrqTestOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.IrqTestOuterCore.prototype.constructor = xyz.swapee.wc.IrqTestOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.IrqTestOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestOuterCore.
 * @interface xyz.swapee.wc.IIrqTestOuterCoreFields
 */
xyz.swapee.wc.IIrqTestOuterCoreFields = class { }
/**
 * The _IIrqTest_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IIrqTestOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IIrqTestOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore} */
xyz.swapee.wc.RecordIIrqTestOuterCore

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore} xyz.swapee.wc.BoundIIrqTestOuterCore */

/** @typedef {xyz.swapee.wc.IrqTestOuterCore} xyz.swapee.wc.BoundIrqTestOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core.core

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.Model.Core} xyz.swapee.wc.IIrqTestOuterCore.Model The _IIrqTest_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core} xyz.swapee.wc.IIrqTestOuterCore.WeakModel The _IIrqTest_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IIrqTestOuterCore_ interface.
 * @interface xyz.swapee.wc.IIrqTestOuterCoreCaster
 */
xyz.swapee.wc.IIrqTestOuterCoreCaster = class { }
/**
 * Cast the _IIrqTestOuterCore_ instance into the _BoundIIrqTestOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestOuterCore}
 */
xyz.swapee.wc.IIrqTestOuterCoreCaster.prototype.asIIrqTestOuterCore
/**
 * Access the _IrqTestOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestOuterCore}
 */
xyz.swapee.wc.IIrqTestOuterCoreCaster.prototype.superIrqTestOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestOuterCore.Model.Core The core property (optional overlay).
 * @prop {string} [core=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe The core property (required overlay).
 * @prop {string} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core The core property (optional overlay).
 * @prop {*} [core=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe The core property (required overlay).
 * @prop {*} core The core property.
 */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core} xyz.swapee.wc.IIrqTestPort.Inputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.IIrqTestPort.Inputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core} xyz.swapee.wc.IIrqTestPort.WeakInputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.IIrqTestPort.WeakInputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.Model.Core} xyz.swapee.wc.IIrqTestCore.Model.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe} xyz.swapee.wc.IIrqTestCore.Model.Core_Safe The core property (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml}  73e425e5975d11b37093b7cbbbb4e903 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IIrqTestPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestPort)} xyz.swapee.wc.AbstractIrqTestPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestPort} xyz.swapee.wc.IrqTestPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestPort` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestPort
 */
xyz.swapee.wc.AbstractIrqTestPort = class extends /** @type {xyz.swapee.wc.AbstractIrqTestPort.constructor&xyz.swapee.wc.IrqTestPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestPort.prototype.constructor = xyz.swapee.wc.AbstractIrqTestPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestPort.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestPort.Initialese[]) => xyz.swapee.wc.IIrqTestPort} xyz.swapee.wc.IrqTestPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestPortFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IIrqTestPort.Inputs>)} xyz.swapee.wc.IIrqTestPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IIrqTest_, providing input
 * pins.
 * @interface xyz.swapee.wc.IIrqTestPort
 */
xyz.swapee.wc.IIrqTestPort = class extends /** @type {xyz.swapee.wc.IIrqTestPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IIrqTestPort.resetPort} */
xyz.swapee.wc.IIrqTestPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IIrqTestPort.resetIrqTestPort} */
xyz.swapee.wc.IIrqTestPort.prototype.resetIrqTestPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestPort&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestPort.Initialese>)} xyz.swapee.wc.IrqTestPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestPort} xyz.swapee.wc.IIrqTestPort.typeof */
/**
 * A concrete class of _IIrqTestPort_ instances.
 * @constructor xyz.swapee.wc.IrqTestPort
 * @implements {xyz.swapee.wc.IIrqTestPort} The port that serves as an interface to the _IIrqTest_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestPort.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestPort = class extends /** @type {xyz.swapee.wc.IrqTestPort.constructor&xyz.swapee.wc.IIrqTestPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.IrqTestPort.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestPort.
 * @interface xyz.swapee.wc.IIrqTestPortFields
 */
xyz.swapee.wc.IIrqTestPortFields = class { }
/**
 * The inputs to the _IIrqTest_'s controller via its port.
 */
xyz.swapee.wc.IIrqTestPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IIrqTestPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IIrqTestPortFields.prototype.props = /** @type {!xyz.swapee.wc.IIrqTestPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestPort} */
xyz.swapee.wc.RecordIIrqTestPort

/** @typedef {xyz.swapee.wc.IIrqTestPort} xyz.swapee.wc.BoundIIrqTestPort */

/** @typedef {xyz.swapee.wc.IrqTestPort} xyz.swapee.wc.BoundIrqTestPort */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestOuterCore.WeakModel)} xyz.swapee.wc.IIrqTestPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestOuterCore.WeakModel} xyz.swapee.wc.IIrqTestOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IIrqTest_'s controller via its port.
 * @record xyz.swapee.wc.IIrqTestPort.Inputs
 */
xyz.swapee.wc.IIrqTestPort.Inputs = class extends /** @type {xyz.swapee.wc.IIrqTestPort.Inputs.constructor&xyz.swapee.wc.IIrqTestOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IIrqTestPort.Inputs.prototype.constructor = xyz.swapee.wc.IIrqTestPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IIrqTestOuterCore.WeakModel)} xyz.swapee.wc.IIrqTestPort.WeakInputs.constructor */
/**
 * The inputs to the _IIrqTest_'s controller via its port.
 * @record xyz.swapee.wc.IIrqTestPort.WeakInputs
 */
xyz.swapee.wc.IIrqTestPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IIrqTestPort.WeakInputs.constructor&xyz.swapee.wc.IIrqTestOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IIrqTestPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IIrqTestPort.WeakInputs

/**
 * Contains getters to cast the _IIrqTestPort_ interface.
 * @interface xyz.swapee.wc.IIrqTestPortCaster
 */
xyz.swapee.wc.IIrqTestPortCaster = class { }
/**
 * Cast the _IIrqTestPort_ instance into the _BoundIIrqTestPort_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestPort}
 */
xyz.swapee.wc.IIrqTestPortCaster.prototype.asIIrqTestPort
/**
 * Access the _IrqTestPort_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestPort}
 */
xyz.swapee.wc.IIrqTestPortCaster.prototype.superIrqTestPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IIrqTestPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestPort.__resetPort<!xyz.swapee.wc.IIrqTestPort>} xyz.swapee.wc.IIrqTestPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IIrqTestPort.resetPort} */
/**
 * Resets the _IIrqTest_ port.
 * @return {void}
 */
xyz.swapee.wc.IIrqTestPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort<!xyz.swapee.wc.IIrqTestPort>} xyz.swapee.wc.IIrqTestPort._resetIrqTestPort */
/** @typedef {typeof xyz.swapee.wc.IIrqTestPort.resetIrqTestPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IIrqTestPort.resetIrqTestPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml}  05cd1bd78f8ee417d25f52d258d4453e */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IIrqTestCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestCore)} xyz.swapee.wc.AbstractIrqTestCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestCore} xyz.swapee.wc.IrqTestCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestCore` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestCore
 */
xyz.swapee.wc.AbstractIrqTestCore = class extends /** @type {xyz.swapee.wc.AbstractIrqTestCore.constructor&xyz.swapee.wc.IrqTestCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestCore.prototype.constructor = xyz.swapee.wc.AbstractIrqTestCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestCore.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestCoreCaster&xyz.swapee.wc.IIrqTestOuterCore)} xyz.swapee.wc.IIrqTestCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IIrqTestCore
 */
xyz.swapee.wc.IIrqTestCore = class extends /** @type {xyz.swapee.wc.IIrqTestCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IIrqTestOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IIrqTestCore.resetCore} */
xyz.swapee.wc.IIrqTestCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IIrqTestCore.resetIrqTestCore} */
xyz.swapee.wc.IIrqTestCore.prototype.resetIrqTestCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestCore&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestCore.Initialese>)} xyz.swapee.wc.IrqTestCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestCore} xyz.swapee.wc.IIrqTestCore.typeof */
/**
 * A concrete class of _IIrqTestCore_ instances.
 * @constructor xyz.swapee.wc.IrqTestCore
 * @implements {xyz.swapee.wc.IIrqTestCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestCore.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestCore = class extends /** @type {xyz.swapee.wc.IrqTestCore.constructor&xyz.swapee.wc.IIrqTestCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.IrqTestCore.prototype.constructor = xyz.swapee.wc.IrqTestCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.IrqTestCore.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestCore.
 * @interface xyz.swapee.wc.IIrqTestCoreFields
 */
xyz.swapee.wc.IIrqTestCoreFields = class { }
/**
 * The _IIrqTest_'s memory.
 */
xyz.swapee.wc.IIrqTestCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IIrqTestCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IIrqTestCoreFields.prototype.props = /** @type {xyz.swapee.wc.IIrqTestCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestCore} */
xyz.swapee.wc.RecordIIrqTestCore

/** @typedef {xyz.swapee.wc.IIrqTestCore} xyz.swapee.wc.BoundIIrqTestCore */

/** @typedef {xyz.swapee.wc.IrqTestCore} xyz.swapee.wc.BoundIrqTestCore */

/** @typedef {xyz.swapee.wc.IIrqTestOuterCore.Model} xyz.swapee.wc.IIrqTestCore.Model The _IIrqTest_'s memory. */

/**
 * Contains getters to cast the _IIrqTestCore_ interface.
 * @interface xyz.swapee.wc.IIrqTestCoreCaster
 */
xyz.swapee.wc.IIrqTestCoreCaster = class { }
/**
 * Cast the _IIrqTestCore_ instance into the _BoundIIrqTestCore_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestCore}
 */
xyz.swapee.wc.IIrqTestCoreCaster.prototype.asIIrqTestCore
/**
 * Access the _IrqTestCore_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestCore}
 */
xyz.swapee.wc.IIrqTestCoreCaster.prototype.superIrqTestCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IIrqTestCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestCore.__resetCore<!xyz.swapee.wc.IIrqTestCore>} xyz.swapee.wc.IIrqTestCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IIrqTestCore.resetCore} */
/**
 * Resets the _IIrqTest_ core.
 * @return {void}
 */
xyz.swapee.wc.IIrqTestCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore<!xyz.swapee.wc.IIrqTestCore>} xyz.swapee.wc.IIrqTestCore._resetIrqTestCore */
/** @typedef {typeof xyz.swapee.wc.IIrqTestCore.resetIrqTestCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IIrqTestCore.resetIrqTestCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml}  6e596152fe97c25809e030bfdfddf87a */
/** @typedef {xyz.swapee.wc.IIrqTestComputer.Initialese&xyz.swapee.wc.IIrqTestController.Initialese} xyz.swapee.wc.IIrqTestProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestProcessor)} xyz.swapee.wc.AbstractIrqTestProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestProcessor} xyz.swapee.wc.IrqTestProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestProcessor
 */
xyz.swapee.wc.AbstractIrqTestProcessor = class extends /** @type {xyz.swapee.wc.AbstractIrqTestProcessor.constructor&xyz.swapee.wc.IrqTestProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestProcessor.prototype.constructor = xyz.swapee.wc.AbstractIrqTestProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestProcessor.Initialese[]) => xyz.swapee.wc.IIrqTestProcessor} xyz.swapee.wc.IrqTestProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IIrqTestProcessorCaster&xyz.swapee.wc.IIrqTestComputer&xyz.swapee.wc.IIrqTestCore&xyz.swapee.wc.IIrqTestController)} xyz.swapee.wc.IIrqTestProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestController} xyz.swapee.wc.IIrqTestController.typeof */
/**
 * The processor to compute changes to the memory for the _IIrqTest_.
 * @interface xyz.swapee.wc.IIrqTestProcessor
 */
xyz.swapee.wc.IIrqTestProcessor = class extends /** @type {xyz.swapee.wc.IIrqTestProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IIrqTestComputer.typeof&xyz.swapee.wc.IIrqTestCore.typeof&xyz.swapee.wc.IIrqTestController.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestProcessor.Initialese>)} xyz.swapee.wc.IrqTestProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestProcessor} xyz.swapee.wc.IIrqTestProcessor.typeof */
/**
 * A concrete class of _IIrqTestProcessor_ instances.
 * @constructor xyz.swapee.wc.IrqTestProcessor
 * @implements {xyz.swapee.wc.IIrqTestProcessor} The processor to compute changes to the memory for the _IIrqTest_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestProcessor.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestProcessor = class extends /** @type {xyz.swapee.wc.IrqTestProcessor.constructor&xyz.swapee.wc.IIrqTestProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.IrqTestProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IIrqTestProcessor} */
xyz.swapee.wc.RecordIIrqTestProcessor

/** @typedef {xyz.swapee.wc.IIrqTestProcessor} xyz.swapee.wc.BoundIIrqTestProcessor */

/** @typedef {xyz.swapee.wc.IrqTestProcessor} xyz.swapee.wc.BoundIrqTestProcessor */

/**
 * Contains getters to cast the _IIrqTestProcessor_ interface.
 * @interface xyz.swapee.wc.IIrqTestProcessorCaster
 */
xyz.swapee.wc.IIrqTestProcessorCaster = class { }
/**
 * Cast the _IIrqTestProcessor_ instance into the _BoundIIrqTestProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestProcessor}
 */
xyz.swapee.wc.IIrqTestProcessorCaster.prototype.asIIrqTestProcessor
/**
 * Access the _IrqTestProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestProcessor}
 */
xyz.swapee.wc.IIrqTestProcessorCaster.prototype.superIrqTestProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/100-IrqTestMemory.xml}  02a5ad54393dff3c7b59744da377be76 */
/**
 * The memory of the _IIrqTest_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.IrqTestMemory
 */
xyz.swapee.wc.IrqTestMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.IrqTestMemory.prototype.core = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/102-IrqTestInputs.xml}  7e41453f1ba6b911fd383b7d52532b38 */
/**
 * The inputs of the _IIrqTest_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.IrqTestInputs
 */
xyz.swapee.wc.front.IrqTestInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.IrqTestInputs.prototype.core = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml}  71205dcdc292023022a0b2e6c35489ba */
/**
 * An atomic wrapper for the _IIrqTest_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.IrqTestEnv
 */
xyz.swapee.wc.IrqTestEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.IrqTestEnv.prototype.irqTest = /** @type {xyz.swapee.wc.IIrqTest} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs>&xyz.swapee.wc.IIrqTestProcessor.Initialese&xyz.swapee.wc.IIrqTestComputer.Initialese&xyz.swapee.wc.IIrqTestController.Initialese} xyz.swapee.wc.IIrqTest.Initialese */

/** @typedef {function(new: xyz.swapee.wc.IrqTest)} xyz.swapee.wc.AbstractIrqTest.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTest} xyz.swapee.wc.IrqTest.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTest` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTest
 */
xyz.swapee.wc.AbstractIrqTest = class extends /** @type {xyz.swapee.wc.AbstractIrqTest.constructor&xyz.swapee.wc.IrqTest.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTest.prototype.constructor = xyz.swapee.wc.AbstractIrqTest
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTest.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTest} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTest}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTest.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTest.Initialese[]) => xyz.swapee.wc.IIrqTest} xyz.swapee.wc.IrqTestConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTest.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IIrqTest.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IIrqTest.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IIrqTest.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.IrqTestMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.IrqTestClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestCaster&xyz.swapee.wc.IIrqTestProcessor&xyz.swapee.wc.IIrqTestComputer&xyz.swapee.wc.IIrqTestController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, null>)} xyz.swapee.wc.IIrqTest.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IIrqTest
 */
xyz.swapee.wc.IIrqTest = class extends /** @type {xyz.swapee.wc.IIrqTest.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IIrqTestProcessor.typeof&xyz.swapee.wc.IIrqTestComputer.typeof&xyz.swapee.wc.IIrqTestController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTest* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTest.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTest&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTest.Initialese>)} xyz.swapee.wc.IrqTest.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTest} xyz.swapee.wc.IIrqTest.typeof */
/**
 * A concrete class of _IIrqTest_ instances.
 * @constructor xyz.swapee.wc.IrqTest
 * @implements {xyz.swapee.wc.IIrqTest} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTest.Initialese>} ‎
 */
xyz.swapee.wc.IrqTest = class extends /** @type {xyz.swapee.wc.IrqTest.constructor&xyz.swapee.wc.IIrqTest.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTest* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTest* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTest.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.IrqTest.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTest.
 * @interface xyz.swapee.wc.IIrqTestFields
 */
xyz.swapee.wc.IIrqTestFields = class { }
/**
 * The input pins of the _IIrqTest_ port.
 */
xyz.swapee.wc.IIrqTestFields.prototype.pinout = /** @type {!xyz.swapee.wc.IIrqTest.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTest} */
xyz.swapee.wc.RecordIIrqTest

/** @typedef {xyz.swapee.wc.IIrqTest} xyz.swapee.wc.BoundIIrqTest */

/** @typedef {xyz.swapee.wc.IrqTest} xyz.swapee.wc.BoundIrqTest */

/** @typedef {xyz.swapee.wc.IIrqTestController.Inputs} xyz.swapee.wc.IIrqTest.Pinout The input pins of the _IIrqTest_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>)} xyz.swapee.wc.IIrqTestBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IIrqTestBuffer
 */
xyz.swapee.wc.IIrqTestBuffer = class extends /** @type {xyz.swapee.wc.IIrqTestBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IIrqTestBuffer.prototype.constructor = xyz.swapee.wc.IIrqTestBuffer

/**
 * A concrete class of _IIrqTestBuffer_ instances.
 * @constructor xyz.swapee.wc.IrqTestBuffer
 * @implements {xyz.swapee.wc.IIrqTestBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.IrqTestBuffer = class extends xyz.swapee.wc.IIrqTestBuffer { }
xyz.swapee.wc.IrqTestBuffer.prototype.constructor = xyz.swapee.wc.IrqTestBuffer

/**
 * Contains getters to cast the _IIrqTest_ interface.
 * @interface xyz.swapee.wc.IIrqTestCaster
 */
xyz.swapee.wc.IIrqTestCaster = class { }
/**
 * Cast the _IIrqTest_ instance into the _BoundIIrqTest_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTest}
 */
xyz.swapee.wc.IIrqTestCaster.prototype.asIIrqTest
/**
 * Access the _IrqTest_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTest}
 */
xyz.swapee.wc.IIrqTestCaster.prototype.superIrqTest

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.IrqTestMemoryPQs
 */
xyz.swapee.wc.IrqTestMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.IrqTestMemoryPQs.prototype.constructor = xyz.swapee.wc.IrqTestMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.IrqTestMemoryQPs
 * @dict
 */
xyz.swapee.wc.IrqTestMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.IrqTestMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.IrqTestMemoryPQs)} xyz.swapee.wc.IrqTestInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestMemoryPQs} xyz.swapee.wc.IrqTestMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.IrqTestInputsPQs
 */
xyz.swapee.wc.IrqTestInputsPQs = class extends /** @type {xyz.swapee.wc.IrqTestInputsPQs.constructor&xyz.swapee.wc.IrqTestMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.IrqTestInputsPQs.prototype.constructor = xyz.swapee.wc.IrqTestInputsPQs

/** @typedef {function(new: xyz.swapee.wc.IrqTestMemoryPQs)} xyz.swapee.wc.IrqTestInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.IrqTestInputsQPs
 * @dict
 */
xyz.swapee.wc.IrqTestInputsQPs = class extends /** @type {xyz.swapee.wc.IrqTestInputsQPs.constructor&xyz.swapee.wc.IrqTestMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.IrqTestInputsQPs.prototype.constructor = xyz.swapee.wc.IrqTestInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.IrqTestVdusPQs
 */
xyz.swapee.wc.IrqTestVdusPQs = class { }
xyz.swapee.wc.IrqTestVdusPQs.prototype.constructor = xyz.swapee.wc.IrqTestVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.IrqTestVdusQPs
 * @dict
 */
xyz.swapee.wc.IrqTestVdusQPs = class { }
xyz.swapee.wc.IrqTestVdusQPs.prototype.constructor = xyz.swapee.wc.IrqTestVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml}  a4af8d3a5407eca50e3e6e590cd0b593 */
/** @typedef {xyz.swapee.wc.back.IIrqTestController.Initialese&xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese&xyz.swapee.wc.IIrqTest.Initialese&xyz.swapee.wc.IIrqTestGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IIrqTestProcessor.Initialese&xyz.swapee.wc.IIrqTestComputer.Initialese&IIrqTestGenerator.Initialese} xyz.swapee.wc.IIrqTestHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestHtmlComponent)} xyz.swapee.wc.AbstractIrqTestHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestHtmlComponent} xyz.swapee.wc.IrqTestHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestHtmlComponent
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractIrqTestHtmlComponent.constructor&xyz.swapee.wc.IrqTestHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractIrqTestHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestHtmlComponent.Initialese[]) => xyz.swapee.wc.IIrqTestHtmlComponent} xyz.swapee.wc.IrqTestHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IIrqTestHtmlComponentCaster&xyz.swapee.wc.back.IIrqTestController&xyz.swapee.wc.back.IIrqTestTouchscreen&xyz.swapee.wc.IIrqTest&xyz.swapee.wc.IIrqTestGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IIrqTestProcessor&xyz.swapee.wc.IIrqTestComputer&IIrqTestGenerator)} xyz.swapee.wc.IIrqTestHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IIrqTestController} xyz.swapee.wc.back.IIrqTestController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IIrqTestTouchscreen} xyz.swapee.wc.back.IIrqTestTouchscreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IIrqTestGPU} xyz.swapee.wc.IIrqTestGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof IIrqTestGenerator} IIrqTestGenerator.typeof */
/**
 * The _IIrqTest_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IIrqTestHtmlComponent
 */
xyz.swapee.wc.IIrqTestHtmlComponent = class extends /** @type {xyz.swapee.wc.IIrqTestHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IIrqTestController.typeof&xyz.swapee.wc.back.IIrqTestTouchscreen.typeof&xyz.swapee.wc.IIrqTest.typeof&xyz.swapee.wc.IIrqTestGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IIrqTestProcessor.typeof&xyz.swapee.wc.IIrqTestComputer.typeof&IIrqTestGenerator.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese>)} xyz.swapee.wc.IrqTestHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestHtmlComponent} xyz.swapee.wc.IIrqTestHtmlComponent.typeof */
/**
 * A concrete class of _IIrqTestHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.IrqTestHtmlComponent
 * @implements {xyz.swapee.wc.IIrqTestHtmlComponent} The _IIrqTest_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestHtmlComponent = class extends /** @type {xyz.swapee.wc.IrqTestHtmlComponent.constructor&xyz.swapee.wc.IIrqTestHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.IrqTestHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IIrqTestHtmlComponent} */
xyz.swapee.wc.RecordIIrqTestHtmlComponent

/** @typedef {xyz.swapee.wc.IIrqTestHtmlComponent} xyz.swapee.wc.BoundIIrqTestHtmlComponent */

/** @typedef {xyz.swapee.wc.IrqTestHtmlComponent} xyz.swapee.wc.BoundIrqTestHtmlComponent */

/**
 * Contains getters to cast the _IIrqTestHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IIrqTestHtmlComponentCaster
 */
xyz.swapee.wc.IIrqTestHtmlComponentCaster = class { }
/**
 * Cast the _IIrqTestHtmlComponent_ instance into the _BoundIIrqTestHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestHtmlComponent}
 */
xyz.swapee.wc.IIrqTestHtmlComponentCaster.prototype.asIIrqTestHtmlComponent
/**
 * Access the _IrqTestHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestHtmlComponent}
 */
xyz.swapee.wc.IIrqTestHtmlComponentCaster.prototype.superIrqTestHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml}  aadcbc20b996cc62e9719f30f6825635 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IIrqTestElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestElement)} xyz.swapee.wc.AbstractIrqTestElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestElement} xyz.swapee.wc.IrqTestElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestElement` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestElement
 */
xyz.swapee.wc.AbstractIrqTestElement = class extends /** @type {xyz.swapee.wc.AbstractIrqTestElement.constructor&xyz.swapee.wc.IrqTestElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestElement.prototype.constructor = xyz.swapee.wc.AbstractIrqTestElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestElement.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestElement|typeof xyz.swapee.wc.IrqTestElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestElement|typeof xyz.swapee.wc.IrqTestElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestElement|typeof xyz.swapee.wc.IrqTestElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestElement.Initialese[]) => xyz.swapee.wc.IIrqTestElement} xyz.swapee.wc.IrqTestElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestElementFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs, null>)} xyz.swapee.wc.IIrqTestElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IIrqTest_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IIrqTestElement
 */
xyz.swapee.wc.IIrqTestElement = class extends /** @type {xyz.swapee.wc.IIrqTestElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IIrqTestElement.solder} */
xyz.swapee.wc.IIrqTestElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IIrqTestElement.render} */
xyz.swapee.wc.IIrqTestElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IIrqTestElement.server} */
xyz.swapee.wc.IIrqTestElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IIrqTestElement.inducer} */
xyz.swapee.wc.IIrqTestElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestElement&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestElement.Initialese>)} xyz.swapee.wc.IrqTestElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElement} xyz.swapee.wc.IIrqTestElement.typeof */
/**
 * A concrete class of _IIrqTestElement_ instances.
 * @constructor xyz.swapee.wc.IrqTestElement
 * @implements {xyz.swapee.wc.IIrqTestElement} A component description.
 *
 * The _IIrqTest_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestElement.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestElement = class extends /** @type {xyz.swapee.wc.IrqTestElement.constructor&xyz.swapee.wc.IIrqTestElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.IrqTestElement.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestElement.
 * @interface xyz.swapee.wc.IIrqTestElementFields
 */
xyz.swapee.wc.IIrqTestElementFields = class { }
/**
 * The element-specific inputs to the _IIrqTest_ component.
 */
xyz.swapee.wc.IIrqTestElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IIrqTestElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestElement} */
xyz.swapee.wc.RecordIIrqTestElement

/** @typedef {xyz.swapee.wc.IIrqTestElement} xyz.swapee.wc.BoundIIrqTestElement */

/** @typedef {xyz.swapee.wc.IrqTestElement} xyz.swapee.wc.BoundIrqTestElement */

/** @typedef {xyz.swapee.wc.IIrqTestPort.Inputs&xyz.swapee.wc.IIrqTestDisplay.Queries&xyz.swapee.wc.IIrqTestController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IIrqTestElementPort.Inputs} xyz.swapee.wc.IIrqTestElement.Inputs The element-specific inputs to the _IIrqTest_ component. */

/**
 * Contains getters to cast the _IIrqTestElement_ interface.
 * @interface xyz.swapee.wc.IIrqTestElementCaster
 */
xyz.swapee.wc.IIrqTestElementCaster = class { }
/**
 * Cast the _IIrqTestElement_ instance into the _BoundIIrqTestElement_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestElement}
 */
xyz.swapee.wc.IIrqTestElementCaster.prototype.asIIrqTestElement
/**
 * Access the _IrqTestElement_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestElement}
 */
xyz.swapee.wc.IIrqTestElementCaster.prototype.superIrqTestElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IrqTestMemory, props: !xyz.swapee.wc.IIrqTestElement.Inputs) => Object<string, *>} xyz.swapee.wc.IIrqTestElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestElement.__solder<!xyz.swapee.wc.IIrqTestElement>} xyz.swapee.wc.IIrqTestElement._solder */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.IrqTestMemory} model The model.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} props The element props.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IIrqTestOuterCore.WeakModel.Core* ⤴ *IIrqTestOuterCore.WeakModel.Core* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IIrqTestElementPort.Inputs.NoSolder* Default `false`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IIrqTestElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.IrqTestMemory, instance?: !xyz.swapee.wc.IIrqTestTouchscreen&xyz.swapee.wc.IIrqTestController) => !engineering.type.VNode} xyz.swapee.wc.IIrqTestElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestElement.__render<!xyz.swapee.wc.IIrqTestElement>} xyz.swapee.wc.IIrqTestElement._render */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.IrqTestMemory} [model] The model for the view.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IIrqTestTouchscreen&xyz.swapee.wc.IIrqTestController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IIrqTestElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.IrqTestMemory, inputs: !xyz.swapee.wc.IIrqTestElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IIrqTestElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestElement.__server<!xyz.swapee.wc.IIrqTestElement>} xyz.swapee.wc.IIrqTestElement._server */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.IrqTestMemory} memory The memory registers.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} inputs The inputs to the port.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IIrqTestOuterCore.WeakModel.Core* ⤴ *IIrqTestOuterCore.WeakModel.Core* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IIrqTestElementPort.Inputs.NoSolder* Default `false`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IIrqTestElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.IrqTestMemory, port?: !xyz.swapee.wc.IIrqTestElement.Inputs) => ?} xyz.swapee.wc.IIrqTestElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestElement.__inducer<!xyz.swapee.wc.IIrqTestElement>} xyz.swapee.wc.IIrqTestElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.IrqTestMemory} [model] The model of the component into which to induce the state.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IIrqTestOuterCore.WeakModel.Core* ⤴ *IIrqTestOuterCore.WeakModel.Core* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IIrqTestElementPort.Inputs.NoSolder* Default `false`.
 * @return {?}
 */
xyz.swapee.wc.IIrqTestElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml}  729613afdafe39d73de428a2884348a7 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IIrqTestElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestElementPort)} xyz.swapee.wc.AbstractIrqTestElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestElementPort} xyz.swapee.wc.IrqTestElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestElementPort
 */
xyz.swapee.wc.AbstractIrqTestElementPort = class extends /** @type {xyz.swapee.wc.AbstractIrqTestElementPort.constructor&xyz.swapee.wc.IrqTestElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestElementPort.prototype.constructor = xyz.swapee.wc.AbstractIrqTestElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestElementPort|typeof xyz.swapee.wc.IrqTestElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestElementPort|typeof xyz.swapee.wc.IrqTestElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestElementPort|typeof xyz.swapee.wc.IrqTestElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestElementPort.Initialese[]) => xyz.swapee.wc.IIrqTestElementPort} xyz.swapee.wc.IrqTestElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IIrqTestElementPort.Inputs>)} xyz.swapee.wc.IIrqTestElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IIrqTestElementPort
 */
xyz.swapee.wc.IIrqTestElementPort = class extends /** @type {xyz.swapee.wc.IIrqTestElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestElementPort.Initialese>)} xyz.swapee.wc.IrqTestElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElementPort} xyz.swapee.wc.IIrqTestElementPort.typeof */
/**
 * A concrete class of _IIrqTestElementPort_ instances.
 * @constructor xyz.swapee.wc.IrqTestElementPort
 * @implements {xyz.swapee.wc.IIrqTestElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestElementPort.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestElementPort = class extends /** @type {xyz.swapee.wc.IrqTestElementPort.constructor&xyz.swapee.wc.IIrqTestElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.IrqTestElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestElementPort.
 * @interface xyz.swapee.wc.IIrqTestElementPortFields
 */
xyz.swapee.wc.IIrqTestElementPortFields = class { }
/**
 * The inputs to the _IIrqTestElement_'s controller via its element port.
 */
xyz.swapee.wc.IIrqTestElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IIrqTestElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IIrqTestElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IIrqTestElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestElementPort} */
xyz.swapee.wc.RecordIIrqTestElementPort

/** @typedef {xyz.swapee.wc.IIrqTestElementPort} xyz.swapee.wc.BoundIIrqTestElementPort */

/** @typedef {xyz.swapee.wc.IrqTestElementPort} xyz.swapee.wc.BoundIrqTestElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder)} xyz.swapee.wc.IIrqTestElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder} xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _IIrqTestElement_'s controller via its element port.
 * @record xyz.swapee.wc.IIrqTestElementPort.Inputs
 */
xyz.swapee.wc.IIrqTestElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IIrqTestElementPort.Inputs.constructor&xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.IIrqTestElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IIrqTestElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.IIrqTestElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _IIrqTestElement_'s controller via its element port.
 * @record xyz.swapee.wc.IIrqTestElementPort.WeakInputs
 */
xyz.swapee.wc.IIrqTestElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IIrqTestElementPort.WeakInputs.constructor&xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.IIrqTestElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IIrqTestElementPort.WeakInputs

/**
 * Contains getters to cast the _IIrqTestElementPort_ interface.
 * @interface xyz.swapee.wc.IIrqTestElementPortCaster
 */
xyz.swapee.wc.IIrqTestElementPortCaster = class { }
/**
 * Cast the _IIrqTestElementPort_ instance into the _BoundIIrqTestElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestElementPort}
 */
xyz.swapee.wc.IIrqTestElementPortCaster.prototype.asIIrqTestElementPort
/**
 * Access the _IrqTestElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestElementPort}
 */
xyz.swapee.wc.IIrqTestElementPortCaster.prototype.superIrqTestElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml}  8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IIrqTestDesigner
 */
xyz.swapee.wc.IIrqTestDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.IrqTestClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IIrqTest />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.IrqTestClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IIrqTest />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.IrqTestClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `IrqTest` _typeof IIrqTestController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IIrqTestDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `IrqTest` _typeof IIrqTestController_
   * - `This` _typeof IIrqTestController_
   * @param {!xyz.swapee.wc.IIrqTestDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `IrqTest` _!IrqTestMemory_
   * - `This` _!IrqTestMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.IrqTestClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.IrqTestClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IIrqTestDesigner.prototype.constructor = xyz.swapee.wc.IIrqTestDesigner

/**
 * A concrete class of _IIrqTestDesigner_ instances.
 * @constructor xyz.swapee.wc.IrqTestDesigner
 * @implements {xyz.swapee.wc.IIrqTestDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.IrqTestDesigner = class extends xyz.swapee.wc.IIrqTestDesigner { }
xyz.swapee.wc.IrqTestDesigner.prototype.constructor = xyz.swapee.wc.IrqTestDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IIrqTestController} IrqTest
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IIrqTestController} IrqTest
 * @prop {typeof xyz.swapee.wc.IIrqTestController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IIrqTestDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.IrqTestMemory} IrqTest
 * @prop {!xyz.swapee.wc.IrqTestMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml}  0943077255d6ee63860d4c60329d2bb3 */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings>} xyz.swapee.wc.IIrqTestDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.IrqTestDisplay)} xyz.swapee.wc.AbstractIrqTestDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestDisplay} xyz.swapee.wc.IrqTestDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestDisplay
 */
xyz.swapee.wc.AbstractIrqTestDisplay = class extends /** @type {xyz.swapee.wc.AbstractIrqTestDisplay.constructor&xyz.swapee.wc.IrqTestDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestDisplay.prototype.constructor = xyz.swapee.wc.AbstractIrqTestDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestDisplay.Initialese[]) => xyz.swapee.wc.IIrqTestDisplay} xyz.swapee.wc.IrqTestDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.IrqTestMemory, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, xyz.swapee.wc.IIrqTestDisplay.Queries, null>)} xyz.swapee.wc.IIrqTestDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IIrqTest_.
 * @interface xyz.swapee.wc.IIrqTestDisplay
 */
xyz.swapee.wc.IIrqTestDisplay = class extends /** @type {xyz.swapee.wc.IIrqTestDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IIrqTestDisplay.paint} */
xyz.swapee.wc.IIrqTestDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestDisplay.Initialese>)} xyz.swapee.wc.IrqTestDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestDisplay} xyz.swapee.wc.IIrqTestDisplay.typeof */
/**
 * A concrete class of _IIrqTestDisplay_ instances.
 * @constructor xyz.swapee.wc.IrqTestDisplay
 * @implements {xyz.swapee.wc.IIrqTestDisplay} Display for presenting information from the _IIrqTest_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestDisplay.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestDisplay = class extends /** @type {xyz.swapee.wc.IrqTestDisplay.constructor&xyz.swapee.wc.IIrqTestDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.IrqTestDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestDisplay.
 * @interface xyz.swapee.wc.IIrqTestDisplayFields
 */
xyz.swapee.wc.IIrqTestDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IIrqTestDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IIrqTestDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IIrqTestDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IIrqTestDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestDisplay} */
xyz.swapee.wc.RecordIIrqTestDisplay

/** @typedef {xyz.swapee.wc.IIrqTestDisplay} xyz.swapee.wc.BoundIIrqTestDisplay */

/** @typedef {xyz.swapee.wc.IrqTestDisplay} xyz.swapee.wc.BoundIrqTestDisplay */

/** @typedef {xyz.swapee.wc.IIrqTestDisplay.Queries} xyz.swapee.wc.IIrqTestDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IIrqTestDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IIrqTestDisplay_ interface.
 * @interface xyz.swapee.wc.IIrqTestDisplayCaster
 */
xyz.swapee.wc.IIrqTestDisplayCaster = class { }
/**
 * Cast the _IIrqTestDisplay_ instance into the _BoundIIrqTestDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestDisplay}
 */
xyz.swapee.wc.IIrqTestDisplayCaster.prototype.asIIrqTestDisplay
/**
 * Cast the _IIrqTestDisplay_ instance into the _BoundIIrqTestTouchscreen_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestTouchscreen}
 */
xyz.swapee.wc.IIrqTestDisplayCaster.prototype.asIIrqTestTouchscreen
/**
 * Access the _IrqTestDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestDisplay}
 */
xyz.swapee.wc.IIrqTestDisplayCaster.prototype.superIrqTestDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.IrqTestMemory, land: null) => void} xyz.swapee.wc.IIrqTestDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestDisplay.__paint<!xyz.swapee.wc.IIrqTestDisplay>} xyz.swapee.wc.IIrqTestDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IIrqTestDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.IrqTestMemory} memory The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IIrqTestDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml}  29c4729e9de06b1395d747b24d4e0565 */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.IrqTestClasses>} xyz.swapee.wc.back.IIrqTestDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.IrqTestDisplay)} xyz.swapee.wc.back.AbstractIrqTestDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IrqTestDisplay} xyz.swapee.wc.back.IrqTestDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IIrqTestDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractIrqTestDisplay
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractIrqTestDisplay.constructor&xyz.swapee.wc.back.IrqTestDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractIrqTestDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractIrqTestDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractIrqTestDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IIrqTestDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IrqTestClasses, null>)} xyz.swapee.wc.back.IIrqTestDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IIrqTestDisplay
 */
xyz.swapee.wc.back.IIrqTestDisplay = class extends /** @type {xyz.swapee.wc.back.IIrqTestDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IIrqTestDisplay.paint} */
xyz.swapee.wc.back.IIrqTestDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestDisplay.Initialese>)} xyz.swapee.wc.back.IrqTestDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IIrqTestDisplay} xyz.swapee.wc.back.IIrqTestDisplay.typeof */
/**
 * A concrete class of _IIrqTestDisplay_ instances.
 * @constructor xyz.swapee.wc.back.IrqTestDisplay
 * @implements {xyz.swapee.wc.back.IIrqTestDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.IrqTestDisplay = class extends /** @type {xyz.swapee.wc.back.IrqTestDisplay.constructor&xyz.swapee.wc.back.IIrqTestDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.IrqTestDisplay.prototype.constructor = xyz.swapee.wc.back.IrqTestDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.IrqTestDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IIrqTestDisplay} */
xyz.swapee.wc.back.RecordIIrqTestDisplay

/** @typedef {xyz.swapee.wc.back.IIrqTestDisplay} xyz.swapee.wc.back.BoundIIrqTestDisplay */

/** @typedef {xyz.swapee.wc.back.IrqTestDisplay} xyz.swapee.wc.back.BoundIrqTestDisplay */

/**
 * Contains getters to cast the _IIrqTestDisplay_ interface.
 * @interface xyz.swapee.wc.back.IIrqTestDisplayCaster
 */
xyz.swapee.wc.back.IIrqTestDisplayCaster = class { }
/**
 * Cast the _IIrqTestDisplay_ instance into the _BoundIIrqTestDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIIrqTestDisplay}
 */
xyz.swapee.wc.back.IIrqTestDisplayCaster.prototype.asIIrqTestDisplay
/**
 * Access the _IrqTestDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundIrqTestDisplay}
 */
xyz.swapee.wc.back.IIrqTestDisplayCaster.prototype.superIrqTestDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.IrqTestMemory, land?: null) => void} xyz.swapee.wc.back.IIrqTestDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IIrqTestDisplay.__paint<!xyz.swapee.wc.back.IIrqTestDisplay>} xyz.swapee.wc.back.IIrqTestDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IIrqTestDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.IrqTestMemory} [memory] The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IIrqTestDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IIrqTestDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/41-IrqTestClasses.xml}  2060d8e7a8edfa939198f8e2ebc2ce78 */
/**
 * The classes of the _IIrqTestDisplay_.
 * @record xyz.swapee.wc.IrqTestClasses
 */
xyz.swapee.wc.IrqTestClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IrqTestClasses.prototype.props = /** @type {xyz.swapee.wc.IrqTestClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml}  4baa941ccd1b081a82b4ad055628d3a3 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>} xyz.swapee.wc.IIrqTestController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.IrqTestController)} xyz.swapee.wc.AbstractIrqTestController.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestController} xyz.swapee.wc.IrqTestController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestController` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestController
 */
xyz.swapee.wc.AbstractIrqTestController = class extends /** @type {xyz.swapee.wc.AbstractIrqTestController.constructor&xyz.swapee.wc.IrqTestController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestController.prototype.constructor = xyz.swapee.wc.AbstractIrqTestController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestController.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestController.Initialese[]) => xyz.swapee.wc.IIrqTestController} xyz.swapee.wc.IrqTestControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IrqTestMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IIrqTestController.Inputs>)} xyz.swapee.wc.IIrqTestController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IIrqTestController
 */
xyz.swapee.wc.IIrqTestController = class extends /** @type {xyz.swapee.wc.IIrqTestController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IIrqTestController.resetPort} */
xyz.swapee.wc.IIrqTestController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestController&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestController.Initialese>)} xyz.swapee.wc.IrqTestController.constructor */
/**
 * A concrete class of _IIrqTestController_ instances.
 * @constructor xyz.swapee.wc.IrqTestController
 * @implements {xyz.swapee.wc.IIrqTestController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestController.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestController = class extends /** @type {xyz.swapee.wc.IrqTestController.constructor&xyz.swapee.wc.IIrqTestController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.IrqTestController.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestController.
 * @interface xyz.swapee.wc.IIrqTestControllerFields
 */
xyz.swapee.wc.IIrqTestControllerFields = class { }
/**
 * The inputs to the _IIrqTest_'s controller.
 */
xyz.swapee.wc.IIrqTestControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IIrqTestController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IIrqTestControllerFields.prototype.props = /** @type {xyz.swapee.wc.IIrqTestController} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestController} */
xyz.swapee.wc.RecordIIrqTestController

/** @typedef {xyz.swapee.wc.IIrqTestController} xyz.swapee.wc.BoundIIrqTestController */

/** @typedef {xyz.swapee.wc.IrqTestController} xyz.swapee.wc.BoundIrqTestController */

/** @typedef {xyz.swapee.wc.IIrqTestPort.Inputs} xyz.swapee.wc.IIrqTestController.Inputs The inputs to the _IIrqTest_'s controller. */

/** @typedef {xyz.swapee.wc.IIrqTestPort.WeakInputs} xyz.swapee.wc.IIrqTestController.WeakInputs The inputs to the _IIrqTest_'s controller. */

/**
 * Contains getters to cast the _IIrqTestController_ interface.
 * @interface xyz.swapee.wc.IIrqTestControllerCaster
 */
xyz.swapee.wc.IIrqTestControllerCaster = class { }
/**
 * Cast the _IIrqTestController_ instance into the _BoundIIrqTestController_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestController}
 */
xyz.swapee.wc.IIrqTestControllerCaster.prototype.asIIrqTestController
/**
 * Cast the _IIrqTestController_ instance into the _BoundIIrqTestProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestProcessor}
 */
xyz.swapee.wc.IIrqTestControllerCaster.prototype.asIIrqTestProcessor
/**
 * Access the _IrqTestController_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestController}
 */
xyz.swapee.wc.IIrqTestControllerCaster.prototype.superIrqTestController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IIrqTestController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestController.__resetPort<!xyz.swapee.wc.IIrqTestController>} xyz.swapee.wc.IIrqTestController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IIrqTestController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IIrqTestController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml}  9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IIrqTestController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.IrqTestController)} xyz.swapee.wc.front.AbstractIrqTestController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IrqTestController} xyz.swapee.wc.front.IrqTestController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IIrqTestController` interface.
 * @constructor xyz.swapee.wc.front.AbstractIrqTestController
 */
xyz.swapee.wc.front.AbstractIrqTestController = class extends /** @type {xyz.swapee.wc.front.AbstractIrqTestController.constructor&xyz.swapee.wc.front.IrqTestController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractIrqTestController.prototype.constructor = xyz.swapee.wc.front.AbstractIrqTestController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractIrqTestController.class = /** @type {typeof xyz.swapee.wc.front.AbstractIrqTestController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IIrqTestController.Initialese[]) => xyz.swapee.wc.front.IIrqTestController} xyz.swapee.wc.front.IrqTestControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IIrqTestControllerCaster&xyz.swapee.wc.front.IIrqTestControllerAT)} xyz.swapee.wc.front.IIrqTestController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IIrqTestControllerAT} xyz.swapee.wc.front.IIrqTestControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IIrqTestController
 */
xyz.swapee.wc.front.IIrqTestController = class extends /** @type {xyz.swapee.wc.front.IIrqTestController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IIrqTestControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IIrqTestController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestController.Initialese>)} xyz.swapee.wc.front.IrqTestController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IIrqTestController} xyz.swapee.wc.front.IIrqTestController.typeof */
/**
 * A concrete class of _IIrqTestController_ instances.
 * @constructor xyz.swapee.wc.front.IrqTestController
 * @implements {xyz.swapee.wc.front.IIrqTestController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestController.Initialese>} ‎
 */
xyz.swapee.wc.front.IrqTestController = class extends /** @type {xyz.swapee.wc.front.IrqTestController.constructor&xyz.swapee.wc.front.IIrqTestController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IrqTestController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.IrqTestController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IIrqTestController} */
xyz.swapee.wc.front.RecordIIrqTestController

/** @typedef {xyz.swapee.wc.front.IIrqTestController} xyz.swapee.wc.front.BoundIIrqTestController */

/** @typedef {xyz.swapee.wc.front.IrqTestController} xyz.swapee.wc.front.BoundIrqTestController */

/**
 * Contains getters to cast the _IIrqTestController_ interface.
 * @interface xyz.swapee.wc.front.IIrqTestControllerCaster
 */
xyz.swapee.wc.front.IIrqTestControllerCaster = class { }
/**
 * Cast the _IIrqTestController_ instance into the _BoundIIrqTestController_ type.
 * @type {!xyz.swapee.wc.front.BoundIIrqTestController}
 */
xyz.swapee.wc.front.IIrqTestControllerCaster.prototype.asIIrqTestController
/**
 * Access the _IrqTestController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundIrqTestController}
 */
xyz.swapee.wc.front.IIrqTestControllerCaster.prototype.superIrqTestController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml}  58eb466d38c7ce33d05c95e9b4a27f2f */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs>&xyz.swapee.wc.IIrqTestController.Initialese} xyz.swapee.wc.back.IIrqTestController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.IrqTestController)} xyz.swapee.wc.back.AbstractIrqTestController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IrqTestController} xyz.swapee.wc.back.IrqTestController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IIrqTestController` interface.
 * @constructor xyz.swapee.wc.back.AbstractIrqTestController
 */
xyz.swapee.wc.back.AbstractIrqTestController = class extends /** @type {xyz.swapee.wc.back.AbstractIrqTestController.constructor&xyz.swapee.wc.back.IrqTestController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractIrqTestController.prototype.constructor = xyz.swapee.wc.back.AbstractIrqTestController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractIrqTestController.class = /** @type {typeof xyz.swapee.wc.back.AbstractIrqTestController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IIrqTestController.Initialese[]) => xyz.swapee.wc.back.IIrqTestController} xyz.swapee.wc.back.IrqTestControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IIrqTestControllerCaster&xyz.swapee.wc.IIrqTestController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IIrqTestController.Inputs>)} xyz.swapee.wc.back.IIrqTestController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IIrqTestController
 */
xyz.swapee.wc.back.IIrqTestController = class extends /** @type {xyz.swapee.wc.back.IIrqTestController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IIrqTestController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IIrqTestController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestController.Initialese>)} xyz.swapee.wc.back.IrqTestController.constructor */
/**
 * A concrete class of _IIrqTestController_ instances.
 * @constructor xyz.swapee.wc.back.IrqTestController
 * @implements {xyz.swapee.wc.back.IIrqTestController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestController.Initialese>} ‎
 */
xyz.swapee.wc.back.IrqTestController = class extends /** @type {xyz.swapee.wc.back.IrqTestController.constructor&xyz.swapee.wc.back.IIrqTestController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IrqTestController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.IrqTestController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IIrqTestController} */
xyz.swapee.wc.back.RecordIIrqTestController

/** @typedef {xyz.swapee.wc.back.IIrqTestController} xyz.swapee.wc.back.BoundIIrqTestController */

/** @typedef {xyz.swapee.wc.back.IrqTestController} xyz.swapee.wc.back.BoundIrqTestController */

/**
 * Contains getters to cast the _IIrqTestController_ interface.
 * @interface xyz.swapee.wc.back.IIrqTestControllerCaster
 */
xyz.swapee.wc.back.IIrqTestControllerCaster = class { }
/**
 * Cast the _IIrqTestController_ instance into the _BoundIIrqTestController_ type.
 * @type {!xyz.swapee.wc.back.BoundIIrqTestController}
 */
xyz.swapee.wc.back.IIrqTestControllerCaster.prototype.asIIrqTestController
/**
 * Access the _IrqTestController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundIrqTestController}
 */
xyz.swapee.wc.back.IIrqTestControllerCaster.prototype.superIrqTestController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml}  285ac5531c1323b1989a3a12419f1165 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IIrqTestController.Initialese} xyz.swapee.wc.back.IIrqTestControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.IrqTestControllerAR)} xyz.swapee.wc.back.AbstractIrqTestControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IrqTestControllerAR} xyz.swapee.wc.back.IrqTestControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IIrqTestControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractIrqTestControllerAR
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractIrqTestControllerAR.constructor&xyz.swapee.wc.back.IrqTestControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractIrqTestControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractIrqTestControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractIrqTestControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IIrqTestControllerAR.Initialese[]) => xyz.swapee.wc.back.IIrqTestControllerAR} xyz.swapee.wc.back.IrqTestControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IIrqTestControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IIrqTestController)} xyz.swapee.wc.back.IIrqTestControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IIrqTestControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IIrqTestControllerAR
 */
xyz.swapee.wc.back.IIrqTestControllerAR = class extends /** @type {xyz.swapee.wc.back.IIrqTestControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IIrqTestController.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IIrqTestControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese>)} xyz.swapee.wc.back.IrqTestControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IIrqTestControllerAR} xyz.swapee.wc.back.IIrqTestControllerAR.typeof */
/**
 * A concrete class of _IIrqTestControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.IrqTestControllerAR
 * @implements {xyz.swapee.wc.back.IIrqTestControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IIrqTestControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.IrqTestControllerAR = class extends /** @type {xyz.swapee.wc.back.IrqTestControllerAR.constructor&xyz.swapee.wc.back.IIrqTestControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IrqTestControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.IrqTestControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IIrqTestControllerAR} */
xyz.swapee.wc.back.RecordIIrqTestControllerAR

/** @typedef {xyz.swapee.wc.back.IIrqTestControllerAR} xyz.swapee.wc.back.BoundIIrqTestControllerAR */

/** @typedef {xyz.swapee.wc.back.IrqTestControllerAR} xyz.swapee.wc.back.BoundIrqTestControllerAR */

/**
 * Contains getters to cast the _IIrqTestControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IIrqTestControllerARCaster
 */
xyz.swapee.wc.back.IIrqTestControllerARCaster = class { }
/**
 * Cast the _IIrqTestControllerAR_ instance into the _BoundIIrqTestControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIIrqTestControllerAR}
 */
xyz.swapee.wc.back.IIrqTestControllerARCaster.prototype.asIIrqTestControllerAR
/**
 * Access the _IrqTestControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundIrqTestControllerAR}
 */
xyz.swapee.wc.back.IIrqTestControllerARCaster.prototype.superIrqTestControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml}  cf2367e24c3b3ea189fcce5aca8ae914 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IIrqTestControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.IrqTestControllerAT)} xyz.swapee.wc.front.AbstractIrqTestControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IrqTestControllerAT} xyz.swapee.wc.front.IrqTestControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IIrqTestControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractIrqTestControllerAT
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractIrqTestControllerAT.constructor&xyz.swapee.wc.front.IrqTestControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractIrqTestControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractIrqTestControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractIrqTestControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IIrqTestControllerAT.Initialese[]) => xyz.swapee.wc.front.IIrqTestControllerAT} xyz.swapee.wc.front.IrqTestControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IIrqTestControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IIrqTestControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IIrqTestControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IIrqTestControllerAT
 */
xyz.swapee.wc.front.IIrqTestControllerAT = class extends /** @type {xyz.swapee.wc.front.IIrqTestControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IIrqTestControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese>)} xyz.swapee.wc.front.IrqTestControllerAT.constructor */
/**
 * A concrete class of _IIrqTestControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.IrqTestControllerAT
 * @implements {xyz.swapee.wc.front.IIrqTestControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IIrqTestControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.IrqTestControllerAT = class extends /** @type {xyz.swapee.wc.front.IrqTestControllerAT.constructor&xyz.swapee.wc.front.IIrqTestControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IrqTestControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.IrqTestControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IIrqTestControllerAT} */
xyz.swapee.wc.front.RecordIIrqTestControllerAT

/** @typedef {xyz.swapee.wc.front.IIrqTestControllerAT} xyz.swapee.wc.front.BoundIIrqTestControllerAT */

/** @typedef {xyz.swapee.wc.front.IrqTestControllerAT} xyz.swapee.wc.front.BoundIrqTestControllerAT */

/**
 * Contains getters to cast the _IIrqTestControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IIrqTestControllerATCaster
 */
xyz.swapee.wc.front.IIrqTestControllerATCaster = class { }
/**
 * Cast the _IIrqTestControllerAT_ instance into the _BoundIIrqTestControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIIrqTestControllerAT}
 */
xyz.swapee.wc.front.IIrqTestControllerATCaster.prototype.asIIrqTestControllerAT
/**
 * Access the _IrqTestControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundIrqTestControllerAT}
 */
xyz.swapee.wc.front.IIrqTestControllerATCaster.prototype.superIrqTestControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml}  a371c2115fe076d48b1a9e737748cbcf */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null>&IIrqTestInterruptLine.Initialese&xyz.swapee.wc.IIrqTestDisplay.Initialese} xyz.swapee.wc.IIrqTestTouchscreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.IrqTestTouchscreen)} xyz.swapee.wc.AbstractIrqTestTouchscreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestTouchscreen} xyz.swapee.wc.IrqTestTouchscreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestTouchscreen` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestTouchscreen
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen = class extends /** @type {xyz.swapee.wc.AbstractIrqTestTouchscreen.constructor&xyz.swapee.wc.IrqTestTouchscreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestTouchscreen.prototype.constructor = xyz.swapee.wc.AbstractIrqTestTouchscreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestTouchscreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestTouchscreen.Initialese[]) => xyz.swapee.wc.IIrqTestTouchscreen} xyz.swapee.wc.IrqTestTouchscreenConstructor */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestTouchscreenFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestTouchscreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null, null>&xyz.swapee.wc.front.IIrqTestController&IIrqTestInterruptLine&xyz.swapee.wc.IIrqTestDisplay)} xyz.swapee.wc.IIrqTestTouchscreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/** @typedef {typeof IIrqTestInterruptLine} IIrqTestInterruptLine.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @interface xyz.swapee.wc.IIrqTestTouchscreen
 */
xyz.swapee.wc.IIrqTestTouchscreen = class extends /** @type {xyz.swapee.wc.IIrqTestTouchscreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IIrqTestController.typeof&IIrqTestInterruptLine.typeof&xyz.swapee.wc.IIrqTestDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestTouchscreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestTouchscreen.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded} */
xyz.swapee.wc.IIrqTestTouchscreen.prototype.stashKeyboardItemAfterMenuExpanded = function() {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestTouchscreen&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestTouchscreen.Initialese>)} xyz.swapee.wc.IrqTestTouchscreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IIrqTestTouchscreen} xyz.swapee.wc.IIrqTestTouchscreen.typeof */
/**
 * A concrete class of _IIrqTestTouchscreen_ instances.
 * @constructor xyz.swapee.wc.IrqTestTouchscreen
 * @implements {xyz.swapee.wc.IIrqTestTouchscreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display and handling user gestures received through the
 * interrupt line.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestTouchscreen.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestTouchscreen = class extends /** @type {xyz.swapee.wc.IrqTestTouchscreen.constructor&xyz.swapee.wc.IIrqTestTouchscreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestTouchscreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestTouchscreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestTouchscreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.IrqTestTouchscreen.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestTouchscreen.
 * @interface xyz.swapee.wc.IIrqTestTouchscreenFields
 */
xyz.swapee.wc.IIrqTestTouchscreenFields = class { }
/**
 * . Default `null`.
 */
xyz.swapee.wc.IIrqTestTouchscreenFields.prototype.keyboardItem = /** @type {HTMLElement} */ (void 0)
/**
 * . Default `false`.
 */
xyz.swapee.wc.IIrqTestTouchscreenFields.prototype.menuExpanded = /** @type {boolean} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestTouchscreen} */
xyz.swapee.wc.RecordIIrqTestTouchscreen

/** @typedef {xyz.swapee.wc.IIrqTestTouchscreen} xyz.swapee.wc.BoundIIrqTestTouchscreen */

/** @typedef {xyz.swapee.wc.IrqTestTouchscreen} xyz.swapee.wc.BoundIrqTestTouchscreen */

/**
 * Contains getters to cast the _IIrqTestTouchscreen_ interface.
 * @interface xyz.swapee.wc.IIrqTestTouchscreenCaster
 */
xyz.swapee.wc.IIrqTestTouchscreenCaster = class { }
/**
 * Cast the _IIrqTestTouchscreen_ instance into the _BoundIIrqTestTouchscreen_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestTouchscreen}
 */
xyz.swapee.wc.IIrqTestTouchscreenCaster.prototype.asIIrqTestTouchscreen
/**
 * Access the _IrqTestTouchscreen_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestTouchscreen}
 */
xyz.swapee.wc.IIrqTestTouchscreenCaster.prototype.superIrqTestTouchscreen

/**
 * @typedef {(this: THIS, memory: *) => ?} xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded<!xyz.swapee.wc.IIrqTestTouchscreen>} xyz.swapee.wc.IIrqTestTouchscreen._stashKeyboardItemAfterMenuExpanded */
/** @typedef {typeof xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded} */
/**
 * Sets the keyboard item to the "selected" one when the menu is expanded, so
 * that the user can start navigating using the keyboard.
 * @param {*} memory
 * @return {?}
 */
xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded = function(memory) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IIrqTestTouchscreen
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml}  e65c5285355b12f10099372d112274e5 */
/** @typedef {xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.IrqTestTouchscreen)} xyz.swapee.wc.back.AbstractIrqTestTouchscreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IrqTestTouchscreen} xyz.swapee.wc.back.IrqTestTouchscreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IIrqTestTouchscreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractIrqTestTouchscreen
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen = class extends /** @type {xyz.swapee.wc.back.AbstractIrqTestTouchscreen.constructor&xyz.swapee.wc.back.IrqTestTouchscreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.prototype.constructor = xyz.swapee.wc.back.AbstractIrqTestTouchscreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese[]) => xyz.swapee.wc.back.IIrqTestTouchscreen} xyz.swapee.wc.back.IrqTestTouchscreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IIrqTestTouchscreenCaster&xyz.swapee.wc.back.IIrqTestTouchscreenAT)} xyz.swapee.wc.back.IIrqTestTouchscreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IIrqTestTouchscreenAT} xyz.swapee.wc.back.IIrqTestTouchscreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IIrqTestTouchscreen
 */
xyz.swapee.wc.back.IIrqTestTouchscreen = class extends /** @type {xyz.swapee.wc.back.IIrqTestTouchscreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IIrqTestTouchscreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestTouchscreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IIrqTestTouchscreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestTouchscreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese>)} xyz.swapee.wc.back.IrqTestTouchscreen.constructor */
/**
 * A concrete class of _IIrqTestTouchscreen_ instances.
 * @constructor xyz.swapee.wc.back.IrqTestTouchscreen
 * @implements {xyz.swapee.wc.back.IIrqTestTouchscreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese>} ‎
 */
xyz.swapee.wc.back.IrqTestTouchscreen = class extends /** @type {xyz.swapee.wc.back.IrqTestTouchscreen.constructor&xyz.swapee.wc.back.IIrqTestTouchscreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestTouchscreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestTouchscreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IrqTestTouchscreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.IrqTestTouchscreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IIrqTestTouchscreen} */
xyz.swapee.wc.back.RecordIIrqTestTouchscreen

/** @typedef {xyz.swapee.wc.back.IIrqTestTouchscreen} xyz.swapee.wc.back.BoundIIrqTestTouchscreen */

/** @typedef {xyz.swapee.wc.back.IrqTestTouchscreen} xyz.swapee.wc.back.BoundIrqTestTouchscreen */

/**
 * Contains getters to cast the _IIrqTestTouchscreen_ interface.
 * @interface xyz.swapee.wc.back.IIrqTestTouchscreenCaster
 */
xyz.swapee.wc.back.IIrqTestTouchscreenCaster = class { }
/**
 * Cast the _IIrqTestTouchscreen_ instance into the _BoundIIrqTestTouchscreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIIrqTestTouchscreen}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenCaster.prototype.asIIrqTestTouchscreen
/**
 * Access the _IrqTestTouchscreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundIrqTestTouchscreen}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenCaster.prototype.superIrqTestTouchscreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml}  4fa34f26dea3f86d63f4c1df79276bfd */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IIrqTestTouchscreen.Initialese} xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.IrqTestTouchscreenAR)} xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR} xyz.swapee.wc.front.IrqTestTouchscreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IIrqTestTouchscreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.constructor&xyz.swapee.wc.front.IrqTestTouchscreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese[]) => xyz.swapee.wc.front.IIrqTestTouchscreenAR} xyz.swapee.wc.front.IrqTestTouchscreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IIrqTestTouchscreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IIrqTestTouchscreen)} xyz.swapee.wc.front.IIrqTestTouchscreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IIrqTestTouchscreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IIrqTestTouchscreenAR
 */
xyz.swapee.wc.front.IIrqTestTouchscreenAR = class extends /** @type {xyz.swapee.wc.front.IIrqTestTouchscreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IIrqTestTouchscreen.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestTouchscreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IIrqTestTouchscreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestTouchscreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese>)} xyz.swapee.wc.front.IrqTestTouchscreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IIrqTestTouchscreenAR} xyz.swapee.wc.front.IIrqTestTouchscreenAR.typeof */
/**
 * A concrete class of _IIrqTestTouchscreenAR_ instances.
 * @constructor xyz.swapee.wc.front.IrqTestTouchscreenAR
 * @implements {xyz.swapee.wc.front.IIrqTestTouchscreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IIrqTestTouchscreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.IrqTestTouchscreenAR = class extends /** @type {xyz.swapee.wc.front.IrqTestTouchscreenAR.constructor&xyz.swapee.wc.front.IIrqTestTouchscreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestTouchscreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestTouchscreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IrqTestTouchscreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.IrqTestTouchscreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IIrqTestTouchscreenAR} */
xyz.swapee.wc.front.RecordIIrqTestTouchscreenAR

/** @typedef {xyz.swapee.wc.front.IIrqTestTouchscreenAR} xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR */

/** @typedef {xyz.swapee.wc.front.IrqTestTouchscreenAR} xyz.swapee.wc.front.BoundIrqTestTouchscreenAR */

/**
 * Contains getters to cast the _IIrqTestTouchscreenAR_ interface.
 * @interface xyz.swapee.wc.front.IIrqTestTouchscreenARCaster
 */
xyz.swapee.wc.front.IIrqTestTouchscreenARCaster = class { }
/**
 * Cast the _IIrqTestTouchscreenAR_ instance into the _BoundIIrqTestTouchscreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.IIrqTestTouchscreenARCaster.prototype.asIIrqTestTouchscreenAR
/**
 * Access the _IrqTestTouchscreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundIrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.IIrqTestTouchscreenARCaster.prototype.superIrqTestTouchscreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml}  66ba436809d812aad641c755e455f946 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.IrqTestTouchscreenAT)} xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT} xyz.swapee.wc.back.IrqTestTouchscreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IIrqTestTouchscreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.constructor&xyz.swapee.wc.back.IrqTestTouchscreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese[]) => xyz.swapee.wc.back.IIrqTestTouchscreenAT} xyz.swapee.wc.back.IrqTestTouchscreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IIrqTestTouchscreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IIrqTestTouchscreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IIrqTestTouchscreenAR_ trait.
 * @interface xyz.swapee.wc.back.IIrqTestTouchscreenAT
 */
xyz.swapee.wc.back.IIrqTestTouchscreenAT = class extends /** @type {xyz.swapee.wc.back.IIrqTestTouchscreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestTouchscreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IIrqTestTouchscreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestTouchscreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese>)} xyz.swapee.wc.back.IrqTestTouchscreenAT.constructor */
/**
 * A concrete class of _IIrqTestTouchscreenAT_ instances.
 * @constructor xyz.swapee.wc.back.IrqTestTouchscreenAT
 * @implements {xyz.swapee.wc.back.IIrqTestTouchscreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IIrqTestTouchscreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.IrqTestTouchscreenAT = class extends /** @type {xyz.swapee.wc.back.IrqTestTouchscreenAT.constructor&xyz.swapee.wc.back.IIrqTestTouchscreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestTouchscreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestTouchscreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IrqTestTouchscreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IrqTestTouchscreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IIrqTestTouchscreenAT} */
xyz.swapee.wc.back.RecordIIrqTestTouchscreenAT

/** @typedef {xyz.swapee.wc.back.IIrqTestTouchscreenAT} xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT */

/** @typedef {xyz.swapee.wc.back.IrqTestTouchscreenAT} xyz.swapee.wc.back.BoundIrqTestTouchscreenAT */

/**
 * Contains getters to cast the _IIrqTestTouchscreenAT_ interface.
 * @interface xyz.swapee.wc.back.IIrqTestTouchscreenATCaster
 */
xyz.swapee.wc.back.IIrqTestTouchscreenATCaster = class { }
/**
 * Cast the _IIrqTestTouchscreenAT_ instance into the _BoundIIrqTestTouchscreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenATCaster.prototype.asIIrqTestTouchscreenAT
/**
 * Access the _IrqTestTouchscreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenATCaster.prototype.superIrqTestTouchscreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IIrqTestDisplay.Initialese} xyz.swapee.wc.IIrqTestGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.IrqTestGPU)} xyz.swapee.wc.AbstractIrqTestGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.IrqTestGPU} xyz.swapee.wc.IrqTestGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IIrqTestGPU` interface.
 * @constructor xyz.swapee.wc.AbstractIrqTestGPU
 */
xyz.swapee.wc.AbstractIrqTestGPU = class extends /** @type {xyz.swapee.wc.AbstractIrqTestGPU.constructor&xyz.swapee.wc.IrqTestGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractIrqTestGPU.prototype.constructor = xyz.swapee.wc.AbstractIrqTestGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractIrqTestGPU.class = /** @type {typeof xyz.swapee.wc.AbstractIrqTestGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractIrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IIrqTestGPU.Initialese[]) => xyz.swapee.wc.IIrqTestGPU} xyz.swapee.wc.IrqTestGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IIrqTestGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IIrqTestGPUCaster&com.webcircuits.IBrowserView<.!IrqTestMemory,>&xyz.swapee.wc.back.IIrqTestDisplay)} xyz.swapee.wc.IIrqTestGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!IrqTestMemory,>} com.webcircuits.IBrowserView<.!IrqTestMemory,>.typeof */
/**
 * Handles the periphery of the _IIrqTestDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IIrqTestGPU
 */
xyz.swapee.wc.IIrqTestGPU = class extends /** @type {xyz.swapee.wc.IIrqTestGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!IrqTestMemory,>.typeof&xyz.swapee.wc.back.IIrqTestDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IIrqTestGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IIrqTestGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IIrqTestGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestGPU.Initialese>)} xyz.swapee.wc.IrqTestGPU.constructor */
/**
 * A concrete class of _IIrqTestGPU_ instances.
 * @constructor xyz.swapee.wc.IrqTestGPU
 * @implements {xyz.swapee.wc.IIrqTestGPU} Handles the periphery of the _IIrqTestDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestGPU.Initialese>} ‎
 */
xyz.swapee.wc.IrqTestGPU = class extends /** @type {xyz.swapee.wc.IrqTestGPU.constructor&xyz.swapee.wc.IIrqTestGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IIrqTestGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IIrqTestGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IrqTestGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.IrqTestGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IIrqTestGPU.
 * @interface xyz.swapee.wc.IIrqTestGPUFields
 */
xyz.swapee.wc.IIrqTestGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IIrqTestGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IIrqTestGPU} */
xyz.swapee.wc.RecordIIrqTestGPU

/** @typedef {xyz.swapee.wc.IIrqTestGPU} xyz.swapee.wc.BoundIIrqTestGPU */

/** @typedef {xyz.swapee.wc.IrqTestGPU} xyz.swapee.wc.BoundIrqTestGPU */

/**
 * Contains getters to cast the _IIrqTestGPU_ interface.
 * @interface xyz.swapee.wc.IIrqTestGPUCaster
 */
xyz.swapee.wc.IIrqTestGPUCaster = class { }
/**
 * Cast the _IIrqTestGPU_ instance into the _BoundIIrqTestGPU_ type.
 * @type {!xyz.swapee.wc.BoundIIrqTestGPU}
 */
xyz.swapee.wc.IIrqTestGPUCaster.prototype.asIIrqTestGPU
/**
 * Access the _IrqTestGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundIrqTestGPU}
 */
xyz.swapee.wc.IIrqTestGPUCaster.prototype.superIrqTestGPU

// nss:xyz.swapee.wc
/* @typal-end */