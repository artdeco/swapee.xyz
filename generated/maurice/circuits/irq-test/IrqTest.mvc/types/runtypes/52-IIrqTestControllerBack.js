/** @const {?} */ $xyz.swapee.wc.back.IIrqTestController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IIrqTestController.Initialese filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
$xyz.swapee.wc.back.IIrqTestController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IIrqTestController.Initialese} */
xyz.swapee.wc.back.IIrqTestController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IIrqTestController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IIrqTestControllerCaster filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/** @interface */
$xyz.swapee.wc.back.IIrqTestControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIIrqTestController} */
$xyz.swapee.wc.back.IIrqTestControllerCaster.prototype.asIIrqTestController
/** @type {!xyz.swapee.wc.back.BoundIrqTestController} */
$xyz.swapee.wc.back.IIrqTestControllerCaster.prototype.superIrqTestController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestControllerCaster}
 */
xyz.swapee.wc.back.IIrqTestControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IIrqTestController filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.IIrqTestController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
$xyz.swapee.wc.back.IIrqTestController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestController}
 */
xyz.swapee.wc.back.IIrqTestController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IrqTestController filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestController.Initialese>}
 */
$xyz.swapee.wc.back.IrqTestController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.IrqTestController
/** @type {function(new: xyz.swapee.wc.back.IIrqTestController, ...!xyz.swapee.wc.back.IIrqTestController.Initialese)} */
xyz.swapee.wc.back.IrqTestController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.IrqTestController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.AbstractIrqTestController filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestController}
 */
$xyz.swapee.wc.back.AbstractIrqTestController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractIrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController
/** @type {function(new: xyz.swapee.wc.back.AbstractIrqTestController)} */
xyz.swapee.wc.back.AbstractIrqTestController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IrqTestControllerConstructor filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestController, ...!xyz.swapee.wc.back.IIrqTestController.Initialese)} */
xyz.swapee.wc.back.IrqTestControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.RecordIIrqTestController filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.BoundIIrqTestController filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIIrqTestController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIIrqTestController} */
xyz.swapee.wc.back.BoundIIrqTestController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.BoundIrqTestController filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundIrqTestController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIrqTestController} */
xyz.swapee.wc.back.BoundIrqTestController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */