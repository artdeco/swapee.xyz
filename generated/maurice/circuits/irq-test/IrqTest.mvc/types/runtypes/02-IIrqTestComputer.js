/** @const {?} */ $xyz.swapee.wc.IIrqTestComputer
/** @const {?} */ xyz.swapee.wc.IIrqTestComputer
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputer.Initialese filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IIrqTestComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestComputer.Initialese} */
xyz.swapee.wc.IIrqTestComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputerCaster filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/** @interface */
$xyz.swapee.wc.IIrqTestComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestComputer} */
$xyz.swapee.wc.IIrqTestComputerCaster.prototype.asIIrqTestComputer
/** @type {!xyz.swapee.wc.BoundIrqTestComputer} */
$xyz.swapee.wc.IIrqTestComputerCaster.prototype.superIrqTestComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestComputerCaster}
 */
xyz.swapee.wc.IIrqTestComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputer filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.IrqTestMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.IIrqTestComputer = function() {}
/**
 * @param {xyz.swapee.wc.IrqTestMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestComputer}
 */
xyz.swapee.wc.IIrqTestComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IrqTestComputer filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestComputer.Initialese>}
 */
$xyz.swapee.wc.IrqTestComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.IrqTestComputer
/** @type {function(new: xyz.swapee.wc.IIrqTestComputer, ...!xyz.swapee.wc.IIrqTestComputer.Initialese)} */
xyz.swapee.wc.IrqTestComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.IrqTestComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.AbstractIrqTestComputer filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestComputer}
 */
$xyz.swapee.wc.AbstractIrqTestComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestComputer)} */
xyz.swapee.wc.AbstractIrqTestComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IrqTestComputerConstructor filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestComputer, ...!xyz.swapee.wc.IIrqTestComputer.Initialese)} */
xyz.swapee.wc.IrqTestComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.RecordIIrqTestComputer filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/** @typedef {{ compute: xyz.swapee.wc.IIrqTestComputer.compute }} */
xyz.swapee.wc.RecordIIrqTestComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.BoundIIrqTestComputer filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.IrqTestMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundIIrqTestComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestComputer} */
xyz.swapee.wc.BoundIIrqTestComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.BoundIrqTestComputer filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestComputer} */
xyz.swapee.wc.BoundIrqTestComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputer.compute filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.IrqTestMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.IrqTestMemory): void} */
xyz.swapee.wc.IIrqTestComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IIrqTestComputer, xyz.swapee.wc.IrqTestMemory): void} */
xyz.swapee.wc.IIrqTestComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IIrqTestComputer.__compute} */
xyz.swapee.wc.IIrqTestComputer.__compute

// nss:xyz.swapee.wc.IIrqTestComputer,$xyz.swapee.wc.IIrqTestComputer,xyz.swapee.wc
/* @typal-end */