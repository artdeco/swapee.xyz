/** @const {?} */ $xyz.swapee.wc.front.IIrqTestControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IIrqTestControllerAT.Initialese filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IIrqTestControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} */
xyz.swapee.wc.front.IIrqTestControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IIrqTestControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IIrqTestControllerATCaster filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/** @interface */
$xyz.swapee.wc.front.IIrqTestControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIIrqTestControllerAT} */
$xyz.swapee.wc.front.IIrqTestControllerATCaster.prototype.asIIrqTestControllerAT
/** @type {!xyz.swapee.wc.front.BoundIrqTestControllerAT} */
$xyz.swapee.wc.front.IIrqTestControllerATCaster.prototype.superIrqTestControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IIrqTestControllerATCaster}
 */
xyz.swapee.wc.front.IIrqTestControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IIrqTestControllerAT filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IIrqTestControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IIrqTestControllerAT}
 */
xyz.swapee.wc.front.IIrqTestControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IrqTestControllerAT filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IIrqTestControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.IrqTestControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.IrqTestControllerAT
/** @type {function(new: xyz.swapee.wc.front.IIrqTestControllerAT, ...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese)} */
xyz.swapee.wc.front.IrqTestControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.IrqTestControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.AbstractIrqTestControllerAT filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.IrqTestControllerAT}
 */
$xyz.swapee.wc.front.AbstractIrqTestControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractIrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractIrqTestControllerAT)} */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IrqTestControllerATConstructor filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestControllerAT, ...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese)} */
xyz.swapee.wc.front.IrqTestControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.RecordIIrqTestControllerAT filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIIrqTestControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.BoundIIrqTestControllerAT filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIIrqTestControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIIrqTestControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIIrqTestControllerAT} */
xyz.swapee.wc.front.BoundIIrqTestControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.BoundIrqTestControllerAT filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIIrqTestControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundIrqTestControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIrqTestControllerAT} */
xyz.swapee.wc.front.BoundIrqTestControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */