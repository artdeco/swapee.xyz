/** @const {?} */ $xyz.swapee.wc.IIrqTestController
/** @const {?} */ xyz.swapee.wc.IIrqTestController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.Initialese filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IIrqTestController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestController.Initialese} */
xyz.swapee.wc.IIrqTestController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestControllerFields filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @interface */
$xyz.swapee.wc.IIrqTestControllerFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTestController.Inputs} */
$xyz.swapee.wc.IIrqTestControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestControllerFields}
 */
xyz.swapee.wc.IIrqTestControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestControllerCaster filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @interface */
$xyz.swapee.wc.IIrqTestControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestController} */
$xyz.swapee.wc.IIrqTestControllerCaster.prototype.asIIrqTestController
/** @type {!xyz.swapee.wc.BoundIIrqTestProcessor} */
$xyz.swapee.wc.IIrqTestControllerCaster.prototype.asIIrqTestProcessor
/** @type {!xyz.swapee.wc.BoundIrqTestController} */
$xyz.swapee.wc.IIrqTestControllerCaster.prototype.superIrqTestController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestControllerCaster}
 */
xyz.swapee.wc.IIrqTestControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IrqTestMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
$xyz.swapee.wc.IIrqTestController = function() {}
/** @return {void} */
$xyz.swapee.wc.IIrqTestController.prototype.resetPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestController}
 */
xyz.swapee.wc.IIrqTestController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IrqTestController filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestController.Initialese>}
 */
$xyz.swapee.wc.IrqTestController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.IrqTestController
/** @type {function(new: xyz.swapee.wc.IIrqTestController, ...!xyz.swapee.wc.IIrqTestController.Initialese)} */
xyz.swapee.wc.IrqTestController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.IrqTestController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.AbstractIrqTestController filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestController}
 */
$xyz.swapee.wc.AbstractIrqTestController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestController)} */
xyz.swapee.wc.AbstractIrqTestController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IrqTestControllerConstructor filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestController, ...!xyz.swapee.wc.IIrqTestController.Initialese)} */
xyz.swapee.wc.IrqTestControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.RecordIIrqTestController filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @typedef {{ resetPort: xyz.swapee.wc.IIrqTestController.resetPort }} */
xyz.swapee.wc.RecordIIrqTestController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.BoundIIrqTestController filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestControllerFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IrqTestMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
$xyz.swapee.wc.BoundIIrqTestController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestController} */
xyz.swapee.wc.BoundIIrqTestController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.BoundIrqTestController filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestController} */
xyz.swapee.wc.BoundIrqTestController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.resetPort filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IIrqTestController): void} */
xyz.swapee.wc.IIrqTestController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IIrqTestController.__resetPort} */
xyz.swapee.wc.IIrqTestController.__resetPort

// nss:xyz.swapee.wc.IIrqTestController,$xyz.swapee.wc.IIrqTestController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.Inputs filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestPort.Inputs}
 */
$xyz.swapee.wc.IIrqTestController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestController.Inputs} */
xyz.swapee.wc.IIrqTestController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.WeakInputs filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestPort.WeakInputs}
 */
$xyz.swapee.wc.IIrqTestController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestController.WeakInputs} */
xyz.swapee.wc.IIrqTestController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestController
/* @typal-end */