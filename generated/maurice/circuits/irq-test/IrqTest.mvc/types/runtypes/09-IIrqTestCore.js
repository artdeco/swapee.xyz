/** @const {?} */ $xyz.swapee.wc.IIrqTestCore
/** @const {?} */ xyz.swapee.wc.IIrqTestCore
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.Initialese filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @record */
$xyz.swapee.wc.IIrqTestCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestCore.Initialese} */
xyz.swapee.wc.IIrqTestCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCoreFields filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @interface */
$xyz.swapee.wc.IIrqTestCoreFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTestCore.Model} */
$xyz.swapee.wc.IIrqTestCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestCoreFields}
 */
xyz.swapee.wc.IIrqTestCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCoreCaster filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @interface */
$xyz.swapee.wc.IIrqTestCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestCore} */
$xyz.swapee.wc.IIrqTestCoreCaster.prototype.asIIrqTestCore
/** @type {!xyz.swapee.wc.BoundIrqTestCore} */
$xyz.swapee.wc.IIrqTestCoreCaster.prototype.superIrqTestCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestCoreCaster}
 */
xyz.swapee.wc.IIrqTestCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCoreCaster}
 * @extends {xyz.swapee.wc.IIrqTestOuterCore}
 */
$xyz.swapee.wc.IIrqTestCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IIrqTestCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IIrqTestCore.prototype.resetIrqTestCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestCore}
 */
xyz.swapee.wc.IIrqTestCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IrqTestCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestCore.Initialese>}
 */
$xyz.swapee.wc.IrqTestCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.IrqTestCore
/** @type {function(new: xyz.swapee.wc.IIrqTestCore)} */
xyz.swapee.wc.IrqTestCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.IrqTestCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.AbstractIrqTestCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestCore}
 */
$xyz.swapee.wc.AbstractIrqTestCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractIrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestCore)} */
xyz.swapee.wc.AbstractIrqTestCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.RecordIIrqTestCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @typedef {{ resetCore: xyz.swapee.wc.IIrqTestCore.resetCore, resetIrqTestCore: xyz.swapee.wc.IIrqTestCore.resetIrqTestCore }} */
xyz.swapee.wc.RecordIIrqTestCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.BoundIIrqTestCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestCoreFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCoreCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestOuterCore}
 */
$xyz.swapee.wc.BoundIIrqTestCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestCore} */
xyz.swapee.wc.BoundIIrqTestCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.BoundIrqTestCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestCore} */
xyz.swapee.wc.BoundIrqTestCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.resetCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IIrqTestCore): void} */
xyz.swapee.wc.IIrqTestCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IIrqTestCore.__resetCore} */
xyz.swapee.wc.IIrqTestCore.__resetCore

// nss:xyz.swapee.wc.IIrqTestCore,$xyz.swapee.wc.IIrqTestCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.resetIrqTestCore filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestCore.resetIrqTestCore
/** @typedef {function(this: xyz.swapee.wc.IIrqTestCore): void} */
xyz.swapee.wc.IIrqTestCore._resetIrqTestCore
/** @typedef {typeof $xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore} */
xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore

// nss:xyz.swapee.wc.IIrqTestCore,$xyz.swapee.wc.IIrqTestCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.Model filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model}
 */
$xyz.swapee.wc.IIrqTestCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestCore.Model} */
xyz.swapee.wc.IIrqTestCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestCore
/* @typal-end */