/** @const {?} */ $xyz.swapee.wc.IIrqTestGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IIrqTestDisplay.Initialese}
 */
$xyz.swapee.wc.IIrqTestGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestGPU.Initialese} */
xyz.swapee.wc.IIrqTestGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IIrqTestGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IIrqTestGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestGPUFields}
 */
xyz.swapee.wc.IIrqTestGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IIrqTestGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestGPU} */
$xyz.swapee.wc.IIrqTestGPUCaster.prototype.asIIrqTestGPU
/** @type {!xyz.swapee.wc.BoundIrqTestGPU} */
$xyz.swapee.wc.IIrqTestGPUCaster.prototype.superIrqTestGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestGPUCaster}
 */
xyz.swapee.wc.IIrqTestGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!IrqTestMemory,>}
 * @extends {xyz.swapee.wc.back.IIrqTestDisplay}
 */
$xyz.swapee.wc.IIrqTestGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestGPU}
 */
xyz.swapee.wc.IIrqTestGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IrqTestGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestGPU.Initialese>}
 */
$xyz.swapee.wc.IrqTestGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.IrqTestGPU
/** @type {function(new: xyz.swapee.wc.IIrqTestGPU, ...!xyz.swapee.wc.IIrqTestGPU.Initialese)} */
xyz.swapee.wc.IrqTestGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.IrqTestGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.AbstractIrqTestGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestGPU}
 */
$xyz.swapee.wc.AbstractIrqTestGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestGPU)} */
xyz.swapee.wc.AbstractIrqTestGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IrqTestGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestGPU, ...!xyz.swapee.wc.IIrqTestGPU.Initialese)} */
xyz.swapee.wc.IrqTestGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.RecordIIrqTestGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.BoundIIrqTestGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestGPUFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!IrqTestMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestDisplay}
 */
$xyz.swapee.wc.BoundIIrqTestGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestGPU} */
xyz.swapee.wc.BoundIIrqTestGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.BoundIrqTestGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestGPU} */
xyz.swapee.wc.BoundIrqTestGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */