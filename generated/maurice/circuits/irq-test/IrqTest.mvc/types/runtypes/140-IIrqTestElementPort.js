/** @const {?} */ $xyz.swapee.wc.IIrqTestElementPort
/** @const {?} */ $xyz.swapee.wc.IIrqTestElementPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IIrqTestElementPort.WeakInputs
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.Initialese filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IIrqTestElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestElementPort.Initialese} */
xyz.swapee.wc.IIrqTestElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPortFields filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @interface */
$xyz.swapee.wc.IIrqTestElementPortFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTestElementPort.Inputs} */
$xyz.swapee.wc.IIrqTestElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IIrqTestElementPort.Inputs} */
$xyz.swapee.wc.IIrqTestElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestElementPortFields}
 */
xyz.swapee.wc.IIrqTestElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPortCaster filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @interface */
$xyz.swapee.wc.IIrqTestElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestElementPort} */
$xyz.swapee.wc.IIrqTestElementPortCaster.prototype.asIIrqTestElementPort
/** @type {!xyz.swapee.wc.BoundIrqTestElementPort} */
$xyz.swapee.wc.IIrqTestElementPortCaster.prototype.superIrqTestElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestElementPortCaster}
 */
xyz.swapee.wc.IIrqTestElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IIrqTestElementPort.Inputs>}
 */
$xyz.swapee.wc.IIrqTestElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestElementPort}
 */
xyz.swapee.wc.IIrqTestElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IrqTestElementPort filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElementPort.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestElementPort.Initialese>}
 */
$xyz.swapee.wc.IrqTestElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.IrqTestElementPort
/** @type {function(new: xyz.swapee.wc.IIrqTestElementPort, ...!xyz.swapee.wc.IIrqTestElementPort.Initialese)} */
xyz.swapee.wc.IrqTestElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.IrqTestElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.AbstractIrqTestElementPort filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElementPort.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestElementPort}
 */
$xyz.swapee.wc.AbstractIrqTestElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestElementPort)} */
xyz.swapee.wc.AbstractIrqTestElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestElementPort|typeof xyz.swapee.wc.IrqTestElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestElementPort|typeof xyz.swapee.wc.IrqTestElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestElementPort|typeof xyz.swapee.wc.IrqTestElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestElementPort}
 */
xyz.swapee.wc.AbstractIrqTestElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IrqTestElementPortConstructor filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestElementPort, ...!xyz.swapee.wc.IIrqTestElementPort.Initialese)} */
xyz.swapee.wc.IrqTestElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.RecordIIrqTestElementPort filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.BoundIIrqTestElementPort filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestElementPortFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IIrqTestElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundIIrqTestElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestElementPort} */
xyz.swapee.wc.BoundIIrqTestElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.BoundIrqTestElementPort filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestElementPort} */
xyz.swapee.wc.BoundIrqTestElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @typedef {boolean} */
xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @record */
$xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder} */
xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.Inputs filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder}
 */
$xyz.swapee.wc.IIrqTestElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IIrqTestElementPort.Inputs}
 */
xyz.swapee.wc.IIrqTestElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @record */
$xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.WeakInputs filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder}
 */
$xyz.swapee.wc.IIrqTestElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IIrqTestElementPort.WeakInputs}
 */
xyz.swapee.wc.IIrqTestElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @record */
$xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.IIrqTestElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/140-IIrqTestElementPort.xml} xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props 729613afdafe39d73de428a2884348a7 */
/** @record */
$xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.IIrqTestElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElementPort.WeakInputs
/* @typal-end */