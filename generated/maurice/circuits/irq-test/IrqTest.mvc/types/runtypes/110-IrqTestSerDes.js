/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.IrqTestMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.IrqTestMemoryPQs.prototype.core
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IrqTestMemoryPQs}
 */
xyz.swapee.wc.IrqTestMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.IrqTestMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.IrqTestMemoryQPs.prototype.a74ad
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestMemoryQPs}
 */
xyz.swapee.wc.IrqTestMemoryQPs
/** @type {function(new: xyz.swapee.wc.IrqTestMemoryQPs)} */
xyz.swapee.wc.IrqTestMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IrqTestMemoryPQs}
 */
$xyz.swapee.wc.IrqTestInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IrqTestInputsPQs}
 */
xyz.swapee.wc.IrqTestInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.IrqTestInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestInputsQPs}
 */
xyz.swapee.wc.IrqTestInputsQPs
/** @type {function(new: xyz.swapee.wc.IrqTestInputsQPs)} */
xyz.swapee.wc.IrqTestInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.IrqTestVdusPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IrqTestVdusPQs}
 */
xyz.swapee.wc.IrqTestVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.IrqTestVdusQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestVdusQPs}
 */
xyz.swapee.wc.IrqTestVdusQPs
/** @type {function(new: xyz.swapee.wc.IrqTestVdusQPs)} */
xyz.swapee.wc.IrqTestVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */