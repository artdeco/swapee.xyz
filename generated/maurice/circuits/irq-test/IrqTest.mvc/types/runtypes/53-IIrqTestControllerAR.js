/** @const {?} */ $xyz.swapee.wc.back.IIrqTestControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IIrqTestControllerAR.Initialese filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
$xyz.swapee.wc.back.IIrqTestControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} */
xyz.swapee.wc.back.IIrqTestControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IIrqTestControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IIrqTestControllerARCaster filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/** @interface */
$xyz.swapee.wc.back.IIrqTestControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIIrqTestControllerAR} */
$xyz.swapee.wc.back.IIrqTestControllerARCaster.prototype.asIIrqTestControllerAR
/** @type {!xyz.swapee.wc.back.BoundIrqTestControllerAR} */
$xyz.swapee.wc.back.IIrqTestControllerARCaster.prototype.superIrqTestControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestControllerARCaster}
 */
xyz.swapee.wc.back.IIrqTestControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IIrqTestControllerAR filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IIrqTestController}
 */
$xyz.swapee.wc.back.IIrqTestControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestControllerAR}
 */
xyz.swapee.wc.back.IIrqTestControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IrqTestControllerAR filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.IrqTestControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.IrqTestControllerAR
/** @type {function(new: xyz.swapee.wc.back.IIrqTestControllerAR, ...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese)} */
xyz.swapee.wc.back.IrqTestControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.IrqTestControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.AbstractIrqTestControllerAR filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestControllerAR}
 */
$xyz.swapee.wc.back.AbstractIrqTestControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractIrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractIrqTestControllerAR)} */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IrqTestControllerARConstructor filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestControllerAR, ...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese)} */
xyz.swapee.wc.back.IrqTestControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.RecordIIrqTestControllerAR filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.BoundIIrqTestControllerAR filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 */
$xyz.swapee.wc.back.BoundIIrqTestControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIIrqTestControllerAR} */
xyz.swapee.wc.back.BoundIIrqTestControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.BoundIrqTestControllerAR filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundIrqTestControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIrqTestControllerAR} */
xyz.swapee.wc.back.BoundIrqTestControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */