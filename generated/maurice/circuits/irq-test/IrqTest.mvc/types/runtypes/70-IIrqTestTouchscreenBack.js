/** @const {?} */ $xyz.swapee.wc.back.IIrqTestTouchscreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese}
 */
$xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} */
xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IIrqTestTouchscreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IIrqTestTouchscreenCaster filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/** @interface */
$xyz.swapee.wc.back.IIrqTestTouchscreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIIrqTestTouchscreen} */
$xyz.swapee.wc.back.IIrqTestTouchscreenCaster.prototype.asIIrqTestTouchscreen
/** @type {!xyz.swapee.wc.back.BoundIrqTestTouchscreen} */
$xyz.swapee.wc.back.IIrqTestTouchscreenCaster.prototype.superIrqTestTouchscreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestTouchscreenCaster}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IIrqTestTouchscreen filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenAT}
 */
$xyz.swapee.wc.back.IIrqTestTouchscreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestTouchscreen}
 */
xyz.swapee.wc.back.IIrqTestTouchscreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IrqTestTouchscreen filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese>}
 */
$xyz.swapee.wc.back.IrqTestTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.IrqTestTouchscreen
/** @type {function(new: xyz.swapee.wc.back.IIrqTestTouchscreen, ...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese)} */
xyz.swapee.wc.back.IrqTestTouchscreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.IrqTestTouchscreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.AbstractIrqTestTouchscreen filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestTouchscreen}
 */
$xyz.swapee.wc.back.AbstractIrqTestTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen
/** @type {function(new: xyz.swapee.wc.back.AbstractIrqTestTouchscreen)} */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IrqTestTouchscreenConstructor filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestTouchscreen, ...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese)} */
xyz.swapee.wc.back.IrqTestTouchscreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.RecordIIrqTestTouchscreen filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestTouchscreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.BoundIIrqTestTouchscreen filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT}
 */
$xyz.swapee.wc.back.BoundIIrqTestTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIIrqTestTouchscreen} */
xyz.swapee.wc.back.BoundIIrqTestTouchscreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.BoundIrqTestTouchscreen filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundIrqTestTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIrqTestTouchscreen} */
xyz.swapee.wc.back.BoundIrqTestTouchscreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */