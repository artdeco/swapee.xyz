/** @const {?} */ $xyz.swapee.wc.IIrqTestDisplay
/** @const {?} */ xyz.swapee.wc.IIrqTestDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.Initialese filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings>}
 */
$xyz.swapee.wc.IIrqTestDisplay.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestDisplay.Initialese} */
xyz.swapee.wc.IIrqTestDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplayFields filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @interface */
$xyz.swapee.wc.IIrqTestDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTestDisplay.Settings} */
$xyz.swapee.wc.IIrqTestDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IIrqTestDisplay.Queries} */
$xyz.swapee.wc.IIrqTestDisplayFields.prototype.queries
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestDisplayFields}
 */
xyz.swapee.wc.IIrqTestDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplayCaster filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @interface */
$xyz.swapee.wc.IIrqTestDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestDisplay} */
$xyz.swapee.wc.IIrqTestDisplayCaster.prototype.asIIrqTestDisplay
/** @type {!xyz.swapee.wc.BoundIIrqTestTouchscreen} */
$xyz.swapee.wc.IIrqTestDisplayCaster.prototype.asIIrqTestTouchscreen
/** @type {!xyz.swapee.wc.BoundIrqTestDisplay} */
$xyz.swapee.wc.IIrqTestDisplayCaster.prototype.superIrqTestDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestDisplayCaster}
 */
xyz.swapee.wc.IIrqTestDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.IrqTestMemory, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, xyz.swapee.wc.IIrqTestDisplay.Queries, null>}
 */
$xyz.swapee.wc.IIrqTestDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestDisplay}
 */
xyz.swapee.wc.IIrqTestDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IrqTestDisplay filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestDisplay.Initialese>}
 */
$xyz.swapee.wc.IrqTestDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.IrqTestDisplay
/** @type {function(new: xyz.swapee.wc.IIrqTestDisplay, ...!xyz.swapee.wc.IIrqTestDisplay.Initialese)} */
xyz.swapee.wc.IrqTestDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.IrqTestDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.AbstractIrqTestDisplay filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestDisplay}
 */
$xyz.swapee.wc.AbstractIrqTestDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestDisplay)} */
xyz.swapee.wc.AbstractIrqTestDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IrqTestDisplayConstructor filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestDisplay, ...!xyz.swapee.wc.IIrqTestDisplay.Initialese)} */
xyz.swapee.wc.IrqTestDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.RecordIIrqTestDisplay filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @typedef {{ paint: xyz.swapee.wc.IIrqTestDisplay.paint }} */
xyz.swapee.wc.RecordIIrqTestDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.BoundIIrqTestDisplay filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestDisplayFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.IrqTestMemory, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, xyz.swapee.wc.IIrqTestDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIIrqTestDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestDisplay} */
xyz.swapee.wc.BoundIIrqTestDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.BoundIrqTestDisplay filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestDisplay} */
xyz.swapee.wc.BoundIrqTestDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.paint filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.IrqTestMemory, null): void} */
xyz.swapee.wc.IIrqTestDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IIrqTestDisplay, !xyz.swapee.wc.IrqTestMemory, null): void} */
xyz.swapee.wc.IIrqTestDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IIrqTestDisplay.__paint} */
xyz.swapee.wc.IIrqTestDisplay.__paint

// nss:xyz.swapee.wc.IIrqTestDisplay,$xyz.swapee.wc.IIrqTestDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.Queries filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @record */
$xyz.swapee.wc.IIrqTestDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestDisplay.Queries} */
xyz.swapee.wc.IIrqTestDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.Settings filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestDisplay.Queries}
 */
$xyz.swapee.wc.IIrqTestDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestDisplay.Settings} */
xyz.swapee.wc.IIrqTestDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestDisplay
/* @typal-end */