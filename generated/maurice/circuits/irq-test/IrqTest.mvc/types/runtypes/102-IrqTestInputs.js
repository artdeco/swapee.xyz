/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/102-IrqTestInputs.xml} xyz.swapee.wc.front.IrqTestInputs filter:!ControllerPlugin~props 7e41453f1ba6b911fd383b7d52532b38 */
/** @record */
$xyz.swapee.wc.front.IrqTestInputs = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.front.IrqTestInputs.prototype.core
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.IrqTestInputs}
 */
xyz.swapee.wc.front.IrqTestInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */