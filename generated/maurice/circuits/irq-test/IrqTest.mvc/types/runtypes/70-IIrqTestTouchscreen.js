/** @const {?} */ $xyz.swapee.wc.IIrqTestTouchscreen
/** @const {?} */ xyz.swapee.wc.IIrqTestTouchscreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreen.Initialese filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null>}
 * @extends {IIrqTestInterruptLine.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestDisplay.Initialese}
 */
$xyz.swapee.wc.IIrqTestTouchscreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestTouchscreen.Initialese} */
xyz.swapee.wc.IIrqTestTouchscreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestTouchscreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreenFields filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @interface */
$xyz.swapee.wc.IIrqTestTouchscreenFields = function() {}
/** @type {HTMLElement} */
$xyz.swapee.wc.IIrqTestTouchscreenFields.prototype.keyboardItem
/** @type {boolean} */
$xyz.swapee.wc.IIrqTestTouchscreenFields.prototype.menuExpanded
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestTouchscreenFields}
 */
xyz.swapee.wc.IIrqTestTouchscreenFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreenCaster filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @interface */
$xyz.swapee.wc.IIrqTestTouchscreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestTouchscreen} */
$xyz.swapee.wc.IIrqTestTouchscreenCaster.prototype.asIIrqTestTouchscreen
/** @type {!xyz.swapee.wc.BoundIrqTestTouchscreen} */
$xyz.swapee.wc.IIrqTestTouchscreenCaster.prototype.superIrqTestTouchscreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestTouchscreenCaster}
 */
xyz.swapee.wc.IIrqTestTouchscreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreen filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IIrqTestController}
 * @extends {IIrqTestInterruptLine}
 * @extends {xyz.swapee.wc.IIrqTestDisplay}
 */
$xyz.swapee.wc.IIrqTestTouchscreen = function() {}
/**
 * @param {*} memory
 * @return {?}
 */
$xyz.swapee.wc.IIrqTestTouchscreen.prototype.stashKeyboardItemAfterMenuExpanded = function(memory) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestTouchscreen}
 */
xyz.swapee.wc.IIrqTestTouchscreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IrqTestTouchscreen filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestTouchscreen.Initialese>}
 */
$xyz.swapee.wc.IrqTestTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.IrqTestTouchscreen
/** @type {function(new: xyz.swapee.wc.IIrqTestTouchscreen, ...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese)} */
xyz.swapee.wc.IrqTestTouchscreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.IrqTestTouchscreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.AbstractIrqTestTouchscreen filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestTouchscreen}
 */
$xyz.swapee.wc.AbstractIrqTestTouchscreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestTouchscreen)} */
xyz.swapee.wc.AbstractIrqTestTouchscreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IrqTestTouchscreenConstructor filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestTouchscreen, ...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese)} */
xyz.swapee.wc.IrqTestTouchscreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.RecordIIrqTestTouchscreen filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @typedef {{ stashKeyboardItemAfterMenuExpanded: xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded }} */
xyz.swapee.wc.RecordIIrqTestTouchscreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.BoundIIrqTestTouchscreen filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIIrqTestController}
 * @extends {BoundIIrqTestInterruptLine}
 * @extends {xyz.swapee.wc.BoundIIrqTestDisplay}
 */
$xyz.swapee.wc.BoundIIrqTestTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestTouchscreen} */
xyz.swapee.wc.BoundIIrqTestTouchscreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.BoundIrqTestTouchscreen filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestTouchscreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestTouchscreen} */
xyz.swapee.wc.BoundIrqTestTouchscreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @this {THIS}
 * @template THIS
 * @param {*} memory
 */
$xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded = function(memory) {}
/** @typedef {function(*)} */
xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.IIrqTestTouchscreen, *)} */
xyz.swapee.wc.IIrqTestTouchscreen._stashKeyboardItemAfterMenuExpanded
/** @typedef {typeof $xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded} */
xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded

// nss:xyz.swapee.wc.IIrqTestTouchscreen,$xyz.swapee.wc.IIrqTestTouchscreen,xyz.swapee.wc
/* @typal-end */