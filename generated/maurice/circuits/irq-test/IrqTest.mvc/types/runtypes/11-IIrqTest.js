/** @const {?} */ $xyz.swapee.wc.IIrqTest
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTestEnv filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @record */
$xyz.swapee.wc.IrqTestEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IIrqTest} */
$xyz.swapee.wc.IrqTestEnv.prototype.irqTest
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IrqTestEnv}
 */
xyz.swapee.wc.IrqTestEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest.Initialese filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {xyz.swapee.wc.IIrqTestProcessor.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestComputer.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
$xyz.swapee.wc.IIrqTest.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTest.Initialese} */
xyz.swapee.wc.IIrqTest.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTest
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTestFields filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @interface */
$xyz.swapee.wc.IIrqTestFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTest.Pinout} */
$xyz.swapee.wc.IIrqTestFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestFields}
 */
xyz.swapee.wc.IIrqTestFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTestCaster filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @interface */
$xyz.swapee.wc.IIrqTestCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTest} */
$xyz.swapee.wc.IIrqTestCaster.prototype.asIIrqTest
/** @type {!xyz.swapee.wc.BoundIrqTest} */
$xyz.swapee.wc.IIrqTestCaster.prototype.superIrqTest
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestCaster}
 */
xyz.swapee.wc.IIrqTestCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCaster}
 * @extends {xyz.swapee.wc.IIrqTestProcessor}
 * @extends {xyz.swapee.wc.IIrqTestComputer}
 * @extends {xyz.swapee.wc.IIrqTestController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, null>}
 */
$xyz.swapee.wc.IIrqTest = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTest}
 */
xyz.swapee.wc.IIrqTest

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTest filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTest}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTest.Initialese>}
 */
$xyz.swapee.wc.IrqTest = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.IrqTest
/** @type {function(new: xyz.swapee.wc.IIrqTest, ...!xyz.swapee.wc.IIrqTest.Initialese)} */
xyz.swapee.wc.IrqTest.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.IrqTest.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.AbstractIrqTest filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init
 * @extends {xyz.swapee.wc.IrqTest}
 */
$xyz.swapee.wc.AbstractIrqTest = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTest}
 */
xyz.swapee.wc.AbstractIrqTest
/** @type {function(new: xyz.swapee.wc.AbstractIrqTest)} */
xyz.swapee.wc.AbstractIrqTest.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTest}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTest.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTestConstructor filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @typedef {function(new: xyz.swapee.wc.IIrqTest, ...!xyz.swapee.wc.IIrqTest.Initialese)} */
xyz.swapee.wc.IrqTestConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest.MVCOptions filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @record */
$xyz.swapee.wc.IIrqTest.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IIrqTest.Pinout)|undefined} */
$xyz.swapee.wc.IIrqTest.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IIrqTest.Pinout)|undefined} */
$xyz.swapee.wc.IIrqTest.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IIrqTest.Pinout} */
$xyz.swapee.wc.IIrqTest.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.IrqTestMemory)|undefined} */
$xyz.swapee.wc.IIrqTest.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.IrqTestClasses)|undefined} */
$xyz.swapee.wc.IIrqTest.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IIrqTest.MVCOptions} */
xyz.swapee.wc.IIrqTest.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTest
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.RecordIIrqTest filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTest

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.BoundIIrqTest filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestFields}
 * @extends {xyz.swapee.wc.RecordIIrqTest}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestProcessor}
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, null>}
 */
$xyz.swapee.wc.BoundIIrqTest = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTest} */
xyz.swapee.wc.BoundIIrqTest

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.BoundIrqTest filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTest}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTest = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTest} */
xyz.swapee.wc.BoundIrqTest

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest.Pinout filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestController.Inputs}
 */
$xyz.swapee.wc.IIrqTest.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTest.Pinout} */
xyz.swapee.wc.IIrqTest.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTest
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTestBuffer filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
$xyz.swapee.wc.IIrqTestBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestBuffer}
 */
xyz.swapee.wc.IIrqTestBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTestBuffer filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestBuffer}
 */
$xyz.swapee.wc.IrqTestBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestBuffer}
 */
xyz.swapee.wc.IrqTestBuffer
/** @type {function(new: xyz.swapee.wc.IIrqTestBuffer)} */
xyz.swapee.wc.IrqTestBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */