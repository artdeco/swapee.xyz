/** @const {?} */ $xyz.swapee.wc.IIrqTestPort
/** @const {?} */ xyz.swapee.wc.IIrqTestPort
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.Initialese filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IIrqTestPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestPort.Initialese} */
xyz.swapee.wc.IIrqTestPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPortFields filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @interface */
$xyz.swapee.wc.IIrqTestPortFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTestPort.Inputs} */
$xyz.swapee.wc.IIrqTestPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IIrqTestPort.Inputs} */
$xyz.swapee.wc.IIrqTestPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestPortFields}
 */
xyz.swapee.wc.IIrqTestPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPortCaster filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @interface */
$xyz.swapee.wc.IIrqTestPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestPort} */
$xyz.swapee.wc.IIrqTestPortCaster.prototype.asIIrqTestPort
/** @type {!xyz.swapee.wc.BoundIrqTestPort} */
$xyz.swapee.wc.IIrqTestPortCaster.prototype.superIrqTestPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestPortCaster}
 */
xyz.swapee.wc.IIrqTestPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IIrqTestPort.Inputs>}
 */
$xyz.swapee.wc.IIrqTestPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IIrqTestPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IIrqTestPort.prototype.resetIrqTestPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestPort}
 */
xyz.swapee.wc.IIrqTestPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IrqTestPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestPort.Initialese>}
 */
$xyz.swapee.wc.IrqTestPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.IrqTestPort
/** @type {function(new: xyz.swapee.wc.IIrqTestPort, ...!xyz.swapee.wc.IIrqTestPort.Initialese)} */
xyz.swapee.wc.IrqTestPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.IrqTestPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.AbstractIrqTestPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestPort}
 */
$xyz.swapee.wc.AbstractIrqTestPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestPort)} */
xyz.swapee.wc.AbstractIrqTestPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IrqTestPortConstructor filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestPort, ...!xyz.swapee.wc.IIrqTestPort.Initialese)} */
xyz.swapee.wc.IrqTestPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.RecordIIrqTestPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @typedef {{ resetPort: xyz.swapee.wc.IIrqTestPort.resetPort, resetIrqTestPort: xyz.swapee.wc.IIrqTestPort.resetIrqTestPort }} */
xyz.swapee.wc.RecordIIrqTestPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.BoundIIrqTestPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestPortFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IIrqTestPort.Inputs>}
 */
$xyz.swapee.wc.BoundIIrqTestPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestPort} */
xyz.swapee.wc.BoundIIrqTestPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.BoundIrqTestPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestPort} */
xyz.swapee.wc.BoundIrqTestPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.resetPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IIrqTestPort): void} */
xyz.swapee.wc.IIrqTestPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IIrqTestPort.__resetPort} */
xyz.swapee.wc.IIrqTestPort.__resetPort

// nss:xyz.swapee.wc.IIrqTestPort,$xyz.swapee.wc.IIrqTestPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.resetIrqTestPort filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestPort.resetIrqTestPort
/** @typedef {function(this: xyz.swapee.wc.IIrqTestPort): void} */
xyz.swapee.wc.IIrqTestPort._resetIrqTestPort
/** @typedef {typeof $xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort} */
xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort

// nss:xyz.swapee.wc.IIrqTestPort,$xyz.swapee.wc.IIrqTestPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.Inputs filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel}
 */
$xyz.swapee.wc.IIrqTestPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IIrqTestPort.Inputs}
 */
xyz.swapee.wc.IIrqTestPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.WeakInputs filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel}
 */
$xyz.swapee.wc.IIrqTestPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IIrqTestPort.WeakInputs}
 */
xyz.swapee.wc.IIrqTestPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestPort
/* @typal-end */