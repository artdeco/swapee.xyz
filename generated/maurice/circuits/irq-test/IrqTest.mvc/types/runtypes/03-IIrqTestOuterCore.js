/** @const {?} */ $xyz.swapee.wc.IIrqTestOuterCore
/** @const {?} */ $xyz.swapee.wc.IIrqTestOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IIrqTestOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IIrqTestPort
/** @const {?} */ $xyz.swapee.wc.IIrqTestPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IIrqTestPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IIrqTestCore
/** @const {?} */ $xyz.swapee.wc.IIrqTestCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Initialese filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
$xyz.swapee.wc.IIrqTestOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestOuterCore.Initialese} */
xyz.swapee.wc.IIrqTestOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCoreFields filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @interface */
$xyz.swapee.wc.IIrqTestOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTestOuterCore.Model} */
$xyz.swapee.wc.IIrqTestOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestOuterCoreFields}
 */
xyz.swapee.wc.IIrqTestOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCoreCaster filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @interface */
$xyz.swapee.wc.IIrqTestOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestOuterCore} */
$xyz.swapee.wc.IIrqTestOuterCoreCaster.prototype.asIIrqTestOuterCore
/** @type {!xyz.swapee.wc.BoundIrqTestOuterCore} */
$xyz.swapee.wc.IIrqTestOuterCoreCaster.prototype.superIrqTestOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestOuterCoreCaster}
 */
xyz.swapee.wc.IIrqTestOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreCaster}
 */
$xyz.swapee.wc.IIrqTestOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestOuterCore}
 */
xyz.swapee.wc.IIrqTestOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IrqTestOuterCore filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestOuterCore.Initialese>}
 */
$xyz.swapee.wc.IrqTestOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.IrqTestOuterCore
/** @type {function(new: xyz.swapee.wc.IIrqTestOuterCore)} */
xyz.swapee.wc.IrqTestOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.IrqTestOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.AbstractIrqTestOuterCore filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestOuterCore}
 */
$xyz.swapee.wc.AbstractIrqTestOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractIrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestOuterCore)} */
xyz.swapee.wc.AbstractIrqTestOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.RecordIIrqTestOuterCore filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.BoundIIrqTestOuterCore filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIIrqTestOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestOuterCore} */
xyz.swapee.wc.BoundIIrqTestOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.BoundIrqTestOuterCore filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestOuterCore} */
xyz.swapee.wc.BoundIrqTestOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model.Core.core filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @typedef {string} */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model.Core filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
$xyz.swapee.wc.IIrqTestOuterCore.Model.Core = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IIrqTestOuterCore.Model.Core.prototype.core
/** @typedef {$xyz.swapee.wc.IIrqTestOuterCore.Model.Core} */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model.Core}
 */
$xyz.swapee.wc.IIrqTestOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestOuterCore.Model} */
xyz.swapee.wc.IIrqTestOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
$xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core.prototype.core
/** @typedef {$xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core} */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.WeakModel filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IIrqTestOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestOuterCore.WeakModel} */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
$xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe} */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
$xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe} */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.Inputs.Core filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IIrqTestPort.Inputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestPort.Inputs.Core} */
xyz.swapee.wc.IIrqTestPort.Inputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.Inputs.Core_Safe filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.IIrqTestPort.Inputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestPort.Inputs.Core_Safe} */
xyz.swapee.wc.IIrqTestPort.Inputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.WeakInputs.Core filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IIrqTestPort.WeakInputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestPort.WeakInputs.Core} */
xyz.swapee.wc.IIrqTestPort.WeakInputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.WeakInputs.Core_Safe filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.IIrqTestPort.WeakInputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestPort.WeakInputs.Core_Safe} */
xyz.swapee.wc.IIrqTestPort.WeakInputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestCore.Model.Core filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model.Core}
 */
$xyz.swapee.wc.IIrqTestCore.Model.Core = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestCore.Model.Core} */
xyz.swapee.wc.IIrqTestCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestCore.Model.Core_Safe filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe}
 */
$xyz.swapee.wc.IIrqTestCore.Model.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestCore.Model.Core_Safe} */
xyz.swapee.wc.IIrqTestCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestCore.Model
/* @typal-end */