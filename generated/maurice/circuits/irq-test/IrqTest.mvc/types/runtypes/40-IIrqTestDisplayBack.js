/** @const {?} */ $xyz.swapee.wc.back.IIrqTestDisplay
/** @const {?} */ xyz.swapee.wc.back.IIrqTestDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplay.Initialese filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.IrqTestClasses>}
 */
$xyz.swapee.wc.back.IIrqTestDisplay.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IIrqTestDisplay.Initialese} */
xyz.swapee.wc.back.IIrqTestDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IIrqTestDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplayCaster filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/** @interface */
$xyz.swapee.wc.back.IIrqTestDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIIrqTestDisplay} */
$xyz.swapee.wc.back.IIrqTestDisplayCaster.prototype.asIIrqTestDisplay
/** @type {!xyz.swapee.wc.back.BoundIrqTestDisplay} */
$xyz.swapee.wc.back.IIrqTestDisplayCaster.prototype.superIrqTestDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestDisplayCaster}
 */
xyz.swapee.wc.back.IIrqTestDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplay filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IrqTestClasses, null>}
 */
$xyz.swapee.wc.back.IIrqTestDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IIrqTestDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestDisplay}
 */
xyz.swapee.wc.back.IIrqTestDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IrqTestDisplay filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IIrqTestDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestDisplay.Initialese>}
 */
$xyz.swapee.wc.back.IrqTestDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.IrqTestDisplay
/** @type {function(new: xyz.swapee.wc.back.IIrqTestDisplay)} */
xyz.swapee.wc.back.IrqTestDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.IrqTestDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.AbstractIrqTestDisplay filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.IrqTestDisplay}
 */
$xyz.swapee.wc.back.AbstractIrqTestDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractIrqTestDisplay)} */
xyz.swapee.wc.back.AbstractIrqTestDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.RecordIIrqTestDisplay filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/** @typedef {{ paint: xyz.swapee.wc.back.IIrqTestDisplay.paint }} */
xyz.swapee.wc.back.RecordIIrqTestDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.BoundIIrqTestDisplay filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IrqTestClasses, null>}
 */
$xyz.swapee.wc.back.BoundIIrqTestDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIIrqTestDisplay} */
xyz.swapee.wc.back.BoundIIrqTestDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.BoundIrqTestDisplay filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundIrqTestDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIrqTestDisplay} */
xyz.swapee.wc.back.BoundIrqTestDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplay.paint filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IIrqTestDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.IrqTestMemory=, null=): void} */
xyz.swapee.wc.back.IIrqTestDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IIrqTestDisplay, !xyz.swapee.wc.IrqTestMemory=, null=): void} */
xyz.swapee.wc.back.IIrqTestDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IIrqTestDisplay.__paint} */
xyz.swapee.wc.back.IIrqTestDisplay.__paint

// nss:xyz.swapee.wc.back.IIrqTestDisplay,$xyz.swapee.wc.back.IIrqTestDisplay,xyz.swapee.wc.back
/* @typal-end */