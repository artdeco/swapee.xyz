/** @const {?} */ $xyz.swapee.wc.front.IIrqTestController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IIrqTestController.Initialese filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @record */
$xyz.swapee.wc.front.IIrqTestController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IIrqTestController.Initialese} */
xyz.swapee.wc.front.IIrqTestController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IIrqTestController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IIrqTestControllerCaster filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @interface */
$xyz.swapee.wc.front.IIrqTestControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIIrqTestController} */
$xyz.swapee.wc.front.IIrqTestControllerCaster.prototype.asIIrqTestController
/** @type {!xyz.swapee.wc.front.BoundIrqTestController} */
$xyz.swapee.wc.front.IIrqTestControllerCaster.prototype.superIrqTestController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IIrqTestControllerCaster}
 */
xyz.swapee.wc.front.IIrqTestControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IIrqTestController filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerAT}
 */
$xyz.swapee.wc.front.IIrqTestController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IIrqTestController}
 */
xyz.swapee.wc.front.IIrqTestController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IrqTestController filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init
 * @implements {xyz.swapee.wc.front.IIrqTestController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestController.Initialese>}
 */
$xyz.swapee.wc.front.IrqTestController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.IrqTestController
/** @type {function(new: xyz.swapee.wc.front.IIrqTestController, ...!xyz.swapee.wc.front.IIrqTestController.Initialese)} */
xyz.swapee.wc.front.IrqTestController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.IrqTestController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.AbstractIrqTestController filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init
 * @extends {xyz.swapee.wc.front.IrqTestController}
 */
$xyz.swapee.wc.front.AbstractIrqTestController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractIrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController
/** @type {function(new: xyz.swapee.wc.front.AbstractIrqTestController)} */
xyz.swapee.wc.front.AbstractIrqTestController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IrqTestControllerConstructor filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestController, ...!xyz.swapee.wc.front.IIrqTestController.Initialese)} */
xyz.swapee.wc.front.IrqTestControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.RecordIIrqTestController filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIIrqTestController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.BoundIIrqTestController filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIIrqTestController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIIrqTestControllerAT}
 */
$xyz.swapee.wc.front.BoundIIrqTestController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIIrqTestController} */
xyz.swapee.wc.front.BoundIIrqTestController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.BoundIrqTestController filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIIrqTestController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundIrqTestController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIrqTestController} */
xyz.swapee.wc.front.BoundIrqTestController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */