/** @const {?} */ $xyz.swapee.wc.IIrqTestHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IIrqTestHtmlComponent.Initialese filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IIrqTestController.Initialese}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese}
 * @extends {xyz.swapee.wc.IIrqTest.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestProcessor.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestComputer.Initialese}
 * @extends {IIrqTestGenerator.Initialese}
 */
$xyz.swapee.wc.IIrqTestHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} */
xyz.swapee.wc.IIrqTestHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IIrqTestHtmlComponentCaster filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/** @interface */
$xyz.swapee.wc.IIrqTestHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestHtmlComponent} */
$xyz.swapee.wc.IIrqTestHtmlComponentCaster.prototype.asIIrqTestHtmlComponent
/** @type {!xyz.swapee.wc.BoundIrqTestHtmlComponent} */
$xyz.swapee.wc.IIrqTestHtmlComponentCaster.prototype.superIrqTestHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestHtmlComponentCaster}
 */
xyz.swapee.wc.IIrqTestHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IIrqTestHtmlComponent filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IIrqTestController}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreen}
 * @extends {xyz.swapee.wc.IIrqTest}
 * @extends {xyz.swapee.wc.IIrqTestGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.IIrqTestProcessor}
 * @extends {xyz.swapee.wc.IIrqTestComputer}
 * @extends {IIrqTestGenerator}
 */
$xyz.swapee.wc.IIrqTestHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestHtmlComponent}
 */
xyz.swapee.wc.IIrqTestHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IrqTestHtmlComponent filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.IrqTestHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.IrqTestHtmlComponent
/** @type {function(new: xyz.swapee.wc.IIrqTestHtmlComponent, ...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese)} */
xyz.swapee.wc.IrqTestHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.IrqTestHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.AbstractIrqTestHtmlComponent filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestHtmlComponent}
 */
$xyz.swapee.wc.AbstractIrqTestHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestHtmlComponent)} */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IrqTestHtmlComponentConstructor filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestHtmlComponent, ...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese)} */
xyz.swapee.wc.IrqTestHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.RecordIIrqTestHtmlComponent filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.BoundIIrqTestHtmlComponent filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestController}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreen}
 * @extends {xyz.swapee.wc.BoundIIrqTest}
 * @extends {xyz.swapee.wc.BoundIIrqTestGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundIIrqTestProcessor}
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {BoundIIrqTestGenerator}
 */
$xyz.swapee.wc.BoundIIrqTestHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestHtmlComponent} */
xyz.swapee.wc.BoundIIrqTestHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.BoundIrqTestHtmlComponent filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestHtmlComponent} */
xyz.swapee.wc.BoundIrqTestHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */