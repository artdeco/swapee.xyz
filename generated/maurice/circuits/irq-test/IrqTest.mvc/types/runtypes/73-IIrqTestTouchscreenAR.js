/** @const {?} */ $xyz.swapee.wc.front.IIrqTestTouchscreenAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreen.Initialese}
 */
$xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} */
xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IIrqTestTouchscreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IIrqTestTouchscreenARCaster filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/** @interface */
$xyz.swapee.wc.front.IIrqTestTouchscreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR} */
$xyz.swapee.wc.front.IIrqTestTouchscreenARCaster.prototype.asIIrqTestTouchscreenAR
/** @type {!xyz.swapee.wc.front.BoundIrqTestTouchscreenAR} */
$xyz.swapee.wc.front.IIrqTestTouchscreenARCaster.prototype.superIrqTestTouchscreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IIrqTestTouchscreenARCaster}
 */
xyz.swapee.wc.front.IIrqTestTouchscreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IIrqTestTouchscreenAR filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestTouchscreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreen}
 */
$xyz.swapee.wc.front.IIrqTestTouchscreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IIrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.IIrqTestTouchscreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IrqTestTouchscreenAR filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IIrqTestTouchscreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese>}
 */
$xyz.swapee.wc.front.IrqTestTouchscreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.IrqTestTouchscreenAR
/** @type {function(new: xyz.swapee.wc.front.IIrqTestTouchscreenAR, ...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese)} */
xyz.swapee.wc.front.IrqTestTouchscreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.IrqTestTouchscreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
$xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR)} */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IrqTestTouchscreenARConstructor filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestTouchscreenAR, ...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese)} */
xyz.swapee.wc.front.IrqTestTouchscreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.RecordIIrqTestTouchscreenAR filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIIrqTestTouchscreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIIrqTestTouchscreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestTouchscreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIIrqTestTouchscreen}
 */
$xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR} */
xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.BoundIrqTestTouchscreenAR filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundIrqTestTouchscreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIrqTestTouchscreenAR} */
xyz.swapee.wc.front.BoundIrqTestTouchscreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */