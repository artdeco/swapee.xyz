/** @const {?} */ $xyz.swapee.wc.IIrqTestProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IIrqTestProcessor.Initialese filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestComputer.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
$xyz.swapee.wc.IIrqTestProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestProcessor.Initialese} */
xyz.swapee.wc.IIrqTestProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IIrqTestProcessorCaster filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/** @interface */
$xyz.swapee.wc.IIrqTestProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestProcessor} */
$xyz.swapee.wc.IIrqTestProcessorCaster.prototype.asIIrqTestProcessor
/** @type {!xyz.swapee.wc.BoundIrqTestProcessor} */
$xyz.swapee.wc.IIrqTestProcessorCaster.prototype.superIrqTestProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestProcessorCaster}
 */
xyz.swapee.wc.IIrqTestProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IIrqTestProcessor filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestProcessorCaster}
 * @extends {xyz.swapee.wc.IIrqTestComputer}
 * @extends {xyz.swapee.wc.IIrqTestCore}
 * @extends {xyz.swapee.wc.IIrqTestController}
 */
$xyz.swapee.wc.IIrqTestProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestProcessor}
 */
xyz.swapee.wc.IIrqTestProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IrqTestProcessor filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestProcessor.Initialese>}
 */
$xyz.swapee.wc.IrqTestProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.IrqTestProcessor
/** @type {function(new: xyz.swapee.wc.IIrqTestProcessor, ...!xyz.swapee.wc.IIrqTestProcessor.Initialese)} */
xyz.swapee.wc.IrqTestProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.IrqTestProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.AbstractIrqTestProcessor filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestProcessor}
 */
$xyz.swapee.wc.AbstractIrqTestProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestProcessor)} */
xyz.swapee.wc.AbstractIrqTestProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IrqTestProcessorConstructor filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestProcessor, ...!xyz.swapee.wc.IIrqTestProcessor.Initialese)} */
xyz.swapee.wc.IrqTestProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.RecordIIrqTestProcessor filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.BoundIIrqTestProcessor filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {xyz.swapee.wc.BoundIIrqTestCore}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 */
$xyz.swapee.wc.BoundIIrqTestProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestProcessor} */
xyz.swapee.wc.BoundIIrqTestProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.BoundIrqTestProcessor filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestProcessor} */
xyz.swapee.wc.BoundIrqTestProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */