/** @const {?} */ $xyz.swapee.wc.IIrqTestDesigner
/** @const {?} */ $xyz.swapee.wc.IIrqTestDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IIrqTestDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @interface */
$xyz.swapee.wc.IIrqTestDesigner = function() {}
/**
 * @param {xyz.swapee.wc.IrqTestClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IIrqTestDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.IrqTestClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IIrqTestDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IIrqTestDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IIrqTestDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IIrqTestDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.IrqTestClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IIrqTestDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestDesigner}
 */
xyz.swapee.wc.IIrqTestDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IrqTestDesigner filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestDesigner}
 */
$xyz.swapee.wc.IrqTestDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.IrqTestDesigner}
 */
xyz.swapee.wc.IrqTestDesigner
/** @type {function(new: xyz.swapee.wc.IIrqTestDesigner)} */
xyz.swapee.wc.IrqTestDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @record */
$xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IIrqTestController} */
$xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh.prototype.IrqTest
/** @typedef {$xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh} */
xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner.relay.Mesh filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @record */
$xyz.swapee.wc.IIrqTestDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IIrqTestController} */
$xyz.swapee.wc.IIrqTestDesigner.relay.Mesh.prototype.IrqTest
/** @type {typeof xyz.swapee.wc.IIrqTestController} */
$xyz.swapee.wc.IIrqTestDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IIrqTestDesigner.relay.Mesh} */
xyz.swapee.wc.IIrqTestDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner.relay.MemPool filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @record */
$xyz.swapee.wc.IIrqTestDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.IrqTestMemory} */
$xyz.swapee.wc.IIrqTestDesigner.relay.MemPool.prototype.IrqTest
/** @type {!xyz.swapee.wc.IrqTestMemory} */
$xyz.swapee.wc.IIrqTestDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IIrqTestDesigner.relay.MemPool} */
xyz.swapee.wc.IIrqTestDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestDesigner.relay
/* @typal-end */