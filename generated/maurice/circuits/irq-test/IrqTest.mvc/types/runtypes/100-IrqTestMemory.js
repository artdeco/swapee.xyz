/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/100-IrqTestMemory.xml} xyz.swapee.wc.IrqTestMemory filter:!ControllerPlugin~props 02a5ad54393dff3c7b59744da377be76 */
/** @record */
$xyz.swapee.wc.IrqTestMemory = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.IrqTestMemory.prototype.core
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IrqTestMemory}
 */
xyz.swapee.wc.IrqTestMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */