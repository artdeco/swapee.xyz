/** @const {?} */ $xyz.swapee.wc.back.IIrqTestTouchscreenAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} */
xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IIrqTestTouchscreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IIrqTestTouchscreenATCaster filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/** @interface */
$xyz.swapee.wc.back.IIrqTestTouchscreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT} */
$xyz.swapee.wc.back.IIrqTestTouchscreenATCaster.prototype.asIIrqTestTouchscreenAT
/** @type {!xyz.swapee.wc.back.BoundIrqTestTouchscreenAT} */
$xyz.swapee.wc.back.IIrqTestTouchscreenATCaster.prototype.superIrqTestTouchscreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestTouchscreenATCaster}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IIrqTestTouchscreenAT filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.IIrqTestTouchscreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IrqTestTouchscreenAT filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestTouchscreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese>}
 */
$xyz.swapee.wc.back.IrqTestTouchscreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IrqTestTouchscreenAT
/** @type {function(new: xyz.swapee.wc.back.IIrqTestTouchscreenAT, ...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese)} */
xyz.swapee.wc.back.IrqTestTouchscreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IrqTestTouchscreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
$xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT)} */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IrqTestTouchscreenATConstructor filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestTouchscreenAT, ...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese)} */
xyz.swapee.wc.back.IrqTestTouchscreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.RecordIIrqTestTouchscreenAT filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestTouchscreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestTouchscreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT} */
xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.BoundIrqTestTouchscreenAT filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundIrqTestTouchscreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIrqTestTouchscreenAT} */
xyz.swapee.wc.back.BoundIrqTestTouchscreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */