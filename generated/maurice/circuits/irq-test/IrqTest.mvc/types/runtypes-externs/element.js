/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElement.Initialese  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.IIrqTestElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestElement.Initialese} */
xyz.swapee.wc.IIrqTestElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElementFields  aadcbc20b996cc62e9719f30f6825635 */
/** @interface */
$xyz.swapee.wc.IIrqTestElementFields = function() {}
/** @type {!xyz.swapee.wc.IIrqTestElement.Inputs} */
$xyz.swapee.wc.IIrqTestElementFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestElementFields}
 */
xyz.swapee.wc.IIrqTestElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElementCaster  aadcbc20b996cc62e9719f30f6825635 */
/** @interface */
$xyz.swapee.wc.IIrqTestElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIIrqTestElement} */
$xyz.swapee.wc.IIrqTestElementCaster.prototype.asIIrqTestElement
/** @type {!xyz.swapee.wc.BoundIrqTestElement} */
$xyz.swapee.wc.IIrqTestElementCaster.prototype.superIrqTestElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestElementCaster}
 */
xyz.swapee.wc.IIrqTestElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElement  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs, null>}
 */
$xyz.swapee.wc.IIrqTestElement = function() {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} model
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IIrqTestElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IIrqTestElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IIrqTestElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} [model]
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.IIrqTestElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IIrqTestElement}
 */
xyz.swapee.wc.IIrqTestElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IrqTestElement  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElement.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestElement.Initialese>}
 */
$xyz.swapee.wc.IrqTestElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.IrqTestElement
/** @type {function(new: xyz.swapee.wc.IIrqTestElement, ...!xyz.swapee.wc.IIrqTestElement.Initialese)} */
xyz.swapee.wc.IrqTestElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.IrqTestElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.AbstractIrqTestElement  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElement.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestElement}
 */
$xyz.swapee.wc.AbstractIrqTestElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractIrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement
/** @type {function(new: xyz.swapee.wc.AbstractIrqTestElement)} */
xyz.swapee.wc.AbstractIrqTestElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IIrqTestElement|typeof xyz.swapee.wc.IrqTestElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.__extend
/**
 * @param {...((!xyz.swapee.wc.IIrqTestElement|typeof xyz.swapee.wc.IrqTestElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.continues
/**
 * @param {...((!xyz.swapee.wc.IIrqTestElement|typeof xyz.swapee.wc.IrqTestElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestElement}
 */
xyz.swapee.wc.AbstractIrqTestElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IrqTestElementConstructor  aadcbc20b996cc62e9719f30f6825635 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestElement, ...!xyz.swapee.wc.IIrqTestElement.Initialese)} */
xyz.swapee.wc.IrqTestElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.RecordIIrqTestElement  aadcbc20b996cc62e9719f30f6825635 */
/** @typedef {{ solder: xyz.swapee.wc.IIrqTestElement.solder, render: xyz.swapee.wc.IIrqTestElement.render, server: xyz.swapee.wc.IIrqTestElement.server, inducer: xyz.swapee.wc.IIrqTestElement.inducer }} */
xyz.swapee.wc.RecordIIrqTestElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.BoundIIrqTestElement  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestElementFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestElement.Inputs, null>}
 */
$xyz.swapee.wc.BoundIIrqTestElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundIIrqTestElement} */
xyz.swapee.wc.BoundIIrqTestElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.BoundIrqTestElement  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundIrqTestElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundIrqTestElement} */
xyz.swapee.wc.BoundIrqTestElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElement.solder  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} model
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IIrqTestElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} model
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.IIrqTestElement}
 */
$xyz.swapee.wc.IIrqTestElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} model
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.IIrqTestElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement.solder} */
xyz.swapee.wc.IIrqTestElement.solder
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement._solder} */
xyz.swapee.wc.IIrqTestElement._solder
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement.__solder} */
xyz.swapee.wc.IIrqTestElement.__solder

// nss:xyz.swapee.wc.IIrqTestElement,$xyz.swapee.wc.IIrqTestElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElement.render  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IIrqTestElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.IrqTestMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IIrqTestElement.render
/** @typedef {function(this: xyz.swapee.wc.IIrqTestElement, !xyz.swapee.wc.IrqTestMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IIrqTestElement._render
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement.__render} */
xyz.swapee.wc.IIrqTestElement.__render

// nss:xyz.swapee.wc.IIrqTestElement,$xyz.swapee.wc.IIrqTestElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElement.server  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IIrqTestElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.IIrqTestElement}
 */
$xyz.swapee.wc.IIrqTestElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.IIrqTestElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement.server} */
xyz.swapee.wc.IIrqTestElement.server
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement._server} */
xyz.swapee.wc.IIrqTestElement._server
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement.__server} */
xyz.swapee.wc.IIrqTestElement.__server

// nss:xyz.swapee.wc.IIrqTestElement,$xyz.swapee.wc.IIrqTestElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElement.inducer  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} [model]
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} [port]
 */
$xyz.swapee.wc.IIrqTestElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} [model]
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} [port]
 * @this {xyz.swapee.wc.IIrqTestElement}
 */
$xyz.swapee.wc.IIrqTestElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} [model]
 * @param {!xyz.swapee.wc.IIrqTestElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.IIrqTestElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement.inducer} */
xyz.swapee.wc.IIrqTestElement.inducer
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement._inducer} */
xyz.swapee.wc.IIrqTestElement._inducer
/** @typedef {typeof $xyz.swapee.wc.IIrqTestElement.__inducer} */
xyz.swapee.wc.IIrqTestElement.__inducer

// nss:xyz.swapee.wc.IIrqTestElement,$xyz.swapee.wc.IIrqTestElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/130-IIrqTestElement.xml} xyz.swapee.wc.IIrqTestElement.Inputs  aadcbc20b996cc62e9719f30f6825635 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestPort.Inputs}
 * @extends {xyz.swapee.wc.IIrqTestDisplay.Queries}
 * @extends {xyz.swapee.wc.IIrqTestController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.IIrqTestElementPort.Inputs}
 */
$xyz.swapee.wc.IIrqTestElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IIrqTestElement.Inputs} */
xyz.swapee.wc.IIrqTestElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IIrqTestElement
/* @typal-end */