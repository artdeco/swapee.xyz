/**
 * @fileoverview
 * @externs
 */

xyz.swapee.wc.IIrqTestComputer={}
xyz.swapee.wc.IIrqTestPort={}
xyz.swapee.wc.IIrqTestCore={}
xyz.swapee.wc.front={}
xyz.swapee.wc.IIrqTestDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IIrqTestDisplay={}
xyz.swapee.wc.IIrqTestController={}
xyz.swapee.wc.IIrqTestTouchscreen={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.wc={}
$$xyz.swapee.wc.IIrqTestComputer={}
$$xyz.swapee.wc.IIrqTestPort={}
$$xyz.swapee.wc.IIrqTestCore={}
$$xyz.swapee.wc.IIrqTestDisplay={}
$$xyz.swapee.wc.back={}
$$xyz.swapee.wc.back.IIrqTestDisplay={}
$$xyz.swapee.wc.IIrqTestController={}
$$xyz.swapee.wc.IIrqTestTouchscreen={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
xyz.swapee.wc.IIrqTestComputer.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/** @interface */
xyz.swapee.wc.IIrqTestComputerCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestComputer} */
xyz.swapee.wc.IIrqTestComputerCaster.prototype.asIIrqTestComputer
/** @type {!xyz.swapee.wc.BoundIrqTestComputer} */
xyz.swapee.wc.IIrqTestComputerCaster.prototype.superIrqTestComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.IrqTestMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
xyz.swapee.wc.IIrqTestComputer = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init */
xyz.swapee.wc.IIrqTestComputer.prototype.constructor = function(...init) {}
/**
 * @param {xyz.swapee.wc.IrqTestMemory} mem
 * @return {void}
 */
xyz.swapee.wc.IIrqTestComputer.prototype.compute = function(mem) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IrqTestComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestComputer.Initialese>}
 */
xyz.swapee.wc.IrqTestComputer = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init */
xyz.swapee.wc.IrqTestComputer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.IrqTestComputer.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.AbstractIrqTestComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestComputer.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestComputer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestComputer}
 */
xyz.swapee.wc.AbstractIrqTestComputer.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IrqTestComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestComputer, ...!xyz.swapee.wc.IIrqTestComputer.Initialese)} */
xyz.swapee.wc.IrqTestComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.RecordIIrqTestComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/** @typedef {{ compute: xyz.swapee.wc.IIrqTestComputer.compute }} */
xyz.swapee.wc.RecordIIrqTestComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.BoundIIrqTestComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.IrqTestMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
xyz.swapee.wc.BoundIIrqTestComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.BoundIrqTestComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/02-IIrqTestComputer.xml} xyz.swapee.wc.IIrqTestComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props aade9056d51880e07671c7d35f1d1e6f */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.IrqTestMemory} mem
 * @return {void}
 */
$$xyz.swapee.wc.IIrqTestComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.IrqTestMemory): void} */
xyz.swapee.wc.IIrqTestComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IIrqTestComputer, xyz.swapee.wc.IrqTestMemory): void} */
xyz.swapee.wc.IIrqTestComputer._compute
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestComputer.__compute} */
xyz.swapee.wc.IIrqTestComputer.__compute

// nss:xyz.swapee.wc.IIrqTestComputer,$$xyz.swapee.wc.IIrqTestComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
xyz.swapee.wc.IIrqTestOuterCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @interface */
xyz.swapee.wc.IIrqTestOuterCoreFields
/** @type {!xyz.swapee.wc.IIrqTestOuterCore.Model} */
xyz.swapee.wc.IIrqTestOuterCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @interface */
xyz.swapee.wc.IIrqTestOuterCoreCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestOuterCore} */
xyz.swapee.wc.IIrqTestOuterCoreCaster.prototype.asIIrqTestOuterCore
/** @type {!xyz.swapee.wc.BoundIrqTestOuterCore} */
xyz.swapee.wc.IIrqTestOuterCoreCaster.prototype.superIrqTestOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreCaster}
 */
xyz.swapee.wc.IIrqTestOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IrqTestOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestOuterCore.Initialese>}
 */
xyz.swapee.wc.IrqTestOuterCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.IrqTestOuterCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.AbstractIrqTestOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore = function() {}
/**
 * @param {...(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestOuterCore}
 */
xyz.swapee.wc.AbstractIrqTestOuterCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.IrqTestMemoryPQs = function() {}
/** @type {string} */
xyz.swapee.wc.IrqTestMemoryPQs.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.IrqTestMemoryQPs = function() {}
/** @type {string} */
xyz.swapee.wc.IrqTestMemoryQPs.prototype.a74ad

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.RecordIIrqTestOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.BoundIIrqTestOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestOuterCoreCaster}
 */
xyz.swapee.wc.BoundIIrqTestOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.BoundIrqTestOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model.Core.core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @typedef {string} */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model.Core}
 */
xyz.swapee.wc.IIrqTestOuterCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core}
 */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
xyz.swapee.wc.IIrqTestPort.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @interface */
xyz.swapee.wc.IIrqTestPortFields
/** @type {!xyz.swapee.wc.IIrqTestPort.Inputs} */
xyz.swapee.wc.IIrqTestPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IIrqTestPort.Inputs} */
xyz.swapee.wc.IIrqTestPortFields.prototype.props

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @interface */
xyz.swapee.wc.IIrqTestPortCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestPort} */
xyz.swapee.wc.IIrqTestPortCaster.prototype.asIIrqTestPort
/** @type {!xyz.swapee.wc.BoundIrqTestPort} */
xyz.swapee.wc.IIrqTestPortCaster.prototype.superIrqTestPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IIrqTestPort.Inputs>}
 */
xyz.swapee.wc.IIrqTestPort = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init */
xyz.swapee.wc.IIrqTestPort.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IIrqTestPort.prototype.resetPort = function() {}
/** @return {void} */
xyz.swapee.wc.IIrqTestPort.prototype.resetIrqTestPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IrqTestPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestPort.Initialese>}
 */
xyz.swapee.wc.IrqTestPort = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init */
xyz.swapee.wc.IrqTestPort.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.IrqTestPort.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.AbstractIrqTestPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestPort.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestPort.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestPort|typeof xyz.swapee.wc.IrqTestPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestPort}
 */
xyz.swapee.wc.AbstractIrqTestPort.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IrqTestPortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestPort, ...!xyz.swapee.wc.IIrqTestPort.Initialese)} */
xyz.swapee.wc.IrqTestPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IrqTestMemoryPQs}
 */
xyz.swapee.wc.IrqTestInputsPQs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestMemoryPQs}
 * @dict
 */
xyz.swapee.wc.IrqTestInputsQPs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.RecordIIrqTestPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/** @typedef {{ resetPort: xyz.swapee.wc.IIrqTestPort.resetPort, resetIrqTestPort: xyz.swapee.wc.IIrqTestPort.resetIrqTestPort }} */
xyz.swapee.wc.RecordIIrqTestPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.BoundIIrqTestPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestPortFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IIrqTestPort.Inputs>}
 */
xyz.swapee.wc.BoundIIrqTestPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.BoundIrqTestPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IIrqTestPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IIrqTestPort): void} */
xyz.swapee.wc.IIrqTestPort._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestPort.__resetPort} */
xyz.swapee.wc.IIrqTestPort.__resetPort

// nss:xyz.swapee.wc.IIrqTestPort,$$xyz.swapee.wc.IIrqTestPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.resetIrqTestPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestPort.resetIrqTestPort
/** @typedef {function(this: xyz.swapee.wc.IIrqTestPort): void} */
xyz.swapee.wc.IIrqTestPort._resetIrqTestPort
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort} */
xyz.swapee.wc.IIrqTestPort.__resetIrqTestPort

// nss:xyz.swapee.wc.IIrqTestPort,$$xyz.swapee.wc.IIrqTestPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel}
 */
xyz.swapee.wc.IIrqTestPort.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/04-IIrqTestPort.xml} xyz.swapee.wc.IIrqTestPort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 73e425e5975d11b37093b7cbbbb4e903 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel}
 */
xyz.swapee.wc.IIrqTestPort.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @record */
xyz.swapee.wc.IIrqTestCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @interface */
xyz.swapee.wc.IIrqTestCoreFields
/** @type {!xyz.swapee.wc.IIrqTestCore.Model} */
xyz.swapee.wc.IIrqTestCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @interface */
xyz.swapee.wc.IIrqTestCoreCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestCore} */
xyz.swapee.wc.IIrqTestCoreCaster.prototype.asIIrqTestCore
/** @type {!xyz.swapee.wc.BoundIrqTestCore} */
xyz.swapee.wc.IIrqTestCoreCaster.prototype.superIrqTestCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCoreCaster}
 * @extends {xyz.swapee.wc.IIrqTestOuterCore}
 */
xyz.swapee.wc.IIrqTestCore = function() {}
/** @return {void} */
xyz.swapee.wc.IIrqTestCore.prototype.resetCore = function() {}
/** @return {void} */
xyz.swapee.wc.IIrqTestCore.prototype.resetIrqTestCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IrqTestCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestCore.Initialese>}
 */
xyz.swapee.wc.IrqTestCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.IrqTestCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.AbstractIrqTestCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @constructor
 * @extends {xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore = function() {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestOuterCore|typeof xyz.swapee.wc.IrqTestOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestCore}
 */
xyz.swapee.wc.AbstractIrqTestCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.RecordIIrqTestCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/** @typedef {{ resetCore: xyz.swapee.wc.IIrqTestCore.resetCore, resetIrqTestCore: xyz.swapee.wc.IIrqTestCore.resetIrqTestCore }} */
xyz.swapee.wc.RecordIIrqTestCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.BoundIIrqTestCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestCoreFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCoreCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestOuterCore}
 */
xyz.swapee.wc.BoundIIrqTestCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.BoundIrqTestCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IIrqTestCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IIrqTestCore): void} */
xyz.swapee.wc.IIrqTestCore._resetCore
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestCore.__resetCore} */
xyz.swapee.wc.IIrqTestCore.__resetCore

// nss:xyz.swapee.wc.IIrqTestCore,$$xyz.swapee.wc.IIrqTestCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.resetIrqTestCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestCore.resetIrqTestCore
/** @typedef {function(this: xyz.swapee.wc.IIrqTestCore): void} */
xyz.swapee.wc.IIrqTestCore._resetIrqTestCore
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore} */
xyz.swapee.wc.IIrqTestCore.__resetIrqTestCore

// nss:xyz.swapee.wc.IIrqTestCore,$$xyz.swapee.wc.IIrqTestCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/09-IIrqTestCore.xml} xyz.swapee.wc.IIrqTestCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 05cd1bd78f8ee417d25f52d258d4453e */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model}
 */
xyz.swapee.wc.IIrqTestCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>}
 */
xyz.swapee.wc.IIrqTestController.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IIrqTestProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestComputer.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
xyz.swapee.wc.IIrqTestProcessor.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IIrqTestProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/** @interface */
xyz.swapee.wc.IIrqTestProcessorCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestProcessor} */
xyz.swapee.wc.IIrqTestProcessorCaster.prototype.asIIrqTestProcessor
/** @type {!xyz.swapee.wc.BoundIrqTestProcessor} */
xyz.swapee.wc.IIrqTestProcessorCaster.prototype.superIrqTestProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @interface */
xyz.swapee.wc.IIrqTestControllerFields
/** @type {!xyz.swapee.wc.IIrqTestController.Inputs} */
xyz.swapee.wc.IIrqTestControllerFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @interface */
xyz.swapee.wc.IIrqTestControllerCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestController} */
xyz.swapee.wc.IIrqTestControllerCaster.prototype.asIIrqTestController
/** @type {!xyz.swapee.wc.BoundIIrqTestProcessor} */
xyz.swapee.wc.IIrqTestControllerCaster.prototype.asIIrqTestProcessor
/** @type {!xyz.swapee.wc.BoundIrqTestController} */
xyz.swapee.wc.IIrqTestControllerCaster.prototype.superIrqTestController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IrqTestMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
xyz.swapee.wc.IIrqTestController = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init */
xyz.swapee.wc.IIrqTestController.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IIrqTestController.prototype.resetPort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IIrqTestProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestProcessorCaster}
 * @extends {xyz.swapee.wc.IIrqTestComputer}
 * @extends {xyz.swapee.wc.IIrqTestCore}
 * @extends {xyz.swapee.wc.IIrqTestController}
 */
xyz.swapee.wc.IIrqTestProcessor = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init */
xyz.swapee.wc.IIrqTestProcessor.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IrqTestProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestProcessor.Initialese>}
 */
xyz.swapee.wc.IrqTestProcessor = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init */
xyz.swapee.wc.IrqTestProcessor.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.IrqTestProcessor.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.AbstractIrqTestProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestProcessor.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestProcessor.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestCore|typeof xyz.swapee.wc.IrqTestCore)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestProcessor}
 */
xyz.swapee.wc.AbstractIrqTestProcessor.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.IrqTestProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestProcessor, ...!xyz.swapee.wc.IIrqTestProcessor.Initialese)} */
xyz.swapee.wc.IrqTestProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.RecordIIrqTestProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.RecordIIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @typedef {{ resetPort: xyz.swapee.wc.IIrqTestController.resetPort }} */
xyz.swapee.wc.RecordIIrqTestController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.BoundIIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestControllerFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IIrqTestOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IIrqTestController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IIrqTestController.Inputs, !xyz.swapee.wc.IrqTestMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
xyz.swapee.wc.BoundIIrqTestController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.BoundIIrqTestProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {xyz.swapee.wc.BoundIIrqTestCore}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 */
xyz.swapee.wc.BoundIIrqTestProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/10-IIrqTestProcessor.xml} xyz.swapee.wc.BoundIrqTestProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6e596152fe97c25809e030bfdfddf87a */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/100-IrqTestMemory.xml} xyz.swapee.wc.IrqTestMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 02a5ad54393dff3c7b59744da377be76 */
/** @record */
xyz.swapee.wc.IrqTestMemory = function() {}
/** @type {string} */
xyz.swapee.wc.IrqTestMemory.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/102-IrqTestInputs.xml} xyz.swapee.wc.front.IrqTestInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7e41453f1ba6b911fd383b7d52532b38 */
/** @record */
xyz.swapee.wc.front.IrqTestInputs = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.front.IrqTestInputs.prototype.core

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTestEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @record */
xyz.swapee.wc.IrqTestEnv = function() {}
/** @type {xyz.swapee.wc.IIrqTest} */
xyz.swapee.wc.IrqTestEnv.prototype.irqTest

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {xyz.swapee.wc.IIrqTestProcessor.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestComputer.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
xyz.swapee.wc.IIrqTest.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTestFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @interface */
xyz.swapee.wc.IIrqTestFields
/** @type {!xyz.swapee.wc.IIrqTest.Pinout} */
xyz.swapee.wc.IIrqTestFields.prototype.pinout

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTestCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @interface */
xyz.swapee.wc.IIrqTestCaster
/** @type {!xyz.swapee.wc.BoundIIrqTest} */
xyz.swapee.wc.IIrqTestCaster.prototype.asIIrqTest
/** @type {!xyz.swapee.wc.BoundIrqTest} */
xyz.swapee.wc.IIrqTestCaster.prototype.superIrqTest

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCaster}
 * @extends {xyz.swapee.wc.IIrqTestProcessor}
 * @extends {xyz.swapee.wc.IIrqTestComputer}
 * @extends {xyz.swapee.wc.IIrqTestController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, null>}
 */
xyz.swapee.wc.IIrqTest = function() {}
/** @param {...!xyz.swapee.wc.IIrqTest.Initialese} init */
xyz.swapee.wc.IIrqTest.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTest exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTest}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTest.Initialese>}
 */
xyz.swapee.wc.IrqTest = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTest.Initialese} init */
xyz.swapee.wc.IrqTest.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.IrqTest.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.AbstractIrqTest exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTest.Initialese} init
 * @extends {xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTest}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTest.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTest}
 */
xyz.swapee.wc.AbstractIrqTest.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTestConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @typedef {function(new: xyz.swapee.wc.IIrqTest, ...!xyz.swapee.wc.IIrqTest.Initialese)} */
xyz.swapee.wc.IrqTestConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @record */
xyz.swapee.wc.IIrqTest.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IIrqTest.Pinout)|undefined} */
xyz.swapee.wc.IIrqTest.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IIrqTest.Pinout)|undefined} */
xyz.swapee.wc.IIrqTest.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IIrqTest.Pinout} */
xyz.swapee.wc.IIrqTest.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.IrqTestMemory)|undefined} */
xyz.swapee.wc.IIrqTest.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.IrqTestClasses)|undefined} */
xyz.swapee.wc.IIrqTest.MVCOptions.prototype.classes

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.RecordIIrqTest exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTest

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.BoundIIrqTest exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestFields}
 * @extends {xyz.swapee.wc.RecordIIrqTest}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestProcessor}
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, null>}
 */
xyz.swapee.wc.BoundIIrqTest = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.BoundIrqTest exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTest}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTest = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestPort.Inputs}
 */
xyz.swapee.wc.IIrqTestController.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTest.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestController.Inputs}
 */
xyz.swapee.wc.IIrqTest.Pinout = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IIrqTestBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
xyz.swapee.wc.IIrqTestBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/11-IIrqTest.xml} xyz.swapee.wc.IrqTestBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 71205dcdc292023022a0b2e6c35489ba */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestBuffer}
 */
xyz.swapee.wc.IrqTestBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IIrqTestDisplay.Initialese}
 */
xyz.swapee.wc.IIrqTestGPU.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IIrqTestHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IIrqTestController.Initialese}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese}
 * @extends {xyz.swapee.wc.IIrqTest.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestProcessor.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestComputer.Initialese}
 * @extends {IIrqTestGenerator.Initialese}
 */
xyz.swapee.wc.IIrqTestHtmlComponent.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IIrqTestHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/** @interface */
xyz.swapee.wc.IIrqTestHtmlComponentCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestHtmlComponent} */
xyz.swapee.wc.IIrqTestHtmlComponentCaster.prototype.asIIrqTestHtmlComponent
/** @type {!xyz.swapee.wc.BoundIrqTestHtmlComponent} */
xyz.swapee.wc.IIrqTestHtmlComponentCaster.prototype.superIrqTestHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IIrqTestGPUFields
/** @type {!Object<string, string>} */
xyz.swapee.wc.IIrqTestGPUFields.prototype.vdusPQs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IIrqTestGPUCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestGPU} */
xyz.swapee.wc.IIrqTestGPUCaster.prototype.asIIrqTestGPU
/** @type {!xyz.swapee.wc.BoundIrqTestGPU} */
xyz.swapee.wc.IIrqTestGPUCaster.prototype.superIrqTestGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IIrqTestGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!IrqTestMemory,>}
 * @extends {xyz.swapee.wc.back.IIrqTestDisplay}
 */
xyz.swapee.wc.IIrqTestGPU = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init */
xyz.swapee.wc.IIrqTestGPU.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IIrqTestHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IIrqTestController}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreen}
 * @extends {xyz.swapee.wc.IIrqTest}
 * @extends {xyz.swapee.wc.IIrqTestGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.IIrqTestProcessor}
 * @extends {xyz.swapee.wc.IIrqTestComputer}
 * @extends {IIrqTestGenerator}
 */
xyz.swapee.wc.IIrqTestHtmlComponent = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init */
xyz.swapee.wc.IIrqTestHtmlComponent.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IrqTestHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese>}
 */
xyz.swapee.wc.IrqTestHtmlComponent = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init */
xyz.swapee.wc.IrqTestHtmlComponent.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.IrqTestHtmlComponent.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.AbstractIrqTestHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestHtmlComponent|typeof xyz.swapee.wc.IrqTestHtmlComponent)|(!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.IIrqTest|typeof xyz.swapee.wc.IrqTest)|(!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IIrqTestProcessor|typeof xyz.swapee.wc.IrqTestProcessor)|(!xyz.swapee.wc.IIrqTestComputer|typeof xyz.swapee.wc.IrqTestComputer)|(!IIrqTestGenerator|typeof IrqTestGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestHtmlComponent}
 */
xyz.swapee.wc.AbstractIrqTestHtmlComponent.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.IrqTestHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestHtmlComponent, ...!xyz.swapee.wc.IIrqTestHtmlComponent.Initialese)} */
xyz.swapee.wc.IrqTestHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.RecordIIrqTestHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.RecordIIrqTestGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIIrqTestGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.BoundIIrqTestGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestGPUFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!IrqTestMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestDisplay}
 */
xyz.swapee.wc.BoundIIrqTestGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.BoundIIrqTestHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIIrqTestHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestController}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreen}
 * @extends {xyz.swapee.wc.BoundIIrqTest}
 * @extends {xyz.swapee.wc.BoundIIrqTestGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IIrqTestController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundIIrqTestProcessor}
 * @extends {xyz.swapee.wc.BoundIIrqTestComputer}
 * @extends {BoundIIrqTestGenerator}
 */
xyz.swapee.wc.BoundIIrqTestHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/12-IIrqTestHtmlComponent.xml} xyz.swapee.wc.BoundIrqTestHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a4af8d3a5407eca50e3e6e590cd0b593 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @interface */
xyz.swapee.wc.IIrqTestDesigner = function() {}
/**
 * @param {xyz.swapee.wc.IrqTestClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IIrqTestDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.IrqTestClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IIrqTestDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh} mesh
 * @return {?}
 */
xyz.swapee.wc.IIrqTestDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IIrqTestDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IIrqTestDesigner.relay.MemPool} memPool
 * @return {?}
 */
xyz.swapee.wc.IIrqTestDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.IrqTestClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IIrqTestDesigner.prototype.lendClasses = function(classes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IrqTestDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IIrqTestDesigner}
 */
xyz.swapee.wc.IrqTestDesigner = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @record */
xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IIrqTestController} */
xyz.swapee.wc.IIrqTestDesigner.communicator.Mesh.prototype.IrqTest

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @record */
xyz.swapee.wc.IIrqTestDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IIrqTestController} */
xyz.swapee.wc.IIrqTestDesigner.relay.Mesh.prototype.IrqTest
/** @type {typeof xyz.swapee.wc.IIrqTestController} */
xyz.swapee.wc.IIrqTestDesigner.relay.Mesh.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/170-IIrqTestDesigner.xml} xyz.swapee.wc.IIrqTestDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8c8d61bbdeac1b07af7c8c5f3c8405e1 */
/** @record */
xyz.swapee.wc.IIrqTestDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.IrqTestMemory} */
xyz.swapee.wc.IIrqTestDesigner.relay.MemPool.prototype.IrqTest
/** @type {!xyz.swapee.wc.IrqTestMemory} */
xyz.swapee.wc.IIrqTestDesigner.relay.MemPool.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings>}
 */
xyz.swapee.wc.IIrqTestDisplay.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @interface */
xyz.swapee.wc.IIrqTestDisplayFields
/** @type {!xyz.swapee.wc.IIrqTestDisplay.Settings} */
xyz.swapee.wc.IIrqTestDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IIrqTestDisplay.Queries} */
xyz.swapee.wc.IIrqTestDisplayFields.prototype.queries

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @interface */
xyz.swapee.wc.IIrqTestDisplayCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestDisplay} */
xyz.swapee.wc.IIrqTestDisplayCaster.prototype.asIIrqTestDisplay
/** @type {!xyz.swapee.wc.BoundIIrqTestTouchscreen} */
xyz.swapee.wc.IIrqTestDisplayCaster.prototype.asIIrqTestTouchscreen
/** @type {!xyz.swapee.wc.BoundIrqTestDisplay} */
xyz.swapee.wc.IIrqTestDisplayCaster.prototype.superIrqTestDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.IrqTestMemory, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, xyz.swapee.wc.IIrqTestDisplay.Queries, null>}
 */
xyz.swapee.wc.IIrqTestDisplay = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init */
xyz.swapee.wc.IIrqTestDisplay.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {null} land
 * @return {void}
 */
xyz.swapee.wc.IIrqTestDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestDisplay.Initialese>}
 */
xyz.swapee.wc.IrqTestDisplay = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init */
xyz.swapee.wc.IrqTestDisplay.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.IrqTestDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.AbstractIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestDisplay.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestDisplay}
 */
xyz.swapee.wc.AbstractIrqTestDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IrqTestDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestDisplay, ...!xyz.swapee.wc.IIrqTestDisplay.Initialese)} */
xyz.swapee.wc.IrqTestDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IrqTestGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestGPU.Initialese>}
 */
xyz.swapee.wc.IrqTestGPU = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init */
xyz.swapee.wc.IrqTestGPU.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.IrqTestGPU.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.AbstractIrqTestGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestGPU.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestGPU.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestGPU|typeof xyz.swapee.wc.IrqTestGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestGPU}
 */
xyz.swapee.wc.AbstractIrqTestGPU.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.IrqTestGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestGPU, ...!xyz.swapee.wc.IIrqTestGPU.Initialese)} */
xyz.swapee.wc.IrqTestGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/80-IIrqTestGPU.xml} xyz.swapee.wc.BoundIrqTestGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.RecordIIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @typedef {{ paint: xyz.swapee.wc.IIrqTestDisplay.paint }} */
xyz.swapee.wc.RecordIIrqTestDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.BoundIIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestDisplayFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.IrqTestMemory, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, xyz.swapee.wc.IIrqTestDisplay.Queries, null>}
 */
xyz.swapee.wc.BoundIIrqTestDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.BoundIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} memory
 * @param {null} land
 * @return {void}
 */
$$xyz.swapee.wc.IIrqTestDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.IrqTestMemory, null): void} */
xyz.swapee.wc.IIrqTestDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IIrqTestDisplay, !xyz.swapee.wc.IrqTestMemory, null): void} */
xyz.swapee.wc.IIrqTestDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestDisplay.__paint} */
xyz.swapee.wc.IIrqTestDisplay.__paint

// nss:xyz.swapee.wc.IIrqTestDisplay,$$xyz.swapee.wc.IIrqTestDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/** @record */
xyz.swapee.wc.IIrqTestDisplay.Queries = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplay.xml} xyz.swapee.wc.IIrqTestDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0943077255d6ee63860d4c60329d2bb3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestDisplay.Queries}
 */
xyz.swapee.wc.IIrqTestDisplay.Settings = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.IrqTestClasses>}
 */
xyz.swapee.wc.back.IIrqTestDisplay.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/** @interface */
xyz.swapee.wc.back.IIrqTestDisplayCaster
/** @type {!xyz.swapee.wc.back.BoundIIrqTestDisplay} */
xyz.swapee.wc.back.IIrqTestDisplayCaster.prototype.asIIrqTestDisplay
/** @type {!xyz.swapee.wc.back.BoundIrqTestDisplay} */
xyz.swapee.wc.back.IIrqTestDisplayCaster.prototype.superIrqTestDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IrqTestClasses, null>}
 */
xyz.swapee.wc.back.IIrqTestDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.IrqTestMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
xyz.swapee.wc.back.IIrqTestDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IIrqTestDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestDisplay.Initialese>}
 */
xyz.swapee.wc.back.IrqTestDisplay = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.IrqTestDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.AbstractIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay = function() {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestDisplay|typeof xyz.swapee.wc.back.IrqTestDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestDisplay}
 */
xyz.swapee.wc.back.AbstractIrqTestDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.IrqTestVdusPQs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/110-IrqTestSerDes.xml} xyz.swapee.wc.IrqTestVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.IrqTestVdusQPs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.RecordIIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/** @typedef {{ paint: xyz.swapee.wc.back.IIrqTestDisplay.paint }} */
xyz.swapee.wc.back.RecordIIrqTestDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.BoundIIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.IrqTestClasses, null>}
 */
xyz.swapee.wc.back.BoundIIrqTestDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.BoundIrqTestDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundIrqTestDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/40-IIrqTestDisplayBack.xml} xyz.swapee.wc.back.IIrqTestDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29c4729e9de06b1395d747b24d4e0565 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IrqTestMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$$xyz.swapee.wc.back.IIrqTestDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.IrqTestMemory=, null=): void} */
xyz.swapee.wc.back.IIrqTestDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IIrqTestDisplay, !xyz.swapee.wc.IrqTestMemory=, null=): void} */
xyz.swapee.wc.back.IIrqTestDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.back.IIrqTestDisplay.__paint} */
xyz.swapee.wc.back.IIrqTestDisplay.__paint

// nss:xyz.swapee.wc.back.IIrqTestDisplay,$$xyz.swapee.wc.back.IIrqTestDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/41-IrqTestClasses.xml} xyz.swapee.wc.IrqTestClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 2060d8e7a8edfa939198f8e2ebc2ce78 */
/** @record */
xyz.swapee.wc.IrqTestClasses = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestController.Initialese>}
 */
xyz.swapee.wc.IrqTestController = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init */
xyz.swapee.wc.IrqTestController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.IrqTestController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.AbstractIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestController.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestController}
 */
xyz.swapee.wc.AbstractIrqTestController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IrqTestControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestController, ...!xyz.swapee.wc.IIrqTestController.Initialese)} */
xyz.swapee.wc.IrqTestControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.BoundIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IIrqTestController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IIrqTestController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IIrqTestController): void} */
xyz.swapee.wc.IIrqTestController._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestController.__resetPort} */
xyz.swapee.wc.IIrqTestController.__resetPort

// nss:xyz.swapee.wc.IIrqTestController,$$xyz.swapee.wc.IIrqTestController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/50-IIrqTestController.xml} xyz.swapee.wc.IIrqTestController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4baa941ccd1b081a82b4ad055628d3a3 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestPort.WeakInputs}
 */
xyz.swapee.wc.IIrqTestController.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IIrqTestController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @record */
xyz.swapee.wc.front.IIrqTestController.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IIrqTestControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @interface */
xyz.swapee.wc.front.IIrqTestControllerCaster
/** @type {!xyz.swapee.wc.front.BoundIIrqTestController} */
xyz.swapee.wc.front.IIrqTestControllerCaster.prototype.asIIrqTestController
/** @type {!xyz.swapee.wc.front.BoundIrqTestController} */
xyz.swapee.wc.front.IIrqTestControllerCaster.prototype.superIrqTestController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IIrqTestControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/** @interface */
xyz.swapee.wc.front.IIrqTestControllerATCaster
/** @type {!xyz.swapee.wc.front.BoundIIrqTestControllerAT} */
xyz.swapee.wc.front.IIrqTestControllerATCaster.prototype.asIIrqTestControllerAT
/** @type {!xyz.swapee.wc.front.BoundIrqTestControllerAT} */
xyz.swapee.wc.front.IIrqTestControllerATCaster.prototype.superIrqTestControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IIrqTestControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.front.IIrqTestControllerAT = function() {}
/** @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init */
xyz.swapee.wc.front.IIrqTestControllerAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerAT}
 */
xyz.swapee.wc.front.IIrqTestController = function() {}
/** @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init */
xyz.swapee.wc.front.IIrqTestController.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init
 * @implements {xyz.swapee.wc.front.IIrqTestController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestController.Initialese>}
 */
xyz.swapee.wc.front.IrqTestController = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init */
xyz.swapee.wc.front.IrqTestController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.IrqTestController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.AbstractIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestController.Initialese} init
 * @extends {xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestController}
 */
xyz.swapee.wc.front.AbstractIrqTestController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.IrqTestControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestController, ...!xyz.swapee.wc.front.IIrqTestController.Initialese)} */
xyz.swapee.wc.front.IrqTestControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.RecordIIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIIrqTestController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.RecordIIrqTestControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIIrqTestControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.BoundIIrqTestControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIIrqTestControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.front.BoundIIrqTestControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.BoundIIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIIrqTestController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIIrqTestControllerAT}
 */
xyz.swapee.wc.front.BoundIIrqTestController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/51-IIrqTestControllerFront.xml} xyz.swapee.wc.front.BoundIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9c17094660aad6ca5ab68ea36bd7a4f3 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIIrqTestController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundIrqTestController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IIrqTestController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IIrqTestController.Inputs>}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
xyz.swapee.wc.back.IIrqTestController.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IIrqTestControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/** @interface */
xyz.swapee.wc.back.IIrqTestControllerCaster
/** @type {!xyz.swapee.wc.back.BoundIIrqTestController} */
xyz.swapee.wc.back.IIrqTestControllerCaster.prototype.asIIrqTestController
/** @type {!xyz.swapee.wc.back.BoundIrqTestController} */
xyz.swapee.wc.back.IIrqTestControllerCaster.prototype.superIrqTestController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.IIrqTestController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
xyz.swapee.wc.back.IIrqTestController = function() {}
/** @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init */
xyz.swapee.wc.back.IIrqTestController.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestController.Initialese>}
 */
xyz.swapee.wc.back.IrqTestController = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init */
xyz.swapee.wc.back.IrqTestController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.IrqTestController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.AbstractIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestController.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestController|typeof xyz.swapee.wc.back.IrqTestController)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestController}
 */
xyz.swapee.wc.back.AbstractIrqTestController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.IrqTestControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestController, ...!xyz.swapee.wc.back.IIrqTestController.Initialese)} */
xyz.swapee.wc.back.IrqTestControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.RecordIIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.BoundIIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerCaster}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IIrqTestController.Inputs>}
 */
xyz.swapee.wc.back.BoundIIrqTestController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/52-IIrqTestControllerBack.xml} xyz.swapee.wc.back.BoundIrqTestController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 58eb466d38c7ce33d05c95e9b4a27f2f */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundIrqTestController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IIrqTestControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestController.Initialese}
 */
xyz.swapee.wc.back.IIrqTestControllerAR.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IIrqTestControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/** @interface */
xyz.swapee.wc.back.IIrqTestControllerARCaster
/** @type {!xyz.swapee.wc.back.BoundIIrqTestControllerAR} */
xyz.swapee.wc.back.IIrqTestControllerARCaster.prototype.asIIrqTestControllerAR
/** @type {!xyz.swapee.wc.back.BoundIrqTestControllerAR} */
xyz.swapee.wc.back.IIrqTestControllerARCaster.prototype.superIrqTestControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IIrqTestControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IIrqTestController}
 */
xyz.swapee.wc.back.IIrqTestControllerAR = function() {}
/** @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init */
xyz.swapee.wc.back.IIrqTestControllerAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IrqTestControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese>}
 */
xyz.swapee.wc.back.IrqTestControllerAR = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init */
xyz.swapee.wc.back.IrqTestControllerAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.IrqTestControllerAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.AbstractIrqTestControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestControllerAR|typeof xyz.swapee.wc.back.IrqTestControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestController|typeof xyz.swapee.wc.IrqTestController))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestControllerAR}
 */
xyz.swapee.wc.back.AbstractIrqTestControllerAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.IrqTestControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestControllerAR, ...!xyz.swapee.wc.back.IIrqTestControllerAR.Initialese)} */
xyz.swapee.wc.back.IrqTestControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.RecordIIrqTestControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.BoundIIrqTestControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIIrqTestController}
 */
xyz.swapee.wc.back.BoundIIrqTestControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/53-IIrqTestControllerAR.xml} xyz.swapee.wc.back.BoundIrqTestControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 285ac5531c1323b1989a3a12419f1165 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundIrqTestControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IIrqTestControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.front.IIrqTestControllerAT.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IrqTestControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IIrqTestControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese>}
 */
xyz.swapee.wc.front.IrqTestControllerAT = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init */
xyz.swapee.wc.front.IrqTestControllerAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.IrqTestControllerAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.AbstractIrqTestControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestControllerAT|typeof xyz.swapee.wc.front.IrqTestControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestControllerAT}
 */
xyz.swapee.wc.front.AbstractIrqTestControllerAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.IrqTestControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestControllerAT, ...!xyz.swapee.wc.front.IIrqTestControllerAT.Initialese)} */
xyz.swapee.wc.front.IrqTestControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/54-IIrqTestControllerAT.xml} xyz.swapee.wc.front.BoundIrqTestControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cf2367e24c3b3ea189fcce5aca8ae914 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIIrqTestControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundIrqTestControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null>}
 * @extends {IIrqTestInterruptLine.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestDisplay.Initialese}
 */
xyz.swapee.wc.IIrqTestTouchscreen.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreenFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @interface */
xyz.swapee.wc.IIrqTestTouchscreenFields
/** @type {HTMLElement} */
xyz.swapee.wc.IIrqTestTouchscreenFields.prototype.keyboardItem
/** @type {boolean} */
xyz.swapee.wc.IIrqTestTouchscreenFields.prototype.menuExpanded

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @interface */
xyz.swapee.wc.IIrqTestTouchscreenCaster
/** @type {!xyz.swapee.wc.BoundIIrqTestTouchscreen} */
xyz.swapee.wc.IIrqTestTouchscreenCaster.prototype.asIIrqTestTouchscreen
/** @type {!xyz.swapee.wc.BoundIrqTestTouchscreen} */
xyz.swapee.wc.IIrqTestTouchscreenCaster.prototype.superIrqTestTouchscreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @interface
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IIrqTestController}
 * @extends {IIrqTestInterruptLine}
 * @extends {xyz.swapee.wc.IIrqTestDisplay}
 */
xyz.swapee.wc.IIrqTestTouchscreen = function() {}
/** @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init */
xyz.swapee.wc.IIrqTestTouchscreen.prototype.constructor = function(...init) {}
/**
 * @param {*} memory
 * @return {?}
 */
xyz.swapee.wc.IIrqTestTouchscreen.prototype.stashKeyboardItemAfterMenuExpanded = function(memory) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.IIrqTestTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IIrqTestTouchscreen.Initialese>}
 */
xyz.swapee.wc.IrqTestTouchscreen = function(...init) {}
/** @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init */
xyz.swapee.wc.IrqTestTouchscreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.IrqTestTouchscreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.AbstractIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IIrqTestController|typeof xyz.swapee.wc.front.IrqTestController)|(!IIrqTestInterruptLine|typeof IrqTestInterruptLine)|(!xyz.swapee.wc.IIrqTestDisplay|typeof xyz.swapee.wc.IrqTestDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.IrqTestTouchscreen}
 */
xyz.swapee.wc.AbstractIrqTestTouchscreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IrqTestTouchscreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @typedef {function(new: xyz.swapee.wc.IIrqTestTouchscreen, ...!xyz.swapee.wc.IIrqTestTouchscreen.Initialese)} */
xyz.swapee.wc.IrqTestTouchscreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.RecordIIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/** @typedef {{ stashKeyboardItemAfterMenuExpanded: xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded }} */
xyz.swapee.wc.RecordIIrqTestTouchscreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.BoundIIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenFields}
 * @extends {xyz.swapee.wc.RecordIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.IrqTestMemory, !xyz.swapee.wc.front.IrqTestInputs, !HTMLDivElement, !xyz.swapee.wc.IIrqTestDisplay.Settings, !xyz.swapee.wc.IIrqTestDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIIrqTestController}
 * @extends {BoundIIrqTestInterruptLine}
 * @extends {xyz.swapee.wc.BoundIIrqTestDisplay}
 */
xyz.swapee.wc.BoundIIrqTestTouchscreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.BoundIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundIrqTestTouchscreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreen.xml} xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a371c2115fe076d48b1a9e737748cbcf */
/**
 * @this {THIS}
 * @template THIS
 * @param {*} memory
 */
$$xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded = function(memory) {}
/** @typedef {function(*)} */
xyz.swapee.wc.IIrqTestTouchscreen.stashKeyboardItemAfterMenuExpanded
/** @typedef {function(this: xyz.swapee.wc.IIrqTestTouchscreen, *)} */
xyz.swapee.wc.IIrqTestTouchscreen._stashKeyboardItemAfterMenuExpanded
/** @typedef {typeof $$xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded} */
xyz.swapee.wc.IIrqTestTouchscreen.__stashKeyboardItemAfterMenuExpanded

// nss:xyz.swapee.wc.IIrqTestTouchscreen,$$xyz.swapee.wc.IIrqTestTouchscreen,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese}
 */
xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IIrqTestTouchscreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/** @interface */
xyz.swapee.wc.back.IIrqTestTouchscreenCaster
/** @type {!xyz.swapee.wc.back.BoundIIrqTestTouchscreen} */
xyz.swapee.wc.back.IIrqTestTouchscreenCaster.prototype.asIIrqTestTouchscreen
/** @type {!xyz.swapee.wc.back.BoundIrqTestTouchscreen} */
xyz.swapee.wc.back.IIrqTestTouchscreenCaster.prototype.superIrqTestTouchscreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IIrqTestTouchscreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/** @interface */
xyz.swapee.wc.back.IIrqTestTouchscreenATCaster
/** @type {!xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT} */
xyz.swapee.wc.back.IIrqTestTouchscreenATCaster.prototype.asIIrqTestTouchscreenAT
/** @type {!xyz.swapee.wc.back.BoundIrqTestTouchscreenAT} */
xyz.swapee.wc.back.IIrqTestTouchscreenATCaster.prototype.superIrqTestTouchscreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IIrqTestTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.back.IIrqTestTouchscreenAT = function() {}
/** @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init */
xyz.swapee.wc.back.IIrqTestTouchscreenAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IIrqTestTouchscreen = function() {}
/** @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init */
xyz.swapee.wc.back.IIrqTestTouchscreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestTouchscreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese>}
 */
xyz.swapee.wc.back.IrqTestTouchscreen = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init */
xyz.swapee.wc.back.IrqTestTouchscreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.IrqTestTouchscreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.AbstractIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreen|typeof xyz.swapee.wc.back.IrqTestTouchscreen)|(!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreen}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.IrqTestTouchscreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestTouchscreen, ...!xyz.swapee.wc.back.IIrqTestTouchscreen.Initialese)} */
xyz.swapee.wc.back.IrqTestTouchscreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.RecordIIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestTouchscreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.RecordIIrqTestTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIIrqTestTouchscreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestTouchscreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.BoundIIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IIrqTestTouchscreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.BoundIIrqTestTouchscreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/70-IIrqTestTouchscreenBack.xml} xyz.swapee.wc.back.BoundIrqTestTouchscreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e65c5285355b12f10099372d112274e5 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundIrqTestTouchscreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreen.Initialese}
 */
xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IIrqTestTouchscreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/** @interface */
xyz.swapee.wc.front.IIrqTestTouchscreenARCaster
/** @type {!xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR} */
xyz.swapee.wc.front.IIrqTestTouchscreenARCaster.prototype.asIIrqTestTouchscreenAR
/** @type {!xyz.swapee.wc.front.BoundIrqTestTouchscreenAR} */
xyz.swapee.wc.front.IIrqTestTouchscreenARCaster.prototype.superIrqTestTouchscreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IIrqTestTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestTouchscreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IIrqTestTouchscreen}
 */
xyz.swapee.wc.front.IIrqTestTouchscreenAR = function() {}
/** @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init */
xyz.swapee.wc.front.IIrqTestTouchscreenAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IrqTestTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IIrqTestTouchscreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese>}
 */
xyz.swapee.wc.front.IrqTestTouchscreenAR = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init */
xyz.swapee.wc.front.IrqTestTouchscreenAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.IrqTestTouchscreenAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IIrqTestTouchscreenAR|typeof xyz.swapee.wc.front.IrqTestTouchscreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IIrqTestTouchscreen|typeof xyz.swapee.wc.IrqTestTouchscreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.IrqTestTouchscreenAR}
 */
xyz.swapee.wc.front.AbstractIrqTestTouchscreenAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.IrqTestTouchscreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/** @typedef {function(new: xyz.swapee.wc.front.IIrqTestTouchscreenAR, ...!xyz.swapee.wc.front.IIrqTestTouchscreenAR.Initialese)} */
xyz.swapee.wc.front.IrqTestTouchscreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.RecordIIrqTestTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIIrqTestTouchscreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIIrqTestTouchscreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IIrqTestTouchscreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIIrqTestTouchscreen}
 */
xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/73-IIrqTestTouchscreenAR.xml} xyz.swapee.wc.front.BoundIrqTestTouchscreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4fa34f26dea3f86d63f4c1df79276bfd */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIIrqTestTouchscreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundIrqTestTouchscreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IrqTestTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IIrqTestTouchscreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese>}
 */
xyz.swapee.wc.back.IrqTestTouchscreenAT = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init */
xyz.swapee.wc.back.IrqTestTouchscreenAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.IrqTestTouchscreenAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IIrqTestTouchscreenAT|typeof xyz.swapee.wc.back.IrqTestTouchscreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.IrqTestTouchscreenAT}
 */
xyz.swapee.wc.back.AbstractIrqTestTouchscreenAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.IrqTestTouchscreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/** @typedef {function(new: xyz.swapee.wc.back.IIrqTestTouchscreenAT, ...!xyz.swapee.wc.back.IIrqTestTouchscreenAT.Initialese)} */
xyz.swapee.wc.back.IrqTestTouchscreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/74-IIrqTestTouchscreenAT.xml} xyz.swapee.wc.back.BoundIrqTestTouchscreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 66ba436809d812aad641c755e455f946 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIIrqTestTouchscreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundIrqTestTouchscreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/** @record */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe.prototype.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.Inputs.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core}
 */
xyz.swapee.wc.IIrqTestPort.Inputs.Core = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.Inputs.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe}
 */
xyz.swapee.wc.IIrqTestPort.Inputs.Core_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.WeakInputs.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core}
 */
xyz.swapee.wc.IIrqTestPort.WeakInputs.Core = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestPort.WeakInputs.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.WeakModel.Core_Safe}
 */
xyz.swapee.wc.IIrqTestPort.WeakInputs.Core_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestCore.Model.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model.Core}
 */
xyz.swapee.wc.IIrqTestCore.Model.Core = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/irq-test/IrqTest.mvc/design/03-IIrqTestOuterCore.xml} xyz.swapee.wc.IIrqTestCore.Model.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d419d16cb0da9ef4cac8d8609b34187 */
/**
 * @record
 * @extends {xyz.swapee.wc.IIrqTestOuterCore.Model.Core_Safe}
 */
xyz.swapee.wc.IIrqTestCore.Model.Core_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */