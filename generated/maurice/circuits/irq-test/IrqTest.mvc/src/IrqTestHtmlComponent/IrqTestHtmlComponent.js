import IrqTestHtmlController from '../IrqTestHtmlController'
import IrqTestVirtualDisplay from '../IrqTestVirtualDisplay'
import IrqTestVirtualScreen from '../IrqTestVirtualScreen'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractIrqTestHtmlComponent} from '../../gen/AbstractIrqTestHtmlComponent'

/** @extends {xyz.swapee.wc.IrqTestHtmlComponent} */
export default class extends AbstractIrqTestHtmlComponent.implements(
 IrqTestHtmlController,
 IrqTestVirtualDisplay,
 IrqTestVirtualScreen,
 IntegratedComponentInitialiser,
){}