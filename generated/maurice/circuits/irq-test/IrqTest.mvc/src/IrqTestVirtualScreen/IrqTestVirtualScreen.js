import {Painter} from '@webcircuits/webcircuits'

/** @extends {xyz.swapee.wc.IrqTestVirtualScreen} */
export default class extends Painter.continues(
 /**@type {!xyz.swapee.wc.IIrqTestVirtualScreen}*/({
  paint:[
   function paint_stashKeyboardItemAfterMenuExpanded({core:core}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    const _mem=serMemory({core:core})
    t_pa({
     pid:'4f3ddab',
     mem:_mem,
    })
   },
  ],
 }),
){}