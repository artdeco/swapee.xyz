import paint from './methods/paint'
import AbstractIrqTestDisplay from '../../gen/AbstractIrqTestDisplay'

/** @extends {xyz.swapee.wc.IrqTestDisplay} */
export default class extends AbstractIrqTestDisplay.implements(
 /** @type {!xyz.swapee.wc.IIrqTestDisplay} */ ({
  paint:paint,
 }),
){}