import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import IrqTestServerController from '../IrqTestServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractIrqTestElement from '../../gen/AbstractIrqTestElement'

/** @extends {xyz.swapee.wc.IrqTestElement} */
export default class IrqTestElement extends AbstractIrqTestElement.implements(
 IrqTestServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IIrqTestElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IIrqTestElement}*/({
   classesMap:       true,
   rootSelector:     `.IrqTest`,
   stylesheet:       'html/styles/IrqTest.css',
   blockName:        'html/IrqTestBlock.html',
  }),
){}

// thank you for using web circuits
