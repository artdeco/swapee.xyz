import paint from './methods/paint'
import {Painter} from '@webcircuits/webcircuits'

const IrqTestVirtualDisplay=Painter.__trait(
 /** @type {!com.webcircuits.IPainter} */ ({
  paint:paint,
 }),
)
export default IrqTestVirtualDisplay