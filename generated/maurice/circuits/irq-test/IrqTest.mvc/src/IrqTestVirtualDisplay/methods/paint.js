/** @type {xyz.swapee.wc.back.IIrqTestDisplay._paint} */
export default function paint({core:core}){
  const{
   asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
  }=/**@type {!xyz.swapee.wc.back.IIrqTestDisplay}*/(this)
  const _mem=serMemory({core:core})
  t_pa({
   pid:'4704c46',
   mem:_mem,
  })
 }