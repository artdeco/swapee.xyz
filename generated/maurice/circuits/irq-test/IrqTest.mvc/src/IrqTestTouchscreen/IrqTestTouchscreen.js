import stashKeyboardItemAfterMenuExpanded from './methods/stash-keyboard-item-after-menu-expanded'
import AbstractIrqTestControllerAT from '../../gen/AbstractIrqTestControllerAT'
import HyperIrqTestInterruptLine from '../../../parts/120-interrupt-line/src/IrqTestInterruptLine/aop/HyperIrqTestInterruptLine'
import paint_stashKeyboardItemAfterMenuExpanded from '../../../parts/70-touchscreen/src/methods/stash-keyboard-item-after-menu-expanded'
import IrqTestDisplay from '../IrqTestDisplay'
import AbstractIrqTestTouchscreen from '../../gen/AbstractIrqTestTouchscreen'

/** @extends {xyz.swapee.wc.IrqTestTouchscreen} */
export default class extends AbstractIrqTestTouchscreen.implements(
 /**@type {!xyz.swapee.wc.IIrqTestTouchscreen}*/({get queries(){return this.settings}}),
 AbstractIrqTestControllerAT,
 HyperIrqTestInterruptLine,
 /**@type {!xyz.swapee.wc.IIrqTestTouchscreen}*/({
  paint:[
   paint_stashKeyboardItemAfterMenuExpanded,
  ],
 }),
 IrqTestDisplay,
 /** @type {!xyz.swapee.wc.IIrqTestTouchscreen} */ ({
  stashKeyboardItemAfterMenuExpanded:stashKeyboardItemAfterMenuExpanded,
  __$id:2940675042,
 }),
){}