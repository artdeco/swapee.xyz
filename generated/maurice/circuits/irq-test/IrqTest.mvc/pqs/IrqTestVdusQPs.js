import {IrqTestVdusPQs} from './IrqTestVdusPQs'
export const IrqTestVdusQPs=/**@type {!xyz.swapee.wc.IrqTestVdusQPs}*/(Object.keys(IrqTestVdusPQs)
 .reduce((a,k)=>{a[IrqTestVdusPQs[k]]=k;return a},{}))