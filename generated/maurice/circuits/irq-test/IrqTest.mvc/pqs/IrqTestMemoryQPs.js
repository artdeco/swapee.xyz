import {IrqTestMemoryPQs} from './IrqTestMemoryPQs'
export const IrqTestMemoryQPs=/**@type {!xyz.swapee.wc.IrqTestMemoryQPs}*/(Object.keys(IrqTestMemoryPQs)
 .reduce((a,k)=>{a[IrqTestMemoryPQs[k]]=k;return a},{}))