import {IrqTestInputsPQs} from './IrqTestInputsPQs'
export const IrqTestInputsQPs=/**@type {!xyz.swapee.wc.IrqTestInputsQPs}*/(Object.keys(IrqTestInputsPQs)
 .reduce((a,k)=>{a[IrqTestInputsPQs[k]]=k;return a},{}))