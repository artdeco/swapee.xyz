/** @extends {xyz.swapee.wc.AbstractSwapeeMeAccount} */
export default class AbstractSwapeeMeAccount extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractSwapeeMeAccountComputer} */
export class AbstractSwapeeMeAccountComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeMeAccountController} */
export class AbstractSwapeeMeAccountController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeMeAccountPort} */
export class SwapeeMeAccountPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractSwapeeMeAccountView} */
export class AbstractSwapeeMeAccountView extends (<view>
  <classes>
   <string opt name="Shown">The class.</string>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeMeAccountElement} */
export class AbstractSwapeeMeAccountElement extends (<element v3 html mv>
 <block src="./SwapeeMeAccount.mvc/src/SwapeeMeAccountElement/methods/render.jsx" />
 <inducer src="./SwapeeMeAccount.mvc/src/SwapeeMeAccountElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent} */
export class AbstractSwapeeMeAccountHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.ISwapeeMe via="SwapeeMe" />
  </connectors>

</html-ic>) { }
// </class-end>