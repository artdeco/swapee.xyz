import AbstractHyperSwapeeMeAccountInterruptLine from '../../../../gen/AbstractSwapeeMeAccountInterruptLine/hyper/AbstractHyperSwapeeMeAccountInterruptLine'
import SwapeeMeAccountInterruptLine from '../../SwapeeMeAccountInterruptLine'
import SwapeeMeAccountInterruptLineGeneralAspects from '../SwapeeMeAccountInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperSwapeeMeAccountInterruptLine} */
export default class extends AbstractHyperSwapeeMeAccountInterruptLine
 .consults(
  SwapeeMeAccountInterruptLineGeneralAspects,
 )
 .implements(
  SwapeeMeAccountInterruptLine,
 )
{}