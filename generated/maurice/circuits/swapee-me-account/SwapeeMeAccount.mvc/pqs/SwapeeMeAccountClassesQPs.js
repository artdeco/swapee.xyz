import SwapeeMeAccountClassesPQs from './SwapeeMeAccountClassesPQs'
export const SwapeeMeAccountClassesQPs=/**@type {!xyz.swapee.wc.SwapeeMeAccountClassesQPs}*/(Object.keys(SwapeeMeAccountClassesPQs)
 .reduce((a,k)=>{a[SwapeeMeAccountClassesPQs[k]]=k;return a},{}))