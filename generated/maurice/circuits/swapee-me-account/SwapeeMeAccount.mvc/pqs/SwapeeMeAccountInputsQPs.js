import {SwapeeMeAccountInputsPQs} from './SwapeeMeAccountInputsPQs'
export const SwapeeMeAccountInputsQPs=/**@type {!xyz.swapee.wc.SwapeeMeAccountInputsQPs}*/(Object.keys(SwapeeMeAccountInputsPQs)
 .reduce((a,k)=>{a[SwapeeMeAccountInputsPQs[k]]=k;return a},{}))