import {SwapeeMeAccountMemoryPQs} from './SwapeeMeAccountMemoryPQs'
export const SwapeeMeAccountMemoryQPs=/**@type {!xyz.swapee.wc.SwapeeMeAccountMemoryQPs}*/(Object.keys(SwapeeMeAccountMemoryPQs)
 .reduce((a,k)=>{a[SwapeeMeAccountMemoryPQs[k]]=k;return a},{}))