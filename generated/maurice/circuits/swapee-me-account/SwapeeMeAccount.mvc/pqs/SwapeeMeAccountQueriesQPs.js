import {SwapeeMeAccountQueriesPQs} from './SwapeeMeAccountQueriesPQs'
export const SwapeeMeAccountQueriesQPs=/**@type {!xyz.swapee.wc.SwapeeMeAccountQueriesQPs}*/(Object.keys(SwapeeMeAccountQueriesPQs)
 .reduce((a,k)=>{a[SwapeeMeAccountQueriesPQs[k]]=k;return a},{}))