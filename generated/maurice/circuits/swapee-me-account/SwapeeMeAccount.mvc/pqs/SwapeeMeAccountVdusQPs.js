import {SwapeeMeAccountVdusPQs} from './SwapeeMeAccountVdusPQs'
export const SwapeeMeAccountVdusQPs=/**@type {!xyz.swapee.wc.SwapeeMeAccountVdusQPs}*/(Object.keys(SwapeeMeAccountVdusPQs)
 .reduce((a,k)=>{a[SwapeeMeAccountVdusPQs[k]]=k;return a},{}))