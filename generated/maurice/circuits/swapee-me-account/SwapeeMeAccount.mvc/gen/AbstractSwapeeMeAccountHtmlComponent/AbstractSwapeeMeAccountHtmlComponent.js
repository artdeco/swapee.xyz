import AbstractSwapeeMeAccountGPU from '../AbstractSwapeeMeAccountGPU'
import AbstractSwapeeMeAccountScreenBack from '../AbstractSwapeeMeAccountScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {SwapeeMeAccountInputsQPs} from '../../pqs/SwapeeMeAccountInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractSwapeeMeAccount from '../AbstractSwapeeMeAccount'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountHtmlComponent}
 */
function __AbstractSwapeeMeAccountHtmlComponent() {}
__AbstractSwapeeMeAccountHtmlComponent.prototype = /** @type {!_AbstractSwapeeMeAccountHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent}
 */
class _AbstractSwapeeMeAccountHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.SwapeeMeAccountHtmlComponent} */ (res)
  }
}
/**
 * The _ISwapeeMeAccount_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent} ‎
 */
export class AbstractSwapeeMeAccountHtmlComponent extends newAbstract(
 _AbstractSwapeeMeAccountHtmlComponent,623632859812,null,{
  asISwapeeMeAccountHtmlComponent:1,
  superSwapeeMeAccountHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent} */
AbstractSwapeeMeAccountHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent} */
function AbstractSwapeeMeAccountHtmlComponentClass(){}


AbstractSwapeeMeAccountHtmlComponent[$implementations]=[
 __AbstractSwapeeMeAccountHtmlComponent,
 HtmlComponent,
 AbstractSwapeeMeAccount,
 AbstractSwapeeMeAccountGPU,
 AbstractSwapeeMeAccountScreenBack,
 Landed,
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  constructor(){
   this.land={
    SwapeeMe:null,
   }
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  inputsQPs:SwapeeMeAccountInputsQPs,
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function paint_Shown_on_element(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '94a08':token,
   }=SwapeeMe.model
   const{
    asISwapeeMeAccountGPU:{
     element:element,
    },
    classes:{Shown:Shown},
    asIBrowserView:{addClass:addClass,removeClass:removeClass},
   }=this
   if(token!==undefined) addClass(element,Shown)
   else removeClass(element,Shown)
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function paint_TokenLa_Content(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '94a08':token,
   }=SwapeeMe.model
   const{asISwapeeMeAccountGPU:{TokenLa:TokenLa}}=this
   TokenLa.setText(token)
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function paint_UsernameLa_Content(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '4498e':displayName,
   }=SwapeeMe.model
   const{asISwapeeMeAccountGPU:{UsernameLa:UsernameLa}}=this
   UsernameLa.setText(displayName)
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function $reveal_UserBlock(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '94a08':token,
   }=SwapeeMe.model
   const{
    asISwapeeMeAccountGPU:{UserBlock:UserBlock},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(UserBlock,token||false)
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function $conceal_NoUserBlock(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '94a08':token,
   }=SwapeeMe.model
   const{
    asISwapeeMeAccountGPU:{NoUserBlock:NoUserBlock},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(NoUserBlock,token||false)
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function interrupt_click_on_NoUserBlock(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   const{asISwapeeMeAccountGPU:{NoUserBlock:NoUserBlock}}=this
   NoUserBlock.listen('click',(ev)=>{
    SwapeeMe['2cef1']()
   })
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function paint_src_on_UserpicIm(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '08906':userpic,
   }=SwapeeMe.model
   const{asISwapeeMeAccountGPU:{UserpicIm:UserpicIm},asIBrowserView:{attr:attr}}=this
   attr(UserpicIm,'src',userpic)
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function paint_alt_on_UserpicIm(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   let{
    '14c4b':username,
   }=SwapeeMe.model
   const{asISwapeeMeAccountGPU:{UserpicIm:UserpicIm},asIBrowserView:{attr:attr}}=this
   attr(UserpicIm,'alt',username)
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  paint:function interrupt_click_on_SignOutBu(_,{SwapeeMe:SwapeeMe}){
   if(!SwapeeMe) return
   const{asISwapeeMeAccountGPU:{SignOutBu:SignOutBu}}=this
   SignOutBu.listen('click',(ev)=>{
    SwapeeMe['14272']()
   })
  },
 }),
 AbstractSwapeeMeAccountHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asISwapeeMeAccountGPU:{
     SwapeeMe:SwapeeMe,
    },
   }=this
   complete(6200449439,{SwapeeMe:SwapeeMe})
  },
 }),
]