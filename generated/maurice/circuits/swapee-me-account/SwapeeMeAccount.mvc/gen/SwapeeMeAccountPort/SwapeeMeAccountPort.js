import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {SwapeeMeAccountInputsPQs} from '../../pqs/SwapeeMeAccountInputsPQs'
import {SwapeeMeAccountOuterCoreConstructor} from '../SwapeeMeAccountCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMeAccountPort}
 */
function __SwapeeMeAccountPort() {}
__SwapeeMeAccountPort.prototype = /** @type {!_SwapeeMeAccountPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMeAccountPort} */ function SwapeeMeAccountPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.SwapeeMeAccountOuterCore} */ ({model:null})
  SwapeeMeAccountOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountPort}
 */
class _SwapeeMeAccountPort { }
/**
 * The port that serves as an interface to the _ISwapeeMeAccount_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountPort} ‎
 */
export class SwapeeMeAccountPort extends newAbstract(
 _SwapeeMeAccountPort,62363285985,SwapeeMeAccountPortConstructor,{
  asISwapeeMeAccountPort:1,
  superSwapeeMeAccountPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountPort} */
SwapeeMeAccountPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountPort} */
function SwapeeMeAccountPortClass(){}

export const SwapeeMeAccountPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ISwapeeMeAccount.Pinout>}*/({
 get Port() { return SwapeeMeAccountPort },
})

SwapeeMeAccountPort[$implementations]=[
 SwapeeMeAccountPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountPort}*/({
  resetPort(){
   this.resetSwapeeMeAccountPort()
  },
  resetSwapeeMeAccountPort(){
   SwapeeMeAccountPortConstructor.call(this)
  },
 }),
 __SwapeeMeAccountPort,
 Parametric,
 SwapeeMeAccountPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountPort}*/({
  constructor(){
   mountPins(this.inputs,SwapeeMeAccountInputsPQs)
  },
 }),
]


export default SwapeeMeAccountPort