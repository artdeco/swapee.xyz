import AbstractSwapeeMeAccountControllerAR from '../AbstractSwapeeMeAccountControllerAR'
import {AbstractSwapeeMeAccountController} from '../AbstractSwapeeMeAccountController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountControllerBack}
 */
function __AbstractSwapeeMeAccountControllerBack() {}
__AbstractSwapeeMeAccountControllerBack.prototype = /** @type {!_AbstractSwapeeMeAccountControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountController}
 */
class _AbstractSwapeeMeAccountControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountController} ‎
 */
class AbstractSwapeeMeAccountControllerBack extends newAbstract(
 _AbstractSwapeeMeAccountControllerBack,623632859822,null,{
  asISwapeeMeAccountController:1,
  superSwapeeMeAccountController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountController} */
AbstractSwapeeMeAccountControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountController} */
function AbstractSwapeeMeAccountControllerBackClass(){}

export default AbstractSwapeeMeAccountControllerBack


AbstractSwapeeMeAccountControllerBack[$implementations]=[
 __AbstractSwapeeMeAccountControllerBack,
 AbstractSwapeeMeAccountController,
 AbstractSwapeeMeAccountControllerAR,
 DriverBack,
]