import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountDisplay}
 */
function __AbstractSwapeeMeAccountDisplay() {}
__AbstractSwapeeMeAccountDisplay.prototype = /** @type {!_AbstractSwapeeMeAccountDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay}
 */
class _AbstractSwapeeMeAccountDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay} ‎
 */
class AbstractSwapeeMeAccountDisplay extends newAbstract(
 _AbstractSwapeeMeAccountDisplay,623632859819,null,{
  asISwapeeMeAccountDisplay:1,
  superSwapeeMeAccountDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay} */
AbstractSwapeeMeAccountDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay} */
function AbstractSwapeeMeAccountDisplayClass(){}

export default AbstractSwapeeMeAccountDisplay


AbstractSwapeeMeAccountDisplay[$implementations]=[
 __AbstractSwapeeMeAccountDisplay,
 GraphicsDriverBack,
 AbstractSwapeeMeAccountDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeMeAccountDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.ISwapeeMeAccountDisplay}*/({
    TokenLa:twinMock,
    UserpicIm:twinMock,
    NoUserBlock:twinMock,
    UserBlock:twinMock,
    UsernameLa:twinMock,
    SignOutBu:twinMock,
    SwapeeMe:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.SwapeeMeAccountDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.ISwapeeMeAccountDisplay.Initialese}*/({
   TokenLa:1,
   UserpicIm:1,
   NoUserBlock:1,
   UserBlock:1,
   UsernameLa:1,
   SignOutBu:1,
   SwapeeMe:1,
  }),
  initializer({
   TokenLa:_TokenLa,
   UserpicIm:_UserpicIm,
   NoUserBlock:_NoUserBlock,
   UserBlock:_UserBlock,
   UsernameLa:_UsernameLa,
   SignOutBu:_SignOutBu,
   SwapeeMe:_SwapeeMe,
  }) {
   if(_TokenLa!==undefined) this.TokenLa=_TokenLa
   if(_UserpicIm!==undefined) this.UserpicIm=_UserpicIm
   if(_NoUserBlock!==undefined) this.NoUserBlock=_NoUserBlock
   if(_UserBlock!==undefined) this.UserBlock=_UserBlock
   if(_UsernameLa!==undefined) this.UsernameLa=_UsernameLa
   if(_SignOutBu!==undefined) this.SignOutBu=_SignOutBu
   if(_SwapeeMe!==undefined) this.SwapeeMe=_SwapeeMe
  },
 }),
]