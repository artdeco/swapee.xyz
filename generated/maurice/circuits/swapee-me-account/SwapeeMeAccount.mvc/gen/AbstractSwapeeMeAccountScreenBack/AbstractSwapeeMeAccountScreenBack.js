import AbstractSwapeeMeAccountScreenAT from '../AbstractSwapeeMeAccountScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountScreenBack}
 */
function __AbstractSwapeeMeAccountScreenBack() {}
__AbstractSwapeeMeAccountScreenBack.prototype = /** @type {!_AbstractSwapeeMeAccountScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen}
 */
class _AbstractSwapeeMeAccountScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen} ‎
 */
class AbstractSwapeeMeAccountScreenBack extends newAbstract(
 _AbstractSwapeeMeAccountScreenBack,623632859826,null,{
  asISwapeeMeAccountScreen:1,
  superSwapeeMeAccountScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen} */
AbstractSwapeeMeAccountScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen} */
function AbstractSwapeeMeAccountScreenBackClass(){}

export default AbstractSwapeeMeAccountScreenBack


AbstractSwapeeMeAccountScreenBack[$implementations]=[
 __AbstractSwapeeMeAccountScreenBack,
 AbstractSwapeeMeAccountScreenAT,
]