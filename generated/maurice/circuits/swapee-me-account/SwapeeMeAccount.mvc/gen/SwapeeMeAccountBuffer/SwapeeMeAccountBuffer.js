import {makeBuffers} from '@webcircuits/webcircuits'

export const SwapeeMeAccountBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  core:String,
 }),
})

export default SwapeeMeAccountBuffer