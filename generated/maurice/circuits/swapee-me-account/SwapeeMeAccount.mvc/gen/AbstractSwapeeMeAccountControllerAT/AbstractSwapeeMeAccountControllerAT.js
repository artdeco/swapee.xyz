import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountControllerAT}
 */
function __AbstractSwapeeMeAccountControllerAT() {}
__AbstractSwapeeMeAccountControllerAT.prototype = /** @type {!_AbstractSwapeeMeAccountControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT}
 */
class _AbstractSwapeeMeAccountControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeMeAccountControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT} ‎
 */
class AbstractSwapeeMeAccountControllerAT extends newAbstract(
 _AbstractSwapeeMeAccountControllerAT,623632859824,null,{
  asISwapeeMeAccountControllerAT:1,
  superSwapeeMeAccountControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT} */
AbstractSwapeeMeAccountControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT} */
function AbstractSwapeeMeAccountControllerATClass(){}

export default AbstractSwapeeMeAccountControllerAT


AbstractSwapeeMeAccountControllerAT[$implementations]=[
 __AbstractSwapeeMeAccountControllerAT,
 UartUniversal,
 AbstractSwapeeMeAccountControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT}*/({
  get asISwapeeMeAccountController(){
   return this
  },
 }),
]