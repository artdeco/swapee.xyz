import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountDisplay}
 */
function __AbstractSwapeeMeAccountDisplay() {}
__AbstractSwapeeMeAccountDisplay.prototype = /** @type {!_AbstractSwapeeMeAccountDisplay} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMeAccountDisplay} */ function SwapeeMeAccountDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.TokenLa=null
  /** @type {HTMLImageElement} */ this.UserpicIm=null
  /** @type {HTMLDivElement} */ this.NoUserBlock=null
  /** @type {HTMLDivElement} */ this.UserBlock=null
  /** @type {HTMLSpanElement} */ this.UsernameLa=null
  /** @type {HTMLButtonElement} */ this.SignOutBu=null
  /** @type {HTMLElement} */ this.SwapeeMe=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountDisplay}
 */
class _AbstractSwapeeMeAccountDisplay { }
/**
 * Display for presenting information from the _ISwapeeMeAccount_.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountDisplay} ‎
 */
class AbstractSwapeeMeAccountDisplay extends newAbstract(
 _AbstractSwapeeMeAccountDisplay,623632859817,SwapeeMeAccountDisplayConstructor,{
  asISwapeeMeAccountDisplay:1,
  superSwapeeMeAccountDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountDisplay} */
AbstractSwapeeMeAccountDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountDisplay} */
function AbstractSwapeeMeAccountDisplayClass(){}

export default AbstractSwapeeMeAccountDisplay


AbstractSwapeeMeAccountDisplay[$implementations]=[
 __AbstractSwapeeMeAccountDisplay,
 Display,
 AbstractSwapeeMeAccountDisplayClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{swapeeMeScopeSel:swapeeMeScopeSel}}=this
    this.scan({
     swapeeMeSel:swapeeMeScopeSel,
    })
   })
  },
  scan:function vduScan({swapeeMeSel:swapeeMeSelScope}){
   const{element:element,asISwapeeMeAccountScreen:{vdusPQs:{
    TokenLa:TokenLa,
    UserpicIm:UserpicIm,
    NoUserBlock:NoUserBlock,
    UserBlock:UserBlock,
    UsernameLa:UsernameLa,
    SignOutBu:SignOutBu,
   }},queries:{swapeeMeSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const SwapeeMe=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _SwapeeMe=swapeeMeSel?root.querySelector(swapeeMeSel):void 0
    return _SwapeeMe
   })(swapeeMeSelScope)
   Object.assign(this,{
    TokenLa:/**@type {HTMLSpanElement}*/(children[TokenLa]),
    UserpicIm:/**@type {HTMLImageElement}*/(children[UserpicIm]),
    NoUserBlock:/**@type {HTMLDivElement}*/(children[NoUserBlock]),
    UserBlock:/**@type {HTMLDivElement}*/(children[UserBlock]),
    UsernameLa:/**@type {HTMLSpanElement}*/(children[UsernameLa]),
    SignOutBu:/**@type {HTMLButtonElement}*/(children[SignOutBu]),
    SwapeeMe:SwapeeMe,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.SwapeeMeAccountDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese}*/({
   TokenLa:1,
   UserpicIm:1,
   NoUserBlock:1,
   UserBlock:1,
   UsernameLa:1,
   SignOutBu:1,
   SwapeeMe:1,
  }),
  initializer({
   TokenLa:_TokenLa,
   UserpicIm:_UserpicIm,
   NoUserBlock:_NoUserBlock,
   UserBlock:_UserBlock,
   UsernameLa:_UsernameLa,
   SignOutBu:_SignOutBu,
   SwapeeMe:_SwapeeMe,
  }) {
   if(_TokenLa!==undefined) this.TokenLa=_TokenLa
   if(_UserpicIm!==undefined) this.UserpicIm=_UserpicIm
   if(_NoUserBlock!==undefined) this.NoUserBlock=_NoUserBlock
   if(_UserBlock!==undefined) this.UserBlock=_UserBlock
   if(_UsernameLa!==undefined) this.UsernameLa=_UsernameLa
   if(_SignOutBu!==undefined) this.SignOutBu=_SignOutBu
   if(_SwapeeMe!==undefined) this.SwapeeMe=_SwapeeMe
  },
 }),
]