import SwapeeMeAccountBuffer from '../SwapeeMeAccountBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {SwapeeMeAccountPortConnector} from '../SwapeeMeAccountPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountController}
 */
function __AbstractSwapeeMeAccountController() {}
__AbstractSwapeeMeAccountController.prototype = /** @type {!_AbstractSwapeeMeAccountController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountController}
 */
class _AbstractSwapeeMeAccountController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountController} ‎
 */
export class AbstractSwapeeMeAccountController extends newAbstract(
 _AbstractSwapeeMeAccountController,623632859820,null,{
  asISwapeeMeAccountController:1,
  superSwapeeMeAccountController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountController} */
AbstractSwapeeMeAccountController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountController} */
function AbstractSwapeeMeAccountControllerClass(){}


AbstractSwapeeMeAccountController[$implementations]=[
 AbstractSwapeeMeAccountControllerClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.ISwapeeMeAccountPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractSwapeeMeAccountController,
 SwapeeMeAccountBuffer,
 IntegratedController,
 /**@type {!AbstractSwapeeMeAccountController}*/(SwapeeMeAccountPortConnector),
]


export default AbstractSwapeeMeAccountController