
import AbstractSwapeeMeAccount from '../AbstractSwapeeMeAccount'

/** @abstract {xyz.swapee.wc.ISwapeeMeAccountElement} */
export default class AbstractSwapeeMeAccountElement { }



AbstractSwapeeMeAccountElement[$implementations]=[AbstractSwapeeMeAccount,
 /** @type {!AbstractSwapeeMeAccountElement} */ ({
  rootId:'SwapeeMeAccount',
  __$id:6236328598,
  fqn:'xyz.swapee.wc.ISwapeeMeAccount',
  maurice_element_v3:true,
 }),
]