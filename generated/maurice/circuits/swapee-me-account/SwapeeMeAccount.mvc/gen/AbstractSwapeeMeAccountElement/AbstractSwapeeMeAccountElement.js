import SwapeeMeAccountRenderVdus from './methods/render-vdus'
import SwapeeMeAccountElementPort from '../SwapeeMeAccountElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {SwapeeMeAccountInputsPQs} from '../../pqs/SwapeeMeAccountInputsPQs'
import {SwapeeMeAccountQueriesPQs} from '../../pqs/SwapeeMeAccountQueriesPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractSwapeeMeAccount from '../AbstractSwapeeMeAccount'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountElement}
 */
function __AbstractSwapeeMeAccountElement() {}
__AbstractSwapeeMeAccountElement.prototype = /** @type {!_AbstractSwapeeMeAccountElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountElement}
 */
class _AbstractSwapeeMeAccountElement { }
/**
 * A component description.
 *
 * The _ISwapeeMeAccount_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountElement} ‎
 */
class AbstractSwapeeMeAccountElement extends newAbstract(
 _AbstractSwapeeMeAccountElement,623632859813,null,{
  asISwapeeMeAccountElement:1,
  superSwapeeMeAccountElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElement} */
AbstractSwapeeMeAccountElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElement} */
function AbstractSwapeeMeAccountElementClass(){}

export default AbstractSwapeeMeAccountElement


AbstractSwapeeMeAccountElement[$implementations]=[
 __AbstractSwapeeMeAccountElement,
 ElementBase,
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':core':coreColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'core':coreAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(coreAttr===undefined?{'core':coreColAttr}:{}),
   }
  },
 }),
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'core':coreAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    core:coreAttr,
   }
  },
 }),
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 Landed,
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.ISwapeeMeAccountElement}*/({land:{
    SwapeeMe:null,
   }}))
  },
  render:function renderSwapeeMe(){
   const{
    asILanded:{
     land:{
      SwapeeMe:SwapeeMe,
     },
    },
    asISwapeeMeAccountElement:{
     buildSwapeeMe:buildSwapeeMe,
    },
   }=this
   if(!SwapeeMe) return
   const{model:SwapeeMeModel}=SwapeeMe
   const{
    '94a08':token,
    '4498e':displayName,
    '08906':userpic,
    '14c4b':username,
   }=SwapeeMeModel
   const res=buildSwapeeMe({
    token:token,
    displayName:displayName,
    userpic:userpic,
    username:username,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  render:SwapeeMeAccountRenderVdus,
 }),
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  classes:{
   'Shown': 'a1921',
  },
  inputsPQs:SwapeeMeAccountInputsPQs,
  queriesPQs:SwapeeMeAccountQueriesPQs,
  vdus:{
   'TokenLa': 'ad091',
   'UserpicIm': 'ad092',
   'NoUserBlock': 'ad093',
   'UserBlock': 'ad094',
   'UsernameLa': 'ad095',
   'SignOutBu': 'ad096',
   'SwapeeMe': 'ad097',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','core','query:swapee-me','no-solder',':no-solder',':core','fe646','e12b1','fe8e1','2f0be','75ee3','3b1f1','cff70','4ccfe','a74ad','children']),
   })
  },
  get Port(){
   return SwapeeMeAccountElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:swapee-me':swapeeMeSel}){
   const _ret={}
   if(swapeeMeSel) _ret.swapeeMeSel=swapeeMeSel
   return _ret
  },
 }),
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  constructor(){
   this.land={
    SwapeeMe:null,
   }
  },
 }),
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  calibrate:async function awaitOnSwapeeMe({swapeeMeSel:swapeeMeSel}){
   if(!swapeeMeSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const SwapeeMe=await milleu(swapeeMeSel,true)
   if(!SwapeeMe) {
    console.warn('❗️ swapeeMeSel %s must be present on the page for %s to work',swapeeMeSel,fqn)
    return{}
   }
   land.SwapeeMe=SwapeeMe
  },
 }),
 AbstractSwapeeMeAccountElementClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
  solder:(_,{
   swapeeMeSel:swapeeMeSel,
  })=>{
   return{
    swapeeMeSel:swapeeMeSel,
   }
  },
 }),
]



AbstractSwapeeMeAccountElement[$implementations]=[AbstractSwapeeMeAccount,
 /** @type {!AbstractSwapeeMeAccountElement} */ ({
  rootId:'SwapeeMeAccount',
  __$id:6236328598,
  fqn:'xyz.swapee.wc.ISwapeeMeAccount',
  maurice_element_v3:true,
 }),
]