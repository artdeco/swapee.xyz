import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountProcessor}
 */
function __AbstractSwapeeMeAccountProcessor() {}
__AbstractSwapeeMeAccountProcessor.prototype = /** @type {!_AbstractSwapeeMeAccountProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountProcessor}
 */
class _AbstractSwapeeMeAccountProcessor { }
/**
 * The processor to compute changes to the memory for the _ISwapeeMeAccount_.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountProcessor} ‎
 */
class AbstractSwapeeMeAccountProcessor extends newAbstract(
 _AbstractSwapeeMeAccountProcessor,62363285988,null,{
  asISwapeeMeAccountProcessor:1,
  superSwapeeMeAccountProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountProcessor} */
AbstractSwapeeMeAccountProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountProcessor} */
function AbstractSwapeeMeAccountProcessorClass(){}

export default AbstractSwapeeMeAccountProcessor


AbstractSwapeeMeAccountProcessor[$implementations]=[
 __AbstractSwapeeMeAccountProcessor,
]