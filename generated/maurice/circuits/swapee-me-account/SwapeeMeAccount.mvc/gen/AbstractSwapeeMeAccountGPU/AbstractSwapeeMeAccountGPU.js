import AbstractSwapeeMeAccountDisplay from '../AbstractSwapeeMeAccountDisplayBack'
import SwapeeMeAccountClassesPQs from '../../pqs/SwapeeMeAccountClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {SwapeeMeAccountClassesQPs} from '../../pqs/SwapeeMeAccountClassesQPs'
import {SwapeeMeAccountVdusPQs} from '../../pqs/SwapeeMeAccountVdusPQs'
import {SwapeeMeAccountVdusQPs} from '../../pqs/SwapeeMeAccountVdusQPs'
import {SwapeeMeAccountMemoryPQs} from '../../pqs/SwapeeMeAccountMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountGPU}
 */
function __AbstractSwapeeMeAccountGPU() {}
__AbstractSwapeeMeAccountGPU.prototype = /** @type {!_AbstractSwapeeMeAccountGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountGPU}
 */
class _AbstractSwapeeMeAccountGPU { }
/**
 * Handles the periphery of the _ISwapeeMeAccountDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountGPU} ‎
 */
class AbstractSwapeeMeAccountGPU extends newAbstract(
 _AbstractSwapeeMeAccountGPU,623632859816,null,{
  asISwapeeMeAccountGPU:1,
  superSwapeeMeAccountGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountGPU} */
AbstractSwapeeMeAccountGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountGPU} */
function AbstractSwapeeMeAccountGPUClass(){}

export default AbstractSwapeeMeAccountGPU


AbstractSwapeeMeAccountGPU[$implementations]=[
 __AbstractSwapeeMeAccountGPU,
 AbstractSwapeeMeAccountGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountGPU}*/({
  classesQPs:SwapeeMeAccountClassesQPs,
  vdusPQs:SwapeeMeAccountVdusPQs,
  vdusQPs:SwapeeMeAccountVdusQPs,
  memoryPQs:SwapeeMeAccountMemoryPQs,
 }),
 AbstractSwapeeMeAccountDisplay,
 BrowserView,
 AbstractSwapeeMeAccountGPUClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountGPU}*/({
  allocator(){
   pressFit(this.classes,'',SwapeeMeAccountClassesPQs)
  },
 }),
]