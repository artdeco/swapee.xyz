import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountControllerAR}
 */
function __AbstractSwapeeMeAccountControllerAR() {}
__AbstractSwapeeMeAccountControllerAR.prototype = /** @type {!_AbstractSwapeeMeAccountControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR}
 */
class _AbstractSwapeeMeAccountControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeMeAccountControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR} ‎
 */
class AbstractSwapeeMeAccountControllerAR extends newAbstract(
 _AbstractSwapeeMeAccountControllerAR,623632859823,null,{
  asISwapeeMeAccountControllerAR:1,
  superSwapeeMeAccountControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR} */
AbstractSwapeeMeAccountControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR} */
function AbstractSwapeeMeAccountControllerARClass(){}

export default AbstractSwapeeMeAccountControllerAR


AbstractSwapeeMeAccountControllerAR[$implementations]=[
 __AbstractSwapeeMeAccountControllerAR,
 AR,
 AbstractSwapeeMeAccountControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]