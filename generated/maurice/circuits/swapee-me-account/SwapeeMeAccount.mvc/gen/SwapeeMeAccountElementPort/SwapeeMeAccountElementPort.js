import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMeAccountElementPort}
 */
function __SwapeeMeAccountElementPort() {}
__SwapeeMeAccountElementPort.prototype = /** @type {!_SwapeeMeAccountElementPort} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMeAccountElementPort} */ function SwapeeMeAccountElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    tokenLaOpts: {},
    userpicImOpts: {},
    noUserBlockOpts: {},
    userBlockOpts: {},
    usernameLaOpts: {},
    signOutBuOpts: {},
    swapeeMeOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountElementPort}
 */
class _SwapeeMeAccountElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountElementPort} ‎
 */
class SwapeeMeAccountElementPort extends newAbstract(
 _SwapeeMeAccountElementPort,623632859814,SwapeeMeAccountElementPortConstructor,{
  asISwapeeMeAccountElementPort:1,
  superSwapeeMeAccountElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElementPort} */
SwapeeMeAccountElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElementPort} */
function SwapeeMeAccountElementPortClass(){}

export default SwapeeMeAccountElementPort


SwapeeMeAccountElementPort[$implementations]=[
 __SwapeeMeAccountElementPort,
 SwapeeMeAccountElementPortClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'token-la-opts':undefined,
    'userpic-im-opts':undefined,
    'no-user-block-opts':undefined,
    'user-block-opts':undefined,
    'username-la-opts':undefined,
    'sign-out-bu-opts':undefined,
    'swapee-me-opts':undefined,
   })
  },
 }),
]