import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountComputer}
 */
function __AbstractSwapeeMeAccountComputer() {}
__AbstractSwapeeMeAccountComputer.prototype = /** @type {!_AbstractSwapeeMeAccountComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountComputer}
 */
class _AbstractSwapeeMeAccountComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountComputer} ‎
 */
export class AbstractSwapeeMeAccountComputer extends newAbstract(
 _AbstractSwapeeMeAccountComputer,62363285981,null,{
  asISwapeeMeAccountComputer:1,
  superSwapeeMeAccountComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountComputer} */
AbstractSwapeeMeAccountComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountComputer} */
function AbstractSwapeeMeAccountComputerClass(){}


AbstractSwapeeMeAccountComputer[$implementations]=[
 __AbstractSwapeeMeAccountComputer,
 Adapter,
]


export default AbstractSwapeeMeAccountComputer