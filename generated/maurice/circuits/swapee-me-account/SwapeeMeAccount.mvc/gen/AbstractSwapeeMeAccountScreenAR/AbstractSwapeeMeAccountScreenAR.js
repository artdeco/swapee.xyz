import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountScreenAR}
 */
function __AbstractSwapeeMeAccountScreenAR() {}
__AbstractSwapeeMeAccountScreenAR.prototype = /** @type {!_AbstractSwapeeMeAccountScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR}
 */
class _AbstractSwapeeMeAccountScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeMeAccountScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR} ‎
 */
class AbstractSwapeeMeAccountScreenAR extends newAbstract(
 _AbstractSwapeeMeAccountScreenAR,623632859827,null,{
  asISwapeeMeAccountScreenAR:1,
  superSwapeeMeAccountScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR} */
AbstractSwapeeMeAccountScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR} */
function AbstractSwapeeMeAccountScreenARClass(){}

export default AbstractSwapeeMeAccountScreenAR


AbstractSwapeeMeAccountScreenAR[$implementations]=[
 __AbstractSwapeeMeAccountScreenAR,
 AR,
 AbstractSwapeeMeAccountScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractSwapeeMeAccountScreenAR}