import AbstractSwapeeMeAccountProcessor from '../AbstractSwapeeMeAccountProcessor'
import {SwapeeMeAccountCore} from '../SwapeeMeAccountCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractSwapeeMeAccountComputer} from '../AbstractSwapeeMeAccountComputer'
import {AbstractSwapeeMeAccountController} from '../AbstractSwapeeMeAccountController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccount}
 */
function __AbstractSwapeeMeAccount() {}
__AbstractSwapeeMeAccount.prototype = /** @type {!_AbstractSwapeeMeAccount} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccount}
 */
class _AbstractSwapeeMeAccount { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccount} ‎
 */
class AbstractSwapeeMeAccount extends newAbstract(
 _AbstractSwapeeMeAccount,62363285989,null,{
  asISwapeeMeAccount:1,
  superSwapeeMeAccount:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccount} */
AbstractSwapeeMeAccount.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccount} */
function AbstractSwapeeMeAccountClass(){}

export default AbstractSwapeeMeAccount


AbstractSwapeeMeAccount[$implementations]=[
 __AbstractSwapeeMeAccount,
 SwapeeMeAccountCore,
 AbstractSwapeeMeAccountProcessor,
 IntegratedComponent,
 AbstractSwapeeMeAccountComputer,
 AbstractSwapeeMeAccountController,
]


export {AbstractSwapeeMeAccount}