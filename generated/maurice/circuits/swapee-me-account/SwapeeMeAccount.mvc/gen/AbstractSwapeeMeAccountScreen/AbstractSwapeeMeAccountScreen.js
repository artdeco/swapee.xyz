import SwapeeMeAccountClassesPQs from '../../pqs/SwapeeMeAccountClassesPQs'
import AbstractSwapeeMeAccountScreenAR from '../AbstractSwapeeMeAccountScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {SwapeeMeAccountInputsPQs} from '../../pqs/SwapeeMeAccountInputsPQs'
import {SwapeeMeAccountQueriesPQs} from '../../pqs/SwapeeMeAccountQueriesPQs'
import {SwapeeMeAccountMemoryQPs} from '../../pqs/SwapeeMeAccountMemoryQPs'
import {SwapeeMeAccountVdusPQs} from '../../pqs/SwapeeMeAccountVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountScreen}
 */
function __AbstractSwapeeMeAccountScreen() {}
__AbstractSwapeeMeAccountScreen.prototype = /** @type {!_AbstractSwapeeMeAccountScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountScreen}
 */
class _AbstractSwapeeMeAccountScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountScreen} ‎
 */
class AbstractSwapeeMeAccountScreen extends newAbstract(
 _AbstractSwapeeMeAccountScreen,623632859825,null,{
  asISwapeeMeAccountScreen:1,
  superSwapeeMeAccountScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountScreen} */
AbstractSwapeeMeAccountScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountScreen} */
function AbstractSwapeeMeAccountScreenClass(){}

export default AbstractSwapeeMeAccountScreen


AbstractSwapeeMeAccountScreen[$implementations]=[
 __AbstractSwapeeMeAccountScreen,
 AbstractSwapeeMeAccountScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountScreen}*/({
  inputsPQs:SwapeeMeAccountInputsPQs,
  classesPQs:SwapeeMeAccountClassesPQs,
  queriesPQs:SwapeeMeAccountQueriesPQs,
  memoryQPs:SwapeeMeAccountMemoryQPs,
 }),
 Screen,
 AbstractSwapeeMeAccountScreenAR,
 AbstractSwapeeMeAccountScreenClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountScreen}*/({
  vdusPQs:SwapeeMeAccountVdusPQs,
 }),
]