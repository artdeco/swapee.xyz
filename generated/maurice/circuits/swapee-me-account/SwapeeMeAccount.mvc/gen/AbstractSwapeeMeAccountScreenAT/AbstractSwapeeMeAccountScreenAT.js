import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractSwapeeMeAccountScreenAT}
 */
function __AbstractSwapeeMeAccountScreenAT() {}
__AbstractSwapeeMeAccountScreenAT.prototype = /** @type {!_AbstractSwapeeMeAccountScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT}
 */
class _AbstractSwapeeMeAccountScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeMeAccountScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT} ‎
 */
class AbstractSwapeeMeAccountScreenAT extends newAbstract(
 _AbstractSwapeeMeAccountScreenAT,623632859828,null,{
  asISwapeeMeAccountScreenAT:1,
  superSwapeeMeAccountScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT} */
AbstractSwapeeMeAccountScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT} */
function AbstractSwapeeMeAccountScreenATClass(){}

export default AbstractSwapeeMeAccountScreenAT


AbstractSwapeeMeAccountScreenAT[$implementations]=[
 __AbstractSwapeeMeAccountScreenAT,
 UartUniversal,
]