import {mountPins} from '@type.engineering/seers'
import {SwapeeMeAccountMemoryPQs} from '../../pqs/SwapeeMeAccountMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMeAccountCore}
 */
function __SwapeeMeAccountCore() {}
__SwapeeMeAccountCore.prototype = /** @type {!_SwapeeMeAccountCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountCore}
 */
class _SwapeeMeAccountCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountCore} ‎
 */
class SwapeeMeAccountCore extends newAbstract(
 _SwapeeMeAccountCore,62363285987,null,{
  asISwapeeMeAccountCore:1,
  superSwapeeMeAccountCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountCore} */
SwapeeMeAccountCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountCore} */
function SwapeeMeAccountCoreClass(){}

export default SwapeeMeAccountCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_SwapeeMeAccountOuterCore}
 */
function __SwapeeMeAccountOuterCore() {}
__SwapeeMeAccountOuterCore.prototype = /** @type {!_SwapeeMeAccountOuterCore} */ ({ })
/** @this {xyz.swapee.wc.SwapeeMeAccountOuterCore} */
export function SwapeeMeAccountOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model}*/
  this.model={
    core: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore}
 */
class _SwapeeMeAccountOuterCore { }
/**
 * The _ISwapeeMeAccount_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore} ‎
 */
export class SwapeeMeAccountOuterCore extends newAbstract(
 _SwapeeMeAccountOuterCore,62363285983,SwapeeMeAccountOuterCoreConstructor,{
  asISwapeeMeAccountOuterCore:1,
  superSwapeeMeAccountOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore} */
SwapeeMeAccountOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore} */
function SwapeeMeAccountOuterCoreClass(){}


SwapeeMeAccountOuterCore[$implementations]=[
 __SwapeeMeAccountOuterCore,
 SwapeeMeAccountOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountOuterCore}*/({
  constructor(){
   mountPins(this.model,SwapeeMeAccountMemoryPQs)

  },
 }),
]

SwapeeMeAccountCore[$implementations]=[
 SwapeeMeAccountCoreClass.prototype=/**@type {!xyz.swapee.wc.ISwapeeMeAccountCore}*/({
  resetCore(){
   this.resetSwapeeMeAccountCore()
  },
  resetSwapeeMeAccountCore(){
   SwapeeMeAccountOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.SwapeeMeAccountOuterCore}*/(
     /**@type {!xyz.swapee.wc.ISwapeeMeAccountOuterCore}*/(this)),
   )
  },
 }),
 __SwapeeMeAccountCore,
 SwapeeMeAccountOuterCore,
]

export {SwapeeMeAccountCore}