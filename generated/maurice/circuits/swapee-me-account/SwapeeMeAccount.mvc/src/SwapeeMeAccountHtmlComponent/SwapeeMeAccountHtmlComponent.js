import SwapeeMeAccountHtmlController from '../SwapeeMeAccountHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractSwapeeMeAccountHtmlComponent} from '../../gen/AbstractSwapeeMeAccountHtmlComponent'

/** @extends {xyz.swapee.wc.SwapeeMeAccountHtmlComponent} */
export default class extends AbstractSwapeeMeAccountHtmlComponent.implements(
 SwapeeMeAccountHtmlController,
 IntegratedComponentInitialiser,
){}