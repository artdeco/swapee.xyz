import solder from './methods/solder'
import server from './methods/server'
import buildSwapeeMe from './methods/build-swapee-me'
import render from './methods/render'
import SwapeeMeAccountServerController from '../SwapeeMeAccountServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractSwapeeMeAccountElement from '../../gen/AbstractSwapeeMeAccountElement'

/** @extends {xyz.swapee.wc.SwapeeMeAccountElement} */
export default class SwapeeMeAccountElement extends AbstractSwapeeMeAccountElement.implements(
 SwapeeMeAccountServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ISwapeeMeAccountElement} */ ({
  solder:solder,
  server:server,
  buildSwapeeMe:buildSwapeeMe,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ISwapeeMeAccountElement}*/({
   classesMap: true,
   rootSelector:     `.SwapeeMeAccount`,
   stylesheet:       'html/styles/SwapeeMeAccount.css',
   blockName:        'html/SwapeeMeAccountBlock.html',
  }),
){}

// thank you for using web circuits
