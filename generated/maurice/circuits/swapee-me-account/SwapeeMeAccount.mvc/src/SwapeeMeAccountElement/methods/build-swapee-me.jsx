/** @type {xyz.swapee.wc.ISwapeeMeAccountElement._buildSwapeeMe} */
export default function buildSwapeeMe({
 token:token,displayName:displayName,userpic:userpic,username:username,
},{signOut:signOut,signIn:signIn}) {
 return (<div $id="SwapeeMeAccount" Shown={token!==undefined}>
  <span $id="TokenLa">{token}</span>
  <span $id="UsernameLa">{displayName}</span>
  {/* $present={token} */}
  <div $id="UserBlock" $reveal={token||false}  />

  <div $id="NoUserBlock" $conceal={token||false} onClick={signIn} />

  <img $id="UserpicIm" src={userpic} alt={username} />
  <button $id="SignOutBu" onClick={signOut}/>
 </div>)
}