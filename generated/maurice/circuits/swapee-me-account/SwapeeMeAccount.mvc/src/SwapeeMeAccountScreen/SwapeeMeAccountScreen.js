import AbstractSwapeeMeAccountControllerAT from '../../gen/AbstractSwapeeMeAccountControllerAT'
import SwapeeMeAccountDisplay from '../SwapeeMeAccountDisplay'
import AbstractSwapeeMeAccountScreen from '../../gen/AbstractSwapeeMeAccountScreen'

/** @extends {xyz.swapee.wc.SwapeeMeAccountScreen} */
export default class extends AbstractSwapeeMeAccountScreen.implements(
 AbstractSwapeeMeAccountControllerAT,
 SwapeeMeAccountDisplay,
 /**@type {!xyz.swapee.wc.ISwapeeMeAccountScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.ISwapeeMeAccountScreen} */ ({
  __$id:6236328598,
 }),
){}