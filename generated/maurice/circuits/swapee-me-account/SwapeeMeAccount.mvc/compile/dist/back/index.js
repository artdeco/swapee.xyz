/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccount` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccount}
 */
class AbstractSwapeeMeAccount extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeMeAccount_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeMeAccountPort}
 */
class SwapeeMeAccountPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountController}
 */
class AbstractSwapeeMeAccountController extends (class {/* lazy-loaded */}) {}
/**
 * The _ISwapeeMeAccount_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.SwapeeMeAccountHtmlComponent}
 */
class SwapeeMeAccountHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeMeAccountBuffer}
 */
class SwapeeMeAccountBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountComputer}
 */
class AbstractSwapeeMeAccountComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.SwapeeMeAccountController}
 */
class SwapeeMeAccountController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeMeAccount = AbstractSwapeeMeAccount
module.exports.SwapeeMeAccountPort = SwapeeMeAccountPort
module.exports.AbstractSwapeeMeAccountController = AbstractSwapeeMeAccountController
module.exports.SwapeeMeAccountHtmlComponent = SwapeeMeAccountHtmlComponent
module.exports.SwapeeMeAccountBuffer = SwapeeMeAccountBuffer
module.exports.AbstractSwapeeMeAccountComputer = AbstractSwapeeMeAccountComputer
module.exports.SwapeeMeAccountController = SwapeeMeAccountController