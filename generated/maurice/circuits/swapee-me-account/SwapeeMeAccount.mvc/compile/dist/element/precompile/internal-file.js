/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function e(a,b,f,g){return c["372700389812"](a,b,f,g,!1,void 0)};function h(){}h.prototype={};class k{}class m extends e(k,62363285988,null,{H:1,Z:2}){}m[d]=[h];


const n=function(){return require(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];const p={core:"a74ad"};function q(){}q.prototype={};class r{}class t extends e(r,62363285987,null,{A:1,U:2}){}function v(){}v.prototype={};function x(){this.model={core:""}}class y{}class z extends e(y,62363285983,x,{F:1,X:2}){}z[d]=[v,{constructor(){n(this.model,p)}}];t[d]=[{},q,z];

const A=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const B=A.IntegratedComponentInitialiser,C=A.IntegratedComponent,aa=A["95173443851"],ba=A["95173443852"];
const D=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=D["615055805212"],da=D["615055805218"],ea=D["615055805235"];function E(){}E.prototype={};class fa{}class F extends e(fa,62363285981,null,{u:1,R:2}){}F[d]=[E,da];const G={regulate:ca({core:String})};
const H=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ha=H.IntegratedController,ia=H.Parametric;const I={...p};function J(){}J.prototype={};function K(){const a={model:null};x.call(a);this.inputs=a.model}class ja{}class L extends e(ja,62363285985,K,{G:1,Y:2}){}function M(){}L[d]=[M.prototype={resetPort(){K.call(this)}},J,ia,M.prototype={constructor(){n(this.inputs,I)}}];function N(){}N.prototype={};class ka{}class O extends e(ka,623632859820,null,{v:1,T:2}){}O[d]=[{resetPort(){this.port.resetPort()}},N,G,ha,{get Port(){return L}}];function P(){}P.prototype={};class la{}class Q extends e(la,62363285989,null,{s:1,P:2}){}Q[d]=[P,t,m,C,F,O];function ma(){return{}};const na=require(eval('"@type.engineering/web-computing"')).h;function oa(){return na("div",{$id:"SwapeeMeAccount"})};const R=require(eval('"@type.engineering/web-computing"')).h;function pa({m:a,displayName:b,o:f,username:g},{K:l,J:u}){return R("div",{$id:"SwapeeMeAccount",Shown:void 0!==a},R("span",{$id:"TokenLa"},a),R("span",{$id:"UsernameLa"},b),R("div",{$id:"UserBlock",$reveal:a||!1}),R("div",{$id:"NoUserBlock",$conceal:a||!1,onClick:u}),R("img",{$id:"UserpicIm",src:f,alt:g}),R("button",{$id:"SignOutBu",onClick:l}))};const qa=require(eval('"@type.engineering/web-computing"')).h;function ra(){return qa("div",{$id:"SwapeeMeAccount"})};var S=class extends O.implements(){};require("https");require("http");const sa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}sa("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const ta=T.ElementBase,ua=T.HTMLBlocker;const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function va(){this.inputs={noSolder:!1,aa:{},ea:{},I:{},ba:{},da:{},M:{},$:{}}}class wa{}class W extends e(wa,623632859814,va,{D:1,W:2}){}W[d]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"token-la-opts":void 0,"userpic-im-opts":void 0,"no-user-block-opts":void 0,"user-block-opts":void 0,"username-la-opts":void 0,"sign-out-bu-opts":void 0,"swapee-me-opts":void 0})}}];function X(){}X.prototype={};class xa{}class Y extends e(xa,623632859813,null,{j:1,V:2}){}function Z(){}
Y[d]=[X,ta,Z.prototype={calibrate:function({":no-solder":a,":core":b}){const {attributes:{"no-solder":f,core:g}}=this;return{...(void 0===f?{"no-solder":a}:{}),...(void 0===g?{core:b}:{})}}},Z.prototype={calibrate:({"no-solder":a,core:b})=>({noSolder:a,core:b})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},ea,Z.prototype={constructor(){Object.assign(this,{land:{i:null}})},render:function(){const {asILanded:{land:{i:a}},j:{l:b}}=this;if(a){var {"94a08":f,"4498e":g,"08906":l,"14c4b":u}=a.model,
w=b({m:f,displayName:g,o:l,username:u},{});w.attributes.$id=this.rootId;w.nodeName="div";return w}}},Z.prototype={render:function(){return U("div",{$id:"SwapeeMeAccount"},U("vdu",{$id:"TokenLa"}),U("vdu",{$id:"UserpicIm"}),U("vdu",{$id:"NoUserBlock"}),U("vdu",{$id:"UserBlock"}),U("vdu",{$id:"UsernameLa"}),U("vdu",{$id:"SignOutBu"}))}},Z.prototype={classes:{Shown:"a1921"},inputsPQs:I,queriesPQs:{g:"50b0c"},vdus:{TokenLa:"ad091",UserpicIm:"ad092",NoUserBlock:"ad093",UserBlock:"ad094",UsernameLa:"ad095",
SignOutBu:"ad096",SwapeeMe:"ad097"}},C,ba,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder core query:swapee-me no-solder :no-solder :core fe646 e12b1 fe8e1 2f0be 75ee3 3b1f1 cff70 4ccfe a74ad children".split(" "))})},get Port(){return W},calibrate:function({"query:swapee-me":a}){const b={};a&&(b.g=a);return b}},Z.prototype={constructor(){this.land={i:null}}},Z.prototype={calibrate:async function({g:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:f},asIElement:{fqn:g}}=
this,l=await b(a,!0);if(!l)return console.warn("\u2757\ufe0f swapeeMeSel %s must be present on the page for %s to work",a,g),{};f.i=l}},Z.prototype={solder:(a,{g:b})=>({g:b})}];Y[d]=[Q,{rootId:"SwapeeMeAccount",__$id:6236328598,fqn:"xyz.swapee.wc.ISwapeeMeAccount",maurice_element_v3:!0}];class ya extends Y.implements(S,aa,ua,B,{solder:ma,server:oa,l:pa,render:ra},{classesMap:!0,rootSelector:".SwapeeMeAccount",stylesheet:"html/styles/SwapeeMeAccount.css",blockName:"html/SwapeeMeAccountBlock.html"}){};module.exports["62363285980"]=Q;module.exports["62363285981"]=Q;module.exports["62363285983"]=L;module.exports["62363285984"]=O;module.exports["62363285988"]=ya;module.exports["623632859811"]=G;module.exports["623632859830"]=F;module.exports["623632859861"]=S;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['6236328598']=module.exports