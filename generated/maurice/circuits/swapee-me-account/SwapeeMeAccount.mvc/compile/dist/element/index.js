/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccount` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccount}
 */
class AbstractSwapeeMeAccount extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ISwapeeMeAccount_, providing input
 * pins.
 * @extends {xyz.swapee.wc.SwapeeMeAccountPort}
 */
class SwapeeMeAccountPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountController` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountController}
 */
class AbstractSwapeeMeAccountController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ISwapeeMeAccount_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.SwapeeMeAccountElement}
 */
class SwapeeMeAccountElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.SwapeeMeAccountBuffer}
 */
class SwapeeMeAccountBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountComputer` interface.
 * @extends {xyz.swapee.wc.AbstractSwapeeMeAccountComputer}
 */
class AbstractSwapeeMeAccountComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.SwapeeMeAccountController}
 */
class SwapeeMeAccountController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractSwapeeMeAccount = AbstractSwapeeMeAccount
module.exports.SwapeeMeAccountPort = SwapeeMeAccountPort
module.exports.AbstractSwapeeMeAccountController = AbstractSwapeeMeAccountController
module.exports.SwapeeMeAccountElement = SwapeeMeAccountElement
module.exports.SwapeeMeAccountBuffer = SwapeeMeAccountBuffer
module.exports.AbstractSwapeeMeAccountComputer = AbstractSwapeeMeAccountComputer
module.exports.SwapeeMeAccountController = SwapeeMeAccountController

Object.defineProperties(module.exports, {
 'AbstractSwapeeMeAccount': {get: () => require('./precompile/internal')[62363285981]},
 [62363285981]: {get: () => module.exports['AbstractSwapeeMeAccount']},
 'SwapeeMeAccountPort': {get: () => require('./precompile/internal')[62363285983]},
 [62363285983]: {get: () => module.exports['SwapeeMeAccountPort']},
 'AbstractSwapeeMeAccountController': {get: () => require('./precompile/internal')[62363285984]},
 [62363285984]: {get: () => module.exports['AbstractSwapeeMeAccountController']},
 'SwapeeMeAccountElement': {get: () => require('./precompile/internal')[62363285988]},
 [62363285988]: {get: () => module.exports['SwapeeMeAccountElement']},
 'SwapeeMeAccountBuffer': {get: () => require('./precompile/internal')[623632859811]},
 [623632859811]: {get: () => module.exports['SwapeeMeAccountBuffer']},
 'AbstractSwapeeMeAccountComputer': {get: () => require('./precompile/internal')[623632859830]},
 [623632859830]: {get: () => module.exports['AbstractSwapeeMeAccountComputer']},
 'SwapeeMeAccountController': {get: () => require('./precompile/internal')[623632859861]},
 [623632859861]: {get: () => module.exports['SwapeeMeAccountController']},
})