import Module from './element'

/**@extends {xyz.swapee.wc.AbstractSwapeeMeAccount}*/
export class AbstractSwapeeMeAccount extends Module['62363285981'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccount} */
AbstractSwapeeMeAccount.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeMeAccountPort} */
export const SwapeeMeAccountPort=Module['62363285983']
/**@extends {xyz.swapee.wc.AbstractSwapeeMeAccountController}*/
export class AbstractSwapeeMeAccountController extends Module['62363285984'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountController} */
AbstractSwapeeMeAccountController.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeMeAccountElement} */
export const SwapeeMeAccountElement=Module['62363285988']
/** @type {typeof xyz.swapee.wc.SwapeeMeAccountBuffer} */
export const SwapeeMeAccountBuffer=Module['623632859811']
/**@extends {xyz.swapee.wc.AbstractSwapeeMeAccountComputer}*/
export class AbstractSwapeeMeAccountComputer extends Module['623632859830'] {}
/** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountComputer} */
AbstractSwapeeMeAccountComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.SwapeeMeAccountController} */
export const SwapeeMeAccountController=Module['623632859861']