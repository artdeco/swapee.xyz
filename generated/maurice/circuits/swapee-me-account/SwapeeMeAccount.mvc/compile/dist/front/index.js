/**
 * Display for presenting information from the _ISwapeeMeAccount_.
 * @extends {xyz.swapee.wc.SwapeeMeAccountDisplay}
 */
class SwapeeMeAccountDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.SwapeeMeAccountScreen}
 */
class SwapeeMeAccountScreen extends (class {/* lazy-loaded */}) {}

module.exports.SwapeeMeAccountDisplay = SwapeeMeAccountDisplay
module.exports.SwapeeMeAccountScreen = SwapeeMeAccountScreen