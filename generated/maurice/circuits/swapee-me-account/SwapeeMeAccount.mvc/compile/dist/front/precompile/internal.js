var f="depack-remove-start",g=f;try{if(f)throw Error();Object.setPrototypeOf(g,g);g.K=new WeakMap;g.map=new Map;g.set=new Set;Object.getOwnPropertySymbols({});Object.getOwnPropertyDescriptors({});f.includes("");[].keys();Object.values({});Object.assign({},{})}catch(a){}f="depack-remove-end";/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
Please make sure you have a Commercial License to use this library.
*/
const k=h["37270038985"],l=h["372700389810"],m=h["372700389811"];function t(a,b,c,d){return h["372700389812"](a,b,c,d,!1,void 0)};/*

@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package:
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const u=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();/*

@LICENSE @webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const v=u["61893096584"],x=u["61893096586"],y=u["618930965811"],z=u["618930965812"],A=u["618930965815"];function B(){}B.prototype={};function C(){this.o=this.h=this.l=this.j=this.g=this.m=this.i=null}class D{}class E extends t(D,623632859817,C,{A:1,G:2}){}
E[m]=[B,v,function(){}.prototype={constructor(){k(this,()=>{const {queries:{J:a}}=this;this.scan({s:a})})},scan:function({s:a}){const {element:b,u:{vdusPQs:{i:c,m:d,g:n,j:p,l:q,h:O}},queries:{s:w}}=this,e=A(b);let r;a?r=b.closest(a):r=document;Object.assign(this,{i:e[c],m:e[d],g:e[n],j:e[p],l:e[q],h:e[O],o:w?r.querySelector(w):void 0})}},{[l]:{i:1,m:1,g:1,j:1,l:1,h:1,o:1},initializer({i:a,m:b,g:c,j:d,l:n,h:p,o:q}){void 0!==a&&(this.i=a);void 0!==b&&(this.m=b);void 0!==c&&(this.g=c);void 0!==d&&(this.j=
d);void 0!==n&&(this.l=n);void 0!==p&&(this.h=p);void 0!==q&&(this.o=q)}}];var F=class extends E.implements(){};function G(){}G.prototype={};class H{}class I extends t(H,623632859824,null,{v:1,F:2}){}I[m]=[G,y,function(){}.prototype={}];function J(){}J.prototype={};class K{}class L extends t(K,623632859827,null,{C:1,I:2}){}L[m]=[J,z,function(){}.prototype={allocator(){this.methods={}}}];const M={D:"a74ad"};const N={...M};const P=Object.keys(M).reduce((a,b)=>{a[M[b]]=b;return a},{});function Q(){}Q.prototype={};class R{}class S extends t(R,623632859825,null,{u:1,H:2}){}function T(){}S[m]=[Q,T.prototype={inputsPQs:N,queriesPQs:{s:"50b0c"},memoryQPs:P},x,L,T.prototype={vdusPQs:{i:"ad091",m:"ad092",g:"ad093",j:"ad094",l:"ad095",h:"ad096",o:"ad097"}}];var U=class extends S.implements(I,F,{get queries(){return this.settings}},{__$id:6236328598}){};module.exports["623632859841"]=F;module.exports["623632859871"]=U;

//# sourceMappingURL=internal.js.map