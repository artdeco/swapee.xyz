import { SwapeeMeAccountDisplay, SwapeeMeAccountScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.SwapeeMeAccountDisplay} */
export { SwapeeMeAccountDisplay }
/** @lazy @api {xyz.swapee.wc.SwapeeMeAccountScreen} */
export { SwapeeMeAccountScreen }