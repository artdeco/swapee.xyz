/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ISwapeeMeAccountComputer={}
xyz.swapee.wc.ISwapeeMeAccountComputer.compute={}
xyz.swapee.wc.ISwapeeMeAccountOuterCore={}
xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model={}
xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model.Core={}
xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel={}
xyz.swapee.wc.ISwapeeMeAccountPort={}
xyz.swapee.wc.ISwapeeMeAccountPort.Inputs={}
xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs={}
xyz.swapee.wc.ISwapeeMeAccountCore={}
xyz.swapee.wc.ISwapeeMeAccountCore.Model={}
xyz.swapee.wc.ISwapeeMeAccountPortInterface={}
xyz.swapee.wc.ISwapeeMeAccountProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ISwapeeMeAccountController={}
xyz.swapee.wc.front.ISwapeeMeAccountControllerAT={}
xyz.swapee.wc.front.ISwapeeMeAccountScreenAR={}
xyz.swapee.wc.ISwapeeMeAccount={}
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil={}
xyz.swapee.wc.ISwapeeMeAccountHtmlComponent={}
xyz.swapee.wc.ISwapeeMeAccountElement={}
xyz.swapee.wc.ISwapeeMeAccountElement.build={}
xyz.swapee.wc.ISwapeeMeAccountElement.short={}
xyz.swapee.wc.ISwapeeMeAccountElementPort={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts={}
xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs={}
xyz.swapee.wc.ISwapeeMeAccountDesigner={}
xyz.swapee.wc.ISwapeeMeAccountDesigner.communicator={}
xyz.swapee.wc.ISwapeeMeAccountDesigner.relay={}
xyz.swapee.wc.ISwapeeMeAccountDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ISwapeeMeAccountDisplay={}
xyz.swapee.wc.back.ISwapeeMeAccountController={}
xyz.swapee.wc.back.ISwapeeMeAccountControllerAR={}
xyz.swapee.wc.back.ISwapeeMeAccountScreen={}
xyz.swapee.wc.back.ISwapeeMeAccountScreenAT={}
xyz.swapee.wc.ISwapeeMeAccountController={}
xyz.swapee.wc.ISwapeeMeAccountScreen={}
xyz.swapee.wc.ISwapeeMeAccountGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/02-ISwapeeMeAccountComputer.xml}  666eb5c5e687072afcc7ce32a6c1199d */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountComputer)} xyz.swapee.wc.AbstractSwapeeMeAccountComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountComputer} xyz.swapee.wc.SwapeeMeAccountComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountComputer` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountComputer
 */
xyz.swapee.wc.AbstractSwapeeMeAccountComputer = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountComputer.constructor&xyz.swapee.wc.SwapeeMeAccountComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountComputer.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountComputer.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountComputer}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountComputer} xyz.swapee.wc.SwapeeMeAccountComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.SwapeeMeAccountMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeMeAccountLand>)} xyz.swapee.wc.ISwapeeMeAccountComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ISwapeeMeAccountComputer
 */
xyz.swapee.wc.ISwapeeMeAccountComputer = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountComputer.compute} */
xyz.swapee.wc.ISwapeeMeAccountComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese>)} xyz.swapee.wc.SwapeeMeAccountComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountComputer} xyz.swapee.wc.ISwapeeMeAccountComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeMeAccountComputer_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountComputer
 * @implements {xyz.swapee.wc.ISwapeeMeAccountComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountComputer = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountComputer.constructor&xyz.swapee.wc.ISwapeeMeAccountComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountComputer}
 */
xyz.swapee.wc.SwapeeMeAccountComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountComputer} */
xyz.swapee.wc.RecordISwapeeMeAccountComputer

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountComputer} xyz.swapee.wc.BoundISwapeeMeAccountComputer */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountComputer} xyz.swapee.wc.BoundSwapeeMeAccountComputer */

/**
 * Contains getters to cast the _ISwapeeMeAccountComputer_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountComputerCaster
 */
xyz.swapee.wc.ISwapeeMeAccountComputerCaster = class { }
/**
 * Cast the _ISwapeeMeAccountComputer_ instance into the _BoundISwapeeMeAccountComputer_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountComputer}
 */
xyz.swapee.wc.ISwapeeMeAccountComputerCaster.prototype.asISwapeeMeAccountComputer
/**
 * Access the _SwapeeMeAccountComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountComputer}
 */
xyz.swapee.wc.ISwapeeMeAccountComputerCaster.prototype.superSwapeeMeAccountComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.SwapeeMeAccountMemory, land: !xyz.swapee.wc.ISwapeeMeAccountComputer.compute.Land) => void} xyz.swapee.wc.ISwapeeMeAccountComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountComputer.__compute<!xyz.swapee.wc.ISwapeeMeAccountComputer>} xyz.swapee.wc.ISwapeeMeAccountComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.SwapeeMeAccountMemory} mem The memory.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountComputer.compute.Land} land The land.
 * - `SwapeeMe` _!xyz.swapee.wc.SwapeeMeMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeAccountComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeAccountComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/03-ISwapeeMeAccountOuterCore.xml}  7f8ed4551808a41e778698154115358b */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeMeAccountOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountOuterCore)} xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountOuterCore} xyz.swapee.wc.SwapeeMeAccountOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore
 */
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.constructor&xyz.swapee.wc.SwapeeMeAccountOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountOuterCore|typeof xyz.swapee.wc.SwapeeMeAccountOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountOuterCore|typeof xyz.swapee.wc.SwapeeMeAccountOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountOuterCore|typeof xyz.swapee.wc.SwapeeMeAccountOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountOuterCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountOuterCoreCaster)} xyz.swapee.wc.ISwapeeMeAccountOuterCore.constructor */
/**
 * The _ISwapeeMeAccount_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ISwapeeMeAccountOuterCore
 */
xyz.swapee.wc.ISwapeeMeAccountOuterCore = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeAccountOuterCore.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountOuterCore

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountOuterCore.Initialese>)} xyz.swapee.wc.SwapeeMeAccountOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountOuterCore} xyz.swapee.wc.ISwapeeMeAccountOuterCore.typeof */
/**
 * A concrete class of _ISwapeeMeAccountOuterCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountOuterCore
 * @implements {xyz.swapee.wc.ISwapeeMeAccountOuterCore} The _ISwapeeMeAccount_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountOuterCore = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountOuterCore.constructor&xyz.swapee.wc.ISwapeeMeAccountOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeAccountOuterCore.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountOuterCore}
 */
xyz.swapee.wc.SwapeeMeAccountOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountOuterCore.
 * @interface xyz.swapee.wc.ISwapeeMeAccountOuterCoreFields
 */
xyz.swapee.wc.ISwapeeMeAccountOuterCoreFields = class { }
/**
 * The _ISwapeeMeAccount_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ISwapeeMeAccountOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore} */
xyz.swapee.wc.RecordISwapeeMeAccountOuterCore

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore} xyz.swapee.wc.BoundISwapeeMeAccountOuterCore */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountOuterCore} xyz.swapee.wc.BoundSwapeeMeAccountOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model.Core.core

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model.Core} xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model The _ISwapeeMeAccount_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.Core} xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel The _ISwapeeMeAccount_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ISwapeeMeAccountOuterCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountOuterCoreCaster
 */
xyz.swapee.wc.ISwapeeMeAccountOuterCoreCaster = class { }
/**
 * Cast the _ISwapeeMeAccountOuterCore_ instance into the _BoundISwapeeMeAccountOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountOuterCore}
 */
xyz.swapee.wc.ISwapeeMeAccountOuterCoreCaster.prototype.asISwapeeMeAccountOuterCore
/**
 * Access the _SwapeeMeAccountOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountOuterCore}
 */
xyz.swapee.wc.ISwapeeMeAccountOuterCoreCaster.prototype.superSwapeeMeAccountOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model.Core The core property (optional overlay).
 * @prop {string} [core=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model.Core_Safe The core property (required overlay).
 * @prop {string} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.Core The core property (optional overlay).
 * @prop {*} [core=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.Core_Safe The core property (required overlay).
 * @prop {*} core The core property.
 */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.Core} xyz.swapee.wc.ISwapeeMeAccountPort.Inputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ISwapeeMeAccountPort.Inputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.Core} xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model.Core} xyz.swapee.wc.ISwapeeMeAccountCore.Model.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model.Core_Safe} xyz.swapee.wc.ISwapeeMeAccountCore.Model.Core_Safe The core property (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/04-ISwapeeMeAccountPort.xml}  217aed30d8b8a973388f68e912939d83 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeMeAccountPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountPort)} xyz.swapee.wc.AbstractSwapeeMeAccountPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountPort} xyz.swapee.wc.SwapeeMeAccountPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountPort
 */
xyz.swapee.wc.AbstractSwapeeMeAccountPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountPort.constructor&xyz.swapee.wc.SwapeeMeAccountPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountPort|typeof xyz.swapee.wc.SwapeeMeAccountPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountPort|typeof xyz.swapee.wc.SwapeeMeAccountPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountPort|typeof xyz.swapee.wc.SwapeeMeAccountPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountPort.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountPort} xyz.swapee.wc.SwapeeMeAccountPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeMeAccountPort.Inputs>)} xyz.swapee.wc.ISwapeeMeAccountPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ISwapeeMeAccount_, providing input
 * pins.
 * @interface xyz.swapee.wc.ISwapeeMeAccountPort
 */
xyz.swapee.wc.ISwapeeMeAccountPort = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountPort.resetPort} */
xyz.swapee.wc.ISwapeeMeAccountPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountPort.resetSwapeeMeAccountPort} */
xyz.swapee.wc.ISwapeeMeAccountPort.prototype.resetSwapeeMeAccountPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountPort.Initialese>)} xyz.swapee.wc.SwapeeMeAccountPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountPort} xyz.swapee.wc.ISwapeeMeAccountPort.typeof */
/**
 * A concrete class of _ISwapeeMeAccountPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountPort
 * @implements {xyz.swapee.wc.ISwapeeMeAccountPort} The port that serves as an interface to the _ISwapeeMeAccount_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountPort = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountPort.constructor&xyz.swapee.wc.ISwapeeMeAccountPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountPort}
 */
xyz.swapee.wc.SwapeeMeAccountPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountPort.
 * @interface xyz.swapee.wc.ISwapeeMeAccountPortFields
 */
xyz.swapee.wc.ISwapeeMeAccountPortFields = class { }
/**
 * The inputs to the _ISwapeeMeAccount_'s controller via its port.
 */
xyz.swapee.wc.ISwapeeMeAccountPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMeAccountPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeMeAccountPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeMeAccountPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountPort} */
xyz.swapee.wc.RecordISwapeeMeAccountPort

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountPort} xyz.swapee.wc.BoundISwapeeMeAccountPort */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountPort} xyz.swapee.wc.BoundSwapeeMeAccountPort */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeMeAccountPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel} xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ISwapeeMeAccount_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeMeAccountPort.Inputs
 */
xyz.swapee.wc.ISwapeeMeAccountPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountPort.Inputs.constructor&xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeAccountPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel)} xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs.constructor */
/**
 * The inputs to the _ISwapeeMeAccount_'s controller via its port.
 * @record xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountPortInterface
 */
xyz.swapee.wc.ISwapeeMeAccountPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.ISwapeeMeAccountPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.ISwapeeMeAccountPortInterface.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountPortInterface

/**
 * A concrete class of _ISwapeeMeAccountPortInterface_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountPortInterface
 * @implements {xyz.swapee.wc.ISwapeeMeAccountPortInterface} The port interface.
 */
xyz.swapee.wc.SwapeeMeAccountPortInterface = class extends xyz.swapee.wc.ISwapeeMeAccountPortInterface { }
xyz.swapee.wc.SwapeeMeAccountPortInterface.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountPortInterface.Props
 * @prop {string} core The core property.
 */

/**
 * Contains getters to cast the _ISwapeeMeAccountPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountPortCaster
 */
xyz.swapee.wc.ISwapeeMeAccountPortCaster = class { }
/**
 * Cast the _ISwapeeMeAccountPort_ instance into the _BoundISwapeeMeAccountPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountPort}
 */
xyz.swapee.wc.ISwapeeMeAccountPortCaster.prototype.asISwapeeMeAccountPort
/**
 * Access the _SwapeeMeAccountPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountPort}
 */
xyz.swapee.wc.ISwapeeMeAccountPortCaster.prototype.superSwapeeMeAccountPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeAccountPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountPort.__resetPort<!xyz.swapee.wc.ISwapeeMeAccountPort>} xyz.swapee.wc.ISwapeeMeAccountPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountPort.resetPort} */
/**
 * Resets the _ISwapeeMeAccount_ port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeAccountPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeAccountPort.__resetSwapeeMeAccountPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountPort.__resetSwapeeMeAccountPort<!xyz.swapee.wc.ISwapeeMeAccountPort>} xyz.swapee.wc.ISwapeeMeAccountPort._resetSwapeeMeAccountPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountPort.resetSwapeeMeAccountPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeAccountPort.resetSwapeeMeAccountPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeAccountPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/09-ISwapeeMeAccountCore.xml}  d82ca1fc33b06d8477f50d465d8972d5 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ISwapeeMeAccountCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountCore)} xyz.swapee.wc.AbstractSwapeeMeAccountCore.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountCore} xyz.swapee.wc.SwapeeMeAccountCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountCore` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountCore
 */
xyz.swapee.wc.AbstractSwapeeMeAccountCore = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountCore.constructor&xyz.swapee.wc.SwapeeMeAccountCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountCore.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountCore.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountCore|typeof xyz.swapee.wc.SwapeeMeAccountCore)|(!xyz.swapee.wc.ISwapeeMeAccountOuterCore|typeof xyz.swapee.wc.SwapeeMeAccountOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountCore|typeof xyz.swapee.wc.SwapeeMeAccountCore)|(!xyz.swapee.wc.ISwapeeMeAccountOuterCore|typeof xyz.swapee.wc.SwapeeMeAccountOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountCore|typeof xyz.swapee.wc.SwapeeMeAccountCore)|(!xyz.swapee.wc.ISwapeeMeAccountOuterCore|typeof xyz.swapee.wc.SwapeeMeAccountOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountCore}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountCoreCaster&xyz.swapee.wc.ISwapeeMeAccountOuterCore)} xyz.swapee.wc.ISwapeeMeAccountCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ISwapeeMeAccountCore
 */
xyz.swapee.wc.ISwapeeMeAccountCore = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeAccountOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeMeAccountCore.resetCore} */
xyz.swapee.wc.ISwapeeMeAccountCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountCore.resetSwapeeMeAccountCore} */
xyz.swapee.wc.ISwapeeMeAccountCore.prototype.resetSwapeeMeAccountCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountCore&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountCore.Initialese>)} xyz.swapee.wc.SwapeeMeAccountCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountCore} xyz.swapee.wc.ISwapeeMeAccountCore.typeof */
/**
 * A concrete class of _ISwapeeMeAccountCore_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountCore
 * @implements {xyz.swapee.wc.ISwapeeMeAccountCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountCore.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountCore = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountCore.constructor&xyz.swapee.wc.ISwapeeMeAccountCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeAccountCore.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountCore}
 */
xyz.swapee.wc.SwapeeMeAccountCore.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountCore.
 * @interface xyz.swapee.wc.ISwapeeMeAccountCoreFields
 */
xyz.swapee.wc.ISwapeeMeAccountCoreFields = class { }
/**
 * The _ISwapeeMeAccount_'s memory.
 */
xyz.swapee.wc.ISwapeeMeAccountCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ISwapeeMeAccountCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ISwapeeMeAccountCoreFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeMeAccountCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountCore} */
xyz.swapee.wc.RecordISwapeeMeAccountCore

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountCore} xyz.swapee.wc.BoundISwapeeMeAccountCore */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountCore} xyz.swapee.wc.BoundSwapeeMeAccountCore */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model} xyz.swapee.wc.ISwapeeMeAccountCore.Model The _ISwapeeMeAccount_'s memory. */

/**
 * Contains getters to cast the _ISwapeeMeAccountCore_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountCoreCaster
 */
xyz.swapee.wc.ISwapeeMeAccountCoreCaster = class { }
/**
 * Cast the _ISwapeeMeAccountCore_ instance into the _BoundISwapeeMeAccountCore_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountCore}
 */
xyz.swapee.wc.ISwapeeMeAccountCoreCaster.prototype.asISwapeeMeAccountCore
/**
 * Access the _SwapeeMeAccountCore_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountCore}
 */
xyz.swapee.wc.ISwapeeMeAccountCoreCaster.prototype.superSwapeeMeAccountCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeAccountCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountCore.__resetCore<!xyz.swapee.wc.ISwapeeMeAccountCore>} xyz.swapee.wc.ISwapeeMeAccountCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountCore.resetCore} */
/**
 * Resets the _ISwapeeMeAccount_ core.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeAccountCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeAccountCore.__resetSwapeeMeAccountCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountCore.__resetSwapeeMeAccountCore<!xyz.swapee.wc.ISwapeeMeAccountCore>} xyz.swapee.wc.ISwapeeMeAccountCore._resetSwapeeMeAccountCore */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountCore.resetSwapeeMeAccountCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeAccountCore.resetSwapeeMeAccountCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeAccountCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/10-ISwapeeMeAccountProcessor.xml}  08a238857c09b66cdcc60a89ad215f4f */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese&xyz.swapee.wc.ISwapeeMeAccountController.Initialese} xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountProcessor)} xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountProcessor} xyz.swapee.wc.SwapeeMeAccountProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountProcessor
 */
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.constructor&xyz.swapee.wc.SwapeeMeAccountProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!xyz.swapee.wc.ISwapeeMeAccountCore|typeof xyz.swapee.wc.SwapeeMeAccountCore)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!xyz.swapee.wc.ISwapeeMeAccountCore|typeof xyz.swapee.wc.SwapeeMeAccountCore)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!xyz.swapee.wc.ISwapeeMeAccountCore|typeof xyz.swapee.wc.SwapeeMeAccountCore)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountProcessor}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountProcessor} xyz.swapee.wc.SwapeeMeAccountProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountProcessorCaster&xyz.swapee.wc.ISwapeeMeAccountComputer&xyz.swapee.wc.ISwapeeMeAccountCore&xyz.swapee.wc.ISwapeeMeAccountController)} xyz.swapee.wc.ISwapeeMeAccountProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountController} xyz.swapee.wc.ISwapeeMeAccountController.typeof */
/**
 * The processor to compute changes to the memory for the _ISwapeeMeAccount_.
 * @interface xyz.swapee.wc.ISwapeeMeAccountProcessor
 */
xyz.swapee.wc.ISwapeeMeAccountProcessor = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeAccountComputer.typeof&xyz.swapee.wc.ISwapeeMeAccountCore.typeof&xyz.swapee.wc.ISwapeeMeAccountController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese>)} xyz.swapee.wc.SwapeeMeAccountProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountProcessor} xyz.swapee.wc.ISwapeeMeAccountProcessor.typeof */
/**
 * A concrete class of _ISwapeeMeAccountProcessor_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountProcessor
 * @implements {xyz.swapee.wc.ISwapeeMeAccountProcessor} The processor to compute changes to the memory for the _ISwapeeMeAccount_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountProcessor = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountProcessor.constructor&xyz.swapee.wc.ISwapeeMeAccountProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountProcessor}
 */
xyz.swapee.wc.SwapeeMeAccountProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountProcessor} */
xyz.swapee.wc.RecordISwapeeMeAccountProcessor

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountProcessor} xyz.swapee.wc.BoundISwapeeMeAccountProcessor */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountProcessor} xyz.swapee.wc.BoundSwapeeMeAccountProcessor */

/**
 * Contains getters to cast the _ISwapeeMeAccountProcessor_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountProcessorCaster
 */
xyz.swapee.wc.ISwapeeMeAccountProcessorCaster = class { }
/**
 * Cast the _ISwapeeMeAccountProcessor_ instance into the _BoundISwapeeMeAccountProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountProcessor}
 */
xyz.swapee.wc.ISwapeeMeAccountProcessorCaster.prototype.asISwapeeMeAccountProcessor
/**
 * Access the _SwapeeMeAccountProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountProcessor}
 */
xyz.swapee.wc.ISwapeeMeAccountProcessorCaster.prototype.superSwapeeMeAccountProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/100-SwapeeMeAccountMemory.xml}  20847348148a12dab50c70647f5a19b1 */
/**
 * The memory of the _ISwapeeMeAccount_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.SwapeeMeAccountMemory
 */
xyz.swapee.wc.SwapeeMeAccountMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.SwapeeMeAccountMemory.prototype.core = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/102-SwapeeMeAccountInputs.xml}  3de3964e8d049580f2e610786a074210 */
/**
 * The inputs of the _ISwapeeMeAccount_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.SwapeeMeAccountInputs
 */
xyz.swapee.wc.front.SwapeeMeAccountInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.SwapeeMeAccountInputs.prototype.core = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/11-ISwapeeMeAccount.xml}  48f8e1a7ddfac0dbd50946339525f7c2 */
/**
 * An atomic wrapper for the _ISwapeeMeAccount_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.SwapeeMeAccountEnv
 */
xyz.swapee.wc.SwapeeMeAccountEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.SwapeeMeAccountEnv.prototype.swapeeMeAccount = /** @type {xyz.swapee.wc.ISwapeeMeAccount} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.ISwapeeMeAccountController.Inputs>&xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese&xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese&xyz.swapee.wc.ISwapeeMeAccountController.Initialese} xyz.swapee.wc.ISwapeeMeAccount.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccount)} xyz.swapee.wc.AbstractSwapeeMeAccount.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccount} xyz.swapee.wc.SwapeeMeAccount.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccount` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccount
 */
xyz.swapee.wc.AbstractSwapeeMeAccount = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccount.constructor&xyz.swapee.wc.SwapeeMeAccount.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccount.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccount
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccount.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccount} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccount|typeof xyz.swapee.wc.SwapeeMeAccount)|(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccount}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccount.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccount}
 */
xyz.swapee.wc.AbstractSwapeeMeAccount.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccount}
 */
xyz.swapee.wc.AbstractSwapeeMeAccount.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccount|typeof xyz.swapee.wc.SwapeeMeAccount)|(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccount}
 */
xyz.swapee.wc.AbstractSwapeeMeAccount.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccount|typeof xyz.swapee.wc.SwapeeMeAccount)|(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccount}
 */
xyz.swapee.wc.AbstractSwapeeMeAccount.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccount.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccount} xyz.swapee.wc.SwapeeMeAccountConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccount.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ISwapeeMeAccount.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ISwapeeMeAccount.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ISwapeeMeAccount.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.SwapeeMeAccountMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.SwapeeMeAccountClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountCaster&xyz.swapee.wc.ISwapeeMeAccountProcessor&xyz.swapee.wc.ISwapeeMeAccountComputer&xyz.swapee.wc.ISwapeeMeAccountController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.ISwapeeMeAccountController.Inputs, !xyz.swapee.wc.SwapeeMeAccountLand>)} xyz.swapee.wc.ISwapeeMeAccount.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ISwapeeMeAccount
 */
xyz.swapee.wc.ISwapeeMeAccount = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccount.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeAccountProcessor.typeof&xyz.swapee.wc.ISwapeeMeAccountComputer.typeof&xyz.swapee.wc.ISwapeeMeAccountController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccount* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccount.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccount.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccount&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccount.Initialese>)} xyz.swapee.wc.SwapeeMeAccount.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccount} xyz.swapee.wc.ISwapeeMeAccount.typeof */
/**
 * A concrete class of _ISwapeeMeAccount_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccount
 * @implements {xyz.swapee.wc.ISwapeeMeAccount} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccount.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccount = class extends /** @type {xyz.swapee.wc.SwapeeMeAccount.constructor&xyz.swapee.wc.ISwapeeMeAccount.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccount* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccount.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccount* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccount.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccount.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccount}
 */
xyz.swapee.wc.SwapeeMeAccount.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccount.
 * @interface xyz.swapee.wc.ISwapeeMeAccountFields
 */
xyz.swapee.wc.ISwapeeMeAccountFields = class { }
/**
 * The input pins of the _ISwapeeMeAccount_ port.
 */
xyz.swapee.wc.ISwapeeMeAccountFields.prototype.pinout = /** @type {!xyz.swapee.wc.ISwapeeMeAccount.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccount} */
xyz.swapee.wc.RecordISwapeeMeAccount

/** @typedef {xyz.swapee.wc.ISwapeeMeAccount} xyz.swapee.wc.BoundISwapeeMeAccount */

/** @typedef {xyz.swapee.wc.SwapeeMeAccount} xyz.swapee.wc.BoundSwapeeMeAccount */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountController.Inputs} xyz.swapee.wc.ISwapeeMeAccount.Pinout The input pins of the _ISwapeeMeAccount_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs>)} xyz.swapee.wc.ISwapeeMeAccountBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ISwapeeMeAccountBuffer
 */
xyz.swapee.wc.ISwapeeMeAccountBuffer = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeAccountBuffer.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountBuffer

/**
 * A concrete class of _ISwapeeMeAccountBuffer_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountBuffer
 * @implements {xyz.swapee.wc.ISwapeeMeAccountBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.SwapeeMeAccountBuffer = class extends xyz.swapee.wc.ISwapeeMeAccountBuffer { }
xyz.swapee.wc.SwapeeMeAccountBuffer.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountBuffer

/**
 * Contains getters to cast the _ISwapeeMeAccount_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountCaster
 */
xyz.swapee.wc.ISwapeeMeAccountCaster = class { }
/**
 * Cast the _ISwapeeMeAccount_ instance into the _BoundISwapeeMeAccount_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccount}
 */
xyz.swapee.wc.ISwapeeMeAccountCaster.prototype.asISwapeeMeAccount
/**
 * Access the _SwapeeMeAccount_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccount}
 */
xyz.swapee.wc.ISwapeeMeAccountCaster.prototype.superSwapeeMeAccount

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/110-SwapeeMeAccountSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeAccountMemoryPQs
 */
xyz.swapee.wc.SwapeeMeAccountMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeMeAccountMemoryPQs.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeAccountMemoryQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeAccountMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.SwapeeMeAccountMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountMemoryPQs)} xyz.swapee.wc.SwapeeMeAccountInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountMemoryPQs} xyz.swapee.wc.SwapeeMeAccountMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeAccountInputsPQs
 */
xyz.swapee.wc.SwapeeMeAccountInputsPQs = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountInputsPQs.constructor&xyz.swapee.wc.SwapeeMeAccountMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeAccountInputsPQs.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountInputsPQs

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountMemoryPQs)} xyz.swapee.wc.SwapeeMeAccountInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeAccountInputsQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeAccountInputsQPs = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountInputsQPs.constructor&xyz.swapee.wc.SwapeeMeAccountMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.SwapeeMeAccountInputsQPs.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.SwapeeMeAccountVdusPQs
 */
xyz.swapee.wc.SwapeeMeAccountVdusPQs = class {
  constructor() {
    /**
     * `ad091`
     */
    this.TokenLa=/** @type {string} */ (void 0)
    /**
     * `ad092`
     */
    this.UserpicIm=/** @type {string} */ (void 0)
    /**
     * `ad093`
     */
    this.NoUserBlock=/** @type {string} */ (void 0)
    /**
     * `ad094`
     */
    this.UserBlock=/** @type {string} */ (void 0)
    /**
     * `ad095`
     */
    this.UsernameLa=/** @type {string} */ (void 0)
    /**
     * `ad096`
     */
    this.SignOutBu=/** @type {string} */ (void 0)
    /**
     * `ad097`
     */
    this.SwapeeMe=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.SwapeeMeAccountVdusPQs.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.SwapeeMeAccountVdusQPs
 * @dict
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs = class { }
/**
 * `TokenLa`
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs.prototype.ad091 = /** @type {string} */ (void 0)
/**
 * `UserpicIm`
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs.prototype.ad092 = /** @type {string} */ (void 0)
/**
 * `NoUserBlock`
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs.prototype.ad093 = /** @type {string} */ (void 0)
/**
 * `UserBlock`
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs.prototype.ad094 = /** @type {string} */ (void 0)
/**
 * `UsernameLa`
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs.prototype.ad095 = /** @type {string} */ (void 0)
/**
 * `SignOutBu`
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs.prototype.ad096 = /** @type {string} */ (void 0)
/**
 * `SwapeeMe`
 */
xyz.swapee.wc.SwapeeMeAccountVdusQPs.prototype.ad097 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/12-ISwapeeMeAccountHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtilFields)} xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.router} */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _ISwapeeMeAccountHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountHtmlComponentUtil
 * @implements {xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil} ‎
 */
xyz.swapee.wc.SwapeeMeAccountHtmlComponentUtil = class extends xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil { }
xyz.swapee.wc.SwapeeMeAccountHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountHtmlComponentUtil

/**
 * Fields of the ISwapeeMeAccountHtmlComponentUtil.
 * @interface xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtilFields
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil} */
xyz.swapee.wc.RecordISwapeeMeAccountHtmlComponentUtil

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil} xyz.swapee.wc.BoundISwapeeMeAccountHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountHtmlComponentUtil} xyz.swapee.wc.BoundSwapeeMeAccountHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.ISwapeeMePort} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeMeAccountPort} SwapeeMeAccount The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe
 * @prop {!xyz.swapee.wc.SwapeeMeAccountMemory} SwapeeMeAccount
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.ISwapeeMe.Pinout} SwapeeMe
 * @prop {!xyz.swapee.wc.ISwapeeMeAccount.Pinout} SwapeeMeAccount
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.__router<!xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil>} xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `SwapeeMe` _typeof ISwapeeMePort_
 * - `SwapeeMeAccount` _typeof ISwapeeMeAccountPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `SwapeeMe` _!SwapeeMeMemory_
 * - `SwapeeMeAccount` _!SwapeeMeAccountMemory_
 * @param {!xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `SwapeeMe` _!ISwapeeMe.Pinout_
 * - `SwapeeMeAccount` _!ISwapeeMeAccount.Pinout_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/12-ISwapeeMeAccountHtmlComponent.xml}  e6ab2a284c061bd2a5e40dadb0478d60 */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese&xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese&xyz.swapee.wc.ISwapeeMeAccount.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ISwapeeMeAccountProcessor.Initialese&xyz.swapee.wc.ISwapeeMeAccountComputer.Initialese} xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountHtmlComponent)} xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent} xyz.swapee.wc.SwapeeMeAccountHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent
 */
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.constructor&xyz.swapee.wc.SwapeeMeAccountHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent|typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeMeAccountController|typeof xyz.swapee.wc.back.SwapeeMeAccountController)|(!xyz.swapee.wc.back.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.back.SwapeeMeAccountScreen)|(!xyz.swapee.wc.ISwapeeMeAccount|typeof xyz.swapee.wc.SwapeeMeAccount)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeMeAccountGPU|typeof xyz.swapee.wc.SwapeeMeAccountGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent|typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeMeAccountController|typeof xyz.swapee.wc.back.SwapeeMeAccountController)|(!xyz.swapee.wc.back.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.back.SwapeeMeAccountScreen)|(!xyz.swapee.wc.ISwapeeMeAccount|typeof xyz.swapee.wc.SwapeeMeAccount)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeMeAccountGPU|typeof xyz.swapee.wc.SwapeeMeAccountGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent|typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent)|(!xyz.swapee.wc.back.ISwapeeMeAccountController|typeof xyz.swapee.wc.back.SwapeeMeAccountController)|(!xyz.swapee.wc.back.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.back.SwapeeMeAccountScreen)|(!xyz.swapee.wc.ISwapeeMeAccount|typeof xyz.swapee.wc.SwapeeMeAccount)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.ISwapeeMeAccountGPU|typeof xyz.swapee.wc.SwapeeMeAccountGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ISwapeeMeAccountProcessor|typeof xyz.swapee.wc.SwapeeMeAccountProcessor)|(!xyz.swapee.wc.ISwapeeMeAccountComputer|typeof xyz.swapee.wc.SwapeeMeAccountComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountHtmlComponent} xyz.swapee.wc.SwapeeMeAccountHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountHtmlComponentCaster&xyz.swapee.wc.back.ISwapeeMeAccountController&xyz.swapee.wc.back.ISwapeeMeAccountScreen&xyz.swapee.wc.ISwapeeMeAccount&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeMeAccountLand>&xyz.swapee.wc.ISwapeeMeAccountGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.ISwapeeMeAccountController.Inputs, !HTMLDivElement, !xyz.swapee.wc.SwapeeMeAccountLand>&xyz.swapee.wc.ISwapeeMeAccountProcessor&xyz.swapee.wc.ISwapeeMeAccountComputer)} xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeAccountController} xyz.swapee.wc.back.ISwapeeMeAccountController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeAccountScreen} xyz.swapee.wc.back.ISwapeeMeAccountScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccount} xyz.swapee.wc.ISwapeeMeAccount.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountGPU} xyz.swapee.wc.ISwapeeMeAccountGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountProcessor} xyz.swapee.wc.ISwapeeMeAccountProcessor.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountComputer} xyz.swapee.wc.ISwapeeMeAccountComputer.typeof */
/**
 * The _ISwapeeMeAccount_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ISwapeeMeAccountHtmlComponent
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponent = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeMeAccountController.typeof&xyz.swapee.wc.back.ISwapeeMeAccountScreen.typeof&xyz.swapee.wc.ISwapeeMeAccount.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.ISwapeeMeAccountGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ISwapeeMeAccountProcessor.typeof&xyz.swapee.wc.ISwapeeMeAccountComputer.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.Initialese>)} xyz.swapee.wc.SwapeeMeAccountHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountHtmlComponent} xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ISwapeeMeAccountHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountHtmlComponent
 * @implements {xyz.swapee.wc.ISwapeeMeAccountHtmlComponent} The _ISwapeeMeAccount_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountHtmlComponent = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountHtmlComponent.constructor&xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountHtmlComponent}
 */
xyz.swapee.wc.SwapeeMeAccountHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountHtmlComponent} */
xyz.swapee.wc.RecordISwapeeMeAccountHtmlComponent

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountHtmlComponent} xyz.swapee.wc.BoundISwapeeMeAccountHtmlComponent */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountHtmlComponent} xyz.swapee.wc.BoundSwapeeMeAccountHtmlComponent */

/**
 * Contains getters to cast the _ISwapeeMeAccountHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountHtmlComponentCaster
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentCaster = class { }
/**
 * Cast the _ISwapeeMeAccountHtmlComponent_ instance into the _BoundISwapeeMeAccountHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountHtmlComponent}
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentCaster.prototype.asISwapeeMeAccountHtmlComponent
/**
 * Access the _SwapeeMeAccountHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountHtmlComponent}
 */
xyz.swapee.wc.ISwapeeMeAccountHtmlComponentCaster.prototype.superSwapeeMeAccountHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/130-ISwapeeMeAccountElement.xml}  0b01b9ae643d90bb149d5aa7d22aaa50 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.SwapeeMeAccountLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.ISwapeeMe>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.ISwapeeMeAccountElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ISwapeeMeAccountElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountElement)} xyz.swapee.wc.AbstractSwapeeMeAccountElement.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountElement} xyz.swapee.wc.SwapeeMeAccountElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountElement` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountElement
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElement = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountElement.constructor&xyz.swapee.wc.SwapeeMeAccountElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountElement.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElement.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountElement|typeof xyz.swapee.wc.SwapeeMeAccountElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElement}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElement}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountElement|typeof xyz.swapee.wc.SwapeeMeAccountElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElement}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountElement|typeof xyz.swapee.wc.SwapeeMeAccountElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElement}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountElement.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountElement} xyz.swapee.wc.SwapeeMeAccountElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountElementFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.ISwapeeMeAccountElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.ISwapeeMeAccountElement.Inputs, !xyz.swapee.wc.SwapeeMeAccountLand>&com.webcircuits.ILanded<!xyz.swapee.wc.SwapeeMeAccountLand>&guest.maurice.IMilleu<!xyz.swapee.wc.ISwapeeMe>)} xyz.swapee.wc.ISwapeeMeAccountElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _ISwapeeMeAccount_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ISwapeeMeAccountElement
 */
xyz.swapee.wc.ISwapeeMeAccountElement = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountElement.solder} */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountElement.render} */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountElement.build} */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountElement.buildSwapeeMe} */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.buildSwapeeMe = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountElement.short} */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountElement.server} */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountElement.inducer} */
xyz.swapee.wc.ISwapeeMeAccountElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountElement&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountElement.Initialese>)} xyz.swapee.wc.SwapeeMeAccountElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement} xyz.swapee.wc.ISwapeeMeAccountElement.typeof */
/**
 * A concrete class of _ISwapeeMeAccountElement_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountElement
 * @implements {xyz.swapee.wc.ISwapeeMeAccountElement} A component description.
 *
 * The _ISwapeeMeAccount_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountElement.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountElement = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountElement.constructor&xyz.swapee.wc.ISwapeeMeAccountElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElement}
 */
xyz.swapee.wc.SwapeeMeAccountElement.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountElement.
 * @interface xyz.swapee.wc.ISwapeeMeAccountElementFields
 */
xyz.swapee.wc.ISwapeeMeAccountElementFields = class { }
/**
 * The element-specific inputs to the _ISwapeeMeAccount_ component.
 */
xyz.swapee.wc.ISwapeeMeAccountElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMeAccountElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.ISwapeeMeAccountElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement} */
xyz.swapee.wc.RecordISwapeeMeAccountElement

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement} xyz.swapee.wc.BoundISwapeeMeAccountElement */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountElement} xyz.swapee.wc.BoundSwapeeMeAccountElement */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountPort.Inputs&xyz.swapee.wc.ISwapeeMeAccountDisplay.Queries&xyz.swapee.wc.ISwapeeMeAccountController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs} xyz.swapee.wc.ISwapeeMeAccountElement.Inputs The element-specific inputs to the _ISwapeeMeAccount_ component. */

/**
 * Contains getters to cast the _ISwapeeMeAccountElement_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountElementCaster
 */
xyz.swapee.wc.ISwapeeMeAccountElementCaster = class { }
/**
 * Cast the _ISwapeeMeAccountElement_ instance into the _BoundISwapeeMeAccountElement_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountElement}
 */
xyz.swapee.wc.ISwapeeMeAccountElementCaster.prototype.asISwapeeMeAccountElement
/**
 * Access the _SwapeeMeAccountElement_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountElement}
 */
xyz.swapee.wc.ISwapeeMeAccountElementCaster.prototype.superSwapeeMeAccountElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeMeAccountMemory, props: !xyz.swapee.wc.ISwapeeMeAccountElement.Inputs) => Object<string, *>} xyz.swapee.wc.ISwapeeMeAccountElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement.__solder<!xyz.swapee.wc.ISwapeeMeAccountElement>} xyz.swapee.wc.ISwapeeMeAccountElement._solder */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.SwapeeMeAccountMemory} model The model.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountElement.Inputs} props The element props.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ISwapeeMeAccountOuterCore.WeakModel.Core* ⤴ *ISwapeeMeAccountOuterCore.WeakModel.Core* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeMeAccountDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeMeAccountElementPort.Inputs.NoSolder* Default `false`.
 * - `[tokenLaOpts]` _!Object?_ The options to pass to the _TokenLa_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.TokenLaOpts* Default `{}`.
 * - `[userpicImOpts]` _!Object?_ The options to pass to the _UserpicIm_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UserpicImOpts* Default `{}`.
 * - `[noUserBlockOpts]` _!Object?_ The options to pass to the _NoUserBlock_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts* Default `{}`.
 * - `[userBlockOpts]` _!Object?_ The options to pass to the _UserBlock_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UserBlockOpts* Default `{}`.
 * - `[usernameLaOpts]` _!Object?_ The options to pass to the _UsernameLa_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts* Default `{}`.
 * - `[signOutBuOpts]` _!Object?_ The options to pass to the _SignOutBu_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ISwapeeMeAccountElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeMeAccountMemory, instance?: !xyz.swapee.wc.ISwapeeMeAccountScreen&xyz.swapee.wc.ISwapeeMeAccountController) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeMeAccountElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement.__render<!xyz.swapee.wc.ISwapeeMeAccountElement>} xyz.swapee.wc.ISwapeeMeAccountElement._render */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.SwapeeMeAccountMemory} [model] The model for the view.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountScreen&xyz.swapee.wc.ISwapeeMeAccountController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeMeAccountElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.ISwapeeMeAccountElement.build.Cores, instances: !xyz.swapee.wc.ISwapeeMeAccountElement.build.Instances) => ?} xyz.swapee.wc.ISwapeeMeAccountElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement.__build<!xyz.swapee.wc.ISwapeeMeAccountElement>} xyz.swapee.wc.ISwapeeMeAccountElement._build */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountElement.build.Cores} cores The models of components on the land.
 * - `SwapeeMe` _!xyz.swapee.wc.ISwapeeMeCore.Model_
 * @param {!xyz.swapee.wc.ISwapeeMeAccountElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `SwapeeMe` _!xyz.swapee.wc.ISwapeeMe_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeMeAccountElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.ISwapeeMeCore.Model} SwapeeMe
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.ISwapeeMe} SwapeeMe
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ISwapeeMeCore.Model, instance: !xyz.swapee.wc.ISwapeeMe) => ?} xyz.swapee.wc.ISwapeeMeAccountElement.__buildSwapeeMe
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement.__buildSwapeeMe<!xyz.swapee.wc.ISwapeeMeAccountElement>} xyz.swapee.wc.ISwapeeMeAccountElement._buildSwapeeMe */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement.buildSwapeeMe} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.ISwapeeMe_ component.
 * @param {!xyz.swapee.wc.ISwapeeMeCore.Model} model
 * @param {!xyz.swapee.wc.ISwapeeMe} instance
 * @return {?}
 */
xyz.swapee.wc.ISwapeeMeAccountElement.buildSwapeeMe = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.SwapeeMeAccountMemory, ports: !xyz.swapee.wc.ISwapeeMeAccountElement.short.Ports, cores: !xyz.swapee.wc.ISwapeeMeAccountElement.short.Cores) => ?} xyz.swapee.wc.ISwapeeMeAccountElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement.__short<!xyz.swapee.wc.ISwapeeMeAccountElement>} xyz.swapee.wc.ISwapeeMeAccountElement._short */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.SwapeeMeAccountMemory} model The model from which to feed properties to peer's ports.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountElement.short.Ports} ports The ports of the peers.
 * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMePortInterface_
 * @param {!xyz.swapee.wc.ISwapeeMeAccountElement.short.Cores} cores The cores of the peers.
 * - `SwapeeMe` _xyz.swapee.wc.ISwapeeMeCore.Model_
 * @return {?}
 */
xyz.swapee.wc.ISwapeeMeAccountElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMePortInterface} SwapeeMe
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.ISwapeeMeCore.Model} SwapeeMe
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeMeAccountMemory, inputs: !xyz.swapee.wc.ISwapeeMeAccountElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ISwapeeMeAccountElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement.__server<!xyz.swapee.wc.ISwapeeMeAccountElement>} xyz.swapee.wc.ISwapeeMeAccountElement._server */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.SwapeeMeAccountMemory} memory The memory registers.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountElement.Inputs} inputs The inputs to the port.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ISwapeeMeAccountOuterCore.WeakModel.Core* ⤴ *ISwapeeMeAccountOuterCore.WeakModel.Core* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeMeAccountDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeMeAccountElementPort.Inputs.NoSolder* Default `false`.
 * - `[tokenLaOpts]` _!Object?_ The options to pass to the _TokenLa_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.TokenLaOpts* Default `{}`.
 * - `[userpicImOpts]` _!Object?_ The options to pass to the _UserpicIm_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UserpicImOpts* Default `{}`.
 * - `[noUserBlockOpts]` _!Object?_ The options to pass to the _NoUserBlock_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts* Default `{}`.
 * - `[userBlockOpts]` _!Object?_ The options to pass to the _UserBlock_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UserBlockOpts* Default `{}`.
 * - `[usernameLaOpts]` _!Object?_ The options to pass to the _UsernameLa_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts* Default `{}`.
 * - `[signOutBuOpts]` _!Object?_ The options to pass to the _SignOutBu_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ISwapeeMeAccountElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.SwapeeMeAccountMemory, port?: !xyz.swapee.wc.ISwapeeMeAccountElement.Inputs) => ?} xyz.swapee.wc.ISwapeeMeAccountElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElement.__inducer<!xyz.swapee.wc.ISwapeeMeAccountElement>} xyz.swapee.wc.ISwapeeMeAccountElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.SwapeeMeAccountMemory} [model] The model of the component into which to induce the state.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.ISwapeeMeAccountElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *ISwapeeMeAccountOuterCore.WeakModel.Core* ⤴ *ISwapeeMeAccountOuterCore.WeakModel.Core* Default `null`.
 * - `[swapeeMeSel=""]` _string?_ The query to discover the _SwapeeMe_ VDU. ⤴ *ISwapeeMeAccountDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ISwapeeMeAccountElementPort.Inputs.NoSolder* Default `false`.
 * - `[tokenLaOpts]` _!Object?_ The options to pass to the _TokenLa_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.TokenLaOpts* Default `{}`.
 * - `[userpicImOpts]` _!Object?_ The options to pass to the _UserpicIm_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UserpicImOpts* Default `{}`.
 * - `[noUserBlockOpts]` _!Object?_ The options to pass to the _NoUserBlock_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts* Default `{}`.
 * - `[userBlockOpts]` _!Object?_ The options to pass to the _UserBlock_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UserBlockOpts* Default `{}`.
 * - `[usernameLaOpts]` _!Object?_ The options to pass to the _UsernameLa_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts* Default `{}`.
 * - `[signOutBuOpts]` _!Object?_ The options to pass to the _SignOutBu_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts* Default `{}`.
 * - `[swapeeMeOpts]` _!Object?_ The options to pass to the _SwapeeMe_ vdu. ⤴ *ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.ISwapeeMeAccountElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeAccountElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/140-ISwapeeMeAccountElementPort.xml}  d7ade498e9775800a3929274f0f40f21 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ISwapeeMeAccountElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountElementPort)} xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountElementPort} xyz.swapee.wc.SwapeeMeAccountElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountElementPort
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.constructor&xyz.swapee.wc.SwapeeMeAccountElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountElementPort|typeof xyz.swapee.wc.SwapeeMeAccountElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountElementPort|typeof xyz.swapee.wc.SwapeeMeAccountElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountElementPort|typeof xyz.swapee.wc.SwapeeMeAccountElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElementPort}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountElementPort.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountElementPort} xyz.swapee.wc.SwapeeMeAccountElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs>)} xyz.swapee.wc.ISwapeeMeAccountElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ISwapeeMeAccountElementPort
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountElementPort.Initialese>)} xyz.swapee.wc.SwapeeMeAccountElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort} xyz.swapee.wc.ISwapeeMeAccountElementPort.typeof */
/**
 * A concrete class of _ISwapeeMeAccountElementPort_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountElementPort
 * @implements {xyz.swapee.wc.ISwapeeMeAccountElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountElementPort.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountElementPort = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountElementPort.constructor&xyz.swapee.wc.ISwapeeMeAccountElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountElementPort}
 */
xyz.swapee.wc.SwapeeMeAccountElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountElementPort.
 * @interface xyz.swapee.wc.ISwapeeMeAccountElementPortFields
 */
xyz.swapee.wc.ISwapeeMeAccountElementPortFields = class { }
/**
 * The inputs to the _ISwapeeMeAccountElement_'s controller via its element port.
 */
xyz.swapee.wc.ISwapeeMeAccountElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ISwapeeMeAccountElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElementPort} */
xyz.swapee.wc.RecordISwapeeMeAccountElementPort

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountElementPort} xyz.swapee.wc.BoundISwapeeMeAccountElementPort */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountElementPort} xyz.swapee.wc.BoundSwapeeMeAccountElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _TokenLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts.tokenLaOpts

/**
 * The options to pass to the _UserpicIm_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts.userpicImOpts

/**
 * The options to pass to the _NoUserBlock_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts.noUserBlockOpts

/**
 * The options to pass to the _UserBlock_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts.userBlockOpts

/**
 * The options to pass to the _UsernameLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts.usernameLaOpts

/**
 * The options to pass to the _SignOutBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts.signOutBuOpts

/**
 * The options to pass to the _SwapeeMe_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts.swapeeMeOpts

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts)} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts.typeof */
/**
 * The inputs to the _ISwapeeMeAccountElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.constructor&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoSolder&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.TokenLaOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserpicImOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoUserBlockOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserBlockOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UsernameLaOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SignOutBuOpts&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SwapeeMeOpts)} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.TokenLaOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.TokenLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserpicImOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserpicImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoUserBlockOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoUserBlockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserBlockOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserBlockOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UsernameLaOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UsernameLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SignOutBuOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SignOutBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SwapeeMeOpts} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SwapeeMeOpts.typeof */
/**
 * The inputs to the _ISwapeeMeAccountElement_'s controller via its element port.
 * @record xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs
 */
xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.constructor&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.TokenLaOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserpicImOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoUserBlockOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserBlockOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UsernameLaOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SignOutBuOpts.typeof&xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SwapeeMeOpts.typeof} */ (class {}) { }
xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs

/**
 * Contains getters to cast the _ISwapeeMeAccountElementPort_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountElementPortCaster
 */
xyz.swapee.wc.ISwapeeMeAccountElementPortCaster = class { }
/**
 * Cast the _ISwapeeMeAccountElementPort_ instance into the _BoundISwapeeMeAccountElementPort_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountElementPort}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPortCaster.prototype.asISwapeeMeAccountElementPort
/**
 * Access the _SwapeeMeAccountElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountElementPort}
 */
xyz.swapee.wc.ISwapeeMeAccountElementPortCaster.prototype.superSwapeeMeAccountElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts The options to pass to the _TokenLa_ vdu (optional overlay).
 * @prop {!Object} [tokenLaOpts] The options to pass to the _TokenLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.TokenLaOpts_Safe The options to pass to the _TokenLa_ vdu (required overlay).
 * @prop {!Object} tokenLaOpts The options to pass to the _TokenLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts The options to pass to the _UserpicIm_ vdu (optional overlay).
 * @prop {!Object} [userpicImOpts] The options to pass to the _UserpicIm_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserpicImOpts_Safe The options to pass to the _UserpicIm_ vdu (required overlay).
 * @prop {!Object} userpicImOpts The options to pass to the _UserpicIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts The options to pass to the _NoUserBlock_ vdu (optional overlay).
 * @prop {!Object} [noUserBlockOpts] The options to pass to the _NoUserBlock_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.NoUserBlockOpts_Safe The options to pass to the _NoUserBlock_ vdu (required overlay).
 * @prop {!Object} noUserBlockOpts The options to pass to the _NoUserBlock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts The options to pass to the _UserBlock_ vdu (optional overlay).
 * @prop {!Object} [userBlockOpts] The options to pass to the _UserBlock_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UserBlockOpts_Safe The options to pass to the _UserBlock_ vdu (required overlay).
 * @prop {!Object} userBlockOpts The options to pass to the _UserBlock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts The options to pass to the _UsernameLa_ vdu (optional overlay).
 * @prop {!Object} [usernameLaOpts] The options to pass to the _UsernameLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.UsernameLaOpts_Safe The options to pass to the _UsernameLa_ vdu (required overlay).
 * @prop {!Object} usernameLaOpts The options to pass to the _UsernameLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts The options to pass to the _SignOutBu_ vdu (optional overlay).
 * @prop {!Object} [signOutBuOpts] The options to pass to the _SignOutBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SignOutBuOpts_Safe The options to pass to the _SignOutBu_ vdu (required overlay).
 * @prop {!Object} signOutBuOpts The options to pass to the _SignOutBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts The options to pass to the _SwapeeMe_ vdu (optional overlay).
 * @prop {!Object} [swapeeMeOpts] The options to pass to the _SwapeeMe_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.Inputs.SwapeeMeOpts_Safe The options to pass to the _SwapeeMe_ vdu (required overlay).
 * @prop {!Object} swapeeMeOpts The options to pass to the _SwapeeMe_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.TokenLaOpts The options to pass to the _TokenLa_ vdu (optional overlay).
 * @prop {*} [tokenLaOpts=null] The options to pass to the _TokenLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.TokenLaOpts_Safe The options to pass to the _TokenLa_ vdu (required overlay).
 * @prop {*} tokenLaOpts The options to pass to the _TokenLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserpicImOpts The options to pass to the _UserpicIm_ vdu (optional overlay).
 * @prop {*} [userpicImOpts=null] The options to pass to the _UserpicIm_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserpicImOpts_Safe The options to pass to the _UserpicIm_ vdu (required overlay).
 * @prop {*} userpicImOpts The options to pass to the _UserpicIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoUserBlockOpts The options to pass to the _NoUserBlock_ vdu (optional overlay).
 * @prop {*} [noUserBlockOpts=null] The options to pass to the _NoUserBlock_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.NoUserBlockOpts_Safe The options to pass to the _NoUserBlock_ vdu (required overlay).
 * @prop {*} noUserBlockOpts The options to pass to the _NoUserBlock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserBlockOpts The options to pass to the _UserBlock_ vdu (optional overlay).
 * @prop {*} [userBlockOpts=null] The options to pass to the _UserBlock_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UserBlockOpts_Safe The options to pass to the _UserBlock_ vdu (required overlay).
 * @prop {*} userBlockOpts The options to pass to the _UserBlock_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UsernameLaOpts The options to pass to the _UsernameLa_ vdu (optional overlay).
 * @prop {*} [usernameLaOpts=null] The options to pass to the _UsernameLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.UsernameLaOpts_Safe The options to pass to the _UsernameLa_ vdu (required overlay).
 * @prop {*} usernameLaOpts The options to pass to the _UsernameLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SignOutBuOpts The options to pass to the _SignOutBu_ vdu (optional overlay).
 * @prop {*} [signOutBuOpts=null] The options to pass to the _SignOutBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SignOutBuOpts_Safe The options to pass to the _SignOutBu_ vdu (required overlay).
 * @prop {*} signOutBuOpts The options to pass to the _SignOutBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SwapeeMeOpts The options to pass to the _SwapeeMe_ vdu (optional overlay).
 * @prop {*} [swapeeMeOpts=null] The options to pass to the _SwapeeMe_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountElementPort.WeakInputs.SwapeeMeOpts_Safe The options to pass to the _SwapeeMe_ vdu (required overlay).
 * @prop {*} swapeeMeOpts The options to pass to the _SwapeeMe_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/170-ISwapeeMeAccountDesigner.xml}  517e7917173a77d0a4df7ab0180d6843 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ISwapeeMeAccountDesigner
 */
xyz.swapee.wc.ISwapeeMeAccountDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.SwapeeMeAccountClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeMeAccount />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeMeAccountClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ISwapeeMeAccount />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.SwapeeMeAccountClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeMeAccountDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMeController_
   * - `SwapeeMeAccount` _typeof ISwapeeMeAccountController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ISwapeeMeAccountDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `SwapeeMe` _typeof xyz.swapee.wc.ISwapeeMeController_
   * - `SwapeeMeAccount` _typeof ISwapeeMeAccountController_
   * - `This` _typeof ISwapeeMeAccountController_
   * @param {!xyz.swapee.wc.ISwapeeMeAccountDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `SwapeeMe` _!xyz.swapee.wc.SwapeeMeMemory_
   * - `SwapeeMeAccount` _!SwapeeMeAccountMemory_
   * - `This` _!SwapeeMeAccountMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeMeAccountClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.SwapeeMeAccountClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ISwapeeMeAccountDesigner.prototype.constructor = xyz.swapee.wc.ISwapeeMeAccountDesigner

/**
 * A concrete class of _ISwapeeMeAccountDesigner_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountDesigner
 * @implements {xyz.swapee.wc.ISwapeeMeAccountDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.SwapeeMeAccountDesigner = class extends xyz.swapee.wc.ISwapeeMeAccountDesigner { }
xyz.swapee.wc.SwapeeMeAccountDesigner.prototype.constructor = xyz.swapee.wc.SwapeeMeAccountDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeMeAccountController} SwapeeMeAccount
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ISwapeeMeController} SwapeeMe
 * @prop {typeof xyz.swapee.wc.ISwapeeMeAccountController} SwapeeMeAccount
 * @prop {typeof xyz.swapee.wc.ISwapeeMeAccountController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.SwapeeMeMemory} SwapeeMe
 * @prop {!xyz.swapee.wc.SwapeeMeAccountMemory} SwapeeMeAccount
 * @prop {!xyz.swapee.wc.SwapeeMeAccountMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/200-SwapeeMeAccountLand.xml}  6bc86a4b0bdaeafd5ef0b32406313ff4 */
/**
 * The surrounding of the _ISwapeeMeAccount_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.SwapeeMeAccountLand
 */
xyz.swapee.wc.SwapeeMeAccountLand = class { }
/**
 *
 */
xyz.swapee.wc.SwapeeMeAccountLand.prototype.SwapeeMe = /** @type {xyz.swapee.wc.ISwapeeMe} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/40-ISwapeeMeAccountDisplay.xml}  50f11abed594f9959ba580ac89279730 */
/**
 * @typedef {Object} $xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese
 * @prop {HTMLSpanElement} [TokenLa] The label for the token.
 * @prop {HTMLImageElement} [UserpicIm] The image for the userpic.
 * @prop {HTMLDivElement} [NoUserBlock]
 * @prop {HTMLDivElement} [UserBlock]
 * @prop {HTMLSpanElement} [UsernameLa]
 * @prop {HTMLButtonElement} [SignOutBu]
 * @prop {HTMLElement} [SwapeeMe] The via for the _SwapeeMe_ peer.
 */
/** @typedef {$xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ISwapeeMeAccountDisplay.Settings>} xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountDisplay)} xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountDisplay} xyz.swapee.wc.SwapeeMeAccountDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountDisplay
 */
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.constructor&xyz.swapee.wc.SwapeeMeAccountDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.SwapeeMeAccountDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.SwapeeMeAccountDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.SwapeeMeAccountDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountDisplay} xyz.swapee.wc.SwapeeMeAccountDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.SwapeeMeAccountMemory, !HTMLDivElement, !xyz.swapee.wc.ISwapeeMeAccountDisplay.Settings, xyz.swapee.wc.ISwapeeMeAccountDisplay.Queries, null>)} xyz.swapee.wc.ISwapeeMeAccountDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ISwapeeMeAccount_.
 * @interface xyz.swapee.wc.ISwapeeMeAccountDisplay
 */
xyz.swapee.wc.ISwapeeMeAccountDisplay = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountDisplay.paint} */
xyz.swapee.wc.ISwapeeMeAccountDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese>)} xyz.swapee.wc.SwapeeMeAccountDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountDisplay} xyz.swapee.wc.ISwapeeMeAccountDisplay.typeof */
/**
 * A concrete class of _ISwapeeMeAccountDisplay_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountDisplay
 * @implements {xyz.swapee.wc.ISwapeeMeAccountDisplay} Display for presenting information from the _ISwapeeMeAccount_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountDisplay = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountDisplay.constructor&xyz.swapee.wc.ISwapeeMeAccountDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.SwapeeMeAccountDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountDisplay.
 * @interface xyz.swapee.wc.ISwapeeMeAccountDisplayFields
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ISwapeeMeAccountDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ISwapeeMeAccountDisplay.Queries} */ (void 0)
/**
 * The label for the token. Default `null`.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.TokenLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * The image for the userpic. Default `null`.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.UserpicIm = /** @type {HTMLImageElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.NoUserBlock = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.UserBlock = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.UsernameLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.SignOutBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * The via for the _SwapeeMe_ peer. Default `null`.
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayFields.prototype.SwapeeMe = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountDisplay} */
xyz.swapee.wc.RecordISwapeeMeAccountDisplay

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountDisplay} xyz.swapee.wc.BoundISwapeeMeAccountDisplay */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountDisplay} xyz.swapee.wc.BoundSwapeeMeAccountDisplay */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountDisplay.Queries} xyz.swapee.wc.ISwapeeMeAccountDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.ISwapeeMeAccountDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [swapeeMeSel=""] The query to discover the _SwapeeMe_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _ISwapeeMeAccountDisplay_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountDisplayCaster
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayCaster = class { }
/**
 * Cast the _ISwapeeMeAccountDisplay_ instance into the _BoundISwapeeMeAccountDisplay_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountDisplay}
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayCaster.prototype.asISwapeeMeAccountDisplay
/**
 * Cast the _ISwapeeMeAccountDisplay_ instance into the _BoundISwapeeMeAccountScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountScreen}
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayCaster.prototype.asISwapeeMeAccountScreen
/**
 * Access the _SwapeeMeAccountDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountDisplay}
 */
xyz.swapee.wc.ISwapeeMeAccountDisplayCaster.prototype.superSwapeeMeAccountDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.SwapeeMeAccountMemory, land: null) => void} xyz.swapee.wc.ISwapeeMeAccountDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountDisplay.__paint<!xyz.swapee.wc.ISwapeeMeAccountDisplay>} xyz.swapee.wc.ISwapeeMeAccountDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeMeAccountMemory} memory The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeAccountDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeAccountDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/40-ISwapeeMeAccountDisplayBack.xml}  75876881dc14731f484c2d86c5bd011d */
/**
 * @typedef {Object} $xyz.swapee.wc.back.ISwapeeMeAccountDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [TokenLa] The label for the token.
 * @prop {!com.webcircuits.IHtmlTwin} [UserpicIm] The image for the userpic.
 * @prop {!com.webcircuits.IHtmlTwin} [NoUserBlock]
 * @prop {!com.webcircuits.IHtmlTwin} [UserBlock]
 * @prop {!com.webcircuits.IHtmlTwin} [UsernameLa]
 * @prop {!com.webcircuits.IHtmlTwin} [SignOutBu]
 * @prop {!com.webcircuits.IHtmlTwin} [SwapeeMe] The via for the _SwapeeMe_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.ISwapeeMeAccountDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.SwapeeMeAccountClasses>} xyz.swapee.wc.back.ISwapeeMeAccountDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeAccountDisplay)} xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay} xyz.swapee.wc.back.SwapeeMeAccountDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeAccountDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.constructor&xyz.swapee.wc.back.SwapeeMeAccountDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeAccountDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.SwapeeMeAccountClasses, !xyz.swapee.wc.SwapeeMeAccountLand>)} xyz.swapee.wc.back.ISwapeeMeAccountDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountDisplay
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplay = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeAccountDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ISwapeeMeAccountDisplay.paint} */
xyz.swapee.wc.back.ISwapeeMeAccountDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeAccountDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountDisplay.Initialese>)} xyz.swapee.wc.back.SwapeeMeAccountDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeAccountDisplay} xyz.swapee.wc.back.ISwapeeMeAccountDisplay.typeof */
/**
 * A concrete class of _ISwapeeMeAccountDisplay_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeAccountDisplay
 * @implements {xyz.swapee.wc.back.ISwapeeMeAccountDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeAccountDisplay = class extends /** @type {xyz.swapee.wc.back.SwapeeMeAccountDisplay.constructor&xyz.swapee.wc.back.ISwapeeMeAccountDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.SwapeeMeAccountDisplay.prototype.constructor = xyz.swapee.wc.back.SwapeeMeAccountDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay}
 */
xyz.swapee.wc.back.SwapeeMeAccountDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountDisplay.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields = class { }
/**
 * The label for the token.
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields.prototype.TokenLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The image for the userpic.
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields.prototype.UserpicIm = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields.prototype.NoUserBlock = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields.prototype.UserBlock = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields.prototype.UsernameLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields.prototype.SignOutBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _SwapeeMe_ peer.
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayFields.prototype.SwapeeMe = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountDisplay} */
xyz.swapee.wc.back.RecordISwapeeMeAccountDisplay

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountDisplay} xyz.swapee.wc.back.BoundISwapeeMeAccountDisplay */

/** @typedef {xyz.swapee.wc.back.SwapeeMeAccountDisplay} xyz.swapee.wc.back.BoundSwapeeMeAccountDisplay */

/**
 * Contains getters to cast the _ISwapeeMeAccountDisplay_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountDisplayCaster
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayCaster = class { }
/**
 * Cast the _ISwapeeMeAccountDisplay_ instance into the _BoundISwapeeMeAccountDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeAccountDisplay}
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayCaster.prototype.asISwapeeMeAccountDisplay
/**
 * Access the _SwapeeMeAccountDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeAccountDisplay}
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplayCaster.prototype.superSwapeeMeAccountDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.SwapeeMeAccountMemory, land?: !xyz.swapee.wc.SwapeeMeAccountLand) => void} xyz.swapee.wc.back.ISwapeeMeAccountDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountDisplay.__paint<!xyz.swapee.wc.back.ISwapeeMeAccountDisplay>} xyz.swapee.wc.back.ISwapeeMeAccountDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeAccountDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.SwapeeMeAccountMemory} [memory] The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.SwapeeMeAccountLand} [land] The land data.
 * - `SwapeeMe` _xyz.swapee.wc.ISwapeeMe_
 * @return {void}
 */
xyz.swapee.wc.back.ISwapeeMeAccountDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ISwapeeMeAccountDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/41-SwapeeMeAccountClasses.xml}  676a1e71c6e8ad4fafdb017f95d07ffd */
/**
 * The classes of the _ISwapeeMeAccountDisplay_.
 * @record xyz.swapee.wc.SwapeeMeAccountClasses
 */
xyz.swapee.wc.SwapeeMeAccountClasses = class { }
/**
 * The class.
 */
xyz.swapee.wc.SwapeeMeAccountClasses.prototype.Shown = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.SwapeeMeAccountClasses.prototype.props = /** @type {xyz.swapee.wc.SwapeeMeAccountClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/50-ISwapeeMeAccountController.xml}  0b2889ad7457341b60c387aa26f3ec6b */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs, !xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel>} xyz.swapee.wc.ISwapeeMeAccountController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountController)} xyz.swapee.wc.AbstractSwapeeMeAccountController.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountController} xyz.swapee.wc.SwapeeMeAccountController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountController` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountController
 */
xyz.swapee.wc.AbstractSwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountController.constructor&xyz.swapee.wc.SwapeeMeAccountController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountController.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountController.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountController}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountController}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountController}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountController}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountController.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountController} xyz.swapee.wc.SwapeeMeAccountControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs, !xyz.swapee.wc.ISwapeeMeAccountOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ISwapeeMeAccountOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs, !xyz.swapee.wc.ISwapeeMeAccountController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs, !xyz.swapee.wc.SwapeeMeAccountMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs>)} xyz.swapee.wc.ISwapeeMeAccountController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ISwapeeMeAccountController
 */
xyz.swapee.wc.ISwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ISwapeeMeAccountController.resetPort} */
xyz.swapee.wc.ISwapeeMeAccountController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountController&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountController.Initialese>)} xyz.swapee.wc.SwapeeMeAccountController.constructor */
/**
 * A concrete class of _ISwapeeMeAccountController_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountController
 * @implements {xyz.swapee.wc.ISwapeeMeAccountController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountController.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountController.constructor&xyz.swapee.wc.ISwapeeMeAccountController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountController}
 */
xyz.swapee.wc.SwapeeMeAccountController.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountController.
 * @interface xyz.swapee.wc.ISwapeeMeAccountControllerFields
 */
xyz.swapee.wc.ISwapeeMeAccountControllerFields = class { }
/**
 * The inputs to the _ISwapeeMeAccount_'s controller.
 */
xyz.swapee.wc.ISwapeeMeAccountControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ISwapeeMeAccountController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ISwapeeMeAccountControllerFields.prototype.props = /** @type {xyz.swapee.wc.ISwapeeMeAccountController} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountController} */
xyz.swapee.wc.RecordISwapeeMeAccountController

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountController} xyz.swapee.wc.BoundISwapeeMeAccountController */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountController} xyz.swapee.wc.BoundSwapeeMeAccountController */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountPort.Inputs} xyz.swapee.wc.ISwapeeMeAccountController.Inputs The inputs to the _ISwapeeMeAccount_'s controller. */

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountPort.WeakInputs} xyz.swapee.wc.ISwapeeMeAccountController.WeakInputs The inputs to the _ISwapeeMeAccount_'s controller. */

/**
 * Contains getters to cast the _ISwapeeMeAccountController_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountControllerCaster
 */
xyz.swapee.wc.ISwapeeMeAccountControllerCaster = class { }
/**
 * Cast the _ISwapeeMeAccountController_ instance into the _BoundISwapeeMeAccountController_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountController}
 */
xyz.swapee.wc.ISwapeeMeAccountControllerCaster.prototype.asISwapeeMeAccountController
/**
 * Cast the _ISwapeeMeAccountController_ instance into the _BoundISwapeeMeAccountProcessor_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountProcessor}
 */
xyz.swapee.wc.ISwapeeMeAccountControllerCaster.prototype.asISwapeeMeAccountProcessor
/**
 * Access the _SwapeeMeAccountController_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountController}
 */
xyz.swapee.wc.ISwapeeMeAccountControllerCaster.prototype.superSwapeeMeAccountController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ISwapeeMeAccountController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ISwapeeMeAccountController.__resetPort<!xyz.swapee.wc.ISwapeeMeAccountController>} xyz.swapee.wc.ISwapeeMeAccountController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ISwapeeMeAccountController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ISwapeeMeAccountController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/51-ISwapeeMeAccountControllerFront.xml}  33f62b6ecdb802637451a7054153dd9f */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ISwapeeMeAccountController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeMeAccountController)} xyz.swapee.wc.front.AbstractSwapeeMeAccountController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeMeAccountController} xyz.swapee.wc.front.SwapeeMeAccountController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeMeAccountController` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeMeAccountController
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeMeAccountController.constructor&xyz.swapee.wc.front.SwapeeMeAccountController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeMeAccountController.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeMeAccountController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountController.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountController|typeof xyz.swapee.wc.front.SwapeeMeAccountController)|(!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT|typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountController|typeof xyz.swapee.wc.front.SwapeeMeAccountController)|(!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT|typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountController|typeof xyz.swapee.wc.front.SwapeeMeAccountController)|(!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT|typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountController}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeMeAccountController.Initialese[]) => xyz.swapee.wc.front.ISwapeeMeAccountController} xyz.swapee.wc.front.SwapeeMeAccountControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeMeAccountControllerCaster&xyz.swapee.wc.front.ISwapeeMeAccountControllerAT)} xyz.swapee.wc.front.ISwapeeMeAccountController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeAccountControllerAT} xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ISwapeeMeAccountController
 */
xyz.swapee.wc.front.ISwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.front.ISwapeeMeAccountController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeMeAccountController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeMeAccountController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeAccountController.Initialese>)} xyz.swapee.wc.front.SwapeeMeAccountController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeAccountController} xyz.swapee.wc.front.ISwapeeMeAccountController.typeof */
/**
 * A concrete class of _ISwapeeMeAccountController_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeAccountController
 * @implements {xyz.swapee.wc.front.ISwapeeMeAccountController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeAccountController.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.front.SwapeeMeAccountController.constructor&xyz.swapee.wc.front.ISwapeeMeAccountController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeMeAccountController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountController}
 */
xyz.swapee.wc.front.SwapeeMeAccountController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeMeAccountController} */
xyz.swapee.wc.front.RecordISwapeeMeAccountController

/** @typedef {xyz.swapee.wc.front.ISwapeeMeAccountController} xyz.swapee.wc.front.BoundISwapeeMeAccountController */

/** @typedef {xyz.swapee.wc.front.SwapeeMeAccountController} xyz.swapee.wc.front.BoundSwapeeMeAccountController */

/**
 * Contains getters to cast the _ISwapeeMeAccountController_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeAccountControllerCaster
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerCaster = class { }
/**
 * Cast the _ISwapeeMeAccountController_ instance into the _BoundISwapeeMeAccountController_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeMeAccountController}
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerCaster.prototype.asISwapeeMeAccountController
/**
 * Access the _SwapeeMeAccountController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeMeAccountController}
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerCaster.prototype.superSwapeeMeAccountController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/52-ISwapeeMeAccountControllerBack.xml}  3fabc93532bf74c8b7e56efecba95b2d */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs>&xyz.swapee.wc.ISwapeeMeAccountController.Initialese} xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeAccountController)} xyz.swapee.wc.back.AbstractSwapeeMeAccountController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeAccountController} xyz.swapee.wc.back.SwapeeMeAccountController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeAccountController` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeAccountController
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeAccountController.constructor&xyz.swapee.wc.back.SwapeeMeAccountController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeAccountController.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeAccountController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountController.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountController|typeof xyz.swapee.wc.back.SwapeeMeAccountController)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountController|typeof xyz.swapee.wc.back.SwapeeMeAccountController)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountController|typeof xyz.swapee.wc.back.SwapeeMeAccountController)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountController}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeAccountController} xyz.swapee.wc.back.SwapeeMeAccountControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeAccountControllerCaster&xyz.swapee.wc.ISwapeeMeAccountController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ISwapeeMeAccountController.Inputs>)} xyz.swapee.wc.back.ISwapeeMeAccountController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountController
 */
xyz.swapee.wc.back.ISwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeAccountController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ISwapeeMeAccountController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeAccountController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeAccountController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese>)} xyz.swapee.wc.back.SwapeeMeAccountController.constructor */
/**
 * A concrete class of _ISwapeeMeAccountController_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeAccountController
 * @implements {xyz.swapee.wc.back.ISwapeeMeAccountController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeAccountController = class extends /** @type {xyz.swapee.wc.back.SwapeeMeAccountController.constructor&xyz.swapee.wc.back.ISwapeeMeAccountController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeAccountController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountController}
 */
xyz.swapee.wc.back.SwapeeMeAccountController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountController} */
xyz.swapee.wc.back.RecordISwapeeMeAccountController

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountController} xyz.swapee.wc.back.BoundISwapeeMeAccountController */

/** @typedef {xyz.swapee.wc.back.SwapeeMeAccountController} xyz.swapee.wc.back.BoundSwapeeMeAccountController */

/**
 * Contains getters to cast the _ISwapeeMeAccountController_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountControllerCaster
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerCaster = class { }
/**
 * Cast the _ISwapeeMeAccountController_ instance into the _BoundISwapeeMeAccountController_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeAccountController}
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerCaster.prototype.asISwapeeMeAccountController
/**
 * Access the _SwapeeMeAccountController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeAccountController}
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerCaster.prototype.superSwapeeMeAccountController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/53-ISwapeeMeAccountControllerAR.xml}  32c0910c8df8e91c49b81aeaa8ba2d42 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeMeAccountController.Initialese} xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeAccountControllerAR)} xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR} xyz.swapee.wc.back.SwapeeMeAccountControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeAccountControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.constructor&xyz.swapee.wc.back.SwapeeMeAccountControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR|typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR|typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR|typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeAccountController|typeof xyz.swapee.wc.SwapeeMeAccountController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeAccountControllerAR} xyz.swapee.wc.back.SwapeeMeAccountControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeAccountControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeMeAccountController)} xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeMeAccountControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountControllerAR
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerAR = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeMeAccountController.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeAccountControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.Initialese>)} xyz.swapee.wc.back.SwapeeMeAccountControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeAccountControllerAR} xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.typeof */
/**
 * A concrete class of _ISwapeeMeAccountControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeAccountControllerAR
 * @implements {xyz.swapee.wc.back.ISwapeeMeAccountControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ISwapeeMeAccountControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeAccountControllerAR = class extends /** @type {xyz.swapee.wc.back.SwapeeMeAccountControllerAR.constructor&xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeAccountControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountControllerAR}
 */
xyz.swapee.wc.back.SwapeeMeAccountControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountControllerAR} */
xyz.swapee.wc.back.RecordISwapeeMeAccountControllerAR

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountControllerAR} xyz.swapee.wc.back.BoundISwapeeMeAccountControllerAR */

/** @typedef {xyz.swapee.wc.back.SwapeeMeAccountControllerAR} xyz.swapee.wc.back.BoundSwapeeMeAccountControllerAR */

/**
 * Contains getters to cast the _ISwapeeMeAccountControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountControllerARCaster
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerARCaster = class { }
/**
 * Cast the _ISwapeeMeAccountControllerAR_ instance into the _BoundISwapeeMeAccountControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeAccountControllerAR}
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerARCaster.prototype.asISwapeeMeAccountControllerAR
/**
 * Access the _SwapeeMeAccountControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeAccountControllerAR}
 */
xyz.swapee.wc.back.ISwapeeMeAccountControllerARCaster.prototype.superSwapeeMeAccountControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/54-ISwapeeMeAccountControllerAT.xml}  5140520d5189a8a80b6ff0e8471a3860 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeMeAccountControllerAT)} xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT} xyz.swapee.wc.front.SwapeeMeAccountControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeMeAccountControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.constructor&xyz.swapee.wc.front.SwapeeMeAccountControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT|typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT|typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT|typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.Initialese[]) => xyz.swapee.wc.front.ISwapeeMeAccountControllerAT} xyz.swapee.wc.front.SwapeeMeAccountControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeMeAccountControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeMeAccountControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ISwapeeMeAccountControllerAT
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerAT = class extends /** @type {xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeMeAccountControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.Initialese>)} xyz.swapee.wc.front.SwapeeMeAccountControllerAT.constructor */
/**
 * A concrete class of _ISwapeeMeAccountControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeAccountControllerAT
 * @implements {xyz.swapee.wc.front.ISwapeeMeAccountControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ISwapeeMeAccountControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeMeAccountControllerAT = class extends /** @type {xyz.swapee.wc.front.SwapeeMeAccountControllerAT.constructor&xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeMeAccountControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountControllerAT}
 */
xyz.swapee.wc.front.SwapeeMeAccountControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeMeAccountControllerAT} */
xyz.swapee.wc.front.RecordISwapeeMeAccountControllerAT

/** @typedef {xyz.swapee.wc.front.ISwapeeMeAccountControllerAT} xyz.swapee.wc.front.BoundISwapeeMeAccountControllerAT */

/** @typedef {xyz.swapee.wc.front.SwapeeMeAccountControllerAT} xyz.swapee.wc.front.BoundSwapeeMeAccountControllerAT */

/**
 * Contains getters to cast the _ISwapeeMeAccountControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeAccountControllerATCaster
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerATCaster = class { }
/**
 * Cast the _ISwapeeMeAccountControllerAT_ instance into the _BoundISwapeeMeAccountControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeMeAccountControllerAT}
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerATCaster.prototype.asISwapeeMeAccountControllerAT
/**
 * Access the _SwapeeMeAccountControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeMeAccountControllerAT}
 */
xyz.swapee.wc.front.ISwapeeMeAccountControllerATCaster.prototype.superSwapeeMeAccountControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/70-ISwapeeMeAccountScreen.xml}  6a8306c1705475cdaae98fa7bc1f94f1 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.front.SwapeeMeAccountInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeMeAccountDisplay.Settings, !xyz.swapee.wc.ISwapeeMeAccountDisplay.Queries, null>&xyz.swapee.wc.ISwapeeMeAccountDisplay.Initialese} xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountScreen)} xyz.swapee.wc.AbstractSwapeeMeAccountScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountScreen} xyz.swapee.wc.SwapeeMeAccountScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountScreen` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountScreen
 */
xyz.swapee.wc.AbstractSwapeeMeAccountScreen = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountScreen.constructor&xyz.swapee.wc.SwapeeMeAccountScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountScreen.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountScreen.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.SwapeeMeAccountScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeMeAccountController|typeof xyz.swapee.wc.front.SwapeeMeAccountController)|(!xyz.swapee.wc.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.SwapeeMeAccountDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.SwapeeMeAccountScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeMeAccountController|typeof xyz.swapee.wc.front.SwapeeMeAccountController)|(!xyz.swapee.wc.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.SwapeeMeAccountDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.SwapeeMeAccountScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ISwapeeMeAccountController|typeof xyz.swapee.wc.front.SwapeeMeAccountController)|(!xyz.swapee.wc.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.SwapeeMeAccountDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountScreen} xyz.swapee.wc.SwapeeMeAccountScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.SwapeeMeAccountMemory, !xyz.swapee.wc.front.SwapeeMeAccountInputs, !HTMLDivElement, !xyz.swapee.wc.ISwapeeMeAccountDisplay.Settings, !xyz.swapee.wc.ISwapeeMeAccountDisplay.Queries, null, null>&xyz.swapee.wc.front.ISwapeeMeAccountController&xyz.swapee.wc.ISwapeeMeAccountDisplay)} xyz.swapee.wc.ISwapeeMeAccountScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ISwapeeMeAccountScreen
 */
xyz.swapee.wc.ISwapeeMeAccountScreen = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ISwapeeMeAccountController.typeof&xyz.swapee.wc.ISwapeeMeAccountDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese>)} xyz.swapee.wc.SwapeeMeAccountScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ISwapeeMeAccountScreen} xyz.swapee.wc.ISwapeeMeAccountScreen.typeof */
/**
 * A concrete class of _ISwapeeMeAccountScreen_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountScreen
 * @implements {xyz.swapee.wc.ISwapeeMeAccountScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountScreen = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountScreen.constructor&xyz.swapee.wc.ISwapeeMeAccountScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.SwapeeMeAccountScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountScreen} */
xyz.swapee.wc.RecordISwapeeMeAccountScreen

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountScreen} xyz.swapee.wc.BoundISwapeeMeAccountScreen */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountScreen} xyz.swapee.wc.BoundSwapeeMeAccountScreen */

/**
 * Contains getters to cast the _ISwapeeMeAccountScreen_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountScreenCaster
 */
xyz.swapee.wc.ISwapeeMeAccountScreenCaster = class { }
/**
 * Cast the _ISwapeeMeAccountScreen_ instance into the _BoundISwapeeMeAccountScreen_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountScreen}
 */
xyz.swapee.wc.ISwapeeMeAccountScreenCaster.prototype.asISwapeeMeAccountScreen
/**
 * Access the _SwapeeMeAccountScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountScreen}
 */
xyz.swapee.wc.ISwapeeMeAccountScreenCaster.prototype.superSwapeeMeAccountScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/70-ISwapeeMeAccountScreenBack.xml}  412a4fffec8865de17678a7794240c4c */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese} xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeAccountScreen)} xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeAccountScreen} xyz.swapee.wc.back.SwapeeMeAccountScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeAccountScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.constructor&xyz.swapee.wc.back.SwapeeMeAccountScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.back.SwapeeMeAccountScreen)|(!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT|typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.back.SwapeeMeAccountScreen)|(!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT|typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.back.SwapeeMeAccountScreen)|(!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT|typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeAccountScreen} xyz.swapee.wc.back.SwapeeMeAccountScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeAccountScreenCaster&xyz.swapee.wc.back.ISwapeeMeAccountScreenAT)} xyz.swapee.wc.back.ISwapeeMeAccountScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ISwapeeMeAccountScreenAT} xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountScreen
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreen = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeAccountScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeAccountScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese>)} xyz.swapee.wc.back.SwapeeMeAccountScreen.constructor */
/**
 * A concrete class of _ISwapeeMeAccountScreen_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeAccountScreen
 * @implements {xyz.swapee.wc.back.ISwapeeMeAccountScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeAccountScreen = class extends /** @type {xyz.swapee.wc.back.SwapeeMeAccountScreen.constructor&xyz.swapee.wc.back.ISwapeeMeAccountScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeAccountScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreen}
 */
xyz.swapee.wc.back.SwapeeMeAccountScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountScreen} */
xyz.swapee.wc.back.RecordISwapeeMeAccountScreen

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountScreen} xyz.swapee.wc.back.BoundISwapeeMeAccountScreen */

/** @typedef {xyz.swapee.wc.back.SwapeeMeAccountScreen} xyz.swapee.wc.back.BoundSwapeeMeAccountScreen */

/**
 * Contains getters to cast the _ISwapeeMeAccountScreen_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountScreenCaster
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenCaster = class { }
/**
 * Cast the _ISwapeeMeAccountScreen_ instance into the _BoundISwapeeMeAccountScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeAccountScreen}
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenCaster.prototype.asISwapeeMeAccountScreen
/**
 * Access the _SwapeeMeAccountScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeAccountScreen}
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenCaster.prototype.superSwapeeMeAccountScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/73-ISwapeeMeAccountScreenAR.xml}  9739671526f74546b34e785129bb0f09 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ISwapeeMeAccountScreen.Initialese} xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.SwapeeMeAccountScreenAR)} xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR} xyz.swapee.wc.front.SwapeeMeAccountScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ISwapeeMeAccountScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.constructor&xyz.swapee.wc.front.SwapeeMeAccountScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR|typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.SwapeeMeAccountScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR|typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.SwapeeMeAccountScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR|typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ISwapeeMeAccountScreen|typeof xyz.swapee.wc.SwapeeMeAccountScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR}
 */
xyz.swapee.wc.front.AbstractSwapeeMeAccountScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.Initialese[]) => xyz.swapee.wc.front.ISwapeeMeAccountScreenAR} xyz.swapee.wc.front.SwapeeMeAccountScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ISwapeeMeAccountScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ISwapeeMeAccountScreen)} xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeMeAccountScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ISwapeeMeAccountScreenAR
 */
xyz.swapee.wc.front.ISwapeeMeAccountScreenAR = class extends /** @type {xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ISwapeeMeAccountScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ISwapeeMeAccountScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.Initialese>)} xyz.swapee.wc.front.SwapeeMeAccountScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ISwapeeMeAccountScreenAR} xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.typeof */
/**
 * A concrete class of _ISwapeeMeAccountScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.SwapeeMeAccountScreenAR
 * @implements {xyz.swapee.wc.front.ISwapeeMeAccountScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ISwapeeMeAccountScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.SwapeeMeAccountScreenAR = class extends /** @type {xyz.swapee.wc.front.SwapeeMeAccountScreenAR.constructor&xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ISwapeeMeAccountScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.SwapeeMeAccountScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.SwapeeMeAccountScreenAR}
 */
xyz.swapee.wc.front.SwapeeMeAccountScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ISwapeeMeAccountScreenAR} */
xyz.swapee.wc.front.RecordISwapeeMeAccountScreenAR

/** @typedef {xyz.swapee.wc.front.ISwapeeMeAccountScreenAR} xyz.swapee.wc.front.BoundISwapeeMeAccountScreenAR */

/** @typedef {xyz.swapee.wc.front.SwapeeMeAccountScreenAR} xyz.swapee.wc.front.BoundSwapeeMeAccountScreenAR */

/**
 * Contains getters to cast the _ISwapeeMeAccountScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ISwapeeMeAccountScreenARCaster
 */
xyz.swapee.wc.front.ISwapeeMeAccountScreenARCaster = class { }
/**
 * Cast the _ISwapeeMeAccountScreenAR_ instance into the _BoundISwapeeMeAccountScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundISwapeeMeAccountScreenAR}
 */
xyz.swapee.wc.front.ISwapeeMeAccountScreenARCaster.prototype.asISwapeeMeAccountScreenAR
/**
 * Access the _SwapeeMeAccountScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundSwapeeMeAccountScreenAR}
 */
xyz.swapee.wc.front.ISwapeeMeAccountScreenARCaster.prototype.superSwapeeMeAccountScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/74-ISwapeeMeAccountScreenAT.xml}  5b171f6f047023447c634a3181729027 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.SwapeeMeAccountScreenAT)} xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT} xyz.swapee.wc.back.SwapeeMeAccountScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ISwapeeMeAccountScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.constructor&xyz.swapee.wc.back.SwapeeMeAccountScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT|typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT|typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT|typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT}
 */
xyz.swapee.wc.back.AbstractSwapeeMeAccountScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese[]) => xyz.swapee.wc.back.ISwapeeMeAccountScreenAT} xyz.swapee.wc.back.SwapeeMeAccountScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ISwapeeMeAccountScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeMeAccountScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountScreenAT
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenAT = class extends /** @type {xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ISwapeeMeAccountScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese>)} xyz.swapee.wc.back.SwapeeMeAccountScreenAT.constructor */
/**
 * A concrete class of _ISwapeeMeAccountScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.SwapeeMeAccountScreenAT
 * @implements {xyz.swapee.wc.back.ISwapeeMeAccountScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ISwapeeMeAccountScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.SwapeeMeAccountScreenAT = class extends /** @type {xyz.swapee.wc.back.SwapeeMeAccountScreenAT.constructor&xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ISwapeeMeAccountScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.SwapeeMeAccountScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.SwapeeMeAccountScreenAT}
 */
xyz.swapee.wc.back.SwapeeMeAccountScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountScreenAT} */
xyz.swapee.wc.back.RecordISwapeeMeAccountScreenAT

/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountScreenAT} xyz.swapee.wc.back.BoundISwapeeMeAccountScreenAT */

/** @typedef {xyz.swapee.wc.back.SwapeeMeAccountScreenAT} xyz.swapee.wc.back.BoundSwapeeMeAccountScreenAT */

/**
 * Contains getters to cast the _ISwapeeMeAccountScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ISwapeeMeAccountScreenATCaster
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenATCaster = class { }
/**
 * Cast the _ISwapeeMeAccountScreenAT_ instance into the _BoundISwapeeMeAccountScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundISwapeeMeAccountScreenAT}
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenATCaster.prototype.asISwapeeMeAccountScreenAT
/**
 * Access the _SwapeeMeAccountScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundSwapeeMeAccountScreenAT}
 */
xyz.swapee.wc.back.ISwapeeMeAccountScreenATCaster.prototype.superSwapeeMeAccountScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/swapee-me-account/SwapeeMeAccount.mvc/design/80-ISwapeeMeAccountGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ISwapeeMeAccountDisplay.Initialese} xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.SwapeeMeAccountGPU)} xyz.swapee.wc.AbstractSwapeeMeAccountGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.SwapeeMeAccountGPU} xyz.swapee.wc.SwapeeMeAccountGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ISwapeeMeAccountGPU` interface.
 * @constructor xyz.swapee.wc.AbstractSwapeeMeAccountGPU
 */
xyz.swapee.wc.AbstractSwapeeMeAccountGPU = class extends /** @type {xyz.swapee.wc.AbstractSwapeeMeAccountGPU.constructor&xyz.swapee.wc.SwapeeMeAccountGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractSwapeeMeAccountGPU.prototype.constructor = xyz.swapee.wc.AbstractSwapeeMeAccountGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractSwapeeMeAccountGPU.class = /** @type {typeof xyz.swapee.wc.AbstractSwapeeMeAccountGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountGPU|typeof xyz.swapee.wc.SwapeeMeAccountGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractSwapeeMeAccountGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractSwapeeMeAccountGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountGPU|typeof xyz.swapee.wc.SwapeeMeAccountGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ISwapeeMeAccountGPU|typeof xyz.swapee.wc.SwapeeMeAccountGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ISwapeeMeAccountDisplay|typeof xyz.swapee.wc.back.SwapeeMeAccountDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountGPU}
 */
xyz.swapee.wc.AbstractSwapeeMeAccountGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese[]) => xyz.swapee.wc.ISwapeeMeAccountGPU} xyz.swapee.wc.SwapeeMeAccountGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ISwapeeMeAccountGPUCaster&com.webcircuits.IBrowserView<.!SwapeeMeAccountMemory,.!SwapeeMeAccountLand>&xyz.swapee.wc.back.ISwapeeMeAccountDisplay)} xyz.swapee.wc.ISwapeeMeAccountGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!SwapeeMeAccountMemory,.!SwapeeMeAccountLand>} com.webcircuits.IBrowserView<.!SwapeeMeAccountMemory,.!SwapeeMeAccountLand>.typeof */
/**
 * Handles the periphery of the _ISwapeeMeAccountDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ISwapeeMeAccountGPU
 */
xyz.swapee.wc.ISwapeeMeAccountGPU = class extends /** @type {xyz.swapee.wc.ISwapeeMeAccountGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!SwapeeMeAccountMemory,.!SwapeeMeAccountLand>.typeof&xyz.swapee.wc.back.ISwapeeMeAccountDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ISwapeeMeAccountGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ISwapeeMeAccountGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ISwapeeMeAccountGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese>)} xyz.swapee.wc.SwapeeMeAccountGPU.constructor */
/**
 * A concrete class of _ISwapeeMeAccountGPU_ instances.
 * @constructor xyz.swapee.wc.SwapeeMeAccountGPU
 * @implements {xyz.swapee.wc.ISwapeeMeAccountGPU} Handles the periphery of the _ISwapeeMeAccountDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese>} ‎
 */
xyz.swapee.wc.SwapeeMeAccountGPU = class extends /** @type {xyz.swapee.wc.SwapeeMeAccountGPU.constructor&xyz.swapee.wc.ISwapeeMeAccountGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ISwapeeMeAccountGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ISwapeeMeAccountGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ISwapeeMeAccountGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.SwapeeMeAccountGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.SwapeeMeAccountGPU}
 */
xyz.swapee.wc.SwapeeMeAccountGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ISwapeeMeAccountGPU.
 * @interface xyz.swapee.wc.ISwapeeMeAccountGPUFields
 */
xyz.swapee.wc.ISwapeeMeAccountGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ISwapeeMeAccountGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountGPU} */
xyz.swapee.wc.RecordISwapeeMeAccountGPU

/** @typedef {xyz.swapee.wc.ISwapeeMeAccountGPU} xyz.swapee.wc.BoundISwapeeMeAccountGPU */

/** @typedef {xyz.swapee.wc.SwapeeMeAccountGPU} xyz.swapee.wc.BoundSwapeeMeAccountGPU */

/**
 * Contains getters to cast the _ISwapeeMeAccountGPU_ interface.
 * @interface xyz.swapee.wc.ISwapeeMeAccountGPUCaster
 */
xyz.swapee.wc.ISwapeeMeAccountGPUCaster = class { }
/**
 * Cast the _ISwapeeMeAccountGPU_ instance into the _BoundISwapeeMeAccountGPU_ type.
 * @type {!xyz.swapee.wc.BoundISwapeeMeAccountGPU}
 */
xyz.swapee.wc.ISwapeeMeAccountGPUCaster.prototype.asISwapeeMeAccountGPU
/**
 * Access the _SwapeeMeAccountGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundSwapeeMeAccountGPU}
 */
xyz.swapee.wc.ISwapeeMeAccountGPUCaster.prototype.superSwapeeMeAccountGPU

// nss:xyz.swapee.wc
/* @typal-end */