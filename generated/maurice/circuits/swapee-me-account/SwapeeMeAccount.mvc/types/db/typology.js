/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.ISwapeeMeAccountComputer': {
  'id': 62363285981,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeAccountMemoryPQs': {
  'id': 62363285982,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountOuterCore': {
  'id': 62363285983,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeAccountInputsPQs': {
  'id': 62363285984,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountPort': {
  'id': 62363285985,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetSwapeeMeAccountPort': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountPortInterface': {
  'id': 62363285986,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountCore': {
  'id': 62363285987,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetSwapeeMeAccountCore': 2
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountProcessor': {
  'id': 62363285988,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccount': {
  'id': 62363285989,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountBuffer': {
  'id': 623632859810,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountHtmlComponentUtil': {
  'id': 623632859811,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountHtmlComponent': {
  'id': 623632859812,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountElement': {
  'id': 623632859813,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildSwapeeMe': 4,
   'short': 5,
   'server': 6,
   'inducer': 7
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountElementPort': {
  'id': 623632859814,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountDesigner': {
  'id': 623632859815,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountGPU': {
  'id': 623632859816,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountDisplay': {
  'id': 623632859817,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeAccountVdusPQs': {
  'id': 623632859818,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountDisplay': {
  'id': 623632859819,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ISwapeeMeAccountController': {
  'id': 623632859820,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.ISwapeeMeAccountController': {
  'id': 623632859821,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountController': {
  'id': 623632859822,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountControllerAR': {
  'id': 623632859823,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeAccountControllerAT': {
  'id': 623632859824,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ISwapeeMeAccountScreen': {
  'id': 623632859825,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountScreen': {
  'id': 623632859826,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.ISwapeeMeAccountScreenAR': {
  'id': 623632859827,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.ISwapeeMeAccountScreenAT': {
  'id': 623632859828,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.SwapeeMeAccountQueriesPQs': {
  'id': 623632859829,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.SwapeeMeAccountClassesPQs': {
  'id': 623632859830,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})