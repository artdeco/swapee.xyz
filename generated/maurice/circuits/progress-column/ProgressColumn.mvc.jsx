/** @extends {xyz.swapee.wc.AbstractProgressColumn} */
export default class AbstractProgressColumn extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractProgressColumnComputer} */
export class AbstractProgressColumnComputer extends (<computer>
   <adapter name="adaptTransactionStatus">
    <xyz.swapee.wc.IProgressColumnCore status />
    <outputs>
     <xyz.swapee.wc.IProgressColumnCore transactionStatus />
    </outputs>
   </adapter>
   <adapter name="adaptProcessingTransaction">
    <xyz.swapee.wc.IProgressColumnCore transactionStatus />
    <outputs>
     <xyz.swapee.wc.IProgressColumnCore processingTransaction />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractProgressColumnController} */
export class AbstractProgressColumnController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractProgressColumnPort} */
export class ProgressColumnPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractProgressColumnView} */
export class AbstractProgressColumnView extends (<view>
  <classes>
   <string opt name="CircleLoading">Circles in steps which are being transitioned to next.</string>
   <string opt name="CircleComplete">Circles in steps which have been completed.</string>
   <string opt name="CircleWaiting">Circles in future steps.</string>
   <string opt name="CircleFailed" />

   <string opt name="StepLabelFailed" />
   <string opt name="StepLabelWaiting" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractProgressColumnElement} */
export class AbstractProgressColumnElement extends (<element v3 html mv>
 <block src="./ProgressColumn.mvc/src/ProgressColumnElement/methods/render.jsx" />
 <inducer src="./ProgressColumn.mvc/src/ProgressColumnElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractProgressColumnHtmlComponent} */
export class AbstractProgressColumnHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>