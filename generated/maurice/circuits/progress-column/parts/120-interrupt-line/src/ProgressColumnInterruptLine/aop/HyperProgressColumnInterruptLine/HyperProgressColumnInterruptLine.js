import AbstractHyperProgressColumnInterruptLine from '../../../../gen/AbstractProgressColumnInterruptLine/hyper/AbstractHyperProgressColumnInterruptLine'
import ProgressColumnInterruptLine from '../../ProgressColumnInterruptLine'
import ProgressColumnInterruptLineGeneralAspects from '../ProgressColumnInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperProgressColumnInterruptLine} */
export default class extends AbstractHyperProgressColumnInterruptLine
 .consults(
  ProgressColumnInterruptLineGeneralAspects,
 )
 .implements(
  ProgressColumnInterruptLine,
 )
{}