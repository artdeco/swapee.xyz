import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {6526983971} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const h=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const k=h["37270038985"],l=h["372700389810"],m=h["372700389811"];function M(b,c,d,e){return h["372700389812"](b,c,d,e,!1,void 0)};
const N=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const aa=N["61893096584"],ba=N["61893096586"],ca=N["618930965811"],da=N["618930965812"],ea=N["618930965815"],fa=N["618930965819"];function O(){}O.prototype={};function ha(){this.M=this.H=this.v=this.R=this.L=this.F=this.K=this.D=this.h=this.U=this.P=this.J=this.C=this.O=this.I=this.A=this.G=this.i=this.V=this.g=this.u=this.T=this.l=this.j=this.o=this.s=this.m=null}class ia{}class P extends M(ia,"IProgressColumnDisplay",ha,{W:1,la:2}){}
P[m]=[O,aa,{constructor(){k(this,()=>{this.scan({})})},scan:function(){const {element:b,X:{vdusPQs:{U:c,v:d,H:e,M:n,J:p,P:q,h:r,u:t,g:u,V:v,i:w,R:x,G:y,m:z,s:A,o:B,j:C,l:D,T:E,A:F,I:G,O:H,C:I,D:J,K,F:L,L:ja}}}=this,a=ea(b);Object.assign(this,{U:a[c],v:a[d],H:a[e],M:a[n],J:a[p],P:a[q],h:a[r],u:a[t],g:a[u],V:a[v],i:a[w],R:a[x],G:a[y],m:a[z],s:a[A],o:a[B],j:a[C],l:a[D],T:a[E],A:a[F],I:a[G],O:a[H],C:a[I],D:a[J],K:a[K],F:a[L],L:a[ja]})}},{[l]:{m:1,s:1,o:1,j:1,l:1,T:1,u:1,g:1,V:1,
i:1,G:1,A:1,I:1,O:1,C:1,J:1,P:1,U:1,h:1,D:1,K:1,F:1,L:1,R:1,v:1,H:1,M:1},initializer({m:b,s:c,o:d,j:e,l:n,T:p,u:q,g:r,V:t,i:u,G:v,A:w,I:x,O:y,C:z,J:A,P:B,U:C,h:D,D:E,K:F,F:G,L:H,R:I,v:J,H:K,M:L}){void 0!==b&&(this.m=b);void 0!==c&&(this.s=c);void 0!==d&&(this.o=d);void 0!==e&&(this.j=e);void 0!==n&&(this.l=n);void 0!==p&&(this.T=p);void 0!==q&&(this.u=q);void 0!==r&&(this.g=r);void 0!==t&&(this.V=t);void 0!==u&&(this.i=u);void 0!==v&&(this.G=v);void 0!==w&&(this.A=w);void 0!==x&&(this.I=x);void 0!==
y&&(this.O=y);void 0!==z&&(this.C=z);void 0!==A&&(this.J=A);void 0!==B&&(this.P=B);void 0!==C&&(this.U=C);void 0!==D&&(this.h=D);void 0!==E&&(this.D=E);void 0!==F&&(this.K=F);void 0!==G&&(this.F=G);void 0!==H&&(this.L=H);void 0!==I&&(this.R=I);void 0!==J&&(this.v=J);void 0!==K&&(this.H=K);void 0!==L&&(this.M=L)}}];var Q=class extends P.implements(){};function R(){}R.prototype={};class ka{}class S extends M(ka,"IProgressColumnControllerAT",null,{$:1,ka:2}){}S[m]=[R,ca,{}];function T(){}T.prototype={};class la{}class U extends M(la,"IProgressColumnScreenAR",null,{aa:1,oa:2}){}U[m]=[T,da,{allocator(){this.methods={}}}];const V={ba:"a74ad",ca:"89fc6",da:"a7265",ia:"02236",ha:"497c0",transactionId:"67113",Y:"b5666",status:"9acb4",ea:"da35f",fa:"277b1",ga:"d7265"};const ma={...V};const na=Object.keys(V).reduce((b,c)=>{b[V[c]]=c;return b},{});const W={ja:"7121e",Z:"b274c",pa:"de5a0",qa:"517c7"};const oa=Object.keys(W).reduce((b,c)=>{b[W[c]]=c;return b},{});function X(){}X.prototype={};class pa{}class Y extends M(pa,"IProgressColumnScreen",null,{X:1,ma:2}){}function Z(){}
Y[m]=[X,Z.prototype={deduceInputs(){const {W:{h:b,g:c,i:d}}=this;return{transactionId:null==b?void 0:b.innerText,Y:null==c?void 0:c.innerText,Z:null==d?void 0:d.innerText}}},Z.prototype={inputsPQs:ma,memoryQPs:na,cacheQPs:oa},ba,fa,U,Z.prototype={vdusPQs:{U:"fa9e1",v:"fa9e2",H:"fa9e3",M:"fa9e4",J:"fa9e5",h:"fa9e6",R:"fa9e9",G:"fa9e10",C:"fa9e12",D:"fa9e13",P:"fa9e14",u:"fa9e15",g:"fa9e16",V:"fa9e17",i:"fa9e18",F:"fa9e19",L:"fa9e20",l:"fa9e21",T:"fa9e22",A:"fa9e23",I:"fa9e24",O:"fa9e25",K:"fa9e26",
s:"fa9e27",o:"fa9e28",j:"fa9e29",m:"fa9e30"}}];var qa=class extends Y.implements(S,Q,{get queries(){return this.settings}},{__$id:6526983971}){};module.exports["652698397141"]=Q;module.exports["652698397171"]=qa;
/*! @embed-object-end {6526983971} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule