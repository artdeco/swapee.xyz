/**
 * Display for presenting information from the _IProgressColumn_.
 * @extends {xyz.swapee.wc.ProgressColumnDisplay}
 */
class ProgressColumnDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ProgressColumnScreen}
 */
class ProgressColumnScreen extends (class {/* lazy-loaded */}) {}

module.exports.ProgressColumnDisplay = ProgressColumnDisplay
module.exports.ProgressColumnScreen = ProgressColumnScreen