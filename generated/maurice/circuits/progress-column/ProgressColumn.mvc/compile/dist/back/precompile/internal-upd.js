import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {6526983971} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const f=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const ta=f["372700389810"],g=f["372700389811"];function h(a,b,c,d){return f["372700389812"](a,b,c,d,!1,void 0)};function k(){}k.prototype={};class ua{}class l extends h(ua,"IProgressColumnProcessor",null,{Ca:1,Pa:2}){}l[g]=[k];
const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const va=m["61505580523"],n=m["615055805212"],wa=m["615055805218"],xa=m["615055805221"],ya=m["615055805223"],p=m["615055805233"],za=m["615055805238"];const q={Fa:"a74ad",la:"89fc6",Y:"a7265",Z:"02236",fa:"497c0",transactionId:"67113",$:"b5666",status:"9acb4",ca:"da35f",da:"277b1",ea:"d7265"};const r={ga:"7121e",h:"b274c",ma:"de5a0",oa:"517c7"};function t(){}t.prototype={};function Aa(){this.model={ga:!1,h:"",ma:!1,oa:!1}}class Ba{}class u extends h(Ba,"IProgressColumnCore",Aa,{xa:1,Ja:2}){}function v(){}v.prototype={};function w(){this.model={la:!1,Y:!1,Z:!1,fa:!1,transactionId:"",$:"",ca:!1,da:!1,ea:!1,status:""}}class Ca{}class x extends h(Ca,"IProgressColumnOuterCore",w,{Aa:1,Na:2}){}x[g]=[v,{constructor(){p(this.model,"",q);p(this.model,"",r)}}];u[g]=[{},t,x];

const y=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const Da=y.IntegratedController,Ea=y.Parametric;
const z=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const Fa=z.IntegratedComponentInitialiser,Ga=z.IntegratedComponent,Ha=z["38"];function A(){}A.prototype={};class Ia{}class B extends h(Ia,"IProgressColumnComputer",null,{ua:1,Ha:2}){}B[g]=[A,wa];const C={regulate:n({la:Boolean,Y:Boolean,Z:Boolean,fa:Boolean,transactionId:String,$:String,ca:Boolean,da:Boolean,ea:Boolean,status:String})};const D={...q};function E(){}E.prototype={};function Ja(){const a={model:null};w.call(a);this.inputs=a.model}class Ka{}class F extends h(Ka,"IProgressColumnPort",Ja,{Ba:1,Oa:2}){}function G(){}F[g]=[G.prototype={},E,Ea,G.prototype={constructor(){p(this.inputs,"",D)}}];function H(){}H.prototype={};class La{}class I extends h(La,"IProgressColumnController",null,{ra:1,sa:2}){}I[g]=[{},H,C,Da,{get Port(){return F}}];const Ma=n({ga:Boolean,h:String,ma:Boolean,oa:Boolean},{silent:!0});const Na=Object.keys(r).reduce((a,b)=>{a[r[b]]=b;return a},{});function J(){}J.prototype={};class Oa{}class K extends h(Oa,"IProgressColumn",null,{ta:1,Ga:2}){}K[g]=[J,u,l,Ga,B,I,{regulateState:Ma,stateQPs:Na}];
const L=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const Pa=L["12817393923"],Qa=L["12817393924"],Ra=L["12817393925"],Sa=L["12817393926"];function M(){}M.prototype={};class Ta{}class N extends h(Ta,"IProgressColumnControllerAR",null,{wa:1,Ia:2}){}N[g]=[M,Sa,{allocator(){this.methods={}}}];function O(){}O.prototype={};class Ua{}class P extends h(Ua,"IProgressColumnController",null,{ra:1,sa:2}){}P[g]=[O,I,N,Pa];var Q=class extends P.implements(){};const Va=B.__trait({pa:function({h:a}){if(["exchanging","sending"].includes(a))return{ga:!0}},qa:function({status:a}){if(["exchanging","sending","failed","finished"].includes(a))return{h:a}},adapt:[function(a,b,c){a={h:a.h};a=c?c(a):a;b=c?c(b):b;return this.pa(a,b)},function(a,b,c){a={status:a.status};a=c?c(a):a;b=c?c(b):b;return this.qa(a,b)}]});class R extends B.implements(Va){};function S(){}S.prototype={};class Wa{}class T extends h(Wa,"IProgressColumnDisplay",null,{ya:1,Ka:2}){}
T[g]=[S,Qa,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{D:a,l:a,i:a,j:a,m:a,o:a,C:a,u:a,s:a,G:a,F:a,A:a,v:a,J:a,L:a,K:a,H:a,I:a,X:a,M:a,T:a,W:a,O:a,P:a,U:a,R:a,V:a})}},{[ta]:{J:1,L:1,K:1,H:1,I:1,X:1,u:1,s:1,G:1,F:1,v:1,M:1,T:1,W:1,O:1,m:1,o:1,D:1,C:1,P:1,U:1,R:1,V:1,A:1,l:1,i:1,j:1},initializer({J:a,L:b,K:c,H:d,I:e,X:W,u:X,s:Y,G:Z,F:aa,v:ba,M:ca,T:da,W:ea,O:fa,m:ha,o:ia,D:ja,C:ka,P:la,U:ma,R:na,V:oa,A:pa,l:qa,i:ra,j:sa}){void 0!==
a&&(this.J=a);void 0!==b&&(this.L=b);void 0!==c&&(this.K=c);void 0!==d&&(this.H=d);void 0!==e&&(this.I=e);void 0!==W&&(this.X=W);void 0!==X&&(this.u=X);void 0!==Y&&(this.s=Y);void 0!==Z&&(this.G=Z);void 0!==aa&&(this.F=aa);void 0!==ba&&(this.v=ba);void 0!==ca&&(this.M=ca);void 0!==da&&(this.T=da);void 0!==ea&&(this.W=ea);void 0!==fa&&(this.O=fa);void 0!==ha&&(this.m=ha);void 0!==ia&&(this.o=ia);void 0!==ja&&(this.D=ja);void 0!==ka&&(this.C=ka);void 0!==la&&(this.P=la);void 0!==ma&&(this.U=ma);void 0!==
na&&(this.R=na);void 0!==oa&&(this.V=oa);void 0!==pa&&(this.A=pa);void 0!==qa&&(this.l=qa);void 0!==ra&&(this.i=ra);void 0!==sa&&(this.j=sa)}}];const U={ba:"5e522",aa:"a5ad9",ia:"9912b",ha:"dc9d7",ja:"7e898",ka:"e4d45"};const Xa=Object.keys(U).reduce((a,b)=>{a[U[b]]=b;return a},{});const Ya={D:"fa9e1",l:"fa9e2",i:"fa9e3",j:"fa9e4",m:"fa9e5",C:"fa9e6",A:"fa9e9",v:"fa9e10",O:"fa9e12",P:"fa9e13",o:"fa9e14",u:"fa9e15",s:"fa9e16",G:"fa9e17",F:"fa9e18",R:"fa9e19",V:"fa9e20",I:"fa9e21",X:"fa9e22",M:"fa9e23",T:"fa9e24",W:"fa9e25",U:"fa9e26",L:"fa9e27",K:"fa9e28",H:"fa9e29",J:"fa9e30"};const Za=Object.keys(Ya).reduce((a,b)=>{a[Ya[b]]=b;return a},{});function $a(){}$a.prototype={};class ab{}class bb extends h(ab,"IProgressColumnGPU",null,{g:1,La:2}){}function cb(){}bb[g]=[$a,cb.prototype={classesQPs:Xa,vdusQPs:Za,memoryPQs:q},T,va,cb.prototype={allocator(){Ha(this.classes,"",U)}}];function db(){}db.prototype={};class eb{}class fb extends h(eb,"IProgressColumnScreenAT",null,{Ea:1,Ra:2}){}fb[g]=[db,Ra];function gb(){}gb.prototype={};class hb{}class ib extends h(hb,"IProgressColumnScreen",null,{Da:1,Qa:2}){}ib[g]=[gb,fb];const jb=Object.keys(D).reduce((a,b)=>{a[D[b]]=b;return a},{});function kb(){}kb.prototype={};class lb{static mvc(a,b,c){return ya(this,a,b,null,c)}}class mb extends h(lb,"IProgressColumnHtmlComponent",null,{za:1,Ma:2}){}function V(){}
mb[g]=[kb,xa,K,bb,ib,V.prototype={inputsQPs:jb},{paint:[function(){this.C.setText(this.model.transactionId)},function(){this.s.setText(this.model.$)},function(){this.F.setText(this.model.h)}]},V.prototype={paint:function({ca:a}){const {g:{l:b},classes:{ba:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?d(b,c):e(b,c)}},V.prototype={paint:function({transactionId:a}){const {g:{l:b},classes:{aa:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?d(b,c):e(b,c)}},V.prototype={paint:function({da:a}){const {g:{i:b},
classes:{ba:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?d(b,c):e(b,c)}},V.prototype={paint:function({transactionId:a}){const {g:{i:b},classes:{ia:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?e(b,c):d(b,c)}},V.prototype={paint:function({Z:a}){const {g:{i:b},classes:{ha:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?d(b,c):e(b,c)}},V.prototype={paint:function({fa:a}){const {g:{i:b},classes:{aa:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?d(b,c):e(b,c)}},V.prototype={paint:function({ea:a}){const {g:{j:b},
classes:{ba:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?d(b,c):e(b,c)}},V.prototype={paint:function({h:a}){const {g:{j:b},classes:{ia:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?e(b,c):d(b,c)}},V.prototype={paint:function({status:a}){const {g:{j:b},classes:{ha:c},asIBrowserView:{addClass:d,removeClass:e}}=this;"failed"==a?d(b,c):e(b,c)}},V.prototype={paint:function({status:a}){const {g:{j:b},classes:{aa:c},asIBrowserView:{addClass:d,removeClass:e}}=this;"finished"==a?d(b,c):e(b,
c)}},V.prototype={paint:function({Z:a}){const {g:{m:b},classes:{ja:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?d(b,c):e(b,c)}},V.prototype={paint:function({transactionId:a}){const {g:{m:b},classes:{ka:c},asIBrowserView:{addClass:d,removeClass:e}}=this;a?e(b,c):d(b,c)}},V.prototype={paint:function({status:a}){const {g:{o:b},classes:{ja:c},asIBrowserView:{addClass:d,removeClass:e}}=this;"failed"==a?d(b,c):e(b,c)}},V.prototype={paint:function({h:a}){const {g:{o:b},classes:{ka:c},asIBrowserView:{addClass:d,
removeClass:e}}=this;a?e(b,c):d(b,c)}},V.prototype={paint:za({D:{transactionId:1},u:{$:1},G:{h:1},A:{Y:0},v:{Y:0}})}];var nb=class extends mb.implements(Q,R,Fa){};module.exports["65269839710"]=K;module.exports["65269839711"]=K;module.exports["65269839713"]=F;module.exports["65269839714"]=I;module.exports["652698397110"]=nb;module.exports["652698397111"]=C;module.exports["652698397130"]=B;module.exports["652698397131"]=R;module.exports["652698397161"]=Q;
/*! @embed-object-end {6526983971} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule