import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractProgressColumn}*/
export class AbstractProgressColumn extends Module['65269839711'] {}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumn} */
AbstractProgressColumn.class=function(){}
/** @type {typeof xyz.swapee.wc.ProgressColumnPort} */
export const ProgressColumnPort=Module['65269839713']
/**@extends {xyz.swapee.wc.AbstractProgressColumnController}*/
export class AbstractProgressColumnController extends Module['65269839714'] {}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnController} */
AbstractProgressColumnController.class=function(){}
/** @type {typeof xyz.swapee.wc.ProgressColumnHtmlComponent} */
export const ProgressColumnHtmlComponent=Module['652698397110']
/** @type {typeof xyz.swapee.wc.ProgressColumnBuffer} */
export const ProgressColumnBuffer=Module['652698397111']
/**@extends {xyz.swapee.wc.AbstractProgressColumnComputer}*/
export class AbstractProgressColumnComputer extends Module['652698397130'] {}
/** @type {typeof xyz.swapee.wc.AbstractProgressColumnComputer} */
AbstractProgressColumnComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.ProgressColumnComputer} */
export const ProgressColumnComputer=Module['652698397131']
/** @type {typeof xyz.swapee.wc.back.ProgressColumnController} */
export const ProgressColumnController=Module['652698397161']