/**
 * An abstract class of `xyz.swapee.wc.IProgressColumn` interface.
 * @extends {xyz.swapee.wc.AbstractProgressColumn}
 */
class AbstractProgressColumn extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IProgressColumn_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ProgressColumnPort}
 */
class ProgressColumnPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnController` interface.
 * @extends {xyz.swapee.wc.AbstractProgressColumnController}
 */
class AbstractProgressColumnController extends (class {/* lazy-loaded */}) {}
/**
 * The _IProgressColumn_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ProgressColumnHtmlComponent}
 */
class ProgressColumnHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ProgressColumnBuffer}
 */
class ProgressColumnBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IProgressColumnComputer` interface.
 * @extends {xyz.swapee.wc.AbstractProgressColumnComputer}
 */
class AbstractProgressColumnComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ProgressColumnComputer}
 */
class ProgressColumnComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ProgressColumnController}
 */
class ProgressColumnController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractProgressColumn = AbstractProgressColumn
module.exports.ProgressColumnPort = ProgressColumnPort
module.exports.AbstractProgressColumnController = AbstractProgressColumnController
module.exports.ProgressColumnHtmlComponent = ProgressColumnHtmlComponent
module.exports.ProgressColumnBuffer = ProgressColumnBuffer
module.exports.AbstractProgressColumnComputer = AbstractProgressColumnComputer
module.exports.ProgressColumnComputer = ProgressColumnComputer
module.exports.ProgressColumnController = ProgressColumnController