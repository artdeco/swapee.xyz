import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {6526983971} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const b=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const e=b["372700389811"];function g(a,c,d,f){return b["372700389812"](a,c,d,f,!1,void 0)};function h(){}h.prototype={};class aa{}class q extends g(aa,"IProgressColumnProcessor",null,{V:1,Aa:2}){}q[e]=[h];

const r=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const u=r["615055805212"],ba=r["615055805218"],v=r["615055805233"];const w={core:"a74ad",v:"89fc6",i:"a7265",s:"02236",o:"497c0",transactionId:"67113",u:"b5666",status:"9acb4",j:"da35f",l:"277b1",m:"d7265"};const x={A:"7121e",g:"b274c",D:"de5a0",F:"517c7"};function y(){}y.prototype={};function ca(){this.model={A:!1,g:"",D:!1,F:!1}}class da{}class z extends g(da,"IProgressColumnCore",ca,{M:1,va:2}){}function A(){}A.prototype={};function B(){this.model={v:!1,i:!1,s:!1,o:!1,transactionId:"",u:"",j:!1,l:!1,m:!1,status:""}}class ea{}class D extends g(ea,"IProgressColumnOuterCore",B,{T:1,ya:2}){}D[e]=[A,{constructor(){v(this.model,"",w);v(this.model,"",x)}}];z[e]=[{},y,D];
const E=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const fa=E.IntegratedComponentInitialiser,F=E.IntegratedComponent,ha=E["95173443851"];function G(){}G.prototype={};class ia{}class H extends g(ia,"IProgressColumnComputer",null,{J:1,ta:2}){}H[e]=[G,ba];const I={regulate:u({v:Boolean,i:Boolean,s:Boolean,o:Boolean,transactionId:String,u:String,j:Boolean,l:Boolean,m:Boolean,status:String})};
const J=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ja=J.IntegratedController,ka=J.Parametric;const K={...w};function L(){}L.prototype={};function la(){const a={model:null};B.call(a);this.inputs=a.model}class ma{}class M extends g(ma,"IProgressColumnPort",la,{U:1,za:2}){}function N(){}M[e]=[N.prototype={},L,ka,N.prototype={constructor(){v(this.inputs,"",K)}}];function O(){}O.prototype={};class na{}class P extends g(na,"IProgressColumnController",null,{K:1,ua:2}){}P[e]=[{},O,I,ja,{get Port(){return M}}];const oa=u({A:Boolean,g:String,D:Boolean,F:Boolean},{silent:!0});const pa=Object.keys(x).reduce((a,c)=>{a[x[c]]=c;return a},{});function Q(){}Q.prototype={};class qa{}class R extends g(qa,"IProgressColumn",null,{I:1,sa:2}){}R[e]=[Q,z,q,F,H,P,{regulateState:oa,stateQPs:pa}];function ra(){return{}};const sa=require(eval('"@type.engineering/web-computing"')).h;function ta(){return sa("div",{$id:"ProgressColumn"})};const S=require(eval('"@type.engineering/web-computing"')).h;
function ua({j:a,l:c,m:d,transactionId:f,s:m,u:n,i:p,o:t,g:k,status:l}){return S("div",{$id:"ProgressColumn"},S("p",{$id:"TransactionIdWr",$reveal:f}),S("span",{$id:"Step1Circle",CircleLoading:a,CircleComplete:!!f}),S("span",{$id:"Step2Circle",CircleLoading:c,CircleWaiting:!f,CircleFailed:m,CircleComplete:t}),S("span",{$id:"Step3Circle",CircleLoading:d,CircleWaiting:!k,CircleFailed:"failed"==l,CircleComplete:"finished"==l}),S("span",{$id:"Step2La",StepLabelFailed:m,StepLabelWaiting:!f}),S("span",
{$id:"Step3La",StepLabelFailed:"failed"==l,StepLabelWaiting:!k}),S("p",{$id:"TransactionIdLa"},f," "),S("div",{$id:"PaymentStatusWr",$reveal:n},S("span",{$id:"PaymentStatusLa"},n)),S("div",{$id:"TransactionStatusWr",$reveal:k},S("span",{$id:"TransactionStatusLa"},k)),S("div",{$id:"Step3Wr",$conceal:p}),S("div",{$id:"Step2BotSep",$conceal:p}))};var T=class extends P.implements(){};const va=H.__trait({G:function({g:a}){if(["exchanging","sending"].includes(a))return{A:!0}},H:function({status:a}){if(["exchanging","sending","failed","finished"].includes(a))return{g:a}},adapt:[function(a,c,d){a={g:a.g};a=d?d(a):a;c=d?d(c):c;return this.G(a,c)},function(a,c,d){a={status:a.status};a=d?d(a):a;c=d?d(c):c;return this.H(a,c)}]});class U extends H.implements(va){};require("https");require("http");const wa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}wa("aqt");require("fs");require("child_process");
const V=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const xa=V.ElementBase,ya=V.HTMLBlocker;const W=require(eval('"@type.engineering/web-computing"')).h;function X(){}X.prototype={};function za(){this.inputs={noSolder:!1,Y:{},$:{},Z:{},W:{},X:{},Ba:{},ba:{},aa:{},Fa:{},Ea:{},ia:{},ea:{},ka:{},pa:{},fa:{},la:{},qa:{},Da:{},Ca:{},ga:{},ma:{},ha:{},na:{},ra:{},da:{},ja:{},oa:{}}}class Aa{}class Ba extends g(Aa,"IProgressColumnElementPort",za,{R:1,xa:2}){}
Ba[e]=[X,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"kyc-block-opts":void 0,"kyc-required-opts":void 0,"kyc-not-required-opts":void 0,"exchange-email-opts":void 0,"info-notice-opts":void 0,"support-notice-opts":void 0,"payment-status-wr-opts":void 0,"payment-status-la-opts":void 0,"transaction-status-wr-opts":void 0,"transaction-status-la-opts":void 0,"step2-bot-sep-opts":void 0,"step1-co-opts":void 0,"step2-co-opts":void 0,"step3-co-opts":void 0,"step1-la-opts":void 0,
"step2-la-opts":void 0,"step3-la-opts":void 0,"transaction-id-wr-opts":void 0,"transaction-id-la-opts":void 0,"step1-progress-circle-wr-opts":void 0,"step2-progress-circle-wr-opts":void 0,"step1-wr-opts":void 0,"step2-wr-opts":void 0,"step3-wr-opts":void 0,"step1-circle-opts":void 0,"step2-circle-opts":void 0,"step3-circle-opts":void 0,"creating-transaction":void 0,"finished-on-payment":void 0,"payment-failed":void 0,"payment-completed":void 0,"transaction-id":void 0,"payment-status":void 0,"loading-step1":void 0,
"loading-step2":void 0,"loading-step3":void 0})}}];function Ca(){}Ca.prototype={};class Da{}class Y extends g(Da,"IProgressColumnElement",null,{P:1,wa:2}){}function Z(){}
Y[e]=[Ca,xa,Z.prototype={calibrate:function({":no-solder":a,":creating-transaction":c,":finished-on-payment":d,":payment-failed":f,":payment-completed":m,":transaction-id":n,":payment-status":p,":loading-step1":t,":loading-step2":k,":loading-step3":l,":status":C}){const {attributes:{"no-solder":Ea,"creating-transaction":Fa,"finished-on-payment":Ga,"payment-failed":Ha,"payment-completed":Ia,"transaction-id":Ja,"payment-status":Ka,"loading-step1":La,"loading-step2":Ma,"loading-step3":Na,status:Oa}}=
this;return{...(void 0===Ea?{"no-solder":a}:{}),...(void 0===Fa?{"creating-transaction":c}:{}),...(void 0===Ga?{"finished-on-payment":d}:{}),...(void 0===Ha?{"payment-failed":f}:{}),...(void 0===Ia?{"payment-completed":m}:{}),...(void 0===Ja?{"transaction-id":n}:{}),...(void 0===Ka?{"payment-status":p}:{}),...(void 0===La?{"loading-step1":t}:{}),...(void 0===Ma?{"loading-step2":k}:{}),...(void 0===Na?{"loading-step3":l}:{}),...(void 0===Oa?{status:C}:{})}}},Z.prototype={calibrate:({"no-solder":a,
"creating-transaction":c,"finished-on-payment":d,"payment-failed":f,"payment-completed":m,"transaction-id":n,"payment-status":p,"loading-step1":t,"loading-step2":k,"loading-step3":l,status:C})=>({noSolder:a,v:c,i:d,s:f,o:m,transactionId:n,u:p,j:t,l:k,m:l,status:C})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return W("div",{$id:"ProgressColumn"},W("vdu",{$id:"TransactionIdWr"}),W("vdu",{$id:"Step1Circle"}),W("vdu",{$id:"Step2Circle"}),W("vdu",{$id:"Step3Circle"}),
W("vdu",{$id:"Step2La"}),W("vdu",{$id:"Step3La"}),W("vdu",{$id:"TransactionIdLa"}),W("vdu",{$id:"PaymentStatusWr"}),W("vdu",{$id:"PaymentStatusLa"}),W("vdu",{$id:"TransactionStatusWr"}),W("vdu",{$id:"TransactionStatusLa"}),W("vdu",{$id:"Step3Wr"}),W("vdu",{$id:"Step2BotSep"}),W("vdu",{$id:"KycBlock"}),W("vdu",{$id:"KycRequired"}),W("vdu",{$id:"KycNotRequired"}),W("vdu",{$id:"ExchangeEmail"}),W("vdu",{$id:"InfoNotice"}),W("vdu",{$id:"SupportNotice"}),W("vdu",{$id:"Step1Co"}),W("vdu",{$id:"Step2Co"}),
W("vdu",{$id:"Step3Co"}),W("vdu",{$id:"Step1La"}),W("vdu",{$id:"Step1ProgressCircleWr"}),W("vdu",{$id:"Step2ProgressCircleWr"}),W("vdu",{$id:"Step1Wr"}),W("vdu",{$id:"Step2Wr"}))}},Z.prototype={classes:{CircleLoading:"5e522",CircleComplete:"a5ad9",CircleWaiting:"9912b",CircleFailed:"dc9d7",StepLabelFailed:"7e898",StepLabelWaiting:"e4d45"},inputsPQs:K,cachePQs:x,vdus:{TransactionIdWr:"fa9e1",Step1Circle:"fa9e2",Step2Circle:"fa9e3",Step3Circle:"fa9e4",Step2La:"fa9e5",TransactionIdLa:"fa9e6",Step3Wr:"fa9e9",
Step2BotSep:"fa9e10",Step1La:"fa9e12",Step1ProgressCircleWr:"fa9e13",Step3La:"fa9e14",PaymentStatusWr:"fa9e15",PaymentStatusLa:"fa9e16",TransactionStatusWr:"fa9e17",TransactionStatusLa:"fa9e18",Step1Wr:"fa9e19",Step2Wr:"fa9e20",InfoNotice:"fa9e21",SupportNotice:"fa9e22",Step1Co:"fa9e23",Step2Co:"fa9e24",Step3Co:"fa9e25",Step2ProgressCircleWr:"fa9e26",KycRequired:"fa9e27",KycNotRequired:"fa9e28",ExchangeEmail:"fa9e29",KycBlock:"fa9e30"}},F,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder creatingTransaction finishedOnPayment paymentFailed paymentCompleted transactionId paymentStatus loadingStep1 loadingStep2 loadingStep3 status no-solder :no-solder creating-transaction :creating-transaction finished-on-payment :finished-on-payment payment-failed :payment-failed payment-completed :payment-completed transaction-id :transaction-id payment-status :payment-status loading-step1 :loading-step1 loading-step2 :loading-step2 loading-step3 :loading-step3 :status fe646 ef1d4 6c5cd 35399 c8e3d 2598d 6d28e 87e7e c8d31 c6c14 f378b 4fba9 87196 c3763 08522 fbf32 3cc1f 41e54 77c32 8a859 07d58 10349 e5771 f81a7 70de9 7bc63 41102 f4b68 89fc6 a7265 02236 497c0 67113 b5666 da35f 277b1 d7265 9acb4 children".split(" "))})},
get Port(){return Ba}}];Y[e]=[R,{rootId:"ProgressColumn",__$id:6526983971,fqn:"xyz.swapee.wc.IProgressColumn",maurice_element_v3:!0}];class Pa extends Y.implements(T,U,ha,ya,fa,{solder:ra,server:ta,render:ua},{classesMap:!0,rootSelector:".ProgressColumn",stylesheet:"html/styles/ProgressColumn.css",blockName:"html/ProgressColumnBlock.html"}){};module.exports["65269839710"]=R;module.exports["65269839711"]=R;module.exports["65269839713"]=M;module.exports["65269839714"]=P;module.exports["65269839718"]=Pa;module.exports["652698397111"]=I;module.exports["652698397130"]=H;module.exports["652698397131"]=U;module.exports["652698397161"]=T;
/*! @embed-object-end {6526983971} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule